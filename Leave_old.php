<?php
  //////////////////////////////////////////////////////////////////////////////
  // This page allows users to manage and view leave.                         //
  //////////////////////////////////////////////////////////////////////////////
  
  include 'Scripts/Include.php';
  SetSettings();
  CheckAuthorisation('Leave.php');
  
  //////////////////////////////////////////////////////////////////////////////
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3c.org/TR/1999/REC-html401-19991224/loose.dtd">
<HTML>
  <HEAD>
    <?php
      // PHP SCRIPT ////////////////////////////////////////////////////////////
      BuildHead('Leave');
      //////////////////////////////////////////////////////////////////////////
    ?>
  </HEAD>
  <BODY>
    <?php
      // PHP SCRIPT ////////////////////////////////////////////////////////////
      BuildBanner();
      //////////////////////////////////////////////////////////////////////////
    ?>
    <DIV class="contentcontainer">
      <?php
        // PHP SCRIPT //////////////////////////////////////////////////////////
        BuildMenu('Main', 'Leave.php');
        ////////////////////////////////////////////////////////////////////////
      ?>
      <DIV class="content">
        <BR /><BR />
        <?php
          // PHP SCRIPT ////////////////////////////////////////////////////////
          if ($_GET['id'])
          {
            $id = SubStr($_GET['id'], 5, StrLen($_GET['id']) - 5);
            $_SESSION['ApproveLeave'] = array($id);
          }
          
          BuildMessageSet('Leave');
          
          $resultSetAvail = ExecuteQuery('SELECT * FROM AvailabilityType ORDER BY AvailabilityType_ID ASC');
          if (MySQL_Num_Rows($resultSetAvail) > 0)
          {
            $availabilities = array();
            while ($row = MySQL_Fetch_Array($resultSetAvail))
            {
              $availabilities[] = $row['AvailabilityType_Description'];
            }
          }
          
          $resultSetLeave = ExecuteQuery('SELECT * FROM LeaveType ORDER BY LeaveType_ID ASC');
          if (MySQL_Num_Rows($resultSetLeave) > 0)
          {
            $leaves = array();
            while ($row = MySQL_Fetch_Array($resultSetLeave))
            {
              $leaves[] = $row['LeaveType_Description'];
            }
          }
          //////////////////////////////////////////////////////////////////////
        ?>
        <?php
          // PHP SCRIPT ////////////////////////////////////////////////////////
          if (isset($_SESSION['AllocateLeave']))
          {
            BuildContentHeader('Allocate Leave', "", "", false);  
            echo '<DIV class="contentflow">
                    <P>Enter the details of the allocation below. The allocated leave will be entered for the last day of the specified month.</P>
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center" class="short">
                      <FORM method="post" action="Handlers/Leave_Handler.php">
                        <INPUT name="Type" type="hidden" value="Allocate" />
                        <TR>
                          <TD colspan="4" class="header">Allocation Details
                          </TD>
                        </TR>
                        <TR>                         
                          <TD class="short">Date:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                            BuildMonthSelector(1, 'Month', GetMonthFromSessionDate($_SESSION['AllocateLeave'][0]));
                            echo '&nbsp;';
                            BuildYearSelector(2, 'Year', GetYearFromSessionDate($_SESSION['AllocateLeave'][0]));
                    echo '</TD>    
                        </TR>
                        <TR>
                          <TD colspan="4" class="center">
                            <INPUT tabindex="3" name="Submit" type="submit" class="button" value="Submit" />   
                            <INPUT tabindex="4" name="Submit" type="submit" class="button" value="Cancel" />                  
                          </TD>
                        </TR>
                      </FORM>
                    </TABLE>
                  </DIV>';
          } else
          if (isset($_SESSION['ApproveLeave']))
          {
            $row = MySQL_Fetch_Array(ExecuteQuery('SELECT `Leave`.*, IF (Leave_Doctor = "1", "Yes", "No") AS Doctor FROM `Leave` WHERE Leave_ID = "'.$_SESSION['ApproveLeave'][0].'"')); 
            $startDate = GetDatabaseDate(GetDayFromDatabaseDate($row['Leave_Start']) - 7, GetMonthFromDatabaseDate($row['Leave_Start']), GetYearFromDatabaseDate($row['Leave_Start']));
            $endDate = GetDatabaseDate(GetDayFromDatabaseDate($row['Leave_End']) + 7, GetMonthFromDatabaseDate($row['Leave_End']), GetYearFromDatabaseDate($row['Leave_End']));
            if ($row['Leave_Start'] == $row['Leave_End'])
              $dateRange = GetTextualDateFromDatabaseDate($row['Leave_Start']);
            else        
              $dateRange = GetTextualDateFromDatabaseDate($row['Leave_Start']).' to '.GetTextualDateFromDatabaseDate($row['Leave_End']);
            
            $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = "'.$row['Leave_Employee'].'"'));
            $rowTempB = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM LeaveType WHERE LeaveType_ID = "'.$row['Leave_Type'].'"'));
            $rowTempC = MySQL_Fetch_Array(ExecuteQuery('SELECT SUM(LeaveDue_Annual_Days) AS Due FROM LeaveDue WHERE LeaveDue_Employee = '.$row['Leave_Employee'].''));
            $rowTempD = MySQL_Fetch_Array(ExecuteQuery('SELECT SUM(Leave_Days) AS Taken FROM `Leave` WHERE Leave_Employee = '.$row['Leave_Employee'].' AND Leave_Type = "1" AND Leave_IsApproved <> "3" AND Leave_IsApproved <> "2"'));
            
            $loStart = Date('Y-m-d', MkTime(0, 0, 0, Date('m'), Date('d'), Date('Y')-3));
            $loEnd = Date('Y-m-d');
            $loRSSick = ExecuteQuery('SELECT * FROM `Leave` WHERE Leave_Employee = '.$row['Leave_Employee'].' AND Leave_Type = "2" AND Leave_IsApproved <> "3" AND Leave_IsApproved <> "2" AND (Leave_Start BETWEEN "'.$loStart.'" AND "'.$loEnd.'" OR Leave_End BETWEEN "'.$loStart.'" AND "'.$loEnd.'") AND Leave_ID <> "'.$_SESSION['ApproveLeave'][0].'"');
            
            $loSickDays = 0;
            while ($loRowSick = MySQL_Fetch_Array($loRSSick))
            {
              $loStartDate = $loRowSick['Leave_Start'];
              if ($loStartDate < $loStart)
              {
                $loStartDate = $loStart;
              }
              
              $loEndDate = $loRowSick['Leave_End'];
              if ($loEndDate > $loEnd)
              {
                $loEndDate = $loEnd;
              }
              
              while ($loStartDate <= $loEndDate)
              {
                $loDay = SubStr($loStartDate, 8, 2);
                $loMonth = SubStr($loStartDate, 5, 2);
                $loYear = SubStr($loStartDate, 0, 4);
                
                $loRowHolidayCheck = MySQL_Fetch_Array(ExecuteQuery('SELECT COUNT(*) FROM PublicHoliday WHERE PublicHoliday_Date = "'.$loStartDate.'"'));
                if ($loRowHolidayCheck[0] == 0)
                {
                  $loDate = MkTime(0, 0, 0, $loMonth, $loDay, $loYear);
                  if (!((Date('w', $loDate) == 0) || (Date('w', $loDate) == 6)))
                    $loSickDays++;
                }
                
                $loStartDate = Date('Y-m-d', MkTime(0, 0, 0, $loMonth, $loDay + 1, $loYear));
              };
            }
            
            $rowApprove = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = '.$row['Leave_Approved'].''));
            BuildContentHeader('Approve Leave - '.$rowTemp['Staff_First_Name'].' '.$rowTemp['Staff_Last_Name'].' for '.$dateRange, "", "", false);  
            echo '<DIV class="contentflow">
                    <P>When approving leave check and make sure that the number of days specified matches the start and end dates. This leave is set to be authorised by <B>'.$rowApprove['Staff_First_Name'].' '.$rowApprove['Staff_Last_Name'].'</B></P>
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center" class="short">
                      <TR>
                        <TD colspan="2" class="header">Leave Details
                        </TD>
                      </TR>
                      <TR>
                        <TD class="short">Leave Type:
                        </TD>
                        <TD class="bold">'.$rowTempB['LeaveType_Description'].'
                        </TD>
                      </TR>
                      <TR>                         
                        <TD>Start Date:
                        </TD>
                        <TD class="bold">'.GetTextualDateFromDatabaseDate($row['Leave_Start']).'
                        </TD>    
                      </TR>
                      <TR>                   
                        <TD>End Date:
                        </TD>
                        <TD class="bold">'.GetTextualDateFromDatabaseDate($row['Leave_End']).'
                        </TD>    
                      </TR>
                      <TR>                 
                        <TD>Days:
                        </TD>
                        <TD class="bold">'.$row['Leave_Days'].'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(Annual: '.SPrintF('%02.2f', $rowTempC['Due'] - $rowTempD['Taken']).', Sick: '.SPrintF('%02.2f', 30 - $loSickDays).')
                        </TD>  
                      </TR>
                      <TR>                 
                        <TD>Doctor\'s Certificate:
                        </TD>
                        <TD class="bold">';
                          if ($rowTempB['LeaveType_Description'] == 'Sick')
                            echo $row['Doctor']; 
                          else
                            echo 'N/A';
                  echo '</TD>
                      </TR>
                      <TR>
                        <TD class="vtop">Description:
                        </TD>
                        <TD rowspan="4" class="bold vtop">'.$row['Leave_Comments'].'
                        </TD>
                      </TR>
                    </TABLE>  
                  </DIV>
                  <DIV>
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center" class="short">
                      <FORM method="post" action="Handlers/Leave_Handler.php">
                        <INPUT name="Type" type="hidden" value="Approve" />
                        <TR>
                          <TD class="header">Approve
                          </TD>
                        </TR>
                        <TR>
                          <TD class="center">How do you wish to handle this request?
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                        </TR>
                        <TR>
                          <TD class="center">
                            <INPUT tabindex="1" name="Submit" type="submit" class="button" value="Approve" />
                            <INPUT tabindex="3" name="Submit" type="submit" class="button" value="Cancel" />
                            <INPUT tabindex="2" name="Submit" type="submit" class="button" value="Deny" />
                          </TD>
                        </TR>
                      </FORM>
                    </TABLE>  
                  </DIV>
                  <DIV>
                    <BR />                    
                    <SPAN class="note">*
                    </SPAN>
                    Leave cannot be approved more than 6 weeks in advance.
                  </DIV>
                  <BR /><BR />';
            
          Session_Unregister('Graphs'); //This must be here or somewhere before otherwise there is a HUGE problem with caching and the session variables.  
          
          BuildGanttChart(array('SELECT CONCAT(Staff_First_Name, " ", SUBSTRING(Staff_Last_Name, 1, 1)) AS Staff_First_Name FROM Staff WHERE Staff_IsEmployee > 0 ORDER BY Staff_First_Name, Staff_Last_Name ASC',
                                  'SELECT CONCAT(Staff_First_Name, " ", SUBSTRING(Staff_Last_Name, 1, 1)) AS Staff_First_Name, OvertimeBank.* FROM OvertimeBank, Staff WHERE OvertimeBank_Name = Staff_Code AND (OvertimeBank_Start BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" OR OvertimeBank_End BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" OR (OvertimeBank_Start <= "'.$endDate.' 23:59:59" AND OvertimeBank_End >= "'.$startDate.'")) AND OvertimeBank_Payment = "3" AND OvertimeBank_IsApproved = "1" ORDER BY Staff_First_Name, Staff_Last_Name ASC',
                                  'SELECT CONCAT(Staff_First_Name, " ", SUBSTRING(Staff_Last_Name, 1, 1)) AS Staff_First_Name, `Leave`.* FROM `Leave`, Staff WHERE Leave_Employee = Staff_Code AND ((Leave_Start >= "'.$startDate.'" AND Leave_Start <= "'.$endDate.'") OR (Leave_End >= "'.$startDate.'" AND Leave_End <= "'.$endDate.'") OR (Leave_Start <= "'.$endDate.' 23:59:59" AND Leave_End >= "'.$startDate.'")) AND Leave_Days > 0 AND Leave_IsApproved = "1" ORDER BY Staff_First_Name, Staff_Last_Name ASC',
                                  'SELECT CONCAT(Staff_First_Name, " ", SUBSTRING(Staff_Last_Name, 1, 1)) AS Staff_First_Name, Availability.* FROM Availability, Staff WHERE Availability_Name = Staff_Code AND (Availability_Start BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" OR Availability_End BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" OR (Availability_Start <= "'.$startDate.' 23:59:59" AND Availability_End >= "'.$endDate.'")) ORDER BY Staff_First_Name, Staff_Last_Name ASC',
                                  'SELECT CONCAT(Staff_First_Name, " ", SUBSTRING(Staff_Last_Name, 1, 1)) AS Staff_First_Name, `Leave`.* FROM `Leave`, Staff WHERE Leave_Employee = Staff_Code AND ((Leave_Start >= "'.$startDate.'" AND Leave_Start <= "'.$endDate.'") OR (Leave_End >= "'.$startDate.'" AND Leave_End <= "'.$endDate.'") OR (Leave_Start <= "'.$endDate.' 23:59:59" AND Leave_End >= "'.$startDate.'")) AND Leave_Days > 0 AND Leave_IsApproved = "0" ORDER BY Staff_First_Name, Staff_Last_Name ASC',
                                  'SELECT CONCAT(Staff_First_Name, " ", SUBSTRING(Staff_Last_Name, 1, 1)) AS Staff_First_Name, OvertimeBank.* FROM OvertimeBank, Staff WHERE OvertimeBank_Name = Staff_Code AND (OvertimeBank_Start BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" OR OvertimeBank_End BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" OR (OvertimeBank_Start <= "'.$endDate.' 23:59:59" AND OvertimeBank_End >= "'.$startDate.'")) AND OvertimeBank_Payment = "3" AND OvertimeBank_IsApproved = "0" ORDER BY Staff_First_Name, Staff_Last_Name ASC'), "", "", 'Availability for '.$dateRange, $startDate, $endDate, 'Monthly', 
                            array(array('Staff_First_Name'),
                                  array('Staff_First_Name', 'OvertimeBank_Start', 'OvertimeBank_End', 'OvertimeBank_Payment'),
                                  array('Staff_First_Name', 'Leave_Start', 'Leave_End', 'Leave_Type'),
                                  array('Staff_First_Name', 'Availability_Start', 'Availability_End', 'Availability_Type'),
                                  array('Staff_First_Name', 'Leave_Start', 'Leave_End', 'Leave_Type'),
                                  array('Staff_First_Name', 'OvertimeBank_Start', 'OvertimeBank_End', 'OvertimeBank_Payment')), 
                            array(array(),
                                  array("", "", 'Banked Overtime'),
                                  $leaves,
                                  $availabilities,
                                  array('Pending'),
                                  array()),
                            array(array(),
                                  array("", "", '#FF511C'),
                                  array('#329E1F', '#03B272', '#0BB7AC', '#0B9BC6', '#0C61C9', '#6919FF', '#A215D6'),
                                  array('#FF11FB', '#FF0F83', '#DB0A2D', '#FFAA42', '#FFE049', '#B4FF32'),
                                  array('#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF'),
                                  array("", "", '#FFFFFF')));
            echo '<DIV class="contentflow">
                    <P>This is the availability over the requested leave period.</P>
                    <BR /><BR />
                    <COMMENT>
                      <IMG src="Scripts/Graphing.php?Graph=0&Random='.Rand().'" />
                    </COMMENT>
                    <!--[if lte IE 6]>
                      <IFRAME src="Scripts/Graphing.php?Graph=0&Random='.Rand().'" width="900" height="600" frameborder="0"></IFRAME>
                    <![endif]-->
                    <!--[if gt IE 6]>
                      <IMG src="Scripts/Graphing.php?Graph=0&Random='.Rand().'" />
                    <![endif]-->
                  </DIV>
                  <BR /><BR />';
          } else
          if (isset($_SESSION['CancelLeave']))
          {            
            $row = MySQL_Fetch_Array(ExecuteQuery('SELECT Leave.*, IF (Leave_Doctor = "1", "Yes", "No") AS Doctor FROM Leave WHERE Leave_ID = "'.$_SESSION['CancelLeave'][0].'"')); 
            if ($row['Leave_Start'] == $row['Leave_End'])
              $dateRange = GetTextualDateFromDatabaseDate($row['Leave_Start']);
            else        
              $dateRange = GetTextualDateFromDatabaseDate($row['Leave_Start']).' to '.GetTextualDateFromDatabaseDate($row['Leave_End']);
            
            $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = "'.$row['Leave_Employee'].'"'));
            $rowTempB = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM LeaveType WHERE LeaveType_ID = "'.$row['Leave_Type'].'"'));
            $rowTempC = MySQL_Fetch_Array(ExecuteQuery('SELECT SUM(LeaveDue_Annual_Days) AS Due FROM LeaveDue WHERE LeaveDue_Employee = '.$row['Leave_Employee'].''));
            $rowTempD = MySQL_Fetch_Array(ExecuteQuery('SELECT SUM(Leave_Days) AS Taken FROM Leave WHERE Leave_Employee = '.$row['Leave_Employee'].' AND Leave_Type = "1" AND Leave_IsApproved <> "3" AND Leave_ID <> "'.$_SESSION['CancelLeave'][0].'"'));
            
            $start = Date('Y-m-d H:i:s', MKTime(Date('h'), Date('i'), Date('s'), Date('m'), Date('d'), Date('Y')-3));
            $end = Date('Y-m-d H:i:s');
            $rowSick = MySQL_Fetch_Array(ExecuteQuery('SELECT SUM(Leave_Days) AS Taken FROM Leave WHERE Leave_Employee = '.$row['Leave_Employee'].' AND Leave_Type = "2" AND Leave_IsApproved <> "3" AND Leave_IsApproved <> "3" AND Leave_IsApproved <> "2" AND (Leave_Start BETWEEN "'.$start.'" AND "'.$end.'" OR Leave_End BETWEEN "'.$start.'" AND "'.$end.'")'));
            
            BuildContentHeader('Cancel Leave - '.$rowTemp['Staff_First_Name'].' '.$rowTemp['Staff_Last_Name'].' for '.$dateRange, "", "", false);  
            echo '<DIV class="contentflow">
                    <P>When cancelling leave always make sure that the right leave is being cancelled.</P>
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center" class="short">
                      <TR>
                        <TD colspan="2" class="header">Leave Details
                        </TD>
                      </TR>
                      <TR>
                        <TD class="short">Leave Type:
                        </TD>
                        <TD class="bold">'.$rowTempB['LeaveType_Description'].'
                        </TD>
                      </TR>
                      <TR>                         
                        <TD>Start Date:
                        </TD>
                        <TD class="bold">'.GetTextualDateFromDatabaseDate($row['Leave_Start']).'
                        </TD>    
                      </TR>
                      <TR>                   
                        <TD>End Date:
                        </TD>
                        <TD class="bold">'.GetTextualDateFromDatabaseDate($row['Leave_End']).'
                        </TD>    
                      </TR>
                      <TR>                 
                        <TD>Days:
                        </TD>
                        <TD class="bold">'.$row['Leave_Days'].'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(Annual: '.SPrintF('%02.2f', $rowTempC['Due'] - $rowTempD['Taken']).', Sick: '.SPrintF('%02.2f', 30 - $rowSick['Taken']).')
                        </TD>  
                      </TR>
                      <TR>                 
                        <TD>Doctor\'s Certificate:
                        </TD>
                        <TD class="bold">';
                          if ($rowTempB['LeaveType_Description'] == 'Sick')
                            echo $row['Doctor']; 
                          else
                            echo 'N/A';
                  echo '</TD>
                      </TR>
                      <TR>
                        <TD class="vtop">Description:
                        </TD>
                        <TD rowspan="4" class="bold vtop">'.$row['Leave_Comments'].'
                        </TD>
                      </TR>
                    </TABLE>  
                  </DIV>
                  <DIV>
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center" class="short">
                      <FORM method="post" action="Handlers/Leave_Handler.php">
                        <INPUT name="Type" type="hidden" value="Cancel" />
                        <TR>
                          <TD class="header">Cancel
                          </TD>
                        </TR>
                        <TR>
                          <TD class="center">Are you sure you wish to cancel this leave?
                          </TD>
                        </TR>
                        <TR>
                          <TD class="center">
                            <INPUT tabindex="1" name="Submit" type="submit" class="button" value="Yes" />
                            <INPUT tabindex="2" name="Submit" type="submit" class="button" value="No" />
                          </TD>
                        </TR>
                      </FORM>
                    </TABLE>  
                  </DIV>
                  <BR /><BR />';
          } else
          if (isset($_SESSION['EditLeave'])) 
          {
            BuildContentHeader('Edit Leave for '.$_SESSION['EditLeave'][100], "", "", false);
            $row = MySQL_Fetch_Array(ExecuteQuery('SELECT SUM(LeaveDue_Annual_Days) AS Due FROM LeaveDue WHERE LeaveDue_Employee = '.$_SESSION['EditLeave'][1].''));
            $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT SUM(Leave_Days) AS Taken FROM Leave WHERE Leave_Employee = '.$_SESSION['EditLeave'][1].' AND Leave_Type = "1" AND Leave_IsApproved <> "3"'));
            
            $loStart = Date('Y-m-d', MkTime(0, 0, 0, Date('m'), Date('d'), Date('Y')-3));
            $loEnd = Date('Y-m-d');
            $loRSSick = ExecuteQuery('SELECT * FROM Leave WHERE Leave_Employee = '.$_SESSION['EditLeave'][1].' AND Leave_ID <> "'.$_SESSION['EditLeave'][0].'" AND Leave_Type = "2" AND Leave_IsApproved <> "3" AND Leave_IsApproved <> "2" AND (Leave_Start BETWEEN "'.$loStart.'" AND "'.$loEnd.'" OR Leave_End BETWEEN "'.$loStart.'" AND "'.$loEnd.'")');
            
            $loSickDays = 0;
            while ($loRowSick = MySQL_Fetch_Array($loRSSick))
            {
              $loStartDate = $loRowSick['Leave_Start'];
              if ($loStartDate < $loStart)
              {
                $loStartDate = $loStart;
              }
              
              $loEndDate = $loRowSick['Leave_End'];
              if ($loEndDate > $loEnd)
              {
                $loEndDate = $loEnd;
              }
              
              while ($loStartDate <= $loEndDate)
              {
                $loDay = SubStr($loStartDate, 8, 2);
                $loMonth = SubStr($loStartDate, 5, 2);
                $loYear = SubStr($loStartDate, 0, 4);
                
                $loRowHolidayCheck = MySQL_Fetch_Array(ExecuteQuery('SELECT COUNT(*) FROM PublicHoliday WHERE PublicHoliday_Date = "'.$loStartDate.'"'));
                if ($loRowHolidayCheck[0] == 0)
                {
                  $loDate = MkTime(0, 0, 0, $loMonth, $loDay, $loYear);
                  if (!((Date('w', $loDate) == 0) || (Date('w', $loDate) == 6)))
                    $loSickDays++;
                }
                
                $loStartDate = Date('Y-m-d', MkTime(0, 0, 0, $loMonth, $loDay + 1, $loYear));
              };
            }
            
            echo '<DIV class="contentflow">
                    <P>Once you have made changes, ensure that you submit them otherwise they will not take effect.</P>
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center" class="long">
                      <FORM method="post" action="Handlers/Leave_Handler.php">
                        <INPUT name="Type" type="hidden" value="Edit" />
                        <TR>
                          <TD colspan="4" class="header">Leave Details
                          </TD>
                        </TR>
                        <TR>
                          <TD class="short">Authorise By:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                            BuildStaffManSelector(1, 'Authorise', 'standard', $_SESSION['EditLeave'][2]);
                    echo '</TD> 
                          <TD class="short">Description:
                          </TD>
                          <TD rowspan="4">
                            <TEXTAREA tabindex="10" name="Description" class="standard" maxlength="240">'.$_SESSION['EditLeave'][3].'</TEXTAREA>
                          </TD>
                        </TR>
                        <TR>
                          <TD>Leave Type:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                            BuildLeaveSelector(2, 'Leave', 'standard', $_SESSION['EditLeave'][4]);
                    echo '</TD> 
                        </TR>
                        <TR>                         
                          <TD>Start Date:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                            BuildDaySelector(3, 'StartDay', GetDayFromSessionDate($_SESSION['EditLeave'][5]));
                            echo '&nbsp;';
                            BuildMonthSelector(4, 'StartMonth', GetMonthFromSessionDate($_SESSION['EditLeave'][5]));
                            echo '&nbsp;';
                            BuildYearSelector(5, 'StartYear', GetYearFromSessionDate($_SESSION['EditLeave'][5]));
                    echo '</TD>    
                        </TR>
                        <TR>                   
                          <TD>End Date:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                            BuildDaySelector(6, 'EndDay', GetDayFromSessionDate($_SESSION['EditLeave'][6]));
                            echo '&nbsp;';
                            BuildMonthSelector(7, 'EndMonth', GetMonthFromSessionDate($_SESSION['EditLeave'][6]));
                            echo '&nbsp;';
                            BuildYearSelector(8, 'EndYear', GetYearFromSessionDate($_SESSION['EditLeave'][6]));
                    echo '</TD>    
                        </TR>
                        <TR>                 
                          <TD>Days:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD class="bold">
                            <INPUT tabindex="9" name="Days" type="text" class="text veryshort" value="'.$_SESSION['EditLeave'][7].'" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(Annual: '.SPrintF('%02.2f', $row['Due'] - $rowTemp['Taken']).', Sick: '.SPrintF('%02.2f', 30 - $loSickDays).')
                          </TD>             
                          <TD>Doctor\'s Certificate:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                            BuildYesNoSelector(11, 'Doctor', 'veryshort', $_SESSION['EditLeave'][8]);
                    echo '</TD>
                        </TR>
                        <TR>
                          <TD colspan="4" class="center">
                            <INPUT tabindex="12" name="Submit" type="submit" class="button" value="Submit" />   
                            <INPUT tabindex="13" name="Submit" type="submit" class="button" value="Cancel" />                  
                          </TD>
                        </TR>
                      </FORM>
                    </TABLE>  
                  </DIV>
                  <DIV>
                    <BR />                    
                    <SPAN class="note">*
                    </SPAN>
                    These fields are required.
                  </DIV>';
          } else
          if (isset($_SESSION['RequestLeave']))
          {
            $row = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = '.$_SESSION['RequestLeave'][0].''));
            if (($_SESSION['cAuth'] & 64) && ($_SESSION['RequestLeave'][0] != $_SESSION['cUID']))
              BuildContentHeader('Request Leave - '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'], "", "", false);
            else
              BuildContentHeader('Request Leave', "", "", false);
            $row = MySQL_Fetch_Array(ExecuteQuery('SELECT SUM(LeaveDue_Annual_Days) AS Due FROM LeaveDue WHERE LeaveDue_Employee = '.$_SESSION['RequestLeave'][0].''));
            $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT SUM(Leave_Days) AS Taken FROM `Leave` WHERE Leave_Employee = '.$_SESSION['RequestLeave'][0].' AND Leave_Type = "1" AND Leave_IsApproved = "1"'));
            
            $loStart = Date('Y-m-d', MkTime(0, 0, 0, Date('m'), Date('d'), Date('Y')-3));
            $loEnd = Date('Y-m-d');
            $loRSSick = ExecuteQuery('SELECT * FROM `Leave` WHERE Leave_Employee = '.$_SESSION['RequestLeave'][0].' AND Leave_Type = "2" AND Leave_IsApproved <> "3" AND Leave_IsApproved <> "2" AND (Leave_Start BETWEEN "'.$loStart.'" AND "'.$loEnd.'" OR Leave_End BETWEEN "'.$loStart.'" AND "'.$loEnd.'")');
            
            $loSickDays = 0;
            while ($loRowSick = MySQL_Fetch_Array($loRSSick))
            {
              $loStartDate = $loRowSick['Leave_Start'];
              if ($loStartDate < $loStart)
              {
                $loStartDate = $loStart;
              }
              
              $loEndDate = $loRowSick['Leave_End'];
              if ($loEndDate > $loEnd)
              {
                $loEndDate = $loEnd;
              }
              
              while ($loStartDate <= $loEndDate)
              {
                $loDay = SubStr($loStartDate, 8, 2);
                $loMonth = SubStr($loStartDate, 5, 2);
                $loYear = SubStr($loStartDate, 0, 4);
                
                $loRowHolidayCheck = MySQL_Fetch_Array(ExecuteQuery('SELECT COUNT(*) FROM PublicHoliday WHERE PublicHoliday_Date = "'.$loStartDate.'"'));
                if ($loRowHolidayCheck[0] == 0)
                {
                  $loDate = MkTime(0, 0, 0, $loMonth, $loDay, $loYear);
                  if (!((Date('w', $loDate) == 0) || (Date('w', $loDate) == 6)))
                    $loSickDays++;
                }
                
                $loStartDate = Date('Y-m-d', MkTime(0, 0, 0, $loMonth, $loDay + 1, $loYear));
              };
            }
            
            echo '<DIV class="contentflow">
                    <P>Enter the details of the leave below. Ensure that all the required information is entered before submission and that this information is valid. When you submit this request the person authorising the leave will be notified. Once the leave has been approved you will be notified by mail.</P>
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center" class="long">
                      <FORM method="post" action="Handlers/Leave_Handler.php">
                        <INPUT name="Type" type="hidden" value="Request" />
                        <TR>
                          <TD colspan="4" class="header">Leave Details
                          </TD>
                        </TR>
                        <TR>
                          <TD class="short">Authorise By:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                            BuildStaffManSelector(1, 'Authorise', 'standard', $_SESSION['RequestLeave'][1]);
                    echo '</TD> 
                          <TD class="short">Description:
                          </TD>
                          <TD rowspan="4">
                            <TEXTAREA tabindex="10" name="Description" class="standard" maxlength="240">'.$_SESSION['RequestLeave'][2].'</TEXTAREA>
                          </TD>
                        </TR>
                        <TR>
                          <TD>Leave Type:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                            BuildLeaveSelector(2, 'Leave', 'standard', $_SESSION['RequestLeave'][3]);
                    echo '</TD> 
                        </TR>
                        <TR>                         
                          <TD>Start Date:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                            BuildDaySelector(3, 'StartDay', GetDayFromSessionDate($_SESSION['RequestLeave'][4]));
                            echo '&nbsp;';
                            BuildMonthSelector(4, 'StartMonth', GetMonthFromSessionDate($_SESSION['RequestLeave'][4]));
                            echo '&nbsp;';
                            BuildYearSelector(5, 'StartYear', GetYearFromSessionDate($_SESSION['RequestLeave'][4]));
                    echo '</TD>    
                        </TR>
                        <TR>                   
                          <TD>End Date:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                            BuildDaySelector(6, 'EndDay', GetDayFromSessionDate($_SESSION['RequestLeave'][5]));
                            echo '&nbsp;';
                            BuildMonthSelector(7, 'EndMonth', GetMonthFromSessionDate($_SESSION['RequestLeave'][5]));
                            echo '&nbsp;';
                            BuildYearSelector(8, 'EndYear', GetYearFromSessionDate($_SESSION['RequestLeave'][5]));
                    echo '</TD>    
                        </TR>
                        <TR>                 
                          <TD>Days:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD class="bold">
                            <INPUT tabindex="9" name="Days" type="text" class="text veryshort" value="'.$_SESSION['RequestLeave'][6].'" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(Annual: '.SPrintF('%02.2f', $row['Due'] - $rowTemp['Taken']).', Sick: '.SPrintF('%02.2f', 30 - $loSickDays).')
                          </TD>             
                          <TD>Doctor\'s Certificate:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                            BuildYesNoSelector(11, 'Doctor', 'veryshort', $_SESSION['RequestLeave'][7]);
                    echo '</TD>
                        </TR>
                        <TR>
                          <TD colspan="4" class="center">
                            <INPUT tabindex="12" name="Submit" type="submit" class="button" value="Submit" />   
                            <INPUT tabindex="13" name="Submit" type="submit" class="button" value="Cancel" />                  
                          </TD>
                        </TR>
                      </FORM>
                    </TABLE>  
                  </DIV>
                  <DIV>
                    <BR />                    
                    <SPAN class="note">*
                    </SPAN>
                    These fields are required.
                  </DIV>';
          } else
          if (isset($_SESSION['ViewLeave']))
          {
            Session_Unregister('Graphs'); //This must be here or somewhere before otherwise there is a HUGE problem with caching and the session variables.  
            
            $startDate = GetDatabaseDateFromSessionDate($_SESSION['ViewLeave'][0]);
            $endDate = GetDatabaseDateFromSessionDate($_SESSION['ViewLeave'][1]);
            if ($_SESSION['ViewLeave'][0] == $_SESSION['ViewLeave'][1])
              $dateRange = GetTextualDateFromSessionDate($_SESSION['ViewLeave'][0]);
            else        
              $dateRange = GetTextualDateFromSessionDate($_SESSION['ViewLeave'][0]).' to '.GetTextualDateFromSessionDate($_SESSION['ViewLeave'][1]);
            
            $row = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = "'.$_SESSION['ViewLeave'][3].'"'));                    
            switch ($_SESSION['ViewLeave'][2])
            {
              case '0':
                $display = true;
                switch ($_SESSION['ViewLeave'][3])
                {
                  case "":
                    BuildContentHeader('Leave Graphic Report - '.$dateRange, "", "", false);
                    if (MySQL_Num_Rows(ExecuteQuery('SELECT CONCAT(Staff_First_Name, " ", SUBSTRING(Staff_Last_Name, 1, 1)) AS Staff_First_Name, `Leave`.* FROM `Leave`, Staff WHERE Leave_Employee = Staff_Code AND ((Leave_Start >= "'.$startDate.'" AND Leave_Start <= "'.$endDate.'") OR (Leave_End >= "'.$startDate.'" AND Leave_End <= "'.$endDate.'")) AND Leave_Days > 0 AND Leave_IsApproved = "1" LIMIT 1')) > 0)
                      BuildGanttChart(array('SELECT CONCAT(Staff_First_Name, " ", SUBSTRING(Staff_Last_Name, 1, 1)) AS Staff_First_Name, `Leave`.* FROM `Leave`, Staff WHERE Leave_Employee = Staff_Code AND ((Leave_Start >= "'.$startDate.'" AND Leave_Start <= "'.$endDate.'") OR (Leave_End >= "'.$startDate.'" AND Leave_End <= "'.$endDate.'")) AND Leave_Days > 0 AND Leave_IsApproved = "1" ORDER BY Staff_First_Name, Staff_Last_Name ASC'), "", "", 'Leave Allocation for '.$dateRange, $startDate, $endDate, 'Monthly', array(array('Staff_First_Name', 'Leave_Start', 'Leave_End', 'Leave_Type')), array($leaves), array(array('#329E1F', '#03B272', '#0BB7AC', '#0B9BC6', '#0C61C9', '#6919FF', '#A215D6')));
                    else
                      $display = false;
                    break;
                  default:   
                    BuildContentHeader('Leave Graphic Report - '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].' for '.$dateRange, "", "", false);
                    if (MySQL_Num_Rows(ExecuteQuery('SELECT CONCAT(Staff_First_Name, " ", SUBSTRING(Staff_Last_Name, 1, 1)) AS Staff_First_Name, Leave.* FROM Leave, Staff WHERE Leave_Employee = Staff_Code AND Leave_Employee = "'.$_SESSION['ViewLeave'][3].'" AND ((Leave_Start >= "'.$startDate.'" AND Leave_Start <= "'.$endDate.'") OR (Leave_End >= "'.$startDate.'" AND Leave_End <= "'.$endDate.'")) AND Leave_Days > 0 AND Leave_IsApproved = "1" LIMIT 1')))         
                      BuildGanttChart(array('SELECT CONCAT(Staff_First_Name, " ", SUBSTRING(Staff_Last_Name, 1, 1)) AS Staff_First_Name, Leave.* FROM Leave, Staff WHERE Leave_Employee = Staff_Code AND Leave_Employee = "'.$_SESSION['ViewLeave'][3].'" AND ((Leave_Start >= "'.$startDate.'" AND Leave_Start <= "'.$endDate.'") OR (Leave_End >= "'.$startDate.'" AND Leave_End <= "'.$endDate.'")) AND Leave_Days > 0 AND Leave_IsApproved = "1" ORDER BY Staff_First_Name, Staff_Last_Name ASC'), "", "", 'Leave Allocation of '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].' for '.$dateRange, $startDate, $endDate, 'Monthly', array(array('Staff_First_Name', 'Leave_Start', 'Leave_End', 'Leave_Type')), array($leaves), array(array('#329E1F', '#03B272', '#0BB7AC', '#0B9BC6', '#0C61C9', '#6919FF', '#A215D6')));                            
                    else
                      $display = false;
                    break;
                }
                if ($display)
                  echo '<DIV class="contentflow">
                          <P>This is the leave allocation.</P>
                          <BR /><BR />
                          <COMMENT>
                            <IMG src="Scripts/Graphing.php?Graph=0&Random='.Rand().'" />
                          </COMMENT>
                          <!--[if IE]>
                            <IFRAME src="Scripts/Graphing.php?Graph=0&Random='.Rand().'" width="900" height="600" frameborder="0"></IFRAME>
                          <![endif]-->
                        </DIV>
                        <BR /><BR />';
                else
                  echo '<DIV class="contentflow">
                          <P>No leave has been allocated for the given date range.</P>
                        </DIV>';
                break;
              case '1': 
                switch ($_SESSION['ViewLeave'][3])
                {
                  case "":
                    BuildContentHeader('Leave Summary - '.$dateRange, "", "", false);      
                    $resultSetStaff = ExecuteQuery('SELECT * FROM Staff WHERE Staff_IsEmployee > "0" ORDER BY Staff_First_Name, Staff_Last_Name');
                    /*if (MySQL_Num_Rows($resultSetStaff) > 0) 
                    {
                      echo '<DIV class="contentflow">
                            <P>These are the leave details.</P>';
                      while ($rowStaff = MySQL_Fetch_Array($resultSetStaff))
                      {
                        echo '<BR /><BR />
                              <TABLE cellspacing="5" align="center" class="long">
                                <TR>
                                  <TD colspan="6" class="header">'.$rowStaff['Staff_First_Name'].' '.$rowStaff['Staff_Last_Name'].'
                                  </TD>
                                </TR>';
                        $resultSet = ExecuteQuery('SELECT Leave.*, IF (Leave_Doctor = "0", "No", "Yes") AS Doctor, LeaveType_Description FROM Leave, LeaveType WHERE LeaveType_ID = Leave_Type AND Leave_Employee = '.$rowStaff['Staff_Code'].' AND Leave_IsApproved = "1" AND (Leave_Start BETWEEN "'.$_SESSION['ViewLeave'][0].'" AND "'.$_SESSION['ViewLeave'][1].'" OR Leave_End BETWEEN "'.$_SESSION['ViewLeave'][0].'" AND "'.$_SESSION['ViewLeave'][1].'") ORDER BY Leave_Start'); 
                        if (MySQL_Num_Rows($resultSet) > 0)
                        {
                          echo '<TR>
                                  <TD colspan="6" class="subheaderclear">Leave Approved / Taken Details
                                  </TD>
                                </TR>
                                <TR>
                                  <TD class="subheader short">Period
                                  </TD>
                                  <TD class="subheader">Days
                                  </TD>
                                  <TD class="subheader">Type
                                  </TD>
                                  <TD class="subheader">Approved By
                                  </TD>
                                  <TD class="subheader">Dr\'s Certificate
                                  </TD>
                                  <TD class="subheader">Comments
                                  </TD>
                                </TR>';
                                $total = 0;
                                while ($row = MySQL_Fetch_Array($resultSet))
                                {
                                  $total += $row['Leave_Days'];
                                
                                  $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = "'.$row['Leave_Approved'].'"'));                              
                                  echo '<TR>
                                          <TD class="rowA">'.(GetTextualDateFromDatabaseDate($row['Leave_Start']) == GetTextualDateFromDatabaseDate($row['Leave_End']) ? GetTextualDateFromDatabaseDate($row['Leave_Start']) : GetTextualDateFromDatabaseDate($row['Leave_Start']).' to '.GetTextualDateFromDatabaseDate($row['Leave_End'])).'
                                          </TD>
                                          <TD class="rowA center">'.SPrintF('%02.2f', $row['Leave_Days']).'
                                          </TD>
                                          <TD class="rowA center">';
                                            if ($row['LeaveType_Description'] == 'Family')
                                              echo 'Compassionate'; 
                                            else
                                              echo $row['LeaveType_Description'];
                                    echo '</TD>
                                          <TD class="rowA center">'.$rowTemp['Staff_First_Name'].' '.$rowTemp['Staff_Last_Name'].'
                                          </TD>
                                          <TD class="rowA center">';
                                            if ($row['LeaveType_Description'] == 'Sick')
                                              echo $row['Doctor']; 
                                            else
                                              echo 'N/A';
                                    echo '</TD>
                                          <TD class="rowA">'.$row['Leave_Comments'].'
                                          </TD>
                                        </TR>';                         
                                  $count++;
                                }  
                          echo '<TR>
                                  <TD class="rowB bold">Total
                                  </TD>
                                  <TD class="rowB center bold">'.SPrintF('%02.2f', $total).'
                                  </TD>
                                  <TD class="rowB">
                                  </TD>
                                  <TD class="rowB">
                                  </TD>
                                  <TD class="rowB">
                                  </TD>
                                  <TD class="rowB">
                                  </TD>
                                </TR>';  
                        }
                        
                        $resultSet = ExecuteQuery('SELECT Leave.*, IF (Leave_Doctor = "0", "No", "Yes") AS Doctor, LeaveType_Description FROM Leave, LeaveType WHERE LeaveType_ID = Leave_Type AND Leave_Employee = '.$rowStaff['Staff_Code'].' AND Leave_IsApproved = "0" AND (Leave_Start BETWEEN "'.$_SESSION['ViewLeave'][0].'" AND "'.$_SESSION['ViewLeave'][1].'" OR Leave_End BETWEEN "'.$_SESSION['ViewLeave'][0].'" AND "'.$_SESSION['ViewLeave'][1].'") ORDER BY Leave_Start'); 
                        if (MySQL_Num_Rows($resultSet) > 0)
                        {
                          echo '<TR>
                                  <TD colspan="6" class="subheaderclear">Leave Pending Details
                                  </TD>
                                </TR>
                                <TR>
                                  <TD class="subheader short">Period
                                  </TD>
                                  <TD class="subheader">Days
                                  </TD>
                                  <TD class="subheader">Type
                                  </TD>
                                  <TD class="subheader">To Approve
                                  </TD>
                                  <TD class="subheader">Dr\'s Certificate
                                  </TD>
                                  <TD class="subheader">Comments
                                  </TD>
                                </TR>';
                                $total = 0;
                                while ($row = MySQL_Fetch_Array($resultSet))
                                {
                                  $total += $row['Leave_Days'];
                                
                                  $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = "'.$row['Leave_Approved'].'"'));                              
                                  echo '<TR>
                                          <TD class="rowA">'.(GetTextualDateFromDatabaseDate($row['Leave_Start']) == GetTextualDateFromDatabaseDate($row['Leave_End']) ? GetTextualDateFromDatabaseDate($row['Leave_Start']) : GetTextualDateFromDatabaseDate($row['Leave_Start']).' to '.GetTextualDateFromDatabaseDate($row['Leave_End'])).'
                                          </TD>
                                          <TD class="rowA center">'.SPrintF('%02.2f', $row['Leave_Days']).'
                                          </TD>
                                          <TD class="rowA center">';
                                            if ($row['LeaveType_Description'] == 'Family')
                                              echo 'Compassionate'; 
                                            else
                                              echo $row['LeaveType_Description'];
                                    echo '</TD>
                                          <TD class="rowA">'.$rowTemp['Staff_First_Name'].' '.$rowTemp['Staff_Last_Name'].'
                                          </TD>
                                          <TD class="rowA center">';
                                            if ($row['LeaveType_Description'] == 'Sick')
                                              echo $row['Doctor']; 
                                            else
                                              echo 'N/A';
                                    echo '</TD>
                                          <TD class="rowA">'.$row['Leave_Comments'].'
                                          </TD>
                                        </TR>';                         
                                  $count++;
                                }  
                          echo '<TR>
                                  <TD class="rowB bold">Total
                                  </TD>
                                  <TD class="rowB center bold">'.SPrintF('%02.2f', $total).'
                                  </TD>
                                  <TD class="rowB">
                                  </TD>
                                  <TD class="rowB">
                                  </TD>
                                  <TD class="rowB">
                                  </TD>
                                  <TD class="rowB">
                                  </TD>
                                </TR>';  
                        }
                        
                        $resultSet = ExecuteQuery('SELECT Leave.*, IF (Leave_Doctor = "0", "No", "Yes") AS Doctor, LeaveType_Description FROM Leave, LeaveType WHERE LeaveType_ID = Leave_Type AND Leave_Employee = '.$rowStaff['Staff_Code'].' AND Leave_IsApproved = "2" AND (Leave_Start BETWEEN "'.$_SESSION['ViewLeave'][0].'" AND "'.$_SESSION['ViewLeave'][1].'" OR Leave_End BETWEEN "'.$_SESSION['ViewLeave'][0].'" AND "'.$_SESSION['ViewLeave'][1].'") ORDER BY Leave_Start'); 
                        if (MySQL_Num_Rows($resultSet) > 0)
                        { 
                          echo '<TR>
                                  <TD colspan="6" class="subheaderclear">Leave Denied Details
                                  </TD>
                                </TR>
                                <TR>
                                  <TD class="subheader short">Period
                                  </TD>
                                  <TD class="subheader">Days
                                  </TD>
                                  <TD class="subheader">Type
                                  </TD>
                                  <TD class="subheader">Approved By
                                  </TD>
                                  <TD class="subheader">Dr\'s Certificate
                                  </TD>
                                  <TD class="subheader">Comments
                                  </TD>
                                </TR>';
                                $total = 0;
                                while ($row = MySQL_Fetch_Array($resultSet))
                                {
                                  $total += $row['Leave_Days'];
                                
                                  $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = "'.$row['Leave_Approved'].'"'));                              
                                  echo '<TR>
                                          <TD class="rowA">'.(GetTextualDateFromDatabaseDate($row['Leave_Start']) == GetTextualDateFromDatabaseDate($row['Leave_End']) ? GetTextualDateFromDatabaseDate($row['Leave_Start']) : GetTextualDateFromDatabaseDate($row['Leave_Start']).' to '.GetTextualDateFromDatabaseDate($row['Leave_End'])).'
                                          </TD>
                                          <TD class="rowA center">'.SPrintF('%02.2f', $row['Leave_Days']).'
                                          </TD>
                                          <TD class="rowA center">';
                                            if ($row['LeaveType_Description'] == 'Family')
                                              echo 'Compassionate'; 
                                            else
                                              echo $row['LeaveType_Description'];
                                    echo '</TD>
                                          <TD class="rowA center">'.$rowTemp['Staff_First_Name'].' '.$rowTemp['Staff_Last_Name'].'
                                          </TD>
                                          <TD class="rowA center">';
                                            if ($row['LeaveType_Description'] == 'Sick')
                                              echo $row['Doctor']; 
                                            else
                                              echo 'N/A';
                                    echo '</TD>
                                          <TD class="rowA">'.$row['Leave_Comments'].'
                                          </TD>
                                        </TR>';                         
                                  $count++;
                                }  
                          echo '<TR>
                                  <TD class="rowB bold">Total
                                  </TD>
                                  <TD class="rowB center bold">'.SPrintF('%02.2f', $total).'
                                  </TD>
                                  <TD class="rowB">
                                  </TD>
                                  <TD class="rowB">
                                  </TD>
                                  <TD class="rowB">
                                  </TD>
                                  <TD class="rowB">
                                  </TD>
                                </TR>';  
                        }
                        
                        $resultSet = ExecuteQuery('SELECT Leave.*, IF (Leave_Doctor = "0", "No", "Yes") AS Doctor, LeaveType_Description FROM Leave, LeaveType WHERE LeaveType_ID = Leave_Type AND Leave_Employee = '.$rowStaff['Staff_Code'].' AND Leave_IsApproved = "3" AND (Leave_Start BETWEEN "'.$_SESSION['ViewLeave'][0].'" AND "'.$_SESSION['ViewLeave'][1].'" OR Leave_End BETWEEN "'.$_SESSION['ViewLeave'][0].'" AND "'.$_SESSION['ViewLeave'][1].'") ORDER BY Leave_Start'); 
                        if (MySQL_Num_Rows($resultSet) > 0)
                        {
                          echo '<TR>
                                  <TD colspan="6" class="subheaderclear">Leave Cancelled Details
                                  </TD>
                                </TR>
                                <TR>
                                  <TD class="subheader short">Period
                                  </TD>
                                  <TD class="subheader">Days
                                  </TD>
                                  <TD class="subheader">Type
                                  </TD>
                                  <TD class="subheader">Approved By
                                  </TD>
                                  <TD class="subheader">Dr\'s Certificate
                                  </TD>
                                  <TD class="subheader">Comments
                                  </TD>
                                </TR>';
                                $total = 0;
                                while ($row = MySQL_Fetch_Array($resultSet))
                                {
                                  $total += $row['Leave_Days'];
                                
                                  $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = "'.$row['Leave_Approved'].'"'));                              
                                  echo '<TR>
                                          <TD class="rowA">'.(GetTextualDateFromDatabaseDate($row['Leave_Start']) == GetTextualDateFromDatabaseDate($row['Leave_End']) ? GetTextualDateFromDatabaseDate($row['Leave_Start']) : GetTextualDateFromDatabaseDate($row['Leave_Start']).' to '.GetTextualDateFromDatabaseDate($row['Leave_End'])).'
                                          </TD>
                                          <TD class="rowA center">'.SPrintF('%02.2f', $row['Leave_Days']).'
                                          </TD>
                                          <TD class="rowA center">';
                                            if ($row['LeaveType_Description'] == 'Family')
                                              echo 'Compassionate'; 
                                            else
                                              echo $row['LeaveType_Description'];
                                    echo '</TD>
                                          <TD class="rowA center">'.$rowTemp['Staff_First_Name'].' '.$rowTemp['Staff_Last_Name'].'
                                          </TD>
                                          <TD class="rowA center">';
                                            if ($row['LeaveType_Description'] == 'Sick')
                                              echo $row['Doctor']; 
                                            else
                                              echo 'N/A';
                                    echo '</TD>
                                          <TD class="rowA">'.$row['Leave_Comments'].'
                                          </TD>
                                        </TR>';                         
                                  $count++;
                                }  
                          echo '<TR>
                                  <TD class="rowB bold">Total
                                  </TD>
                                  <TD class="rowB center bold">'.SPrintF('%02.2f', $total).'
                                  </TD>
                                  <TD class="rowB">
                                  </TD>
                                  <TD class="rowB">
                                  </TD>
                                  <TD class="rowB">
                                  </TD>
                                  <TD class="rowB">
                                  </TD>
                                </TR>';  
                        }
                        
                        $totalAnnualDue = 0;
                        $totalCompassionateDue = 0;
                        $totalMaternityDue = 0;
                        $totalSickDue = 0;
                        $totalStudyDue = 0;
                        $resultSet = ExecuteQuery('SELECT * FROM LeaveDue WHERE LeaveDue_Employee = '.$rowStaff['Staff_Code'].' AND LeaveDue_Date BETWEEN "'.$_SESSION['ViewLeave'][0].'" AND "'.$_SESSION['ViewLeave'][1].'" ORDER BY LeaveDue_Date ASC'); 
                        if (MySQL_Num_Rows($resultSet) > 0)
                        {   
                          echo '<TR>
                                  <TD colspan="6" class="subheaderclear">Leave Due Details
                                  </TD>
                                </TR>
                                <TR>
                                  <TD class="subheader">Date Added
                                  </TD>
                                  <TD class="subheader veryshort">Annual
                                  </TD>
                                  <TD class="subheader veryshort">Compassionate
                                  </TD>
                                  <TD class="subheader veryshort">Maternity/Paternity
                                  </TD>
                                  <TD class="subheader veryshort">Sick
                                  </TD>
                                  <TD class="subheader veryshort">Study
                                  </TD>
                                </TR>';
                                while ($row = MySQL_Fetch_Array($resultSet))
                                {
                                  $totalAnnualDue += $row['LeaveDue_Annual_Days'];
                                  $totalCompassionateDue += $row['LeaveDue_Compasionate_Days'];
                                  $totalMaternityDue += $row['LeaveDue_Maternity_Days'];
                                  $totalSickDue += $row['LeaveDue_Sick_Days'];
                                  $totalStudyDue += $row['LeaveDue_Study_Days'];
                                  
                                  echo '<TR>
                                          <TD class="rowA center">'.GetTextualDateFromDatabaseDate($row['LeaveDue_Date']).'
                                          </TD>
                                          <TD class="rowA center">'.SPrintF('%02.2f', $row['LeaveDue_Annual_Days']).'
                                          </TD>
                                          <TD class="rowA center">'.SPrintF('%02.2f', $row['LeaveDue_Compasionate_Days']).'
                                          </TD>
                                          <TD class="rowA center">'.SPrintF('%02.2f', $row['LeaveDue_Maternity_Days']).'
                                          </TD>
                                          <TD class="rowA center">'.SPrintF('%02.2f', $row['LeaveDue_Sick_Days']).'
                                          </TD>
                                          <TD class="rowA center">'.SPrintF('%02.2f', $row['LeaveDue_Study_Days']).'
                                          </TD>
                                        </TR>';
                                }  
                          echo '<TR>
                                  <TD class="rowB bold">Total
                                  </TD>
                                  <TD class="rowB center bold">'.SPrintF('%02.2f', $totalAnnualDue).'
                                  </TD>
                                  <TD class="rowB center bold">'.SPrintF('%02.2f', $totalCompassionateDue).'
                                  </TD>
                                  <TD class="rowB center bold">'.SPrintF('%02.2f', $totalMaternityDue).'
                                  </TD>
                                  <TD class="rowB center bold">'.SPrintF('%02.2f', $totalSickDue).'
                                  </TD>
                                  <TD class="rowB center bold">'.SPrintF('%02.2f', $totalStudyDue).'
                                  </TD>
                                </TR>';  
                        }
                        
                        if ($_SESSION['cAuth'] & 64)
                        {
                          echo '<TR>
                                  <TD colspan="6" class="subheaderclear">Summary Details
                                  </TD>
                                </TR>
                                <TR>
                                  <TD class="subheader short">&nbsp;
                                  </TD>
                                  <TD class="subheader veryshort">Annual
                                  </TD>
                                  <TD class="subheader veryshort">Compassionate
                                  </TD>
                                  <TD class="subheader veryshort">Maternity/Paternity
                                  </TD>
                                  <TD class="subheader veryshort">Sick
                                  </TD>
                                  <TD class="subheader veryshort">Study
                                  </TD>
                                </TR>';
                                $resultSet = ExecuteQuery('SELECT SUM(Leave_Days) AS Annual FROM Leave, LeaveType WHERE Leave_Type = LeaveType_ID AND Leave_Employee = '.$rowStaff['Staff_Code'].' AND LeaveType_Description = "Annual" AND Leave_IsApproved = "1" AND (Leave_Start BETWEEN "'.$_SESSION['ViewLeave'][0].'" AND "'.$_SESSION['ViewLeave'][1].'" OR Leave_End BETWEEN "'.$_SESSION['ViewLeave'][0].'" AND "'.$_SESSION['ViewLeave'][1].'")');
                                $row = MySQL_Fetch_Array($resultSet);
                                $countAnnualTaken = $row['Annual'];
                                
                                $resultSet = ExecuteQuery('SELECT SUM(Leave_Days) AS Compassionate FROM Leave, LeaveType WHERE Leave_Type = LeaveType_ID AND Leave_Employee = '.$rowStaff['Staff_Code'].' AND LeaveType_Description = "Family" AND Leave_IsApproved = "1" AND (Leave_Start BETWEEN "'.$_SESSION['ViewLeave'][0].'" AND "'.$_SESSION['ViewLeave'][1].'" OR Leave_End BETWEEN "'.$_SESSION['ViewLeave'][0].'" AND "'.$_SESSION['ViewLeave'][1].'")');
                                $row = MySQL_Fetch_Array($resultSet);
                                $countCompassionateTaken = $row['Compassionate'];
                                
                                $resultSet = ExecuteQuery('SELECT SUM(Leave_Days) AS Maternity FROM Leave, LeaveType WHERE Leave_Type = LeaveType_ID AND Leave_Employee = '.$rowStaff['Staff_Code'].' AND LeaveType_Description = "Maternity/Paternity" AND Leave_IsApproved = "1" AND (Leave_Start BETWEEN "'.$_SESSION['ViewLeave'][0].'" AND "'.$_SESSION['ViewLeave'][1].'" OR Leave_End BETWEEN "'.$_SESSION['ViewLeave'][0].'" AND "'.$_SESSION['ViewLeave'][1].'")');
                                $row = MySQL_Fetch_Array($resultSet);
                                $countMaternityTaken = $row['Maternity'];
                                
                                $resultSet = ExecuteQuery('SELECT SUM(Leave_Days) AS Sick FROM Leave, LeaveType WHERE Leave_Type = LeaveType_ID AND Leave_Employee = '.$rowStaff['Staff_Code'].' AND LeaveType_Description = "Sick" AND Leave_IsApproved = "1" AND (Leave_Start BETWEEN "'.$_SESSION['ViewLeave'][0].'" AND "'.$_SESSION['ViewLeave'][1].'" OR Leave_End BETWEEN "'.$_SESSION['ViewLeave'][0].'" AND "'.$_SESSION['ViewLeave'][1].'")');
                                $row = MySQL_Fetch_Array($resultSet);
                                $countSickTaken = $row['Sick'];
                                
                                $resultSet = ExecuteQuery('SELECT SUM(Leave_Days) AS Study FROM Leave, LeaveType WHERE Leave_Type = LeaveType_ID AND Leave_Employee = '.$rowStaff['Staff_Code'].' AND LeaveType_Description = "Study" AND Leave_IsApproved = "1" AND (Leave_Start BETWEEN "'.$_SESSION['ViewLeave'][0].'" AND "'.$_SESSION['ViewLeave'][1].'" OR Leave_End BETWEEN "'.$_SESSION['ViewLeave'][0].'" AND "'.$_SESSION['ViewLeave'][1].'")');
                                $row = MySQL_Fetch_Array($resultSet);
                                $countStudyTaken = $row['Study'];
                          echo '<TR>
                                  <TD class="rowA">Leave Approved/Taken:
                                  </TD>
                                  <TD class="rowA center">'.SPrintF('%02.2f', $countAnnualTaken).'
                                  </TD>
                                  <TD class="rowA center">'.SPrintF('%02.2f', $countCompassionateTaken).'
                                  </TD>
                                  <TD class="rowA center">'.SPrintF('%02.2f', $countMaternityTaken).'
                                  </TD>
                                  <TD class="rowA center">'.SPrintF('%02.2f', $countSickTaken).'
                                  </TD>
                                  <TD class="rowA center">'.SPrintF('%02.2f', $countStudyTaken).'
                                  </TD>
                                </TR>
                                <TR>
                                  <TD class="rowA">Leave Due:
                                  </TD>
                                  <TD class="rowA center">'.SPrintF('%02.2f', $totalAnnualDue).'
                                  </TD>
                                  <TD class="rowA center">'.SPrintF('%02.2f', $totalCompassionateDue).'
                                  </TD>
                                  <TD class="rowA center">'.SPrintF('%02.2f', $totalMaternityDue).'
                                  </TD>
                                  <TD class="rowA center">'.SPrintF('%02.2f', $totalSickDue).'
                                  </TD>
                                  <TD class="rowA center">'.SPrintF('%02.2f', $totalStudyDue).'
                                  </TD>
                                </TR>
                                <TR>
                                  <TD class="rowB bold">Balance:
                                  </TD>
                                  <TD class="rowB center bold">'.SPrintF('%02.2f', $totalAnnualDue - $countAnnualTaken).'
                                  </TD>
                                  <TD class="rowB center bold">'.SPrintF('%02.2f', $totalCompassionateDue - $countCompassionateTaken).'
                                  </TD>
                                  <TD class="rowB center bold">'.SPrintF('%02.2f', $totalMaternityDue - $countMaternityTaken).'
                                  </TD>
                                  <TD class="rowB center bold">'.SPrintF('%02.2f', $totalSickDue - $countSickTaken).'
                                  </TD>
                                  <TD class="rowB center bold">'.SPrintF('%02.2f', $totalStudyDue - $countStudyTaken).'
                                  </TD>
                                </TR>';
                            
                          $rowDue = MySQL_Fetch_Array(ExecuteQuery('SELECT SUM(LeaveDue_Annual_Days) AS Days FROM LeaveDue WHERE LeaveDue_Employee = '.$rowStaff['Staff_Code'].'')); 
                          $rowTaken = MySQL_Fetch_Array(ExecuteQuery('SELECT SUM(Leave_Days) AS Days FROM Leave WHERE Leave_Employee = '.$rowStaff['Staff_Code'].' AND Leave_IsApproved = "1" AND Leave_Type = "1"')); 
                          echo '<TR>
                                  <TD class="rowB bold">Current Balance:
                                  </TD>
                                  <TD class="rowB center bold">'.SPrintF('%02.2f', $rowDue['Days'] - $rowTaken['Days']).'
                                  </TD>
                                  <TD colspan="4" class="rowB bold">&nbsp;
                                  </TD>
                                </TR>';
                        }
                        echo '</TABLE>
                              <BR /><BR />';
                      }
                      echo '</DIV>';
                    }*/
                    if (MySQL_Num_Rows($resultSetStaff) > 0)
                    {                       
                      echo '<DIV class="contentflow">
                              <P>This is the leave summary.</P>
                              <BR /><BR />
                              <TABLE cellspacing="5" align="center" class="long">
                                <TR>
                                  <TD colspan="9" class="header">Summary Details
                                  </TD>
                                </TR>
                                <TR>
                                  <TD class="subheaderclear">Staff Name
                                  </TD>
                                  <TD colspan="3" class="subheaderclear veryshort">Annual
                                  </TD>
                                  <TD class="subheaderclear veryshort">Compassionate
                                  </TD>
                                  <TD class="subheaderclear veryshort">Maternity/Paternity
                                  </TD>
                                  <TD class="subheaderclear veryshort">Sick
                                  </TD>
                                  <TD class="subheaderclear veryshort">Study
                                  </TD>
                                  <TD class="subheaderclear veryshort">Unpaid
                                  </TD>
                                </TR>
                                <TR>
                                  <TD class="subheader">&nbsp;
                                  </TD>
                                  <TD class="subheader">Due
                                  </TD>
                                  <TD class="subheader">Taken
                                  </TD>
                                  <TD class="subheader">Balance
                                  </TD>
                                  <TD class="subheader">Taken
                                  </TD>
                                  <TD class="subheader">Taken
                                  </TD>
                                  <TD class="subheader">Taken
                                  </TD>
                                  <TD class="subheader">Taken
                                  </TD>
                                  <TD class="subheader">Taken
                                  </TD>
                                </TR>';
                                $totalAnnualDue = 0;
                                $totalAnnualTaken = 0;
                                $totalCompassionateTaken = 0;
                                $totalMaternityTaken = 0;
                                $totalSickTaken = 0;
                                $totalStudyTaken = 0;
                                $totalUnpaidTaken = 0;
                                while ($row = MySQL_Fetch_Array($resultSetStaff))       
                                {
                                  $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT SUM(LeaveDue_Annual_Days) AS AnnualDue FROM LeaveDue WHERE LeaveDue_Employee = '.$row['Staff_Code'].' AND LeaveDue_Date BETWEEN "'.$_SESSION['ViewLeave'][0].'" AND "'.$_SESSION['ViewLeave'][1].'"'));
                                  $countAnnualDue = $rowTemp['AnnualDue'];
                                  $totalAnnualDue += $countAnnualDue;
                                  $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT SUM(Leave_Days) AS AnnualTaken FROM Leave WHERE Leave_Employee = '.$row['Staff_Code'].' AND Leave_Type = "1" AND Leave_IsApproved = "1" AND (Leave_Start BETWEEN "'.$_SESSION['ViewLeave'][0].'" AND "'.$_SESSION['ViewLeave'][1].'" OR Leave_End BETWEEN "'.$_SESSION['ViewLeave'][0].'" AND "'.$_SESSION['ViewLeave'][1].'")'));
                                  $countAnnualTaken = $rowTemp['AnnualTaken'];
                                  $totalAnnualTaken += $countAnnualTaken;
                                          
                                  $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT SUM(Leave_Days) AS CompassionateTaken FROM Leave WHERE Leave_Employee = '.$row['Staff_Code'].' AND Leave_Type = "4" AND Leave_IsApproved = "1" AND (Leave_Start BETWEEN "'.$_SESSION['ViewLeave'][0].'" AND "'.$_SESSION['ViewLeave'][1].'" OR Leave_End BETWEEN "'.$_SESSION['ViewLeave'][0].'" AND "'.$_SESSION['ViewLeave'][1].'")'));
                                  $countCompassionateTaken = $rowTemp['CompassionateTaken'];
                                  $totalCompassionateTaken += $countCompassionateTaken;
                                          
                                  $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT SUM(Leave_Days) AS MaternityTaken FROM Leave WHERE Leave_Employee = '.$row['Staff_Code'].' AND Leave_Type = "3" AND Leave_IsApproved = "1" AND (Leave_Start BETWEEN "'.$_SESSION['ViewLeave'][0].'" AND "'.$_SESSION['ViewLeave'][1].'" OR Leave_End BETWEEN "'.$_SESSION['ViewLeave'][0].'" AND "'.$_SESSION['ViewLeave'][1].'")'));
                                  $countMaternityTaken = $rowTemp['MaternityTaken'];
                                  $totalMaternityTaken += $countMaternityTaken;
                                          
                                  $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT SUM(Leave_Days) AS SickTaken FROM Leave WHERE Leave_Employee = '.$row['Staff_Code'].' AND Leave_Type = "2" AND Leave_IsApproved = "1" AND (Leave_Start BETWEEN "'.$_SESSION['ViewLeave'][0].'" AND "'.$_SESSION['ViewLeave'][1].'" OR Leave_End BETWEEN "'.$_SESSION['ViewLeave'][0].'" AND "'.$_SESSION['ViewLeave'][1].'")'));
                                  $countSickTaken = $rowTemp['SickTaken'];
                                  $totalSickTaken += $countSickTaken;
                                          
                                  $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT SUM(Leave_Days) AS StudyTaken FROM Leave WHERE Leave_Employee = '.$row['Staff_Code'].' AND Leave_Type = "5" AND Leave_IsApproved = "1" AND (Leave_Start BETWEEN "'.$_SESSION['ViewLeave'][0].'" AND "'.$_SESSION['ViewLeave'][1].'" OR Leave_End BETWEEN "'.$_SESSION['ViewLeave'][0].'" AND "'.$_SESSION['ViewLeave'][1].'")'));
                                  $countStudyTaken = $rowTemp['StudyTaken'];
                                  $totalStudyTaken += $countStudyTaken;
                                  
                                  $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT SUM(Leave_Days) AS UnpaidTaken FROM Leave WHERE Leave_Employee = '.$row['Staff_Code'].' AND Leave_Type = "7" AND Leave_IsApproved = "1" AND (Leave_Start BETWEEN "'.$_SESSION['ViewLeave'][0].'" AND "'.$_SESSION['ViewLeave'][1].'" OR Leave_End BETWEEN "'.$_SESSION['ViewLeave'][0].'" AND "'.$_SESSION['ViewLeave'][1].'")'));
                                  $countUnpaidTaken = $rowTemp['UnpaidTaken'];
                                  $totalUnpaidTaken += $countUnpaidTaken;
                                  
                                  echo '<TR>
                                          <TD class="rowA">'.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].'
                                          </TD>
                                          <TD class="rowA center">'.SPrintF('%02.2f', $countAnnualDue).'
                                          </TD>
                                          <TD class="rowA center">'.SPrintF('%02.2f', $countAnnualTaken).'
                                          </TD>
                                          <TD class="rowA center">'.SPrintF('%02.2f', $countAnnualDue - $countAnnualTaken).'
                                          </TD>
                                          <TD class="rowA center">'.SPrintF('%02.2f', $countCompassionateTaken).'
                                          </TD>
                                          <TD class="rowA center">'.SPrintF('%02.2f', $countMaternityTaken).'
                                          </TD>
                                          <TD class="rowA center">'.SPrintF('%02.2f', $countSickTaken).'
                                          </TD>
                                          <TD class="rowA center">'.SPrintF('%02.2f', $countStudyTaken).'
                                          </TD>
                                          <TD class="rowA center">'.SPrintF('%02.2f', $countUnpaidTaken).'
                                          </TD>
                                        </TR>';
                                }
                          echo '<TR>
                                  <TD class="rowB bold">Balance:
                                  </TD>
                                  <TD class="rowB center bold">'.SPrintF('%02.2f', $totalAnnualDue).'
                                  </TD>
                                  <TD class="rowB center bold">'.SPrintF('%02.2f', $totalAnnualTaken).'
                                  </TD>
                                  <TD class="rowB center bold">'.SPrintF('%02.2f', $totalAnnualDue - $totalAnnualTaken).'
                                  </TD>
                                  <TD class="rowB center bold">'.SPrintF('%02.2f', $totalCompassionateTaken).'
                                  </TD>
                                  <TD class="rowB center bold">'.SPrintF('%02.2f', $totalMaternityTaken).'
                                  </TD>
                                  <TD class="rowB center bold">'.SPrintF('%02.2f', $totalSickTaken).'
                                  </TD>
                                  <TD class="rowB center bold">'.SPrintF('%02.2f', $totalStudyTaken).'
                                  </TD>
                                  <TD class="rowB center bold">'.SPrintF('%02.2f', $totalUnpaidTaken).'
                                  </TD>
                                </TR>
                              </TABLE>
                            </DIV>';  
                    }
                    break;
                  default:
                    BuildContentHeader('Leave Summary - '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].' for '.$dateRange, "", "", false);
                    $set = false;
                    $resultSet = ExecuteQuery('SELECT Leave.*, IF (Leave_Doctor = "0", "No", "Yes") AS Doctor, LeaveType_Description FROM Leave, LeaveType WHERE LeaveType_ID = Leave_Type AND Leave_Employee = "'.$_SESSION['ViewLeave'][3].'" AND Leave_IsApproved = "1" AND (Leave_Start BETWEEN "'.$_SESSION['ViewLeave'][0].'" AND "'.$_SESSION['ViewLeave'][1].'" OR Leave_End BETWEEN "'.$_SESSION['ViewLeave'][0].'" AND "'.$_SESSION['ViewLeave'][1].'") ORDER BY Leave_Start'); 
                    if (MySQL_Num_Rows($resultSet) > 0)
                    {
                      if (!$set)
                      {
                        echo '<DIV class="contentflow">
                                <P>These are the leave details.</P>
                                <BR /><BR />
                                <TABLE cellspacing="5" align="center" class="long">';
                        $set = true;
                      }
                      echo '<TR>
                              <TD colspan="6" class="header">Leave Approved / Taken Details
                              </TD>
                            </TR>
                            <TR>
                              <TD class="subheader short">Period
                              </TD>
                              <TD class="subheader">Days
                              </TD>
                              <TD class="subheader">Type
                              </TD>
                              <TD class="subheader">Approved By
                              </TD>
                              <TD class="subheader">Dr\'s Certificate
                              </TD>
                              <TD class="subheader">Comments
                              </TD>
                            </TR>';
                            $total = 0;
                            while ($row = MySQL_Fetch_Array($resultSet))
                            {
                              $total += $row['Leave_Days'];
                            
                              $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = "'.$row['Leave_Approved'].'"'));                              
                              echo '<TR>
                                      <TD class="rowA center">'.(GetTextualDateFromDatabaseDate($row['Leave_Start']) == GetTextualDateFromDatabaseDate($row['Leave_End']) ? GetTextualDateFromDatabaseDate($row['Leave_Start']) : GetTextualDateFromDatabaseDate($row['Leave_Start']).' to '.GetTextualDateFromDatabaseDate($row['Leave_End'])).'
                                      </TD>
                                      <TD class="rowA center">'.SPrintF('%02.2f', $row['Leave_Days']).'
                                      </TD>
                                      <TD class="rowA center">';
                                        if ($row['LeaveType_Description'] == 'Family')
                                          echo 'Compassionate'; 
                                        else
                                          echo $row['LeaveType_Description'];
                                echo '</TD>
                                      <TD class="rowA center">'.$rowTemp['Staff_First_Name'].' '.$rowTemp['Staff_Last_Name'].'
                                      </TD>
                                      <TD class="rowA center">';
                                        if ($row['LeaveType_Description'] == 'Sick')
                                          echo $row['Doctor']; 
                                        else
                                          echo 'N/A';
                                echo '</TD>
                                      <TD class="rowA">'.$row['Leave_Comments'].'
                                      </TD>
                                    </TR>';                         
                              $count++;
                            }  
                      echo '<TR>
                              <TD class="rowB bold">Total
                              </TD>
                              <TD class="rowB center bold">'.SPrintF('%02.2f', $total).'
                              </TD>
                              <TD colspan="4" class="rowB">
                              </TD>
                            </TR>';  
                    }
                    
                    $resultSet = ExecuteQuery('SELECT Leave.*, IF (Leave_Doctor = "0", "No", "Yes") AS Doctor, LeaveType_Description FROM Leave, LeaveType WHERE LeaveType_ID = Leave_Type AND Leave_Employee = "'.$_SESSION['ViewLeave'][3].'" AND Leave_IsApproved = "0" AND (Leave_Start BETWEEN "'.$_SESSION['ViewLeave'][0].'" AND "'.$_SESSION['ViewLeave'][1].'" OR Leave_End BETWEEN "'.$_SESSION['ViewLeave'][0].'" AND "'.$_SESSION['ViewLeave'][1].'") ORDER BY Leave_Start'); 
                    if (MySQL_Num_Rows($resultSet) > 0)
                    {
                      if (!$set)
                      {
                        echo '<DIV class="contentflow">
                                <P>These are the leave details.</P>
                                <BR /><BR />
                                <TABLE cellspacing="5" align="center" class="long">';
                        $set = true;
                      }
                      echo '<TR>
                              <TD colspan="6" class="header">Leave Pending Details
                              </TD>
                            </TR>
                            <TR>
                              <TD class="subheader short">Period
                              </TD>
                              <TD class="subheader">Days
                              </TD>
                              <TD class="subheader">Type
                              </TD>
                              <TD class="subheader">To Approve
                              </TD>
                              <TD class="subheader">Dr\'s Certificate
                              </TD>
                              <TD class="subheader">Comments
                              </TD>
                            </TR>';
                            $total = 0;
                            while ($row = MySQL_Fetch_Array($resultSet))
                            {
                              $total += $row['Leave_Days'];
                            
                              $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = "'.$row['Leave_Approved'].'"'));                              
                              echo '<TR>
                                      <TD class="rowA center">'.(GetTextualDateFromDatabaseDate($row['Leave_Start']) == GetTextualDateFromDatabaseDate($row['Leave_End']) ? GetTextualDateFromDatabaseDate($row['Leave_Start']) : GetTextualDateFromDatabaseDate($row['Leave_Start']).' to '.GetTextualDateFromDatabaseDate($row['Leave_End'])).'
                                      </TD>
                                      <TD class="rowA center">'.SPrintF('%02.2f', $row['Leave_Days']).'
                                      </TD>
                                      <TD class="rowA center">';
                                        if ($row['LeaveType_Description'] == 'Family')
                                          echo 'Compassionate'; 
                                        else
                                          echo $row['LeaveType_Description'];
                                echo '</TD>
                                      <TD class="rowA">'.$rowTemp['Staff_First_Name'].' '.$rowTemp['Staff_Last_Name'].'
                                      </TD>
                                      <TD class="rowA center">';
                                        if ($row['LeaveType_Description'] == 'Sick')
                                          echo $row['Doctor']; 
                                        else
                                          echo 'N/A';
                                echo '</TD>
                                      <TD class="rowA">'.$row['Leave_Comments'].'
                                      </TD>
                                    </TR>';                         
                              $count++;
                            }  
                      echo '<TR>
                              <TD class="rowB bold">Total
                              </TD>
                              <TD class="rowB center bold">'.SPrintF('%02.2f', $total).'
                              </TD>
                              <TD colspan="4" class="rowB">
                              </TD>
                            </TR>';  
                    }
                    
                    $resultSet = ExecuteQuery('SELECT Leave.*, IF (Leave_Doctor = "0", "No", "Yes") AS Doctor, LeaveType_Description FROM Leave, LeaveType WHERE LeaveType_ID = Leave_Type AND Leave_Employee = "'.$_SESSION['ViewLeave'][3].'" AND Leave_IsApproved = "2" AND (Leave_Start BETWEEN "'.$_SESSION['ViewLeave'][0].'" AND "'.$_SESSION['ViewLeave'][1].'" OR Leave_End BETWEEN "'.$_SESSION['ViewLeave'][0].'" AND "'.$_SESSION['ViewLeave'][1].'") ORDER BY Leave_Start'); 
                    if (MySQL_Num_Rows($resultSet) > 0)
                    {
                      if (!$set)
                      {
                        echo '<DIV class="contentflow">
                                <P>These are the leave details.</P>
                                <BR /><BR />
                                <TABLE cellspacing="5" align="center" class="long">';
                        $set = true;
                      }
                      echo '<TR>
                              <TD colspan="6" class="header">Leave Denied Details
                              </TD>
                            </TR>
                            <TR>
                              <TD class="subheader short">Period
                              </TD>
                              <TD class="subheader">Days
                              </TD>
                              <TD class="subheader">Type
                              </TD>
                              <TD class="subheader">Approved By
                              </TD>
                              <TD class="subheader">Dr\'s Certificate
                              </TD>
                              <TD class="subheader">Comments
                              </TD>
                            </TR>';
                            $total = 0;
                            while ($row = MySQL_Fetch_Array($resultSet))
                            {
                              $total += $row['Leave_Days'];
                            
                              $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = "'.$row['Leave_Approved'].'"'));                              
                              echo '<TR>
                                      <TD class="rowA center">'.(GetTextualDateFromDatabaseDate($row['Leave_Start']) == GetTextualDateFromDatabaseDate($row['Leave_End']) ? GetTextualDateFromDatabaseDate($row['Leave_Start']) : GetTextualDateFromDatabaseDate($row['Leave_Start']).' to '.GetTextualDateFromDatabaseDate($row['Leave_End'])).'
                                      </TD>
                                      <TD class="rowA center">'.SPrintF('%02.2f', $row['Leave_Days']).'
                                      </TD>
                                      <TD class="rowA center">';
                                        if ($row['LeaveType_Description'] == 'Family')
                                          echo 'Compassionate'; 
                                        else
                                          echo $row['LeaveType_Description'];
                                echo '</TD>
                                      <TD class="rowA center">'.$rowTemp['Staff_First_Name'].' '.$rowTemp['Staff_Last_Name'].'
                                      </TD>
                                      <TD class="rowA center">';
                                        if ($row['LeaveType_Description'] == 'Sick')
                                          echo $row['Doctor']; 
                                        else
                                          echo 'N/A';
                                echo '</TD>
                                      <TD class="rowA">'.$row['Leave_Comments'].'
                                      </TD>
                                    </TR>';                         
                              $count++;
                            }  
                      echo '<TR>
                              <TD class="rowB bold">Total
                              </TD>
                              <TD class="rowB center bold">'.SPrintF('%02.2f', $total).'
                              </TD>
                              <TD colspan="4" class="rowB">
                              </TD>
                            </TR>';  
                    }
                    
                    $resultSet = ExecuteQuery('SELECT Leave.*, IF (Leave_Doctor = "0", "No", "Yes") AS Doctor, LeaveType_Description FROM Leave, LeaveType WHERE LeaveType_ID = Leave_Type AND Leave_Employee = "'.$_SESSION['ViewLeave'][3].'" AND Leave_IsApproved = "3" AND (Leave_Start BETWEEN "'.$_SESSION['ViewLeave'][0].'" AND "'.$_SESSION['ViewLeave'][1].'" OR Leave_End BETWEEN "'.$_SESSION['ViewLeave'][0].'" AND "'.$_SESSION['ViewLeave'][1].'") ORDER BY Leave_Start'); 
                    if (MySQL_Num_Rows($resultSet) > 0)
                    {
                      if (!$set)
                      {
                        echo '<DIV class="contentflow">
                                <P>These are the leave details.</P>
                                <BR /><BR />
                                <TABLE cellspacing="5" align="center" class="long">';
                        $set = true;
                      }
                      echo '<TR>
                              <TD colspan="6" class="header">Leave Cancelled Details
                              </TD>
                            </TR>
                            <TR>
                              <TD class="subheader short">Period
                              </TD>
                              <TD class="subheader">Days
                              </TD>
                              <TD class="subheader">Type
                              </TD>
                              <TD class="subheader">To Approve
                              </TD>
                              <TD class="subheader">Dr\'s Certificate
                              </TD>
                              <TD class="subheader">Comments
                              </TD>
                            </TR>';
                            $total = 0;
                            while ($row = MySQL_Fetch_Array($resultSet))
                            {
                              $total += $row['Leave_Days'];
                            
                              $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = "'.$row['Leave_Approved'].'"'));                              
                              echo '<TR>
                                      <TD class="rowA center">'.(GetTextualDateFromDatabaseDate($row['Leave_Start']) == GetTextualDateFromDatabaseDate($row['Leave_End']) ? GetTextualDateFromDatabaseDate($row['Leave_Start']) : GetTextualDateFromDatabaseDate($row['Leave_Start']).' to '.GetTextualDateFromDatabaseDate($row['Leave_End'])).'
                                      </TD>
                                      <TD class="rowA center">'.SPrintF('%02.2f', $row['Leave_Days']).'
                                      </TD>
                                      <TD class="rowA center">';
                                        if ($row['LeaveType_Description'] == 'Family')
                                          echo 'Compassionate'; 
                                        else
                                          echo $row['LeaveType_Description'];
                                echo '</TD>
                                      <TD class="rowA">'.$rowTemp['Staff_First_Name'].' '.$rowTemp['Staff_Last_Name'].'
                                      </TD>
                                      <TD class="rowA center">';
                                        if ($row['LeaveType_Description'] == 'Sick')
                                          echo $row['Doctor']; 
                                        else
                                          echo 'N/A';
                                echo '</TD>
                                      <TD class="rowA">'.$row['Leave_Comments'].'
                                      </TD>
                                    </TR>';                         
                              $count++;
                            }  
                      echo '<TR>
                              <TD class="rowB bold">Total
                              </TD>
                              <TD class="rowB center bold">'.SPrintF('%02.2f', $total).'
                              </TD>
                              <TD colspan="4" class="rowB">
                              </TD>
                            </TR>';  
                    }
                    
                    $totalAnnualDue = 0;
                    $resultSet = ExecuteQuery('SELECT * FROM LeaveDue WHERE LeaveDue_Employee = "'.$_SESSION['ViewLeave'][3].'" AND LeaveDue_Date BETWEEN "'.$_SESSION['ViewLeave'][0].'" AND "'.$_SESSION['ViewLeave'][1].'" ORDER BY LeaveDue_Date ASC'); 
                    if (MySQL_Num_Rows($resultSet) > 0)
                    {
                      if (!$set)
                      {
                        echo '<DIV class="contentflow">
                                <P>These are the leave details.</P>
                                <BR /><BR />
                                <TABLE cellspacing="5" align="center" class="long">';
                        $set = true;
                      }
                      echo '<TR>
                              <TD colspan="6" class="header">Leave Due Details
                              </TD>
                            </TR>
                            <TR>
                              <TD class="subheader">Date Added
                              </TD>
                              <TD class="subheader veryshort">Annual
                              </TD>
                              <TD class="subheader veryshort">Compassionate
                              </TD>
                              <TD class="subheader veryshort">Maternity/Paternity
                              </TD>
                              <TD class="subheader veryshort">Sick
                              </TD>
                              <TD class="subheader veryshort">Study
                              </TD>
                            </TR>';
                            while ($row = MySQL_Fetch_Array($resultSet))
                            {
                              $totalAnnualDue += $row['LeaveDue_Annual_Days'];
                              
                              echo '<TR>
                                      <TD class="rowA center">'.GetTextualDateFromDatabaseDate($row['LeaveDue_Date']).'
                                      </TD>
                                      <TD class="rowA center">'.SPrintF('%02.2f', $row['LeaveDue_Annual_Days']).'
                                      </TD>
                                      <TD colspan="4" class="rowA">
                                      </TD>
                                    </TR>';
                            }  
                      echo '<TR>
                              <TD class="rowB bold">Total
                              </TD>
                              <TD class="rowB center bold">'.SPrintF('%02.2f', $totalAnnualDue).'
                              </TD>
                              <TD colspan="4" class="rowB">
                              </TD>
                            </TR>';  
                    }
                    
                    if (($_SESSION['cAuth'] & 64) || ($_SESSION['cUID'] == $_SESSION['ViewLeave'][3]))
                    {
                      if (!$set)
                      {
                        echo '<DIV class="contentflow">
                                <P>These are the leave details.</P>
                                <BR /><BR />
                                <TABLE cellspacing="5" align="center" class="long">';
                        $set = true;
                      }
                      echo '<TR>
                              <TD colspan="6" class="header">Summary Details
                              </TD>
                            </TR>
                            <TR>
                              <TD class="subheader short">&nbsp;
                              </TD>
                              <TD class="subheader veryshort">Annual
                              </TD>
                              <TD class="subheader veryshort">Compassionate
                              </TD>
                              <TD class="subheader veryshort">Maternity/Paternity
                              </TD>
                              <TD class="subheader veryshort">Sick
                              </TD>
                              <TD class="subheader veryshort">Study
                              </TD>
                            </TR>';
                            $resultSet = ExecuteQuery('SELECT SUM(Leave_Days) AS Annual FROM Leave, LeaveType WHERE Leave_Type = LeaveType_ID AND Leave_Employee = "'.$_SESSION['ViewLeave'][3].'" AND LeaveType_Description = "Annual" AND Leave_IsApproved = "1" AND (Leave_Start BETWEEN "'.$_SESSION['ViewLeave'][0].'" AND "'.$_SESSION['ViewLeave'][1].'" OR Leave_End BETWEEN "'.$_SESSION['ViewLeave'][0].'" AND "'.$_SESSION['ViewLeave'][1].'")');
                            $row = MySQL_Fetch_Array($resultSet);
                            $countAnnualTaken = $row['Annual'];
                            
                            $resultSet = ExecuteQuery('SELECT SUM(Leave_Days) AS Compassionate FROM Leave, LeaveType WHERE Leave_Type = LeaveType_ID AND Leave_Employee = "'.$_SESSION['ViewLeave'][3].'" AND LeaveType_Description = "Family" AND Leave_IsApproved = "1" AND (Leave_Start BETWEEN "'.$_SESSION['ViewLeave'][0].'" AND "'.$_SESSION['ViewLeave'][1].'" OR Leave_End BETWEEN "'.$_SESSION['ViewLeave'][0].'" AND "'.$_SESSION['ViewLeave'][1].'")');
                            $row = MySQL_Fetch_Array($resultSet);
                            $countCompassionateTaken = $row['Compassionate'];
                            
                            $resultSet = ExecuteQuery('SELECT SUM(Leave_Days) AS Maternity FROM Leave, LeaveType WHERE Leave_Type = LeaveType_ID AND Leave_Employee = "'.$_SESSION['ViewLeave'][3].'" AND LeaveType_Description = "Maternity/Paternity" AND Leave_IsApproved = "1" AND (Leave_Start BETWEEN "'.$_SESSION['ViewLeave'][0].'" AND "'.$_SESSION['ViewLeave'][1].'" OR Leave_End BETWEEN "'.$_SESSION['ViewLeave'][0].'" AND "'.$_SESSION['ViewLeave'][1].'")');
                            $row = MySQL_Fetch_Array($resultSet);
                            $countMaternityTaken = $row['Maternity'];
                            
                            $resultSet = ExecuteQuery('SELECT SUM(Leave_Days) AS Sick FROM Leave, LeaveType WHERE Leave_Type = LeaveType_ID AND Leave_Employee = "'.$_SESSION['ViewLeave'][3].'" AND LeaveType_Description = "Sick" AND Leave_IsApproved = "1" AND (Leave_Start BETWEEN "'.$_SESSION['ViewLeave'][0].'" AND "'.$_SESSION['ViewLeave'][1].'" OR Leave_End BETWEEN "'.$_SESSION['ViewLeave'][0].'" AND "'.$_SESSION['ViewLeave'][1].'")');
                            $row = MySQL_Fetch_Array($resultSet);
                            $countSickTaken = $row['Sick'];
                            
                            $resultSet = ExecuteQuery('SELECT SUM(Leave_Days) AS Study FROM Leave, LeaveType WHERE Leave_Type = LeaveType_ID AND Leave_Employee = "'.$_SESSION['ViewLeave'][3].'" AND LeaveType_Description = "Study" AND Leave_IsApproved = "1" AND (Leave_Start BETWEEN "'.$_SESSION['ViewLeave'][0].'" AND "'.$_SESSION['ViewLeave'][1].'" OR Leave_End BETWEEN "'.$_SESSION['ViewLeave'][0].'" AND "'.$_SESSION['ViewLeave'][1].'")');
                            $row = MySQL_Fetch_Array($resultSet);
                            $countStudyTaken = $row['Study'];
                      echo '<TR>
                              <TD class="rowA">Leave Approved/Taken:
                              </TD>
                              <TD class="rowA center">'.SPrintF('%02.2f', $countAnnualTaken).'
                              </TD>
                              <TD class="rowA center">'.SPrintF('%02.2f', $countCompassionateTaken).'
                              </TD>
                              <TD class="rowA center">'.SPrintF('%02.2f', $countMaternityTaken).'
                              </TD>
                              <TD class="rowA center">'.SPrintF('%02.2f', $countSickTaken).'
                              </TD>
                              <TD class="rowA center">'.SPrintF('%02.2f', $countStudyTaken).'
                              </TD>
                            </TR>
                            <TR>
                              <TD class="rowA">Leave Due:
                              </TD>
                              <TD class="rowA center">'.SPrintF('%02.2f', $totalAnnualDue).'
                              </TD>
                              <TD colspan="4" class="rowA">
                              </TD>
                            </TR>
                            <TR>
                              <TD class="rowB bold">Period Balance:
                              </TD>
                              <TD class="rowB center bold">'.SPrintF('%02.2f', $totalAnnualDue - $countAnnualTaken).'
                              </TD>
                              <TD colspan="4" class="rowB">
                              </TD>
                            </TR>';
                            
                      $rowDue = MySQL_Fetch_Array(ExecuteQuery('SELECT SUM(LeaveDue_Annual_Days) AS Days FROM LeaveDue WHERE LeaveDue_Employee = '.$_SESSION['ViewLeave'][3].'')); 
                      $rowTaken = MySQL_Fetch_Array(ExecuteQuery('SELECT SUM(Leave_Days) AS Days FROM Leave WHERE Leave_Employee = '.$_SESSION['ViewLeave'][3].' AND Leave_IsApproved = "1" AND Leave_Type = "1"')); 
                      echo '<TR>
                              <TD class="rowB bold">Current Balance:
                              </TD>
                              <TD class="rowB center bold">'.SPrintF('%02.2f', $rowDue['Days'] - $rowTaken['Days']).'
                              </TD>
                              <TD colspan="4" class="rowB bold">&nbsp;
                              </TD>
                            </TR>';
                    }
                    if (!$set)
                    {
                      echo '<DIV class="contentflow">
                              <P>There are no leave details for the given date range.</P>
                              <BR /><BR />';
                    } else
                      echo '</TABLE>';
                    echo '</DIV>';
                    break;
                }
                break;
              case '2': 
                switch ($_SESSION['ViewLeave'][3])
                {
                  case "":
                    BuildContentHeader('Leave Summary AFB - '.$dateRange, "", "", false);      
                    $resultSetStaff = ExecuteQuery('SELECT * FROM Staff WHERE Staff_IsEmployee > "0" ORDER BY Staff_First_Name, Staff_Last_Name');
                    $resultSet = ExecuteQuery('SELECT Leave.*, LeaveType_Description, Staff_First_Name, Staff_Last_Name FROM Leave, LeaveType, Staff WHERE LeaveType_ID = Leave_Type AND Staff_Code = Leave_Employee AND Staff_IsEmployee > "0" AND Leave_IsApproved = "1" AND (Leave_Start BETWEEN "'.$_SESSION['ViewLeave'][0].'" AND "'.$_SESSION['ViewLeave'][1].'" OR Leave_End BETWEEN "'.$_SESSION['ViewLeave'][0].'" AND "'.$_SESSION['ViewLeave'][1].'") AND (LeaveType_Description = "Sick" OR LeaveType_Description = "Annual") ORDER BY Leave_Start'); 
                    if (MySQL_Num_Rows($resultSet) > 0)
                    {
                      $set = true;
                      echo '<DIV class="contentflow">
                              <P>These are the leave details.</P>
                              <BR /><BR />
                              <TABLE cellspacing="5" align="center" class="standard">
                                <TR>
                                  <TD colspan="4" class="header">Leave Approved / Taken Details
                                  </TD>
                                </TR>
                                <TR>
                                  <TD class="subheader short">Staff Name
                                  </TD>
                                  <TD class="subheader short">Period
                                  </TD>
                                  <TD class="subheader veryshort">Days
                                  </TD>
                                  <TD class="subheader veryshort">Type
                                  </TD>
                                </TR>';
                                while ($row = MySQL_Fetch_Array($resultSet))
                                {
                                  $loDays = 0;
                                  
                                  $loStartDate = $row['Leave_Start'];
                                  $loEndDate = $row['Leave_End'];
                                  $loRemainder = $row['Leave_Days'] - (int)$row['Leave_Days'];
                                  
                                  $loStartDate = ($loStartDate > GetDatabaseDateFromSessionDate($_SESSION['ViewLeave'][0]) ? $loStartDate : GetDatabaseDateFromSessionDate($_SESSION['ViewLeave'][0]));
                                  $loEndDate = ($loEndDate < GetDatabaseDateFromSessionDate($_SESSION['ViewLeave'][1]) ? $loEndDate : GetDatabaseDateFromSessionDate($_SESSION['ViewLeave'][1]));
                                  
                                  while ($loStartDate <= $loEndDate)
                                  {
                                    $loDay = SubStr($loStartDate, 8, 2);
                                    $loMonth = SubStr($loStartDate, 5, 2);
                                    $loYear = SubStr($loStartDate, 0, 4);
                                    
                                    $loRowHolidayCheck = MySQL_Fetch_Array(ExecuteQuery('SELECT COUNT(*) FROM PublicHoliday WHERE PublicHoliday_Date = "'.$loStartDate.'"'));
                                    if ($loRowHolidayCheck[0] == 0)
                                    {
                                      $loDate = MkTime(0, 0, 0, $loMonth, $loDay, $loYear);
                                      if (!((Date('w', $loDate) == 0) || (Date('w', $loDate) == 6)))
                                      {
                                        if ($loRemainder > 0)
                                        {
                                          if ($row['Leave_Start'] == $loStartDate)
                                            $loDays += $loRemainder;
                                          else
                                            $loDays++;
                                        } else
                                          $loDays++;
                                      }
                                    }
                                    
                                    $loStartDate = Date('Y-m-d', MkTime(0, 0, 0, $loMonth, $loDay + 1, $loYear));
                                  };
                                  
                                  echo '<TR>
                                          <TD class="rowA center">'.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].'
                                          </TD>
                                          <TD class="rowA center">'.(GetTextualDateFromDatabaseDate($row['Leave_Start']) == GetTextualDateFromDatabaseDate($row['Leave_End']) ? GetTextualDateFromDatabaseDate($row['Leave_Start']) : GetTextualDateFromDatabaseDate($row['Leave_Start']).' to '.GetTextualDateFromDatabaseDate($row['Leave_End'])).'
                                          </TD>
                                          <TD class="rowA center">'.$loDays.'
                                          </TD>
                                          <TD class="rowA center">';
                                              echo $row['LeaveType_Description'];
                                    echo '</TD>
                                        </TR>';                         
                                  $count++;
                                }
                    }
                    
                    if (!$set)
                    {
                      echo '<DIV class="contentflow">
                              <P>There are no leave details for the given date range.</P>
                              <BR /><BR />';
                    } else
                        echo '</TABLE>';
                    echo '</DIV>';
                    break;
                  default:
                    BuildContentHeader('Leave Summary AFB - '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].' for '.$dateRange, "", "", false);
                    $set = false;
                    $resultSet = ExecuteQuery('SELECT Leave.*, LeaveType_Description FROM Leave, LeaveType WHERE LeaveType_ID = Leave_Type AND Leave_Employee = "'.$_SESSION['ViewLeave'][3].'" AND Leave_IsApproved = "1" AND (Leave_Start BETWEEN "'.$_SESSION['ViewLeave'][0].'" AND "'.$_SESSION['ViewLeave'][1].'" OR Leave_End BETWEEN "'.$_SESSION['ViewLeave'][0].'" AND "'.$_SESSION['ViewLeave'][1].'") AND (LeaveType_Description = "Sick" OR LeaveType_Description = "Annual") ORDER BY Leave_Start'); 
                    if (MySQL_Num_Rows($resultSet) > 0)
                    {
                      $set = true;
                      echo '<DIV class="contentflow">
                              <P>These are the leave details.</P>
                              <BR /><BR />
                              <TABLE cellspacing="5" align="center" class="short">
                                <TR>
                                  <TD colspan="3" class="header">Leave Approved / Taken Details
                                  </TD>
                                </TR>
                                <TR>
                                  <TD class="subheader short">Period
                                  </TD>
                                  <TD class="subheader veryshort">Days
                                  </TD>
                                  <TD class="subheader veryshort">Type
                                  </TD>
                                </TR>';
                                while ($row = MySQL_Fetch_Array($resultSet))
                                {
                                  $loDays = 0;
                                  
                                  $loStartDate = $row['Leave_Start'];
                                  $loEndDate = $row['Leave_End'];
                                  $loRemainder = $row['Leave_Days'] - (int)$row['Leave_Days'];
                                  
                                  $loStartDate = ($loStartDate > GetDatabaseDateFromSessionDate($_SESSION['ViewLeave'][0]) ? $loStartDate : GetDatabaseDateFromSessionDate($_SESSION['ViewLeave'][0]));
                                  $loEndDate = ($loEndDate < GetDatabaseDateFromSessionDate($_SESSION['ViewLeave'][1]) ? $loEndDate : GetDatabaseDateFromSessionDate($_SESSION['ViewLeave'][1]));
                                  
                                  while ($loStartDate <= $loEndDate)
                                  {
                                    $loDay = SubStr($loStartDate, 8, 2);
                                    $loMonth = SubStr($loStartDate, 5, 2);
                                    $loYear = SubStr($loStartDate, 0, 4);
                                    
                                    $loRowHolidayCheck = MySQL_Fetch_Array(ExecuteQuery('SELECT COUNT(*) FROM PublicHoliday WHERE PublicHoliday_Date = "'.$loStartDate.'"'));
                                    if ($loRowHolidayCheck[0] == 0)
                                    {
                                      $loDate = MkTime(0, 0, 0, $loMonth, $loDay, $loYear);
                                      if (!((Date('w', $loDate) == 0) || (Date('w', $loDate) == 6)))
                                      {
                                        if ($loRemainder > 0)
                                        {
                                          if ($row['Leave_Start'] == $loStartDate)
                                            $loDays += $loRemainder;
                                          else
                                            $loDays++;
                                        } else
                                          $loDays++;
                                      }
                                    }
                                    
                                    $loStartDate = Date('Y-m-d', MkTime(0, 0, 0, $loMonth, $loDay + 1, $loYear));
                                  };
                                  
                                  echo '<TR>
                                          <TD class="rowA center">'.(GetTextualDateFromDatabaseDate($row['Leave_Start']) == GetTextualDateFromDatabaseDate($row['Leave_End']) ? GetTextualDateFromDatabaseDate($row['Leave_Start']) : GetTextualDateFromDatabaseDate($row['Leave_Start']).' to '.GetTextualDateFromDatabaseDate($row['Leave_End'])).'
                                          </TD>
                                          <TD class="rowA center">'.$loDays.'
                                          </TD>
                                          <TD class="rowA center">';
                                              echo $row['LeaveType_Description'];
                                    echo '</TD>
                                        </TR>';                         
                                  $count++;
                                }
                    }
                    
                    if (!$set)
                    {
                      echo '<DIV class="contentflow">
                              <P>There are no leave details for the given date range.</P>
                              <BR /><BR />';
                    } else
                        echo '</TABLE>';
                    echo '</DIV>';
                    break;
                }
                break;
              default:
                echo 'Kyaaaaaaaaaah! *chop* Invalid report type! Error! Error!';   
                break;
            }
            Session_Unregister('ViewLeave');
          } else
          { 
            BuildContentHeader('Leave Policy', "", "", false);                        
            echo '<DIV class="contentflow">
                    <P>This is S4\'s leave policy. It follows the Basic Conditions of Employment Act of 1997 which can be found <A tabindex="-1" href="./Files/Intranet/Basic_Conditions_of_Employment.pdf">here</A>. Leave is issued on a first-come, first-served basis. Additional information can be found <A tabindex="-1" href="/Files/Intranet/Expenses_Guideline.pdf">here</A>.</P>
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center" class="standard">
                      <TR>
                        <TD colspan="2" class="header">Leave Details
                        </TD>
                      </TR>
                      <TR>
                        <TD class="subheader veryshort">Type
                        </TD>
                        <TD class="subheader">Amount
                        </TD>
                      </TR>
                      <TR>
                        <TD class="rowA">Annual
                        </TD>
                        <TD class="rowA">15 Working days per year. Otherwise 21 working days per year for those with more than 10 years of service.              
                        </TD>
                      </TR>
                      <TR>
                        <TD class="rowB">Compassionate
                        </TD>
                        <TD class="rowB">3 Days per year.               
                        </TD>
                      </TR>
                      <TR>
                        <TD class="rowA">Maternity/Paternity
                        </TD>
                        <TD class="rowA">4 Months for mothers and 3 days for fathers.              
                        </TD>
                      </TR>
                      <TR>
                        <TD class="rowB">Sick
                        </TD>
                        <TD class="rowB">10 Working Days per year (Medical certificate may be required).         
                        </TD>
                      </TR>
                      <TR>
                        <TD class="rowA">Study
                        </TD>
                        <TD class="rowA">Depends on circumstances. Refer to management.               
                        </TD>
                      </TR>
                    </TABLE>
                  </DIV>';            
            
            BuildContentHeader('Maintenance', "", "", true);    
            echo '<DIV class="contentflow">
                    <P>Leave can be requested by using the form below.';
                    if (($_SESSION['cAuth'] & 8) || ($_SESSION['cAuth'] & 64))
                      if ($_SESSION['cAuth'] & 64)
                        echo ' It can also be approved, denied and allocated.';
                      else 
                        echo ' It can also be approved or denied.';
              echo '</P>
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center" class="standard">
                      <FORM method="post" action="Handlers/Leave_Handler.php">
                        <INPUT name="Type" type="hidden" value="Maintain">';
                        if ($_SESSION['cAuth'] & 64)
                          echo '<TR>
                                  <TD colspan="4" class="header">Allocate
                                  </TD>
                                </TR>
                                <TR>
                                  <TD colspan="3">To allocate annual leave, click Allocate and confirm when prompted.
                                  </TD>
                                  <TD class="right">
                                    <INPUT tabindex="1" name="Submit" type="submit" class="button" value="Allocate" />                   
                                  </TD>
                                </TR>'; 
                        if ($_SESSION['cAuth'] & 8)
                        {
                          echo '<TR>
                                  <TD colspan="4" class="header">Approve
                                  </TD>
                                </TR>
                                <TR>
                                  <TD colspan="3">To approve or deny leave, specify the particulars, click Approve and confirm when prompted.
                                  </TD>
                                </TR>
                                <TR>
                                  <TD class="short">Request:
                                    <SPAN class="note">*
                                    </SPAN>
                                  </TD>
                                  <TD>';
                                    BuildLeaveApproveSelector('2', 'ApproveLeave', 'standard', $_SESSION['cUID']);
                            echo '</TD>
                                  <TD colspan="2" class="right">
                                    <INPUT tabindex="3" name="Submit" type="submit" class="button" value="Approve" />                   
                                  </TD>
                                </TR>
                                <TR>
                                  <TD colspan="4" class="header">Cancel
                                  </TD>
                                </TR>
                                <TR>
                                  <TD colspan="3">To cancel leave, specify the particulars, click Cancel and confirm when prompted.
                                  </TD>
                                </TR>
                                <TR>
                                  <TD class="short">Leave:
                                    <SPAN class="note">*
                                    </SPAN>
                                  </TD>
                                  <TD>';
                                    BuildLeaveCancelSelector('4', 'CancelLeave', 'standard');
                            echo '</TD>
                                  <TD colspan="2" class="right">
                                    <INPUT tabindex="5" name="Submit" type="submit" class="button" value="Cancel" />                   
                                  </TD>
                                </TR>
                                <TR>
                                  <TD colspan="4" class="header">Edit
                                  </TD>
                                </TR>
                                <TR>
                                  <TD colspan="3">To edit leave, specify the particulars, click Edit and complete the form that is dislpayed.
                                  </TD>
                                </TR>
                                <TR>
                                  <TD class="short">Leave:
                                    <SPAN class="note">*
                                    </SPAN>
                                  </TD>
                                  <TD>';
                                    BuildLeaveEditSelector('6', 'EditLeave', 'standard', $_SESSION['cUID']);
                            echo '</TD>
                                  <TD colspan="2" class="right">
                                    <INPUT tabindex="7" name="Submit" type="submit" class="button" value="Edit" />                   
                                  </TD>
                                </TR>
                                <TR>
                                  <TD colspan="4" class="header">Request
                                  </TD>
                                </TR>
                                <TR>
                                  <TD colspan="4">To request leave, specify the particulars, click Request and complete the form that is displayed.
                                  </TD>
                                </TR>
                                <TR>
                                  <TD class="short">Request:
                                    <SPAN class="note">*
                                    </SPAN>
                                  </TD>
                                  <TD>';
                                    BuildStaffSelector('8', 'RequestLeave', 'standard', $_SESSION['cUID'], true);
                            echo '</TD>
                                  <TD colspan="2" class="right">
                                    <INPUT tabindex="9" name="Submit" type="submit" class="button" value="Request" />                   
                                  </TD>
                                </TR>';
                        } else
                        {
                          echo '<TR>
                                  <TD colspan="4" class="header">Request
                                  </TD>
                                </TR>
                                <TR>
                                  <TD colspan="3">To request leave, click Request and complete the form that is displayed.
                                  </TD>
                                  <TD class="right">
                                    <INPUT tabindex="8" name="Submit" type="submit" class="button" value="Request" />                   
                                  </TD>
                                </TR>';
                        }
                echo '</FORM>
                    </TABLE>
                  </DIV>';                    
              
            BuildContentHeader('View Leave', "", "", true);              
            echo '<DIV class="contentflow">
                    <P>Leave can be viewed by using the form below.</P>
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center" class="standard">
                      <FORM method="post" action="Handlers/Leave_Handler.php">
                        <INPUT name="Type" type="hidden" value="View">
                        <TR>
                          <TD colspan="4" class="header">View
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="4">To view leave, specify the particulars and click View.
                          </TD>
                        </TR>
                        <TR>
                          <TD class="short">Report Type:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>
                            <SELECT tabindex="10" name="Leave" class="standard">
                              <OPTION value="">< ... ></OPTION>
                              <OPTION value="0">Graphic Report</OPTION>
                              <OPTION value="1">Summary</OPTION>';
                              if ($_SESSION['cAuth'] & 64)
                              {
                                echo '<OPTION value="2">Summary AFB</OPTION>';
                              }
                      echo '</SELECT>
                          </TD>
                        </TR>';
                        if ($_SESSION['cAuth'] & 64)
                        {
                          echo '<TR>
                                  <TD>Staff Name:
                                  </TD>
                                  <TD>';
                                    BuildStaffSelector(11, 'Staff', 'standard', $_SESSION['cUID'], true);
                            echo '</TD>
                                </TR>';
                        }
                  echo '<TR>
                          <TD>Date Range:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                            BuildDaySelector(12, 'StartDay', "");
                            echo '&nbsp;';
                            BuildMonthSelector(13, 'StartMonth', "");
                            echo '&nbsp;';
                            BuildYearSelector(14, 'StartYear', "");
                            echo ' to ';
                            BuildDaySelector(15, 'EndDay', "");
                            echo '&nbsp;';
                            BuildMonthSelector(16, 'EndMonth', "");
                            echo '&nbsp;';
                            BuildYearSelector(17, 'EndYear', "");
                    echo '</TD>
                          <TD colspan="2" class="right">
                            <INPUT tabindex="18" name="Submit" type="submit" class="button" value="View" />                   
                          </TD>
                        </TR>
                      </FORM>
                    </TABLE> 
                  </DIV>'; 
          }       
          //////////////////////////////////////////////////////////////////////
        ?>
        <BR /><BR />
      </DIV>
    </DIV>
    <?php 
      // PHP SCRIPT ////////////////////////////////////////////////////////////
      BuildFooter();
      //////////////////////////////////////////////////////////////////////////
    ?>    
  </BODY>
</HTML>