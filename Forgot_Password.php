<?php
include 'Scripts/Include.php'
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3c.org/TR/1999/REC-html401-19991224/loose.dtd">
<HTML>
  <HEAD>
      
      <style>
          *{
              font-family: arial;
              padding: 0;
              margin: 0;
          }
          
          .Hide{
              display: none;
          }
          
          .red{
              color: red;
              padding: 1em;
          }
          
          .banner{
                padding: 0.5em;
                width: 100%; 
                text-align: center;
                background-color: #295885;
                color: #FFFFFF; 
          }
          
           .foot{
                position: fixed;
                bottom: 0;
                width: 100%; 
                line-height: 48px;
                text-align: right;
                background-color: #295885;
                color: #FFFFFF; 
          }
          lable{
              font-size: larger;
          }
          input{
            width: calc(100% - 240px );
            padding: 0.5em;
          }
          
          #display{
            text-align: center;
            padding-top: 10%;
          }
          
          #setPassword{
            text-align: left;
            padding-left: 250px;
            margin: auto;
            width: 40%;
           
          }
          #reset{
            width: 4em;
            padding: 0.5em;
            margin-top: 0.5em;
            margin-right: 0.5em;
            right: 6em;
          }
         
          
        </style>  
        
  </HEAD>
  <BODY> 

      <div class="banner"><H1>S4 Integration</H1></br><H3>Password Recovery</H3></div>
      <div id ="display"></div>
      <div class="foot"><span style="padding-right: 1em;">2009 to 2017 - S4 Integration</span></div>
      
    <script src="Scripts/jquery-1.9.1.min.js"></script> 
    <script>
        
        var url = "";
        var code = "";       
        var ID = "";
        var Staff_Code = "";
        
       $(document).ready(function() { 
         getAuth();
         
         $(document).on("click","#reset",function() {
                reset();
          });
         
       });
         
         
        function getAuth(){   
          try{    
                url = window.location.href;
                console.log(url); 
                var A = url.split('?',2);
                url = A[0].replace(/Forgot_Password/g,"index");
                code = A[1].substring(0,8);            
                ID = A[1].substring(8);
                if(code!== "" && ID!==""){
                    $.ajax({
                        url: "Handlers/Forgot_Password_Handler.php", //refer to the file
                        type: "POST", //send it through get method            
                        data: { //arguments
                            ID: ID,
                            Code: code
                        },
                        success: function(response) { 
                           console.log(response);
                           if(response==="expired"){
                               console.log("2");
                                 $('#display').html('This link has expired. Please make a new request on the <a href="'+url+'">home page</a>');
                           }
                           else if(response==="noMatch"){
                               console.log("3");
                               $('#display').html('You are unauthorized to view this page.');
                           }
                           else if(response==="used"){
                               console.log("3");
                               $('#display').html('This link has been used. Please make a new request on the <a href="'+url+'">home page</a> ');
                           }
                           else{
                              Staff_Code = response;
                              console.log(response);
                              $('#display').html('<div id ="setPassword"><lable for="newPassword">Enter New Password</lable></br><INPUT type="password" id ="newPassword"><span id="noText" class="Hide red">Please enter password</span></br><lable for="newPasswordR">Confirm New Password:</lable></br><INPUT type="password" id ="newPasswordR"><span id="noMatch" class="Hide red">Password does not match</span></br><INPUT type="button" id="reset" class="r" value="Reset"></br></div>');
                           }
                        },
                        error: function(xhr) {
                            console.log(xhr);
                        }
                    });          
                    
                    
                }
           }
          catch(err){
             return false;
            }
        } 
  
        function reset(){
            
             $('#noText').addClass('Hide');
             $('#noMatch').addClass('Hide');
            
            var newP = $('#newPassword').val();
            var newR =  $('#newPasswordR').val();
            
            if(newP===""){
                $('#noText').removeClass('Hide');
            }
            
           
            else if(newP === newR){    
                 
                 if(Staff_Code !== "" && ID !== ""){
                    $.ajax({
                        url: "Handlers/Forgot_Password_Handler.php", //refer to the file
                        type: "POST", //send it through get method            
                        data: { //arguments
                            Password: newP,
                            Staff_Code: Staff_Code,
                            ID: ID
                        },
                        success: function(response) { 
                           console.log(response);
                           if(response === "changed"){
                               window.location = url;
                           }
                        },
                        error: function(xhr) {
                            console.log(xhr);
                        }
                });       
                }
           }
            
            else{
                $('#noMatch').removeClass('Hide');
                console.log(newP + " : " + newR);
            }
            
        }
        
    </script>
      
  </BODY>
</HTML>
