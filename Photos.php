<?php
  // DETAILS ///////////////////////////////////////////////////////////////////
  //                                                                          //
  //                    Last Edited By: Gareth Ambrose                        //
  //                        Date: 12 May 2008                                 //
  //                                                                          //
  //////////////////////////////////////////////////////////////////////////////
  // This page allows users to manage and view photos.                        //
  //////////////////////////////////////////////////////////////////////////////
  
  include 'Scripts/Include.php';
  SetSettings();
  CheckAuthorisation('Photos.php');
  
  //////////////////////////////////////////////////////////////////////////////
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3c.org/TR/1999/REC-html401-19991224/loose.dtd">
<HTML>
  <HEAD>
    <?php 
      // PHP SCRIPT ////////////////////////////////////////////////////////////
      BuildHead('Photos');
    include ('Scripts/header.php');
      //////////////////////////////////////////////////////////////////////////
    ?>   
  </HEAD>
  <BODY> 
    <?php 
      // PHP SCRIPT ////////////////////////////////////////////////////////////
      BuildBanner();
      //////////////////////////////////////////////////////////////////////////
    ?>    
    <DIV class="contentcontainer">
      <?php 
        // PHP SCRIPT //////////////////////////////////////////////////////////          
        BuildMenu('Main', 'Photos.php');                            
        ////////////////////////////////////////////////////////////////////////
      ?>
      <DIV class="content">
        <BR /><BR />
        <?php
          // PHP SCRIPT ////////////////////////////////////////////////////////
          BuildMessageSet('Photo');
          //////////////////////////////////////////////////////////////////////    
        ?>  
        <?php 
          // PHP SCRIPT //////////////////////////////////////////////////////// 
          if (isset($_SESSION['AddPhoto']))
          {
            BuildContentHeader('Add Photo', "", "", false);
            echo '<DIV class="contentflow">
                    <P>Enter the details of the photos below. Ensure that all the required information is entered before submission and that this information is valid.</P>
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center" class="short">
                      <FORM method="post" action="Handlers/Photos_Handler.php" enctype="multipart/form-data">
                        <INPUT name="Type" type="hidden" value="Add">
                        <TR>
                          <TD colspan="2" class="header">Photo Details
                          </TD>
                        </TR>
                        <TR>
                          <TD class="short">Description:
                          </TD>
                          <TD>
                            <INPUT tabindex="1" name="Description" type="text" class="text long" maxlength="100" value="'.$_SESSION['AddPhoto'][0].'" />
                          </TD>
                        </TR>
                        <TR>
                          <TD>Photo:
                            <SPAN class="note">**
                            </SPAN>
                          </TD>
                          <TD>
                            <INPUT tabindex="2" name="Upload" type="file" class="file" value="'.$_SESSION['AddPhoto'][1].'" />
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="4" class="center">
                            <INPUT tabindex="3" name="Submit" type="submit" class="button" value="Submit" />   
                            <INPUT tabindex="4" name="Submit" type="submit" class="button" value="Cancel" />                  
                          </TD>
                        </TR>
                      </FORM>
                    </TABLE>  
                  </DIV>  
                  <DIV>
                    <BR />
                    <SPAN class="note">*
                    </SPAN>
                    These fields are required.
                    <BR />
                    <SPAN class="note">**
                    </SPAN>
                    Files are limited to a maximum of 5MB and only the following file types are accepted: <B>.bmp .gif .jpg .jpeg .png</B>.
                  </DIV>';
          } else
          if (isset($_SESSION['EditPhoto']))
          {
            $row = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM File WHERE File_ID = "'.$_SESSION['EditPhoto'][0].'"')); 
            BuildContentHeader('Edit Photo: '.$row['File_FileName'], "", "", false);
            echo '<DIV class="contentflow">
                    <P>Once you have made changes, ensure that you submit them otherwise they will not take effect.</P>
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center" class="short">
                      <FORM method="post" action="Handlers/Photos_Handler.php" enctype="multipart/form-data">
                        <INPUT name="Type" type="hidden" value="Edit" />
                        <TR>
                          <TD colspan="2" class="header">Photo Details
                          </TD>
                        </TR>
                        <TR>
                          <TD class="short">Description:
                          </TD>
                          <TD>
                            <INPUT tabindex="1" name="Description" type="text" class="text long" value="'.$_SESSION['EditPhoto'][1].'" maxlength="100" />
                          </TD>
                        </TR>
                        <TR>
                          <TD>File:
                            <SPAN class="note">**
                            </SPAN>
                          </TD>
                          <TD>
                            <INPUT tabindex="2" name="Upload" type="file" class="file" value="'.$_SESSION['EditPhoto'][2].'" />
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="2" class="center">
                            <INPUT tabindex="3" name="Submit" type="submit" class="button" value="Submit" />   
                            <INPUT tabindex="4" name="Submit" type="submit" class="button" value="Cancel" />                  
                          </TD>
                        </TR>
                      </FORM>
                    </TABLE>  
                  </DIV>  
                  <DIV>
                    <BR />
                    <SPAN class="note">*
                    </SPAN>
                    These fields are required.
                    <BR />
                    <SPAN class="note">**
                    </SPAN>
                    Files are limited to a maximum of 5MB and only the following file types are accepted: <B>.bmp .gif .jpg .jpeg .png</B>.
                  </DIV>';
          } else
          if (isset($_SESSION['RemovePhoto']))
          {
            $row = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM File WHERE File_ID = "'.$_SESSION['RemovePhoto'][0].'"')); 
            BuildContentHeader('Remove Photo: '.$row['File_FileName'], "", "", false);
            echo '<DIV class="contentflow">
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center" class="short">
                      <FORM method="post" action="Handlers/Photos_Handler.php">
                        <INPUT name="Type" type="hidden" value="Remove" />
                        <TR>
                          <TD colspan="4" class="header">Remove
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="4" class="center">Are you sure you wish to remove this photo?
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="2" class="center">
                            <INPUT tabindex="1" name="Submit" type="submit" class="button" value="Yes" />   
                            <INPUT tabindex="2" name="Submit" type="submit" class="button" value="No" />                  
                          </TD>
                        </TR>
                      </FORM>
                    </TABLE>  
                  </DIV>';
          } else
          {
              BuildContentHeader('Maintenance', "", "", false);
              echo '<DIV class="contentflow">
                      <P>New photos can be added by using the form below.';
                      if ($_SESSION['cAuth'] & 32)
                        echo ' Photos already listed can also be edited or removed.';
                echo '</P>
                      <BR /><BR />
                      <TABLE cellspacing="5" align="center" class="standard">
                        <FORM method="post" action="Handlers/Photos_Handler.php">
                          <INPUT name="Type" type="hidden" value="Maintain">
                          <TR>
                            <TD colspan="4" class="header">Add
                            </TD>
                          </TR>
                          <TR>
                            <TD colspan="3">To add a new photo, click Add and complete the form that is displayed.
                            </TD>
                            <TD class="right">
                              <INPUT tabindex="1" name="Submit" type="submit" class="button" value="Add" />                   
                            </TD>
                          </TR>';
                          if ($_SESSION['cAuth'] & 32)
                          {
                            echo '<TR>
                                    <TD colspan="4" class="header">Edit
                                    </TD>
                                  </TR>
                                  <TR>
                                    <TD colspan="4">To edit a photo, specify the particulars, click Edit and complete the form that is displayed.
                                    </TD>
                                  </TR>
                                  <TR>
                                    <TD class="short">File:
                                      <SPAN class="note">*
                                      </SPAN>
                                    </TD>
                                    <TD>';
                                      BuildFileSelector(1, 'EditPhoto', 'standard', "", '5', true);
                              echo '</TD>
                                    <TD colspan="2" class="right">
                                      <INPUT tabindex="2" name="Submit" type="submit" class="button" value="Edit" />                   
                                    </TD>
                                  </TR>
                                  <TR>
                                    <TD colspan="4" class="header">Remove
                                    </TD>
                                  </TR>
                                  <TR>
                                    <TD colspan="4">To remove a photo, specify the particulars, click Remove and confirm when prompted.
                                    </TD>
                                  </TR>
                                  <TR>
                                    <TD class="short">File:
                                      <SPAN class="note">*
                                      </SPAN>
                                    </TD>
                                    <TD>';
                                      BuildFileSelector(3, 'RemovePhoto', 'standard', "", '5', true);
                              echo '</TD>
                                    <TD colspan="2" class="right">
                                      <INPUT tabindex="4" name="Submit" type="submit" class="button" value="Remove" />                   
                                    </TD>
                                  </TR>';
                          }
                    echo '</FORM>
                      </TABLE>
                    </DIV>';
                    
            BuildContentHeader('Photos', "", "", true);
            $resultSet = ExecuteQuery('SELECT * FROM File WHERE File_Type = "5" ORDER BY File_Updated ASC');
            if (MySQL_Num_Rows($resultSet))
            {   
              echo '<DIV class="contentflow">
                      <P>These are the photos listed. To view a photo in full size, click on the image.</P>
                      <BR /><BR />
                      <TABLE cellspacing="5" align="center" class="long">
                        <TR>
                          <TD colspan="4" class="header">Photo Details
                          </TD>
                        </TR>
                        <TR>';
                        $count = 0;
                        $highlight = true;
                        while ($row = MySQL_Fetch_Array($resultSet))
                        {                          
                          if ($highlight)
                            $colour = 'rowA';
                          else
                            $colour = 'rowB'; 
                          
                          if (($count % 4 == 0) && ($count != 0))
                            echo '<TR>';
                          
                          echo '<TD class="'.$colour.' center"><A tabindex="-1" href="Files/Photos/'.$row['File_FileName'].'" class="image"><IMG src="Files/Photos/Thumb_'.$row['File_FileName'].'" alt="'.$row['File_DisplayName'].'" /></A>
                                </TD>';
                          
                          if ($count % 4 == 3)
                          {
                            $highlight = !$highlight;
                            echo '</TR>';            
                          }
                          $count++;
                        }
                        $countTemp = $count % 4;
                        for (; $countTemp < 4; $countTemp++)
                            echo '<TD class="'.$colour.' center">
                                  </TD>';
                  echo '</TR>
                      </TABLE>
                    </DIV>';
              } else  
                echo '<DIV class="contentflow">
                        <P>There are no photos listed.</P>
                     </DIV>';          
          }                
          //////////////////////////////////////////////////////////////////////
        ?>
        <BR /><BR />  
      </DIV>
    </DIV>
    <?php 
      // PHP SCRIPT ////////////////////////////////////////////////////////////
      BuildFooter();
      //////////////////////////////////////////////////////////////////////////
    ?>    
  </BODY>
</HTML>