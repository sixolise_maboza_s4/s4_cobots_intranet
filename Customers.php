<?php
  // DETAILS ///////////////////////////////////////////////////////////////////
  //                                                                          //
  //                    Last Edited By: Gareth Ambrose                        //
  //                        Date: 12 May 2008                                 //
  //                                                                          //
  //////////////////////////////////////////////////////////////////////////////
  // This page allows users to manage and view customers.                     //
  //////////////////////////////////////////////////////////////////////////////
  
  include 'Scripts/Include.php';
  SetSettings();
  CheckAuthorisation('Customers.php');
  
  //////////////////////////////////////////////////////////////////////////////
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3c.org/TR/1999/REC-html401-19991224/loose.dtd">
<HTML>
  <HEAD>
    <?php
      // PHP SCRIPT ////////////////////////////////////////////////////////////
      BuildHead('Customers');
    include ('Scripts/header.php');
      //////////////////////////////////////////////////////////////////////////
    ?>

      <SCRIPT>
         $(document).ready(function() {
             var pos=4;
             var posArray =[];
             $('.removeRow').each(function() {
                  posArray.push(parseInt($(this).attr('rel')));
             });
            $("#addNew").on("click",function(){
                var iDuplicate = true;
                while(iDuplicate)
                {
                    if(posArray.indexOf(pos) < 0)
                        iDuplicate = false;
                    else
                        pos++;
                        
                }
               var ap= '<TR><TD class="center bold" style="text-indent: 50px;"><INPUT  name="text'+pos+'" type="text" class="text standard" value="" /> :'+
                             '</TD><TD class="center"><INPUT name="text'+pos+'Value" type="text" class="text veryshort right" maxlength="6" value="" /> %'+
                             '</TD><TD class="center"><a class="btn-tiny btn removeRow" rel="'+pos+'"><img src="Images/remove.png" style="border:none;cursor:pointer;width:15px; height:15px;"/> Remove</a>'+
                             '</TD></TR>';
              $('#paymentTerms > tbody:last-child').append(ap);
              posArray.push(pos);
              pos++;
            });
            $("#paymentTerms").on("click",".removeRow",function(){

               var relValue = parseInt($(this).attr("rel"));
               posArray.splice(posArray.indexOf(relValue), 1 );
               $(this).closest('tr').remove();
            });
            $('#fakeSubmit').on('click',function(){
                $.post( "Customers.php", { p_payment:'Add','terms[]': posArray },function(response){
                    $('#realSubmit').click();
                });
            });
            $('#fakeEdit').on('click',function(){
                $.post( "Customers.php", { p_payment:'Edit','terms[]': posArray },function(response){
                    $('#realSubmit').click();
                });
            });
            
            $("#customeTextArea").on("keyup",function(event){
                $(this).css("height","0px");
                var sh = $(this).prop("scrollHeight");
                var minh = $(this).css("min-height").replace("px", "");
                $(this).css("height",Math.max(sh,minh)+"px"); 
                var keycode = (event.keyCode ? event.keyCode : event.which);
                if(keycode == '13')
                    document.getElementById('customeTextArea').value +='- ';
                
                var txtval = $("#customeTextArea").val();
                if(txtval.substr(txtval.length - 1) == '\n'){
                        $("#customeTextArea").val(txtval.substring(0,txtval.length - 1));
                }

             });
            $("#customeTextArea").keyup();
         });
      </SCRIPT>
      <STYLE>
          .btn.btn-tiny, .btn-group-tiny>.btn {
            font-size: 11px;
            padding: 2px 6px;
            line-height: 16px;
            margin: 2px;
          }
          .btn {
            display: inline-block;
            margin-bottom: 0;
            font-weight: normal;
            text-align: center;
            vertical-align: middle;
            cursor: pointer;
            background-image: none;
            border: 1px solid transparent;
            white-space: nowrap;
            padding: 6px 12px;
            font-size: 13px;
            line-height: 18px;
            border-radius: 4px;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            -o-user-select: none;
            user-select: none;
            color: #444444;
            background-color: #fff;
            border-color: #ccc;
            text-shadow: none;
          }
          a#addNew,a.removeRow{ text-decoration: none; }
      </STYLE>
  </HEAD>
  <BODY> 
    <?php
      // PHP SCRIPT ////////////////////////////////////////////////////////////
      BuildBanner();
      //////////////////////////////////////////////////////////////////////////
    ?>
    <DIV id="main">
      <?php 
        // PHP SCRIPT //////////////////////////////////////////////////////////
        BuildTopBar();
        BuildMenu('Main', 'Customers.php');                               
        ////////////////////////////////////////////////////////////////////////
      ?>
        <section id="content_wrapper">
            <?php BuildBreadCrumb($currentPath);?>
            <!-- -------------- Content -------------- -->
            <section id="content" class="table-layout">
                <!-- -------------- Column Center -------------- -->
                <div class="chute chute-center" style="height: 869px;">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel">
        <?php
          // PHP SCRIPT ////////////////////////////////////////////////////////
          BuildMessageSet('Customer');
          //////////////////////////////////////////////////////////////////////    
        ?>      
        <?php
          // PHP SCRIPT ////////////////////////////////////////////////////////
          if(isset($_POST['p_payment']))
          {
              $_SESSION[$_POST['p_payment'].'Customer'][21] = $_POST['terms'];
              exit;
          }
          else
          if (isset($_SESSION['AddCustomer']))
          {
            BuildContentHeader('Add Customer', "", "", true);
            echo '<DIV class="contentflow">
                    <P>Enter the details of the customer below. Ensure that all the required information is entered before submission and that this information is valid.</P>
                    <BR /><BR />
                    <FORM method="post" action="Handlers/Customers_Handler.php">
                    <TABLE cellspacing="5" align="center" class="long">
                        <INPUT name="Type" type="hidden" value="Add">
                        <TR>
                          <TD colspan="4" class="header">Company Details
                          </TD>
                        </TR>
                        <TR>
                          <TD class="short">Company Name:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>
                            <INPUT tabindex="4" name="CompanyName" type="text" class="text standard" maxlength="100" value="'.$_SESSION['AddCustomer'][0].'" />
                          </TD>
                          <TD class="short">Description of Supply:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD rowspan="4">
                            <TEXTAREA tabindex="8" name="Description" class="standard" maxlength="100">'.$_SESSION['AddCustomer'][1].'</TEXTAREA>
                          </TD>
                        </TR>
                        <TR>
                          <TD>Payment Method:
                          </TD>
                          <TD>';
                            BuildPaymentMethod(5, 'Payment',  $_SESSION['AddCustomer'][2], 'standard');
                    echo '</TD>
                        </TR>
                        <TR>
                          <TD class="short">Account Number:
                          </TD>
                          <TD>
                            <INPUT tabindex="6" name="Account" type="text" class="text short" maxlength="15" value="'.$_SESSION['AddCustomer'][3].'" />
                          </TD>
                        </TR>
                        <TR>
                          <TD>Company Prefix:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>
                            <INPUT tabindex="7" name="Prefix" type="text" class="veryshort" maxlength="3" value="'.$_SESSION['AddCustomer'][4].'" />
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="4" class="header">Contact Details
                          </TD>
                        </TR>
                        <TR>
                          <TD>Contact Person:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>
                            <INPUT tabindex="8" name="Contact" type="text" class="text standard" maxlength="100" value="'.$_SESSION['AddCustomer'][5].'" />
                          </TD>
                          <TD>Contact Number (Work):
                            <SPAN class="note">**
                            </SPAN>
                          </TD>
                          <TD>
                            <INPUT tabindex="10" name="ContactWork" type="text" class="text short" value="'.$_SESSION['AddCustomer'][6].'" maxlength="15" />
                          </TD>
                        </TR>
                        <TR>
                          <TD>Email address:
                          </TD>
                          <TD>
                            <INPUT tabindex="9" name="Email" type="text" class="text standard" maxlength="50" value="'.$_SESSION['AddCustomer'][7].'" />
                          </TD>
                          <TD>Contact Number (Fax):
                            <SPAN class="note">**
                            </SPAN>
                          </TD>
                          <TD>
                            <INPUT tabindex="11" name="ContactFax" type="text" class="text short" value="'.$_SESSION['AddCustomer'][8].'" maxlength="15" />
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="4" class="header">Physical Address Details
                          </TD>
                        </TR>
                        <TR>
                          <TD>Street 1:
                          </TD>
                          <TD>
                            <INPUT tabindex="12" name="PhysicalStreet1" type="text" class="text standard" maxlength="50" value="'.$_SESSION['AddCustomer'][9].'" />
                          </TD>
                          <TD>Town / City:
                          </TD>
                          <TD>
                            <INPUT tabindex="15" name="PhysicalTownCity" type="text" class="text standard" maxlength="20" value="'.$_SESSION['AddCustomer'][10].'" />
                          </TD>
                        </TR>
                        <TR>
                          <TD>Street 2:
                          </TD>
                          <TD>
                            <INPUT tabindex="13" name="PhysicalStreet2" type="text" class="text standard" maxlength="50" value="'.$_SESSION['AddCustomer'][11].'" />
                          </TD>
                          <TD>Country:
                          </TD>
                          <TD>
                            <INPUT tabindex="16" name="PhysicalCountry" type="text" class="text standard" maxlength="20" value="'.$_SESSION['AddCustomer'][12].'" />
                          </TD>
                        </TR>
                        <TR>
                          <TD>Suburb:
                          </TD>
                          <TD>
                            <INPUT tabindex="14" name="PhysicalSuburb" type="text" class="text standard" maxlength="50" value="'.$_SESSION['AddCustomer'][13].'" />
                          </TD>
                          <TD>Postal Code:
                          </TD>
                          <TD>
                            <INPUT tabindex="17" name="PhysicalPostalCode" type="text" class="text veryshort" maxlength="6" value="'.$_SESSION['AddCustomer'][14].'" />
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="4" class="header">Postal Address Details
                          </TD>
                        </TR>
                        <TR>
                          <TD>Street 1:
                          </TD>
                          <TD>
                            <INPUT tabindex="18" name="PostalStreet1" type="text" class="text standard" maxlength="50" value="'.$_SESSION['AddCustomer'][15].'" />
                          </TD>
                          <TD>Town / City:
                          </TD>
                          <TD>
                            <INPUT tabindex="21" name="PostalTownCity" type="text" class="text standard" maxlength="20" value="'.$_SESSION['AddCustomer'][16].'" />
                          </TD>
                        </TR>
                        <TR>
                          <TD>Street 2:
                          </TD>
                          <TD>
                            <INPUT tabindex="19" name="PostalStreet2" type="text" class="text standard" maxlength="50" value="'.$_SESSION['AddCustomer'][17].'" />
                          </TD>
                          <TD>Country:
                          </TD>
                          <TD>
                            <INPUT tabindex="22" name="PostalCountry" type="text" class="text standard" maxlength="20" value="'.$_SESSION['AddCustomer'][18].'" />
                          </TD>
                        </TR>
                        <TR>
                          <TD>Suburb:
                          </TD>
                          <TD>
                            <INPUT tabindex="20" name="PostalSuburb" type="text" class="text standard" maxlength="50" value="'.$_SESSION['AddCustomer'][19].'" />
                          </TD>
                          <TD>Postal Code:
                          </TD>
                          <TD>
                            <INPUT tabindex="23" name="PostalPostalCode" type="text" class="text veryshort" maxlength="6" value="'.$_SESSION['AddCustomer'][20].'" />
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="4" class="header">Payment Details
                          </TD>
                        </TR>
                        <TR>
                          <TD class="short bold" colspan="5">ABBREVIATED TERMS AND CONDITIONS:
                          </TD>
                        </TR>
                        <TR>
                          <TD class="short" colspan="5"><INPUT  name="TermsCon" type="text" style="width: 90%; border-width: 0.5px;" class="text" value="'.$_SESSION['AddCustomer'][28].'" />
                          </TD>
                        </TR>
                        <TR>
                          <TD class="short" colspan="5">
                             <UL style="margin: 0; padding: 0; list-style-type: none;">
                                 <li style="text-indent: 3px; margin: 2px 0;">- Delivery will be within <INPUT  name="Delivery" type="text" style="width: 40px;" class="text center" value="'.$_SESSION['AddCustomer'][26].'" /> working days from project commencement.</li><li style="text-indent: 3px; margin: 2px 0;">- Prices are valid for <INPUT  name="Valid" type="text" style="width: 40px;" class="text center" value="'.$_SESSION['AddCustomer'][25].'" /> working days.</li>
                             </UL>
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="5">
                            <TEXTAREA tabindex="8" style="width:90%;max-height:170px;" name="conditions" id="customeTextArea" class="standard">'.$_SESSION['AddCustomer'][23].'</TEXTAREA>
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="3">Payment Terms(&nbsp;&nbsp;<INPUT  name="DaysFromInvoice" style="width: 40px;" type="text" class="text center" value="'.$_SESSION['AddCustomer'][24].'" /> days from <input type="radio" name="InvoiceStatement" value="invoice"'.($_SESSION['AddCustomer'][27] == "invoice" ? "checked" : "").'>invoice<input type="radio" name="InvoiceStatement" value="statement"'.($_SESSION['AddCustomer'][27] != "invoice"? "checked" : "").'> statement):<SPAN class="note">*
                            </SPAN></TD>
                           <TD colspan="2"><a id="addNew" class="btn-tiny btn"><img src="Images/add_new.png" style="border:none;cursor:pointer;width:15px; height:15px;"/>
                               Add</a></TD>
                        </TR>
                        <TR>
                          <TD colspan="5">
                            <TABLE style="border:none;" cellspacing="10" id="paymentTerms">
                            <TBODY>';
                            if(!isset($_SESSION['AddCustomer'][22])){
                                echo '<TR>
                                           <TD class="center bold" style="text-indent: 50px;"><INPUT  name="text1" type="text" class="text standard" value="On Commissioning" /> :
                                           </TD>
                                           <TD class="center"><INPUT name="text1Value" type="text" class="text veryshort right" maxlength="6" value="100" /> %
                                           </TD>
                                           <TD class="center"><a class="btn-tiny btn removeRow" rel="1"><img src="Images/remove.png" style="border:none; cursor:pointer;width:15px; height:15px;"/>
                                           Remove</a>
                                           </TD>
                                       </TR>
                                       <TR>
                                           <TD class="center bold" style="text-indent: 50px;"><INPUT  name="text2" type="text" class="text standard" value="On Delivery" /> :
                                           </TD>
                                           <TD class="center"><INPUT name="text2Value" type="text" class="text veryshort right" maxlength="6" value="0" /> %
                                           </TD>
                                           <TD class="center"><a class="btn-tiny btn removeRow" rel="2"><img src="Images/remove.png" style="border:none;cursor:pointer;width:15px; height:15px;"/>
                                           Remove</a>
                                           </TD>
                                       </TR>
                                       <TR>
                                           <TD class="center bold" style="text-indent: 50px;"><INPUT  name="text3" type="text" class="text standard" value="On Order" /> :
                                           </TD>
                                           <TD class="center"><INPUT name="text3Value" type="text" class="text veryshort right" maxlength="6" value="0" /> %
                                           </TD>
                                           <TD class="center"><a class="btn-tiny btn removeRow" rel="3"><img src="Images/remove.png" style="border:none;cursor:pointer;width:15px; height:15px;"/>
                                           Remove</a>
                                           </TD>
                                       </TR>';
                            }
                            else
                            {
                                for($i=1; $i <= sizeof($_SESSION['AddCustomer'][22]); $i++)
                                {
                                echo '<TR>
                                           <TD class="center" style="text-indent: 50px;"><INPUT  name="text'.$i.'" type="text" class="text standard" value="'.$_SESSION['AddCustomer'][22][$i-1][0].'" />
                                           </TD>
                                           <TD class="center"><INPUT name="text'.$i.'Value" type="text" class="text veryshort right" maxlength="6" value="'.$_SESSION['AddCustomer'][22][$i-1][1].'" /> %
                                           </TD>
                                           <TD class="center"><a class="btn-tiny btn removeRow" rel="'.$i.'"><img src="Images/remove.png" style="border:none; cursor:pointer;width:15px; height:15px;"/>
                                               Remove</a>
                                           </TD>
                                       </TR>';
                                }
                            }
                        echo '</TBODY>
                            </TABLE>
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="5" class="center">
                            <INPUT tabindex="24" name="fakeSubmit" id="fakeSubmit" type="button" class="button" value="Submit" />  
                            <INPUT style="display:none;" tabindex="24" id="realSubmit" name="Submit" type="submit" class="button" value="Submit" />   
                            <INPUT tabindex="25" name="Submit" type="submit" class="button" value="Cancel" />                   
                          </TD>
                        </TR>  
                    </TABLE>  
                    </FORM>
                  </DIV>  
                  <DIV>
                    <BR />
                    <SPAN class="note">*
                    </SPAN>
                    These fields are required.
                    <BR />
                    <SPAN class="note">**
                    </SPAN>
                    These fields must be completed as far as possible and the international number format must be used. Eg: +27-41-1234567.
                  </DIV>';
          } else
          if (isset($_SESSION['EditCustomer']))
          { 
            $row = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Customer WHERE Customer_Code = "'.$_SESSION['EditCustomer'][0].'"'));
            BuildContentHeader('Edit Customer - '.$row['Customer_Name'], "", "", true);
            echo '<DIV class="contentflow">
                    <P>Once you have made changes, ensure that you submit them otherwise they will not take effect.<P>
                    <BR /><BR />
                    <FORM method="post" action="Handlers/Customers_Handler.php">
                    <TABLE cellspacing="5" align="center" class="long">
                        <INPUT name="Type" type="hidden" value="Edit">
                        <TR>
                          <TD colspan="4" class="header">Company Details
                          </TD>
                        </TR>
                        <TR>
                          <TD class="short">Company Name:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>
                            <INPUT tabindex="5" name="CompanyName" type="text" class="text standard" value="'.$_SESSION['EditCustomer'][1].'" maxlength="100" />
                          </TD>
                          <TD class="short">Description of Supply:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD rowspan="4">
                            <TEXTAREA tabindex="8" name="Description" class="standard" maxlength="100">'.$_SESSION['EditCustomer'][2].'</TEXTAREA>
                          </TD>
                        </TR>
                        <TR>
                          <TD>Payment Method:
                          </TD>
                          <TD>';
                            BuildPaymentMethod(6, 'Payment', $_SESSION['EditCustomer'][3], 'standard');
                    echo '</TD>
                        </TR>
                        <TR>
                          <TD class="short">Account Number:
                          </TD>
                          <TD>
                            <INPUT tabindex="7" name="Account" type="text" class="text short" value="'.$_SESSION['EditCustomer'][4].'" maxlength="15" />
                          </TD>
                        </TR>
                        <TR>
                          <TD>&nbsp;
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="4" class="header">Contact Details
                          </TD>
                        </TR>
                        <TR>
                          <TD>Contact Person:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>
                            <INPUT tabindex="8" name="Contact" type="text" class="text standard" value="'.$_SESSION['EditCustomer'][5].'" maxlength="100" />
                          </TD>
                          <TD>Contact Number (Work):
                            <SPAN class="note">**
                            </SPAN>
                          </TD>
                          <TD>
                            <INPUT tabindex="10" name="ContactWork" type="text" class="text short"';                     
                            if (($_SESSION['EditCustomer'][6] == "-") || ($_SESSION['EditCustomer'][6] == "") || ($_SESSION['EditCustomer'][6] == "--"))
                              echo ' value="+__-__-_______"';
                            else
                              echo ' value="'.$_SESSION['EditCustomer'][6].'"';
                    echo ' maxlength="15" />
                          </TD>
                        </TR>
                        <TR>
                          <TD>Email address:
                          </TD>
                          <TD>
                            <INPUT tabindex="9" name="Email" type="text" class="text standard" value="'.$_SESSION['EditCustomer'][7].'" maxlength="50" />
                          </TD>
                          <TD>Contact Number (Fax):
                            <SPAN class="note">**
                            </SPAN>
                          </TD>
                          <TD>
                            <INPUT tabindex="11" name="ContactFax" type="text" class="text short"';                    
                            if (($_SESSION['EditCustomer'][8] == "-") || ($_SESSION['EditCustomer'][8] == "") || ($_SESSION['EditCustomer'][8] == "--"))
                              echo ' value="+__-__-_______"';
                            else
                              echo ' value="'.$_SESSION['EditCustomer'][8].'"';
                    echo ' maxlength="15" />
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="4" class="header">Physical Address Details
                          </TD>
                        </TR>
                        <TR>
                          <TD>Street 1:
                          </TD>
                          <TD>
                            <INPUT tabindex="12" name="PhysicalStreet1" type="text" class="text standard" value="'.$_SESSION['EditCustomer'][9].'" maxlength="50" />
                          </TD>
                          <TD>Town / City:
                          </TD>
                          <TD>
                            <INPUT tabindex="15" name="PhysicalTownCity" type="text" class="text standard" value="'.$_SESSION['EditCustomer'][10].'" maxlength="20" />
                          </TD>
                        </TR>
                        <TR>
                          <TD>Street 2:
                          </TD>
                          <TD>
                            <INPUT tabindex="13" name="PhysicalStreet2" type="text" class="text standard" value="'.$_SESSION['EditCustomer'][11].'" maxlength="50" />
                          </TD>
                          <TD>Country:
                          </TD>
                          <TD>
                            <INPUT tabindex="16" name="PhysicalCountry" type="text" class="text standard" value="'.$_SESSION['EditCustomer'][12].'" maxlength="20" />
                          </TD>
                        </TR>
                        <TR>
                          <TD>Suburb:
                          </TD>
                          <TD>
                            <INPUT tabindex="14" name="PhysicalSuburb" type="text" class="text standard" value="'.$_SESSION['EditCustomer'][13].'" maxlength="50" />
                          </TD>
                          <TD>Postal Code:
                          </TD>
                          <TD>
                            <INPUT tabindex="17" name="PhysicalPostalCode" type="text" class="text veryshort" value="'.$_SESSION['EditCustomer'][14].'" maxlength="6" />
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="4" class="header">Postal Address Details
                          </TD>
                        </TR>
                        <TR>
                          <TD>Street 1:
                          </TD>
                          <TD>
                            <INPUT tabindex="18" name="PostalStreet1" type="text" class="text standard" value="'.$_SESSION['EditCustomer'][15].'" maxlength="50" />
                          </TD>
                          <TD>Town / City:
                          </TD>
                          <TD>
                            <INPUT tabindex="21" name="PostalTownCity" type="text" class="text standard" value="'.$_SESSION['EditCustomer'][16].'" maxlength="20" />
                          </TD>
                        </TR>
                        <TR>
                          <TD>Street 2:
                          </TD>
                          <TD>
                            <INPUT tabindex="19" name="PostalStreet2" type="text" class="text standard" value="'.$_SESSION['EditCustomer'][17].'" maxlength="50" />
                          </TD>
                          <TD>Country:
                          </TD>
                          <TD>
                            <INPUT tabindex="22" name="PostalCountry" type="text" class="text standard" value="'.$_SESSION['EditCustomer'][18].'" maxlength="20" />
                          </TD>
                        </TR>
                        <TR>
                          <TD>Suburb:
                          </TD>
                          <TD>
                            <INPUT tabindex="20" name="PostalSuburb" type="text" class="text standard" value="'.$_SESSION['EditCustomer'][19].'" maxlength="50" />
                          </TD>
                          <TD>Postal Code:
                          </TD>
                          <TD>
                            <INPUT tabindex="23" name="PostalPostalCode" type="text" class="text veryshort" value="'.$_SESSION['EditCustomer'][20].'" maxlength="6" />
                          </TD>
                        </TR>
                        
                       <TR>
                          <TD colspan="4" class="header">Payment Details
                          </TD>
                        </TR>
                        <TR>
                          <TD class="short bold" colspan="5">ABBREVIATED TERMS AND CONDITIONS:
                          </TD>
                        </TR>
                        <TR>
                          <TD class="short" colspan="5"><INPUT  name="TermsCon" type="text" style="width: 90%; border-width: 0.5px;" class="text" value="'.$_SESSION['EditCustomer'][28].'" />
                          </TD>
                        </TR>
                        <TR>
                          <TD class="short" colspan="5">
                             <UL style="margin: 0; padding: 0; list-style-type: none;">
                                 <li style="text-indent: 3px; margin: 2px 0;">- Delivery will be within <INPUT  name="Delivery" type="text" style="width: 40px;" class="text center" value="'.$_SESSION['EditCustomer'][26].'" /> working days from project commencement.</li><li style="text-indent: 3px; margin: 2px 0;">- Prices are valid for <INPUT  name="Valid" type="text" style="width: 40px;" class="text center" value="'.$_SESSION['EditCustomer'][25].'" /> working days.</li>
                             </UL>
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="5">
                            <TEXTAREA tabindex="8" style="width:90%;max-height:170px;" name="conditions" id="customeTextArea" class="standard">'.$_SESSION['EditCustomer'][23].'</TEXTAREA>
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="3">Payment Terms(&nbsp;&nbsp;<INPUT  name="DaysFromInvoice" type="text" style="width: 40px;" class="text center" value="'.$_SESSION['EditCustomer'][24].'" /> days from <input type="radio" name="InvoiceStatement" value="invoice"'.($_SESSION['EditCustomer'][27] == "invoice" ? " checked" : "").'>invoice<input type="radio" name="InvoiceStatement" value="statement"'.($_SESSION['EditCustomer'][27] != "invoice"? " checked" : "").'>statement):</TD>
                           <TD colspan="2"><a id="addNew" class="btn-tiny btn"><img src="Images/add_new.png" style="border:none;cursor:pointer;width:15px; height:15px;"/>
                           Add</a></TD>
                        </TR>
                        <TR>
                          <TD colspan="4">
                            <TABLE style="border:none;" cellspacing="10" id="paymentTerms">
                            <TBODY>';
                            if(!isset($_SESSION['EditCustomer'][22])){
                                echo '<TR>
                                           <TD class="center bold" style="text-indent: 50px;"><INPUT  name="text1" type="text" class="text standard" value="On Commissioning" /> :
                                           </TD>
                                           <TD class="center"><INPUT name="text1Value" type="text" class="text veryshort right" maxlength="6" value="100" /> %
                                           </TD>
                                           <TD class="center"><a class="btn-tiny btn removeRow" rel="1"><img src="Images/remove.png" style="border:none; cursor:pointer; width:15px; height:15px;"/>
                                           Remove</a>
                                           </TD>
                                       </TR>
                                       <TR>
                                           <TD class="center bold" style="text-indent: 50px;"><INPUT  name="text2" type="text" class="text standard" value="On Delivery" /> :
                                           </TD>
                                           <TD class="center"><INPUT name="text2Value" type="text" class="text veryshort right" maxlength="6" value="0" /> %
                                           </TD>
                                           <TD class="center"><a class="btn-tiny btn removeRow" rel="2"><img src="Images/remove.png" style="border:none;cursor:pointer;width:15px; height:15px;"/>
                                           Remove</a>
                                           </TD>
                                       </TR>
                                       <TR>
                                           <TD class="center bold" style="text-indent: 50px;"><INPUT  name="text3" type="text" class="text standard" value="On Order" /> :
                                           </TD>
                                           <TD class="center"><INPUT name="text3Value" type="text" class="text veryshort right" maxlength="6" value="0" /> %
                                           </TD>
                                           <TD class="center"><a class="btn-tiny btn removeRow" rel="3"><img src="Images/remove.png" style="border:none;cursor:pointer;width:15px; height:15px;"/>
                                           Remove</a>
                                           </TD>
                                       </TR>';
                            }
                            else
                            {
                                for($i=1; $i <= sizeof($_SESSION['EditCustomer'][22]); $i++)
                                {
                                echo '<TR>
                                           <TD class="center bold" style="text-indent: 50px;"><INPUT  name="text'.$i.'" type="text" class="text standard" value="'.$_SESSION['EditCustomer'][22][$i-1][0].'" /> :
                                           </TD>
                                           <TD class="center"><INPUT name="text'.$i.'Value" type="text" class="text veryshort right" maxlength="6" value="'.$_SESSION['EditCustomer'][22][$i-1][1].'" /> %
                                           </TD>
                                           <TD class="center"><a class="btn-tiny btn removeRow" rel="'.$i.'"><img src="Images/remove.png" style="border:none; cursor:pointer;width:15px; height:15px;"/> Remove</a>
                                           </TD>
                                       </TR>';
                                }
                            }
                        echo '</TBODY>
                            </TABLE>
                          </TD>
                        </TR>

                        <TR>
                          <TD colspan="5" class="center">
                            <INPUT tabindex="24" name="fakeEdit" id="fakeEdit" type="button" class="button" value="Submit" /> 
                            <INPUT tabindex="24" name="Submit" id="realSubmit" style="display:none" type="submit" class="button" value="Submit" />   
                            <INPUT tabindex="25" name="Submit" type="submit" class="button" value="Cancel" />                    
                          </TD>
                        </TR>
                    </TABLE>  
                    </FORM>
                  </DIV>  
                  <DIV>
                    <BR />
                    <SPAN class="note">*
                    </SPAN>
                    These fields are required.
                    <BR />
                    <SPAN class="note">**
                    </SPAN>
                    These fields must be completed as far as possible and the international number format must be used. Eg: +27-41-1234567.
                  </DIV>';
          } else
          if (isset($_SESSION['ViewCustomer']))
          {
            $row = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Customer WHERE Customer_Code = '.$_SESSION['ViewCustomer'][0].''));
            BuildContentHeader('View Customer - '.$row['Customer_Name'], "", "", true);
            echo '<DIV class="contentflow">
                    <P>These are the customer details.<P>
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center" class="standard">';
                    if ($_SESSION['cAuth'] & 16)
                    {
                      echo '<TR>
                              <TD colspan="4" class="header">Company Details
                              </TD>
                            </TR>
                            <TR>
                              <TD>Payment Method:
                              </TD>
                              <TD class="bold">';
                                $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT Payment_Description FROM Payment WHERE Payment_ID = "'.$row['Customer_Payment_Method'].'"'));
                                echo $rowTemp['Payment_Description'];                                  
                        echo '</TD>
                              <TD>Account Number:
                              </TD>
                              <TD class="bold">'.$row['Customer_Account'].'
                              </TD>
                            </TR>
                            <TR>
                              <TD>Customer Prefix:
                              </TD>
                              <TD class="bold">'.$row['Customer_Prefix'].'
                              </TD>
                            </TR>';
                      }
                echo '<TR>
                        <TD colspan="4" class="header">Contact Details
                        </TD>
                      </TR>
                      <TR>
                        <TD class="short">Contact Person:
                        </TD>
                        <TD class="bold">'.$row['Customer_Contact'].'
                        </TD>
                        <TD class="short">Contact Number (Work):
                        </TD>
                        <TD class="bold">'.$row['Customer_Phone'].'
                        </TD>
                      </TR>
                      <TR>
                        <TD>Email address:
                        </TD>
                        <TD class="bold">'.$row['Customer_Email'].'
                        </TD>
                        <TD>Contact Number (Fax):
                        </TD>
                        <TD class="bold">'.$row['Customer_Fax'].'
                        </TD>
                      </TR>
                      <TR>
                        <TD colspan="4" class="header">Physical Address Details
                        </TD>
                      </TR>
                      <TR>
                        <TD>Street 1:
                        </TD>
                        <TD class="bold">'.$row['Customer_Address_Street'].'
                        </TD>
                        <TD>Town / City:
                        </TD>
                        <TD class="bold">'.$row['Customer_Address_City'].'
                        </TD>
                      </TR>
                      <TR>
                        <TD>Street 2:
                        </TD>
                        <TD class="bold">'.$row['Customer_Address_Street2'].'
                        </TD>
                        <TD>Country:
                        </TD>
                        <TD class="bold">'.$row['Customer_Address_Country'].'
                        </TD>
                      </TR>
                      <TR>
                        <TD>Suburb:
                        </TD>
                        <TD class="bold">'.$row['Customer_Address_Suburb'].'
                        </TD>
                        <TD>Postal Code:
                        </TD>
                        <TD class="bold">'.$row['Customer_Address_Code'].'
                        </TD>
                      </TR>
                      <TR>
                        <TD colspan="4" class="header">Postal Address Details
                        </TD>
                      </TR>
                      <TR>
                        <TD>Street 1:
                        </TD>
                        <TD class="bold">'.$row['Customer_Postal_Street'].'
                        </TD>
                        <TD>Town / City:
                        </TD>
                        <TD class="bold">'.$row['Customer_Postal_City'].'
                        </TD>
                      </TR>
                      <TR>
                        <TD>Street 2:
                        </TD>
                        <TD class="bold">'.$row['Customer_Postal_Street2'].'
                        </TD>
                        <TD>Country:
                        </TD>
                        <TD class="bold">'.$row['Customer_Postal_Country'].'
                        </TD>
                      </TR>
                      <TR>
                        <TD>Suburb:
                        </TD>
                        <TD class="bold">'.$row['Customer_Postal_Suburb'].'
                        </TD>
                        <TD>Postal Code:
                        </TD>
                        <TD class="bold">'.$row['Customer_Postal_Code'].'
                        </TD>
                      </TR>
                      



                      
                      <TR>
                          <TD colspan="4" class="header">Payment Details
                          </TD>
                        </TR>
                        <TR>
                          <TD class="short bold" colspan="4">ABBREVIATED TERMS AND CONDITIONS:
                          </TD>
                        </TR>
                        <TR>
                          <TD class="short" colspan="4">'.$row['Customer_tcHeader'].'
                          </TD>
                        </TR>
                        <TR>
                          <TD class="short" colspan="4">
                             <UL style="margin: 0; padding: 0; list-style-type: none;">
                                 <li style="text-indent: 3px; margin: 2px 0;">- Delivery will be within '.$row['Customer_DeliveryDays'].' working days from project commencement.</li><li style="text-indent: 3px; margin: 2px 0;">- Prices are valid for '.$row['Customer_PriceValid'].' working days.</li>';
                                 $_SESSION['ViewCustomer'][2] = ($row['Customer_TermsConditions'] != "") ? $row['Customer_TermsConditions'] : $_SESSION['ViewCustomer'][2];
                                 $tcArray = explode(PHP_EOL, $_SESSION['ViewCustomer'][2]);
                                  foreach ($tcArray as $value) {
                                    echo '<li style="text-indent: 3px; margin: 2px 0;">'.$value.'</li>';
                                  }
             
                          echo ' </UL>
                          </TD>
                        </TR>
                        <TR style="margin-top:5px;">
                          <TD colspan="4">
                             <UL style="margin: 0; padding: 0; list-style-type: none;">
                                <li style="text-indent: 3px; margin: 2px 0;">- Payment Terms( '.$row['Customer_DaysFromInvoice'].' days from '.$row['Customer_InvoiceStatement'].'):</li>
                                 <li>
                                     <UL style="margin: 0; padding: 0; list-style-type: none;">';
                                      foreach ($_SESSION['ViewCustomer'][1] as $key => $value) {
                                          echo '<li style="text-indent: 40px; margin: 2px 0; display: inline-block; width:30%;">'.$_SESSION['ViewCustomer'][1][$key][0].': </li><li style="text-indent: 3px; margin: 2px 0;display: inline-block; width:5%; text-align:right;">'.$_SESSION['ViewCustomer'][1][$key][1].'</li><li style="text-indent: 3px; margin: 2px 0;display: inline-block; width:65%;"></li>';
                                      }       
                               echo '</UL>
                                 </li>
                             </UL>
                          </TD>
                        </TR>
                    </TABLE>  
                  </DIV>';
            Session_Unregister('ViewCustomer');
          } else
          {

           if (FeatureExceptions('Customers','',$_SESSION['cUID']))
            {
              BuildContentHeader('Maintenance', "", "", true);
              echo '<DIV class="contentflow">
                      <P>New customers can be added using the form below. Details on customers already listed can also be edited.</P>
                      <BR /><BR />                
                      <TABLE cellspacing="5" align="center" class="standard">
                        <FORM method="post" action="Handlers/Customers_Handler.php">
                          <INPUT name="Type" type="hidden" value="Maintain">
                          <TR>
                            <TD colspan="4" class="header">Add
                            </TD>
                          </TR>
                          <TR>
                            <TD colspan="3">To add a new customer, click Add and complete the form that is displayed.
                            </TD>
                            <TD class="right">
                              <INPUT tabindex="1" name="Submit" type="submit" class="button" value="Add" />
                            </TD>
                          </TR>
                          <TR>
                            <TD colspan="4" class="header">Edit
                            </TD>
                          </TR>
                          <TR>
                            <TD colspan="4">To edit a customer, specify the particulars and click Edit.
                            </TD>
                          </TR>
                          <TR>
                            <TD class="short"">Customer:
                              <SPAN class="note">*
                              </SPAN>
                            </TD>
                            <TD>';
                              BuildCustomerSelector(2, 'Customer', 'standard', "");
                      echo '</TD>
                            <TD colspan="2" class="right">
                              <INPUT tabindex="3" name="Submit" type="submit" class="button" value="Edit" />                   
                            </TD>
                          </TR>
                        </FORM>
                      </TABLE>
                    </DIV>';
              
              BuildContentHeader('View Customers', "", "", true);
            } else
              BuildContentHeader('View Customers', "", "", true);
            
            echo '<DIV class="contentflow">
                    <P>Customers can be viewed by using the form below.</P>
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center" class="standard">
                      <FORM method="post" action="Handlers/Customers_Handler.php">
                        <INPUT name="Type" type="hidden" value="View">
                        <TR>
                          <TD colspan="4" class="header">View
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="4">To view customers, specify the particulars and click View.
                          </TD>
                        </TR>
                        <TR>
                          <TD class="short">Customer:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                            BuildCustomerSelector(4, 'Customer', 'standard', "");
                    echo '</TD>
                          <TD colspan="2" class="right">
                            <INPUT tabindex="5" name="Submit" type="submit" class="button" value="View" />                   
                          </TD>
                        </TR>
                      </FORM>
                    </TABLE>
                  </DIV>';
          }
          //////////////////////////////////////////////////////////////////////
        ?>
                            </div>
                        </div>
                    </div>
            </section
    </DIV>
    <?php
      // PHP SCRIPT ////////////////////////////////////////////////////////////
      BuildFooter();
      //////////////////////////////////////////////////////////////////////////
    ?>
  </BODY>
</HTML>