<?php
  //////////////////////////////////////////////////////////////////////////////
  // This page allows users to manage and view staff.                         //
  //////////////////////////////////////////////////////////////////////////////
  
  include 'Scripts/Include.php';
  SetSettings();
  CheckAuthorisation('StaffMan.php');
  
  //////////////////////////////////////////////////////////////////////////////
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3c.org/TR/1999/REC-html401-19991224/loose.dtd">
<HTML>
  <HEAD>
      <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <?php
      // PHP SCRIPT ////////////////////////////////////////////////////////////
      BuildHead('Staff Management');
    include ('Scripts/header.php');
      //////////////////////////////////////////////////////////////////////////
    ?>   
  </HEAD>
  <BODY> 
    <?php 
      // PHP SCRIPT ////////////////////////////////////////////////////////////
      BuildBanner();
      //////////////////////////////////////////////////////////////////////////
    ?>
    <DIV id="main">
      <?php
        // PHP SCRIPT //////////////////////////////////////////////////////////
      BuildTopBar();
        BuildMenu('Main', 'StaffMan.php');
        ////////////////////////////////////////////////////////////////////////
      ?>
        <section id="content_wrapper">
            <?php BuildBreadCrumb();?>
            <!-- -------------- Content -------------- -->
            <section id="content" class="table-layout">
                <!-- -------------- Column Center -------------- -->
                <div class="chute chute-center" style="height: 869px;">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel">
        <?php
          // PHP SCRIPT ////////////////////////////////////////////////////////
          BuildMessageSet('StaffMan');
          
          function GetCount($paPeople, $paCountDisabled = 'All', $paCountForeign = 'All', $paCountFunction = 'All', $paCountGender = 'All', $paCountPosition = 'All', $paCountPositionPermanent = 'All', $paCountRace = 'All')
          {
            $loCount = 0;
            for ($loI = 0; $loI < Count($paPeople); $loI++)
            {
              $loPeople = $paPeople[$loI];
              //array(Gender, Race, Foreign, Disabled, Position, PositionPermanent, Function);
              if (($paCountDisabled == 'All' || $paCountDisabled == $loPeople[3]) &&
                  ($paCountForeign == 'All' || $paCountForeign == $loPeople[2]) &&
                  ($paCountFunction == 'All' || $paCountFunction == $loPeople[6]) &&
                  ($paCountGender == 'All' || $paCountGender == $loPeople[0]) &&
                  ($paCountPosition == 'All' || $paCountPosition == $loPeople[4]) &&
                  ($paCountPositionPermanent == 'All' || $paCountPositionPermanent == $loPeople[5]) &&
                  ($paCountRace == 'All' || $paCountRace == $loPeople[1]))
              {
                $loCount++;
              }
            }
            return $loCount;
          }
          //////////////////////////////////////////////////////////////////////    
        ?>
        <?php
          // PHP SCRIPT ////////////////////////////////////////////////////////
          if (IsSet($_SESSION['EditStaffMan']))
          {
              $row = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = '.$_SESSION['EditStaffMan'][0].''));
              BuildContentHeader('Edit Staff - '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'], "", "", false);
              echo '<DIV class="contentflow">
                    <P>Once you have made changes, ensure that you submit them otherwise they will not take effect.<P>
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center" class="long">
                      <FORM method="post" action="Handlers/StaffMan_Handler.php">
                        <INPUT name="Type" type="hidden" value="Edit">
                        <TR>
                          <TD colspan="4" class="header">Management Details
                          </TD>
                        </TR>
                        <TR>
                          <TD class="short">Company Function:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD class="standard">';
              BuildFunctionSelector(1, 'Function', 'standard', $_SESSION['EditStaffMan'][16]);
              echo '</TD>
                          <TD class="short">Timesheet Required (old Social Club Field):
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD class="standard">';
              BuildYesNoSelector(14, 'Social', 'veryshort', $_SESSION['EditStaffMan'][14]);
              echo '</TD>
                        </TR>
                        <TR>
                          <TD class="short">Company Position:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD class="standard">';
              BuildPositionSelector(2, 'Position', 'standard', $_SESSION['EditStaffMan'][15]);
              echo '</TD>
                          <TD>AFB Number:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD class="standard">
                            <INPUT tabindex="15" name="AFBNumber" type="text" class="text veryshort" value="'.$_SESSION['EditStaffMan'][1].'" maxlength="11" />
                          </TD>
                        </TR>
                        <TR>
                          <TD>Contract:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
              BuildContractSelector(3, 'Contract', 'standard', $_SESSION['EditStaffMan'][9]);
              echo '</TD>
                          <TD>Leave Allocation:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>
                            <INPUT tabindex="16" name="Leave" type="text" class="text veryshort" value="'.$_SESSION['EditStaffMan'][3].'" maxlength="7" />
                          </TD>
                        </TR>
                        <TR>';
              BuildLabelCell('Probation Expiry Date:', 'short', 2);
              echo '<TD>';
              BuildDaySelector(4, 'ProbeDay', GetDayFromSessionDate($_SESSION['EditStaffMan'][20]));
              echo '&nbsp;';
              BuildMonthSelector(5, 'ProbeMonth', GetMonthFromSessionDate($_SESSION['EditStaffMan'][20]));
              echo '&nbsp;';
              BuildYearSelector(6, 'ProbeYear', GetYearFromSessionDate($_SESSION['EditStaffMan'][20]));
              echo '</TD>
                          <TD>Overtime Rate:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>
                            <INPUT tabindex="17" name="Rate" type="text" class="text veryshort" value="'.$_SESSION['EditStaffMan'][5].'" maxlength="6" />
                          </TD>
                        </TR>
                        <TR>
                          <TD>Work Hours:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
              BuildHoursSelector(7, 'HoursStart', 'veryshort', $_SESSION['EditStaffMan'][6]);
              echo '&nbsp;to ';
              BuildHoursSelector(8, 'HoursEnd', 'veryshort', $_SESSION['EditStaffMan'][7]);
              echo '</TD>
                          <TD>Travel Amount:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>
                            <INPUT tabindex="18" name="TravelAmount" type="text" class="text veryshort" value="'.$_SESSION['EditStaffMan'][8].'" maxlength="3" />
                          </TD>
                        </TR>
                        <TR>
                          <TD>Overtime Payout:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
              BuildOvertimePayoutSelector(9, 'PayoutOvertime', 'short', $_SESSION['EditStaffMan'][13]);
              echo '</TD>
                          <TD>Travel Duration (<B>Days</B>):
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>
                            <INPUT tabindex="19" name="TravelDuration" type="text" class="text veryshort" value="'.$_SESSION['EditStaffMan'][10].'" maxlength="3" />
                          </TD>
                        </TR>
                        <TR>
                          <TD>Accountable for Hours:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
              BuildYesNoSelector(10, 'HoursAccountable', 'veryshort', $_SESSION['EditStaffMan'][12]);
              echo '</TD>
                          <TD>Cellphone Allowance:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD class="bold">
                          R <INPUT tabindex="20" name="Cellphone" type="text" class="text veryshort" value="'.$_SESSION['EditStaffMan'][2].'" maxlength="11" />
                          </TD>
                        </TR>
                        <TR>
                          <TD>Current Employee:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
              BuildYesNoSelector(11, 'Current', 'veryshort', $_SESSION['EditStaffMan'][11]);
              echo '</TD>
                          <TD>Travel Allowance:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD class="bold">
                          R <INPUT tabindex="21" name="TravelAllowance" type="text" class="text veryshort" value="'.$_SESSION['EditStaffMan'][18].'" maxlength="4" />
                          </TD>
                        </TR>
                        <TR>
                          <TD>Auth Level:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
              BuildAuthLevelSelector(12, 'AuthLevel', 'veryshort', $_SESSION['EditStaffMan'][4]);
              echo '</TD>
                          <TD>Salary:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD class="bold">
                          R <INPUT tabindex="22" name="Salary" type="text" class="text veryshort" value="'.$_SESSION['EditStaffMan'][17].'" maxlength="6" />
                          </TD>
                        </TR>
                        <TR>';
              BuildLabelCell('RA Included:', 'short');
              echo '<TD>';
              BuildYesNoSelector(13, 'RAIncluded', 'veryshort', $_SESSION['EditStaffMan'][21]);
              echo '</TD>
                          <TD>RA Contribution:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD class="bold">
                          R <INPUT tabindex="23" name="RAContribution" type="text" class="text veryshort" value="'.$_SESSION['EditStaffMan'][19].'" maxlength="4" />
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="4" class="center">
                            <INPUT tabindex="24" name="Submit" type="submit" class="button" value="Submit" />   
                            <INPUT tabindex="25" name="Submit" type="submit" class="button" value="Cancel" />                   
                          </TD>
                        </TR>
                      </FORM>
                    </TABLE>  
                  </DIV>  
                  <DIV>
                    <BR /><BR />
                    <SPAN class="note">*
                    </SPAN>
                    These fields are required.
                    <BR /><BR />
                    <SPAN class="note">**
                    </SPAN>
                    Probation expiry date is only required for Temporary staff.
                  </DIV>';
          } else
              if (IsSet($_SESSION['ViewStaffManDemographics']))
              {
                  $loPeople = array();
                  $loPositions = array();
                  $loRaces = array();

                  $loRSPeople = ExecuteQuery('SELECT Staff_Demographics_Gender, Staff_Demographics_Race, Staff_Demographics_Foreign, Staff_Demographics_Disabled, Staff_Function, Staff_Position, Position_Permanent FROM Staff LEFT JOIN Position ON Staff_Position = Position_ID WHERE Staff_IsEmployee > 0');
                  if (MySQL_Num_Rows($loRSPeople) > 0)
                  {
                      while ($loRowPeople = MySQL_Fetch_Array($loRSPeople))
                      {
                          $loPeople[] = array($loRowPeople['Staff_Demographics_Gender'], $loRowPeople['Staff_Demographics_Race'], $loRowPeople['Staff_Demographics_Foreign'], $loRowPeople['Staff_Demographics_Disabled'], $loRowPeople['Staff_Position'], $loRowPeople['Position_Permanent'], $loRowPeople['Staff_Function']);
                      }
                  }

                  $loRSPositions = ExecuteQuery('SELECT Position_ID, Position_Description, Position_Permanent FROM Position ORDER BY Position_ID DESC');
                  if (MySQL_Num_Rows($loRSPositions) > 0)
                  {
                      while ($loRowPosition = MySQL_Fetch_Array($loRSPositions))
                      {
                          $loPositions[] = array($loRowPosition['Position_ID'], $loRowPosition['Position_Description'], $loRowPosition['Position_Permanent']);
                      }
                  }

                  $loRSRaces = ExecuteQuery('SELECT Race_Description FROM Race ORDER BY Race_Description ASC');
                  if (MySQL_Num_Rows($loRSRaces) > 0)
                  {
                      while ($loRowRace = MySQL_Fetch_Array($loRSRaces))
                      {
                          $loRaces[] = $loRowRace['Race_Description'];
                      }
                  }

                  $loPeopleCount = Count($loPeople);
                  $loPositionsCount = Count($loPositions);
                  $loRacesCount = Count($loRaces);

                  $loTotalsPermanent = array();
                  $loTotalsTemporary = array();
                  for ($loI = 0; $loI < ($loRacesCount*2); $loI++)
                  {
                      $loTotalsPermanent[] = 0;
                      $loTotalsTemporary[] = 0;
                  }
                  $loTotalsPermanent[] = 0; //Foreign Nationals - Male
                  $loTotalsPermanent[] = 0; //Foreign Nationals - Female
                  $loTotalsPermanent[] = 0; //Total

                  $loTotalsTemporary[] = 0; //Foreign Nationals - Male
                  $loTotalsTemporary[] = 0; //Foreign Nationals - Female
                  $loTotalsTemporary[] = 0; //Total

                  $loTotalsCount = Count($loTotalsPermanent);

                  BuildContentHeader('View Staff Demographics - '.Date('d M Y'), "", "", false);
                  echo '<DIV class="contentflow">
                    <P>These are the staff management details for all staff.<P>
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center" class="long">
                      <TR>
                        <TD class="header" colspan="'.(4 + ($loRacesCount * 2)).'">All Staff
                        </TD>
                      </TR>
                      <TR>
                        <TD class="header" rowspan="2">Occupational Levels
                        </TD>
                        <TD class="header" colspan="'.$loRacesCount.'">Male
                        </TD>
                        <TD class="header" colspan="'.$loRacesCount.'">Female
                        </TD>
                        <TD class="header" colspan="2">Foreign Nationals
                        </TD>
                        <TD class="header" rowspan="2">Total
                        </TD>
                      </TR>
                      <TR>';
                  for ($loI = 0; $loI < $loRacesCount; $loI++)
                  {
                      echo '<TD class="subheaderclear">'.$loRaces[$loI].'
                                </TD>';
                  }
                  for ($loI = 0; $loI < $loRacesCount; $loI++)
                  {
                      echo '<TD class="subheaderclear">'.$loRaces[$loI].'
                                </TD>';
                  }
                  echo '<TD class="subheaderclear">Male
                        </TD>
                        <TD class="subheaderclear">Female
                        </TD>
                      </TR>';
                  for ($loI = 0; $loI < $loPositionsCount; $loI++)
                  {
                      $loPosition = $loPositions[$loI];
                      if ($loPosition[2] == 1) //Check if it is a permanent position.
                      {
                          $loTotal = 0;
                          echo '<TR>
                                  <TD>'.$loPosition[1].'
                                  </TD>';
                          //Males
                          for ($loJ = 0; $loJ < $loRacesCount; $loJ++)
                          {
                              $loCount = GetCount($loPeople, 'All', 'All', 'All', 'Male', $loPosition[0], '1', $loRaces[$loJ]);
                              echo '<TD class="center'.($loJ == 0 ? ' bleft' : "").'">'.$loCount.'
                                          </TD>';
                              $loTotalsPermanent[$loJ] += $loCount;
                              $loTotal += $loCount;
                          }

                          //Females
                          for ($loJ = 0; $loJ < $loRacesCount; $loJ++)
                          {
                              $loCount = GetCount($loPeople, 'All', 'All', 'All', 'Female', $loPosition[0], '1', $loRaces[$loJ]);
                              echo '<TD class="center'.($loJ == 0 ? ' bleft' : "").'">'.$loCount.'
                                          </TD>';
                              $loTotalsPermanent[$loJ + $loRacesCount] += $loCount;
                              $loTotal += $loCount;
                          }

                          //Male Foreign Nationals
                          $loCount = GetCount($loPeople, 'All', '1', 'All', 'Male', $loPosition[0], '1', 'All');
                          echo '<TD class="center bleft">'.$loCount.'
                                        </TD>';
                          $loTotalsPermanent[($loRacesCount * 2)] += $loCount;

                          //Female Foreign Nationals
                          $loCount = GetCount($loPeople, 'All', '1', 'All', 'Female', $loPosition[0], '1', 'All');
                          echo '<TD class="center">'.$loCount.'
                                        </TD>';
                          $loTotalsPermanent[($loRacesCount * 2) + 1] += $loCount;

                          $loTotalsPermanent[($loRacesCount * 2) + 2] += $loTotal;
                          echo '<TD class="bold center bleft">'.$loTotal.'
                                  </TD>
                                </TR>';
                      }
                  }
                  echo '<TR>
                        <TD class="bold">Total Permanent
                        </TD>';
                  $loTotal = 0;
                  for ($loI = 0; $loI < $loTotalsCount; $loI++)
                  {
                      echo '<TD class="bold center'.(($loI % $loRacesCount == 0) || ($loI == ($loTotalsCount - 1)) ? ' bleft' : "").'">'.$loTotalsPermanent[$loI].'
                                </TD>';
                      $loTotal += $loTotalsPermanent[$loI];
                  }
                  echo '</TR>';
                  for ($loI = 0; $loI < $loPositionsCount; $loI++)
                  {
                      $loPosition = $loPositions[$loI];
                      if ($loPosition[2] == 0) //Check if it is a temporary position.
                      {
                          $loTotal = 0;
                          echo '<TR>
                                  <TD>'.$loPosition[1].'
                                  </TD>';
                          //Males
                          for ($loJ = 0; $loJ < $loRacesCount; $loJ++)
                          {
                              $loCount = GetCount($loPeople, 'All', 'All', 'All', 'Male', $loPosition[0], '0', $loRaces[$loJ]);
                              echo '<TD class="center'.($loJ == 0 ? ' bleft' : "").'">'.$loCount.'
                                          </TD>';
                              $loTotalsTemporary[$loJ] += $loCount;
                              $loTotal += $loCount;
                          }

                          //Females
                          for ($loJ = 0; $loJ < $loRacesCount; $loJ++)
                          {
                              $loCount = GetCount($loPeople, 'All', 'All', 'All', 'Female', $loPosition[0], '0', $loRaces[$loJ]);
                              echo '<TD class="center'.($loJ == 0 ? ' bleft' : "").'">'.$loCount.'
                                          </TD>';
                              $loTotalsTemporary[$loJ + $loRacesCount] += $loCount;
                              $loTotal += $loCount;
                          }

                          //Male Foreign Nationals
                          $loCount = GetCount($loPeople, 'All', '1', 'All', 'Male', $loPosition[0], '0', 'All');
                          echo '<TD class="center bleft">'.$loCount.'
                                        </TD>';
                          $loTotalsTemporary[($loRacesCount * 2)] += $loCount;

                          //Female Foreign Nationals
                          $loCount = GetCount($loPeople, 'All', '1', 'All', 'Female', $loPosition[0], '0', 'All');
                          echo '<TD class="center">'.$loCount.'
                                        </TD>';
                          $loTotalsTemporary[($loRacesCount * 2) + 1] += $loCount;

                          $loTotalsTemporary[($loRacesCount * 2) + 2] += $loTotal;
                          echo '<TD class="bold center bleft">'.$loTotal.'
                                  </TD>
                                </TR>';
                      }
                  }
                  echo '<TR>
                        <TD class="bold">Grand Total
                        </TD>';
                  for ($loI = 0; $loI < $loTotalsCount; $loI++)
                  {
                      echo '<TD class="bold center'.(($loI % $loRacesCount == 0) || ($loI == ($loTotalsCount - 1)) ? ' bleft' : "").'">'.($loTotalsPermanent[$loI] + $loTotalsTemporary[$loI]).'
                                </TD>';
                  }
                  echo '</TR>
                    </TABLE>';
                  for ($loI = 0; $loI < $loTotalsCount; $loI++)
                  {
                      $loTotalsPermanent[$loI] = 0;
                      $loTotalsTemporary[$loI] = 0;
                  }
                  echo '<BR />
                    <BR />
                    <P>These are the staff management details for all staff with disabilities.<P>
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center" class="long">
                      <TR>
                        <TD class="header" colspan="'.(4 + ($loRacesCount * 2)).'">All Staff With Disabilities
                        </TD>
                      </TR>
                      <TR>
                        <TD class="header" rowspan="2">Occupational Levels
                        </TD>
                        <TD class="header" colspan="'.$loRacesCount.'">Male
                        </TD>
                        <TD class="header" colspan="'.$loRacesCount.'">Female
                        </TD>
                        <TD class="header" colspan="2">Foreign Nationals
                        </TD>
                        <TD class="header" rowspan="2">Total
                        </TD>
                      </TR>
                      <TR>';
                  for ($loI = 0; $loI < $loRacesCount; $loI++)
                  {
                      echo '<TD class="subheaderclear">'.$loRaces[$loI].'
                                </TD>';
                  }
                  for ($loI = 0; $loI < $loRacesCount; $loI++)
                  {
                      echo '<TD class="subheaderclear">'.$loRaces[$loI].'
                                </TD>';
                  }
                  echo '<TD class="subheaderclear">Male
                        </TD>
                        <TD class="subheaderclear">Female
                        </TD>
                      </TR>';
                  for ($loI = 0; $loI < $loPositionsCount; $loI++)
                  {
                      $loPosition = $loPositions[$loI];
                      if ($loPosition[2] == 1) //Check if it is a permanent position.
                      {
                          $loTotal = 0;
                          echo '<TR>
                                  <TD>'.$loPosition[1].'
                                  </TD>';
                          //Males
                          for ($loJ = 0; $loJ < $loRacesCount; $loJ++)
                          {
                              $loCount = GetCount($loPeople, '1', 'All', 'All', 'Male', $loPosition[0], '1', $loRaces[$loJ]);
                              echo '<TD class="center'.($loJ == 0 ? ' bleft' : "").'">'.$loCount.'
                                          </TD>';
                              $loTotalsPermanent[$loJ] += $loCount;
                              $loTotal += $loCount;
                          }

                          //Females
                          for ($loJ = 0; $loJ < $loRacesCount; $loJ++)
                          {
                              $loCount = GetCount($loPeople, '1', 'All', 'All', 'Female', $loPosition[0], '1', $loRaces[$loJ]);
                              echo '<TD class="center'.($loJ == 0 ? ' bleft' : "").'">'.$loCount.'
                                          </TD>';
                              $loTotalsPermanent[$loJ + $loRacesCount] += $loCount;
                              $loTotal += $loCount;
                          }

                          //Male Foreign Nationals
                          $loCount = GetCount($loPeople, '1', '1', 'All', 'Male', $loPosition[0], '1', 'All');
                          echo '<TD class="center bleft">'.$loCount.'
                                        </TD>';
                          $loTotalsPermanent[($loRacesCount * 2)] += $loCount;

                          //Female Foreign Nationals
                          $loCount = GetCount($loPeople, '1', '1', 'All', 'Female', $loPosition[0], '1', 'All');
                          echo '<TD class="center">'.$loCount.'
                                        </TD>';
                          $loTotalsPermanent[($loRacesCount * 2) + 1] += $loCount;

                          $loTotalsPermanent[($loRacesCount * 2) + 2] += $loTotal;
                          echo '<TD class="bold center bleft">'.$loTotal.'
                                  </TD>
                                </TR>';
                      }
                  }
                  echo '<TR>
                        <TD class="bold">Total Permanent
                        </TD>';
                  $loTotal = 0;
                  for ($loI = 0; $loI < $loTotalsCount; $loI++)
                  {
                      echo '<TD class="bold center'.(($loI % $loRacesCount == 0) || ($loI == ($loTotalsCount - 1)) ? ' bleft' : "").'">'.$loTotalsPermanent[$loI].'
                                </TD>';
                      $loTotal += $loTotalsPermanent[$loI];
                  }
                  echo '</TR>';
                  for ($loI = 0; $loI < $loPositionsCount; $loI++)
                  {
                      $loPosition = $loPositions[$loI];
                      if ($loPosition[2] == 0) //Check if it is a temporary position.
                      {
                          $loTotal = 0;
                          echo '<TR>
                                  <TD>'.$loPosition[1].'
                                  </TD>';
                          //Males
                          for ($loJ = 0; $loJ < $loRacesCount; $loJ++)
                          {
                              $loCount = GetCount($loPeople, '1', 'All', 'All', 'Male', $loPosition[0], '0', $loRaces[$loJ]);
                              echo '<TD class="center'.($loJ == 0 ? ' bleft' : "").'">'.$loCount.'
                                          </TD>';
                              $loTotalsTemporary[$loJ] += $loCount;
                              $loTotal += $loCount;
                          }

                          //Females
                          for ($loJ = 0; $loJ < $loRacesCount; $loJ++)
                          {
                              $loCount = GetCount($loPeople, '1', 'All', 'All', 'Female', $loPosition[0], '0', $loRaces[$loJ]);
                              echo '<TD class="center'.($loJ == 0 ? ' bleft' : "").'">'.$loCount.'
                                          </TD>';
                              $loTotalsTemporary[$loJ + $loRacesCount] += $loCount;
                              $loTotal += $loCount;
                          }

                          //Male Foreign Nationals
                          $loCount = GetCount($loPeople, '1', '1', 'All', 'Male', $loPosition[0], '0', 'All');
                          echo '<TD class="center bleft">'.$loCount.'
                                        </TD>';
                          $loTotalsTemporary[($loRacesCount * 2)] += $loCount;

                          //Female Foreign Nationals
                          $loCount = GetCount($loPeople, '1', '1', 'All', 'Female', $loPosition[0], '0', 'All');
                          echo '<TD class="center">'.$loCount.'
                                        </TD>';
                          $loTotalsTemporary[($loRacesCount * 2) + 1] += $loCount;

                          $loTotalsTemporary[($loRacesCount * 2) + 2] += $loTotal;
                          echo '<TD class="bold center bleft">'.$loTotal.'
                                  </TD>
                                </TR>';
                      }
                  }
                  echo '<TR>
                        <TD class="bold">Grand Total
                        </TD>';
                  for ($loI = 0; $loI < $loTotalsCount; $loI++)
                  {
                      echo '<TD class="bold center'.(($loI % $loRacesCount == 0) || ($loI == ($loTotalsCount - 1)) ? ' bleft' : "").'">'.($loTotalsPermanent[$loI] + $loTotalsTemporary[$loI]).'
                                </TD>';
                  }
                  echo '</TR>
                    </TABLE>';
                  for ($loI = 0; $loI < $loTotalsCount; $loI++)
                  {
                      $loTotalsPermanent[$loI] = 0;
                      $loTotalsTemporary[$loI] = 0;
                  }
                  echo '<BR />
                    <BR />
                    <P>These are the staff management details for all staff with a Core function.<P>
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center" class="long">
                      <TR>
                        <TD class="header" colspan="'.(4 + ($loRacesCount * 2)).'">All Staff with Core Function
                        </TD>
                      </TR>
                      <TR>
                        <TD class="header" rowspan="2">Occupational Levels
                        </TD>
                        <TD class="header" colspan="'.$loRacesCount.'">Male
                        </TD>
                        <TD class="header" colspan="'.$loRacesCount.'">Female
                        </TD>
                        <TD class="header" colspan="2">Foreign Nationals
                        </TD>
                        <TD class="header" rowspan="2">Total
                        </TD>
                      </TR>
                      <TR>';
                  for ($loI = 0; $loI < $loRacesCount; $loI++)
                  {
                      echo '<TD class="subheaderclear">'.$loRaces[$loI].'
                                </TD>';
                  }
                  for ($loI = 0; $loI < $loRacesCount; $loI++)
                  {
                      echo '<TD class="subheaderclear">'.$loRaces[$loI].'
                                </TD>';
                  }
                  echo '<TD class="subheaderclear">Male
                        </TD>
                        <TD class="subheaderclear">Female
                        </TD>
                      </TR>';
                  for ($loI = 0; $loI < $loPositionsCount; $loI++)
                  {
                      $loPosition = $loPositions[$loI];
                      if ($loPosition[2] == 1) //Check if it is a permanent position.
                      {
                          $loTotal = 0;
                          echo '<TR>
                                  <TD>'.$loPosition[1].'
                                  </TD>';
                          //Males
                          for ($loJ = 0; $loJ < $loRacesCount; $loJ++)
                          {
                              $loCount = GetCount($loPeople, 'All', 'All', '1', 'Male', $loPosition[0], '1', $loRaces[$loJ]);
                              echo '<TD class="center'.($loJ == 0 ? ' bleft' : "").'">'.$loCount.'
                                          </TD>';
                              $loTotalsPermanent[$loJ] += $loCount;
                              $loTotal += $loCount;
                          }

                          //Females
                          for ($loJ = 0; $loJ < $loRacesCount; $loJ++)
                          {
                              $loCount = GetCount($loPeople, 'All', 'All', '1', 'Female', $loPosition[0], '1', $loRaces[$loJ]);
                              echo '<TD class="center'.($loJ == 0 ? ' bleft' : "").'">'.$loCount.'
                                          </TD>';
                              $loTotalsPermanent[$loJ + $loRacesCount] += $loCount;
                              $loTotal += $loCount;
                          }

                          //Male Foreign Nationals
                          $loCount = GetCount($loPeople, 'All', '1', '1', 'Male', $loPosition[0], '1', 'All');
                          echo '<TD class="center bleft">'.$loCount.'
                                        </TD>';
                          $loTotalsPermanent[($loRacesCount * 2)] += $loCount;

                          //Female Foreign Nationals
                          $loCount = GetCount($loPeople, 'All', '1', '1', 'Female', $loPosition[0], '1', 'All');
                          echo '<TD class="center">'.$loCount.'
                                        </TD>';
                          $loTotalsPermanent[($loRacesCount * 2) + 1] += $loCount;

                          $loTotalsPermanent[($loRacesCount * 2) + 2] += $loTotal;
                          echo '<TD class="bold center bleft">'.$loTotal.'
                                  </TD>
                                </TR>';
                      }
                  }
                  echo '<TR>
                        <TD class="bold">Total Permanent
                        </TD>';
                  $loTotal = 0;
                  for ($loI = 0; $loI < $loTotalsCount; $loI++)
                  {
                      echo '<TD class="bold center'.(($loI % $loRacesCount == 0) || ($loI == ($loTotalsCount - 1)) ? ' bleft' : "").'">'.$loTotalsPermanent[$loI].'
                                </TD>';
                      $loTotal += $loTotalsPermanent[$loI];
                  }
                  echo '</TR>';
                  for ($loI = 0; $loI < $loPositionsCount; $loI++)
                  {
                      $loPosition = $loPositions[$loI];
                      if ($loPosition[2] == 0) //Check if it is a temporary position.
                      {
                          $loTotal = 0;
                          echo '<TR>
                                  <TD>'.$loPosition[1].'
                                  </TD>';
                          //Males
                          for ($loJ = 0; $loJ < $loRacesCount; $loJ++)
                          {
                              $loCount = GetCount($loPeople, 'All', 'All', '1', 'Male', $loPosition[0], '0', $loRaces[$loJ]);
                              echo '<TD class="center'.($loJ == 0 ? ' bleft' : "").'">'.$loCount.'
                                          </TD>';
                              $loTotalsTemporary[$loJ] += $loCount;
                              $loTotal += $loCount;
                          }

                          //Females
                          for ($loJ = 0; $loJ < $loRacesCount; $loJ++)
                          {
                              $loCount = GetCount($loPeople, 'All', 'All', '1', 'Female', $loPosition[0], '0', $loRaces[$loJ]);
                              echo '<TD class="center'.($loJ == 0 ? ' bleft' : "").'">'.$loCount.'
                                          </TD>';
                              $loTotalsTemporary[$loJ + $loRacesCount] += $loCount;
                              $loTotal += $loCount;
                          }

                          //Male Foreign Nationals
                          $loCount = GetCount($loPeople, 'All', '1', '1', 'Male', $loPosition[0], '0', 'All');
                          echo '<TD class="center bleft">'.$loCount.'
                                        </TD>';
                          $loTotalsTemporary[($loRacesCount * 2)] += $loCount;

                          //Female Foreign Nationals
                          $loCount = GetCount($loPeople, 'All', '1', '1', 'Female', $loPosition[0], '0', 'All');
                          echo '<TD class="center">'.$loCount.'
                                        </TD>';
                          $loTotalsTemporary[($loRacesCount * 2) + 1] += $loCount;

                          $loTotalsTemporary[($loRacesCount * 2) + 2] += $loTotal;
                          echo '<TD class="bold center bleft">'.$loTotal.'
                                  </TD>
                                </TR>';
                      }
                  }
                  echo '<TR>
                        <TD class="bold">Grand Total
                        </TD>';
                  for ($loI = 0; $loI < $loTotalsCount; $loI++)
                  {
                      echo '<TD class="bold center'.(($loI % $loRacesCount == 0) || ($loI == ($loTotalsCount - 1)) ? ' bleft' : "").'">'.($loTotalsPermanent[$loI] + $loTotalsTemporary[$loI]).'
                                </TD>';
                  }
                  echo '</TR>
                    </TABLE>';
                  for ($loI = 0; $loI < $loTotalsCount; $loI++)
                  {
                      $loTotalsPermanent[$loI] = 0;
                      $loTotalsTemporary[$loI] = 0;
                  }
                  echo '<BR />
                    <BR />
                    <P>These are the staff management details for all staff with a Support function.<P>
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center" class="long">
                      <TR>
                        <TD class="header" colspan="'.(4 + ($loRacesCount * 2)).'">All Staff With Support Function
                        </TD>
                      </TR>
                      <TR>
                        <TD class="header" rowspan="2">Occupational Levels
                        </TD>
                        <TD class="header" colspan="'.$loRacesCount.'">Male
                        </TD>
                        <TD class="header" colspan="'.$loRacesCount.'">Female
                        </TD>
                        <TD class="header" colspan="2">Foreign Nationals
                        </TD>
                        <TD class="header" rowspan="2">Total
                        </TD>
                      </TR>
                      <TR>';
                  for ($loI = 0; $loI < $loRacesCount; $loI++)
                  {
                      echo '<TD class="subheaderclear">'.$loRaces[$loI].'
                                </TD>';
                  }
                  for ($loI = 0; $loI < $loRacesCount; $loI++)
                  {
                      echo '<TD class="subheaderclear">'.$loRaces[$loI].'
                                </TD>';
                  }
                  echo '<TD class="subheaderclear">Male
                        </TD>
                        <TD class="subheaderclear">Female
                        </TD>
                      </TR>';
                  for ($loI = 0; $loI < $loPositionsCount; $loI++)
                  {
                      $loPosition = $loPositions[$loI];
                      if ($loPosition[2] == 1) //Check if it is a permanent position.
                      {
                          $loTotal = 0;
                          echo '<TR>
                                  <TD>'.$loPosition[1].'
                                  </TD>';
                          //Males
                          for ($loJ = 0; $loJ < $loRacesCount; $loJ++)
                          {
                              $loCount = GetCount($loPeople, 'All', 'All', '2', 'Male', $loPosition[0], '1', $loRaces[$loJ]);
                              echo '<TD class="center'.($loJ == 0 ? ' bleft' : "").'">'.$loCount.'
                                          </TD>';
                              $loTotalsPermanent[$loJ] += $loCount;
                              $loTotal += $loCount;
                          }

                          //Females
                          for ($loJ = 0; $loJ < $loRacesCount; $loJ++)
                          {
                              $loCount = GetCount($loPeople, 'All', 'All', '2', 'Female', $loPosition[0], '1', $loRaces[$loJ]);
                              echo '<TD class="center'.($loJ == 0 ? ' bleft' : "").'">'.$loCount.'
                                          </TD>';
                              $loTotalsPermanent[$loJ + $loRacesCount] += $loCount;
                              $loTotal += $loCount;
                          }

                          //Male Foreign Nationals
                          $loCount = GetCount($loPeople, 'All', '1', '2', 'Male', $loPosition[0], '1', 'All');
                          echo '<TD class="center bleft">'.$loCount.'
                                        </TD>';
                          $loTotalsPermanent[($loRacesCount * 2)] += $loCount;

                          //Female Foreign Nationals
                          $loCount = GetCount($loPeople, 'All', '1', '2', 'Female', $loPosition[0], '1', 'All');
                          echo '<TD class="center">'.$loCount.'
                                        </TD>';
                          $loTotalsPermanent[($loRacesCount * 2) + 1] += $loCount;

                          $loTotalsPermanent[($loRacesCount * 2) + 2] += $loTotal;
                          echo '<TD class="bold center bleft">'.$loTotal.'
                                  </TD>
                                </TR>';
                      }
                  }
                  echo '<TR>
                        <TD class="bold">Total Permanent
                        </TD>';
                  $loTotal = 0;
                  for ($loI = 0; $loI < $loTotalsCount; $loI++)
                  {
                      echo '<TD class="bold center'.(($loI % $loRacesCount == 0) || ($loI == ($loTotalsCount - 1)) ? ' bleft' : "").'">'.$loTotalsPermanent[$loI].'
                                </TD>';
                      $loTotal += $loTotalsPermanent[$loI];
                  }
                  echo '</TR>';
                  for ($loI = 0; $loI < $loPositionsCount; $loI++)
                  {
                      $loPosition = $loPositions[$loI];
                      if ($loPosition[2] == 0) //Check if it is a temporary position.
                      {
                          $loTotal = 0;
                          echo '<TR>
                                  <TD>'.$loPosition[1].'
                                  </TD>';
                          //Males
                          for ($loJ = 0; $loJ < $loRacesCount; $loJ++)
                          {
                              $loCount = GetCount($loPeople, 'All', 'All', '2', 'Male', $loPosition[0], '0', $loRaces[$loJ]);
                              echo '<TD class="center'.($loJ == 0 ? ' bleft' : "").'">'.$loCount.'
                                          </TD>';
                              $loTotalsTemporary[$loJ] += $loCount;
                              $loTotal += $loCount;
                          }

                          //Females
                          for ($loJ = 0; $loJ < $loRacesCount; $loJ++)
                          {
                              $loCount = GetCount($loPeople, 'All', 'All', '2', 'Female', $loPosition[0], '0', $loRaces[$loJ]);
                              echo '<TD class="center'.($loJ == 0 ? ' bleft' : "").'">'.$loCount.'
                                          </TD>';
                              $loTotalsTemporary[$loJ + $loRacesCount] += $loCount;
                              $loTotal += $loCount;
                          }

                          //Male Foreign Nationals
                          $loCount = GetCount($loPeople, 'All', '1', '2', 'Male', $loPosition[0], '0', 'All');
                          echo '<TD class="center bleft">'.$loCount.'
                                        </TD>';
                          $loTotalsTemporary[($loRacesCount * 2)] += $loCount;

                          //Female Foreign Nationals
                          $loCount = GetCount($loPeople, 'All', '1', '2', 'Female', $loPosition[0], '0', 'All');
                          echo '<TD class="center">'.$loCount.'
                                        </TD>';
                          $loTotalsTemporary[($loRacesCount * 2) + 1] += $loCount;

                          $loTotalsTemporary[($loRacesCount * 2) + 2] += $loTotal;
                          echo '<TD class="bold center bleft">'.$loTotal.'
                                  </TD>
                                </TR>';
                      }
                  }
                  echo '<TR>
                        <TD class="bold">Grand Total
                        </TD>';
                  for ($loI = 0; $loI < $loTotalsCount; $loI++)
                  {
                      echo '<TD class="bold center'.(($loI % $loRacesCount == 0) || ($loI == ($loTotalsCount - 1)) ? ' bleft' : "").'">'.($loTotalsPermanent[$loI] + $loTotalsTemporary[$loI]).'
                                </TD>';
                  }
                  echo '</TR>
                    </TABLE>
                    <BR />
                    <BR />
                    <P>These are the staff management details for each individual staff member.<P>
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center" class="long">
                      <TR>
                        <TD class="header" colspan="'.(5 + ($loRacesCount * 2)).'">All Staff - Individual Breakdown
                        </TD>
                      </TR>
                      <TR>
                        <TD class="header" rowspan="2">Name
                        </TD>
                        <TD class="header" colspan="'.$loRacesCount.'">Male
                        </TD>
                        <TD class="header" colspan="'.$loRacesCount.'">Female
                        </TD>
                        <TD class="header" colspan="2">Disabled
                        </TD>
                        <TD class="header" colspan="2">Foreign Nationals
                        </TD>
                      </TR>
                      <TR>';
                  for ($loI = 0; $loI < $loRacesCount; $loI++)
                  {
                      echo '<TD class="subheaderclear">'.$loRaces[$loI].'
                                </TD>';
                  }
                  for ($loI = 0; $loI < $loRacesCount; $loI++)
                  {
                      echo '<TD class="subheaderclear">'.$loRaces[$loI].'
                                </TD>';
                  }
                  echo '<TD class="subheaderclear">Male
                        </TD>
                        <TD class="subheaderclear">Female
                        </TD>
                        <TD class="subheaderclear">Male
                        </TD>
                        <TD class="subheaderclear">Female
                        </TD>
                      </TR>';
                  $loRSStaff = ExecuteQuery('SELECT Staff_First_Name, Staff_Last_Name, Staff_Demographics_Gender, Staff_Demographics_Race, Staff_Demographics_Foreign, Staff_Demographics_Disabled, Staff_Function, Staff_Position, Position_Permanent FROM Staff  LEFT JOIN Position ON Staff_Position = Position_ID where Staff_IsEmployee = "1" ORDER BY Staff_First_Name, Staff_Last_Name ASC');
                  while ($loRowStaff = MySQL_Fetch_Array($loRSStaff))
                  {
                      $loTotal = 0;
                      echo '<TR>
                                  <TD>'.$loRowStaff['Staff_First_Name'].' '.$loRowStaff['Staff_Last_Name'].'
                                  </TD>';
                      if ($loRowStaff['Staff_Demographics_Gender'] == 'Male')
                      {
                          //Males
                          for ($loJ = 0; $loJ < $loRacesCount; $loJ++)
                          {
                              echo '<TD class="center'.($loJ == 0 ? ' bleft' : "").'">'.($loRowStaff['Staff_Demographics_Race'] == $loRaces[$loJ] ? '<B>1</B>' : 0).'
                                            </TD>';
                          }
                          //Females
                          for ($loJ = 0; $loJ < $loRacesCount; $loJ++)
                          {
                              echo '<TD class="center'.($loJ == 0 ? ' bleft' : "").'">0
                                            </TD>';
                          }
                          //Male Foreign Nationals and Female Foreign Nationals
                          echo '<TD class="center bleft">'.($loRowStaff['Staff_Demographics_Disabled'] == 1 ? '<B>1</B>' : 0).'
                                          </TD>
                                          <TD class="center">0
                                          </TD>
                                          <TD class="center bleft">'.($loRowStaff['Staff_Demographics_Foreign'] == 1 ? '<B>1</B>' : 0).'
                                          </TD>
                                          <TD class="center">0
                                          </TD>';
                      } else
                      {
                          //Males
                          for ($loJ = 0; $loJ < $loRacesCount; $loJ++)
                          {
                              echo '<TD class="center'.($loJ == 0 ? ' bleft' : "").'">0
                                            </TD>';
                          }
                          //Females
                          for ($loJ = 0; $loJ < $loRacesCount; $loJ++)
                          {
                              echo '<TD class="center'.($loJ == 0 ? ' bleft' : "").'">'.($loRowStaff['Staff_Demographics_Race'] == $loRaces[$loJ] ? '<B>1</B>' : 0).'
                                            </TD>';
                          }
                          //Male Foreign Nationals and Female Foreign Nationals
                          echo '<TD class="center bleft">0
                                          </TD>
                                          <TD class="center">'.($loRowStaff['Staff_Demographics_Disabled'] == 1 ? '<B>1</B>' : 0).'
                                          </TD>
                                          <TD class="center bleft">0
                                          </TD>
                                          <TD class="center">'.($loRowStaff['Staff_Demographics_Foreign'] == 1 ? '<B>1</B>' : 0).'
                                          </TD>';
                      }
                      echo '</TR>';
                  }
                  echo '</TABLE>
                  </DIV>';
                  Session_Unregister('ViewStaffManDemographics');
              } else
                  if (IsSet($_SESSION['ViewStaffManSingle']))
                  {
                      $row = MySQL_Fetch_Array(ExecuteQuery('SELECT Staff.*, IF(Staff_IsEmployee > 0, "Yes", "No") AS Current, IF(Staff_RA_Included > 0, "Yes", "No") AS RAIncluded, IF(Staff_Social_Member > 0, "Yes", "No") AS Social, Function_Description, Position_Description FROM Staff LEFT JOIN Function ON Staff_Function = Function_ID LEFT JOIN Position ON Staff_Position = Position_ID WHERE Staff_Code = "'.$_SESSION['ViewStaffManSingle'][0].'"'));
                      $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM StaffContract WHERE StaffContract_ID = "'.$row['Staff_Contract'].'"'));
                      $resultSet = ExecuteQuery('SELECT * FROM WorkHours WHERE WorkHours_Name = '.$_SESSION['ViewStaffManSingle'][0].'');
                      $start = 0;
                      $end = 0;
                      if (MySQL_Num_Rows($resultSet) > 0)
                      {
                          $rowTempB = MySQL_Fetch_Array($resultSet);
                          $startFound = false;
                          for ($count = 0; $count < 24; $count++)
                          {
                              if ($startFound)
                              {
                                  if ($rowTempB['WorkHours_Hour'.$count] == '0')
                                  {
                                      $end = $count;
                                      break;
                                  }
                              } else
                                  if ($rowTempB['WorkHours_Hour'.$count] == '1')
                                  {
                                      $start = $count;
                                      $startFound = true;
                                  }
                          }
                      }
                      BuildContentHeader('View Staff - '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'], "", "", false);
                      echo '<DIV class="contentflow">
                    <P>These are the staff management details.<P>
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center" class="standard">
                      <TR>
                        <TD colspan="4" class="header">Management Details
                        </TD>
                      </TR>
                      <TR>
                        <TD>Accountable for Work Hours:
                        </TD>
                        <TD class="bold">'.($rowTempB['WorkHours_Accountable'] ? 'Yes' : 'No').'
                        </TD>
                      </TR>
                      <TR>
                        <TD>AFB Number:
                        </TD>
                        <TD class="bold">'.$row['Staff_AFB_Number'].'
                        </TD>
                      </TR>
                      <TR>
                        <TD>Auth Level:
                        </TD>
                        <TD class="bold">'.$row['Staff_Auth_Level'].'
                        </TD>
                      </TR>
                      <TR>
                        <TD>Cellphone Allowance:
                        </TD>
                        <TD class="bold">R'.SPrintF('%02.2f', $row['Staff_Cellphone_Allowance']).'
                        </TD>
                      </TR>
                      <TR>
                        <TD>Company Function:
                        </TD>
                        <TD class="bold">'.$row['Function_Description'].'
                        </TD>
                      </TR>
                      <TR>
                        <TD>Company Position:
                        </TD>
                        <TD class="bold">'.$row['Position_Description'].'
                        </TD>
                      </TR>
                      <TR>
                        <TD>Contract:
                        </TD>
                        <TD class="bold">'.$rowTemp['StaffContract_Description'].'
                        </TD>
                      </TR>
                      <TR>
                        <TD>Current Employee:
                        </TD>
                        <TD class="bold">'.$row['Current'].'
                        </TD>
                      </TR>
                      <TR>
                        <TD>Leave Allocation:
                        </TD>
                        <TD class="bold">'.$row['Staff_Leave_Allocation'].'
                        </TD>
                      </TR>
                      <TR>
                        <TD>Overtime Rate:
                        </TD>
                        <TD class="bold">'.$row['Staff_OT_Rate'].'
                        </TD>
                      </TR>
                      <TR>
                        <TD>Payout Overtime:
                        </TD>
                        <TD class="bold">'.($row['Staff_Payout_Overtime'] == 0 ? 'No' : ($row['Staff_Payout_Overtime'] == 1 ? 'All' : 'Capped' )).'
                        </TD>
                      </TR>
                      <TR>
                        <TD>Probation Expiry Date:
                        </TD>
                        <TD class="bold">'.GetTextualDateFromDatabaseDate($row['Staff_Probation_Date_Expires']).'
                        </TD>
                      </TR>
                      <TR>
                        <TD>RA Included:
                        </TD>
                        <TD class="bold">'.$row['RAIncluded'].'
                        </TD>
                      </TR>
                      <TR>
                        <TD>RA Contribution:
                        </TD>
                        <TD class="bold">R'.SPrintF('%02.2f', $row['Staff_RA_Contribution']).'
                        </TD>
                      </TR>
                      <TR>
                        <TD>Salary:
                        </TD>
                        <TD class="bold">R'.SPrintF('%02.2f', $row['Staff_Salary']).'
                        </TD>
                      </TR>
                      <TR>
                        <TD>Timesheet Needed (old social club field):
                        </TD>
                        <TD class="bold">'.$row['Social'].'
                        </TD>
                      </TR>
                      <TR>
                        <TD>Travel Allowance:
                        </TD>
                        <TD class="bold">R'.SPrintF('%02.2f', $row['Staff_Travel_Allowance']).'
                        </TD>
                      </TR>
                      <TR>
                        <TD>Travel Amount:
                        </TD>
                        <TD class="bold">'.$row['Staff_Travel_Amount'].'
                        </TD>
                      </TR>
                      <TR>
                        <TD>Travel Duration (<B>Days</B>):
                        </TD>
                        <TD class="bold">'.$row['Staff_Travel_Duration'].'
                        </TD>
                      </TR>
                      <TR>
                        <TD>Work Hours:
                        </TD>
                        <TD class="bold">'.SPrintF('%02d:00', $start).' to  '.SPrintF('%02d:00', $end).'
                        </TD>
                      </TR>
                    </TABLE>  
                  </DIV>';
                      Session_Unregister('ViewStaffManSingle');
                  } else
                  {
                      BuildContentHeader('Maintenance', "", "", false);
                      echo '<DIV class="contentflow">';
                      if (in_array($_SESSION['cUID'], ManagementAccess()))
                          echo '<P>Staff management details can be edited using the form below.</P>';
                      else
                          echo '<P>Your management details can be edited using the form below.</P>';

                      echo '<BR /><BR />                
                    <TABLE cellspacing="5" align="center" class="standard">
                      <FORM method="post" action="Handlers/StaffMan_Handler.php">
                        <INPUT name="Type" type="hidden" value="Maintain">
                        <TR>
                          <TD colspan="4" class="header">Edit
                          </TD>
                        </TR>
                        <TR>';
                      if (in_array($_SESSION['cUID'], ManagementAccess()))
                          echo '<TD colspan="4">To edit staff management details, specify the particulars, click Edit and complete the form that is displayed</TD>.';
                      else
                          echo '<TD colspan="3">To edit your management details, click Edit and complete the form that is displayed</TD>.'
                              .'<TD class="right">
                                        <INPUT tabindex="3" name="Submit" type="submit" class="button" value="Edit" />                   
                                      </TD>';

                      echo'</TR>';
                      if (in_array($_SESSION['cUID'], ManagementAccess())){
                          echo '<TR>
                          <TD class="short"">Staff Name:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                          BuildStaffSelector(2, 'Staff', 'standard', $_SESSION['cUID'], true, true);
                          echo '</TD>
                          <TD colspan="2" class="right">
                            <INPUT tabindex="3" name="Submit" type="submit" class="button" value="Edit" />                   
                          </TD>
                        </TR>';
                      }
                      echo'</FORM>
                    </TABLE>
                  </DIV>';

                      BuildContentHeader('View Staff', "", "", true);
                      echo '<DIV class="contentflow">';
                      if (in_array($_SESSION['cUID'], ManagementAccess()))
                          echo '<P>Staff management details can be viewed by using the form below.</P>';
                      else
                          echo '<P>Your management details can be viewed by using the form below.</P>';

                      echo'<BR /><BR />
                    <TABLE cellspacing="5" align="center" class="standard">
                      <FORM method="post" action="Handlers/StaffMan_Handler.php">
                        <INPUT name="Type" type="hidden" value="ViewDemographics">
                        <TR>
                          <TD colspan="4" class="header">View Demographics
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="2">To view staff demographic details click View.
                          </TD>
                          <TD colspan="2" class="right">
                            <INPUT tabindex="5" name="Submit" type="submit" class="button" value="View" />                   
                          </TD>
                        </TR>
                      </FORM>
                      <FORM method="post" action="Handlers/StaffMan_Handler.php">
                        <INPUT name="Type" type="hidden" value="ViewSingle">
                        <TR>
                          <TD colspan="4" class="header">View Single
                          </TD>
                        </TR>
                        <TR>';
                      if (in_array($_SESSION['cUID'], ManagementAccess()))
                          echo '<TD colspan="4">To view staff management details, specify the particulars and click View.</TD>';
                      else
                          echo '<TD colspan="3">To view your management details, click View.</TD>'
                              . '<TD class="right">
                                    <INPUT tabindex="5" name="Submit" type="submit" class="button" value="View" />                   
                                  </TD>';

                      echo'</TR>';
                      if (in_array($_SESSION['cUID'], ManagementAccess())){
                          echo '<TR>
                          <TD class="short">Staff Name:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                          BuildStaffSelector(4, 'Staff', 'standard', $_SESSION['cUID'], true, true);
                          echo '</TD>
                          <TD colspan="2" class="right">
                            <INPUT tabindex="5" name="Submit" type="submit" class="button" value="View" />                   
                          </TD>
                        </TR>';
                      }
                      echo '</FORM>
                    </TABLE>
                  </DIV>';    
          }
          //////////////////////////////////////////////////////////////////////
        ?>
                            </div>
                        </div>
                    </div>
            </section
    </DIV>
    <?php 
      // PHP SCRIPT ////////////////////////////////////////////////////////////
      BuildFooter();
      //////////////////////////////////////////////////////////////////////////
    ?>    
  </BODY>
</HTML>