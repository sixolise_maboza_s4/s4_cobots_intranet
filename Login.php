<?php
include 'Scripts/Include.php';
SetSettings();
?>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
 <?php
      // PHP SCRIPT ////////////////////////////////////////////////////////////
      BuildHead('Home');
      //////////////////////////////////////////////////////////////////////////
 ?>
</head>
<body>
<?php
// PHP SCRIPT ////////////////////////////////////////////////////////////
BuildBanner();
//////////////////////////////////////////////////////////////////////////
?>
<DIV id="main"><?php
    // PHP SCRIPT //////////////////////////////////////////////////////////
    include('Scripts/header.php');
    BuildTopBar();
    BuildMenu('Main', 'index.php');

    function GetColour($Group)
    {
        switch ($Group)
        {
            case 5:
                return 'sky';
                break;
            case 6:
                return 'rowA';
                break;
            case 8:
                return 'rowB';
                break;
            default;
                return "";
                break;
        }
    }
    ////////////////////////////////////////////////////////////////////////
    ?>
    <section id="content_wrapper">
        <?php BuildBreadCrumb();?>
        <!-- -------------- Content -------------- -->
        <section id="content" class="table-layout">
            <!-- -------------- Column Center -------------- -->
            <div class="chute chute-center" style="height: 869px;">

                <div class="row">
                    <div class="col-md-12">
                        <div class="panel">
        <?php
        // PHP SCRIPT ////////////////////////////////////////////////////////
        BuildMessageSet('Home');
        //////////////////////////////////////////////////////////////////////
        ///
        CheckStatus();
        ///
        BuildContentHeader('Login', "", "", false);
        echo '<BR style="line-height:100px;" />
                    <DIV class="contentflow">
                      <P>Use the login below to access the Cobots intranet and remember to logout once you have finished.</P>
                      <BR /><BR />
                      <DIV>
                        <TABLE cellspacing="5" align="center" class="short">
                          <FORM method="post" action="Handlers/Login.php">
                            <TR>
                              <TD colspan="2" class="header">Login Details
                              </TD>
                            </TR>
                            <TR>
                              <TD class="short">Username:
                                <SPAN class="note">*
                                </SPAN>
                              </TD>
                              <TD>
                                <INPUT tabindex="1" name="Username" type="text" class="long" />
                              </TD>
                            </TR>
                            <TR>
                              <TD class="half">Password:                              
                                <SPAN class="note">*
                                </SPAN>
                              </TD>
                              <TD>
                                <INPUT tabindex="2" name="Password" type="password" class="long" />
                              </TD>
                            </TR>
                            <TR>
                              <TD colspan="2" class="center">
                                <INPUT tabindex="3" type="submit" class="button" value="Login" />
                              </TD>
                            </TR>
                          </FORM>
                        </TABLE>
                      </DIV>
                    </DIV>
                    <DIV>
                      <BR />
                      <SPAN class="note">*
                      </SPAN>
                      These fields are required.
                      
                    </DIV>';

        ?>  </div>
                    </div>
                </div>
        </section>
</DIV>
        <?php
        // PHP SCRIPT ////////////////////////////////////////////////////////////
        BuildFooter();
        //////////////////////////////////////////////////////////////////////////
        ?>
</body>
</html>


