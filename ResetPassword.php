<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include 'Scripts/Include.php';
SetSettings();
//CheckAuthorisation('ResetPassword.php');
?>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <title>reset user password</title>
      <script src="Scripts/DevExpress/js/jquery-2.1.3.min.js" type="text/javascript"></script>
      <script src="Scripts/DevExpress/js/jszip.min.js" type="text/javascript"></script>
      <link href="Stylesheets/chosen.css" rel="stylesheet" type="text/css"/> 
    <script src="Scripts/FileScripts/ResetPassword.js"></script>
    
<?php
    BuildHead('Reset User Password');
 ?>

</head>
<body>
 <?php
      // PHP SCRIPT ////////////////////////////////////////////////////////////
      BuildBanner();
      //////////////////////////////////////////////////////////////////////////
    ?>
    <div class="contentcontainer">
        <?php 
        // PHP SCRIPT ////////////////////////////////////////////////////////// 
        BuildMenu('Main', 'ResetPassword.php');                               
        ////////////////////////////////////////////////////////////////////////
      ?>
    
    <div class="content">
    	<br/>
    	<br/>
         <?php
          // PHP SCRIPT ////////////////////////////////////////////////////////
          BuildMessageSet('ResetPassword');
          //////////////////////////////////////////////////////////////////////    
        ?>   
      
    	<div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
          <label>Staff Member:<span class="note">*</span> </label>
          <?php BuildStaffSelector(20, 'Staff', 'standard', " ", true); ?>
            <input type="submit" id="bntReset" class="btn btn-info" value="Reset Password"/></div>
        <div class="col-md-3"></div>
      </div>
	</div>
         <?php
      // PHP SCRIPT ////////////////////////////////////////////////////////////
      BuildFooter();
      //////////////////////////////////////////////////////////////////////////
    ?>
</body>
</html>
