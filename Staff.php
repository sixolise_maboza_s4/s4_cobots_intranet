<?php
  // DETAILS ///////////////////////////////////////////////////////////////////
  //                                                                          //
  //                    Last Edited By: Gareth Ambrose                        //
  //                        Date: 12 February 2010                            //
  //                                                                          //
  //////////////////////////////////////////////////////////////////////////////
  // This page allows users to manage and view staff.                         //
  //////////////////////////////////////////////////////////////////////////////
  
  include 'Scripts/Include.php';
  SetSettings();
  CheckAuthorisation('Staff.php');
  
  //////////////////////////////////////////////////////////////////////////////
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3c.org/TR/1999/REC-html401-19991224/loose.dtd">
<HTML>
  <HEAD>
    <?php
      // PHP SCRIPT ////////////////////////////////////////////////////////////
      BuildHead('Staff');
    include ('Scripts/header.php');
      //////////////////////////////////////////////////////////////////////////
    ?>
        <script>
             $(document).ready(function() {  
             $(".seltest").chosen({
			no_results_text: "No results matched"
		});
            });
        </script>  
       
       <STYLE>
           .selectDrop{
                width: 250px !important;
                text-align: left;
            }
       </STYLE>
      
  </HEAD>
  <BODY>
    <?php
      // PHP SCRIPT ////////////////////////////////////////////////////////////
      BuildBanner();
      //////////////////////////////////////////////////////////////////////////
    ?>
    <DIV id="main">
      <?php
        // PHP SCRIPT //////////////////////////////////////////////////////////
      BuildTopBar();
        BuildMenu('Main', 'Staff.php');
        ////////////////////////////////////////////////////////////////////////
      ?>
        <section id="content_wrapper">
            <?php BuildBreadCrumb($currentPath);?>
            <!-- -------------- Content -------------- -->
            <section id="content" class="table-layout">
                <!-- -------------- Column Center -------------- -->
                <div class="chute chute-center" style="height: 869px;">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel">
        <?php
          // PHP SCRIPT ////////////////////////////////////////////////////////
          BuildMessageSet('Staff');
          //////////////////////////////////////////////////////////////////////
        ?>
        <?php
          // PHP SCRIPT ////////////////////////////////////////////////////////
          if (isset($_SESSION['AddStaff']) )
          {
            BuildContentHeader('Add Staff', "", "", true);
            echo '<DIV class="contentflow">
                    <P>Enter the details of the staff member below. Ensure that all the required information is entered before submission and that this information is valid. The new password will be the first three letters of the first name in lower case and the username is the first name in lower case.</P>
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center" class="long">
                      <FORM method="post" action="Handlers/Staff_Handler.php">
                        <INPUT name="Type" type="hidden" value="Add">
                        <TR>
                          <TD colspan="4" class="header">Personal Details
                          </TD>
                        </TR>
                        <TR>
                          <TD class="short">First Name:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>
                            <INPUT tabindex="1" name="FirstName" type="text" class="text standard" maxlength="20" value="'.$_SESSION['AddStaff'][0].'" />
                          </TD>
                          <TD class="short">Contact Number (Home):
                            <SPAN class="note">**
                            </SPAN>
                          </TD>
                          <TD>
                            <INPUT tabindex="5" name="ContactHome" type="text" class="text short" value="'.$_SESSION['AddStaff'][1].'" maxlength="11" />
                          </TD>
                        </TR>
                        <TR>
                          <TD>Last Name:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>
                            <INPUT tabindex="2" name="LastName" type="text" class="text standard" maxlength="20" value="'.$_SESSION['AddStaff'][2].'" />
                          </TD>
                          <TD>Contact Number (Cell):
                            <SPAN class="note">**
                            </SPAN>
                          </TD>
                          <TD>
                            <INPUT tabindex="6" name="ContactCell" type="text" class="text short" value="'.$_SESSION['AddStaff'][3].'" maxlength="11" />
                          </TD>
                        </TR>
                        <TR>
                          <TD>ID / Passport Number:
                            <SPAN class="note">****
                            </SPAN>
                          </TD>
                          <TD>
                            <INPUT tabindex="3" name="IDNumber" type="text" class="text short" maxlength="15" value="'.$_SESSION['AddStaff'][4].'" />
                          </TD>
                          <TD>Contact Number (Work):
                            <SPAN class="note">**
                            </SPAN>
                          </TD>
                          <TD>
                            <INPUT tabindex="6" name="ContactWork" type="text" class="text short" value="'.$_SESSION['AddStaff'][5].'" maxlength="11" />
                          </TD>
                        </TR>
                        <TR>
                          <TD>Income Tax Number:
                          </TD>
                          <TD>
                            <INPUT tabindex="4" name="ITNumber" type="text" class="text short" maxlength="15" value="'.$_SESSION['AddStaff'][6].'" />
                          </TD>
                          <TD>Email address:
                          </TD>
                          <TD>
                            <INPUT tabindex="7" name="Email" type="text" class="text standard" maxlength="50" value="'.$_SESSION['AddStaff'][7].'" />
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="4" class="header">Demographic Details
                          </TD>
                        </TR>
                        <TR>
                          <TD>Gender:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                            BuildGenderSelector(7, 'DemogGender', 'short', $_SESSION['AddStaff'][36]);
                    echo '</TD>
                          <TD>Foreign National:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                            BuildYesNoSelector(8, 'DemogForeign', 'veryshort', $_SESSION['AddStaff'][37]);
                    echo '</TD>
                        </TR>
                        <TR>
                          <TD>Race:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                            BuildRaceSelector(7, 'DemogRace', 'short', $_SESSION['AddStaff'][38]);
                    echo '</TD>
                          <TD>Has a Disability:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                            BuildYesNoSelector(8, 'DemogDisabled', 'veryshort', $_SESSION['AddStaff'][39]);
                    echo '</TD>
                        </TR>
                        <TR>
                          <TD>Marital Status:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                            BuildMaritalSelector(7, 'DemogMarital', 'short', $_SESSION['AddStaff'][40]);
                    echo '</TD>
                          <TD>Smoker:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                            BuildYesNoSelector(8, 'DemogSmoker', 'veryshort', $_SESSION['AddStaff'][41]);
                    echo '</TD>
                        </TR>
                        <TR>
                          <TD colspan="4" class="header">Address Details
                          </TD>
                        </TR>
                        <TR>
                          <TD>Street 1:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>
                            <INPUT tabindex="8" name="Street1" type="text" class="text standard" maxlength="20" value="'.$_SESSION['AddStaff'][8].'" />
                          </TD>
                          <TD>Town / City:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>
                            <INPUT tabindex="11" name="TownCity" type="text" class="text standard" maxlength="20" value="'.$_SESSION['AddStaff'][9].'" />
                          </TD>
                        </TR>
                        <TR>
                          <TD>Street 2:
                          </TD>
                          <TD>
                            <INPUT tabindex="9" name="Street2" type="text" class="text standard" maxlength="20" value="'.$_SESSION['AddStaff'][10].'" />
                          </TD>
                          <TD>Country:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>
                            <INPUT tabindex="12" name="Country" type="text" class="text standard" maxlength="20" value="'.$_SESSION['AddStaff'][11].'" />
                          </TD>
                        </TR>
                        <TR>
                          <TD>Suburb:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>
                            <INPUT tabindex="10" name="Suburb" type="text" class="text standard" maxlength="20" value="'.$_SESSION['AddStaff'][12].'" />
                          </TD>
                          <TD>Postal Code:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>
                            <INPUT tabindex="13" name="PostalCode" type="text" class="text veryshort" maxlength="4" value="'.$_SESSION['AddStaff'][13].'" />
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="4" class="header">Emergency Contact Details
                          </TD>
                        </TR>
                        <TR>
                          <TD>First Name:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>
                            <INPUT tabindex="14" name="EmergencyFirstName" type="text" class="text standard" maxlength="19" value="'.$_SESSION['AddStaff'][14].'" />
                          </TD>
                          <TD>Contact Number (Home):
                            <SPAN class="note">**
                            </SPAN>
                          </TD>
                          <TD>
                            <INPUT tabindex="17" name="EmergencyContactHome" type="text" class="text short" value="'.$_SESSION['AddStaff'][15].'" maxlength="11" />
                          </TD>
                        </TR>
                        <TR>
                          <TD>Last Name:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>
                            <INPUT tabindex="15" name="EmergencyLastName" type="text" class="text standard" maxlength="20" value="'.$_SESSION['AddStaff'][16].'" />
                          </TD>
                          <TD>Contact Number (Cell):
                            <SPAN class="note">**
                            </SPAN>
                          </TD>
                          <TD>
                            <INPUT tabindex="18" name="EmergencyContactCell" type="text" class="text short" value="'.$_SESSION['AddStaff'][17].'" maxlength="11" />
                          </TD>
                        </TR>
                        <TR>
                          <TD>Relation:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>
                            <INPUT tabindex="16" name="EmergencyRelation" type="text" class="text standard" maxlength="15" value="'.$_SESSION['AddStaff'][18].'" />
                          </TD>
                          <TD>Contact Number (Work):
                            <SPAN class="note">**
                            </SPAN>
                          </TD>
                          <TD>
                            <INPUT tabindex="18" name="EmergencyContactWork" type="text" class="text short" value="'.$_SESSION['AddStaff'][19].'" maxlength="11" />
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="4" class="header">Bank Details
                          </TD>
                        </TR>
                        <TR>
                          <TD>Bank Name:
                          </TD>
                          <TD>
                            <INPUT tabindex="19" name="BankName" type="text" class="text standard" maxlength="100" value="'.$_SESSION['AddStaff'][20].'" />
                          </TD>
                          <TD>Account Number:
                          </TD>
                          <TD>
                            <INPUT tabindex="21" name="AccountNumber" type="text" class="text short" maxlength="100" value="'.$_SESSION['AddStaff'][21].'" />
                          </TD>
                        </TR>
                        <TR>
                          <TD>Branch Code:
                          </TD>
                          <TD>
                            <INPUT tabindex="20" name="BranchCode" type="text" class="text veryshort" maxlength="6" value="'.$_SESSION['AddStaff'][22].'" />
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="4" class="header">Work Details
                          </TD>
                        </TR>
                        <TR>
                          <TD>Job Description:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>
                            <INPUT tabindex="22" name="JobDescription" type="text" class="text standard" maxlength="100" value="'.$_SESSION['AddStaff'][23].'" />
                          </TD>
                          <TD>Reports to:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                            BuildStaffSelector(23, 'ReportsTo', 'standard',  $_SESSION['AddStaff'][24], true);
                    echo '</TD>
                        </TR>
                        <TR>
                          <TD class="short">Date Joined:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>'; 
                            BuildDaySelector(22, 'JoinedDay', GetDayFromSessionDate($_SESSION['AddStaff'][35]));
                            echo '&nbsp;';
                            BuildMonthSelector(22, 'JoinedMonth', GetMonthFromSessionDate($_SESSION['AddStaff'][35]));
                            echo '&nbsp;';
                            BuildYearSelector(22, 'JoinedYear', GetYearFromSessionDate($_SESSION['AddStaff'][35]));
                    echo '</TD>
                        </TR>
                        <TR>
                          <TD colspan="4" class="header">Travel Details
                          </TD>
                        </TR>
                        <TR>
                          <TD>Voyager Number:
                          </TD>
                          <TD>
                            <INPUT tabindex="24" name="VoyagerNumber" type="text" class="text short" maxlength="12" value="'.$_SESSION['AddStaff'][25].'" />
                          </TD>
                        </TR>
                        <TR>
                          <TD>Passport 1 - Type:
                            <SPAN class="note">***
                            </SPAN>
                          </TD>
                          <TD>';
                            BuildPassportTypeSelector(25, 'PassportType1', 'standard',  $_SESSION['AddStaff'][26]);
                    echo '</TD>
                          <TD>Passport 2 - Type:
                            <SPAN class="note">***
                            </SPAN>
                          </TD>
                          <TD>';
                            BuildPassportTypeSelector(30, 'PassportType2', 'standard',  $_SESSION['AddStaff'][27]);
                    echo '</TD>
                        </TR>
                        <TR>
                          <TD>Passport 1 - Number:
                            <SPAN class="note">***
                            </SPAN>
                          </TD>
                          <TD>
                            <INPUT tabindex="26" name="PassportNumber1" type="text" class="text short" maxlength="10" value="'.$_SESSION['AddStaff'][28].'" />
                          </TD>
                          <TD>Passport 2 - Number:
                            <SPAN class="note">***
                            </SPAN>
                          </TD>
                          <TD>
                            <INPUT tabindex="31" name="PassportNumber2" type="text" class="text short" maxlength="10" value="'.$_SESSION['AddStaff'][29].'" />
                          </TD>
                        </TR>
                        <TR>
                          <TD class="short">Passport 1 - Expires:
                            <SPAN class="note">***
                            </SPAN>
                          </TD>
                          <TD>'; 
                            BuildDaySelector(27, 'PassportDay1', GetDayFromSessionDate($_SESSION['AddStaff'][30]));
                            echo '&nbsp;';
                            BuildMonthSelector(28, 'PassportMonth1', GetMonthFromSessionDate($_SESSION['AddStaff'][30]));
                            echo '&nbsp;';
                            BuildYearSelector(29, 'PassportYear1', GetYearFromSessionDate($_SESSION['AddStaff'][30]));
                    echo '</TD>
                          <TD class="short">Passport 2 - Expires:
                            <SPAN class="note">***
                            </SPAN>
                          </TD>
                          <TD>'; 
                            BuildDaySelector(32, 'PassportDay2', GetDayFromSessionDate($_SESSION['AddStaff'][31]));
                            echo '&nbsp;';
                            BuildMonthSelector(33, 'PassportMonth2', GetMonthFromSessionDate($_SESSION['AddStaff'][31]));
                            echo '&nbsp;';
                            BuildYearSelector(34, 'PassportYear2', GetYearFromSessionDate($_SESSION['AddStaff'][31]));
                    echo '</TD>
                        </TR>
                        <TR>
                          <TD colspan="4" class="header">Uniform Details
                          </TD>
                        </TR>
                        <TR>
                          <TD class="short">Shirt Size:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>'; 
                            BuildSizeSelector(35, 'Size', 'veryshort', $_SESSION['AddStaff'][32]);
                    echo '</TD>
                        </TR>
                        <TR>
                          <TD colspan="4" class="header">Qualification Details
                          </TD>
                        </TR>
                        <TR>
                          <TD class="short">Type:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>'; 
                            BuildQualificationSelector(36, 'QualType', 'standard', $_SESSION['AddStaff'][33]);
                    echo '</TD>
                          <TD>Description:
                            <SPAN class="note">*****
                            </SPAN>
                          </TD>
                          <TD>
                            <INPUT tabindex="37" name="QualDescription" type="text" class="text standard" maxlength="50" value="'.$_SESSION['AddStaff'][34].'" />
                          </TD>
                        </TR>
                         <TR>
                          <TD colspan="4" class="header">SMS Group
                          </TD>
                        </TR>
                        <TR>
                          <TD class="short">SMS Group:
                            </SPAN>
                          </TD>
                          <TD colspan="3">'; 
                            getActiveStaffGroup_SMS("group", "selectDrop seltest", "SMSGroups[]", $_SESSION['AddStaff'][42],1,0);
                    echo '</TD>
                          
                        </TR>
                        <TR>
                          <TD colspan="4" class="center">
                            <INPUT tabindex="38" name="Submit" type="submit" class="button" value="Submit" />   
                            <INPUT tabindex="38" name="Submit" type="submit" class="button" value="Cancel" />                  
                          </TD>
                        </TR>
                      </FORM>
                    </TABLE>  
                  </DIV>  
                  <DIV>
                    <BR /><BR />
                    <SPAN class="note">*
                    </SPAN>
                    These fields are required.
                    <BR />
                    <SPAN class="note">**
                    </SPAN>
                    These fields must be completed as far as possible and the local number format must be used. Eg: 041-1234567
                    <BR />
                    <SPAN class="note">***
                    </SPAN>
                    If no passport is applicable then leave both passport sections blank. If only one passport is applicable either of the two sections may be used, however, all of the necessary details must be supplied.
                    <BR />
                    <SPAN class="note">****
                    </SPAN>
                    Foreign citizens can enter their passport numbers here.
                    <BR />
                    <SPAN class="note">*****
                    </SPAN>
                    NQF level should be specified here as well if known. Eg: NQF X: Electrical... where X is the NQF level.
                  </DIV>';
          } else
          if (isset($_SESSION['EditStaff']))
          {
            $row = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = "'.$_SESSION['EditStaff'][0].'"'));
            if ($_SESSION['Auth'] & 32 || ($_SESSION['cUID'] =="410"))
            {
              BuildContentHeader('Edit Staff - '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'], "", "", true);
            } else
              BuildContentHeader('Edit Staff', "", "", true);
                
            echo '<DIV class="contentflow">
                    <P>Once you have made changes, ensure that you submit them otherwise they will not take effect.<P>
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center" class="long">
                      <FORM method="post" action="Handlers/Staff_Handler.php">
                        <INPUT name="Type" type="hidden" value="Edit">
                        <TR>
                          <TD colspan="4" class="header">Personal Details
                          </TD>
                        </TR>
                        <TR>
                          <TD class="short">First Name:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>
                            <INPUT tabindex="1" name="FirstName" type="text" class="text standard" value="'.$_SESSION['EditStaff'][1].'" maxlength="20" />
                          </TD>
                          <TD class="short">Contact Number (Home):
                            <SPAN class="note">**
                            </SPAN>
                          </TD>
                          <TD>
                            <INPUT tabindex="5" name="ContactHome" type="text" class="text short"';
                            if (($_SESSION['EditStaff'][2] == "-") || ($_SESSION['EditStaff'][2] == ""))
                              echo ' value="___-_______"';
                            else
                              echo ' value="'.$_SESSION['EditStaff'][2].'"';
                      echo ' maxlength="11" />
                          </TD>
                        </TR>
                        <TR>
                          <TD>Last Name:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>
                            <INPUT tabindex="2" name="LastName" type="text" class="text standard" value="'.$_SESSION['EditStaff'][3].'" maxlength="20" />
                          </TD>
                          <TD>Contact Number (Cell):
                            <SPAN class="note">**
                            </SPAN>
                          </TD>
                          <TD>
                            <INPUT tabindex="6" name="ContactCell" type="text" class="text short"';
                            if (($_SESSION['EditStaff'][4] == "-") || ($_SESSION['EditStaff'][4] == ""))
                              echo ' value="___-_______"';
                            else
                              echo ' value="'.$_SESSION['EditStaff'][4].'"';
                      echo ' maxlength="11" />
                          </TD>
                        </TR>
                        <TR>
                          <TD>ID / Passport Number:
                            <SPAN class="note">****
                            </SPAN>
                          </TD>
                          <TD>
                            <INPUT tabindex="3" name="IDNumber" type="text" class="text short" value="'.$_SESSION['EditStaff'][5].'" maxlength="15" />
                          </TD>
                          <TD>Contact Number (Work):
                            <SPAN class="note">**
                            </SPAN>
                          </TD>
                          <TD>
                            <INPUT tabindex="6" name="ContactWork" type="text" class="text short"';
                            if (($_SESSION['EditStaff'][6] == "-") || ($_SESSION['EditStaff'][6] == ""))
                              echo ' value="___-_______"';
                            else
                              echo ' value="'.$_SESSION['EditStaff'][6].'"';
                      echo ' maxlength="11" />
                          </TD>
                        </TR>
                        <TR>
                          <TD>Income Tax Number:
                          </TD>
                          <TD>
                            <INPUT tabindex="4" name="ITNumber" type="text" class="text short" value="'.$_SESSION['EditStaff'][7].'" maxlength="15" />
                          </TD>
                          <TD>Email address:
                          </TD>
                          <TD>
                            <INPUT tabindex="7" name="Email" type="text" class="text standard" value="'.$_SESSION['EditStaff'][8].'" maxlength="50" />
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="4" class="header">Demographic Details
                          </TD>
                        </TR>
                        <TR>
                          <TD>Gender:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                            BuildGenderSelector(7, 'DemogGender', 'short', $_SESSION['EditStaff'][37]);
                    echo '</TD>
                          <TD>Foreign National:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                            BuildYesNoSelector(8, 'DemogForeign', 'veryshort', $_SESSION['EditStaff'][38]);
                    echo '</TD>
                        </TR>
                        <TR>
                          <TD>Race:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                            BuildRaceSelector(7, 'DemogRace', 'short', $_SESSION['EditStaff'][39]);
                    echo '</TD>
                          <TD>Has a Disability:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                            BuildYesNoSelector(8, 'DemogDisabled', 'veryshort', $_SESSION['EditStaff'][40]);
                    echo '</TD>
                        </TR>
                        <TR>
                          <TD>Marital Status:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                            BuildMaritalSelector(7, 'DemogMarital', 'short', $_SESSION['EditStaff'][41]);
                    echo '</TD>
                          <TD>Smoker:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                            BuildYesNoSelector(8, 'DemogSmoker', 'veryshort', $_SESSION['EditStaff'][42]);
                    echo '</TD>
                        </TR>
                        <TR>
                          <TD colspan="4" class="header">Address Details
                          </TD>
                        </TR>
                        <TR>
                          <TD>Street 1:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>
                            <INPUT tabindex="8" name="Street1" type="text" class="text standard" value="'.$_SESSION['EditStaff'][9].'" maxlength="20" />
                          </TD>
                          <TD>Town / City:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>
                            <INPUT tabindex="11" name="TownCity" type="text" class="text standard" value="'.$_SESSION['EditStaff'][10].'" maxlength="20" />
                          </TD>
                        </TR>
                        <TR>
                          <TD>Street 2:
                          </TD>
                          <TD>
                            <INPUT tabindex="9" name="Street2" type="text" class="text standard" value="'.$_SESSION['EditStaff'][11].'" maxlength="20" />
                          </TD>
                          <TD>Country:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>
                            <INPUT tabindex="12" name="Country" type="text" class="text standard" value="'.$_SESSION['EditStaff'][12].'" maxlength="20" />
                          </TD>
                        </TR>
                        <TR>
                          <TD>Suburb:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>
                            <INPUT tabindex="10" name="Suburb" type="text" class="text standard" value="'.$_SESSION['EditStaff'][13].'" maxlength="20" />
                          </TD>
                          <TD>Postal Code:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>
                            <INPUT tabindex="13" name="PostalCode" type="text" class="text veryshort" value="'.$_SESSION['EditStaff'][14].'" maxlength="4" />
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="4" class="header">Emergency Contact Details
                          </TD>
                        </TR>
                        <TR>
                          <TD>First Name:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>
                            <INPUT tabindex="14" name="EmergencyFirstName" type="text" class="text standard" value="'.$_SESSION['EditStaff'][15].'" maxlength="19" />
                          </TD>
                          <TD>Contact Number (Home):
                            <SPAN class="note">**
                            </SPAN>
                          </TD>
                          <TD>
                            <INPUT tabindex="17" name="EmergencyContactHome" type="text" class="text short"';
                            if (($_SESSION['EditStaff'][16] == "-") || ($_SESSION['EditStaff'][16] == ""))
                              echo ' value="___-_______"';
                            else
                              echo ' value="'.$_SESSION['EditStaff'][16].'"';
                    echo ' maxlength="11" />
                          </TD>
                        </TR>
                        <TR>
                          <TD>Last Name:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>
                            <INPUT tabindex="15" name="EmergencyLastName" type="text" class="text standard" value="'.$_SESSION['EditStaff'][17].'" maxlength="20" />
                          </TD>
                          <TD>Contact Number (Cell):
                            <SPAN class="note">**
                            </SPAN>
                          </TD>
                          <TD>
                            <INPUT tabindex="18" name="EmergencyContactCell" type="text" class="text short"';
                            if (($_SESSION['EditStaff'][18] == "-") || ($_SESSION['EditStaff'][18] == ""))
                              echo ' value="___-_______"';
                            else
                              echo ' value="'.$_SESSION['EditStaff'][18].'"';
                    echo ' maxlength="11" />
                          </TD>
                        </TR>
                        <TR>
                          <TD>Relation:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>
                            <INPUT tabindex="16" name="EmergencyRelation" type="text" class="text standard" value="'.$_SESSION['EditStaff'][19].'" maxlength="15" />
                          </TD>
                          <TD>Contact Number (Work):
                            <SPAN class="note">**
                            </SPAN>
                          </TD>
                          <TD>
                            <INPUT tabindex="18" name="EmergencyContactWork" type="text" class="text short"';
                            if (($_SESSION['EditStaff'][20] == "-") || ($_SESSION['EditStaff'][20] == ""))
                              echo ' value="___-_______"';
                            else
                              echo ' value="'.$_SESSION['EditStaff'][20].'"';
                      echo ' maxlength="11" />
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="4" class="header">Bank Details
                          </TD>
                        </TR>
                        <TR>
                          <TD>Bank Name:
                          </TD>
                          <TD>
                            <INPUT tabindex="19" name="BankName" type="text" class="text standard" value="'.$_SESSION['EditStaff'][21].'" maxlength="100" />
                          </TD>
                          <TD>Account Number:
                          </TD>
                          <TD>
                            <INPUT tabindex="21" name="AccountNumber" type="text" class="text short" value="'.$_SESSION['EditStaff'][22].'" maxlength="100" />
                          </TD>
                        </TR>
                        <TR>
                          <TD>Branch Code:
                          </TD>
                          <TD>
                            <INPUT tabindex="20" name="BranchCode" type="text" class="text veryshort" value="'.$_SESSION['EditStaff'][23].'" maxlength="6" />
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="4" class="header">Work Details
                          </TD>
                        </TR>
                        <TR>
                          <TD>Job Description:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>
                            <INPUT tabindex="22" name="JobDescription" type="text" class="text standard" value="'.$_SESSION['EditStaff'][24].'" maxlength="100" />
                          </TD>
                          <TD>Reports to:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                            BuildStaffSelector(24, 'ReportsTo', 'standard', $_SESSION['EditStaff'][25], true);
                    echo '</TD>
                        </TR>
                        <TR>
                          <TD class="short">Date Joined:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>'; 
                            BuildDaySelector(22, 'JoinedDay', GetDayFromSessionDate($_SESSION['EditStaff'][36]));
                            echo '&nbsp;';
                            BuildMonthSelector(22, 'JoinedMonth', GetMonthFromSessionDate($_SESSION['EditStaff'][36]));
                            echo '&nbsp;';
                            BuildYearSelector96(22, 'JoinedYear', GetYearFromSessionDate($_SESSION['EditStaff'][36]));
                    echo '</TD>
                        </TR>
                        <TR>
                          <TD colspan="4" class="header">Travel Details
                          </TD>
                        </TR>
                        <TR>
                          <TD>Voyager Number:
                          </TD>
                          <TD>
                            <INPUT tabindex="25" name="VoyagerNumber" type="text" class="text short" value="'.$_SESSION['EditStaff'][26].'" maxlength="12" />
                          </TD>
                        </TR>
                        <TR>
                          <TD>Passport 1 - Type:
                            <SPAN class="note">***
                            </SPAN>
                          </TD>
                          <TD>';
                            BuildPassportTypeSelector(26, 'PassportType1', 'standard', $_SESSION['EditStaff'][27]);
                    echo '</TD>
                          <TD>Passport 2 - Type:
                            <SPAN class="note">***
                            </SPAN>
                          </TD>
                          <TD>';
                            BuildPassportTypeSelector(31, 'PassportType2', 'standard', $_SESSION['EditStaff'][28]);
                    echo '</TD>
                        </TR>
                        <TR>
                          <TD>Passport 1 - Number:
                            <SPAN class="note">***
                            </SPAN>
                          </TD>
                          <TD>
                            <INPUT tabindex="27" name="PassportNumber1" type="text" class="text short" value="'.$_SESSION['EditStaff'][29].'" maxlength="10" />
                          </TD>
                          <TD>Passport 2 - Number:
                            <SPAN class="note">***
                            </SPAN>
                          </TD>
                          <TD>
                            <INPUT tabindex="32" name="PassportNumber2" type="text" class="text short" value="'.$_SESSION['EditStaff'][30].'" maxlength="10" />
                          </TD>
                        </TR>
                        <TR>
                          <TD class="short">Passport 1 - Expires:
                            <SPAN class="note">***
                            </SPAN>
                          </TD>
                          <TD>'; 
                            BuildDaySelector(28, 'PassportDay1', GetDayFromSessionDate($_SESSION['EditStaff'][31]));
                            echo '&nbsp;';
                            BuildMonthSelector(29, 'PassportMonth1', GetMonthFromSessionDate($_SESSION['EditStaff'][31]));
                            echo '&nbsp;';
                            BuildYearSelector(30, 'PassportYear1', GetYearFromSessionDate($_SESSION['EditStaff'][31]));
                    echo '</TD>
                          <TD class="short">Passport 2 - Expires:
                            <SPAN class="note">***
                            </SPAN>
                          </TD>
                          <TD>'; 
                            BuildDaySelector(33, 'PassportDay2', GetDayFromSessionDate($_SESSION['EditStaff'][32]));
                            echo '&nbsp;';
                            BuildMonthSelector(34, 'PassportMonth2', GetMonthFromSessionDate($_SESSION['EditStaff'][32]));
                            echo '&nbsp;';
                            BuildYearSelector(35, 'PassportYear2', GetYearFromSessionDate($_SESSION['EditStaff'][32]));
                    echo '</TD>
                        </TR>
                        <TR>
                          <TD colspan="4" class="header">Uniform Details
                          </TD>
                        </TR>
                        <TR>
                          <TD class="short">Shirt Size:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>'; 
                            BuildSizeSelector(36, 'Size', 'veryshort', $_SESSION['EditStaff'][33]);
                    echo '</TD>
                        </TR>
                        <TR>
                          <TD colspan="4" class="header">Qualification Details
                          </TD>
                        </TR>
                        <TR>
                          <TD class="short">Type:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>'; 
                            BuildQualificationSelector(37, 'QualType', 'standard', $_SESSION['EditStaff'][34]);
                    echo '</TD>
                          <TD>Description:
                            <SPAN class="note">*****
                            </SPAN>
                          </TD>
                          <TD>
                            <INPUT tabindex="38" name="QualDescription" type="text" class="text standard" maxlength="50" value="'.$_SESSION['EditStaff'][35].'" />
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="4" class="center">
                            <INPUT tabindex="39" name="Submit" type="submit" class="button" value="Submit" />   
                            <INPUT tabindex="39" name="Submit" type="submit" class="button" value="Cancel" />                   
                          </TD>
                        </TR>
                      </FORM>
                    </TABLE>  
                  </DIV>  
                  <DIV>
                    <BR /><BR />
                    <SPAN class="note">*
                    </SPAN>
                    These fields are required.
                    <BR />
                    <SPAN class="note">**
                    </SPAN>
                    These fields must be completed as far as possible and the local number format must be used. Eg: 041-1234567
                    <BR />
                    <SPAN class="note">***
                    </SPAN>
                    If no passport is applicable then leave both passport sections blank. If only one passport is applicable either of the two sections may be used, however, all of the necessary details must be supplied.
                    <BR />
                    <SPAN class="note">****
                    </SPAN>
                    Foreign citizens can enter their passport numbers here.
                    <BR />
                    <SPAN class="note">*****
                    </SPAN>
                    NQF level should be specified here as well if known. Eg: NQF X: Electrical... where X is the NQF level.
                  </DIV>';
          } else
          if (isset($_SESSION['ViewStaffList']))
          {
            Session_Unregister('ViewStaffList');
            BuildContentHeader('View Staff (All)', "", "", true);
            $resultSet = ExecuteQuery('SELECT * FROM Staff WHERE Staff_IsEmployee > "0" ORDER BY Staff_First_Name, Staff_Last_Name ASC');
            if (MySQL_Num_Rows($resultSet))
            {
              echo '<DIV class="contentflow">
                      <P></P>
                      <BR /><BR />
                      <TABLE cellspacing="5" align="center" class="long">
                        <TR>';
                          if (in_array($_SESSION['cUID'], ManagementAccess()))
                                   echo'<TD colspan="11" class="header">Staff Details</TD>';
                          else
                                   echo'<TD colspan="2" class="header">Staff Details</TD>';
                          
                        echo'</TR>';
                        if (in_array($_SESSION['cUID'], ManagementAccess())){
                        echo'
                         <TR>
                          <TD colspan="6" class="subheaderclear">Personal
                          </TD>
                          <TD colspan="4" class="subheaderclear">Emergency Contact
                          </TD>
                        </TR>';
                        }
                        echo'
                        <TR>
                          <TD class="subheader">Name
                          </TD>';
                          if (in_array($_SESSION['cUID'], ManagementAccess())){
                          echo'
                          <TD class="subheader">Position
                          </TD>
                          <TD class="subheader">Started
                          </TD>
                          <TD class="subheader">Contact (Cell)
                          </TD>
                          <TD class="subheader">Contact (Home)
                          </TD>';
                          }
                          echo'
                          <TD class="subheader">Email
                          </TD>';
                          if (in_array($_SESSION['cUID'], ManagementAccess())){
                           echo'
                          
                          <TD class="subheader">Name
                          </TD>
                          <TD class="subheader">Contact (Work)
                          </TD>
                          <TD class="subheader">Contact (Cell)
                          </TD>
                          <TD class="subheader">Contact (Home)
                          </TD>
                          <TD class="subheader">Size
                          </TD>';
                          }
                          echo'
                        </TR>';
                        $count = 0;
                        while ($row = MySQL_Fetch_Array($resultSet))
                        {
                          if ($count % 2 == 0)
                            $colour = 'rowA';
                          else
                            $colour = 'rowB';  
                          
                          echo '<TR>
                                  <TD class="'.$colour.' veryshort">'.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].'
                                  </TD>';
                                  if (in_array($_SESSION['cUID'], ManagementAccess())){
                                  echo '
                                  <TD class="'.$colour.' center veryshort">'.$row['Staff_Job_Desc'].'
                                  </TD>
                                  <TD class="'.$colour.' center veryshort">'.SPrintF('%02d', GetDayFromDatabaseDate($row['Staff_Start_Date'])).' '.Date('M', MKTime(0, 0, 0, GetMonthFromDatabaseDate($row['Staff_Start_Date']) + 1, 0, 0)).' '.GetYearFromDatabaseDate($row['Staff_Start_Date']).'
                                  </TD>
                                  <TD class="'.$colour.' center veryshort">'.$row['Staff_Phone_Mobile'].'
                                  </TD>
                                  <TD class="'.$colour.' center veryshort">'.$row['Staff_Phone_Home'].'
                                  </TD>';
                                  }
                                  echo'
                                  <TD class="'.$colour.' center veryshort">'.$row['Staff_email'].'
                                  </TD>';
                                  if (in_array($_SESSION['cUID'], ManagementAccess())){
                                  echo 
                                  '<TD class="'.$colour.' center veryshort">'.SubStr($row['Staff_Emergency_Name'], 0, StrPos($row['Staff_Emergency_Name'], ',')).' '.Trim(StrStr($row['Staff_Emergency_Name'], ','),',').'
                                  </TD>
                                  <TD class="'.$colour.' center veryshort">'.$row['Staff_Emergency_Phone_Work'].'
                                  </TD>
                                  <TD class="'.$colour.' center veryshort">'.$row['Staff_Emergency_Phone_Cell'].'
                                  </TD>
                                  <TD class="'.$colour.' center veryshort">'.$row['Staff_Emergency_Phone_Home'].'
                                  </TD>
                                  <TD class="'.$colour.' center veryshort">'.$row['Staff_Size'].'
                                  </TD>';
                                  }
                                  echo
                                '</TR>';
                          $count++;
                        }
                echo '</TABLE>
                    </DIV>';
            } else
              echo '<DIV class="contentflow">
                      <P>There are no staff listed.</P>
                      <BR /><BR />
                    </DIV>';
          } else
          if (isset($_SESSION['ViewStaffSingle']))
          {
            $row = MySQL_Fetch_Array(ExecuteQuery('SELECT Staff.*, Marital_Description FROM Staff INNER JOIN Marital ON Staff_Demographics_Marital = Marital_ID WHERE Staff_Code = "'.$_SESSION['ViewStaffSingle'][0].'"'));
            $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM StaffTravel WHERE StaffTravel_Name = "'.$_SESSION['ViewStaffSingle'][0].'"'));
            $rowTempB = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = '.$row['Staff_Reports_To'].''));
            $rowQual = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM QualificationType WHERE QualificationType_ID = "'.$row['Staff_Qualification_Type'].'"'));
            
            $loFileName = './Files/Temp/'.$_SESSION['ViewStaffSingle'][0].'.vcf';
            $loContents = 'BEGIN:VCARD'.Chr(10).
                          'VERSION:2.1'.Chr(10).
                          'N:'.$row['Staff_Last_Name'].';'.$row['Staff_First_Name'].Chr(10).
                          'FN:'.$row['Staff_First_Name'].Chr(10).
                          'TEL;HOME;VOICE:'.$row['Staff_Phone_Home'].Chr(10).
                          'TEL;CELL;VOICE:'.$row['Staff_Phone_Mobile'].Chr(10).
                          'EMAIL;PREF;INTERNET:'.$row['Staff_email'].Chr(10).
                          'END:VCARD';
            File_Put_Contents($loFileName, $loContents);
            
            BuildContentHeader('View Staff - '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'], "", "", true);
            echo '<DIV class="contentflow">
                    <P>These are the staff details. The staff member\'s contact details can be found <A href="Files/Temp/'.$_SESSION['ViewStaffSingle'][0].'.vcf">here</A> as a vCard.<P>
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center" class="standard">
                      <TR>
                        <TD colspan="4" class="header">Personal Details
                        </TD>
                      </TR>
                      <TR>
                        <TD class="short">Contact Number (Home):
                        </TD>
                        <TD class="bold">'.$row['Staff_Phone_Home'].'
                        </TD>
                        <TD class="short">Email address:
                        </TD>
                        <TD class="bold">'.$row['Staff_email'].'
                        </TD>
                      </TR>
                      <TR>
                        <TD>Contact Number (Cell):
                        </TD>
                        <TD class="bold">'.$row['Staff_Phone_Mobile'].'
                        </TD>';
                        if (($_SESSION['Auth'] & 32) || ($_SESSION['Auth'] & 64) || ($_SESSION['ViewStaffSingle'][0] == $_SESSION['cUID']) || ($_SESSION['cUID'] =="410") )
                          echo '<TD>ID / Passport Number:
                                </TD>
                                <TD class="bold">'.$row['Staff_ID_Number'].'
                                </TD>';
                echo '</TR>
                      <TR>
                        <TD>Contact Number (Work):
                        </TD>
                        <TD class="bold">'.$row['Staff_Phone_Work'].'
                        </TD>';
                        if (($_SESSION['Auth'] & 32) || ($_SESSION['Auth'] & 64) || ($_SESSION['ViewStaffSingle'][0] == $_SESSION['cUID']) || ($_SESSION['cUID'] =="410"))
                          echo '<TD>Income Tax Number:
                                </TD>
                                <TD class="bold">'.$row['Staff_IT_Number'].'
                                </TD>';
                echo '</TR>';
                      if (($_SESSION['Auth'] & 32) || ($_SESSION['Auth'] & 64) || ($_SESSION['ViewStaffSingle'][0] == $_SESSION['cUID']) || ($_SESSION['cUID'] =="410"))
                        echo '<TR>
                                <TD colspan="4" class="header">Demographic Details
                                </TD>
                              </TR>
                              <TR>
                                <TD>Gender:
                                </TD>
                                <TD class="bold">'.$row['Staff_Demographics_Gender'].'
                                </TD>
                                <TD>Foreign National:
                                </TD>
                                <TD class="bold">'.($row['Staff_Demographics_Foreign'] == 1 ? 'Yes' : 'No').'
                                </TD>
                              </TR>
                              <TR>
                                <TD>Race:
                                </TD>
                                <TD class="bold">'.$row['Staff_Demographics_Race'].'
                                </TD>
                                <TD>Has a Disability:
                                </TD>
                                <TD class="bold">'.($row['Staff_Demographics_Disabled'] == 1 ? 'Yes' : 'No').'
                                </TD>
                              </TR>
                              <TR>
                                <TD>Marital Status:
                                </TD>
                                <TD class="bold">'.$row['Marital_Description'].'
                                </TD>
                                <TD>Smoker:
                                </TD>
                                <TD class="bold">'.($row['Staff_Demographics_Smoker'] == 1 ? 'Yes' : 'No').'
                                </TD>
                              </TR>
                              <TR>
                                <TD colspan="4" class="header">Address Details
                                </TD>
                              </TR>
                              <TR>
                                <TD>Street 1:
                                </TD>
                                <TD class="bold">'.$row['Staff_Home_Address_Street'].'
                                </TD>
                                <TD>Town / City:
                                </TD>
                                <TD class="bold">'.$row['Staff_Home_Address_City'].'
                                </TD>
                              </TR>
                              <TR>
                                <TD>Street 2:
                                </TD>
                                <TD class="bold">'.$row['Staff_Home_Address_Street2'].'
                                </TD>
                                <TD>Country:
                                </TD>
                                <TD class="bold">'.$row['Staff_Home_Address_Country'].'
                                </TD>
                              </TR>
                              <TR>
                                <TD>Suburb:
                                </TD>
                                <TD class="bold">'.$row['Staff_Home_Address_Suburb'].'
                                </TD>
                                <TD>Postal Code:
                                </TD>
                                <TD class="bold">'.$row['Staff_Home_Address_Code'].'
                                </TD>
                              </TR>';
                echo '<TR>
                        <TD colspan="4" class="header">Emergency Contact Details
                        </TD>
                      </TR>
                      <TR>
                        <TD>First Name:
                        </TD>
                        <TD class="bold">'.SubStr($row['Staff_Emergency_Name'], 0, StrPos($row['Staff_Emergency_Name'], ',')).'
                        </TD>
                        <TD>Contact Number (Home):
                        </TD>
                        <TD class="bold">'.$row['Staff_Emergency_Phone_Home'].'
                        </TD>
                      </TR>
                      <TR>
                        <TD>Last Name:
                        </TD>
                        <TD class="bold">'.Trim(StrStr($row['Staff_Emergency_Name'], ','),',').'
                        </TD>
                        <TD>Contact Number (Cell):
                        </TD>
                        <TD class="bold">'.$row['Staff_Emergency_Phone_Cell'].'
                        </TD>
                      </TR>
                      <TR>
                        <TD>Relation:
                        </TD>
                        <TD class="bold">'.$row['Staff_Emergency_Relation'].'
                        </TD>
                        <TD>Contact Number (Work):
                        </TD>
                        <TD class="bold">'.$row['Staff_Emergency_Phone_Work'].'
                        </TD>
                      </TR>';
                      if (($_SESSION['Auth'] & 32) || ($_SESSION['Auth'] & 64) || ($_SESSION['ViewStaffSingle'][0] == $_SESSION['cUID']) || ($_SESSION['cUID'] =="410"))
                        echo '<TR>
                                <TD colspan="4" class="header">Bank Details
                                </TD>
                              </TR>
                              <TR>
                                <TD>Bank Name:
                                </TD>
                                <TD class="bold">'.$row['Staff_Bank_Name'].'
                                </TD>
                                <TD>Account Number:
                                </TD>
                                <TD class="bold">'.$row['Staff_Bank_Account'].'
                                </TD>
                              </TR>
                              <TR>
                                <TD>Branch Code:
                                </TD>
                                <TD class="bold">'.$row['Staff_Bank_Branch'].'
                                </TD>
                              </TR>
                              <TR>
                                <TD colspan="4" class="header">Work Details
                                </TD>
                              </TR>
                              <TR>
                                <TD>Job Description:
                                </TD>
                                <TD class="bold">'.$row['Staff_Job_Desc'].'
                                </TD>
                                <TD>Reports To:
                                </TD>
                                <TD class="bold">'.$rowTempB['Staff_First_Name'].'
                                </TD>
                              </TR>
                              <TR>
                                <TD>Date Joined:
                                </TD>
                                <TD class="bold">'.SPrintF('%02d', GetDayFromDatabaseDate($row['Staff_Start_Date'])).' '.Date('M', MKTime(0, 0, 0, GetMonthFromDatabaseDate($row['Staff_Start_Date']) + 1, 0, 0)).' '.GetYearFromDatabaseDate($row['Staff_Start_Date']).'
                                </TD>
                              </TR>
                              <TR>
                                <TD colspan="4" class="header">Travel Details
                                </TD>
                              </TR>
                              <TR>
                                <TD>Voyager Number:
                                </TD>
                                <TD class="bold">'.$rowTemp['StaffTravel_Voyager_Number'].'
                                </TD>
                              </TR>
                              <TR>
                                <TD>Passport 1 - Type:
                                </TD>
                                <TD class="bold">'.$rowTemp['StaffTravel_Passport1_Type'].'
                                </TD>
                                <TD>Passport 2 - Type:
                                </TD>
                                <TD class="bold">'.$rowTemp['StaffTravel_Passport2_Type'].'
                                </TD>
                              </TR>
                              <TR>
                                <TD>Passport 1 - Number:
                                </TD>
                                <TD class="bold">'.$rowTemp['StaffTravel_Passport1_Number'].'
                                </TD>
                                <TD>Passport 2 - Number:
                                </TD>
                                <TD class="bold">'.$rowTemp['StaffTravel_Passport2_Number'].'
                                </TD>
                              </TR>
                              <TR>
                                <TD>Passport 1 - Expires:
                                </TD>
                                <TD class="bold">'.SPrintF('%02d', GetDayFromDatabaseDate($rowTemp['StaffTravel_Passport1_ExpiryDate'])).' '.Date('M', MKTime(0, 0, 0, GetMonthFromDatabaseDate($rowTemp['StaffTravel_Passport1_ExpiryDate']) + 1, 0, 0)).' '.GetYearFromDatabaseDate($rowTemp['StaffTravel_Passport1_ExpiryDate']).'
                                </TD>
                                <TD>Passport 2 - Expires:
                                </TD>
                                <TD class="bold">'.SPrintF('%02d', GetDayFromDatabaseDate($rowTemp['StaffTravel_Passport2_ExpiryDate'])).' '.Date('M', MKTime(0, 0, 0, GetMonthFromDatabaseDate($rowTemp['StaffTravel_Passport2_ExpiryDate']) + 1, 0, 0)).' '.GetYearFromDatabaseDate($rowTemp['StaffTravel_Passport2_ExpiryDate']).'
                                </TD>
                              </TR>';
                echo '<TR>
                        <TD colspan="4" class="header">Uniform Details
                        </TD>
                      </TR>
                      <TR>
                        <TD>Shirt Size:
                        </TD>
                        <TD class="bold">'.$row['Staff_Size'].'
                        </TD>
                      </TR>
                      <TR>
                        <TD colspan="4" class="header">Qualification Details
                        </TD>
                      </TR>
                      <TR>
                        <TD>Type:
                        </TD>
                        <TD class="bold">'.$rowQual['QualificationType_Description'].'
                        </TD>
                        <TD>Description:
                        </TD>
                        <TD class="bold">'.$row['Staff_Qualification_Description'].'
                        </TD>
                      </TR>';
              echo '</TABLE>  
                  </DIV>';
            Session_Unregister('ViewStaffSingle');
          } else
          {
            BuildContentHeader('Maintenance', "", "", true);
            echo '<DIV class="contentflow">';
              if (in_array($_SESSION['cUID'], ManagementAccess()))
                echo '<P>New staff can be added using the form below. Details on staff already listed can also be edited.</P>';
              else 
                echo '<P>Your staff details can be edited using the form below.</P>';
              echo '<BR /><BR />                
                    <TABLE cellspacing="5" align="center" class="standard">
                      <FORM method="post" action="Handlers/Staff_Handler.php">
                        <INPUT name="Type" type="hidden" value="Maintain">';
                       if (in_array($_SESSION['cUID'], ManagementAccess()) || $_SESSION['cUID'] == 405)
                          echo '<TR>
                                  <TD colspan="4" class="header">Add
                                  </TD>
                                </TR>
                                <TR>
                                  <TD colspan="3">To add a new staff member, click Add and complete the form that is displayed.
                                  </TD>
                                  <TD class="right">
                                    <INPUT tabindex="1" name="Submit" type="submit" class="button" value="Add" />
                                  </TD>
                                </TR>';
                  echo '<TR>
                          <TD colspan="4" class="header">Edit
                          </TD>
                        </TR>';
                        if (in_array($_SESSION['cUID'], ManagementAccess()))
                        {
                          echo '<TR>
                                  <TD colspan="4">To edit a staff member, specify the particulars, click Edit and complete the form that is displayed.
                                  </TD>
                                </TR>
                                <TR>
                                  <TD class="short"">Staff Name:
                                    <SPAN class="note">*
                                    </SPAN>
                                  </TD>
                                  <TD>';
                                    BuildStaffSelector(2, 'Staff', 'standard', $_SESSION['cUID'], true, true);
                            echo '</TD>
                                  <TD colspan="2" class="right">
                                    <INPUT tabindex="3" name="Submit" type="submit" class="button" value="Edit" />                   
                                  </TD>
                                </TR>';
                        } else
                          echo'<TR>
                                  <TD colspan="3">To edit your staff details, click Edit.
                                  </TD>
                                  <TD class="right">
                                    <INPUT tabindex="3" name="Submit" type="submit" class="button" value="Edit" />                   
                                  </TD>
                                </TR>';
                echo '</FORM>
                    </TABLE>
                  </DIV>';
                    
            BuildContentHeader('View Staff', "", "", true); 
            echo '<DIV class="contentflow">';
                    if (in_array($_SESSION['cUID'], ManagementAccess()))
                        echo '<P>Staff can be viewed by using the form below.</P>';
                    else
                        echo '<P>Your details can be viewed by using the form below.</P>';
               echo'<BR /><BR />
                    <TABLE cellspacing="5" align="center" class="standard">
                      <FORM method="post" action="Handlers/Staff_Handler.php">
                        <INPUT name="Type" type="hidden" value="ViewSingle">
                        <TR>
                          <TD colspan="4" class="header">View Single
                          </TD>
                        </TR>
                        <TR>';
                          if (in_array($_SESSION['cUID'], ManagementAccess()))
                                echo '<TD colspan="4">To view staff, specify the particulars and click View.</TD>';
                          else{
                                echo '<TD colspan="3">To view staff details, click View.</TD>'
                                     .'<TD class="right">
                                            <INPUT tabindex="5" name="Submit" type="submit" class="button" value="View" />                   
                                      </TD>';
                          }
 
                     echo'</TR>';
                     if (in_array($_SESSION['cUID'], ManagementAccess())){ 
                        echo '<TR>
                          <TD class="short">Staff Name:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                            BuildStaffSelector(4, 'Staff', 'standard', $_SESSION['cUID'], true);
                    echo '</TD>
                          <TD colspan="2" class="right">
                            <INPUT tabindex="5" name="Submit" type="submit" class="button" value="View" />                   
                          </TD>
                        </TR>';
                       }
                     echo'</FORM>
                       <FORM method="post" action="Handlers/Staff_Handler.php">
                        <INPUT name="Type" type="hidden" value="ViewList">
                        <TR>
                          <TD colspan="4" class="header">View List
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="2">To view staff, click View.
                          </TD>
                          <TD colspan="2" class="right">
                            <INPUT tabindex="5" name="Submit" type="submit" class="button" value="View" />                   
                          </TD>
                        </TR>
                      </FORM>
                     </TABLE>
                  </DIV>';    
          }
          //////////////////////////////////////////////////////////////////////
        ?>
      </div>
    </div>
    </div>
    </section
    </DIV>
    <?php 
      // PHP SCRIPT ////////////////////////////////////////////////////////////
      BuildFooter();
      //////////////////////////////////////////////////////////////////////////
    ?>    
  </BODY>
</HTML>