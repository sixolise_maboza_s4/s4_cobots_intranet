<?php
  // DETAILS ///////////////////////////////////////////////////////////////////
  //                                                                          //
  //                    Last Edited By: Gareth Ambrose                        //
  //                        Date: 14 October 2009                             //
  //                                                                          //
  //////////////////////////////////////////////////////////////////////////////
  // This page is the home page of intranet users.                            //
  //////////////////////////////////////////////////////////////////////////////
ob_start();
  include 'Scripts/Include.php';
  SetSettings();
  
  //////////////////////////////////////////////////////////////////////////////
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3c.org/TR/1999/REC-html401-19991224/loose.dtd">
<HTML>
  <HEAD>


    <?php
    include ('Scripts/header.php');
      // PHP SCRIPT ////////////////////////////////////////////////////////////
      BuildHead('Home');
      //////////////////////////////////////////////////////////////////////////
    ?>   
 <meta http-equiv="content-type" content="text/html; charset=utf-8">
  </HEAD>
  <BODY>


    <?php 
      if($_SESSION['OLD_UID'])
              $_SESSION['cUID'] = $_SESSION['OLD_UID'];
      unset($_SESSION['OLD_UID']);
      // PHP SCRIPT ////////////////////////////////////////////////////////////
      BuildBanner();
      //////////////////////////////////////////////////////////////////////////
    ?>    
    <DIV id="main">


      <?php 
        // PHP SCRIPT //////////////////////////////////////////////////////////
      BuildTopBar();
	BuildMenu('Main', 'index.php');
        
        function GetColour($Group)
        {
          switch ($Group)
          {

            case 5:
              return 'sky';
              break;
            case 6:
              return 'rowA';
              break;
            case 8:
              return 'rowB';
              break;
            default;
              return "";
              break;
          }
        }
        ////////////////////////////////////////////////////////////////////////
      ?>
      <section id="content_wrapper">
          <!-- -------------- Topbar -------------- -->
          <header id="topbar" class="alt">
              <div class="topbar-left">
                  <ol class="breadcrumb">
                      <li class="breadcrumb-icon">
                          <a href="index.php"><span class="fa fa-home"></span></a>
                      </li>
                      <li class="breadcrumb-active">
                          <a href="#">COBOTS Intranet</a>
                      </li>
                      <li class="breadcrumb-current-item">
                       /  <?php echo $currentPath;?>
                      </li>
                  </ol>
              </div>
          </header>
          <!-- -------------- /Topbar -------------- -->
          <!-- -------------- Content -------------- -->
          <section id="content" class="table-layout">
              <!-- -------------- Column Center -------------- -->
              <div class="chute chute-center" style="height: 869px;">

                  <div class="row">
                      <div class="col-md-12">
                          <div class="panel">
        <?php
          // PHP SCRIPT ////////////////////////////////////////////////////////
          BuildMessageSet('Home');
          //////////////////////////////////////////////////////////////////////    
        ?>
<!--        --><?php //
//          // PHP SCRIPT ////////////////////////////////////////////////////////
//          CheckStatus();
//
//          UpdateReminders();
//          //////////////////////////////////////////////////////////////////////
//        ?><!--              -->
        

	<?php 
          // PHP SCRIPT ////////////////////////////////////////////////////////
         // if (isset($_SESSION['cAuth']))
                //BuildNewsSection('index.php', "", false, false);
          //////////////////////////////////////////////////////////////////////
        ?>  
        <?php
          // PHP SCRIPT ////////////////////////////////////////////////////////
          if (Connect())
          {
            //The database is accessible.
            if (!isset($_SESSION['cAuth']))
            {
//                echo  '
//                <SCRIPT type="text/javascript" language="JavaScript">
//
//                     window.location = "Login.php";
//
//                </SCRIPT>
//                ';
            } else
            {
              $countA = 0;
              $date = Date('Y-m-d');
              BuildContentHeader('Today\'s Reminders', "", "", true);
              
              if ($_SESSION['cAuth'] & 64)
              {
                $resultSet = ExecuteQuery('SELECT Reminder.*, ReminderPeriod_Description FROM Reminder, ReminderPeriod WHERE ReminderStatus  = 0 AND Reminder_Period = ReminderPeriod_ID AND (Reminder_Type = "3" OR Reminder_Auth = "32") AND Reminder_Trigger_Date = "'.$date.'" ORDER BY Reminder_Description');
                if (MySQL_Num_Rows($resultSet) > 0)
                {
                  echo '<DIV class="contentflow">
                          <BR /><BR />
                          <TABLE cellspacing="5" align="center" class="long">
                            <TR>
                              <TD colspan="4" class="header">Admin Reminders
                              </TD>
                            </TR>
                            <TR>
                              <TD class="subheader veryshort">Occurs
                              </TD>
                              <TD class="subheader">Description
                              </TD>
                              <TD class="subheader veryshort">Last Updated By
                              </TD>
                            </TR>';
                            $count = 0;
                            while ($row = MySQL_Fetch_Array($resultSet))
                            {
                              if ($count % 2 == 0)
                                $colour = 'rowA';
                              else
                                $colour = 'rowB'; 
                              
                              if ($row['Reminder_Updated_Name'] != "")
                                $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = '.$row['Reminder_Updated_Name'].''));
                              else
                                $rowTemp = array();
                              echo '<TR>
                                      <TD class="'.$colour.' center">'.$row['ReminderPeriod_Description'].'
                                      </TD>
                                      <TD class="'.$colour.'">';
                                        DisplayReminderDescription($row);
                                echo '</TD>
                                      <TD class="'.$colour.' center">'.$rowTemp['Staff_First_Name'].' '.$rowTemp['Staff_Last_Name'].'
                                      </TD>
                                    </TR>';
                              $count++;
                            }
                    echo '</TABLE>
                        </DIV>';
                } else
                  $countA++;
              } else
                $countA++;
                
              if ($_SESSION['cAuth'] & 64)
              {
                $resultSet = ExecuteQuery('SELECT Reminder.*, ReminderPeriod_Description FROM Reminder, ReminderPeriod WHERE Reminder_Period = ReminderPeriod_ID AND (Reminder_Type = "4" OR Reminder_Auth = "64") AND Reminder_Trigger_Date = "'.$date.'" ORDER BY Reminder_Description');
                if (MySQL_Num_Rows($resultSet) > 0)
                {
                  echo '<DIV class="contentflow">
                          <BR /><BR />
                          <TABLE cellspacing="5" align="center" class="long">
                            <TR>
                              <TD colspan="4" class="header">Management Reminders
                              </TD>
                            </TR>
                            <TR>
                              <TD class="subheader veryshort">Occurs
                              </TD>
                              <TD class="subheader">Description
                              </TD>
                              <TD class="subheader veryshort">Last Updated By
                              </TD>
                            </TR>';
                            $count = 0;
                            while ($row = MySQL_Fetch_Array($resultSet))
                            {
                              if ($count % 2 == 0)
                                $colour = 'rowA';
                              else
                                $colour = 'rowB'; 
                              
                              if ($row['Reminder_Updated_Name'] != "")
                                $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = '.$row['Reminder_Updated_Name'].''));
                              else
                                $rowTemp = array();
                              echo '<TR>
                                      <TD class="'.$colour.' center">'.$row['ReminderPeriod_Description'].'
                                      </TD>
                                      <TD class="'.$colour.'">';
                                      DisplayReminderDescription($row);
                                echo '</TD>
                                      <TD class="'.$colour.' center">'.$rowTemp['Staff_First_Name'].' '.$rowTemp['Staff_Last_Name'].'
                                      </TD>
                                    </TR>';
                              $count++;
                            }
                    echo '</TABLE>
                        </DIV>';
                } else
                  $countA++;
              } else
                $countA++;
              
              $resultSet = ExecuteQuery('SELECT Reminder.*, ReminderPeriod_Description FROM Reminder, ReminderPeriod WHERE Reminder_Period = ReminderPeriod_ID AND Reminder_Trigger_Name = '.$_SESSION['cUID'].' AND Reminder_Trigger_Date = "'.$date.'" ORDER BY Reminder_Description');
              if (MySQL_Num_Rows($resultSet) > 0)
              {
                echo '<DIV class="contentflow">
                        <BR /><BR />
                        <TABLE cellspacing="5" align="center" class="long">
                          <TR>
                            <TD colspan="4" class="header">Personal Reminders
                            </TD>
                          </TR>
                          <TR>
                            <TD class="subheader veryshort">Occurs
                            </TD>
                            <TD class="subheader">Description
                            </TD>
                            <TD class="subheader veryshort">Last Updated By
                            </TD>
                          </TR>';
                          $count = 0;
                          while ($row = MySQL_Fetch_Array($resultSet))
                          {
                            if ($count % 2 == 0)
                              $colour = 'rowA';
                            else
                              $colour = 'rowB'; 
                            
                            if ($row['Reminder_Updated_Name'] != "")
                              $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = '.$row['Reminder_Updated_Name'].''));
                            else
                              $rowTemp = array();
                            echo '<TR>
                                    <TD class="'.$colour.' center">'.$row['ReminderPeriod_Description'].'
                                    </TD>
                                    <TD class="'.$colour.'">';
                                      DisplayReminderDescription($row);
                              echo '</TD>
                                    <TD class="'.$colour.' center">'.$rowTemp['Staff_First_Name'].' '.$rowTemp['Staff_Last_Name'].'
                                    </TD>
                                  </TR>';
                            $count++;
                          }
                  echo '</TABLE>
                      </DIV>';
              } else
                $countA++;
              
              $resultSet = ExecuteQuery('SELECT Reminder.*, ReminderPeriod_Description FROM Reminder, ReminderPeriod WHERE ReminderStatus = 0 Reminder_Period = ReminderPeriod_ID AND Reminder_Type = "2" AND Reminder_Trigger_Date = "'.$date.'" ORDER BY Reminder_Description');
              if (MySQL_Num_Rows($resultSet) > 0)
              {
                echo '<DIV class="contentflow">
                        <BR /><BR />
                        <TABLE cellspacing="5" align="center" class="long">
                          <TR>
                            <TD colspan="4" class="header">Staff Reminders
                            </TD>
                          </TR>
                          <TR>
                            <TD class="subheader veryshort">Occurs
                            </TD>
                            <TD class="subheader">Description
                            </TD>
                            <TD class="subheader veryshort">Last Updated By
                            </TD>
                          </TR>';
                          $count = 0;
                          while ($row = MySQL_Fetch_Array($resultSet))
                          {
                            if ($count % 2 == 0)
                              $colour = 'rowA';
                            else
                              $colour = 'rowB'; 
                            
                            if ($row['Reminder_Updated_Name'] != "")
                              $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = '.$row['Reminder_Updated_Name'].''));
                            else
                              $rowTemp = array();
                            echo '<TR>
                                    <TD class="'.$colour.' center">'.$row['ReminderPeriod_Description'].'
                                    </TD>
                                    <TD class="'.$colour.'">';
                                    DisplayReminderDescription($row);
                              echo '</TD>
                                    <TD class="'.$colour.' center">'.$rowTemp['Staff_First_Name'].' '.$rowTemp['Staff_Last_Name'].'
                                    </TD>
                                  </TR>';
                            $count++;
                          }
                  echo '</TABLE>
                      </DIV>';
              } else
                $countA++;
              
              if ($countA == 4)
                echo '<DIV class="contentflow">
                        <P>There are no reminders for today.</P>
                      </DIV>
                      <BR /><BR />';
              
              $resultSet = ExecuteQuery('SELECT *, DATEDIFF(Task_RevisedDueDate, "'.Date('Y-m-d H:i:s').'") AS DaysOverdue, DATEDIFF(Task_RevisedDueDate, Task_LastUpdate) AS DaysOver FROM Task WHERE Task_Name = "'.$_SESSION['cUID'].'" AND Task_Status <> 1 AND Task_Status <> 3 ORDER BY Task_ID ASC');
              if (MySQL_Num_Rows($resultSet) > 0)
              {
                BuildContentHeader('Current Tasks', "", "", true);
                echo '<DIV class="contentflow">
                        <BR /><BR />
                        <TABLE cellspacing="5" align="center" class="long">
                          <TR>
                            <TD colspan="5" class="header">Task Details
                            </TD>
                          </TR>
                          <TR>
                            <TD class="subheader">Title
                            </TD>
                            <TD class="subheader">Due Date
                            </TD>
                            <TD class="subheader">Last Updated
                            </TD>
                            <TD class="subheader">Status
                            </TD>
                            <TD class="subheader">Priority
                            </TD>
                          </TR>';
                          while ($row = MySQL_Fetch_Array($resultSet))
                          {
                            if ($count % 2 == 0)
                              $colour = 'rowA';
                            else
                              $colour = 'rowB';
                            
                            echo '<TR>
                                    <TD class="'.$colour.'"><A href="Tasks.php?view='.$row['Task_ID'].'">'.$row['Task_Title'].'</A>
                                    </TD>
                                    <TD class="'.$colour.' center">'.GetTextualDateFromDatabaseDate($row['Task_RevisedDueDate']).'
                                    </TD>
                                    <TD class="'.$colour.' center">'.GetTextualDateTimeFromDatabaseDateTime($row['Task_LastUpdate']).'
                                    </TD>
                                    <TD class="'.$colour.' center">';
                                      switch ($row['Task_Status'])
                                      {
                                        case '0':
                                          echo 'Pending';
                                          break;
                                        case '1':
                                          echo 'Complete'; 
                                          break;  
                                        case '2':
                                          echo 'Follow-Up';
                                          break;
                                        default:
                                          echo 'Geh!';
                                          break;
                                      }               
                              echo '</TD>
                                    <TD class="'.$colour.$bold.' center">'.$row['Task_Priority'].'
                                    </TD>
                                  </TR>';
                            $count++;
                          }  
                  echo '</TABLE>
                      </DIV>';
              }
              


if ($_SESSION['cAuth'] & 64)
              {
                $row = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Financial ORDER BY Financial_DateTime DESC LIMIT 1')); 
				
				
				/* gets the data from a URL */
				function get_data($url) {
					$ch = curl_init();
					$timeout = 5;
					curl_setopt($ch, CURLOPT_URL, $url);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
					$data = curl_exec($ch);
					curl_close($ch);
					return $data;
				}
				
				$pound = get_data("http://www.google.com/ig/calculator?hl=en&q=1GBP=?ZAR");
				$euro = get_data("http://www.google.com/ig/calculator?hl=en&q=1EUR=?ZAR");
				$dollar = get_data("http://www.google.com/ig/calculator?hl=en&q=1USD=?ZAR");
				$yuan = get_data("http://www.google.com/ig/calculator?hl=en&q=1CNY=?ZAR");

				$matchesPound = array();
				$matchesEuro = array();
				$matchesDollar = array();
				$matchesYuan = array();

				preg_match("/[0-9]{1,3}\.[0-9]{1,6}/", $pound, $matchesPound);
				preg_match("/[0-9]{1,3}\.[0-9]{1,6}/", $euro, $matchesEuro);
				preg_match("/[0-9]{1,3}\.[0-9]{1,6}/", $dollar, $matchesDollar);
				preg_match("/[0-9]{1,3}\.[0-9]{1,6}/", $yuan, $matchesYuan);

				$pound = round($matchesPound[0],2);
				$euro = round($matchesEuro[0],2);
				$dollar = round($matchesDollar[0],2);
				$yuan = round($matchesYuan[0],2);

				
//                //BuildContentHeader('Exchange Rates ('.date("l, d M y").')', "", "", true);
//                echo '<DIV class="contentflow">
//                        <BR /><BR />
//                        <TABLE cellspacing="5" align="center" class="short">
//                          <INPUT name="Type" type="hidden" value="Maintain">
//                          <TR>
//                            <TD colspan="2" class="header">Exhange rates
//                            </TD>
//                          </TR>
//                          <TR>
//                            <TD class="subheader standard">Value
//                            </TD>
//                            <TD class="subheader standard">Description
//                            </TD>
//                          </TR>
//                          <TR>
//                            <TD class="rowA">&#8364;1 to R'.$euro.'
//                            </TD>
//                            <TD class="rowA">Euro.
//                            </TD>
//                          </TR>
//                          <TR>
//                            <TD class="rowB">&#163;1 to R'.$pound.'
//                            </TD>
//                            <TD class="rowB">GB Pound.
//                            </TD>
//                          </TR>
//                          <TR>
//                            <TD class="rowA">$1 to R'.$dollar.'
//                            </TD>
//                            <TD class="rowA">US Dollar
//                            </TD>
//                          </TR>
//                          <TR>
//                            <TD class="rowB">RMB1 to R'.$yuan.'
//                            </TD>
//                            <TD class="rowB">Chinese Yuan
//                            </TD>
//                          </TR>
//                          <TR>
//                            <TD class="rowB">R'.$row['Financial_Fuel_Price'].' per litre
//                            </TD>
//                            <TD class="rowB">Fuel Price
//                            </TD>
//                          </TR>
//                        </TABLE>
//                      </DIV>';
              }
              
              
              $resultSet = ExecuteQuery('SELECT * FROM Staff WHERE SUBSTRING(Staff_ID_Number, 3, 2) = '.Date('m').' AND SUBSTRING(Staff_ID_Number, 5, 2) = '.Date('d').' AND Staff_IsEmployee > 0 ORDER BY Staff_First_Name ASC');
              if (MySQL_Num_Rows($resultSet) > 0)
              {
                BuildContentHeader('Today\'s Birthdays', "", "", true);
                echo '<DIV class="contentflow">
                        <BR /><BR />
                        <TABLE cellspacing="5" align="center" class="short">
                          <TR>
                            <TD colspan="4" class="header">Birthdays
                            </TD>
                          </TR>';
                          $count = 0;
                          while ($row = MySQL_Fetch_Array($resultSet))
                          {
                            if ($count % 2 == 0)
                              $colour = 'rowA';
                            else
                              $colour = 'rowB'; 
                            
                            echo '<TR>
                                    <TD class="'.$colour.' center">'.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].'
                                    </TD>
                                  </TR>';
                            $count++;
                          }
                  echo '</TABLE>
                      </DIV>';
              }
              
              $resultSet = ExecuteQuery('SELECT * FROM Poll WHERE Poll_Close_DateTime > "'.Date('Y-m-d H:i:s').'" AND Poll_IsApproved = "1"');
              if (MySQL_Num_Rows($resultSet) > 0)
              {
                $polls = 0;
                while ($row = MySQL_Fetch_Array($resultSet))
                {
                  $resultSetTemp = ExecuteQuery('SELECT * FROM PollVoted WHERE PollVoted_Name = '.$_SESSION['cUID'].' AND PollVoted_Poll = "'.$row['Poll_ID'].'"');
                  if (MySQL_Num_Rows($resultSetTemp) <= 0)
                    $polls++;
                    break;
                }
                if ($polls)
                {
                  BuildContentHeader('Current Polls', "", "", true);
                  $resultSet = ExecuteQuery('SELECT * FROM Poll WHERE Poll_Close_DateTime > "'.Date('Y-m-d H:i:s').'" AND Poll_IsApproved = "1"');
                  echo '<DIV class="contentflow">
                          <P>These are the polls currently open. To vote for a poll select an option and click Vote. You may only vote for a particular poll once.</P>';
                          while ($row = MySQL_Fetch_Array($resultSet))
                          {
                            $resultSetTemp = ExecuteQuery('SELECT * FROM PollVoted WHERE PollVoted_Name = '.$_SESSION['cUID'].' AND PollVoted_Poll = '.$row['Poll_ID'].'');
                            if (MySQL_Num_Rows($resultSetTemp) <= 0)
                            {
                              echo '<BR /><BR />
                                    <BR /><BR />
                                    <TABLE cellspacing="5" align="center" class="short">
                                      <FORM method="post" action="Handlers/Polls_Handler.php">
                                        <INPUT name="Type" type="hidden" value="Vote">
                                        <INPUT name="Poll" type="hidden" value="'.$row['Poll_ID'].'">
                                        <INPUT name="Home" type="hidden" value="1">
                                        <TR>
                                          <TD class="header">'.$row['Poll_Title'].'
                                          </TD>
                                        </TR>
                                        <TR>
                                          <TD class="center">'.SetBreaks($row['Poll_Description']).'
                                          </TD>
                                        </TR>';
                                        $resultSetTemp = ExecuteQuery('SELECT * FROM PollOption WHERE PollOption_Poll = '.$row['Poll_ID'].' ORDER BY PollOption_ID ASC');
                                        while ($rowTemp = MySQL_Fetch_Array($resultSetTemp))
                                        {
                                          echo '<TR>
                                                  <TD>
                                                    <INPUT tabindex="'.($count++).'" name="Option" type="radio" value="'.$rowTemp['PollOption_ID'].'">'.$rowTemp['PollOption_Description'].'</INPUT>
                                                  </TD>
                                                </TR>';
                                        }
                                  echo '<TR>
                                          <TD class="bold center">Poll closes: '.GetTextualDateTimeFromDatabaseDateTime($row['Poll_Close_DateTime']).'
                                          </TD>
                                        </TR>
                                        <TR>
                                          <TD class="center">
                                            <INPUT tabindex="'.($count++).'" name="Submit" type="submit" class="button" value="Vote" />                   
                                          </TD>
                                        </TR>
                                      </FORM>
                                    </TABLE>';
                            }
                          }
                  echo '</DIV>';
                }
              }

              $resultSet = ExecuteQuery('SELECT * FROM News ORDER BY News_DateTime DESC LIMIT 1');
              if (MySQL_Num_Rows($resultSet) > 0)
              {
                $row = MySQL_Fetch_Array($resultSet);
                BuildContentHeader('S4 News ('.GetTextualDateFromDatabaseDate($row['News_DateTime']).')', "", "", true);
                echo '<DIV class="contentflow">
                        <BR /><BR />

               <TABLE cellspacing="5" align="center" class="long">
                          <TR>
                            <TD colspan="4" class="header">News
                            </TD>
                          </TR>
                          <TR>
                            <TD class="subheader veryshort">Date
                            </TD>
                            <TD class="subheader">Title
                            </TD>
                            <TD class="subheader">Description
                            </TD>
                            <TD class="subheader">Submitted By
                            </TD>
                          </TR>';
                          $resultSet = ExecuteQuery('SELECT * FROM News ORDER BY News_DateTime DESC LIMIT 5');
                          $count = 0;
                          while ($row = MySQL_Fetch_Array($resultSet))
                          {
                            if ($count % 2 == 0)
                              $colour = 'rowA';
                            else
                              $colour = 'rowB'; 
                            
                            $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = '.$row['News_SubmittedBy'].''));
                            echo '<TR>
                                    <TD class="'.$colour.' center">'.GetTextualDateFromDatabaseDate($row['News_DateTime']).'
                                    </TD>
                                    <TD class="'.$colour.' center">'.$row['News_Title'].'
                                    </TD>
                                    <TD class="'.$colour.' center">'.iconv('Windows-1252', 'UTF-8//TRANSLIT', $row['News_Brief']).'
                                    </TD>
                                    <TD class="'.$colour.' center">'.$rowTemp['Staff_First_Name'].' '.$rowTemp['Staff_Last_Name'].'
                                    </TD>
                                  </TR>';
                            $count++;
                          }
                  echo '</TABLE>
                      </DIV>';
              }
            }
          } else
          {
            BuildContentHeader('Login', "", "", false);     
            echo '<DIV class="contentflow">
                      <P>The intranet is currently down. Sorry for the inconvenience.</P>';   
          }
          //////////////////////////////////////////////////////////////////////
        ?>
                          </div>
                      </div>
                  </div>
              </div>
      </section>
      </section>
    </DIV>
    <?php 
      // PHP SCRIPT ////////////////////////////////////////////////////////////
      BuildFooter();
      //////////////////////////////////////////////////////////////////////////
    ?>    
  </BODY>

</HTML>