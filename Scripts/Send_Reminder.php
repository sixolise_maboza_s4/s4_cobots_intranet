<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$number_of_days_before = 1;
$todays_date = date( "Ymd" );
//$year = substr($todays_date, 0, 4);
$month = substr($todays_date, 4, 2);
//$date = substr($todays_date, 6, 2);
//$trigger_date = date("Y-m-d", mktime (0,0,0,$month,$date+$number_of_days_before,$year)); //2 days before today

$reminder_date = "";//fill with the date from database
$upcomingReminder = "";
$reminderId = "";


$result = ExecuteQuery('SELECT * FROM OvertimeReminder WHERE MONTH(reminder_date) =  "'.$month.'" ');
$nr = mysql_num_rows($result);

while($row = MySQL_Fetch_Array($result)){
    $upcomingReminder = GetTextualDateFromDatabaseDate($row['reminder_date']);
    $reminderId = $row['reminder_id'];

}
if(!empty($nr)){
    //update sql to delete so that it only sends the reminder once
   // $sql = ExecuteQuery('DELETE FROM OvertimeReminder where reminder_date <= "'.$trigger_date.'" ORDER BY reminder_date ASC');
}

$trigDate = strtotime ( '-1 day' , strtotime ( $upcomingReminder ) ) ;
$trigDate = date('Y-m-j', $trigDate);


if($trigDate == $todays_date  ){
    $reminderDay = date('l', $upcomingReminder);
    global $Header;
$Header .= 'From: Automated Mailing Script<admin@s4integration.co.za>'.Chr(10).
            //  'Reply-To: Do-Not-Reply<andrew@s4integration.co.za>'.Chr(10).
             'Reply-To: Do-Not-Reply<sixolise.maboza@s4integration.co.za>'.Chr(10).
              'Return-Path: Automated Mailing Script<admin@s4integration.co.za>'.Chr(10).
              'X-Mailer: PHP/'.PHPVersion().Chr(10).
              'MIME-Version: 1.0'.Chr(10).
              'Content-Type: multipart/mixed; boundary="'.$Boundary.'"'.Chr(10).Chr(10);

$ToAddress = "sixolise.maboza@s4.co.za";//"staff@s4.co.za"
$Subject = date('F')." Expenses"; //expenses for the current month

$Reminder_message = "Hi all "
        . "<br/> <br/>Expenses should be sent to Louise no later than <b>08h00 on</b> <u>$reminderDay, $upcomingReminder</u>. ";
$Reminder_message .= "Please ensure that overtime requests are logged for approval by <u>08h00</u> in order for Managers to review before the expenses deadline.";
$Reminder_message .=" <br/> Please ensure all outstanding leave requests are booked. Managers please attend to leave requests."
        . " <br/>";
$Reminder_message .= " <br/> <b>If you would prefer your overtime to be paid into an S4 savings account, please email Louise by the above deadline and state the percentage that should be saved(e.g 100% or less)";
$Reminder_message .= " <br/> Of the percentage requested, 30% will be subtracted across the board, and the remainder will be saved (this is in lieu of tax which changes based on annual income)."
        . " <br/> If your tax is less than 30%, the balance of that will be included with your salary (you will not lose it).";
$Reminder_message .= " <br/>  Payslips should please be checked thoroughly each month and any queries should be brought to my attention immediately </b>";

$Reminder_message .=" <br/>  <br/> Please contact Louise  should you have any questions/concerns with regards to the above at louise.vanschalkwyk@s4.co.za";

SendMailHTML($ToAddress,$Subject," ", $Reminder_message);

//    $sql = ExecuteQuery('DELETE FROM OvertimeReminder where reminder_id = "'.$reminderId.'" ');
//    ExecuteQuery($sql);
    
}






