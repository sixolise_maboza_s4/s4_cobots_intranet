<?php
  // DETAILS ///////////////////////////////////////////////////////////////////
  //                                                                          //
  //                    Last Edited By: Gareth Ambrose                        //
  //                        Date: 28 March 2008                               //
  //                                                                          //
  ////////////////////////////////////////////////////////////////////////////// 
  // This scripting page provides functionality for building and displaying   //
  // graphs and charts.                                                       //
  //////////////////////////////////////////////////////////////////////////////
  ini_set('memory_limit', -1);
  ini_set('max_execution_time', 0);
  
  include 'Graphing/jpgraph.php'; //Primary graphing file.
  include 'Graphing/jpgraph_bar.php'; //Allows for building bar graphs.
  include 'Graphing/jpgraph_gantt.php'; //Allows for building gannt charts.
  include 'Graphing/jpgraph_line.php'; //Allows for building line graphs/plots.
  include 'Graphing/jpgraph_pie.php'; //Allows for building pie charts. Used by 3D pie charts.
  include 'Graphing/jpgraph_pie3d.php'; //Allows for building 3d pie charts.
  
  include 'Database.php'; //Database access.
  include 'Settings.php'; //Controls caching and sessions.
  SetSettings();
  
  //Check if a graph must be displayed and build it.
  if (isset($_SESSION['Graphs']))
  {
    if ($_GET['Graph'] >= 0)
    {
      switch ($_SESSION['Graphs'][$_GET['Graph']][8])
      {
        case 'Bar':
          ShowBarGraph();
          break;
        case 'Gantt':
          ShowGanttChart();
          break;
        case 'Line':
          ShowLineGraph();
          break;
        case 'Pie':
          ShowPieChart();
          break;
        default:
          break;
      }
    }
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Stores the details for a bar graph in the session variable.              //
  //////////////////////////////////////////////////////////////////////////////
  function BuildBarGraph($Query, $Width, $Height, $Title, $XTitle, $YTitle, $GradientOne, $GradientTwo)
  {
    if (isset($_SESSION['Graphs']))
      $graphs = $_SESSION['Graphs'];
    else
      $graphs = array();
    
    $graph = array($Query, $Width, $Height, $Title, $XTitle, $YTitle, $GradientOne, $GradientTwo, 'Bar');
    $graphs[] = $graph;
    $_SESSION['Graphs'] = $graphs;
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Stores the details for a bar graph with multiple sources in the session  //
  // variable.                                                                //
  //////////////////////////////////////////////////////////////////////////////
  function BuildBarGraphMulti($Graphs, $Width, $Height, $Title, $XTitle, $YTitle, $GradientOne, $GradientTwo)
  {
    if (isset($_SESSION['Graphs']))
      $graphs = $_SESSION['Graphs'];
    else
      $graphs = array();
    
    $graph = array($Graphs, $Width, $Height, $Title, $XTitle, $YTitle, $GradientOne, $GradientTwo, 'Bar');
    $graphs[] = $graph;
    $_SESSION['Graphs'] = $graphs;
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Stores the details for a bar graph with multiple sources in the session  //
  // variable.                                                                //
  //////////////////////////////////////////////////////////////////////////////
  function BuildBarGraphMultiStat($Graphs, $Width, $Height, $Title, $XTitle, $YTitle, $GradientOne, $GradientTwo)
  {
    if (isset($_SESSION['Graphs']))
      $graphs = $_SESSION['Graphs'];
    else
      $graphs = array();
    
    $graph = array($Graphs, $Width, $Height, $Title, $XTitle, $YTitle, $GradientOne, $GradientTwo, 'Bar', 'Stat');
    $graphs[] = $graph;
    $_SESSION['Graphs'] = $graphs;
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Stores the details for a Gantt chart in the session variable.            //
  //////////////////////////////////////////////////////////////////////////////
  function BuildGanttChart($Queries, $Width, $Height, $Title, $StartDate, $EndDate, $Period, $ParameterArrays, $Legends = array(array()), $Colours = array(array()))
  {
    if (isset($_SESSION['Graphs']))
      $graphs = $_SESSION['Graphs'];
    else
      $graphs = array();
    
    $graph = array($Queries, $Width, $Height, $Title, $StartDate, $EndDate, $Period, $ParameterArrays, 'Gantt', $Legends, $Colours);
    $graphs[] = $graph;
    $_SESSION['Graphs'] = $graphs;
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Stores the details for a line graph in the session variable.             //
  //////////////////////////////////////////////////////////////////////////////
  function BuildLineGraph($Queries, $Width, $Height, $Title, $XTitle, $YTitle, $StartDate, $EndDate, $Legends = array(), $Colours = array())
  {
    if (isset($_SESSION['Graphs']))
      $graphs = $_SESSION['Graphs'];
    else
      $graphs = array();
    
    $graph = array($Queries, $Width, $Height, $Title, $XTitle, $YTitle, $StartDate, $EndDate, 'Line', $Legends, $Colours);
    $graphs[] = $graph;
    $_SESSION['Graphs'] = $graphs;
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Stores the details for a pie chart in the session variable.              //
  //////////////////////////////////////////////////////////////////////////////
  function BuildPieChart($Query, $Width, $Height, $Title, $EnlargeSlice = true, $UnusedTwo, $UnusedThree, $UnusedFour)
  {    
    if (isset($_SESSION['Graphs']))
      $graphs = $_SESSION['Graphs'];
    else
      $graphs = array();
    
    $graph = array($Query, $Width, $Height, $Title, $EnlargeSlice, "", "", "", 'Pie');
    $graphs[] = $graph;
    $_SESSION['Graphs'] = $graphs;
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Outputs a bar graph from the details stored in the session variable.     //
  //////////////////////////////////////////////////////////////////////////////
  function ShowBarGraph()
  {
    if (count($_SESSION['Graphs'][$_GET['Graph']][0]) > 1)
    {
      if ($_SESSION['Graphs'][$_GET['Graph']][9] == 'Stat')
        ShowBarGraphMultiStat();
      else
        ShowBarGraphMulti();
    }
    else
      {
        $g = $_SESSION['Graphs'][$_GET['Graph']];
        
        $maxLeg = 0;
        $resultSet = ExecuteQuery($g[0]);
        while($row = MySQL_Fetch_Array($resultSet))
        {
          $data[] = $row[0];
          $leg[] = $row[1];
          $maxLeg = Max($maxLeg, StrLen($row[1]));
        }
        
        if (count($data) > 0)
        {
          $graph = new Graph($g[1], $g[2], 'auto');
          $graph->img->SetMargin(45, 30, 20, Max(($maxLeg / 5), 1) * 35);
          $graph->SetColor('#BFCDDB');
          $graph->SetScale('textint');
          //$graph->SetShadow();
          $graph->SetMarginColor('#FFFFFF');
          
          if (count($leg) < 25)
            $graph->xaxis->SetTickLabels($leg);
          if (count($data) > 25)
            $graph->xaxis->SetTextLabelInterval((integer)(count($leg) / 10));
          
          $graph->xaxis->HideTicks();
          $graph->xaxis->SetLabelAngle(90);
          //$graph->xaxis->title->SetFont(FF_ARIAL, FS_BOLD);
          $graph->xaxis->title->Set($g[4]);
          //$graph->yaxis->title->SetFont(FF_ARIAL, FS_BOLD);
          $graph->yaxis->title->Set($g[5]);
          //$graph->title->SetFont(FF_ARIAL, FS_BOLD);
          $graph->title->Set($g[3]);
          
          $bplot = new BarPlot($data);
          $bplot->SetFillColor('#0B3162');
          if (($g[6] != "") && ($g[7] != ""))
            $bplot->SetFillGradient($g[6], $g[7], GRAD_MIDVER);
          $bplot->value->SetColor('black', 'navy');
          
          $graph->Add($bplot);
          $graph->Stroke();
        }
      }
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Outputs a bar graph with multiple sources from the details stored in the //
  // session variable.                                                        //
  //////////////////////////////////////////////////////////////////////////////
  function ShowBarGraphMulti()
  {
    $maxLeg = 0;
    foreach ($_SESSION['Graphs'][$_GET['Graph']][0] as $graph)
    {
      $resultSet = ExecuteQuery($graph[0]);
      while($row = MySQL_Fetch_Array($resultSet))
      {
        if ($row[0] != "")
        {
          $data[] = $row[0];
          $leg[] = $graph[1];
          $maxLeg = Max($maxLeg, StrLen($graph[1]));
        }
      }
    }
    
    if (count($data) > 0)
    {
      $g = $_SESSION['Graphs'][$_GET['Graph']];
      
      $graph = new Graph($g[1], $g[2], 'auto');
      $graph->img->SetMargin(45, 30, 20, Max(($maxLeg / 5), 1) * 35);
      $graph->SetColor('#BFCDDB');
      $graph->SetScale('textint');
      //$graph->SetShadow();
      $graph->SetMarginColor('#FFFFFF');
      
      if (count($leg) < 25)
        $graph->xaxis->SetTickLabels($leg);
      if (count($data) > 25)
        $graph->xaxis->SetTextLabelInterval((integer)(count($leg) / 10));
      
      $graph->xaxis->HideTicks();
      $graph->xaxis->SetLabelAngle(90);
      //$graph->xaxis->SetFont(FF_ARIAL, FS_NORMAL, 6);
      $graph->xaxis->title->Set($g[4]);
      //$graph->yaxis->SetFont(FF_ARIAL, FS_NORMAL, 6);
      $graph->yaxis->title->Set($g[5]);
      //$graph->title->SetFont(FF_ARIAL, FS_BOLD);
      $graph->title->Set($g[3]);
      
      $bplot = new BarPlot($data);
      $bplot->SetFillColor('#0B3162');
      if (($g[6] != "") && ($g[7] != ""))
        $bplot->SetFillGradient($g[6], $g[7], GRAD_MIDVER);
      $bplot->value->SetColor('black', 'navy');
      
      $graph->Add($bplot);
      $graph->Stroke();
    }
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Outputs a bar graph with multiple sources from the details stored in the //
  // session variable. This includes parallel display.                        //
  //////////////////////////////////////////////////////////////////////////////
  function ShowBarGraphMultiStat()
  {
    $g = $_SESSION['Graphs'][$_GET['Graph']];
    
    if (SizeOf($g) > 0)
    {
      $info = array();
      $maxLeg = 0;
      foreach ($_SESSION['Graphs'][$_GET['Graph']][0] as $graphTemp)
      {
        $data = array();
        $resultSet = ExecuteQuery($graphTemp[0]);
        while($row = MySQL_Fetch_Array($resultSet))
        {
          if ($row[0] != "")
          {
            $data[] = $row[0];
            $leg[] = $row[1];
            $maxLeg = Max($maxLeg, StrLen($row[1]));
          }
        }
        $leg = array_unique($leg);
        $info[] = $data;
      }

      $graph = new Graph($g[1], $g[2], 'auto');
      $graph->img->SetMargin(45, 30, 20, Max(($maxLeg / 5), 1) * 35);
      $graph->SetColor('#BFCDDB');
      $graph->SetScale('textint');
      //$graph->SetShadow();
      $graph->SetMarginColor('#FFFFFF');
      
      if (count($leg) < 25)
        $graph->xaxis->SetTickLabels($leg);
      if (count($leg) > 25)
        $graph->xaxis->SetTextLabelInterval((integer)(count($leg) / 10));
      
      $graph->xaxis->HideTicks();
      $graph->xaxis->SetLabelAngle(90);
      //$graph->xaxis->SetFont(FF_ARIAL, FS_NORMAL, 6);
      $graph->xaxis->title->Set($g[4]);
      //$graph->yaxis->SetFont(FF_ARIAL, FS_NORMAL, 6);
      $graph->yaxis->title->Set($g[5]);
      //$graph->title->SetFont(FF_ARIAL, FS_BOLD);
      $graph->title->Set($g[3]);
      
      $count = 0;
      foreach ($_SESSION['Graphs'][$_GET['Graph']][0] as $graphTemp)
      {
        if ($graphTemp[2] != "")
          $colour = $graphTemp[2];
        else
          $colour = '#0B3162';
        $graph->legend->Add($graphTemp[1], $colour);
        
        $bplot = new BarPlot($info[$count++]);
        $bplot->SetFillColor($colour);
        if (($graphTemp[6] != "") && ($graphTemp[7] != ""))
          $bplot->SetFillGradient($graphTemp[6], $graphTemp[7], GRAD_MIDVER);
        $bplot->value->SetColor('black', 'navy');
        $bars[] = $bplot;
      }
      $gbplot = new GroupBarPlot($bars);
      $graph->Add($gbplot);
      
      $graph->Stroke();
    }
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Outputs a Gantt char from the details stored in the session variable.    //
  //////////////////////////////////////////////////////////////////////////////
  function ShowGanttChart()
  {
    $g = $_SESSION['Graphs'][$_GET['Graph']];
    
    if ((!($g[1] == "")) && (!($g[2] == "")))
      $graph = new GanttGraph($g[1], $g[2], 'auto');
    else
      $graph = new GanttGraph();
    
    $graph->SetBackgroundGradient('#295885', '#295885', GRAD_HOR, BGRAD_MARGIN);
    $graph->SetColor('#FFFFFF');
    $graph->SetDateRange(SubStr($g[4], 0, 4).SubStr($g[4], 5, 2).SubStr($g[4], 8, 2), SubStr($g[5], 0, 4).SubStr($g[5], 5, 2).SubStr($g[5], 8, 2));
    
    //$graph->SetMarginColor('#0B3162');
    $graph->img->SetMargin(10, 140 , 10, 10);
    
    $graph->title->Set($g[3]);
    $graph->title->SetColor('#FFFFFF');
    
    $graph->legend->SetAbsPos(10, 25, 'right', 'top');
    //$graph->legend->SetLayout(LEGEND_HOR);
    $graph->legend->SetShadow(false, 0);
    
    $count = 0;
    $rows = array();
    for ($i = 0; $i < count($g[0]); $i++)
    {
      $resultSet = ExecuteQuery($g[0][$i]);
      $parameters = $g[7][$i];
      if (count($g[10][$i]))
        $colours = $g[10][$i];
      else
        $colours = array('#329E1F', '#03B272', '#0BB7AC', '#0B9BC6', '#0C61C9', '#6919FF', '#A215D6', '#FF11FB', '#FF0F83', '#DB0A2D', '#FF511C', '#FFA11E', '#D8C515', '#95C91C');
      
      //Add the legends.
      for ($l = 0; $l < count($g[9][$i]); $l++)
      {
        if ($colours[$l] != "")
          $graph->legend->Add($g[9][$i][$l], $colours[$l]);
      }
      switch ($g[6])
      {
        case 'Daily':
        {
          $graph->ShowHeaders(GANTT_HHOUR | GANTT_HMIN);
          
          $graph->scale->hour->SetBackgroundColor('#BFCDDB');
          $graph->scale->hour->grid->SetColor('#000000');
          //$graph->scale->hour->SetFont(FF_ARIAL);
          $graph->scale->hour->SetIntervall(1);
          $graph->scale->hour->SetStyle(MINUTESTYLE_MM);
          
          //$graph->scale->minute->SetFont(FF_ARIAL);
          $graph->scale->minute->SetIntervall(5);
          
          while($row = MySQL_Fetch_Array($resultSet))
          {
            if ($last_name != $row[$parameters[0]])
              $count++;
            $last_name = $row[$parameters[0]];
            
            //Display the rest.
            $bar = new GanttBar($count, $row[$parameters[0]], $row[$parameters[1]], Date('Y-m-d H:i:s', MKTime(SubStr($row[$parameters[1]], 11, 2), SubStr($row[$parameters[1]], 14, 2), $row[$parameters[2]], SubStr($row[$parameters[1]], 5, 2), SubStr($row[$parameters[1]], 8, 2), SubStr($row[$parameters[1]], 0, 4))));
            switch ($row[$parameters[3]])
            {
              case "":
                $bar->SetPattern(BAND_RDIAG, '#BFCDDB');
                $bar->SetFillColor('#BFCDDB');
                break;
              default:
                $bar->SetPattern(BAND_RDIAG, $colours[0]);
                $bar->SetFillColor($colours[0]);
                break;
            }
            $graph->Add($bar);
          }
          break;
        }
        case 'Monthly':
        {
          $graph->ShowHeaders( GANTT_HDAY |  GANTT_HWEEK |  GANTT_HMONTH);
          
          $graph->scale->week->SetBackgroundColor('#BFCDDB');
          //$graph->scale->week->SetFont(FF_ARIAL);
          $graph->scale->week->SetStyle(WEEKSTYLE_FIRSTDAY);
          
          //$graph->scale->month->SetFont(FF_ARIAL);
          $graph->scale->month->SetStyle( MONTHSTYLE_SHORTNAMEYEAR2);
          
          if ($i == 0)
          {
            //Display the current date.
            $vline = new GanttVLine(Date('Y-m-d'), "", '#FF0000', "11", "solid");
            $vline->SetDayOffset(0.5);
            $graph->Add($vline);
            
            //Display public holidays.
            $resultSetTemp = ExecuteQuery('SELECT PublicHoliday_Date FROM PublicHoliday WHERE PublicHoliday_Date BETWEEN "'.$g[4].'" AND "'.$g[5].'"');
            while($rowTemp = mysql_fetch_array($resultSetTemp))
            {
              $vline = new GanttVLine($rowTemp['PublicHoliday_Date'], "", '#0B3162', "11", "solid");
              $vline->SetDayOffset(0.5);
              $graph->Add($vline);
              
              $count++;
            }
          }
          
          //Display the rest.
          $count = 0;
          while($row = MySQL_Fetch_Array($resultSet))
          {
            $found = false;
            for ($k = 0; $k < count($rows); $k++)
            {
              if ($rows[$k] == $row[$parameters[0]])
              {
                $count = $k+1;
                $found = true;
                break;
              }
            }
            if (!$found)
            {
              $rows[] = $row[$parameters[0]];
              $count = count($rows);
            }
            
            $bar = new GanttBar($count, $row[$parameters[0]], Date('Y-m-d H:i:s', MKTime(0, 0, 0, SubStr($row[$parameters[1]], 5, 2), SubStr($row[$parameters[1]], 8, 2), SubStr($row[$parameters[1]], 0, 4))), Date('Y-m-d H:i:s', MKTime(23, 59, 59, SubStr($row[$parameters[2]], 5, 2), SubStr($row[$parameters[2]], 8, 2), SubStr($row[$parameters[2]], 0, 4))));
            switch ($row[$parameters[3]])
            {
              case "":
                $bar->SetPattern(BAND_RDIAG, '#BFCDDB');
                $bar->SetFillColor('#BFCDDB');
                break;
              default:
                $bar->SetPattern(BAND_RDIAG, $colours[$row[$parameters[3]]-1]);
                $bar->SetFillColor($colours[$row[$parameters[3]]-1]);
                break;
            }
            $graph->Add($bar);
          }
          break;
        }
        case 'Scatter':
        {
          $graph->ShowHeaders(GANTT_HDAY | GANTT_HHOUR | GANTT_HMIN);
          
          $graph->scale->day->SetBackgroundColor('#BFCDDB');
          //$graph->scale->day->SetFont(FF_ARIAL);
          $graph->scale->day->SetStyle(DAYSTYLE_FIRSTDAY);
          
          $graph->scale->week->SetBackgroundColor('#BFCDDB');
          //$graph->scale->week->SetFont(FF_ARIAL);
          $graph->scale->week->SetStyle(WEEKSTYLE_FIRSTDAY);
          
          //$graph->scale->month->SetFont(FF_ARIAL);
          $graph->scale->month-> SetStyle(MONTHSTYLE_SHORTNAMEYEAR2);
          
          $graph->scale->hour->SetBackgroundColor('#BFCDDB');
          $graph->scale->hour->grid->SetColor('#000000');
          //$graph->scale->hour->SetFont(FF_ARIAL);
          $graph->scale->hour->SetIntervall(1);
          $graph->scale->hour->SetStyle(MINUTESTYLE_MM);
          
          $graph->scale->day->SetStyle(DAYSTYLE_SHORTDAYDATE3);
          
          if ($i == 0)
          {
            //Display the current date.
            $vline = new GanttVLine(Date('Y-m-d'), "", '#FF0000', "11", "solid");
            $vline->SetDayOffset(0.5);
            $graph->Add($vline);
            
            //Display public holidays.
            $resultSetTemp = ExecuteQuery('SELECT PublicHoliday_Date FROM PublicHoliday WHERE PublicHoliday_Date BETWEEN "'.$g[4].'" AND "'.$g[5].'"');
            while($rowTemp = mysql_fetch_array($resultSetTemp))
            {
              $vline = new GanttVLine($rowTemp['PublicHoliday_Date'], "", '#0B3162', "11", "solid");
              $vline->SetDayOffset(0.5);
              $graph->Add($vline);
              
              $count++;
            }
          }
          
          //Display the rest.
          $count = 0;
          while($row = MySQL_Fetch_Array($resultSet))
          {
            $found = false;
            for ($k = 0; $k < count($rows); $k++)
            {
              if ($rows[$k] == $row[$parameters[0]])
              {
                $count = $k+1;
                $found = true;
                break;
              }
            }
            if (!$found)
            {
              $rows[] = $row[$parameters[0]];
              $count = Count($rows);
            }
            
            $bar = new GanttBar($count, $row[$parameters[0]], Date('Y-m-d H:i:s', MKTime(SubStr($row[$parameters[1]], 11, 2), SubStr($row[$parameters[1]], 14, 2), 0, SubStr($row[$parameters[1]], 5, 2), SubStr($row[$parameters[1]], 8, 2), SubStr($row[$parameters[1]], 0, 4))), Date('Y-m-d H:i:s', MKTime(SubStr($row[$parameters[2]], 11, 2), SubStr($row[$parameters[2]], 14, 2), 59, SubStr($row[$parameters[2]], 5, 2), SubStr($row[$parameters[2]], 8, 2), SubStr($row[$parameters[2]], 0, 4))));
            switch ($row[$parameters[3]])
            {
              case "":
                $bar->SetPattern(BAND_RDIAG, '#BFCDDB');
                $bar->SetFillColor('#BFCDDB');
                break;
              default:
                $bar->SetPattern(BAND_RDIAG, $colours[$row[$parameters[3]]-1]);
                $bar->SetFillColor($colours[$row[$parameters[3]]-1]);
                break;
            }
            $graph->Add($bar);
          }
          break;
        }
        default:
          break;
      }
    }
    
    $graph->Stroke();
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Outputs a line graph from the details stored in the session variable.    //
  //////////////////////////////////////////////////////////////////////////////
  function ShowLineGraph()
  {
    $g = $_SESSION['Graphs'][$_GET['Graph']];
    
    if (SizeOf($g) > 0)
    {
      //Generate x-axis.
      $start = Date('y/m', MKTime(0, 0, 0, GetMonthFromDatabaseDate($g[6]), 1, GetYearFromDatabaseDate($g[6])));
      $end = Date('y/m', MKTime(0, 0, 0, GetMonthFromDatabaseDate($g[7]), 1, GetYearFromDatabaseDate($g[7])));
      $axis = array($start);
      while ($start != $end)
      {
        $start = Date('y/m', MKTime(0, 0, 0, SubStr($start, 3, 2) + 1, 1, SubStr($start, 0, 2)));
        $axis[] = $start;
      }
      $maxLeg = 6;
      
      //Generate x-axis and lines from queries.
      $info = array();
      foreach ($g[0] as $query)
      {
        $data = array();
        $resultSet = ExecuteQuery($query);
        $count = 0;
        
        if (MySQL_Num_Rows($resultSet) == 0)
        {
          while($count < SizeOf($axis))
          {
            $data[] = 0;
            $count++;
          }
        } else
        {
          while(($row = MySQL_Fetch_Array($resultSet)) && ($count < SizeOf($axis)))
          {
            while ($axis[$count++] != $row[0]) 
            {
              $data[] = 0;
            }
            $data[] = $row[1];
          }
        }
        
        $info[] = $data;
      }
      
      $graph = new Graph($g[1], $g[2], 'auto');
      $graph->img->SetMargin(45, 30, 20, Max(($maxLeg / 5), 1) * 35);
      $graph->SetColor('#BFCDDB');
      $graph->SetScale('textlin');
      //$graph->SetShadow();
      $graph->SetMarginColor('#FFFFFF');
      
      if (count($g[10]))
        $colours = $g[10];
      else
        $colours = array('#329E1F', '#03B272', '#0BB7AC', '#0B9BC6', '#0C61C9', '#6919FF', '#A215D6', '#FF11FB', '#FF0F83', '#DB0A2D', '#FF511C', '#FFA11E', '#D8C515', '#95C91C');
      
      //Add the legends.
      for ($l = 0; $l < count($g[9]); $l++)
      {
        $graph->legend->Add($g[9][$l], $colours[$l]);
      }

      $graph->xaxis->SetTickLabels($axis);
      if (count($axis) > 25)
        $graph->xaxis->SetTextLabelInterval((integer)(count($axis) / 10));
      
      $graph->xaxis->HideTicks();
      //$graph->xaxis->SetLabelAngle(45);
      //$graph->xaxis->SetFont(FF_ARIAL, FS_NORMAL, 6);
      $graph->xaxis->title->Set($g[4]);
      //$graph->yaxis->SetFont(FF_ARIAL, FS_NORMAL, 6);
      $graph->yaxis->title->Set($g[5]);
      //$graph->title->SetFont(FF_ARIAL, FS_BOLD);
      $graph->title->Set($g[3]);
      
      $count = 0;
      foreach ($g[0] as $query)
      {
        $graph->Add($lplot = new LinePlot($info[$count]));
        $lplot->SetColor($colours[$count++]);
      }
      
      $graph->Stroke();
    }
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Outputs a pie chart from the details stored in the session variable.     //
  //////////////////////////////////////////////////////////////////////////////
  function ShowPieChart()
  {
    $g = $_SESSION['Graphs'][$_GET['Graph']];
    
    $resultSet = ExecuteQuery($g[0]);
    while($row = MySQL_Fetch_Array($resultSet))
    {
      $data[] = $row[0];
      $leg[] = $row[1];
    }
    
    if (count($data) > 0)
    {
      $graph = new PieGraph($g[1], $g[2], 'auto');
      //$graph->SetShadow();
      $graph->SetMarginColor('white');
      //$graph->title->SetFont(FF_ARIAL, FS_BOLD, 10);
      $graph->title->Set($g[3]);
      
      $graph->legend->SetAbsPos(10, 25, 'right', 'top');
      
      $pplot = new PiePlot3D(Array_Reverse($data));
      $pplot->SetAngle(55);
      $pplot->SetCenter(0.4);
      $pplot->SetSize(0.4);
      $pplot->SetStartAngle(0);
      $pplot->SetLabelType(PIE_VALUE_PER);
      $pplot->SetLegends($leg);
      $pplot->value->SetColor("black");
      if ($g[4])
      {
        $max = array_search(max($data), $data); //Find the position of maximum value and make it huge.
        $pplot->ExplodeSlice($max);
      }
      
      $graph->Add($pplot);
      $graph->Stroke();
    }
  }
?>