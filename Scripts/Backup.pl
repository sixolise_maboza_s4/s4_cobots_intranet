#!/usr/bin/perl
################################################################################
# DEVELOPED BY:   Gareth Ambrose
# LAST UPDATED:   18 February 2010
# DESCRIPTION:    Backs up the S4 databases
################################################################################
use strict;
use warnings;


################################################################################


my $loDir = "/home/e-smith/files/ibays/intranew/html/Files/Databases/";              #Database backup directory.

my @loTime = localtime();
my $loDateTime = (1900+$loTime[5]).sprintf("%02d", 1+$loTime[4]).sprintf("%02d", $loTime[3]).sprintf("%02d", $loTime[2]).sprintf("%02d", $loTime[1]).sprintf("%02d", $loTime[0]);

system("mysqldump -u s4mysql --password=mysql CobotsAdmin > ".$loDir."CobotsAdmin".$loDateTime.".dmp");
system("mysqldump -u s4mysql --password=mysql S4DSA > ".$loDir."S4DSA_".$loDateTime.".dmp");