<?php
  // DETAILS ///////////////////////////////////////////////////////////////////
  //                                                                          //
  //                    Last Edited By: Gareth Ambrose                        //
  //                        Date: 06 December 2007                            //
  //                                                                          //
  //////////////////////////////////////////////////////////////////////////////
  // This scripting page provides functionality for checking and controlling  //
  // access to pages.                                                         //
  //////////////////////////////////////////////////////////////////////////////
  
  //include 'AccessExceptions.php';
  //////////////////////////////////////////////////////////////////////////////
  // Checks whether a user can access a specific page.                        //
  //////////////////////////////////////////////////////////////////////////////
  function CheckAuthorisation($Page)
  {
    //Open the Navigation file and get the authorisation level for the page.
    $xml = new DOMDocument();
    $xml->load('XML/Navigation.xml');
    
    $pagePath = new DOMXPath($xml);
    $page = $pagePath->query('//item[@link ="'.$Page.'"]');
    
    if (!($page->length > 0))
      Header("Location: Error.php");
    
    $level = $page->item(0)->getAttribute('level');
    $level = explode(',', $level);
    
    //Check if the page is restricted or not.
    if ($level[0] != 0) //The page is restricted.
    {
      //Check if the user is logged in and has the necessary privileges.
      if (CheckLoggedIn()) //The user is logged in.
      {

        //Check if the user is a current staff member or is blocked.
        if ($_SESSION['cAuth'] == 0) //The user is no longer a current staff member or is blocked.
        {
          $_SESSION['NotAuthorised'] = true;
          header("Location: index.php");
        } else
        {
          $allowed = false;
          if (AuthExceptions($Page, $_SESSION['cUID']))
          {
              $allowed=true;
              return;
          }

          foreach ($level as $l)
          {

            if ((integer)$l == 255)
            {
              if ($_SESSION['cAuth'] == 255)
                $allowed = true;
            } else
              if ($_SESSION['cAuth'] & (integer)$l)
                $allowed = true;
          }
          if (!$allowed)
          {
            $_SESSION['Restricted'] = true;
            Header("Location: index.php");
          }
        }
      }
    }
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Checks if a user is logged in.                                           //
  //////////////////////////////////////////////////////////////////////////////
  function CheckLoggedIn()
  {
    //Check if the user is logged in.
    if (!isset($_SESSION['cAuth'])) //The user is not logged in.
    {
      $_SESSION['NotLoggedIn'] = true;
      Header("Location: index.php");
      return false;
    }
    
    return true;
  }
?>