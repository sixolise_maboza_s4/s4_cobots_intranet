var agt=navigator.userAgent.toLowerCase();
    var is_major = parseInt(navigator.appVersion);
    var is_minor = parseFloat(navigator.appVersion);

    var is_nav  = ((agt.indexOf('mozilla')!=-1) && (agt.indexOf('spoofer')==-1)
                && (agt.indexOf('compatible') == -1) && (agt.indexOf('opera')==-1)
                && (agt.indexOf('webtv')==-1) && (agt.indexOf('hotjava')==-1));
    var is_nav4 = (is_nav && (is_major == 4));
    var is_nav6 = (is_nav && (is_major == 5));
    var is_nav6up = (is_nav && (is_major >= 5));
    var is_ie     = ((agt.indexOf("msie") != -1) && (agt.indexOf("opera") == -1));
//tooltip Position
var offsetX = 6;
var offsetY = 7;
var opacity = 100;
var toolTipSTYLE;

function initToolTips(){
  if(document.getElementById){
          toolTipSTYLE = document.getElementById("nav").style;
  }
  if(is_ie || is_nav6up)
  {
    toolTipSTYLE.visibility = "visible";
    toolTipSTYLE.display = "none";
    document.onmousemove = moveToMousePos;
  }
}
function moveToMousePos(e)
{
  if(!is_ie){
    x = e.pageX;
    y = e.pageY;
  }else{
    x = event.x + document.body.scrollLeft;
    y = event.y + document.body.scrollTop;
  }

  toolTipSTYLE.left = x + offsetX+'px';
  toolTipSTYLE.top = y + offsetY+'px';
  return true;
}

function toolTip(msg,fg,bg)
{
initToolTips();
  if(toolTip.arguments.length < 1) // if no arguments are passed then hide the tootip
  {
    if(is_nav4)
        toolTipSTYLE.visibility = "hidden";
    else
        toolTipSTYLE.display = "none";
  }
  else // show
  {
    if(!fg) fg = "#777777";
    if(!bg) bg = "#ffffe5";
       var content = '<table height = "30px" border="0" cellspacing="0" cellpadding="0" style="border: none; border-radius:4px;"><tr><td bgcolor="' + fg + '">' +
                                  '<table border="0" cellspacing="0" cellpadding="0" style="border: none;"><tr><td bgcolor="' + bg + '">'+
                                  '<font face="sans-serif" color="' + fg + '" size="-2">' + msg +
                                  '</font></td></tr></table>'+
                                  '</td></tr></table>';
                                 
                                 
   if(is_nav4)
    {
      toolTipSTYLE.document.write(content);
      toolTipSTYLE.document.close();
      toolTipSTYLE.visibility = "visible";
    }

    else if(is_ie || is_nav6up)
    {
      document.getElementById("nav").innerHTML = content;
      toolTipSTYLE.display='block';
    }
  }
}
function getExtension(filename) {
    var parts = filename.split('.');
    return parts[parts.length - 1];
	}
	
function show(fileName){
if(fileName.indexOf(".")!=-1)
{
var pictureName ="";
var extension = getExtension(fileName).toLowerCase();
if(extension =="doc" ||extension =="docx")
{
pictureName = "Images/doc.png";
}
else if(extension=="ppt" || extension=="pptx" )
{
pictureName = "Images/ppt.png";
}
else if(extension=="xls" || extension=="xlsx")
{
pictureName = "Images/xls.png";
}
else if(extension=="pdf")
{
pictureName = "Images/pdf.jpg";
}
else 
{
pictureName = "Images/text.jpg";
}

s = '<table  width="100%" height = "14.5px" border="0" cellspacing="0" cellpadding="0"  style="background-color:white;  border:thin solid lightgrey;   box-shadow:0 2px 2px #0B3162;">';
s += '<tr><td style="color:#0B3162;font-weight:bold; font-size:12px; "><img src='+pictureName+' height=25px width=25px style="border: none; position:relative; top:3px" />'+fileName+'</td></tr>';
s += '</table>';
        toolTip(s);
		}
}