<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require('fpdf/WriteHTML_Handlers.php');

function CreatePdfTable($pdf,$columnLabels,$data,$row,$loCurrencySymbol,$supplierName)
{
    // Begin configuration
    
     if($supplierName== 1162){
         $textColour = array( 0, 0, 0 );
        $tableHeaderTopTextColour = array( 255, 255, 255 );
        $tableHeaderTopFillColour = array( 81, 90, 90 );
        $tableBorderColour = array( 81, 90, 90 );
        $tableRowFillColour = array( 213, 170, 170 );
        $columnSize = array(15,28,72,27,20,28);
     }
     else{
        $textColour = array( 0, 0, 0 );
        $tableHeaderTopTextColour = array( 255, 255, 255 );
        $tableHeaderTopFillColour = array( 20, 70, 120 );
        $tableBorderColour = array( 50, 50, 50 );
        $tableRowFillColour = array( 213, 170, 170 );
        $columnSize = array(15,28,72,27,20,28);
     }
    // End configuration
    
    //Create the table
    $pdf->SetDrawColor( $tableHeaderTopFillColour[0], $tableHeaderTopFillColour[1], $tableHeaderTopFillColour[2] );
    $pdf->Ln(3);
    
    // Create the table header row
    $pdf->SetFont( 'Segoe_UI_bold', 'B', 10 );
    $pdf->SetTextColor( $tableHeaderTopTextColour[0], $tableHeaderTopTextColour[1], $tableHeaderTopTextColour[2] );
    $pdf->SetFillColor( $tableHeaderTopFillColour[0], $tableHeaderTopFillColour[1], $tableHeaderTopFillColour[2] );
    for ( $i=0; $i<count($columnLabels); $i++ ) {
            if($i==0)
                $pdf->Cell( $columnSize[$i], 8, $columnLabels[$i], 0, 0, 'C', true );
            else
                $pdf->Cell( $columnSize[$i], 8, $columnLabels[$i], 0, 0, 'L', true );
            }
    $pdf->Ln();
    
    // Create the table data rows
    foreach ( $data as $dataRow ) {

    // Create the data cells
    $pdf->SetTextColor( $textColour[0], $textColour[1], $textColour[2] );
    $pdf->SetFillColor( $tableRowFillColour[0], $tableRowFillColour[1], $tableRowFillColour[2] );
    $pdf->SetFont( 'segoeui', '', 10 );
    $pdf->SetAligns(array('C','L' ,'L','R','R','R'));
    $pdf->SetWidths(array(15,28,72,27,20,28));
    $pdf->Row(array($dataRow[0], $dataRow[1], $dataRow[2],$dataRow[3],$dataRow[4],$dataRow[5],$dataRow[6]));
    }
    
    $pdf->SetFont( 'Segoe_UI_bold', 'B', 10 );
            $pdf->SetFillColor( $tableHeaderTopFillColour[0], $tableHeaderTopFillColour[1], $tableHeaderTopFillColour[2] );
              for ( $i=0; $i<count($columnLabels); $i++ ) {
            if(($i+1) == count($columnLabels))
            {
                $pdf->SetTextColor( $tableHeaderTopTextColour[0], $tableHeaderTopTextColour[1], $tableHeaderTopTextColour[2] );
                $pdf->Cell( $columnSize[$i], 8, $loCurrencySymbol.$row['OrderNo_Original_Total_Cost'], 0, 0, 'R', true );
            }
            else
                if(($i+2) == count($columnLabels))
                   $pdf->Cell( $columnSize[$i], 8, "Total (Excl.) ",0,0,'C',false);
            else
                $pdf->Cell( $columnSize[$i], 8, "", 0, 0, 'C', false );
            }
}

function BuildPdfContent($pdf,$contentLabelArray,$contentValueArray,$supplierName)
{
    $pdf->SetFont('Segoe_UI_bold','B',10);
    if($supplierName== 1162 ){
         $pdf->Cell(0, 5, "Indusproc (Pty) Ldt", 0, 0, 'L');
         $pdf->Ln();
         $pdf->SetFont('segoeui','',10);
         $pdf->Cell(0, 5, "Reg No: 2017/096636/07 | VAT Reg. No. 4860278433", 0,0,'L');
     }
     else{
         $pdf->Cell(0, 5,'S4 Integration (Pty) Ltd', 0, 0, 'L');
         $pdf->Ln(); 
         $pdf->SetFont('segoeui','',10);
         $pdf->Cell(0, 5, "Reg No: 2009/019275/07 | VAT Reg. No. 4720160094", 0,0,'L');
     }
    $pdf->Ln(5);
    $pdf->Ln(5);
    for ( $i=0; $i<count($contentLabelArray); $i++ ) {
        
        
        if($i==3)
        {
            $pdf->Ln(5);
            $flag = false;
            $pdf->SetFont('Segoe_UI_bold','B',10);
            $pdf->Cell(30, 5, $contentLabelArray[$i], 0, 0, 'L');
            $pdf->SetFont('segoeui','',10);
            foreach ($contentValueArray[$i] as $value) {
                if(!$flag && ($value != "")){
                    $flag = true;
                    $pdf->Cell(0, 5, $value, 0,0,'L');
                }
                else if($value != "") {
                    $pdf->Cell(30, 5, " ", 0, 0, 'L');
                    $pdf->Cell(0, 5, $value, 0,0,'L');
                }
                $pdf->Ln();
            }
        }
        else
        {
            $pdf->SetFont('Segoe_UI_bold','B',10);
            $pdf->Cell(30, 5, $contentLabelArray[$i], 0, 0, 'L');
            $pdf->SetFont('segoeui','',10);
            $pdf->Cell(0, 5, $contentValueArray[$i], 0,0,'L');
        }

        $pdf->Ln(); 
    }
    //S4 Address
    $DeliverTo = array("S4 Integration","150 Mimosa Road","Fairview", "Port Elizabeth", "South Africa", "6065", "",);
    
    if($supplierName== 1162){
         $InvoiceTo = array("Indusproc","150 Mimosa Road", "Fairview", "Port Elizabeth", "6065", "");
    }
    else{
        $InvoiceTo = array("S4 Integration","PO Box 14248","Sidwell", "6061", "", "", "");
    }
    $pdf->Ln(3);
    $DeliverToString ="Deliver To";
    $InvoiceToString ="Invoice To";
    for ($index = 0; $index < count($DeliverTo); $index++) {
        $pdf->SetFont('Segoe_UI_bold','B',10);
        $pdf->Cell(30, 5, $DeliverToString, 0, 0, 'L');
        $pdf->SetFont('segoeui','',10);
        $pdf->Cell(50, 5, $DeliverTo[$index], 0,0,'L');
        
        $pdf->SetFont('Segoe_UI_bold','B',10);
        $pdf->Cell(30, 5, $InvoiceToString, 0, 0, 'L');
        $pdf->SetFont('segoeui','',10);
        $pdf->Cell(50, 5, $InvoiceTo[$index], 0,0,'L');
        $pdf->Ln();
        
        
        $DeliverToString ="";
        $InvoiceToString ="";
    }
    $pdf->Ln(5);
    $pdf->SetFont('Segoe_UI_bold','B',17); 
    $pdf->Cell(0, 10, 'ORDER DETAILS', 0, 0, 'C');
    $pdf->Ln(10);
}

function GeneratePdf($pdf_filename,$columnLabels,$tableData,$row,$staffRow,$loCurrencySymbol)
{
    
    /// i know this is bad but i couldnt get were they call close() 
    if($row['Supplier_Code'] == 1162){   
        $pdf=new PDF_HTML();   // diffrent header and footer
    }
    else{
         $pdf=new PDF_HTML2();    // s4 header and footer
    }
    
   
  //Create Page
    $pdf->AliasNbPages();
    $pdf->SetAutoPageBreak(true, 36);
    $pdf->AddPage();
    $pdf->AddFont('segoeui','','segoeui.php');
    $pdf->AddFont('Segoe_UI_bold','B','Segoe_UI_bold.php');
    $pdf->SetFont('Segoe_UI_bold','B',18); 
    $pdf->Ln(10);
    $pdf->Cell(0, 10, 'PURCHASE ORDER: '.$row['OrderNo_Number'], 0, 0, 'L');
    //$pdf->WriteHTML('<P>PURCHASE ORDER: '.$row['OrderNo_Number'].'</P>');
    $pdf->Ln(13);
    $contentLabelArray = array('Date','Requestor','Contact','Supplier');
    
    $contact = str_ireplace("s4integration.co.za", "s4.co.za",$row['Staff_email']);
    $contact .= ' | +27 41 451 1250';

    if($row['Supplier_Code'] ==1162){   
        $contentValueArray = array(GetTextualDateFromDatabaseDate($row['OrderNo_Date_Time']),$row['Staff_First_Name'].' '.$row['Staff_Last_Name'],$contact,array('Rubicon Electrical & Automation (PE)_ACCOUNT','4 Reith Street','Sidwell','Port Elizabeth','6001'));
    }
    else{
        
         $contentValueArray = array(GetTextualDateFromDatabaseDate($row['OrderNo_Date_Time']),$row['Staff_First_Name'].' '.$row['Staff_Last_Name'],$contact,array($row['Supplier_Name'],$row['Supplier_Address_Street'],$row['Supplier_Address_Suburb'],$row['Supplier_Address_City'],$row['Supplier_Address_Code']));
    }
     
    BuildPdfContent($pdf,$contentLabelArray,$contentValueArray,$row['Supplier_Code']);
       //Create the table
       CreatePdfTable($pdf,$columnLabels,$tableData,$row,$loCurrencySymbol,$row['Supplier_Code']); 
       
       return $pdf;
}




?>