<?php
  // DETAILS ///////////////////////////////////////////////////////////////////
  //                                                                          //
  //                    Last Edited By: Gareth Ambrose                        //
  //                        Date: 08 May 2008                                 //
  //                                                                          //
  //////////////////////////////////////////////////////////////////////////////
  // This scripting page provides functionality for accessing and querying    //
  // the database.                                                            //
  //////////////////////////////////////////////////////////////////////////////
  
  include 'Display.php';    //Controls page display and generates page elements.
  include 'Mailing.php';    //Sends mails.
  
  //////////////////////////////////////////////////////////////////////////////
  // Connect to the database.                                                 //
  //////////////////////////////////////////////////////////////////////////////
  function Connect()
  {
    return MySQL_Connect('192.168.0.200', 's4mysql', 'mysql');
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Connect to the database and execute a query.                             //
  //////////////////////////////////////////////////////////////////////////////
  function ExecuteQuery($Query, $Database = 'CobotsAdmin')
  {
    $conn = Connect();
    if ($conn)
    {
      MySQL_Select_DB($Database, $conn);
      return MySQL_Query($Query, $conn);
    } else
      return false;
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Update reminder triggers.                                                //
  //////////////////////////////////////////////////////////////////////////////
  function UpdateReminders()
  {
    $resultSet = ExecuteQuery('SELECT * FROM Reminder WHERE Reminder_Trigger_Date <= "'.Date('Y-m-d').'" AND (Reminder_Mail <> "" OR Reminder_Mail_HTML <> "")');
    while ($row = MySQL_Fetch_Array($resultSet))
    {
      $rowStaff = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = '.$row['Reminder_Trigger_Name'].''));
      SendMailHTML($rowStaff['Staff_email'], 'Reminder', $row['Reminder_Mail'], $row['Reminder_Mail_HTML']);
      ExecuteQuery('UPDATE Reminder SET Reminder_Mail = "",  Reminder_Mail_HTML = "" WHERE Reminder_ID = "'.$row['Reminder_ID'].'"');
    }
    
    $resultSet = ExecuteQuery('SELECT * FROM Reminder WHERE Reminder_Trigger_Date < "'.Date('Y-m-d').'"');
    while ($row = MySQL_Fetch_Array($resultSet))
    {
      switch ($row['Reminder_Period'])
      {
        case '1':
          ExecuteQuery('DELETE FROM Reminder WHERE Reminder_ID = "'.$row['Reminder_ID'].'"');
          break;
        case '2':
          $date = Date('Y-m-d');
          ExecuteQuery('UPDATE Reminder SET Reminder_Trigger_Date = "'.$date.'" WHERE Reminder_ID = "'.$row['Reminder_ID'].'"');
          break;
        case '3':
          $date = $row['Reminder_Trigger_Date'];
          while (DatabaseDateLater(Date('Y-m-d'), $date))
          {
            $date = GetDatabaseDate(GetDayFromDatabaseDate($date) + 7, GetMonthFromDatabaseDate($date), GetYearFromDatabaseDate($date));
          }
          ExecuteQuery('UPDATE Reminder SET Reminder_Trigger_Date = "'.$date.'" WHERE Reminder_ID = "'.$row['Reminder_ID'].'"');
          break;
        case '4':
          //Use the extra info from the reference field to provide for the varying number of days per month. Eg: expected date is 30th but in Feb this isn't possible so it becomes 28th and then the following month it becomes 30th again.
          $date = $row['Reminder_Trigger_Date'];
          while (DatabaseDateLater(Date('Y-m-d'), $date))
          {
            $maxDay = Date('t', GetDatabaseDate(1, GetMonthFromDatabaseDate($date) + 1, GetYearFromDatabaseDate($date)));
            $day = $row['Reminder_System_Reference'] > $maxDay ? $maxDay : $row['Reminder_System_Reference'];
            $date = GetDatabaseDate($day, GetMonthFromDatabaseDate($date) + 1, GetYearFromDatabaseDate($date));
          }
          ExecuteQuery('UPDATE Reminder SET Reminder_Trigger_Date = "'.$date.'" WHERE Reminder_ID = "'.$row['Reminder_ID'].'"');
          break;
        default:
          break;
      }
    }
    
    //Fix so that moves to next applicable day even if monthly or whatever.
  }
?>