<?php
// DETAILS ///////////////////////////////////////////////////////////////////
//                                                                          //
//                    Last Edited By: Gareth Ambrose                        //
//                        Date: 21 November 2008                            //
//                                                                          //
//////////////////////////////////////////////////////////////////////////////
// This scripting page provides functionality for sending mail.             //
//////////////////////////////////////////////////////////////////////////////

$Header = "";
$Boundary = 'MultiPart'.MD5(Time());

//////////////////////////////////////////////////////////////////////////////
// Gets the file name of an attachment.                                     //
//////////////////////////////////////////////////////////////////////////////
function FileName($File)
{
    $pos = strrpos($File, '/');
    if (!$pos)
        return $File;
    else
        return SubStr($File, $pos + 1, StrLen($File) - $pos);
}

//////////////////////////////////////////////////////////////////////////////
// Sends an email to a specific address.                                    //
//////////////////////////////////////////////////////////////////////////////
function SendMail($ToAddress, $Subject, $Message, $Attachments = "")
{
    global $Header;

    SetHeader($Attachments); //Set the mail header so it is accepted and interpreted correctly by the receiving mail client.

    $Message = SetFooter($Message); //Set the footer message.

    $Message = $Message.Chr(10).Chr(10).SetAttachments($Attachments); //Add any attachments to the mail.

    Mail($ToAddress, $Subject, $Message, $Header); //Send the mail. By default it uses the SMTP server in PHP.ini.
    //Mail('log@s4integration.co.za', $Subject, $Message, $Header); //Send the mail to intranet mail log.
}

//////////////////////////////////////////////////////////////////////////////
// Sends an email to a specific address.                                    //
//////////////////////////////////////////////////////////////////////////////
function SendMailHTML($ToAddress, $Subject, $Message, $MessageHTML, $Attachments = "")
{
    global $Header;

    SetHeader($Attachments); //Set the mail header so it is accepted and interpreted correctly by the receiving mail client.

    $Message = SetFooter($Message); //Set the footer message.

    $Message = SetFooterHTML($MessageHTML); //Set the footer message.

    $Message = $Message.Chr(10).Chr(10).SetAttachments($Attachments); //Add any attachments to the mail.

    Mail($ToAddress, $Subject, $Message, $Header); //Send the mail. By default it uses the SMTP server in PHP.ini.
    //Mail('log@s4integration.co.za', $Subject, $Message, $Header); //Send the mail to intranet mail log.
}

function CCSomeone($loggedEmail, $ccEmail, $title, $email, $html)
{
    if ($loggedEmail != $ccEmail)
        SendMailHTML($ccEmail, $title, $email, $html);
}

//////////////////////////////////////////////////////////////////////////////
// Sets the mail attachments.                                               //
//////////////////////////////////////////////////////////////////////////////
function SetAttachments($Attachments)
{
    global $Boundary;

    if ($Attachments == "")
        return "";
    else
        if (Is_Array($Attachments))
        {
            $attached = "";
            for ($loI = 0; $loI < SizeOf($Attachments); $loI++)
            {
                $file = $Attachments[$loI];

                $contents = File_Get_Contents($file);
                $contents = Chunk_Split(Base64_Encode($contents));

                $attached .= '--'.$Boundary.Chr(10);
                $attached .= 'Content-Transfer-Encoding: base64'.Chr(10);
                $attached .= 'Content-Disposition: attachment; filename='.FileName($file).Chr(10).Chr(10);
                $attached .= $contents.Chr(10).Chr(10);
            }
            $attached .= '--'.$Boundary.'--'.Chr(10);

            return $attached;
        } else
        {
            $file = $Attachments;

            $contents = File_Get_Contents($file);
            $contents = Chunk_Split(Base64_Encode($contents));

            $attached = '--'.$Boundary.Chr(10);
            $attached .= 'Content-Transfer-Encoding: base64'.Chr(10);
            $attached .= 'Content-Disposition: attachment; filename='.FileName($file).Chr(10).Chr(10);
            $attached .= $contents.Chr(10).Chr(10);
            $attached .= '--'.$Boundary.'--'.Chr(10);

            return $attached;
        }
}

//////////////////////////////////////////////////////////////////////////////
// Sets the plain text mail footer and message.                             //
//////////////////////////////////////////////////////////////////////////////
function SetFooter($Message)
{
    global $Boundary;

    return '--'.$Boundary.Chr(10).'Content-Type: text/plain; charset=iso-8859-1'.Chr(10).Chr(10).$Message.Chr(10).Chr(10).'This email is automatically generated by the S4 Integration Mailer, please do not reply. Instead, direct any queries to andrew@s4integration.co.za'.Chr(10);
}

//////////////////////////////////////////////////////////////////////////////
// Sets the HTML mail footer and message.                                   //
//////////////////////////////////////////////////////////////////////////////
function SetFooterHTML($Message)
{
    global $Boundary;

    return '--'.$Boundary.Chr(10).'Content-Type: text/html; charset=iso-8859-1'.Chr(10).Chr(10).$Message.'<BR /><BR />This email is automatically generated by the S4 Integration Mailer, please do not reply. Instead, direct any queries to andrew@s4integration.co.za'.Chr(10);
}

//////////////////////////////////////////////////////////////////////////////
// Sets the mail header.                                                    //
//////////////////////////////////////////////////////////////////////////////
function SetHeader($Attachments)
{
    global $Header;
    global $Boundary;

    //Set the mail header which will be parsed by the mail client.
    $Header .= 'From: Automated Mailing Script<admin@s4integration.co.za>'.Chr(10).
        'Reply-To: Do-Not-Reply<andrew@s4integration.co.za>'.Chr(10).
        'Return-Path: Automated Mailing Script<admin@s4integration.co.za>'.Chr(10).
        'X-Mailer: PHP/'.PHPVersion().Chr(10).
        'MIME-Version: 1.0'.Chr(10).
        'Content-Type: multipart/mixed; boundary="'.$Boundary.'"'.Chr(10).Chr(10);
}
?>