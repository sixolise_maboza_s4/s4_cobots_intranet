<?php
/**
 * Created by PhpStorm.
 * User: Lunga Balasane
 * Date: 11/22/2017
 * Time: 9:39 AM
 */
?>



<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<HTML>

<HEAD>
    <TITLE>Update Script</TITLE>
    <link rel="stylesheet" type="text/css" href="style.css"/>
    <script src="jquery-2.2.4.min.js" type="text/javascript" charset="utf-8"></script>
</HEAD>

<BODY BGCOLOR="#FFFFFF">

<H1>Press to run Script</H1>

<input type="button" id="run_Script" value="Run Script"/>
<div id="display_progress"></div>
<div id="divload2"></div>

<script>

    $(document).on("click","#run_Script",function() {
        $.ajax({
            xhr: function(){
                var xhr = new window.XMLHttpRequest();
                //Upload progress
                xhr.upload.addEventListener("progress", function(evt){
                    if (evt.lengthComputable) {
                        var percentComplete = evt.loaded / evt.total;
                        //Do something with upload progress
                        console.log(percentComplete);
                    }
                    console.log("Upload progress\n",evt.loaded);
                }, false);
                //Download progress
                xhr.addEventListener("progress", function(evt){
                    if (evt.lengthComputable) {
                        var percentComplete = evt.loaded / evt.total;
                        //Do something with download progress
                        console.log(percentComplete);
                    }
                    console.log("Download progress\n",evt.loaded);
                }, false);
                return xhr;
            },
            url: "script.php",
            type: 'POST',
            data: {},
            success:function(data) {
                //resource_container
                $("#display_progress").html(data);

                var x = document.getElementById('divload2');
                x.className = '';
                $(".wait-text").css("display", "none");

            },beforeSend:function () {
                var x = document.getElementById('divload2');
                x.className = 'loader';
                //$(".loader").css("display", "block");
                $(".wait-text").css("display", "block");
            }

        });
    });

    $(document).ready({

    });

</script>

</BODY>
</HTML>


