<?php
/**
 * Created by PhpStorm.
 * User: Lunga Balasane
 * Date: 11/22/2017
 * Time: 10:11 AM
 */

$servername = "192.168.0.203";
$username = "s4mysql";
$password = "mysql";
$dbname = "CobotsAdmin";

$conn = new mysqli($servername, $username, $password, $dbname);

if ($conn->connect_error) {
    die("CobotsAdmin Connection failed: " . $conn->connect_error);
}

$mm_staff_code_Manager3_array = array();
$mm_project_code_array = array();
$mm_project_description_array = array();
$mm_staff_code_Manager8_array = array();


$sql = "Select DISTINCT `Project_Code`, `Project_Description`, `Project_CostManager3`,`Project_CostManager8` FROM `Project`";
$result_proj_mm = $conn->query($sql);
$index = 0;
while ($row_proj_mm = $result_proj_mm->fetch_assoc()) {
    $mm_staff_code_Manager3_array[$index] = $row_proj_mm['Project_CostManager3'];
    $mm_project_code_array[$index] = $row_proj_mm['Project_Code'];
    $mm_project_description_array[$index] = $row_proj_mm['Project_Description'];
    $mm_staff_code_Manager8_array[$index] = $row_proj_mm['Project_CostManager8'];
    $index++;
}

$staff_code_s4 = "";

//======================START New Connection==========================
$servername = "192.168.0.203";
$username = "s4mysql";
$password = "mysql";
$dbname = "S4Admin";

$conn = new mysqli($servername, $username, $password, $dbname);

if ($conn->connect_error) {
    die("S4Admin Connection failed: " . $conn->connect_error);
}
//======================END New Connection============================

echo "======================================================".$index." CobotsAdmin Projects====================================================<br>";

echo "====================START Mechanical=======================<br>";

for ($i = 0;$i < sizeof($mm_project_code_array);$i++) {
    echo '<p><br>Project#: '.($i+1).'<br>';
    $mm_staff_code_Manager3 = $mm_staff_code_Manager3_array[$i];
    $mm_project_code = $mm_project_code_array[$i];
    $mm_project_description = $mm_project_description_array[$i];

    $mm_staff_name = "";

    $sql = "SELECT concat(`Staff_First_Name`,' ',`Staff_Last_Name`) as `StaffName` FROM `Staff` WHERE `Staff_Code` = ".$mm_staff_code_Manager3;
    $result_staff_mmname = $conn->query($sql);
    while ($row_staffname_mm = $result_staff_mmname->fetch_assoc()) {
        $mm_staff_name .= $row_staffname_mm['StaffName']. ': ';
    }

    echo ' <span style="color: red">CobotsAdmin::</span> '.'<b>Staff_Code: </b>'.$mm_staff_code_Manager3.' <b>Staff_Name: </b>'.$mm_staff_name.'<b> Project: </b>'.$mm_project_description.'<br>';

    if(strlen($mm_staff_code_Manager3) > 0){

        $s4_staff_name = "";

        //=========================START Get S4 StaffCode========================
        $sql = "SELECT `Staff_CodeS4` FROM `StaffMapping` WHERE `Staff_CodeMM` = ".$mm_staff_code_Manager3;
        $result_staff_codes4 = $conn->query($sql);
        if($result_staff_codes4->num_rows > 0){
            while ($row_staff_s4 = $result_staff_codes4->fetch_assoc()) {
                $staff_code_s4 = $row_staff_s4['Staff_CodeS4'];;
            }
        }
//        else{
//            $staff_code_s4 = $mm_staff_code_Manager3;
//        }
        //=========================END Get S4 StaffCode========================

//        if(strlen($staff_code_s4) === 0){
//            $staff_code_s4 = $mm_staff_code_Manager3;
//        }

        if(strlen($staff_code_s4) > 0 && strlen($mm_project_code) > 0){
            $sql = "SELECT `Project_CodeS4` FROM `ProjectMapping` WHERE `Project_CodeMM` = ".$mm_project_code;
            $result = $conn->query($sql);
            if($result->num_rows > 0){
                while ($row_proj_s4 = $result->fetch_assoc()) {
                    $project_code_s4 = $row_proj_s4['Project_CodeS4'];

                    $s4_proj_descr = "";

                    $sql = "Select `Project_Description` 
                            FROM `Project` where `Project_Code`=$project_code_s4";
                    $result_proj_ = $conn->query($sql);
                    while ($row_ = $result_proj_->fetch_assoc()){
                        $s4_proj_descr = $row_['Project_Description'];
                    }

                    $sql = "SELECT concat(`Staff_First_Name`,' ',`Staff_Last_Name`) as `StaffName` FROM `Staff` WHERE `Staff_Code` = ".$staff_code_s4;
                    $result_staff_mmname = $conn->query($sql);
                    while ($row_staffname_mm = $result_staff_mmname->fetch_assoc()) {
                        $s4_staff_name .= $row_staffname_mm['StaffName']. ': ';
                    }

                    echo '<span style="color: red">S4Admin::</span> '.'<b>Staff_Code: </b>'.$staff_code_s4.' <b>Staff_Name: </b>'.$s4_staff_name.'<b> Project: </b>'.$s4_proj_descr.'<br>';

                    if(strlen($project_code_s4) > 0){
                        $sql = "UPDATE `Project` SET `Project_CostManager3`= $staff_code_s4 WHERE `Project_Code`= $project_code_s4";
                        if($conn->query($sql) === TRUE){
                            echo 'Updated<br>';
                        }else{
                            echo 'Error: '.$conn->error.'<br>';//"false";
                        }
                    }else{
                        echo '<b>S4 Project_Code</b> is null. '.$project_code_s4.'<br>';
                    }
                }
            }else{
                echo 'No <b>S4_Project_Code</b> for <b>MM_Project_Code:</b> '.$mm_project_code.' '.$conn->error.'<br>';//"false";
            }

        }else{
            echo '<b>S4 Staff Code</b> is null or doesn\'t exist. '.$staff_code_s4.'. MM_Project_Code. '.$mm_project_code.'<br>';
        }
    }else{
        echo '<b>Project_CostManager3</b> is null. '.$mm_staff_code_Manager3.'<br>';
    }
    echo "</p>";
}
echo "<br>====================END Mechanical=======================<br>";


echo "<br>====================START Manufacturing=======================<br>";

for ($i = 0;$i < sizeof($mm_project_code_array);$i++) {
    echo '<p><br>Project#: '.($i+1).'<br>';
    $mm_staff_code_Manager8 = 213; // Zane Godwin  //$mm_staff_code_Manager8_array[$i];
    $mm_project_code = $mm_project_code_array[$i];
    $mm_project_description = $mm_project_description_array[$i];

    $mm_staff_name = "";

    $sql = "SELECT concat(`Staff_First_Name`,' ',`Staff_Last_Name`) as `StaffName` FROM `Staff` WHERE `Staff_Code` = ".$mm_staff_code_Manager8;
    $result_staff_mmname = $conn->query($sql);
    while ($row_staffname_mm = $result_staff_mmname->fetch_assoc()) {
        $mm_staff_name .= $row_staffname_mm['StaffName']. ': ';
    }

    echo ' <span style="color: red">CobotsAdmin::</span> '.'<b>Staff_Code: </b>'.$mm_staff_code_Manager8.' <b>Staff_Name: </b>'.$mm_staff_name.'<b> Project: </b>'.$mm_project_description.'<br>';

    if(strlen($mm_staff_code_Manager8) > 0){

        $s4_staff_name = "";

        //=========================START Get S4 StaffCode========================
        $sql = "SELECT `Staff_CodeS4` FROM `StaffMapping` WHERE `Staff_CodeMM` = ".$mm_staff_code_Manager8;
        $result_staff_codes4 = $conn->query($sql);
        if($result_staff_codes4->num_rows > 0){
            while ($row_staff_s4 = $result_staff_codes4->fetch_assoc()) {
                $staff_code_s4 = $row_staff_s4['Staff_CodeS4'];;
            }
        }
//        else{
//            $staff_code_s4 = $mm_staff_code_Manager8;
//        }
        //=========================END Get S4 StaffCode========================

//        if(strlen($staff_code_s4) === 0){
//            $staff_code_s4 = $mm_staff_code_Manager8;
//        }

        if(strlen($staff_code_s4) > 0 && strlen($mm_project_code) > 0){
            $sql = "SELECT `Project_CodeS4` FROM `ProjectMapping` WHERE `Project_CodeMM` = ".$mm_project_code;
            $result = $conn->query($sql);
            if($result->num_rows > 0){
                while ($row_proj_s4 = $result->fetch_assoc()) {
                    $project_code_s4 = $row_proj_s4['Project_CodeS4'];

                    $s4_proj_descr = "";

                    $sql = "Select `Project_Description` 
                            FROM `Project` where `Project_Code`=$project_code_s4";
                    $result_proj_ = $conn->query($sql);
                    while ($row_ = $result_proj_->fetch_assoc()){
                        $s4_proj_descr = $row_['Project_Description'];
                    }

                    $sql = "SELECT concat(`Staff_First_Name`,' ',`Staff_Last_Name`) as `StaffName` FROM `Staff` WHERE `Staff_Code` = ".$staff_code_s4;
                    $result_staff_mmname = $conn->query($sql);
                    while ($row_staffname_mm = $result_staff_mmname->fetch_assoc()) {
                        $s4_staff_name .= $row_staffname_mm['StaffName']. ': ';
                    }

                    echo '<span style="color: red">S4Admin::</span> '.'<b>Staff_Code: </b>'.$staff_code_s4.' <b>Staff_Name: </b>'.$s4_staff_name.'<b> Project: </b>'.$s4_proj_descr.'<br>';

                    if(strlen($project_code_s4) > 0){
                        $sql = "UPDATE `Project` SET `Project_CostManager8`= $staff_code_s4 WHERE `Project_Code`= $project_code_s4";
                        if($conn->query($sql) === TRUE){
                            echo 'Updated<br>';
                        }else{
                            echo 'Error: '.$conn->error.'<br>';//"false";
                        }
                    }else{
                        echo '<b>S4 Project_Code</b> is null. '.$project_code_s4.'<br>';
                    }
                }
            }else{
                echo 'No <b>S4_Project_Code</b> for <b>MM_Project_Code:</b> '.$mm_project_code.' '.$conn->error.'<br>';//"false";
            }

        }else{
            echo '<b>S4 Staff Code</b> is null or doesn\'t exist. '.$staff_code_s4.'. MM_Project_Code. '.$mm_project_code.'<br>';
        }
    }else{
        echo '<b>Project_CostManager8</b> is null. '.$mm_staff_code_Manager8.'<br>';
    }
    echo "</p>";
}
echo "<br>====================END Manufacturing=======================";