<?php
/**
 * Created by PhpStorm.
 * User: Lunga Balasane
 * Date: 3/28/2017
 * Time: 8:19 AM
 */
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "s4admin_new";

$conn = new mysqli($servername, $username, $password, $dbname);

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
