<?php
/**
 * Created by PhpStorm.
 * User: S4
 * Date: 2018/06/11
 * Time: 07:43
 */
//require('font/segoeui.php');
require('fpdf/fpdf.php');
class PhonelistPdf extends FPDF
{

    function Header() {
        // Logo
        $this->Image('../Images/s4.png',167,10,27,27);

        // Arial bold 15
        $this->SetFont('Arial','B',15);

        // Move to the right
        $this->Cell(80);

        // Title
        //$this->Cell(30,10,'Title Test',1,0,'C');

        // Line break
        $this->Ln(30);
        $this->Ln(10);
    }
    function Footer() {
        // Position at 1.5 cm from bottom
        //$this->Image('../Images/footer.PNG',10,260,180);
        $this->SetY(-17);
        $this->SetTextColor( 20, 70, 120 );

        $this->AddFont('Roboto-Regular','','Roboto-Regular.php');
        $this->SetFont('Roboto-Regular','',7);
        $this->Cell(0, 3, "Directors: Mr. A. White and Mr. V. Fulton", 0,0,'R');
        $this->Ln();

        //$this->WriteHTML('<hr>');
        //$this->Ln();
        $this->SetFont('','',7);
        $this->Cell(0, 3, "Head Office: Unit 5, Pogson Park, Pogson Road, Sydenham, Port Elizabeth, South Africa, 6001 | PO Box 14248, Sidwell, Port Elizabeth, 6061", 0,0,'R');
        $this->Ln();
        $this->Cell(0, 3, "Tel: +27 41 451 1250 | Fax: +27 86 504 1554 | Email: s4@s4.co.za | Web: www.s4.co.za", 0,0,'R');
        $this->Ln();
        $this->Cell(0, 3, "S4 Integration (Pty) Ltd 2009/019275/07 - VAT Reg. No. 4720160094", 0,0,'R');

        // Arial italic 8
        //$this->SetFont('Arial','I',8);
        // Page number
        //$this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
    }
    function printTable($header, $data){

        $this->SetDrawColor(20, 70, 120);
        $this->SetFont('Roboto-Regular','');
        $this->SetTextColor(255, 255, 255);
        $this->SetFillColor(20, 70, 120);


        $this->SetLineWidth(.3);
        $this->SetFont('Roboto-Regular','');

        // Header
        $w = array(55, 20, 35, 35,35,15);
        for($i=0;$i<count($header);$i++){
            //$this->Cell->Rect($w[$i], 7, $w[$i], 7, 'F');
            $this->Cell($w[$i],7,$header[$i],1,0,'C',true);
        }
        $this->Ln();


        $this->SetFillColor(20, 70, 120);
        $this->SetTextColor(0);
        $this->SetFont('Roboto-Regular');

        //Data
//        $fill = false;
        foreach ($data as $row){
            $this->Cell($w[0],6,$row[0],'LR');
            $this->Cell($w[1],6,$row[1],'LR');
            $this->Cell($w[2],6,$row[2],'LR');
            $this->Cell($w[3],6,$row[3],'LR',0,'R');
            $this->Cell($w[4],6,$row[4],'LR',0,'R');
            $this->Cell($w[5],6,$row[5],'LR',0,'R');
            $this->Ln();
           // $fill = !fill;

        }
        // Closing line
        $this->Cell(array_sum($w),0,'',1);
    }

}



?>