<?php
  // DETAILS ///////////////////////////////////////////////////////////////////
  //                                                                          //
  //                    Last Edited By: Gareth Ambrose                        //
  //                        Date: 18 February 2010                            //
  //                                                                          //
  //////////////////////////////////////////////////////////////////////////////
  // This scripting page provides functionality for displaying pages and      //
  //creating page elements.                                                   //
  //////////////////////////////////////////////////////////////////////////////
  

  include 'AccessExceptions.php';
  /* SECTION - PAGE LAYOUT /////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////////////// */
  // Builds the page banner.                                                  //
  //////////////////////////////////////////////////////////////////////////////
  function ManagementAccess()
  {
      return array(15, 318, 1 ,282, 212, 30, 345, 280, 8, 319, 32,410,16,392);
  }

  function BuildBanner()
  {
      $resultSt= ExecuteQuery('SELECT Staff_First_Name, Staff_Last_Name FROM Staff WHERE Staff_Code = "'.$_SESSION['cUID'].'"');
      $LoggedinFullName="";
      if (($_SESSION['cAuth']) && (MySQL_Num_Rows($resultSt) == 1))
      {
            $loggedInStaff = MySQL_Fetch_Array($resultSt);
            if (StrLen($loggedInStaff["Staff_First_Name"].' '.$loggedInStaff["Staff_Last_Name"]) > 16)
                  $LoggedinFullName = SubStr($loggedInStaff["Staff_First_Name"].' '.$loggedInStaff["Staff_Last_Name"], 0, 16).'...</B>';
             else
                  $LoggedinFullName = $loggedInStaff["Staff_First_Name"].' '.$loggedInStaff["Staff_Last_Name"];

          echo '<div class="media-author">'.$LoggedinFullName.'</div>';
      }
      else{
                echo '<DIV class="media-author" >S4 Cobots
                    </DIV>';
          }

  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a page content header.                                            //
  //////////////////////////////////////////////////////////////////////////////
  function BuildContentHeader($Header, $Bookmark, $BookmarkTop, $BreakBefore)
  {
      if ($BreakBefore)
          echo'<div class="panel-heading">
            <span class="panel-title">'.$Header.'</span>
        </div>';
//    if ($BreakBefore)
//      echo '<BR class="long" />';
//    echo '<DIV class="left">
//            <SPAN class="heading">'.$Header.'
//            </SPAN>
//            <BR class="veryshort" />
//          </DIV>';
//    echo '<HR>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds the page footer.                                                  //
  //////////////////////////////////////////////////////////////////////////////  
  function BuildFooter()
  {
    echo '<DIV class="footer">
            <DIV class="footerleft">If you have any comments, queries or bugs to report - submit them
              <A class="footer" href="http://services.s4.co.za/bugzilla/enter_bug.cgi?product=Intranet" target="_blank" tabindex="-1">here</A>.
            </DIV>
            <DIV class="footerright">&copy; 2009 to '.date("Y").' - S4 Integration
            </DIV>
          </DIV>';

          // Added Javascript to toggle the menu. 
          echo '<script>
                    window.jQuery || document.write(\'<script src="Scripts/jquery.js"><\/script>\')
                </script>
                
               <!-- <script src="https://code.jquery.com/jquery-1.12.4.js" integrity="sha256-Qw82+bXyGq6MydymqBxNPYTaUXXq7c8v3CwiYwLLNXU=" crossorigin="anonymous"></script>-->
                 <!-- <script src="./ThemeScripts/jquery-ui.min.js" type="text/javascript"></script>-->
                <script src="./ThemeScripts/holder.min.js" type="text/javascript"></script>
                <script src="./ThemeScripts/ladda.min.js" type="text/javascript"></script>
                <script src="./ThemeScripts/jquery.mixitup.min.js" type="text/javascript"></script>
                <script src="./ThemeScripts/jquery.magnific-popup.js" type="text/javascript"></script>
                <script src="./ThemeScripts/holder.min.js" type="text/javascript"></script>
                <script src="./ThemeScripts/summernote.min.js" type="text/javascript"></script>
                <script src="./ThemeScripts/highcharts.js" type="text/javascript"></script>
                <script src="./ThemeScripts/select2.min.js" type="text/javascript"></script>
                <script src="./ThemeScripts/utility.js" type="text/javascript"></script>
                <script src="./ThemeScripts/main.js" type="text/javascript"></script>
                <script src="./ThemeScripts/demo.js" type="text/javascript"></script>
                
                <script src="./ThemeScripts/widgets_sidebar.js" type="text/javascript"></script>
                <script src="./ThemeScripts/settings.js" type="text/javascript"></script>
                <script src="./ThemeScripts/select2.min.js" type="text/javascript"></script>
                
                <script>
                  $(".sub-menu ul").hide();
                  $(".sub-menu a").click(function () {
                    $(this).parent(".sub-menu").children("ul").slideToggle("100");
                    $(this).find(".right").toggleClass("fa-caret-up fa-caret-down");
                  });

                  $(window).load(function() {$(".sub-menu a.active").trigger("click");});

                  </script>';




  }
  
  $currentPath = "";


  //////////////////////////////////////////////////////////////////////////////
  // Build the navigation menu.                                               //
  //////////////////////////////////////////////////////////////////////////////  
  function BuildMenu($Menu, $Page)
  {

    global $currentPath;
    $xml = new DOMDocument();
    //$xml->Load('XML/Navigation.xml');
//    if (($_SESSION['cUID'] == 15))// || ($_SESSION['cUID'] == 42))
//      $xml->Load('XML/Navigation.New.xml');
//    else
      $xml->Load('XML/Navigation.xml');
    
    $menuPath = new DOMXPath($xml);
    $menu = $menuPath->Query('/navigation/menu[@name ="'.$Menu.'"]/menuitems');
      //  $test2 = BuildMOSMenu("./Files/MOS", -1);

      echo'<aside id="sidebar_left" class="nano nano-light affix has-scrollbar">
            <!-- -------------- Sidebar Left Wrapper  -------------- -->
            <div class="sidebar-left-content nano-content" tabindex="0" style="margin-right: -17px;">
                <!-- -------------- Sidebar Header -------------- -->
                <header class="sidebar-header">
                    
                    <div class="sidebar-widget author-widget">
                        <div class="media">
                            <a href="#" class="media-left"><img src="./Images/no-photo-104x104.png" alt="" class="img-responsive"></a> <div class="media-body">
                                <div class="media-links">';
                                    if(!isset($_SESSION['cUID']))
                                    echo '<a href="Login.php">Login</a>';
                                    else
                                        echo '<span style="color:#fff;">Logged in as:</span>';
                               echo' </div>';
      BuildBanner();
                           echo '</div>
                        </div>
                    </div>
                    
                </header>';
 
    echo '<UL class="nav sidebar-menu">';
    echo BuildSubmenu($menuPath, '/navigation/menu[@name ="'.$Menu.'"]/menuitems', $Page);
    
    if ($_SESSION['cUID'] != 0) {
       //echo "<UL><li><a tabindex=\"-1\" href=\"#\" class=\"parentnormal\">S4 MOS</a><UL>";  
      // BuildMOSMenu("./Files/MOS", 0);
    }

    if (isset($_SESSION['cAuth']))
    {
      echo '<LI  class="sidebar-label pt30"><A tabindex="-1" href="Handlers/Logout.php" >Logout</A></LI>';
    }
        echo '</UL>';
//            echo '</nav>';

//calling the js function to display a tooltip when hovering over a file
echo '<script language="JavaScript" src="Scripts/tooltip.js"></script>';
//A div for displaying the tooltip
//echo '<div id="nav" style="position:absolute; visibility: hidden;left:0;right:0; "></div>';
//
//
//     echo '</DIV>';
    
//    echo '<DIV class="navistrip">S4 Intranet &rarr; '.$currentPath.'
//          </DIV>';

    echo '

                <!-- -------------- /Sidebar Hide Button -------------- -->
            </div>
            <!-- -------------- /Sidebar Left Wrapper  -------------- -->
            <div class="nano-pane" style="display: none;"><div class="nano-slider" style="height: 41px; transform: translate(0px, 0px);"></div></div>
        </aside>';



  }
  
  function CheckLevel($Level, $Auth)
  {
    $level = explode(',', $Level);
    foreach ($level as $l)
    {
      if ($l == 255)
      {
        return ((integer)$l == (integer)$Auth);
      } else
        if ((integer)$l & (integer)$Auth)
          return true;
    }
    
    return false;
  }
  function BuildTopBar(){
    echo ' <header class="navbar navbar-fixed-top bg-dark">
            <div class="navbar-logo-wrapper" >
       <img src="./Images/rectangular-logo-dark.png" alt="logo" height="55">
  
  </div>
            <ul class="nav navbar-nav navbar-left">
                <li><a href="#" class="navbar-logo-text">COBOTS Intranet </a></li>
            </ul>

        </header>';
    //style="background-color: #fff !important;"
    //<img src="./Images/moduasm_logo.jpg " alt="" class=""  height="55" width="">
  }
  function BuildBreadCrumb($currentPath){

      echo '  <header id="topbar" class="alt">
              <div class="topbar-left">
                  <ol class="breadcrumb">
                      <li class="breadcrumb-icon">
                          <a href="index.php"><span class="fa fa-home"></span></a>
                      </li>
                      <li class="breadcrumb-active">
                          <a href="#">Cobots Intranet</a>
                      </li>
                      <li class="breadcrumb-current-item">
                       / ';  echo $currentPath;
                      echo'</li>
                  </ol>
              </div>
          </header>';
  }                                                            
  function BuildSubmenu($MenuPath, $Path, $Page, &$Current = false)                              
  {
    global $currentPath;
    $build = false;
    $buildStringToLink = "";
    $buildStringFromLink = "";
    $loop=0;
    
    $menuItems = $MenuPath->Query($Path.'/item');
    $menuString = "";
    
    foreach ($menuItems as $menuItem)
    {
      $notTravel = true;
    $loop++;
        $buildStringToLink = "";
        $buildStringFromLink = "";
        $icon = $menuItem->GetAttribute('icon');

        if($menuItem->GetAttribute('name') == "Gallery"){
            if($_SERVER[HTTP_HOST] == "192.168.0.200")
                 $menuItem->SetAttribute('link', (isset($_SERVER['HTTPS']) ? "https" : "http") . "://192.168.0.204/");
            else
                $menuItem->SetAttribute('link', (isset($_SERVER['HTTPS']) ? "https" : "http") . "://s4.dynns.com:204/"); 
        }
 
        if($menuItem->GetAttribute('name') == "Travel Documents")
            $notTravel = (in_array($_SESSION['cUID'], ManagementAccess())) ? true : false;
        
        $override = AuthExceptions($menuItem->GetAttribute('link'), $_SESSION['cUID']);
        if (($override || (CheckLevel($menuItem->GetAttribute('level'), $_SESSION['cAuth'])) || ($menuItem->GetAttribute('level') == 0)) && $notTravel)
          $build = true;
        else
          $build = false;
        
         if($menuItem->GetAttribute('name') == "Message"){
             $build = false;
             $SMS = AuthExceptions($menuItem->GetAttribute('link'), $_SESSION['cUID']);
             if($SMS){
                 $build = true; 
             }
         }
        
        
        if ($build)
        {
          $IsCurrent = false;
          if ($menuItem->GetAttribute('link') == $Page)
            $IsCurrent = true;
          
          if ($menuItem->HasChildNodes())
          {
            $buildStringFromLink .= '<!--[if IE 7]><!--></A><!--<![endif]-->
                  <!--[if lte IE 6]><TABLE class="navi"><TR><TD><![endif]-->
                  <UL class="nav sub-nav">';

            if ($IsCurrent)
              $buildStringFromLink .= BuildSubmenu($MenuPath, $Path.'/item[@name="'.$menuItem->GetAttribute('name').'"]/menuitems', $Page);
            else
              $buildStringFromLink .= BuildSubmenu($MenuPath, $Path.'/item[@name="'.$menuItem->GetAttribute('name').'"]/menuitems', $Page, $IsCurrent);
            $buildStringFromLink .= '</UL><!--[if lte IE 6]></TD></TR></TABLE></A><![endif]-->';
            $buildStringFromLink .= '</LI>';
          } else
            $buildStringFromLink .= '</A></LI>';
          
            if ($menuItem->HasChildNodes())
                $buildStringToLink = '<LI class=""><A tabindex="-1" href="'.$menuItem->GetAttribute('link').'"';
            else
                $buildStringToLink = '<LI><A tabindex="-1" href="'.$menuItem->GetAttribute('link').'"';
          
          if ($IsCurrent)
          {
            $Current = true;
            if ($menuItem->HasChildNodes())
            {
              $currentPath = $menuItem->GetAttribute('name').' &rarr; '.$currentPath;
              $buildStringToLink .= ' class="active accordion-toggle"';
            } else
            {
              $currentPath = $menuItem->GetAttribute('name');
              $buildStringToLink .= ' class="active "';
            }
          } else
            if ($menuItem->HasChildNodes())
              $buildStringToLink .= ' class="accordion-toggle"';

            if ($menuItem->HasChildNodes())
              $menuString .= $buildStringToLink.'><span class="'.$icon.'"></span><span class="sidebar-title">'.$menuItem->GetAttribute('name').'</span><span class="caret"></span>'.$buildStringFromLink;
            else
              $menuString .= $buildStringToLink.'><span class="'.$icon.'"></span><span class="sidebar-title">'.$menuItem->GetAttribute('name').'</span>'.$buildStringFromLink;
         
        }

    }
    return $menuString;

  }

function BuildMOSMenu($docdir, $tabs){
$files = array();
//$docdir =$_GET["docfolder"]; 
// Open the current directory
$d = dir($docdir);

// Loop through all of the files:
while (false !== ($file = $d->read())) {
    // If the file is not this file, and does not start with a '.' or '~'
    // and does not end in LCK, then store it for later display:
    if ( ($file{0} != '.') &&
         ($file{0} != '~') &&
         (substr($file, -3) != 'LCK') &&
         ($file != basename($_SERVER['PHP_SELF']))    ) {
        // Store the filename, and full data from a stat() call:
        $files[$file] = $file;
    }
}

// Close the directory
$d->close();

// Now let's output a basic table with this information

// Sort the files so that they are alphabetical
//ksort($files);

// Prepare for using date functions:
//date_default_timezone_set('America/New_York');

// Now loop through them, echoing out a new table row for each one:
foreach($files as $name=>$stat){
    if (is_dir("$docdir/$name")){
        echo "<li ><a title=\"$name\" tabindex=\"-1\" href=\"#\" class=\"parentnormal\">$name</a><ul>";
        BuildMOSMenu("$docdir/$name",$tabs+1);
        echo "</ul></li>\n";

    }
    else 
        
	//echo "<li><a title=\"$name\" tabindex=\"-1\" href='$docdir/$name'>$name</a></li>";
       echo '<li><a onMouseOver="show(\''.$name.'\')" onMouseOut="toolTip()"  href="'.$docdir.'/'.$name.'">'.returnAshortString($name).'</a></li>';         
                    
}
}

////////////////////////////////////////////////////////////////////////////////////////////////////////
//function that checks the length of a string and return a shortened string if the string is too long///
//////////////////////////////////////////////////////////////////////////////////////////////////////// 	
 function returnAshortString($originalString)
{
   $stringToBeedited = str_split($originalString);
   if(strlen($originalString)>21)
   {
     for($j=0;$j<22;$j++)
      {
       if($j<21)
	    {
         $kk[$j]= $stringToBeedited[$j];
        }
       if($j==21)
        {
         $kk[19]=".";
          $kk[20]=".";
        }
    }
   $trt="";
   foreach($kk as $rr)
   {
     $trt=$trt.$rr;
   }
   return $trt;
   }
  else
  {
    return $originalString;
  }
 }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds the page navigation section.                                      //
  //////////////////////////////////////////////////////////////////////////////
  function BuildNavigationSection($Page, $BookmarkTop, $BreakBefore, $BreakAfter)
  {
    $xml = new DOMDocument();
    $xml->Load('XML/Navigation.xml');
    
    $pagePath = new DOMXPath($xml);
    $pages = $pagePath->Query('//item[@link ="'.$Page.'"]/menuitems/item');
    
    if ($pages->length > 0)
    {
      if ($BreakBefore)
        echo '<BR class="long" />';
      
      BuildContentHeader('Navigation', 'Navigation', $BookmarkTop, false);
      
      echo '
<DIV class="contentleft">
              <TABLE cellspacing="5" align="center" class="long noborder">';        
      foreach ($pages as $page)
      {
        if (($page->GetAttribute('level') <= $_SESSION['cAuth']) || ($page->GetAttribute('level') == 0))
          echo '<TR>
                  <TD class="short" style="background-color: #'.$colour.';"><A href="'.$page->GetAttribute('link').'">'.$page->GetAttribute('name').':</A>
                  </TD>
                  <TD style="background-color: #'.$colour.';">'.$page->GetAttribute('description').'
                  </TD>
                </TR>';
      }
      echo '</TABLE>
            </DIV>';
      
      if ($BreakAfter)
        echo '<BR class="long" />';
    }
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds the page news section.                                            //
  //////////////////////////////////////////////////////////////////////////////
  function BuildNewsSection($Page, $BookmarkTop, $BreakBefore, $BreakAfter)
  {
    $xml = new DOMDocument();
    $xml->Load('XML/News.xml');
    
    $pagePath = new DOMXPath($xml);
    $entries = $pagePath->Query('/news/page[@link ="'.$Page.'"]/entry');
                                                                            
    if ($entries->length > 0)
    {
      if ($BreakBefore)
        echo '<BR class="long" />';
      
      BuildContentHeader('Intranet News', 'News', $BookmarkTop, false);
      
      echo '<DIV class="contentleft">
              <TABLE cellspacing="5" align="center" class="long noborder">';
      foreach ($entries as $entry)
      {
        echo '<TR>
                <TD class="short bold">'.$entry->GetAttribute('date').'
                </TD>
                <TD>'.$entry->GetAttribute('text').'
                </TD>
              </TR>';
      }      
      echo '</TABLE>
            </DIV>';
      
      if ($BreakAfter)
        echo '<BR class="long" />';
    }
  }
  /* //////////////////////////////////////////////////////////////////////// */
  /* //////////////////////////////////////////////////////////////////////// */
  
  
  /* SECTION - PAGE DISPLAY ////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////////////// */
  // Checks a user's status to see if any messages need to be displayed. If   //
  // so the most appropriate message is displayed.                            //
  //////////////////////////////////////////////////////////////////////////////
  function CheckStatus()
  {
    if (isset($_SESSION['NotValid']))
    {
      Session_Unregister('NotValid');
      echo '<DIV class="error">
              Login failed. Ensure that the username exists and that both your username and password are entered in correctly.
            </DIV>                                                      
            <BR /><BR />';
    } else
    if (isset($_SESSION['NotLoggedIn']))
    {
      Session_Unregister('NotLoggedIn');
      echo '<DIV class="error">
              You are not logged in, Your Session has timed out. Login before attempting to view any pages.
            </DIV>                                              
            <BR /><BR />';
    } else
    if (isset($_SESSION['NotAuthorised']))
    {
      Session_Unregister('NotAuthorised');
      echo '<DIV class="error">
              You do not have the privileges necessary for accessing the intranet.
            </DIV>                                              
            <BR /><BR />';
    } else
    if (isset($_SESSION['Restricted']))
    {
      Session_Unregister('Restricted');
      echo '<DIV class="error">
              Access denied. You are not authorised to view that page. 
              A record has been made of your attempt to view unauthorised 
              information and the administrator has been notified. 
            </DIV>                                              
            <BR /><BR />';
      //Record attempt of unauthorised access and mail admins.
    }

  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Displays an error message.                                               //
  //////////////////////////////////////////////////////////////////////////////
  function DisplayError($Message)
  {
    switch ($Message)
    {
      case 'Exist':
       $Message = 'Specified file already exists. Please select a different file or give the specified file a different name.';
        break;
      case 'Fail':
       $Message = 'Requested operation failed. An error occurred during the submission process.';
        break;
      case 'Incomplete':
        $Message = 'One or more fields were left incomplete or invalid. Ensure all required fields have values and that these values are valid.';
        break;
      case 'SizeType':
       $Message = 'Specified file is too large or an invalid type. Ensure the file is the correct size and a valid type.';
        break;
      case 'OldPassword':
       $Message = 'The old password you have entered is incorrect.';
        break;
      case 'NewPassword':
       $Message = 'The new password and the new password confirmation do not match.';
        break;
    case 'WeakPassword':
       $Message = 'The system has detected that your password is not strong enough, please reset your password.';
        break;
			case 'Permissions':
       $Message = 'You do not a sufficient permisions.';
        break;
			case 'Format':
        $Message = 'The file you have chosen to import is in the incorrect format.';
        break;
      default:
        break;
    }
    echo '<DIV class="error">'.$Message.'
          </DIV>                                              
          <BR /><BR />';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a message set.                                                    //
  //////////////////////////////////////////////////////////////////////////////
  function BuildMessageSet($SessionControl)
  {
    if (isset($_SESSION[$SessionControl.'Fail']))
    {
  		DisplayError('Fail');
      Session_Unregister($SessionControl.'Fail');
    } else
    if (isset($_SESSION[$SessionControl.'Incomplete']))
    {
  		DisplayError('Incomplete');
      Session_Unregister($SessionControl.'Incomplete');
    } else
		if (isset($_SESSION[$SessionControl.'Format']))
    {
  		DisplayError('Format');
      Session_Unregister($SessionControl.'Format');
    } else
    if (isset($_SESSION[$SessionControl.'Success']))
    {
  		DisplayNotification('Success');
      Session_Unregister($SessionControl.'Success');
    } else
    if (isset($_SESSION[$SessionControl.'Exist']))
    {
  		DisplayError('The specified file already exists.');
      Session_Unregister($SessionControl.'Exist');
    } else
    if (isset($_SESSION[$SessionControl.'SizeType']))
    {
  		DisplayError('SizeType');
      Session_Unregister($SessionControl.'SizeType');
    } else
    if (isset($_SESSION['PasswordsOld']))
    {
  		DisplayError('OldPassword');  
      Session_Unregister('PasswordsOld');
    } else
    if (isset($_SESSION[$SessionControl.'Permissions']))
    {
  		DisplayError('Permissions');
      Session_Unregister($SessionControl.'Permissions');
    }
    else
    if (isset($_SESSION[$SessionControl.'WeakPassword']))
    {
  		DisplayError('WeakPassword');
      Session_Unregister($SessionControl.'WeakPassword');
    }
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Displays a notification message.                                         //
  //////////////////////////////////////////////////////////////////////////////
  function DisplayNotification($Message)
  {
    switch ($Message)
    {
      case 'Success':
        $Message = 'Requested operation has completed successfully.';
        break;
      default:
        break;
    }
    echo '<DIV class="notify">'.$Message.'
          </DIV>                                      
          <BR /><BR />';
  }
  /* //////////////////////////////////////////////////////////////////////// */
  /* //////////////////////////////////////////////////////////////////////// */
  
  
  /* SECTION - PAGE ELEMENTS ///////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////////////// */
  // Builds a textbox.                                                        //
  //////////////////////////////////////////////////////////////////////////////
  function BuildTextBox($Index, $Name, $Type, $Class, $Length, $Value)
  {
    echo '<INPUT tabindex="'.$Index.'" name="'.$Name.'" type="'.$Type.'" class="'.$Class.'" maxlength="'.$Length.'" value="'.$Value.'" />';
  }
  
  
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a textbox cell.                                                   //
  //////////////////////////////////////////////////////////////////////////////
  function BuildTextBoxCell($Index, $Name, $Type, $Class, $Length, $Value, $CellClass = "", $Cols = 1, $Rows = 1, $Additional = "")
  {
    echo '<TD colspan="'.$Cols.'" rowspan="'.$Rows.'" class="'.$CellClass.'">
            <INPUT tabindex="'.$Index.'" name="'.$Name.'" type="'.$Type.'" class="'.$Class.'" '.($Length ? 'maxlength="'.$Length.'" ' : "").'value="'.$Value.'" />'.$Additional.'
          </TD>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a textarea.                                                       //
  //////////////////////////////////////////////////////////////////////////////
  function BuildTextArea($Index, $Name, $Class, $Value)
  {
    echo '<TEXTAREA tabindex="'.$Index.'" name="'.$Name.'" class="'.$Class.'">'.$Value.'</TEXTAREA>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a textarea cell.                                                  //
  //////////////////////////////////////////////////////////////////////////////
  function BuildTextAreaCell($Index, $Name, $Class, $Value, $CellClass, $Cols = 1, $Rows = 1)
  {
    echo '<TD colspan="'.$Cols.'" rowspan="'.$Rows.'" class="'.$CellClass.'">
            <TEXTAREA tabindex="'.$Index.'" name="'.$Name.'" class="'.$Class.'">'.$Value.'</TEXTAREA>
          </TD>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a textarea.                                                       //
  //////////////////////////////////////////////////////////////////////////////
  function BuildFileUpload($Index, $Name, $Class, $Value)
  {
    echo '<INPUT tabindex="'.$Index.'" name="'.$Name.'" type="file" class="'.$Class.'" value="'.$Value.'" />';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a textarea cell.                                                  //
  //////////////////////////////////////////////////////////////////////////////
  function BuildFileUploadCell($Index, $Name, $Class, $Value, $CellClass, $Cols = 1, $Rows = 1)
  {
    echo '<TD colspan="'.$Cols.'" rowspan="'.$Rows.'" class="'.$CellClass.'">
            <INPUT tabindex="'.$Index.'" name="'.$Name.'" type="file" class="'.$Class.'" value="'.$Value.'" />
          </TD>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a submit button.                                                  //
  //////////////////////////////////////////////////////////////////////////////
  function BuildSubmit($IndexSubmit, $Value, $Alt = 'Submit')
  {
    echo '<INPUT tabindex="'.$IndexSubmit.'" name="'.$Alt.'" type="submit" class="button" value="'.$Value.'" />';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a submit button cell.                                             //
  //////////////////////////////////////////////////////////////////////////////
  function BuildSubmitCell($IndexSubmit, $Value, $CellClass, $Cols = 2, $Alt = 'Submit')
  {
    echo '<TD colspan="'.$Cols.'" class="'.$CellClass.'">
            <INPUT tabindex="'.$IndexSubmit.'" name="'.$Alt.'" type="submit" class="button" value="'.$Value.'" />
          </TD>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a submit and cancel row.                                          //
  //////////////////////////////////////////////////////////////////////////////
  function BuildSubmitCancel($IndexSubmit, $IndexCancel, $CellClass, $Cols = 2, $YesNo = false, $ButtonOne = 'Submit', $ButtonTwo = 'Cancel')
  {
    echo '<TR>
            <TD colspan="'.$Cols.'" class="'.$CellClass.'">';
            if ($YesNo)
              echo '<INPUT tabindex="'.$IndexSubmit.'" name="Submit" type="submit" class="button" value="Yes" />
                    <INPUT tabindex="'.$IndexCancel.'" name="Submit" type="submit" class="button" value="No" />';
            else
              echo '<INPUT tabindex="'.$IndexSubmit.'" name="Submit" type="submit" class="button" value="'.$ButtonOne.'" />
                    <INPUT tabindex="'.$IndexCancel.'" name="Submit" type="submit" class="button" value="'.$ButtonTwo.'" />';
      echo '</TD>
          </TR>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a remove prompt.                                                  //
  //////////////////////////////////////////////////////////////////////////////
  function BuildRemovePrompt($IndexYes, $IndexNo, $Handler, $Text, $Value = 'Remove', $Header = 'Remove')
  {
    echo '<TABLE cellspacing="5" align="center" class="short">
            <FORM method="post" action="'.$Handler.'">
              <INPUT name="Type" type="hidden" value="'.$Value.'" />
              <TR>
                <TD colspan="2" class="header">'.$Header.'
                </TD>
              </TR>
              <TR>
                <TD colspan="2" class="center">'.$Text.'
                </TD>
              </TR>
              <TR>
                <TD colspan="2" class="center">
                  <INPUT tabindex="'.$IndexYes.'" name="Submit" type="submit" class="button" value="Yes" />   
                  <INPUT tabindex="'.$IndexNo.'" name="Submit" type="submit" class="button" value="No" />                  
                </TD>
              </TR>
            </FORM>
          </TABLE>';
  }
                    
  //////////////////////////////////////////////////////////////////////////////
  // Builds a label cell.                                                     //
  //////////////////////////////////////////////////////////////////////////////
  function BuildLabelCell($Text, $Class, $Notes = 1, $Cols = 1, $Rows = 1)
  {
    echo '<TD colspan="'.$Cols.'" rowspan="'.$Rows.'" class="'.$Class.'">'.$Text;
            if ($Notes > 0)
            {
              echo '<SPAN class="note">';
              for ($i = 0; $i < $Notes; $i++)
              {
                echo '*';
              }
              echo '</SPAN>';
            }
    echo '</TD>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a day selection list.                                             //
  //////////////////////////////////////////////////////////////////////////////
  function BuildDaySelector($TabIndex, $Name, $Day)
  {
    if ($Day == "")
      $Day = Date('d');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="day">';
    for ($inc = 1; $inc < 32; $inc++)
    {
      if ($inc == $Day)
        echo '<OPTION value="'.SPrintF('%02d',$inc).'" SELECTED>'.SPrintF('%02d',$inc).'</OPTION>';
      else
        echo '<OPTION value="'.SPrintF('%02d',$inc).'">'.SPrintF('%02d',$inc).'</OPTION>';
    }
    echo '</SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a week selection list.                                            //
  //////////////////////////////////////////////////////////////////////////////
  function BuildWeekSelector($paTabIndex, $paName, $paWeek)
  {
    if ($paWeek == "")
      $loWeek = Date('W');
    echo '<SELECT tabindex="'.$paTabIndex.'" name="'.$paName.'" class="day">';
    for ($loInc = 1; $loInc < 54; $loInc++)
    {
      if ($loInc == $loWeek)
        echo '<OPTION value="'.SPrintF('%02d',$loInc).'" SELECTED>'.SPrintF('%02d',$loInc).'</OPTION>';
      else
        echo '<OPTION value="'.SPrintF('%02d',$loInc).'">'.SPrintF('%02d',$loInc).'</OPTION>';
    }
    echo '</SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a number of hours selection list.                                 //
  //////////////////////////////////////////////////////////////////////////////
  function BuildNumberHoursSelector($TabIndex, $Name, $Hour)
  {
    if ($Hour == "")
      $Hour = 1;
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="hour">';
    for ($inc = 1; $inc < 18; $inc++)
    {
      if ($inc == $Hour)
        echo '<OPTION value="'.SPrintF('%02d',$inc).'" SELECTED>'.SPrintF('%02d',$inc).'</OPTION>';
      else
        echo '<OPTION value="'.SPrintF('%02d',$inc).'">'.SPrintF('%02d',$inc).'</OPTION>';
    }
    echo '</SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds an hour selection list.                                           //
  //////////////////////////////////////////////////////////////////////////////
  function BuildHourSelector($TabIndex, $Name, $Hour)
  {
    if ($Hour == "")
      $Hour = Date('H');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="hour">';
    for ($inc = 0; $inc < 24; $inc++)
    {
      if ($inc == $Hour)
        echo '<OPTION value="'.SPrintF('%02d',$inc).'" SELECTED>'.SPrintF('%02d',$inc).'</OPTION>';
      else
        echo '<OPTION value="'.SPrintF('%02d',$inc).'">'.SPrintF('%02d',$inc).'</OPTION>';
    }
    echo '</SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds an hour selection list.                                           //
  //////////////////////////////////////////////////////////////////////////////
  function BuildNumberSelector($TabIndex, $Name, $Number, $Start = 1, $End = 100, $Increase = 1)
  {
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="veryshort">';
    for ($inc = $Start; $inc <= $End; $inc += $Increase)
    {
      if ($inc == $Number)
        echo '<OPTION value="'.SPrintF('%02d',$inc).'" SELECTED>'.SPrintF('%02d',$inc).'</OPTION>';
      else
        echo '<OPTION value="'.SPrintF('%02d',$inc).'">'.SPrintF('%02d',$inc).'</OPTION>';
    }
    echo '</SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a minute selection list.                                          //
  //////////////////////////////////////////////////////////////////////////////  
  function BuildMinuteSelector($TabIndex, $Name, $Minute, $Increase = 15)
  {
    if ($Minute == "")
       $Minute = Date('i');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="minute">';
    for ($inc = 0; $inc < 60; $inc+=$Increase)
    {
      if ($inc == (integer)$Minute)
        echo '<OPTION value="'.SPrintF('%02d',$inc).'" SELECTED>'.SPrintF('%02d',$inc).'</OPTION>';
      else
        echo '<OPTION value="'.SPrintF('%02d',$inc).'">'.SPrintF('%02d',$inc).'</OPTION>';
    }
    echo '</SELECT>';
  }
  

 //////////////////////////////////////////////////////////////////////////////
  // Builds a minute selection list.                                          //
  //////////////////////////////////////////////////////////////////////////////  
  function BuildMinuteSelector2($TabIndex, $Name, $Minute, $Increase = 5)
  {
    if ($Minute == "")
       $Minute = Date('i');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="minute">';
    for ($inc = 0; $inc < 60; $inc+=$Increase)
    {
      if ($inc == (integer)$Minute)
        echo '<OPTION value="'.SPrintF('%02d',$inc).'" SELECTED>'.SPrintF('%02d',$inc).'</OPTION>';
      else
        echo '<OPTION value="'.SPrintF('%02d',$inc).'">'.SPrintF('%02d',$inc).'</OPTION>';
    }
    echo '</SELECT>';
  }


  //////////////////////////////////////////////////////////////////////////////
  // Builds a month selection list.                                           //
  //////////////////////////////////////////////////////////////////////////////
  function BuildMonthSelector($TabIndex, $Name, $Month)
  {
    if ($Month == "")
      $Month = Date('m');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="month">';
    for ($inc = 1; $inc < 13; $inc++)
    {
      $date = MkTime(0, 0, 0, $inc + 1, 0, 0);
      if ($inc == (integer)$Month)
        echo '<OPTION value="'.Date('m', $date).'" SELECTED>'.Date('M', $date).'</OPTION>';
      else
        echo '<OPTION value="'.Date('m', $date).'">'.Date('M', $date).'</OPTION>';
    }
    echo '</SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a payment method selection list.                                  //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildPaymentMethod($TabIndex, $Name, $MethodID, $Class)                                     
  {                                                                            
    $resultSet = ExecuteQuery('SELECT Payment_ID, Payment_Description FROM Payment ORDER BY Payment_Description ASC');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'"> 
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    { 
      if ($row['Payment_ID'] == $MethodID)
        echo '<OPTION value="'.$row['Payment_ID'].'" SELECTED>'.$row['Payment_Description'].'</OPTION>';
      else
        echo '<OPTION value="'.$row['Payment_ID'].'">'.$row['Payment_Description'].'</OPTION>';
    }         
    echo '</SELECT>';                                                           
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a year selection list.                                            //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildYearSelector($TabIndex, $Name, $Year)
  {
    if ($Year == "")
      $Year = Date('Y');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="year">';
    for ($inc = 2004; $inc < 2030; $inc++)
    {
      if ($inc == $Year)
        echo '<OPTION value="'.$inc.'" SELECTED>'.$inc.'</OPTION>';
      else
        echo '<OPTION value="'.$inc.'">'.$inc.'</OPTION>';
    }
    echo '</SELECT>';
  }

//////////////////////////////////////////////////////////////////////////////
  // Builds a year selection list.                                            //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildYearSelector96($TabIndex, $Name, $Year)
  {
    if ($Year == "")
      $Year = Date('Y');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="year">';
    for ($inc = 1996; $inc < 2030; $inc++)
    {
      if ($inc == $Year)
        echo '<OPTION value="'.$inc.'" SELECTED>'.$inc.'</OPTION>';
      else
        echo '<OPTION value="'.$inc.'">'.$inc.'</OPTION>';
    }
    echo '</SELECT>';
  }

  
  //////////////////////////////////////////////////////////////////////////////
  // Builds the html head links.        <LINK rel="stylesheet" type="text/css" href="Stylesheets/Navigation.css"/>                                       //
  //////////////////////////////////////////////////////////////////////////////
  function BuildHead($Title = 'Intranet')
  {
    echo '<LINK rel="icon" href="Images/.png">
         
          <link rel="stylesheet prefetch" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
          <!--[if lte IE 6]>
          <LINK rel="stylesheet" type="text/css" href="Stylesheets/Navigation_IE.css"/>
          <![endif]-->
          <!--[if IE 7]>
          <LINK rel="stylesheet" type="text/css" href="Stylesheets/Navigation_IE7.css"/>
          <![endif]-->
          <LINK rel="stylesheet" type="text/css" href="Stylesheets/Print.css" media="print" />
          <TITLE> S4 Cobots - '.$Title.'
          </TITLE>';
    echo '
        <SCRIPT type="text/javascript" language="JavaScript">
            <!--
              document.writeln("<STYLE type=\"text/css\">");
              var height = 0;
              if (window.innerWidth) //if browser supports window.innerWidth
                height = window.innerHeight;
              else if (document.all) //else if browser supports document.all (IE 4+)
                height = (document.compatMode=="CSS1Compat") ? document.documentElement.clientHeight : document.body.clientHeight;
              
              document.writeln("div.content { height: "+(height - 83)+"px; min-height: "+(height - 83)+"px; margin-left: 260px;}");
              document.writeln("div.navi    { height: "+(height - 83)+"px; min-height: "+(height - 83)+"px; }");
              document.writeln("div.navitop { height: "+(height - 145)+"px; max-height: "+(height - 154)+"px; }");
              document.writeln("</STYLE>");
            //-->
            
          </SCRIPT>';
          
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a casual selection list.                                          //
  //////////////////////////////////////////////////////////////////////////////
  function BuildCasualSelector($TabIndex, $Name, $Class , $CasualCode, $CurrentOnly = false)
  {
    if ($CurrentOnly)
      $resultSet = ExecuteQuery('SELECT Casual_First_Name, Casual_Last_Name, Casual_Code FROM Casual WHERE Casual_IsEmployee = "1" ORDER BY Casual_First_Name ASC');
    else
      $resultSet = ExecuteQuery('SELECT Casual_First_Name, Casual_Last_Name, Casual_Code FROM Casual ORDER BY Casual_First_Name ASC');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    {
      if ($row['Casual_Code'] == $CasualCode)
        echo '<OPTION value="'.$row['Casual_Code'].'" SELECTED>'.$row['Casual_First_Name'].' '.$row['Casual_Last_Name'].'</OPTION>';
      else
        echo '<OPTION value="'.$row['Casual_Code'].'">'.$row['Casual_First_Name'].' '.$row['Casual_Last_Name'].'</OPTION>';
    }         
    echo '</SELECT>';                                             
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a staff task selection list.                                      //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildStaffTaskSelector($TabIndex, $Name, $Class, $Specific = false)                                                                               
  {
    if ($Specific)
    {
      $resultSet = ExecuteQuery('SELECT Task.* FROM Task, Staff WHERE Task_Name = '.$_SESSION['cUID'].' AND Task_Status <> 1 AND Task_Name = Staff_Code AND Staff_IsEmployee > 0 ORDER BY Task_Title ASC');
      $resultSetTemp = ExecuteQuery('SELECT Task.* FROM Task, Staff WHERE Task_AssignedBy = '.$_SESSION['cUID'].' AND Task_Status <> 1 AND Task_Name = Staff_Code AND Staff_IsEmployee > 0 ORDER BY Task_Title ASC');
      echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
              <OPTION value="">< ... ></OPTION>';
      while ($rowTemp = MySQL_Fetch_Array($resultSetTemp))
      {
        echo '<OPTION value="'.$rowTemp['Task_ID'].'">Assigned: '.$rowTemp['Task_Title'].'</OPTION>';
      }
      while ($row = MySQL_Fetch_Array($resultSet))
      {
        echo '<OPTION value="'.$row['Task_ID'].'">Received: '.$row['Task_Title'].'</OPTION>';
      }
      echo '</SELECT>';
    } else
    {
      if ($_SESSION['cAuth'] & 8)
        $resultSet = ExecuteQuery('SELECT Task.*, Staff_First_Name, Staff_Last_Name FROM Task, Staff WHERE Task_Name = Staff_Code AND Staff_IsEmployee > 0 ORDER BY Staff_First_Name ASC');
      else
        $resultSet = ExecuteQuery('SELECT Task.*, Staff_First_Name, Staff_Last_Name FROM Task, Staff WHERE Task_Name = Staff_Code AND Staff_IsEmployee > 0 AND (Task_Name = '.$_SESSION['cUID'].' OR Task_AssignedBy = '.$_SESSION['cUID'].') ORDER BY Staff_First_Name ASC');
      echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
              <OPTION value="">< ... ></OPTION>';
      while ($row = MySQL_Fetch_Array($resultSet))
      {
        echo '<OPTION value="'.$row['Task_ID'].'">'.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].' - '.$row['Task_Title'].'</OPTION>';
      }
      echo '</SELECT>';
    }
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a staff selection list.                                           //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildStaffSelector($TabIndex, $Name, $Class , $StaffCode, $Full = false, $All = false)
  {
    if ($All)
      $resultSet = ExecuteQuery('SELECT Staff_First_Name, Staff_Last_Name, Staff_Code FROM Staff WHERE Staff_Code > 0 ORDER BY Staff_First_Name ASC');
    else
      if ($Full)
        $resultSet = ExecuteQuery('SELECT Staff_First_Name, Staff_Last_Name, Staff_Code FROM Staff WHERE Staff_IsEmployee > 0 ORDER BY Staff_First_Name ASC');
      else                                                                       
        $resultSet = ExecuteQuery('SELECT Staff_First_Name, Staff_Last_Name, Staff_Code FROM Staff WHERE Staff_IsEmployee = 1 ORDER BY Staff_First_Name ASC');
    
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    { 
      if ((integer)$row['Staff_Code'] == (integer)$StaffCode)
        echo '<OPTION value="'.$row['Staff_Code'].'" SELECTED>'.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].'</OPTION>';
      else
        echo '<OPTION value="'.$row['Staff_Code'].'">'.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].'</OPTION>';
    }         
    echo '</SELECT>';                    
  }
  

	//////////////////////////////////////////////////////////////////////////////
  // Builds a staff list of staff not in the files table for staff photos.    //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildStaffCurrentFileSelector($TabIndex, $Name, $Class, $StaffCode)
  {
    $resultSet = ExecuteQuery('SELECT Staff_First_Name, Staff_Last_Name, Staff_Code FROM Staff WHERE concat(Staff_First_Name, " ",Staff_Last_Name) IN (SELECT File_DisplayName FROM File WHERE File_Type = 8) ORDER BY Staff_First_Name');

    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    { 
      if ((integer)$row['Staff_Code'] == (integer)$StaffCode)
        echo '<OPTION value="'.$row['Staff_Code'].'" SELECTED>'.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].'</OPTION>';
      else
        echo '<OPTION value="'.$row['Staff_Code'].'">'.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].'</OPTION>';
    }         
    echo '</SELECT>';               
  }
		
		//////////////////////////////////////////////////////////////////////////////
  // Builds a staff list of staff in the files table for staff photos.    //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildStaffFileSelector($TabIndex, $Name, $Class, $StaffCode)
  {
    $resultSet = ExecuteQuery('SELECT Staff_First_Name, Staff_Last_Name, Staff_Code FROM Staff WHERE concat(Staff_First_Name, " ",Staff_Last_Name) NOT IN (SELECT File_DisplayName FROM File WHERE File_Type = 8) ORDER BY Staff_First_Name');

    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    { 
      if ((integer)$row['Staff_Code'] == (integer)$StaffCode)
        echo '<OPTION value="'.$row['Staff_Code'].'" SELECTED>'.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].'</OPTION>';
      else
        echo '<OPTION value="'.$row['Staff_Code'].'">'.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].'</OPTION>';
    }         
    echo '</SELECT>';               
  }
	
  //////////////////////////////////////////////////////////////////////////////
  // Builds a staff selection list.                                           //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildStaffList($TabIndex, $Name, $Class , $Staff, $Full = false, $All = false, $List = 'All')
  {
    if ($All)
      $resultSet = ExecuteQuery('SELECT Staff_First_Name, Staff_Last_Name, Staff_Code FROM Staff WHERE Staff_Code > 0 ORDER BY Staff_First_Name ASC');
    else
      if ($Full)
        $resultSet = ExecuteQuery('SELECT Staff_First_Name, Staff_Last_Name, Staff_Code FROM Staff WHERE Staff_IsEmployee > 0 ORDER BY Staff_First_Name ASC');
      else                                                                       
        $resultSet = ExecuteQuery('SELECT Staff_First_Name, Staff_Last_Name, Staff_Code FROM Staff WHERE Staff_IsEmployee = 1 ORDER BY Staff_First_Name ASC');
    
    echo '<SELECT MULTIPLE size="10" tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">';
    while ($row = MySQL_Fetch_Array($resultSet))
    {
      switch ($List)
      {
        case 'All':
          echo '<OPTION value="'.$row['Staff_Code'].'">'.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].'</OPTION>';
          break;
        case 'Exclude':
          if (!In_Array($row['Staff_Code'], $Staff))
            echo '<OPTION value="'.$row['Staff_Code'].'">'.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].'</OPTION>';
          break;
        case 'Include':
          if (In_Array($row['Staff_Code'], $Staff))
            echo '<OPTION value="'.$row['Staff_Code'].'">'.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].'</OPTION>';
          break;
        default:
          break;
      }
    }
    echo '</SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a staff DSA department member selection list.                     //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildStaffDSASelector($paTabIndex, $paName, $paClass , $paDSACode)
  {
    $loRS = ExecuteQuery('SELECT StaffDSA_DSACode, CONCAT(Staff_First_Name, " ", Staff_Last_Name) AS Name FROM StaffDSA INNER JOIN Staff ON StaffDSA_StaffCode = Staff_Code ORDER BY StaffDSA_DSACode ASC');
    echo '<SELECT tabindex="'.$paTabIndex.'" name="'.$paName.'" class="'.$paClass.'">
            <OPTION value="">< ... ></OPTION>';
    while ($loRow = MySQL_Fetch_Array($loRS))
    { 
      if ($loRow['StaffDSA_DSACode'] == $paDSACode)
        echo '<OPTION value="'.$loRow['StaffDSA_DSACode'].'" SELECTED>'.$loRow['StaffDSA_DSACode'].' - '.$loRow['Name'].'</OPTION>';
      else
        echo '<OPTION value="'.$loRow['StaffDSA_DSACode'].'">'.$loRow['StaffDSA_DSACode'].' - '.$loRow['Name'].'</OPTION>';
    }         
    echo '</SELECT>';                    
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a staff selection list.                                           //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildGroupStaffList($TabIndex, $Name, $Class , $Staff, $Full = false, $All = false, $List = 'All', $Group = "")
  {
    if ($All)
      $resultSet = ExecuteQuery('SELECT Staff_First_Name, Staff_Last_Name, Staff_Code FROM Staff WHERE Staff_Code > 0 ORDER BY Staff_First_Name ASC');
    else
      if ($Full)
        $resultSet = ExecuteQuery('SELECT Staff_First_Name, Staff_Last_Name, Staff_Code FROM Staff WHERE Staff_IsEmployee > 0 ORDER BY Staff_First_Name ASC');
      else                                                                       
        $resultSet = ExecuteQuery('SELECT Staff_First_Name, Staff_Last_Name, Staff_Code FROM Staff WHERE Staff_IsEmployee = 1 ORDER BY Staff_First_Name ASC');
    
    echo '<SELECT MULTIPLE size="10" tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">';
    while ($row = MySQL_Fetch_Array($resultSet))
    {
      if ($Group == "")
      {
        switch ($List)
        {
          case 'All':
            echo '<OPTION value="'.$row['Staff_Code'].'">'.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].'</OPTION>';
            break;
          case 'Exclude':
            if (!In_Array($row['Staff_Code'], $Staff))
              echo '<OPTION value="'.$row['Staff_Code'].'">'.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].'</OPTION>';
            break;
          case 'Include':
            if (In_Array($row['Staff_Code'], $Staff))
              echo '<OPTION value="'.$row['Staff_Code'].'">'.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].'</OPTION>';
            break;
          default:
            break;
        }
      } else
      {
        /*$resultSetFound = ExecuteQuery('SELECT * FROM TrainingGroupStaff WHERE TrainingGroupStaff_Staff = '.$row['Staff_Code'].' AND TrainingGroupStaff_Group <> "'.$Group.'" LIMIT 1');
        if (MySQL_Num_Rows($resultSetFound) == 0)
        {*/
          switch ($List)
          {
            case 'All':
              echo '<OPTION value="'.$row['Staff_Code'].'">'.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].'</OPTION>';
              break;
            case 'Exclude':
              if (!In_Array($row['Staff_Code'], $Staff))
                echo '<OPTION value="'.$row['Staff_Code'].'">'.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].'</OPTION>';
              break;
            case 'Include':
              if (In_Array($row['Staff_Code'], $Staff))
                echo '<OPTION value="'.$row['Staff_Code'].'">'.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].'</OPTION>';
              break;
            default:
              break;
          }
        //}
      }
    }
    echo '</SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a staff selection list.                                           //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildStaffHistoryTypeSelector($TabIndex, $Name, $Class, $TypeID)                                                                               
  {
    $resultSet = ExecuteQuery('SELECT * FROM StaffHistoryType ORDER BY StaffHistoryType_Description ASC');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    {
      if ($TypeID == $row['StaffHistoryType_ID'])
        echo '<OPTION value="'.$row['StaffHistoryType_ID'].'" SELECTED>'.$row['StaffHistoryType_Description'].'</OPTION>';
      else
        echo '<OPTION value="'.$row['StaffHistoryType_ID'].'">'.$row['StaffHistoryType_Description'].'</OPTION>';
    }         
    echo '</SELECT>';                    
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a file selection list.                                           //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildFileSelector($TabIndex, $Name, $Class , $FileCode, $Type, $UseFileName = false)                                                                               
  {     
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
            
    if ($UseFileName)
    {
      $resultSet = ExecuteQuery('SELECT File_ID, File_FileName FROM File WHERE File_Type = "'.$Type.'" ORDER BY File_FileName ASC');
      while ($row = MySQL_Fetch_Array($resultSet))
      { 
        if ($row['File_ID'] == $FileCode)
          echo '<OPTION value="'.$row['File_ID'].'" SELECTED>'.$row['File_FileName'].'</OPTION>';
        else
          echo '<OPTION value="'.$row['File_ID'].'">'.$row['File_FileName'].'</OPTION>';
      }        
    } else
    {
      $resultSet = ExecuteQuery('SELECT File_ID, File_DisplayName FROM File WHERE File_Type = "'.$Type.'" ORDER BY File_DisplayName ASC');
      while ($row = MySQL_Fetch_Array($resultSet))
      { 
        if ($row['File_ID'] == $FileCode)
          echo '<OPTION value="'.$row['File_ID'].'" SELECTED>'.$row['File_DisplayName'].'</OPTION>';
        else
          echo '<OPTION value="'.$row['File_ID'].'">'.$row['File_DisplayName'].'</OPTION>';
      }             
    } 
    echo '</SELECT>';                    
  }
  
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a site selection list.                                            //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildSiteSelector($TabIndex, $Name, $Class , $SiteID)                                                                               
  {                                                                            
    $resultSet = ExecuteQuery('SELECT * FROM Site ORDER BY Site_Description ASC');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    { 
      if ($row['Site_ID'] == $SiteID)
        echo '<OPTION value="'.$row['Site_ID'].'" SELECTED>'.$row['Site_Description'].'</OPTION>';
      else
        echo '<OPTION value="'.$row['Site_ID'].'">'.$row['Site_Description'].'</OPTION>';
    }         
    echo '</SELECT>';                    
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a site equipment selection list.                                  //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildSiteEquipmentSelector($TabIndex, $Name, $Class , $SiteEquipmentID)                                                                              
  {
    $resultSet = ExecuteQuery('SELECT * FROM SiteEquipment ORDER BY SiteEquipment_SerialNumber ASC');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    {
      $description = StrLen($row['SiteEquipment_Description']) > 21 ? SubStr($row['SiteEquipment_Description'], 0, 21).'...' : $row['SiteEquipment_Description'];
      if ($row['SiteEquipment_ID'] == $SiteEquipmentID)
        echo '<OPTION value="'.$row['SiteEquipment_ID'].'" SELECTED>'.$row['SiteEquipment_SerialNumber'].' - '.$description.'</OPTION>';
      else
        echo '<OPTION value="'.$row['SiteEquipment_ID'].'">'.$row['SiteEquipment_SerialNumber'].' - '.$description.'</OPTION>';
    }
    echo '</SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a staff selection list.                                           //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildCustomerSelector($TabIndex, $Name, $Class , $CustCode)                                                                               
  {                                                                            
    $resultSet = ExecuteQuery('SELECT Customer_Name, Customer_Code FROM Customer ORDER BY Customer_Name ASC');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    { 
      if ($row['Customer_Code'] == $CustCode)
        echo '<OPTION value="'.$row['Customer_Code'].'" SELECTED>'.iconv('Windows-1252', 'UTF-8//TRANSLIT', $row['Customer_Name']).'</OPTION>';
      else
        echo '<OPTION value="'.$row['Customer_Code'].'">'.iconv('Windows-1252', 'UTF-8//TRANSLIT', $row['Customer_Name']).'</OPTION>';
    }         
    echo '</SELECT>';                    
  }
  
   //////////////////////////////////////////////////////////////////////////////
  // Builds a customer selection list.                                           //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildCustomerList($TabIndex, $Name, $Class , $CustCode,$id="")                                                                               
  { 
    if($id){$id =  'id="'.$id.'"';}
    $resultSet = ExecuteQuery('SELECT DISTINCT Project_Customer AS code, Customer_Name,Customer_Code FROM Project INNER JOIN Customer ON Project_Customer = Customer_Code WHERE Project_Closed = 0 ORDER BY Customer_Name ASC');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'" '.$id.'>
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    { 
      if ($row['Customer_Code'] == $CustCode)
        echo '<OPTION value="'.$row['Customer_Code'].'" SELECTED>'.$row['Customer_Name'].'</OPTION>';
      else
        echo '<OPTION value="'.$row['Customer_Code'].'">'.$row['Customer_Name'].'</OPTION>';
    }         
    echo '</SELECT>';                    
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a staff selection list.                                           //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildSystemSelector($TabIndex, $Name, $Class , $SystemID)                                                                               
  {
    $resultSet = ExecuteQuery('SELECT CalloutSystem_ID, CalloutSystem_ShortName FROM CalloutSystem ORDER BY CalloutSystem_ShortName ASC');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    { 
      if ($row['CalloutSystem_ID'] == $SystemID)
        echo '<OPTION value="'.$row['CalloutSystem_ID'].'" SELECTED>'.$row['CalloutSystem_ShortName'].'</OPTION>';
      else
        echo '<OPTION value="'.$row['CalloutSystem_ID'].'">'.$row['CalloutSystem_ShortName'].'</OPTION>';
    }         
    echo '</SELECT>';                    
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a staff selection list.                                           //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildSubSystemSelector($TabIndex, $Name, $Class , $SystemID)                                                                               
  {
    $resultSet = ExecuteQuery('SELECT CalloutSubSystem_ID, CalloutSubSystem_ShortName FROM CalloutSubSystem ORDER BY CalloutSubSystem_ShortName ASC');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    { 
      if ($row['CalloutSubSystem_ID'] == $SystemID)
        echo '<OPTION value="'.$row['CalloutSubSystem_ID'].'" SELECTED>'.$row['CalloutSubSystem_ShortName'].'</OPTION>';
      else
        echo '<OPTION value="'.$row['CalloutSubSystem_ID'].'">'.$row['CalloutSubSystem_ShortName'].'</OPTION>';
    }         
    echo '</SELECT>';                    
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a staff selection list.                                           //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildIPSelector($TabIndex, $Name, $Class, $IPID)                                                                               
  {                                                                            
    $resultSet = ExecuteQuery('SELECT IP_ID, IP_Address FROM IP ORDER BY IP_Address ASC');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    { 
      if ($row['IP_ID'] == $IPID)
        echo '<OPTION value="'.$row['IP_ID'].'" SELECTED>'.$row['IP_Address'].'</OPTION>';
      else
        echo '<OPTION value="'.$row['IP_ID'].'">'.$row['IP_Address'].'</OPTION>';
    }         
    echo '</SELECT>';                    
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a project selection list.                                         //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildProjectList($TabIndex, $Name, $Class, $Projects, $Type = 'Open', $Deleted = true, $List = 'All', $NA = false, $Pending = true)                                                                               
  {
    $restrict = "";
    if ($Deleted)
      $restrict = ' AND Project_Code <> 101';
    
    if ($NA)
      $restrict .= ' AND Project_Code <> 193';      
    
    if ($Pending)
      $restrict .= ' AND Project_Code <> 236';      
    
    switch ($Type)
    {
      case 'All':
        $resultSet = ExecuteQuery('SELECT Project_Description, Project_Code FROM Project WHERE 1=1'.$restrict.' ORDER BY Project_Description');
        break;
      case 'Closed':
        $resultSet = ExecuteQuery('SELECT Project_Description, Project_Code FROM Project WHERE Project_Closed = "1"'.$restrict.' ORDER BY Project_Description');
        break;    
      case 'Open':
        $resultSet = ExecuteQuery('SELECT Project_Description, Project_Code FROM Project WHERE Project_Closed = "0"'.$restrict.' ORDER BY Project_Description');
        break;
      default:
        break;
    }
    
    echo '<SELECT MULTIPLE size="10" tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'" SORTED>';
    while ($row = MySQL_Fetch_Array($resultSet))
    {
      switch ($List)
      {
        case 'All':
          echo '<OPTION value="'.$row['Project_Code'].'">'.$row['Project_Description'].'</OPTION>';
          break;
        case 'Exclude':
          if (!In_Array($row['Project_Code'], $Projects))
            echo '<OPTION value="'.$row['Project_Code'].'">'.$row['Project_Description'].'</OPTION>';
          break;
        case 'Include':
          if (In_Array($row['Project_Code'], $Projects))
            echo '<OPTION value="'.$row['Project_Code'].'">'.$row['Project_Description'].'</OPTION>';
          break;
        default:
          break;
      }
    }         
    echo '</SELECT>';                    
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a project selection list.                                         //
  //////////////////////////////////////////////////////////////////////////////
  
  /// value is product code
  function BuildProjectSelector($TabIndex, $Name, $Class, $ProjectCode, $Type = 'Open', $ShowCode = false, $Deleted = false, $NA = false, $Pending = false)                                                                               
  {
    $restrict = "";
    if ($Deleted)
      $restrict = ' AND Project_Code <> 101';
    
    if ($NA)
      $restrict .= ' AND Project_Code <> 193';      
    
    if ($Pending)
      $restrict .= ' AND Project_Code <> 236';      
    

    
    
     if ($ShowCode)
    {
      switch ($Type)
      {
        case 'All':
          $resultSet = 'SELECT (SELECT Project_Pastel_Prefix FROM MMAdmin.Project WHERE Project_Code = PM.Project_CodeMM) as `MMCode`, P.* FROM Project P '
                   .'LEFT JOIN CobotsAdmin.ProjectMapping PM ON Project_CodeS4 = Project_Code '
                   . 'WHERE 1=1'.$restrict.' ORDER BY Project_Pastel_Prefix, Project_Description ASC';
          break;
        case 'Closed':
          $resultSet = 'SELECT (SELECT Project_Pastel_Prefix FROM MMAdmin.Project WHERE Project_Code = PM.Project_CodeMM) as `MMCode`, P.* FROM Project P '
                  .'LEFT JOIN CobotsAdmin.ProjectMapping PM ON Project_CodeS4 = Project_Code '
                  . 'WHERE Project_Closed = "1"'.$restrict.' ORDER BY Project_Pastel_Prefix, Project_Description ASC';
          break;    
        case 'Open':
          $resultSet = 'SELECT (SELECT Project_Pastel_Prefix FROM MMAdmin.Project WHERE Project_Code = PM.Project_CodeMM) as `MMCode`, P.* FROM Project P '
                  .'LEFT JOIN CobotsAdmin.ProjectMapping PM ON Project_CodeS4 = Project_Code '
                  . 'WHERE Project_Closed = "0"'.$restrict.' ORDER BY Project_Pastel_Prefix, Project_Description ASC';
          break;
        default:
          break;
      }
    } else
    {
      switch ($Type)
      {
        case 'All':
          $resultSet ='SELECT (SELECT Project_Pastel_Prefix FROM MMAdmin.Project WHERE Project_Code = PM.Project_CodeMM) as `MMCode`, P.* FROM Project P '
                  .'LEFT JOIN CobotsAdmin.ProjectMapping PM ON Project_CodeS4 = Project_Code '
                  . 'WHERE 1=1'.$restrict.' ORDER BY Project_Description';
          break;
        case 'Closed':
          $resultSet = 'SELECT (SELECT Project_Pastel_Prefix FROM MMAdmin.Project WHERE Project_Code = PM.Project_CodeMM) as `MMCode`, P.* FROM Project P '
                    .'LEFT JOIN CobotsAdmin.ProjectMapping PM ON Project_CodeS4 = Project_Code '
                    . 'WHERE Project_Closed = "1"'.$restrict.' ORDER BY Project_Description';
          break;    
        case 'Open':
          $resultSet = 'SELECT (SELECT Project_Pastel_Prefix FROM MMAdmin.Project WHERE Project_Code = PM.Project_CodeMM) as `MMCode`, P.* FROM Project P '
                  .'LEFT JOIN CobotsAdmin.ProjectMapping PM ON Project_CodeS4 = Project_Code '
                  . 'WHERE Project_Closed = "0"'.$restrict.' ORDER BY Project_Description';
          break;
        default:
          break;
      }
    }
    
    $resultSet = ExecuteQuery($resultSet);

    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    {
      if ($ShowCode){
       
         if(strcmp($row['MMCode'], "") != 0 ){
              $code = $row['Project_Pastel_Prefix'].' - '.$row['MMCode'].' - ';
         }
         else{ 
          $code = $row['Project_Pastel_Prefix'].' - ';
         }
         
      }
      else
        $code = "";
      
      if ($row['Project_Code'] == $ProjectCode)
        echo '<OPTION value="'.$row['Project_Code'].'" SELECTED>'.$code.$row['Project_Description'].'</OPTION>';
      else
        echo '<OPTION value="'.$row['Project_Code'].'">'.$code.$row['Project_Description'].'</OPTION>';
    }         
    echo '</SELECT>';                    
  }
  
  
    //////////////////////////////////////////////////////////////////////////////
  // Builds timesheet project selection list.                                         //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildTimesheetProjectSelector($TabIndex, $Name, $Class, $ProjectCode, $Type = 'Open',$id="")                                                                               
  {
      if($id){$id =  'id="'.$id.'"';}
      $resultSet = ExecuteQuery('SELECT Project_Customer AS customer, Project_Code AS code, CONCAT(Project_Pastel_Prefix, " - ", Project_Description) AS name FROM Project WHERE Project_Closed = 0 ORDER BY name ASC');
   
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'" '.$id.'>
            <OPTION value="">< ... ></OPTION>';
    $_SESSION['ChainedProjects'] ="";
    while ($row = MySQL_Fetch_Array($resultSet))
    {
      $_SESSION['ChainedProjects'] .= " ".$row['code'];
        
      if ($row['code'] == $ProjectCode)
        echo '<OPTION value="'.$row['code'].'" class= "'.$row['customer'].'" SELECTED>'.$row['name'].'</OPTION>';
      else
        echo '<OPTION value="'.$row['code'].'" class="'.$row['customer'].'">'.$row['name'].'</OPTION>';
    }         
    echo '</SELECT>';                    
  }
  
    //////////////////////////////////////////////////////////////////////////////
  // Builds a task selection list.                                         //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildTimesheetTaskSelector($TabIndex, $Name, $Class, $TaskID,$id="")                                                                               
  {
     if($id){$id =  'id="'.$id.'"';}
    $resultSet = ExecuteQuery('SELECT taskid, name FROM ts_roles_staff sr LEFT JOIN ts_roles_tasks rt ON sr.roleid = rt.roleid '
            . 'LEFT JOIN ts_tasks t ON t.id = rt.taskid WHERE sr.staffid = '.$_SESSION['cUID']);
     
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'" '.$id.'>
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    {
       if(substr($row['name'], 0, 2) == 'TO')
           $chainProject = '1805';
       else
           $chainProject = $_SESSION['ChainedProjects'];
       
       
      if ($row['taskid'] == $TaskID)
        echo '<OPTION value="'.$row['taskid'].'" class="'.$chainProject.'" SELECTED>'.$code.$row['name'].'</OPTION>';
      else
        echo '<OPTION value="'.$row['taskid'].'" class="'.$chainProject.'">'.$code.$row['name'].'</OPTION>';
    }         
    echo '</SELECT>';   
    unset($_SESSION['ChainedProjects']);
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a staff project selection list.                                   //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildStaffProjectSelector($TabIndex, $Name, $Class, $ProjectCode, $StaffCode)                                                                               
  {
    $resultSet = ExecuteQuery('SELECT Project_Code, Project_Description FROM StaffProject, Project WHERE StaffProject_Name = '.$StaffCode.' AND Project_Code = StaffProject_Project ORDER BY Project_Description');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    { 
      if ($row['Project_Code'] == $ProjectCode)
        echo '<OPTION value="'.$row['Project_Code'].'" SELECTED>'.$row['Project_Description'].'</OPTION>';
      else
        echo '<OPTION value="'.$row['Project_Code'].'">'.$row['Project_Description'].'</OPTION>';
    }         
    echo '</SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a poll selection list.                                            //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildPollSelector($TabIndex, $Name, $Class)                                                                               
  {
    $resultSet = ExecuteQuery('SELECT * FROM Poll WHERE Poll_Close_DateTime < "'.Date('Y-m-d H:i:s').'" ORDER BY Poll_Title ASC');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    { 
      echo '<OPTION value="'.$row['Poll_ID'].'">'.$row['Poll_Title'].'</OPTION>';
    }         
    echo '</SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a poll approval selection list.                                   //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildPollApproveSelector($TabIndex, $Name, $Class)                                                                               
  {
    ExecuteQuery('UPDATE Poll SET Poll_IsApproved = "2" WHERE Poll_Close_DateTime < "'.Date('Y-m-d H:i:s').'" AND Poll_IsApproved = "0"');
    
    $resultSet = ExecuteQuery('SELECT * FROM Poll WHERE Poll_IsApproved = "0" ORDER BY Poll_Title ASC');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    { 
      echo '<OPTION value="'.$row['Poll_ID'].'">'.$row['Poll_Title'].'</OPTION>';
    }         
    echo '</SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds an unpaid banked overtime selection list.                         //
  //////////////////////////////////////////////////////////////////////////////
  function BuildOvertimeBankedSelector($TabIndex, $Name, $Class)
  {
    $resultSet = ExecuteQuery('SELECT * FROM OvertimeBank WHERE OvertimeBank_Payment = "0" AND OvertimeBank_Name = '.$_SESSION['cUID'].' AND OvertimeBank_PreApproved <> 0 AND OvertimeBank_Hours > 0 AND OvertimeBank_IsApproved = "1" ORDER BY OvertimeBank_Start ASC');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    {
      echo '<OPTION value="'.$row['OvertimeBank_ID'].'">'.GetTextualDateTimeFromDatabaseDateTime($row['OvertimeBank_Start']).' - '.GetTextualDateTimeFromDatabaseDateTime($row['OvertimeBank_End']).'</OPTION>';
    }
    echo '</SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds an unpaid banked overtime selection list.                         //
  //////////////////////////////////////////////////////////////////////////////
  function BuildOvertimeBankedEditSelector($TabIndex, $Name, $Class)
  {
    $resultSet = ExecuteQuery('SELECT * FROM OvertimeBank WHERE OvertimeBank_Payment = "0" AND OvertimeBank_Name = '.$_SESSION['cUID'].' AND OvertimeBank_PreApproved <> 0 AND OvertimeBank_Hours > 0 AND OvertimeBank_IsApproved = "1" OvertimeBank_End >= "'.Date('Y-m-d', MkTime(0, 0, 0, Date("m")  , Date("d") - 30, Date("Y"))).' 00:00:00" ORDER BY OvertimeBank_Start ASC');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    {
      echo '<OPTION value="'.$row['OvertimeBank_ID'].'">'.GetTextualDateTimeFromDatabaseDateTime($row['OvertimeBank_Start']).' - '.GetTextualDateTimeFromDatabaseDateTime($row['OvertimeBank_End']).'</OPTION>';
    }
    echo '</SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a pre-approved overtime selection list.                           //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildOvertimePreApprovedSelector($TabIndex, $Name, $Class)                                                                               
  {
    $resultSet = ExecuteQuery('SELECT OvertimePreApp.*, Project_Description FROM OvertimePreApp, Project WHERE OvertimePreApp_Name = "'.$_SESSION['cUID'].'" AND OvertimePreApp_Project = Project_Code AND OvertimePreApp_OvertimeUsed = "0" AND `OvertimePreApp_ApprovedDate` >= "'.Date('Y-m-d', MkTime(0, 0, 0, Date("m")  , Date("d") - 30, Date("Y"))).' 00:00:00" AND OvertimePreApp_IsApproved = "1" ORDER BY Project_Description, OvertimePreApp_Start ASC');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    { 
      echo '<OPTION value="'.$row['OvertimePreApp_ID'].'">'.$row['Project_Description'].' - '.GetTextualDateFromDatabaseDate($row['OvertimePreApp_Start']).' to '.GetTextualDateFromDatabaseDate($row['OvertimePreApp_End']).'</OPTION>';
    }         
    echo '</SELECT>';                    
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds an allowance type selection list.                                 //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildOvertimeTypeSelector($TabIndex, $Name, $Class , $OvertimeType)                                                                               
  {                                        
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    if ('Banked' == $OvertimeType)
      echo '<OPTION value="Banked" SELECTED>Banked</OPTION>';
    else
      echo '<OPTION value="Banked">Banked</OPTION>';    
    if ('Non-Banked' == $OvertimeType)
      echo '<OPTION value="Non-Banked" SELECTED>Non-Banked</OPTION>';
    else
      echo '<OPTION value="Non-Banked">Non-Banked</OPTION>';      
    if ('Pre-Approved' == $OvertimeType)
      echo '<OPTION value="Pre-Approved" SELECTED>Pre-Approved</OPTION>';
    else
      echo '<OPTION value="Pre-Approved">Pre-Approved</OPTION>';
    if ('Requested-Normal' == $OvertimeType)
      echo '<OPTION value="Requested-Normal" SELECTED>Requested-Normal</OPTION>';
    else
      echo '<OPTION value="Requested-Normal">Requested-Normal</OPTION>';
    if ('Requested-Banked' == $OvertimeType)
      echo '<OPTION value="Requested-Banked" SELECTED>Requested-Banked</OPTION>';
    else
      echo '<OPTION value="Requested-Banked">Requested-Banked</OPTION>';
    echo '</SELECT>';
  }
                         
  //////////////////////////////////////////////////////////////////////////////
  // Builds an allowance type selection list.                                 //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildAllowanceTypeSelector($TabIndex, $Name, $Class , $AllowanceType)                                                                               
  {                                        
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';     
    //if ('Out-of-Town' == $AllowanceType)
    //  echo '<OPTION value="Out-of-Town" SELECTED>Out-of-Town</OPTION>';
    //else
    //  echo '<OPTION value="Out-of-Town">Out-of-Town</OPTION>';
    if ('Shift' == $AllowanceType)
      echo '<OPTION value="Shift" SELECTED>Shift</OPTION>';
    else
      echo '<OPTION value="Shift">Shift</OPTION>';      
    if ('Standby' == $AllowanceType)
      echo '<OPTION value="Standby" SELECTED>Standby</OPTION>';
    else
      echo '<OPTION value="Standby">Standby</OPTION>';      
    if ('Travel' == $AllowanceType)
      echo '<OPTION value="Travel" SELECTED>Travel</OPTION>';
    else
      echo '<OPTION value="Travel">Travel</OPTION>';    
    echo '</SELECT>';                    
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds an out-of-town allowance type selection list.                     //
  //////////////////////////////////////////////////////////////////////////////
  function BuildOutOfTownAllowanceTypeSelector($TabIndex, $Name, $Class, $AllowanceType)
  {
    $resultSet = ExecuteQuery('SELECT * FROM OutOfTownAllowanceType ORDER BY OutOfTownAllowanceType_Description ASC');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    {
      if ($row['OutOfTownAllowanceType_ID'] == $AllowanceType)
        echo '<OPTION value="'.$row['OutOfTownAllowanceType_ID'].'" SELECTED>'.$row['OutOfTownAllowanceType_Description'].'</OPTION>';
      else
        echo '<OPTION value="'.$row['OutOfTownAllowanceType_ID'].'">'.$row['OutOfTownAllowanceType_Description'].'</OPTION>';
    }
    echo '</SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds an allowance type selection list.                                 //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildCasualAllowanceTypeSelector($TabIndex, $Name, $Class , $AllowanceType)                                                                               
  {                                        
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    if ('Shift' == $AllowanceType)
      echo '<OPTION value="Shift" SELECTED>Shift</OPTION>';
    else
      echo '<OPTION value="Shift">Shift</OPTION>';
    echo '</SELECT>';                    
  }
                         
  //////////////////////////////////////////////////////////////////////////////
  // Builds an allowance selection list.                                      //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildAllowanceSelector($TabIndex, $Name, $Class, $UID, $AllowanceType)                                                                               
  {
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';   
    if ($UID == "")
    {
      $resultSet = ExecuteQuery('SELECT * FROM Staff ORDER BY Staff_First_Name, Staff_Last_Name ASC');      
      switch ($AllowanceType)
      {
        case 'Out-of-Town':
          while ($row = MySQL_Fetch_Array($resultSet))
          {
            $resultSetTemp = ExecuteQuery('SELECT * FROM OutOfTownAllowance WHERE OutOfTownAllowance_Name = '.$row['Staff_Code'].' AND OutOfTownAllowance_Payment = "0" ORDER BY OutOfTownAllowance_Date_Start ASC');
            while ($rowTemp = MySQL_Fetch_Array($resultSetTemp))
            {
              echo '<OPTION value="'.$rowTemp['OutOfTownAllowance_ID'].'">'.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].' - '.GetTextualDateFromDatabaseDate($rowTemp['OutOfTownAllowance_Date_Start']).' to '.GetTextualDateFromDatabaseDate($rowTemp['OutOfTownAllowance_Date_End']).'</OPTION>';
            }
          }
          break;
        case 'Shift':
          while ($row = MySQL_Fetch_Array($resultSet))
          {
            $resultSetTemp = ExecuteQuery('SELECT * FROM ShiftAllowance WHERE ShiftAllowance_Name = '.$row['Staff_Code'].' AND ShiftAllowance_Payment = "0" ORDER BY ShiftAllowance_Date ASC');
            while ($rowTemp = MySQL_Fetch_Array($resultSetTemp))
            {
              echo '<OPTION value="'.$rowTemp['ShiftAllowance_ID'].'">'.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].' - '.GetTextualDateFromDatabaseDate($rowTemp['ShiftAllowance_Date']).'</OPTION>';
            }
          }
          break;
        case 'Standby':
          while ($row = MySQL_Fetch_Array($resultSet))
          {
            $resultSetTemp = ExecuteQuery('SELECT * FROM Standby WHERE Standby_Name = '.$row['Staff_Code'].' AND Standby_Payment = "0" ORDER BY Standby_DateStart ASC');
            while ($rowTemp = MySQL_Fetch_Array($resultSetTemp))
            {
              echo '<OPTION value="'.$rowTemp['Standby_ID'].'">'.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].' - '.GetTextualDateFromDatabaseDate($rowTemp['Standby_DateStart']).'</OPTION>';
            }
          }
          break;
        case 'Travel':
          while ($row = MySQL_Fetch_Array($resultSet))
          {
            $resultSetTemp = ExecuteQuery('SELECT * FROM TravelAllowance WHERE TravelAllowance_Name = '.$row['Staff_Code'].' AND TravelAllowance_Payment = "0" ORDER BY TravelAllowance_Date ASC');
            while ($rowTemp = MySQL_Fetch_Array($resultSetTemp))
            {
              echo '<OPTION value="'.$rowTemp['TravelAllowance_ID'].'">'.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].' - '.GetTextualDateFromDatabaseDate($rowTemp['TravelAllowance_Date']).'</OPTION>';
            }
          }
          break;
        default:
          break;
      }
    } else
    {
      switch ($AllowanceType)
      {
        case 'Out-of-Town':
          $resultSetTemp = ExecuteQuery('SELECT * FROM OutOfTownAllowance WHERE OutOfTownAllowance_Name = '.$_SESSION['cUID'].' AND OutOfTownAllowance_Payment = "0" ORDER BY OutOfTownAllowance_Date_Start ASC');
          while ($rowTemp = MySQL_Fetch_Array($resultSetTemp))
          {
            echo '<OPTION value="'.$rowTemp['OutOfTownAllowance_ID'].'">'.GetTextualDateFromDatabaseDate($rowTemp['OutOfTownAllowance_Date_Start']).' to '.GetTextualDateFromDatabaseDate($rowTemp['OutOfTownAllowance_Date_End']).'</OPTION>';
          }
          break;
        case 'Shift':
          $resultSetTemp = ExecuteQuery('SELECT * FROM ShiftAllowance WHERE ShiftAllowance_Name = '.$_SESSION['cUID'].' AND ShiftAllowance_Payment = "0" ORDER BY ShiftAllowance_Date ASC');
          while ($rowTemp = MySQL_Fetch_Array($resultSetTemp))
          {
            echo '<OPTION value="'.$rowTemp['ShiftAllowance_ID'].'">'.GetTextualDateFromDatabaseDate($rowTemp['ShiftAllowance_Date']).'</OPTION>';
          }
          break;
        case 'Standby':
          $resultSetTemp = ExecuteQuery('SELECT * FROM Standby WHERE Standby_Name = '.$_SESSION['cUID'].' AND Standby_Payment = "0" ORDER BY Standby_DateStart ASC');
          while ($rowTemp = MySQL_Fetch_Array($resultSetTemp))
          {
            echo '<OPTION value="'.$rowTemp['Standby_ID'].'">'.GetTextualDateFromDatabaseDate($rowTemp['Standby_DateStart']).'</OPTION>';
          }
          break;
        case 'Travel':
          $resultSetTemp = ExecuteQuery('SELECT * FROM TravelAllowance WHERE TravelAllowance_Name = '.$_SESSION['cUID'].' AND TravelAllowance_Payment = "0" ORDER BY TravelAllowance_Date ASC');
          while ($rowTemp = MySQL_Fetch_Array($resultSetTemp))
          {
            echo '<OPTION value="'.$rowTemp['TravelAllowance_ID'].'">'.GetTextualDateFromDatabaseDate($rowTemp['TravelAllowance_Date']).'</OPTION>';
          }
          break;
        default:
          break;
      }
    }
    echo '</SELECT>';                    
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds an allowance selection list.                                      //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildCasualAllowanceSelector($TabIndex, $Name, $Class)                                                                               
  {
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    $resultSet = ExecuteQuery('SELECT * FROM Casual ORDER BY Casual_First_Name, Casual_Last_Name ASC');
    while ($row = MySQL_Fetch_Array($resultSet))
    {
      $resultSetTemp = ExecuteQuery('SELECT * FROM CasualExpense_ShiftAllowance WHERE CasualExpense_ShiftAllowance_Name = "'.$row['Casual_Code'].'" AND CasualExpense_ShiftAllowance_Paid = "0" ORDER BY CasualExpense_ShiftAllowance_Date_Start ASC');
      while ($rowTemp = MySQL_Fetch_Array($resultSetTemp))
      {
        echo '<OPTION value="'.$rowTemp['CasualExpense_ShiftAllowance_ID'].'">'.$row['Casual_First_Name'].' '.$row['Casual_Last_Name'].' - '.GetTextualDateFromDatabaseDate($rowTemp['CasualExpense_ShiftAllowance_Date_Start']).' to '.GetTextualDateFromDatabaseDate($rowTemp['CasualExpense_ShiftAllowance_Date_End']).'</OPTION>';
      }
    }
    echo '</SELECT>';                    
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a work hours selection list.                                      //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildHoursSelector($TabIndex, $Name, $Class , $Hour)                                                                               
  {                                        
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">';
    for ($i = 0; $i < 24; $i++)
    {
      if ($i == $Hour)
        echo '<OPTION value="'.$i.'" SELECTED>'.$i.'</OPTION>';
      else
        echo '<OPTION value="'.$i.'">'.$i.'</OPTION>';
    }
    echo '</SELECT>';                    
  }
                         
  //////////////////////////////////////////////////////////////////////////////
  // Builds a priority selection list.                                        //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildPrioritySelector($TabIndex, $Name, $Class , $Priority)                                                                               
  {                                        
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">';
    if ('1' == $Priority)
      echo '<OPTION value="1" SELECTED>1 - Not Urgent</OPTION>';
    else
      echo '<OPTION value="1">1 - Not Urgent</OPTION>';
    for ($i = 2; $i < 10; $i++)
    {
      if ($i == $Priority)
        echo '<OPTION value="'.$i.'" SELECTED>'.$i.'</OPTION>';
      else
        echo '<OPTION value="'.$i.'">'.$i.'</OPTION>';
    }
    if ('10' == $Priority)
      echo '<OPTION value="10" SELECTED>10 - Very Urgent</OPTION>';
    else
      echo '<OPTION value="10">10 - Very Urgent</OPTION>';
    echo '</SELECT>';                    
  }
                         
  //////////////////////////////////////////////////////////////////////////////
  // Builds a deduction selection list.                                       //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildDeductionSelector($TabIndex, $Name, $Class, $UID)                                                                               
  {
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';   
    if ($UID == "")
    {
      $resultSet = ExecuteQuery('SELECT * FROM Staff ORDER BY Staff_First_Name, Staff_Last_Name ASC');      
      while ($row = MySQL_Fetch_Array($resultSet))
      {
        $resultSetTemp = ExecuteQuery('SELECT * FROM Deduction WHERE Deduction_Name = "'.$row['Staff_Code'].'" AND Deduction_Payment = "0" ORDER BY Deduction_Date ASC');
        while ($rowTemp = MySQL_Fetch_Array($resultSetTemp))
        {
          echo '<OPTION value="'.$rowTemp['Deduction_ID'].'">'.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].' - '.GetTextualDateFromDatabaseDate($rowTemp['Deduction_Date']).'</OPTION>';
        }
      }
    } else
    {
      $resultSetTemp = ExecuteQuery('SELECT * FROM Deduction WHERE Deduction_Name = "'.$_SESSION['cUID'].'" AND Deduction_Payment = "0" ORDER BY Deduction_Date ASC');
      while ($rowTemp = MySQL_Fetch_Array($resultSetTemp))
      {
        echo '<OPTION value="'.$rowTemp['Deduction_ID'].'">'.GetTextualDateFromDatabaseDate($rowTemp['Deduction_Date']).'</OPTION>';
      }
    }
    echo '</SELECT>';                    
  }
                         
  //////////////////////////////////////////////////////////////////////////////
  // Builds a reimbursement selection list.                                   //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildReimbursementSelector($TabIndex, $Name, $Class, $UID)                                                                               
  {
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    if ($UID == "")
    {
      $resultSet = ExecuteQuery('SELECT * FROM Staff ORDER BY Staff_First_Name, Staff_Last_Name ASC');      
      while ($row = MySQL_Fetch_Array($resultSet))
      {
        $resultSetTemp = ExecuteQuery('SELECT * FROM ReimbursiveAllowance WHERE ReimbursiveAllowance_Name = "'.$row['Staff_Code'].'" AND ReimbursiveAllowance_Payment = "0" ORDER BY ReimbursiveAllowance_Date ASC');
        while ($rowTemp = MySQL_Fetch_Array($resultSetTemp))
        {
          echo '<OPTION value="'.$rowTemp['ReimbursiveAllowance_ID'].'">'.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].' - '.GetTextualDateFromDatabaseDate($rowTemp['ReimbursiveAllowance_Date']).'</OPTION>';
        }
      }
    } else
    {
      $resultSetTemp = ExecuteQuery('SELECT * FROM ReimbursiveAllowance WHERE ReimbursiveAllowance_Name = "'.$_SESSION['cUID'].'" AND ReimbursiveAllowance_Payment = "0" ORDER BY ReimbursiveAllowance_Date ASC');
      while ($rowTemp = MySQL_Fetch_Array($resultSetTemp))
      {
        echo '<OPTION value="'.$rowTemp['ReimbursiveAllowance_ID'].'">'.GetTextualDateFromDatabaseDate($rowTemp['ReimbursiveAllowance_Date']).'</OPTION>';
      }
    }
    echo '</SELECT>';                    
  }
                         
  //////////////////////////////////////////////////////////////////////////////
  // Builds a shift selection list.                                           //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildShiftSelector($TabIndex, $Name, $Class , $ShiftTypeID)                                                                               
  {                                        
    $resultSet = ExecuteQuery('SELECT ShiftAllowanceType_ID, ShiftAllowanceType_Description FROM ShiftAllowanceType ORDER BY ShiftAllowanceType_Description');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    { 
      if ($row['ShiftAllowanceType_ID'] == $ShiftTypeID)
        echo '<OPTION value="'.$row['ShiftAllowanceType_ID'].'" SELECTED>'.$row['ShiftAllowanceType_Description'].'</OPTION>';
      else
        echo '<OPTION value="'.$row['ShiftAllowanceType_ID'].'">'.$row['ShiftAllowanceType_Description'].'</OPTION>';
    }         
    echo '</SELECT>';                    
  }
               
  //////////////////////////////////////////////////////////////////////////////
  // Builds a shift selection list.                                           //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildTaskSelector($TabIndex, $Name, $Class, $TaskID, $StaffCode = "", $Type = 'Open', $Date = "")                                                                               
  {
    if ($StaffCode == "")    
      $staff = "";
    else
      if ($Type == 'All')    
        $staff = 'WHERE Task_Name = "'.$StaffCode.'" ';
      else
        $staff = 'AND Task_Name = "'.$StaffCode.'" '; 
    
    if ($Date == "")
      $date = "";
    else
    {
      if ($StaffCode == "")
      {
        if ($Type == 'All')
          $date = 'WHERE ((Task_Status = "1" AND Task_LastUpdate >= "'.$Date.' 00:00:00" AND Task_Assigned <= "'.$Date.' 23:59:59") OR (Task_Status = "0" AND Task_Assigned <= "'.$Date.' 23:59:59")) ';
        else
          $date = ' AND ((Task_Status = "1" AND Task_LastUpdate >= "'.$Date.' 00:00:00" AND Task_Assigned <= "'.$Date.' 23:59:59") OR (Task_Status = "0" AND Task_Assigned <= "'.$Date.' 23:59:59")) ';
      } else
      {
        $date = ' AND ((Task_Status = "1" AND Task_LastUpdate >= "'.$Date.' 00:00:00" AND Task_Assigned <= "'.$Date.' 23:59:59") OR (Task_Status = "0" AND Task_Assigned <= "'.$Date.' 23:59:59")) ';
      }
    }
    
    switch ($Type)
    {
      case 'All':
        $resultSet = ExecuteQuery('SELECT Task_Title, Task_ID FROM Task '.$staff.$date.'ORDER BY Task_Title');
        break;
      case 'Closed':
        if ($TaskID == "")
          $resultSet = ExecuteQuery('SELECT Task_Title, Task_ID FROM Task WHERE Task_Status = "1" '.$staff.$date.'ORDER BY Task_Title');
        else  
          $resultSet = ExecuteQuery('SELECT Task_Title, Task_ID FROM Task WHERE (Task_Status = "1" OR Task_ID = "'.$TaskID.'") '.$staff.$date.'ORDER BY Task_Title');
        break;    
      case 'Open':
        if ($TaskID == "")
          $resultSet = ExecuteQuery('SELECT Task_Title, Task_ID FROM Task WHERE Task_Status <> "1" '.$staff.$date.'ORDER BY Task_Title');
        else  
          $resultSet = ExecuteQuery('SELECT Task_Title, Task_ID FROM Task WHERE (Task_Status <> "1" OR Task_ID = "'.$TaskID.'") '.$staff.$date.'ORDER BY Task_Title');
        break;
      default:
        break;
    }       
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';            
    while ($row = MySQL_Fetch_Array($resultSet))
    { 
      if ($row['Task_ID'] == $TaskID)
        echo '<OPTION value="'.$row['Task_ID'].'" SELECTED>'.$row['Task_Title'].'</OPTION>';
      else
        echo '<OPTION value="'.$row['Task_ID'].'">'.$row['Task_Title'].'</OPTION>';
    }         
    echo '</SELECT>';                    
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a overtime payout type selection list.                            //
  //////////////////////////////////////////////////////////////////////////////
  function BuildOvertimePayoutSelector($TabIndex, $Name, $Class, $Status)
  {
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    if ('1' == $Status)
      echo '<OPTION value="1" SELECTED>All</OPTION>';
    else
      echo '<OPTION value="1">All</OPTION>';
    if ('0' == $Status)
      echo '<OPTION value="0" SELECTED>Capped</OPTION>';
    else
      echo '<OPTION value="0">Capped</OPTION>';
    if ('2' == $Status)
      echo '<OPTION value="2" SELECTED>None</OPTION>';
    else
      echo '<OPTION value="2">None</OPTION>';
    echo '</SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a task report type selection list.                                //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildTaskReportTypeSelector($TabIndex, $Name, $Class)                                                                               
  {
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>
            <OPTION value="1">Assigned</OPTION>
            <OPTION value="0">Received</OPTION>
          </SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a task status selection list.                                     //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildTaskStatusSelector($TabIndex, $Name, $Class, $Status)                                                                               
  {                                        
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    if ('1' == $Status)
      echo '<OPTION value="1" SELECTED>Complete</OPTION>';
    else
      echo '<OPTION value="1">Complete</OPTION>';
    if ('2' == $Status)
      echo '<OPTION value="2" SELECTED>Follow-Up</OPTION>';
    else
      echo '<OPTION value="2">Follow-Up</OPTION>';
    if ('3' == $Status)
      echo '<OPTION value="3" SELECTED>On Hold</OPTION>';
    else
      echo '<OPTION value="3">On Hold</OPTION>';
    if ('0' == $Status)
      echo '<OPTION value="0" SELECTED>Pending</OPTION>';
    else
      echo '<OPTION value="0">Pending</OPTION>';
    echo '</SELECT>';                    
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds an auth level selection list.                                     //
  //////////////////////////////////////////////////////////////////////////////
  function BuildAuthLevelSelector($TabIndex, $Name, $Class, $Auth)
  {
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">';
    if (0 == $Auth)
      echo '<OPTION value="0" SELECTED>0</OPTION>';
    else
      echo '<OPTION value="0">0</OPTION>';
    if (1 == $Auth)
      echo '<OPTION value="1" SELECTED>1</OPTION>';
    else
      echo '<OPTION value="1">1</OPTION>';
    if (2 == $Auth)
      echo '<OPTION value="2" SELECTED>2</OPTION>';
    else
      echo '<OPTION value="2">2</OPTION>';
    if (5 == $Auth)
      echo '<OPTION value="5" SELECTED>5</OPTION>';
    else
      echo '<OPTION value="5">5</OPTION>';
    if (9 == $Auth)
      echo '<OPTION value="9" SELECTED>9</OPTION>';
    else
      echo '<OPTION value="9">9</OPTION>';
    if (13 == $Auth)
      echo '<OPTION value="13" SELECTED>13</OPTION>';
    else
      echo '<OPTION value="13">13</OPTION>';
    if (17 == $Auth)
      echo '<OPTION value="17" SELECTED>17</OPTION>';
    else
      echo '<OPTION value="17">17</OPTION>';
    if (21 == $Auth)
      echo '<OPTION value="21" SELECTED>21</OPTION>';
    else
      echo '<OPTION value="21">21</OPTION>';
    if (29 == $Auth)
      echo '<OPTION value="29" SELECTED>29</OPTION>';
    else
      echo '<OPTION value="29">29</OPTION>';
    if (37 == $Auth)
      echo '<OPTION value="37" SELECTED>37</OPTION>';
    else
         echo '<OPTION value="37">37</OPTION>';
    if (53 == $Auth)
      echo '<OPTION value="53" SELECTED>53</OPTION>';
    else
      echo '<OPTION value="53">53</OPTION>';
    if (61 == $Auth)
      echo '<OPTION value="61" SELECTED>61</OPTION>';
    else
      echo '<OPTION value="61">61</OPTION>';
    if (223 == $Auth)
      echo '<OPTION value="223" SELECTED>223</OPTION>';
    else
      echo '<OPTION value="223">223</OPTION>';
    if (255 == $Auth)
      echo '<OPTION value="255" SELECTED>255</OPTION>';
    else
      echo '<OPTION value="255">255</OPTION>';
    echo '</SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a contract selection list.                                        //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildContractSelector($TabIndex, $Name, $Class , $ContractID)                                                                               
  {                                        
    $resultSet = ExecuteQuery('SELECT StaffContract_ID, StaffContract_Description FROM StaffContract ORDER BY StaffContract_Description');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    { 
      if ($row['StaffContract_ID'] == $ContractID)
        echo '<OPTION value="'.$row['StaffContract_ID'].'" SELECTED>'.$row['StaffContract_Description'].'</OPTION>';
      else
        echo '<OPTION value="'.$row['StaffContract_ID'].'">'.$row['StaffContract_Description'].'</OPTION>';
    }         
    echo '</SELECT>';                    
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a standby type selection list.                                    //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildStandbySelector($TabIndex, $Name, $Class , $StandbyID)
  {
    $resultSet = ExecuteQuery('SELECT StandbyType_ID, StandbyType_Description FROM StandbyType ORDER BY StandbyType_Description');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    { 
      if ($row['StandbyType_ID'] == $StandbyID)
        echo '<OPTION value="'.$row['StandbyType_ID'].'" SELECTED>'.$row['StandbyType_Description'].'</OPTION>';
      else
        echo '<OPTION value="'.$row['StandbyType_ID'].'">'.$row['StandbyType_Description'].'</OPTION>';
    }
    echo '</SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a proposed standby selection list.                                //
  //////////////////////////////////////////////////////////////////////////////
  function BuildProposedStandbySelector($TabIndex, $Name, $Class , $UID = "")
  {
    $date = ' AND StandbyProposed_DateStart >= "'.Date('Y-m-d', MkTime(0, 0, 0, Date('m'), -30, Date('Y'))).'"';
    
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    if ($UID == "")
    {
      $resultSet = ExecuteQuery('SELECT StandbyProposed.*, Staff_First_Name, Staff_Last_Name, StandbyType_Description FROM StandbyProposed, Staff, StandbyType WHERE StandbyProposed_Name = Staff_Code AND StandbyProposed_Type = StandbyType_ID'.$date.' ORDER BY Staff_First_Name, Staff_Last_Name, StandbyProposed_DateStart ASC');
      while ($row = MySQL_Fetch_Array($resultSet))
      { 
        echo '<OPTION value="'.$row['StandbyProposed_ID'].'">'.$row['Staff_First_Name'].' '.$row['Staff_First_Name'].' - '.GetTextualDateFromDatabaseDate($row['StandbyProposed_DateStart']).'</OPTION>';
      }
    } else
    {
      $resultSet = ExecuteQuery('SELECT StandbyProposed.*, StandbyType_Description FROM StandbyProposed, StandbyType WHERE StandbyProposed_Name = '.$UID.' AND StandbyProposed_Type = StandbyType_ID'.$date.' ORDER BY StandbyProposed_DateStart ASC');
      while ($row = MySQL_Fetch_Array($resultSet))
      { 
        echo '<OPTION value="'.$row['StandbyProposed_ID'].'">'.GetTextualDateFromDatabaseDate($row['StandbyProposed_DateStart']).'</OPTION>';
      }
    }
    echo '</SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a staff management selection list.                                //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildStaffManSelector($TabIndex, $Name, $Class , $StaffCode, $Auth = 8)                                                                                  
  { 
    $resultSet = ExecuteQuery('SELECT Staff_First_Name, Staff_Last_Name, Staff_Code FROM Staff WHERE (Staff_Auth_Level & '.$Auth.') AND `Staff_IsEmployee` != 0 ORDER BY Staff_First_Name ASC');
    
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    { 
      if ($row['Staff_Code'] == $StaffCode)
        echo '<OPTION value="'.$row['Staff_Code'].'" SELECTED>'.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].'</OPTION>';
      else
        echo '<OPTION value="'.$row['Staff_Code'].'">'.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].'</OPTION>';
    }         
    echo '</SELECT>';                    
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a function selection list.                                        //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildFunctionSelector($TabIndex, $Name, $Class , $FunctionType)                                                                                  
  { 
    $resultSet = ExecuteQuery('SELECT Function_ID, Function_Description FROM Function ORDER BY Function_ID ASC');
    
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    { 
      if ($row['Function_ID'] == $FunctionType)
        echo '<OPTION value="'.$row['Function_ID'].'" SELECTED>'.$row['Function_Description'].'</OPTION>';
      else
        echo '<OPTION value="'.$row['Function_ID'].'">'.$row['Function_Description'].'</OPTION>';
    }         
    echo '</SELECT>';                    
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a position selection list.                                        //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildPositionSelector($TabIndex, $Name, $Class , $PositionType)                                                                                  
  { 
    $resultSet = ExecuteQuery('SELECT Position_ID, Position_Description FROM Position ORDER BY Position_ID ASC');
    
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    { 
      if ($row['Position_ID'] == $PositionType)
        echo '<OPTION value="'.$row['Position_ID'].'" SELECTED>'.$row['Position_Description'].'</OPTION>';
      else
        echo '<OPTION value="'.$row['Position_ID'].'">'.$row['Position_Description'].'</OPTION>';
    }         
    echo '</SELECT>';                    
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a leave type selection list.                                      //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildLeaveSelector($TabIndex, $Name, $Class , $LeaveTypeID, $Auth = 8)                                                                                  
  { 
    $resultSet = ExecuteQuery('SELECT LeaveType_ID, LeaveType_Description FROM LeaveType ORDER BY LeaveType_Description');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    { 
      if ($row['LeaveType_ID'] == $LeaveTypeID)
        echo '<OPTION value="'.$row['LeaveType_ID'].'" SELECTED>'.$row['LeaveType_Description'].'</OPTION>';
      else
        echo '<OPTION value="'.$row['LeaveType_ID'].'">'.$row['LeaveType_Description'].'</OPTION>';
    }         
    echo '</SELECT>';                
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a leave cancellation selection list.                              //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildLeaveCancelSelector($TabIndex, $Name, $Class,$Access=false)                                     
  {
    $date = ' AND Leave_Start >= "'.Date('Y-m-d', MkTime(0, 0, 0, Date('m')-1, 0, Date('Y'))).'" AND Leave_End >= "'.Date('Y-m-d').'"';
    if($Access)
        $date = ' AND Leave_Start >= "'.Date('Y-m-d', MkTime(0, 0, 0, Date('m')-2, 0, Date('Y'))).'"';
    $resultSet = ExecuteQuery('SELECT Leave_ID, Leave_Start, Staff_First_Name, Staff_Last_Name FROM `Leave`, Staff WHERE Leave_Employee = Staff_Code AND Leave_IsApproved <> "3"'.$date.' ORDER BY Staff_First_Name, Staff_Last_Name');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    { 
      echo '<OPTION value="'.$row['Leave_ID'].'">'.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].' - '.GetTextualDateFromDatabaseDate($row['Leave_Start']).'</OPTION>';
    }         
    echo '</SELECT>';     
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a leave edit selection list.                                      //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildLeaveEditSelector($TabIndex, $Name, $Class, $AuthID,$Access=false)                                    
  {
    $date = ' AND Leave_Start >= "'.Date('Y-m-d', MkTime(0, 0, 0, Date('m')-1, 0, Date('Y'))).'" AND Leave_End >= "'.Date('Y-m-d').'"';
    if($Access)
    {
        $date = ' AND Leave_Start >= "'.Date('Y-m-d', MkTime(0, 0, 0, Date('m')-2, 0, Date('Y'))).'"';
        $resultSet = ExecuteQuery('SELECT Leave_ID, Leave_Start, Staff_First_Name, Staff_Last_Name FROM `Leave`, Staff WHERE (Leave_Employee = Staff_Code)'.$date.' ORDER BY Staff_First_Name, Staff_Last_Name');
    }
    else
        $resultSet = ExecuteQuery('SELECT Leave_ID, Leave_Start, Staff_First_Name, Staff_Last_Name FROM `Leave`, Staff WHERE (Leave_Employee = Staff_Code) AND ((Staff_Code = "'.$AuthID.'") OR (Leave_Approved = "'.$AuthID.'"))'.$date.' ORDER BY Staff_First_Name, Staff_Last_Name');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    { 
      echo '<OPTION value="'.$row['Leave_ID'].'">'.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].' - '.GetTextualDateFromDatabaseDate($row['Leave_Start']).'</OPTION>';
    }         
    echo '</SELECT>';     
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a RFQ selection list.                                             //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildRFQSelector($TabIndex, $Name, $Class , $RFQID)                                                                                  
  { 
    $resultSet = ExecuteQuery('SELECT RFQ_ID, RFQ_Reference FROM RFQ ORDER BY RFQ_Reference');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    { 
      if ($row['RFQ_ID'] == $RFQID)
        echo '<OPTION value="'.$row['RFQ_ID'].'" SELECTED>'.$row['RFQ_Reference'].'</OPTION>';
      else
        echo '<OPTION value="'.$row['RFQ_ID'].'">'.$row['RFQ_Reference'].'</OPTION>';
    }         
    echo '</SELECT>';                
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a quotation selection list.                                       //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildQuotationSelector($TabIndex, $Name, $Class , $QuoteID, $Revision = false, $Open = false, $UID = "")                                                                                  
  {
    if ($UID != "")
      $author = 'Quote_Author = '.$UID.'';
    else
      $author = '1=1';
    
    if ($Open)
      $resultSet = ExecuteQuery('SELECT Quote_ID, Quote_Reference, Quote_Revision FROM Quote WHERE (Quote_Accepted_Declined = "0" OR Quote_Accepted_Declined = "3") AND '.$author.' ORDER BY Quote_Reference, Quote_Revision');
    else
      $resultSet = ExecuteQuery('SELECT Quote_ID, Quote_Reference, Quote_Revision FROM Quote WHERE '.$author.' ORDER BY Quote_Reference, Quote_Revision');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';                  
    while ($row = MySQL_Fetch_Array($resultSet))
    { 
      if ($Revision)
        $revision = ' - Rev. '.$row['Quote_Revision'];
      else
        $revision = "";
      if ($row['Quote_ID'] == $QuoteID)
        echo '<OPTION value="'.$row['Quote_ID'].'" SELECTED>'.$row['Quote_Reference'].$revision.'</OPTION>';
      else
        echo '<OPTION value="'.$row['Quote_ID'].'">'.$row['Quote_Reference'].$revision.'</OPTION>';
    }         
    echo '</SELECT>';                
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a purchase order selection list.                                  //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildPurchaseOrderSelector($TabIndex, $Name, $Class , $PurchaseOrderID)                                                                                  
  { 
    $resultSet = ExecuteQuery('SELECT PurchaseOrder_ID, PurchaseOrder_Number FROM PurchaseOrder ORDER BY PurchaseOrder_Number');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';                  
    while ($row = MySQL_Fetch_Array($resultSet))
    { 
      if ($row['PurchaseOrder_ID'] == $PurchaseOrderID)
        echo '<OPTION value="'.$row['PurchaseOrder_ID'].'" SELECTED>'.$row['PurchaseOrder_Number'].'</OPTION>';
      else
        echo '<OPTION value="'.$row['PurchaseOrder_ID'].'">'.$row['PurchaseOrder_Number'].'</OPTION>';
    }         
    echo '</SELECT>';                
  }
  
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a purchase order report selection list.                           //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildPurchaseOrderReportSelector($TabIndex, $Name, $Class)                                     
  {                        
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>
            <OPTION value="1">Complete</OPTION>
            <OPTION value="0">Incomplete</OPTION>
            <OPTION value="2">Outstanding</OPTION>
          </SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a supplier selection list.                                        //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildSupplierSelector($TabIndex, $Name, $Class , $SupplierCode, $Restricted = false)                                                                                  
  { 
    $restrict = "";
    if ($Restricted)
      $restrict = ' AND Supplier_Code <> 413';
    
    $resultSet = ExecuteQuery('SELECT Supplier_Code, Supplier_Name FROM Supplier WHERE  Supplier_IsInactive = 0 AND 1=1'.$restrict.' ORDER BY Supplier_Name');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';                  
    while ($row = MySQL_Fetch_Array($resultSet))
    { 
      if ($row['Supplier_Code'] == $SupplierCode)
        echo '<OPTION value="'.$row['Supplier_Code'].'" SELECTED>'.iconv('Windows-1252', 'UTF-8//TRANSLIT',$row['Supplier_Name']).'</OPTION>';
      else
        echo '<OPTION value="'.$row['Supplier_Code'].'">'.iconv('Windows-1252', 'UTF-8//TRANSLIT',$row['Supplier_Name']).'</OPTION>';
    }         
    echo '</SELECT>';                
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a quotation status selection list.                                //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildQuotationStatusSelector($TabIndex, $Name, $Class , $QuoteTypeID)                                                                                  
  {                                       
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    if ('1' == $QuoteTypeID)                                                
      echo '<OPTION value="1" SELECTED>Accepted</OPTION>';
    else                                                                      
      echo '<OPTION value="1">Accepted</OPTION>';      
    if ('2' == $QuoteTypeID)                                                
      echo '<OPTION value="2" SELECTED>Declined</OPTION>';
    else                                                                      
      echo '<OPTION value="2">Declined</OPTION>';      
    if ('0' == $QuoteTypeID)                                                
      echo '<OPTION value="0" SELECTED>Pending</OPTION>';
    else                                                                      
      echo '<OPTION value="0">Pending</OPTION>';
    if ('3' == $QuoteTypeID)                                                
      echo '<OPTION value="3" SELECTED>Sent</OPTION>';
    else                                                                      
      echo '<OPTION value="3">Sent</OPTION>';
    echo '</SELECT>';                
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a payment method selection list.                                  //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildCurrencySelector($TabIndex, $Name, $Class, $CurrencyID)                                     
  {                        
    $resultSet = ExecuteQuery('SELECT Currency_ID, Currency_Symbol FROM Currency ORDER BY Currency_Symbol');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    { 
      if ($row['Currency_ID'] == $CurrencyID)
        echo '<OPTION value="'.iconv('Windows-1252', 'UTF-8//TRANSLIT', $row['Currency_ID']).'" SELECTED>'.iconv('Windows-1252', 'UTF-8//TRANSLIT', $row['Currency_Symbol']).'</OPTION>';
      else
        echo '<OPTION value="'.iconv('Windows-1252', 'UTF-8//TRANSLIT', $row['Currency_ID']).'">'.iconv('Windows-1252', 'UTF-8//TRANSLIT', $row['Currency_Symbol']).'</OPTION>';
    }         
    echo '</SELECT>';     
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds an internal order selection list.                                 //
  //////////////////////////////////////////////////////////////////////////////
  function BuildInternalOrderSelector($TabIndex, $Name, $Class, $OrderNumber, $OrderApproved = "", $OrderCompleted = "", $HideNumber = false, $NotPending = false, $paLimit = "", $fullAccess=true)                                     
  {
      
      $testin = '1';
      
    if ($NotPending)
    {
      $pending = ' AND ( OrderNo_Project <> "236")';
    } else
      $pending = "";
    
    if ($OrderApproved == "")             
      if ($OrderCompleted == "")
        $resultSet = 'SELECT Supplier_Name, OrderNo.* FROM OrderNo, Supplier WHERE 1=1'.$pending.' AND OrderNo_Supplier = Supplier_Code ORDER BY OrderNo_Number DESC'.($paLimit == "" ? "" : ' LIMIT '.$paLimit);
      else
        $resultSet = 'SELECT Supplier_Name, OrderNo.* FROM OrderNo, Supplier WHERE OrderNo_Complete = "'.$OrderCompleted.'"'.$pending.' AND OrderNo_Supplier = Supplier_Code ORDER BY OrderNo_Number DESC'.($paLimit == "" ? "" : ' LIMIT '.$paLimit);
    else
       if ($OrderCompleted == "")
           if($fullAccess)
                $resultSet = 'SELECT Supplier_Name, OrderNo.* FROM OrderNo, Supplier WHERE OrderNo_Approved = "'.$OrderApproved.'"'.$pending.' AND OrderNo_Supplier = Supplier_Code ORDER BY OrderNo_Number DESC'.($paLimit == "" ? "" : ' LIMIT '.$paLimit);
           else 
                $resultSet ='SELECT Supplier_Name, OrderNo.* FROM OrderNo,Project, Supplier WHERE OrderNo_Supplier = Supplier_Code AND OrderNo.OrderNo_Project = Project.Project_Code AND (Project.Project_CostManager = "'.$_SESSION['cUID'].'" OR Project.Project_Responsible = "'.$_SESSION['cUID'].'") AND OrderNo_Approved = "'.$OrderApproved.'"'.$pending.' ORDER BY OrderNo_Number DESC'.($paLimit == "" ? "" : ' LIMIT '.$paLimit);
           
      else
        $resultSet = 'SELECT Supplier_Name, OrderNo.* FROM OrderNo, Supplier WHERE OrderNo_Supplier = Supplier_Code AND OrderNo_Approved = "'.$OrderApproved.'" AND OrderNo_Complete = "'.$OrderCompleted.'"'.$pending.' ORDER BY OrderNo_Number DESC'.($paLimit == "" ? "" : ' LIMIT '.$paLimit);
      
    $orders = array();
    $resultSet= ExecuteQuery($resultSet);
    
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'"><OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    {
      $row['Supplier_Name'] = str_replace("'", "", $row['Supplier_Name']);  
      if ($HideNumber)
      {
        if ($row['OrderNo_Requested'] == $_SESSION['cUID'])
        {
          if ($row['OrderNo_Approved'] == '1')
          {
            if ($row['OrderNo_Number'] == $OrderNumber)
              echo '<OPTION value="'.$row['OrderNo_Number'].'" SELECTED>'.$row['OrderNo_Number'].' - '.$row['Supplier_Name'].'</OPTION>';
            else
              echo '<OPTION value="'.$row['OrderNo_Number'].'">'.$row['OrderNo_Number'].' - '.$row['Supplier_Name'].'</OPTION>';
          } else
          {
           $orders[] = array($row['OrderNo_Number'], GetTextualDateTimeFromDatabaseDateTime($row['OrderNo_Date_Time']));
          }
        }
      } else
      {
        if ($row['OrderNo_Number'] == $OrderNumber)
          echo '<OPTION value="'.$row['OrderNo_Number'].'" SELECTED>'.$row['OrderNo_Number'].' - '.$row['Supplier_Name'].'</OPTION>';
        else
          echo '<OPTION value="'.$row['OrderNo_Number'].'">'.$row['OrderNo_Number'].' - '.$row['Supplier_Name'].'</OPTION>';
      }
    }
    for ($count = 0; $count < SizeOf($orders); $count++)
    {
      if ($orders[$count][0] == $OrderNumber)
        echo '<OPTION value="'.$orders[$count][0].'" SELECTED>'.$orders[$count][1].'</OPTION>';
      else
        echo '<OPTION value="'.$orders[$count][0].'">'.$orders[$count][1].'</OPTION>';
    }
    echo '</SELECT>';         
  }
  
  function BuildInternalOrderSelector6Months($TabIndex, $Name, $Class, $OrderNumber, $OrderApproved = "", $OrderCompleted = "", $HideNumber = false, $NotPending = false, $paLimit = "", $fullAccess=true)                                     
  {
      
    if ($NotPending)
    {
      $pending = ' AND ( OrderNo_Project <> "236")';
    } else
      $pending = "";

      $date = ' AND OrderNo_Approved_Date > "'.Date('Y-m-d', MkTime(0, 0, 0, Date('m')-6, Date('d'), Date('Y'))).'"'; // six month ago
   

    $sql = 'SELECT Supplier_Name, OrderNo.* FROM OrderNo, Supplier WHERE OrderNo_Supplier = Supplier_Code AND OrderNo_Approved = "'.$OrderApproved.'" AND OrderNo_Complete = "'.$OrderCompleted.'"'.$pending.$date.' ORDER BY OrderNo_Number DESC'.($paLimit == "" ? "" : ' LIMIT '.$paLimit);
    
    $orders = array();
     $resultSet = ExecuteQuery($sql);
    
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'"><OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    {
       $row['Supplier_Name'] = str_replace("'", "", $row['Supplier_Name']);
        if ($row['OrderNo_Number'] == $OrderNumber)
          echo '<OPTION value="'.$row['OrderNo_Number'].'" SELECTED>'.$row['OrderNo_Number'].' - '.$row['Supplier_Name'].'</OPTION>';
        else
          echo '<OPTION value="'.$row['OrderNo_Number'].'">'.$row['OrderNo_Number'].' - '.$row['Supplier_Name'].'</OPTION>';
    
    }
    for ($count = 0; $count < SizeOf($orders); $count++)
    {
      if ($orders[$count][0] == $OrderNumber)
        echo '<OPTION value="'.$orders[$count][0].'" SELECTED>'.$orders[$count][1].'</OPTION>';
      else
        echo '<OPTION value="'.$orders[$count][0].'">'.$orders[$count][1].'</OPTION>';
    }
    echo '</SELECT>';         
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds an internal order selection list.                                 //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildInternalOrderTypeSelector($TabIndex, $Name, $Class, $TypeID)                                     
  {                        
    $resultSet = ExecuteQuery('SELECT OrderType_ID, OrderType_Description FROM OrderType ORDER BY OrderType_Description');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    { 
      if ($row['OrderType_ID'] == $TypeID)
        echo '<OPTION value="'.$row['OrderType_ID'].'" SELECTED>'.$row['OrderType_Description'].'</OPTION>';
      else
        echo '<OPTION value="'.$row['OrderType_ID'].'">'.$row['OrderType_Description'].'</OPTION>';
    }         
    echo '</SELECT>';     
  }
  
   function BuildInternalOrderBEElevelSelector($TabIndex, $Name, $Class, $TypeID)                                     
  {                        
   
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">';
    $o = 0;       
    echo'<OPTION value="'.$o.'">non-compliant</OPTION>';
    
    for($i = 1; $i<11; $i++) 
    {  
        
        if($i===9){
            if($TypeID==$i)
                echo '<OPTION value="'.$i.'" SELECTED>N/A - International</OPTION>';
            else
                echo '<OPTION value="'.$i.'">N/A - International</OPTION>';
        }
        
        else if($i===10){
              if($TypeID==$i)
                echo '<OPTION value="'.$i.'" SELECTED>Awaiting feedback</OPTION>';
            else
                echo '<OPTION value="'.$i.'">Awaiting feedback</OPTION>';
        }
        else{
            if($TypeID==$i)
                echo '<OPTION value="'.$i.'" SELECTED>'.$i.'</OPTION>';
            else
                echo '<OPTION value="'.$i.'">'.$i.'</OPTION>';
        }
        
        // add in additional N/A – International
    }
    echo '</SELECT>';     
  }
  
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds an internal order status selection list.                          //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildInternalOrderStatusSelector($TabIndex, $Name, $Class, $Status)                                     
  {                        
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    if ($Status == '2')
      echo '<OPTION value="2" SELECTED>Cancelled</OPTION>';
    else
      echo '<OPTION value="2">Cancelled</OPTION>';
    if ($Status == '1')
      echo '<OPTION value="1" SELECTED>Complete</OPTION>';
    else
      echo '<OPTION value="1">Complete</OPTION>';
    if ($Status == '3')
      echo '<OPTION value="3" SELECTED>Followed Up</OPTION>';
    else
      echo '<OPTION value="3">Followed Up</OPTION>';
    if ($Status == '0')
      echo '<OPTION value="0" SELECTED>Incomplete</OPTION>';
    else
      echo '<OPTION value="0">Incomplete</OPTION>';
    echo '</SELECT>';         
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a project report type selection list.                             //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildProjectsReportTypeSelector($TabIndex, $Name, $Class)                                     
  {                        
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>
            <OPTION value="0">List</OPTION>
            <OPTION value="1">Summary</OPTION>
          </SELECT>';         
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a project type selection list.                                    //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildProjectsProjectTypeSelector($TabIndex, $Name, $Class)                                     
  {                        
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>
            <OPTION value="0">Closed</OPTION>
            <OPTION value="1">Open</OPTION>
          </SELECT>';         
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a delivery method selection list.                                 //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildDeliveryMethodSelector($TabIndex, $Name, $Class, $Method)                                     
  {                        
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="" SELECTED>< ... ></OPTION>';
    if ($Method == '0')
      echo '<OPTION value="0" SELECTED>Collect</OPTION>';
    else
      echo '<OPTION value="0">Collect</OPTION>';
    if ($Method == '2')
      echo '<OPTION value="2" SELECTED>Courier</OPTION>';
    else
      echo '<OPTION value="2">Courier</OPTION>';       
    if ($Method == '1')
      echo '<OPTION value="1" SELECTED>Delivery</OPTION>';
    else
      echo '<OPTION value="1">Delivery</OPTION>';
    echo '</SELECT>';         
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a invoice selection list.                                         //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildInvoiceSelector($TabIndex, $Name, $Class, $InvoiceID, $IncludeOrder = true)                                     
  {
      $SQL = 'SELECT SupplierInvoice_ID, SupplierInvoice_Number, SupplierInvoice_S4OrderNo FROM SupplierInvoice ORDER BY SupplierInvoice_S4OrderNo DESC';
    $resultSet = ExecuteQuery($SQL);
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'"><OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    { 
      if ($IncludeOrder)
        $order = $row['SupplierInvoice_S4OrderNo'].' - Invoice: ';
      else
        $order = "";
        
      if ($row['SupplierInvoice_ID'] == $InvoiceID)
        echo '<OPTION value="'.$row['SupplierInvoice_ID'].'" SELECTED>'.$order.$row['SupplierInvoice_Number'].'</OPTION>';
      else
        echo '<OPTION value="'.$row['SupplierInvoice_ID'].'">'.$order.$row['SupplierInvoice_Number'].'</OPTION>';
    }         
    echo '</SELECT>';     
  }
  
  function BuildInvoiceSelector6Months($TabIndex, $Name, $Class, $InvoiceID, $IncludeOrder = true)                                     
  {

    $date = 'LEFT JOIN OrderNo on OrderNo.OrderNo_Number = SupplierInvoice_S4OrderNo WHERE OrderNo_Approved_Date >"'.Date('Y-m-d', MkTime(0, 0, 0, Date('m')-6, Date('d'), Date('Y'))).'"'; // six month ago
    $SQL = 'SELECT SupplierInvoice_ID, SupplierInvoice_Number, SupplierInvoice_S4OrderNo FROM SupplierInvoice '.$date. ' ORDER BY SupplierInvoice_S4OrderNo DESC';
    $resultSet = ExecuteQuery($SQL);
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'"><OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    { 
      if ($IncludeOrder)
        $order = $row['SupplierInvoice_S4OrderNo'].' - Invoice: ';
      else
        $order = "";
        
      if ($row['SupplierInvoice_ID'] == $InvoiceID)
        echo '<OPTION value="'.$row['SupplierInvoice_ID'].'" SELECTED>'.$order.$row['SupplierInvoice_Number'].'</OPTION>';
      else
        echo '<OPTION value="'.$row['SupplierInvoice_ID'].'">'.$order.$row['SupplierInvoice_Number'].'</OPTION>';
    }         
    echo '</SELECT>';     
  }
  
  
    //////////////////////////////////////////////////////////////////////////////
  // Builds a category selection list.                                         //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildReportsCategorySelector($TabIndex, $Name, $Class, $CategoryID)                                     
  {
    $resultSet = ExecuteQuery('SELECT ReportsCategory_ID,ReportsCategory_Name FROM ReportsCategory ORDER BY ReportsCategory_Name ASC');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    { 
 
      if ($row['ReportsCategory_ID'] == $CategoryID)
        echo '<OPTION value="'.$row['ReportsCategory_ID'].'" SELECTED>'.$row['ReportsCategory_Name'].'</OPTION>';
      else
        echo '<OPTION value="'.$row['ReportsCategory_ID'].'">'.$row['ReportsCategory_Name'].'</OPTION>';
    }         
    echo '</SELECT>';     
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds an availability selection list.                                   //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildAvailabilitySelector($TabIndex, $Name, $Class, $StaffID)                                     
  {
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    if ($StaffID == "")
    {
      $resultSet = ExecuteQuery('SELECT * FROM Staff ORDER BY Staff_First_Name, Staff_Last_Name ASC');
      while ($row = MySQL_Fetch_Array($resultSet))
      { 
        $resultSetTemp = ExecuteQuery('SELECT * FROM Availability WHERE Availability_Name = '.$row['Staff_Code'].' AND Availability_End >= "'.Date('Y-m-d').' 00:00:00" ORDER BY Availability_Start');
        while ($rowTemp = MySQL_Fetch_Array($resultSetTemp))
        {
          if ($rowTemp['Availability_Start'] == $rowTemp['Availability_End'])
            $dateRange = GetTextualDateFromDatabaseDate($rowTemp['Availability_Start']);
          else
            $dateRange = GetTextualDateFromDatabaseDate($rowTemp['Availability_Start']).' to '.GetTextualDateFromDatabaseDate($rowTemp['Availability_End']);
          
          echo '<OPTION value="'.$rowTemp['Availability_ID'].'">'.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].' - '.$dateRange.'</OPTION>';
        }
      }
    } else
    {
      $resultSetTemp = ExecuteQuery('SELECT * FROM Availability WHERE Availability_Name = '.$_SESSION['cUID'].' AND Availability_End >= "'.Date('Y-m-d').' 00:00:00" ORDER BY Availability_Start');
      while ($rowTemp = MySQL_Fetch_Array($resultSetTemp))
      {
        if ($rowTemp['Availability_Start'] == $rowTemp['Availability_End'])
          $dateRange = GetTextualDateFromDatabaseDate($rowTemp['Availability_Start']);
        else
          $dateRange = GetTextualDateFromDatabaseDate($rowTemp['Availability_Start']).' to '.GetTextualDateFromDatabaseDate($rowTemp['Availability_End']);
        
        echo '<OPTION value="'.$rowTemp['Availability_ID'].'">'.$dateRange.'</OPTION>';
      }
    }
    echo '</SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds an availability type selection list.                              //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildAvailabilityTypeSelector($TabIndex, $Name, $Class, $TypeID)                                     
  {
    $resultSet = ExecuteQuery('SELECT * FROM AvailabilityType ORDER BY AvailabilityType_Description ASC');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    {
      if ($row['AvailabilityType_ID'] == $TypeID)
        echo '<OPTION value="'.$row['AvailabilityType_ID'].'" SELECTED>'.$row['AvailabilityType_Description'].'</OPTION>';
      else
        echo '<OPTION value="'.$row['AvailabilityType_ID'].'">'.$row['AvailabilityType_Description'].'</OPTION>';
    }
    echo '</SELECT>';     
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a meeting type selection list.                                    //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildMeetingTypeSelector($TabIndex, $Name, $Class, $TypeID)                                     
  {
    $resultSet = ExecuteQuery('SELECT * FROM MeetingType ORDER BY MeetingType_Description ASC');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    {
      if ($row['MeetingType_ID'] == $TypeID)
        echo '<OPTION value="'.$row['MeetingType_ID'].'" SELECTED>'.$row['MeetingType_Description'].'</OPTION>';
      else
        echo '<OPTION value="'.$row['MeetingType_ID'].'">'.$row['MeetingType_Description'].'</OPTION>';
    }
    echo '</SELECT>';     
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds an availability type selection list.                              //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildAvailabilityReportTypeSelector($TabIndex, $Name, $Class)                                     
  {
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>
            <OPTION value="All" SELECTED>All</OPTION>
            <OPTION value="Banked Overtime Use">Banked Overtime Use</OPTION>
            <OPTION value="Event">Event</OPTION>
            <OPTION value="Leave">Leave</OPTION>';
            $resultSet = ExecuteQuery('SELECT * FROM AvailabilityType ORDER BY AvailabilityType_Description ASC');
            while ($row = MySQL_Fetch_Array($resultSet))
            {
              if ($row['AvailabilityType_ID'] == $TypeID)
                echo '<OPTION value="'.$row['AvailabilityType_ID'].'" SELECTED>'.$row['AvailabilityType_Description'].'</OPTION>';
              else
                echo '<OPTION value="'.$row['AvailabilityType_ID'].'">'.$row['AvailabilityType_Description'].'</OPTION>';
            }
    echo '</SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a passport type selection list.                                   //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildPassportTypeSelector($TabIndex, $Name, $Class, $Type)                                     
  {                        
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>
            <OPTION value="DE - Germany"';
    if ($Type == 'DE - Germany')
      echo ' SELECTED';
      echo '>DE - Germany</OPTION>
            <OPTION value="GB - United Kingdom"';
    if ($Type == 'GB - United Kingdom')
      echo ' SELECTED';
      echo '>GB - United Kingdom</OPTION>
            <OPTION value="IT - Italy"';
    if ($Type == 'IE - Ireland')
      echo ' SELECTED';
      echo '>IE - Ireland</OPTION>
            <OPTION value="ZA - South Africa"';
if ($Type == 'IT - Italy')
      echo ' SELECTED';
      echo '>IT - Italy</OPTION>
            <OPTION value="NA - Namibia"';
    if ($Type == 'NA - Namibia')
      echo ' SELECTED';
      echo '>NA - Namibia</OPTION>
            <OPTION value="NL - Netherlands"';
    if ($Type == 'NL - Netherlands')
      echo ' SELECTED';
      echo '>NL - Netherlands</OPTION>
            <OPTION value="ZA - South Africa"';
    if ($Type == 'ZA - South Africa')
      echo ' SELECTED';
      echo '>ZA - South Africa</OPTION>
          </SELECT>';     
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a passport type selection list.                                   //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildTravelDocumentCategorySelector($TabIndex, $Name, $Class, $Type)                                     
  {                        
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>
            <OPTION value="Application Form"';
    if ($Type == 'Application Form')
      echo ' SELECTED';
      echo '>Application Form</OPTION>
            <OPTION value="General Travel"';
    if ($Type == 'General Travel')
      echo ' SELECTED';
      echo '>General Travel</OPTION>
            <OPTION value="Passport Copy"';
    if ($Type == 'Passport Copy')
      echo ' SELECTED';
      echo '>Passport Copy</OPTION
            <OPTION value="VISA Requirements"';
    if ($Type == 'VISA Requirements')
      echo ' SELECTED';
      echo '>VISA Requirements</OPTION>
          </SELECT>';     
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a RFQ report type selection list.                                 //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildRFQReportTypeSelector($TabIndex, $Name, $Class)                                     
  {                        
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>
            <OPTION value="0">Closed</OPTION>
            <OPTION value="1">Open</OPTION>';
            if (($_SESSION['cAuth'] & 32) || ($_SESSION['cAuth'] & 64))
              echo '<OPTION value="2">Special</OPTION>';
    echo '</SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a payment type selection list.                                    //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildPaymentTypeSelector($TabIndex, $Name, $Class)                                     
  {                        
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>
            <OPTION value="1">Paid</OPTION>
            <OPTION value="2">Reviewed</OPTION>
            <OPTION value="0">Unpaid</OPTION>
          </SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a Yes/No selection list.                                          //
  //////////////////////////////////////////////////////////////////////////////
  function BuildYesNoSelector($TabIndex, $Name, $Class, $YesNo = '0')
  {
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="0"';
            if ($YesNo == '0')
              echo ' SELECTED';
            echo '>No</OPTION>
            <OPTION value="1"';
            if ($YesNo >= '1')
              echo ' SELECTED';
            echo '>Yes</OPTION>
          </SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds an approval type selection list.                                  //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildApprovalSelector($TabIndex, $Name, $Class, $TypeID)                                     
  {                        
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>
            <OPTION value="2"';
            if ($TypeID == '2')
              echo ' SELECTED';
            echo '>Electronic</OPTION>
            <OPTION value="1"';
            if ($TypeID == '1')
              echo ' SELECTED';
            echo '>Verbal</OPTION>
            <OPTION value="0"';
            if ($TypeID == '0')
              echo ' SELECTED';
            echo '>Written</OPTION>
          </SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a status selection list.                                          //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildCompletionStatusSelector($TabIndex, $Name, $Class)                                     
  {                        
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>
            <OPTION value="1">Complete</OPTION>
            <OPTION value="0">Incomplete</OPTION>
            <OPTION value="2">Cancelled</OPTION>
          </SELECT>';
  }
  
    //////////////////////////////////////////////////////////////////////////////
  // Builds a timesheet status selection list.                                          //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildTimesheetStatusSelector($TabIndex, $Name, $Class)                                     
  {                        
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>
            <OPTION value="1">Complete</OPTION>
            <OPTION value="0">Incomplete</OPTION>
          </SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds an expense report selection list.                                 //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildExpenseReportSelector($TabIndex, $Name, $Class)                                     
  {                        
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>
            <OPTION value="0">All Expenses</OPTION>
            <OPTION value="1">Deductions</OPTION>
            <OPTION value="8">Out-of-Town Allowances</OPTION>
            <OPTION value="7">Overtime</OPTION>
            <OPTION value="2">Reimbursements</OPTION>
            <OPTION value="3">Shift Allowances</OPTION>
            <OPTION value="4">Standby Allowances</OPTION>';
            if ($_SESSION['cAuth'] & 64)
              echo '<OPTION value="5">Summary</OPTION>';
      echo '<OPTION value="6">Travel Allowances</OPTION>
          </SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds an expense report selection list.                                 //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildCasualExpenseReportSelector($TabIndex, $Name, $Class)                                     
  {                        
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>
            <OPTION value="0">All Expenses</OPTION>
            <OPTION value="3">Allowances and Misc.</OPTION>
            <OPTION value="5">Summary</OPTION>
            <OPTION value="8">Work Hours</OPTION>
          </SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a meeting minutes category selection list.                        //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildMeetingMinutesCategorySelector($TabIndex, $Name, $Class, $Type)                                     
  {                        
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>
            <OPTION value="General"';
    if ($Type == 'General')
      echo ' SELECTED';
      echo '>General</OPTION>
            <OPTION value="GM"';
    if ($Type == 'GM')
      echo ' SELECTED';
      echo '>GM</OPTION>
            <OPTION value="VW"';
    if ($Type == 'VW')
      echo ' SELECTED';
      echo '>VW</OPTION>
          </SELECT>';     
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a meeting minutes category selection list.                        //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildDatabaseSelector($TabIndex, $Name, $Class, $Database)                                     
  {                        
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>
            <OPTION value="CobotsAdmin"';
    if ($Database == 'CobotsAdmin')
      echo ' SELECTED';
    echo '>CobotsAdmin</OPTION>
          <OPTION value="CobotsAdmin"';
    if ($Database == 'CobotsAdmin')
      echo ' SELECTED';
    echo '>S4DSA</OPTION>
        </SELECT>';     
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a meeting minutes selection list.                                 //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildMeetingMinutesSelector($TabIndex, $Name, $Class, $MinutesID, $IncludeCategory = true)                                     
  {                        
    $resultSet = ExecuteQuery('SELECT * FROM File WHERE File_Type = "4" ORDER BY File_Category, File_Updated ASC');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    { 
      if ($IncludeCategory)
        $minutes = $row['File_Category'].' - '.GetTextualDateFromDatabaseDate($row['File_DisplayName']);
      else
        $minutes = GetTextualDateFromDatabaseDate($row['File_DisplayName']);
        
      if ($row['File_ID'] == $MinutesID)
        echo '<OPTION value="'.$row['File_ID'].'" SELECTED>'.$minutes.'</OPTION>';
      else
        echo '<OPTION value="'.$row['File_ID'].'">'.$minutes.'</OPTION>';
    }         
    echo '</SELECT>';     
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds an meeting item selection list.                                   //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildMeetingItemSelector($TabIndex, $Name, $Class, $ItemID)                                     
  {
    $resultSet = ExecuteQuery('SELECT MeetingType.* FROM MeetingType ORDER BY MeetingType_Description ASC');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    {
      $resultSetTemp = ExecuteQuery('SELECT Meeting_ID, Meeting_Item_Title FROM Meeting WHERE Meeting_Type = "'.$row['MeetingType_ID'].'" AND Meeting_Date_Archived = "0000-00-00 00:00:00" ORDER BY Meeting_Item_Title ASC');
      while ($rowTemp = MySQL_Fetch_Array($resultSetTemp))
      {
        if ($rowTemp['Meeting_ID'] == $ItemID)
          echo '<OPTION value="'.$rowTemp['Meeting_ID'].'" SELECTED>'.$row['MeetingType_Description'].' - '.$rowTemp['Meeting_Item_Title'].'</OPTION>';
        else
          echo '<OPTION value="'.$rowTemp['Meeting_ID'].'">'.$row['MeetingType_Description'].' - '.$rowTemp['Meeting_Item_Title'].'</OPTION>';
      }
    }
    echo '</SELECT>';     
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a leave approval selection list.                                  //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildLeaveApproveSelector($TabIndex, $Name, $Class, $ApproveID)                                     
  {
    if ($_SESSION['cAuth'] & 64)
    {
      $resultSet = ExecuteQuery('SELECT Leave_ID, Leave_Start, Leave_Approved ,Staff_First_Name, Staff_Last_Name FROM `Leave`, Staff WHERE Leave_Employee = Staff_Code AND Leave_IsApproved = "0" ORDER BY Staff_First_Name, Staff_Last_Name');
    } else
    {
      $resultSet = ExecuteQuery('SELECT Leave_ID, Leave_Start, Leave_Approved  ,Staff_First_Name, Staff_Last_Name FROM `Leave`, Staff WHERE Leave_Approved = '.$ApproveID.' AND Leave_Employee = Staff_Code AND Leave_IsApproved = "0" ORDER BY Staff_First_Name, Staff_Last_Name');
    }
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    { 
      if($row['Leave_Approved'] === $ApproveID)  
        echo '<OPTION value="'.$row['Leave_ID'].'">'.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].' - '.GetTextualDateFromDatabaseDate($row['Leave_Start']).'</OPTION>';
      else
        echo '<OPTION value="'.$row['Leave_ID'].'">'.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].' - '.GetTextualDateFromDatabaseDate($row['Leave_Start']).'</OPTION>';
    }         
    echo '</SELECT>';     
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a banked overtime use approval selection list.                    //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildOvertimeApproveSelector($TabIndex, $Name, $Class)                                     
  {
    $resultSet = ExecuteQuery('SELECT OvertimeBank.*, Staff_First_Name, Staff_Last_Name FROM OvertimeBank, Staff WHERE OvertimeBank_Name = Staff_Code AND OvertimeBank_IsApproved = "0" ORDER BY Staff_First_Name, Staff_Last_Name');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    { 
      echo '<OPTION value="'.$row['OvertimeBank_ID'].'">'.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].' - '.GetTextualDateFromDatabaseDate($row['OvertimeBank_Start']).' to '.GetTextualDateFromDatabaseDate($row['OvertimeBank_End']).'</OPTION>';
    }         
    echo '</SELECT>';     
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a normal overtime approval selection list.                        //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildOvertimeApproveNormalSelector($TabIndex, $Name, $Class)                                     
  {
    $resultSet = ExecuteQuery('SELECT OvertimePreApp.*, Staff_First_Name, Staff_Last_Name FROM OvertimePreApp, Staff WHERE OvertimePreApp_Name = Staff_Code AND OvertimePreApp_IsApproved = "0"'.($_SESSION['cAuth'] & 64 ? "" : ' AND OvertimePreApp_Authorised = '.$_SESSION['cUID'].'').' ORDER BY Staff_First_Name, Staff_Last_Name');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    { 
      if($row['OvertimePreApp_Authorised'] === $_SESSION['cUID'])
         echo '<OPTION value="'.$row['OvertimePreApp_ID'].'">'.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].' - '.GetTextualDateFromDatabaseDate($row['OvertimePreApp_Start']).' to '.GetTextualDateFromDatabaseDate($row['OvertimePreApp_End']).'</OPTION>';
      
      else
         echo '<OPTION  value="'.$row['OvertimePreApp_ID'].'">'.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].' - '.GetTextualDateFromDatabaseDate($row['OvertimePreApp_Start']).' to '.GetTextualDateFromDatabaseDate($row['OvertimePreApp_End']).'</OPTION>';
       
      
    }         
    echo '</SELECT>';     
  }
  
  function BuildOvertimeApproveNormalSelectorMultiple($TabIndex, $Name, $Class)                                     
  {
    $resultSet = ExecuteQuery('SELECT Staff_Code ,concat(Staff_First_Name," ",Staff_Last_Name) as person , count(concat(Staff_First_Name," ",Staff_Last_Name)) as entries  FROM OvertimePreApp, Staff WHERE OvertimePreApp_Name = Staff_Code AND OvertimePreApp_IsApproved = "0"'.($_SESSION['cAuth'] & 64 ? "" : ' AND OvertimePreApp_Authorised = '.$_SESSION['cUID'].'').' GROUP BY person ORDER BY Staff_First_Name, Staff_Last_Name');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    { 
      if($row['OvertimePreApp_Authorised'] === $_SESSION['cUID'])
         echo '<OPTION value="'.$row['Staff_Code'].'">'.$row['person'].'</OPTION>';
      
      else
         echo '<OPTION  value="'.$row['Staff_Code'].'">'.$row['person'].'</OPTION>';
       
    }         
    echo '</SELECT>';     
  }
  
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a reminder selection list.                                        //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildReminderSelector($TabIndex, $Name, $Class, $IncludeSystem = false)                                     
  {
    if ($IncludeSystem)
      $system = "";
    else
      $system = ' AND Reminder_Type <> "5"';
  
    if ($_SESSION['cAuth'] == 255)
      $resultSet = ExecuteQuery('SELECT Reminder.*, ReminderType_Description FROM Reminder, ReminderType WHERE ReminderStatus  = 0 AND Reminder_Type = ReminderType_ID AND (Reminder_Trigger_Name = '.$_SESSION['cUID'].' OR Reminder_Updated_Name = '.$_SESSION['cUID'].' OR Reminder_Auth & '.$_SESSION['cAuth'].')'.$system.' ORDER BY ReminderType_Description, Reminder_System_Reference ASC');
    else
    {
      if (($_SESSION['cAuth'] & 32) || ($_SESSION['cAuth'] & 64))
        $resultSet = ExecuteQuery('SELECT Reminder.*, ReminderType_Description FROM Reminder, ReminderType WHERE ReminderStatus  = 0 AND Reminder_Type = ReminderType_ID AND (Reminder_Trigger_Name = '.$_SESSION['cUID'].' OR Reminder_Updated_Name = '.$_SESSION['cUID'].' OR Reminder_Auth & '.$_SESSION['cAuth'].') AND Reminder_Type <> "5" ORDER BY ReminderType_Description, Reminder_System_Reference ASC');
      else
        $resultSet = ExecuteQuery('SELECT Reminder.*, ReminderType_Description FROM Reminder, ReminderType WHERE ReminderStatus  = 0 AND Reminder_Type = ReminderType_ID AND (Reminder_Trigger_Name = '.$_SESSION['cUID'].' OR Reminder_Updated_Name = '.$_SESSION['cUID'].') AND Reminder_Type <> "5" ORDER BY ReminderType_Description, Reminder_System_Reference ASC');
    }
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    { 
      echo '<OPTION value="'.$row['Reminder_ID'].'">'.$row['ReminderType_Description'].' - '.GetTextualDateFromDatabaseDate($row['Reminder_Trigger_Date']).' - '.$row['Reminder_Description'].'</OPTION>';
    }         
    echo '</SELECT>';     
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a reminder type selection list.                                   //
  //////////////////////////////////////////////////////////////////////////////
  function BuildReminderTypeSelector($TabIndex, $Name, $Class, $TypeID, $IncludePersonal = false, $IncludeSystem = false)                                     
  {
    if ($IncludePersonal)
      $personal = "";
    else
      $personal = ' AND ReminderType_ID <> "1"';
  
    if ($IncludeSystem)
      $system = "";
    else
      $system = ' AND ReminderType_ID <> "5"';
  
    $resultSet = ExecuteQuery('SELECT * FROM ReminderType WHERE 1=1'.$personal.''.$system.' ORDER BY ReminderType_Description ASC');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    {
      if (($row['ReminderType_ID'] == '5') && ($_SESSION['cAuth'] & 255))
      {
        if ($TypeID == $row['ReminderType_ID'])
          echo '<OPTION value="'.$row['ReminderType_ID'].'" SELECTED>'.$row['ReminderType_Description'].'</OPTION>';
        else
          echo '<OPTION value="'.$row['ReminderType_ID'].'">'.$row['ReminderType_Description'].'</OPTION>';
      } else    
      if ($TypeID == $row['ReminderType_ID'])
        echo '<OPTION value="'.$row['ReminderType_ID'].'" SELECTED>'.$row['ReminderType_Description'].'</OPTION>';
      else
        echo '<OPTION value="'.$row['ReminderType_ID'].'">'.$row['ReminderType_Description'].'</OPTION>';
    }         
    echo '</SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a reminder period selection list.                                 //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildReminderPeriodSelector($TabIndex, $Name, $Class, $PeriodID)                                     
  {
    $resultSet = ExecuteQuery('SELECT * FROM ReminderPeriod ORDER BY ReminderPeriod_Description ASC');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    {   
      if ($PeriodID == $row['ReminderPeriod_ID'])
        echo '<OPTION value="'.$row['ReminderPeriod_ID'].'" SELECTED>'.$row['ReminderPeriod_Description'].'</OPTION>';
      else
        echo '<OPTION value="'.$row['ReminderPeriod_ID'].'">'.$row['ReminderPeriod_Description'].'</OPTION>';
    }         
    echo '</SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds an intranet news selection list.                                  //
  //////////////////////////////////////////////////////////////////////////////
  function BuildIntranetNewsSelector($TabIndex, $Name, $Class, $Page)
  {
    $xml = new DOMDocument();
    $xml->Load('XML/News.xml');
    
    $pagePath = new DOMXPath($xml);
    $entries = $pagePath->Query('/news/page[@link ="'.$Page.'"]/entry');
    
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    if ($entries->length > 0)
    {
      $count = 0;
      foreach ($entries as $entry)
      {
        echo '<OPTION value="'.$count.'">'.$entry->GetAttribute('date').' - '.$entry->GetAttribute('text').'</OPTION>';
        $count++;
      }
    }
    echo '</SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a database backup selection list.                                 //
  //////////////////////////////////////////////////////////////////////////////
  function BuildDatabaseBackupSelector($TabIndex, $Name, $Class, $Backup, $Database)
  {
    $backupDirectory = 'Files/Databases/';
    
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    if ($handle = OpenDir($backupDirectory))
    {
      while (false !== ($file = ReadDir($handle)))
      {
        $pathInfo = PathInfo($file);
        if (($pathInfo['extension'] == 'dmp') && (SubStr($file, 0, StrLen($Database)) == $Database))
          if ($Backup == $file)
            echo '<OPTION value="'.$file.'" SELECTED>'.$file.'</OPTION>';
          else
            echo '<OPTION value="'.$file.'">'.$file.'</OPTION>';
      }
      CloseDir($handle);
    }
    echo '</SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a gender selection list.                                          //
  //////////////////////////////////////////////////////////////////////////////
  function BuildGenderSelector($TabIndex, $Name, $Class, $GenderType)
  {
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>
            <OPTION value="Female"';
    if ($GenderType == 'Female')
      echo ' SELECTED';
    echo '>Female</OPTION>
          <OPTION value="Male"';
    if ($GenderType == 'Male')
      echo ' SELECTED';
    echo '>Male</OPTION>
        </SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a race selection list.                                            //
  //////////////////////////////////////////////////////////////////////////////
  function BuildRaceSelector($TabIndex, $Name, $Class, $RaceType)
  {
    $resultSet = ExecuteQuery('SELECT Race_Description FROM Race ORDER BY Race_Description ASC');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    {
      if ($RaceType == $row['Race_Description'])
        echo '<OPTION value="'.$row['Race_Description'].'" SELECTED>'.$row['Race_Description'].'</OPTION>';
      else
        echo '<OPTION value="'.$row['Race_Description'].'">'.$row['Race_Description'].'</OPTION>';
    }
    echo '</SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a feedback category selection list.                               //
  //////////////////////////////////////////////////////////////////////////////
  function BuildFeedbackSelector($TabIndex, $Name, $Class, $FeedbackType)
  {
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>
            <OPTION value="Bug Report"';
    if ($FeedbackType == 'Bug Report')
      echo ' SELECTED';
    echo '>Bug Report</OPTION>
          <OPTION value="Comment"';
    if ($FeedbackType == 'Comment')
      echo ' SELECTED';
    echo '>Comment</OPTION>
          <OPTION value="Query"';
    if ($FeedbackType == 'Query')
      echo ' SELECTED';
    echo '>Query</OPTION>
        </SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a callout selection list.                                         //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildCalloutSelector($TabIndex, $Name, $Class, $CalloutID, $Type = 'Open', $Any = false)                                                                               
  {
    if ($Any)
      $date = "";
    else
      $date = ' AND Callout_DateTime >= "'.Date('Y-m-d', MkTime(0, 0, 0, Date('m')-2, 0, Date('Y'))).'"';
    
    switch ($Type)
    {
      case 'All':
        $resultSet = ExecuteQuery('SELECT Callout_ID, Callout_DateTime, CalloutSystem_Description, Staff_First_Name, Staff_Last_Name FROM Callout, CalloutSystem, Staff WHERE Callout_Name = Staff_Code AND Callout_System = CalloutSystem_ID'.$date.' ORDER BY Callout_ID DESC LIMIT 250'); # ASW addred LIMIT 1000 140111; and change ASC to DESC
        break;
      case 'Closed':
        $resultSet = ExecuteQuery('SELECT Callout_ID, Callout_DateTime, CalloutSystem_Description, Staff_First_Name, Staff_Last_Name FROM Callout, CalloutSystem, Staff WHERE Callout_Name = Staff_Code AND Callout_System = CalloutSystem_ID AND Callout_Status = 1'.$date.' ORDER BY Callout_ID DESC LIMIT 250');# ASW addred LIMIT 1000 140111; and change ASC to DESC
        break;    
      case 'Open':
        $resultSet = ExecuteQuery('SELECT Callout_ID, Callout_DateTime, CalloutSystem_Description, Staff_First_Name, Staff_Last_Name FROM Callout, CalloutSystem, Staff WHERE Callout_Name = Staff_Code AND Callout_System = CalloutSystem_ID AND Callout_Status <> 1'.$date.' ORDER BY Callout_ID DESC LIMIT 250');# ASW addred LIMIT 1000 140111; and change ASC to DESC
        break;
      default:
        break;
    }
    
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    { 
      if ($row['Callout_ID'] == $CalloutID)
        echo '<OPTION value="'.$row['Callout_ID'].'" SELECTED>'.$row['Callout_ID'].' - '.GetTextualDateTimeFromDatabaseDateTime($row['Callout_DateTime']).': '.$row['Staff_First_Name'].' - '.$row['CalloutSystem_Description'].'</OPTION>';
      else
        echo '<OPTION value="'.$row['Callout_ID'].'">'.$row['Callout_ID'].' - '.GetTextualDateTimeFromDatabaseDateTime($row['Callout_DateTime']).': '.$row['Staff_First_Name'].' - '.$row['CalloutSystem_Description'].'</OPTION>';
    }         
    echo '</SELECT>';                    
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a callout selection list.                                         //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildCalloutReviewSelector($TabIndex, $Name, $Class, $Type)
  {
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>
          <OPTION value="2"';
    if ($Type == '2')
      echo ' SELECTED';
    echo '>Flagged</OPTION>
            <OPTION value="0"';
    if ($Type == '0')
      echo ' SELECTED';
    echo '>Pending</OPTION>
        </SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a callout report selection list.                                  //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildCalloutReportSelector($TabIndex, $Name, $Class, $Type)
  {
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>
          <OPTION value="0"';
    if ($Type == '0')
      echo ' SELECTED';
    echo '>Contract</OPTION>
          <OPTION value="1"';
    if ($Type == '1')
      echo ' SELECTED';
    echo '>Helpdesk</OPTION>
        </SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a callout status selection list.                                  //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildCalloutStatusSelector($TabIndex, $Name, $Class, $Type)
  {
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>
          <OPTION value="1"';
    if ($Type == '1')
      echo ' SELECTED';
    echo '>Closed</OPTION>
            <OPTION value="2"';
    if ($Type == '2')
      echo ' SELECTED';
    echo '>Follow Up</OPTION>
            <OPTION value="0"';
    if ($Type == '0')
      echo ' SELECTED';
    echo '>Pending</OPTION>
        </SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a callout work type selection list.                               //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildCalloutWorkSelector($TabIndex, $Name, $Class, $TypeID)                                     
  {
    $resultSet = ExecuteQuery('SELECT * FROM CalloutWorkType ORDER BY CalloutWorkType_Description ASC');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    {   
      if ($TypeID == $row['CalloutWorkType_ID'])
        echo '<OPTION value="'.$row['CalloutWorkType_ID'].'" SELECTED>'.$row['CalloutWorkType_Description'].'</OPTION>';
      else
        echo '<OPTION value="'.$row['CalloutWorkType_ID'].'">'.$row['CalloutWorkType_Description'].'</OPTION>';
    }         
    echo '</SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a staff by customer selection list.                               //
  //////////////////////////////////////////////////////////////////////////////
  function BuildStaffSelectorByCustomer($TabIndex, $Name, $Class, $StaffCode, $CustCode)
  {
    $resultSet = ExecuteQuery('SELECT Staff_Code, Staff_First_Name, Staff_Last_Name FROM StaffCustomerLink, Staff WHERE StaffCustomerLink_Name = Staff_Code AND StaffCustomerLink_Customer = '.$CustCode.' AND Staff_IsEmployee > 0 ORDER BY Staff_First_Name, Staff_Last_Name ASC');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    {
      if ($StaffCode == $row['Staff_Code'])
        echo '<OPTION value="'.$row['Staff_Code'].'" SELECTED>'.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].'</OPTION>';
      else
        echo '<OPTION value="'.$row['Staff_Code'].'">'.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].'</OPTION>';
    }
    echo '</SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a system by customer selection list.                              //
  //////////////////////////////////////////////////////////////////////////////
  function BuildSystemSelectorByCustomer($TabIndex, $Name, $Class, $SystemID, $CustCode)
  {
    $resultSet = ExecuteQuery('SELECT CalloutSystem_ID, CalloutSystem_ShortName FROM SystemCustomerLink, CalloutSystem WHERE SystemCustomerLink_Customer = '.$CustCode.' AND SystemCustomerLink_System = CalloutSystem_ID ORDER BY CalloutSystem_ShortName ASC');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    {
      if ($SystemID == $row['CalloutSystem_ID'])
        echo '<OPTION value="'.$row['CalloutSystem_ID'].'" SELECTED>'.$row['CalloutSystem_ShortName'].'</OPTION>';
      else
        echo '<OPTION value="'.$row['CalloutSystem_ID'].'">'.$row['CalloutSystem_ShortName'].'</OPTION>';
    }
    echo '</SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a sub system by customer selection list.                          //
  //////////////////////////////////////////////////////////////////////////////
  function BuildSubSystemSelectorByCustomer($TabIndex, $Name, $Class, $SubID, $CustCode)
  {
    $resultSet = ExecuteQuery('SELECT CalloutSubSystem.* FROM CalloutSubSystem WHERE CalloutSubSystem_Customer = '.$CustCode.' ORDER BY CalloutSubSystem_ShortName ASC');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    {
      if ($SubID == $row['CalloutSubSystem_ID'])
        echo '<OPTION value="'.$row['CalloutSubSystem_ID'].'" SELECTED>'.$row['CalloutSubSystem_ShortName'].'</OPTION>';
      else
        echo '<OPTION value="'.$row['CalloutSubSystem_ID'].'">'.$row['CalloutSubSystem_ShortName'].'</OPTION>';
    }
    echo '</SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a callout duration selection list.                                //
  //////////////////////////////////////////////////////////////////////////////
  function BuildCalloutTimeSelector($TabIndex, $Name, $Class, $TimeID)
  {
    $resultSet = ExecuteQuery('SELECT CalloutTime.* FROM CalloutTime ORDER BY CalloutTime_Minutes ASC');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    {
      if ($TimeID == $row['CalloutTime_ID'])
        echo '<OPTION value="'.$row['CalloutTime_ID'].'" SELECTED>'.$row['CalloutTime_Description'].'</OPTION>';
      else
        echo '<OPTION value="'.$row['CalloutTime_ID'].'">'.$row['CalloutTime_Description'].'</OPTION>';
    }
    echo '</SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a backup selection list.                                          //
  //////////////////////////////////////////////////////////////////////////////
  function BuildBackupSelector($TabIndex, $Name, $Class, $BackupID)
  {
    $resultSet = ExecuteQuery('SELECT Backup.*, Staff_First_Name, Staff_Last_Name, BackupSystem_Name FROM Backup, Staff, BackupSystem WHERE Backup_Name = Staff_Code AND Backup_System = BackupSystem_ID ORDER BY Backup_DateTime, Staff_First_Name, Staff_Last_Name ASC');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    {
      if ($BackupID == $row['Backup_ID'])
        echo '<OPTION value="'.$row['Backup_ID'].'" SELECTED>'.GetTextualDateFromDatabaseDate($row['Backup_DateTime']).' - '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].' - '.$row['BackupSystem_Name'].'</OPTION>';
      else
        echo '<OPTION value="'.$row['Backup_ID'].'">'.GetTextualDateFromDatabaseDate($row['Backup_DateTime']).' - '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].' - '.$row['BackupSystem_Name'].'</OPTION>';
    }
    echo '</SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a backup media selection list.                                    //
  //////////////////////////////////////////////////////////////////////////////
  function BuildBackupMediaSelector($TabIndex, $Name, $Class, $BackupMediaID)
  {
    $resultSet = ExecuteQuery('SELECT BackupMedia.* FROM BackupMedia ORDER BY BackupMedia_Description ASC');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    {
      if ($BackupMediaID == $row['BackupMedia_ID'])
        echo '<OPTION value="'.$row['BackupMedia_ID'].'" SELECTED>'.$row['BackupMedia_Description'].'</OPTION>';
      else
        echo '<OPTION value="'.$row['BackupMedia_ID'].'">'.$row['BackupMedia_Description'].'</OPTION>';
    }
    echo '</SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a backup system selection list.                                   //
  //////////////////////////////////////////////////////////////////////////////
  function BuildBackupSystemSelector($TabIndex, $Name, $Class, $SystemID)
  {
    $resultSet = ExecuteQuery('SELECT BackupSystem.* FROM BackupSystem ORDER BY BackupSystem_Name ASC');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    {
      if ($SystemID == $row['BackupSystem_ID'])
        echo '<OPTION value="'.$row['BackupSystem_ID'].'" SELECTED>'.$row['BackupSystem_Name'].'</OPTION>';
      else
        echo '<OPTION value="'.$row['BackupSystem_ID'].'">'.$row['BackupSystem_Name'].'</OPTION>';
    }
    echo '</SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a backup system selection list.                                   //
  //////////////////////////////////////////////////////////////////////////////
  function BuildBackupSystemSelectorByCustomer($TabIndex, $Name, $Class, $SystemID, $CustomerCode)
  {
    $resultSet = ExecuteQuery('SELECT BackupSystem.* FROM BackupSystem, BackupSystemCustomer WHERE BackupSystem_ID = BackupSystemCustomer_BackupSystem AND BackupSystemCustomer_Customer = '.$CustomerCode.' ORDER BY BackupSystem_Name ASC');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    {
      if ($SystemID == $row['BackupSystem_ID'])
        echo '<OPTION value="'.$row['BackupSystem_ID'].'" SELECTED>'.$row['BackupSystem_Name'].'</OPTION>';
      else
        echo '<OPTION value="'.$row['BackupSystem_ID'].'">'.$row['BackupSystem_Name'].'</OPTION>';
    }
    echo '</SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a backup status selection list.                                   //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildBackupStatusSelector($TabIndex, $Name, $Class, $Type)
  {
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>
                  <OPTION value="2"';
          if ($Type == '2')
            echo ' SELECTED';
          echo '>Backup NOK</OPTION>
          <OPTION value="1"';
          if ($Type == '1')
            echo ' SELECTED';
          echo '>Backup OK</OPTION>
                  <OPTION value="0"';
          if ($Type == '0')
            echo ' SELECTED';
          echo '>Backup & Restore OK</OPTION>
          </SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a link type selection list.                                       //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildLinkTypeSelector($TabIndex, $Name, $Class, $Type)
  {
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>
                  <OPTION value="Customer - Backup System"';
          if ($Type == 'Customer - Backup System')
            echo ' SELECTED';
          echo '>Customer - Backup System</OPTION>
                  <OPTION value="Customer - Staff"';
          if ($Type == 'Customer - Staff')
            echo ' SELECTED';
          echo '>Customer - Staff</OPTION>
                  <OPTION value="Customer - Supervisor"';
          if ($Type == 'Customer - Supervisor')
            echo ' SELECTED';
          echo '>Customer - Supervisor</OPTION>
                  <OPTION value="Customer - System"';
          if ($Type == 'Customer - System')
            echo ' SELECTED';
          echo '>Customer - System</OPTION>
                  <OPTION value="Customer / System - Staff"';
          if ($Type == 'Customer / System - Staff')
            echo ' SELECTED';
          echo '>Customer / System - Staff</OPTION>
          </SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a backup system customer link selection list.                     //
  //////////////////////////////////////////////////////////////////////////////
  function BuildBackupSystemCustomerLinkSelector($TabIndex, $Name, $Class, $LinkID)
  {
    $resultSet = ExecuteQuery('SELECT BackupSystemCustomer.*, BackupSystem_Name, Customer_Name FROM BackupSystemCustomer, BackupSystem, Customer WHERE BackupSystem_ID = BackupSystemCustomer_BackupSystem AND BackupSystemCustomer_Customer = Customer_Code ORDER BY Customer_Name, BackupSystem_Name ASC');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    {
      if ($LinkID == $row['BackupSystemCustomer_ID'])
        echo '<OPTION value="'.$row['BackupSystemCustomer_ID'].'" SELECTED>'.$row['Customer_Name'].' - '.$row['BackupSystem_Name'].'</OPTION>';
      else
        echo '<OPTION value="'.$row['BackupSystemCustomer_ID'].'">'.$row['Customer_Name'].' - '.$row['BackupSystem_Name'].'</OPTION>';
    }
    echo '</SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a staff customer link selection list.                             //
  //////////////////////////////////////////////////////////////////////////////
  function BuildStaffCustomerLinkSelector($TabIndex, $Name, $Class, $LinkID)
  {
    $resultSet = ExecuteQuery('SELECT StaffCustomerLink.*, Staff_First_Name, Staff_Last_Name, Customer_Name FROM StaffCustomerLink, Staff, Customer WHERE Staff_Code = StaffCustomerLink_Name AND StaffCustomerLink_Customer = Customer_Code ORDER BY Customer_Name, Staff_First_Name, Staff_Last_Name ASC');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    {
      if ($LinkID == $row['StaffCustomerLink_ID'])
        echo '<OPTION value="'.$row['StaffCustomerLink_ID'].'" SELECTED>'.$row['Customer_Name'].' - '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].'</OPTION>';
      else
        echo '<OPTION value="'.$row['StaffCustomerLink_ID'].'">'.$row['Customer_Name'].' - '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].'</OPTION>';
    }
    echo '</SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a staff customer supervisor link selection list.                  //
  //////////////////////////////////////////////////////////////////////////////
  function BuildSupervisorCustomerLinkSelector($TabIndex, $Name, $Class, $LinkID)
  {
    $resultSet = ExecuteQuery('SELECT StaffCustomerSupervisorLink.*, Staff_First_Name, Staff_Last_Name, Customer_Name FROM StaffCustomerSupervisorLink, Staff, Customer WHERE Staff_Code = StaffCustomerSupervisorLink_Name AND StaffCustomerSupervisorLink_Customer = Customer_Code ORDER BY Customer_Name, Staff_First_Name, Staff_Last_Name ASC');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    {
      if ($LinkID == $row['StaffCustomerSupervisorLink_ID'])
        echo '<OPTION value="'.$row['StaffCustomerSupervisorLink_ID'].'" SELECTED>'.$row['Customer_Name'].' - '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].'</OPTION>';
      else
        echo '<OPTION value="'.$row['StaffCustomerSupervisorLink_ID'].'">'.$row['Customer_Name'].' - '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].'</OPTION>';
    }
    echo '</SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a callout system customer link selection list.                    //
  //////////////////////////////////////////////////////////////////////////////
  function BuildCalloutSystemCustomerLinkSelector($TabIndex, $Name, $Class, $LinkID)
  {
    $resultSet = ExecuteQuery('SELECT SystemCustomerLink.*, CalloutSystem_ShortName, Customer_Name FROM SystemCustomerLink, CalloutSystem, Customer WHERE CalloutSystem_ID = SystemCustomerLink_System AND SystemCustomerLink_Customer = Customer_Code ORDER BY Customer_Name, CalloutSystem_ShortName ASC');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    {
      if ($LinkID == $row['SystemCustomerLink_ID'])
        echo '<OPTION value="'.$row['SystemCustomerLink_ID'].'" SELECTED>'.$row['Customer_Name'].' - '.$row['CalloutSystem_ShortName'].'</OPTION>';
      else
        echo '<OPTION value="'.$row['SystemCustomerLink_ID'].'">'.$row['Customer_Name'].' - '.$row['CalloutSystem_ShortName'].'</OPTION>';
    }
    echo '</SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a staff customer system link selection list.                      //
  //////////////////////////////////////////////////////////////////////////////
  function BuildStaffCustomerSystemLinkSelector($TabIndex, $Name, $Class, $LinkID)
  {
    $resultSet = ExecuteQuery('SELECT StaffCustomerSystemLink.*, Staff_First_Name, Staff_Last_Name, Customer_Name, CalloutSystem_ShortName FROM StaffCustomerSystemLink, Staff, Customer, CalloutSystem WHERE CalloutSystem_ID = StaffCustomerSystemLink_System AND Staff_Code = StaffCustomerSystemLink_Name AND StaffCustomerSystemLink_Customer = Customer_Code ORDER BY Customer_Name, Staff_First_Name, Staff_Last_Name, CalloutSystem_ShortName ASC');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    {
      if ($LinkID == $row['StaffCustomerSystemLink_ID'])
        echo '<OPTION value="'.$row['StaffCustomerSystemLink_ID'].'" SELECTED>'.$row['Customer_Name'].' - '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].' - '.$row['CalloutSystem_ShortName'].'</OPTION>';
      else
        echo '<OPTION value="'.$row['StaffCustomerSystemLink_ID'].'">'.$row['Customer_Name'].' - '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].' - '.$row['CalloutSystem_ShortName'].'</OPTION>';
    }
    echo '</SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds an item type selection list.                                      //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildItemTypeSelector($TabIndex, $Name, $Class, $Type)
  {
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>
            <OPTION value="Backup System"';
          if ($Type == 'Backup System')
            echo ' SELECTED';
          echo '>Backup System</OPTION>
            <OPTION value="Callout System"';
          if ($Type == 'Callout System')
            echo ' SELECTED';
          echo '>Callout System</OPTION>
            <OPTION value="Callout Sub System"';
          if ($Type == 'Callout Sub System')
            echo ' SELECTED';
          echo '>Callout Sub System</OPTION>
            <OPTION value="Location"';
          if ($Type == 'Location')
            echo ' SELECTED';
          echo '>Location</OPTION>
            <OPTION value="Location Position"';
          if ($Type == 'Location Position')
            echo ' SELECTED';
          echo '>Location Position</OPTION>
          </SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds an order discrepency selection list.                              //
  //////////////////////////////////////////////////////////////////////////////
  function BuildOrderDiscrepencyTypeSelector($TabIndex, $Name, $Class, $TypeID)
  {
    $resultSet = ExecuteQuery('SELECT OrderDiscrepencyType.* FROM OrderDiscrepencyType ORDER BY OrderDiscrepencyType_Description ASC');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    {
      if ($TypeID == $row['OrderDiscrepencyType_ID'])
        echo '<OPTION value="'.$row['OrderDiscrepencyType_ID'].'" SELECTED>'.$row['OrderDiscrepencyType_Description'].'</OPTION>';
      else
        echo '<OPTION value="'.$row['OrderDiscrepencyType_ID'].'">'.$row['OrderDiscrepencyType_Description'].'</OPTION>';
    }
    echo '</SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a location selection list.                                        //
  //////////////////////////////////////////////////////////////////////////////
  function BuildLocationSelector($TabIndex, $Name, $Class, $LocationID)
  {
    $resultSet = ExecuteQuery('SELECT Location.* FROM Location ORDER BY Location ASC', 'S4DSA');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    {
      if ($LocationID == $row['Location_ID'])
        echo '<OPTION value="'.$row['Location_ID'].'" SELECTED>'.$row['Location'].'</OPTION>';
      else
        echo '<OPTION value="'.$row['Location_ID'].'">'.$row['Location'].'</OPTION>';
    }
    echo '</SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a location position selection list.                               //
  //////////////////////////////////////////////////////////////////////////////
  function BuildLocationPositionSelector($TabIndex, $Name, $Class, $Position)
  {
    $resultSet = ExecuteQuery('SELECT * FROM Location_Exact ORDER BY Location_Exact ASC', 'S4DSA');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    {
      if ($Position == $row['Location_Exact'])
        echo '<OPTION value="'.$row['Location_Exact'].'" SELECTED>'.$row['Location_Exact'].'</OPTION>';
      else
        echo '<OPTION value="'.$row['Location_Exact'].'">'.$row['Location_Exact'].'</OPTION>';
    }
    echo '</SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a part number selection list.                                     //
  //////////////////////////////////////////////////////////////////////////////
  function BuildPartNumberSelector($TabIndex, $Name, $Class, $PartNumberID)
  {
    $resultSet = ExecuteQuery('SELECT DISTINCT Part_Number.Part_Number FROM Part_Number ORDER BY Part_Number ASC', 'S4DSA');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    {
      if ($PartNumberID == $row['Part_Number'])
        echo '<OPTION value="'.$row['Part_Number'].'" SELECTED>'.$row['Part_Number'].'</OPTION>';
      else
        echo '<OPTION value="'.$row['Part_Number'].'">'.$row['Part_Number'].'</OPTION>';
    }
    echo '</SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a part status selection list.                                     //
  //////////////////////////////////////////////////////////////////////////////
  function BuildPartStatusSelector($TabIndex, $Name, $Class, $StatusID)
  {
    $resultSet = ExecuteQuery('SELECT * FROM Status ORDER BY Status_Description ASC', 'S4DSA');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    {
      if ($StatusID == $row['Status_State'])
        echo '<OPTION value="'.$row['Status_State'].'" SELECTED>'.$row['Status_Description'].'</OPTION>';
      else
        echo '<OPTION value="'.$row['Status_State'].'">'.$row['Status_Description'].'</OPTION>';
    }
    echo '</SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a stock report type selection list.                               //
  //////////////////////////////////////////////////////////////////////////////
  function BuildStockReportTypeSelector($TabIndex, $Name, $Class, $Type = "")
  {
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>
                  <OPTION value="Low Stock"';
          if ($Type == 'Low Stock')
            echo ' SELECTED';
          echo '>Low Stock</OPTION>
          </SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a serial number selection list.                                   //
  //////////////////////////////////////////////////////////////////////////////
  function BuildSerialNumberSelector($TabIndex, $Name, $Class, $SerialNumberID)
  {
    $resultSet = ExecuteQuery('SELECT Serial_Number.* FROM Serial_Number ORDER BY Serial_Number, Serial_Part ASC', 'S4DSA');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    {
      $resultSetTemp = ExecuteQuery('SELECT * FROM ModuleLink WHERE ModuleLink_Part = '.$row['ID'].' AND ModuleLink_Type = "0" LIMIT 1', 'S4DSA');
      if (MySQL_Num_Rows($resultSetTemp) == '0')
        $child = "";
      else
        $child = '*';
      
      if ($SerialNumberID == $row['ID'])
        echo '<OPTION value="'.$row['ID'].'" SELECTED>'.$child.' '.$row['Serial_Number'].' - '.$row['Serial_Part'].'</OPTION>';
      else
        echo '<OPTION value="'.$row['ID'].'">'.$child.' '.$row['Serial_Number'].' - '.$row['Serial_Part'].'</OPTION>';
    }
    echo '</SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a module selection list.                                          //
  //////////////////////////////////////////////////////////////////////////////
  function BuildModuleSelector($TabIndex, $Name, $Class, $ModuleID, $ExcludeModule = "", $TopLevel = false)
  {
    $resultSet = ExecuteQuery('SELECT Module.* FROM Module WHERE NOT (Module_ID = "'.$ExcludeModule.'") ORDER BY Module_Description ASC', 'S4DSA');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    {
      $resultSetTemp = ExecuteQuery('SELECT * FROM ModuleLink WHERE ModuleLink_Part = '.$row['Module_ID'].' AND ModuleLink_Type = "1" LIMIT 1', 'S4DSA');
      if (MySQL_Num_Rows($resultSetTemp) > 0)
        $child = '*';
      else
        $child = "";
      
      if ($TopLevel)
      {
        $resultSetTemp = ExecuteQuery('SELECT * FROM ModuleLink WHERE ModuleLink_Part = '.$row['Module_ID'].' AND ModuleLink_Type = "1" AND NOT (ModuleLink_Module = "'.$ExcludeModule.'") LIMIT 1', 'S4DSA');
        if (MySQL_Num_Rows($resultSetTemp) == '0')
        {
          if ($ModuleID == $row['Module_ID'])
            echo '<OPTION value="'.$row['Module_ID'].'" SELECTED>'.$child.' '.$row['Module_Name'].'</OPTION>';
          else
            echo '<OPTION value="'.$row['Module_ID'].'">'.$child.' '.$row['Module_Name'].'</OPTION>';
        }
      } else
      {
        if ($ModuleID == $row['Module_ID'])
          echo '<OPTION value="'.$row['Module_ID'].'" SELECTED>'.$child.' '.$row['Module_Name'].'</OPTION>';
        else
          echo '<OPTION value="'.$row['Module_ID'].'">'.$child.' '.$row['Module_Name'].'</OPTION>';
      }
    }
    echo '</SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds an extranet account selection list.                               //
  //////////////////////////////////////////////////////////////////////////////
  function BuildExtranetAccountSelector($TabIndex, $Name, $Class, $UserID, $All = false)
  {
    if ($All)
      $resultSet = ExecuteQuery('SELECT * FROM ExtranetUser ORDER BY ExtranetUser_Name ASC');
    else
      $resultSet = ExecuteQuery('SELECT * FROM ExtranetUser WHERE ExtranetUser_Enabled = "1" ORDER BY ExtranetUser_Name ASC');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    {
      if ($UserID == $row['ExtranetUser_ID'])
        echo '<OPTION value="'.$row['ExtranetUser_ID'].'" SELECTED>'.$row['ExtranetUser_Name'].'</OPTION>';
      else
        echo '<OPTION value="'.$row['ExtranetUser_ID'].'">'.$row['ExtranetUser_Name'].'</OPTION>';
    }
    echo '</SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a casual hours selection list.                                    //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildCasualHoursSelector($TabIndex, $Name, $Class, $HoursID)                                     
  {
    $resultSet = ExecuteQuery('SELECT CasualExpense.*, Casual_First_Name, Casual_Last_Name FROM CasualExpense, Casual WHERE CasualExpense_Name = Casual_Code AND CasualExpense_Paid <> "1" ORDER BY Casual_First_Name, Casual_Last_Name, CasualExpense_Start, casualExpense_End ASC');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    {
      if ($row['CasualExpense_ID'] == $HoursID)
        echo '<OPTION value="'.$row['CasualExpense_ID'].'" SELECTED>'.$row['Casual_First_Name'].' '.$row['Casual_Last_Name'].' - '.GetTextualDateTimeFromDatabaseDateTime($row['CasualExpense_Start']).' to '.GetTextualDateTimeFromDatabaseDateTime($row['casualExpense_End']).' - '.$row['CasualExpense_Hours'].' Hours</OPTION>';
      else
        echo '<OPTION value="'.$row['CasualExpense_ID'].'">'.$row['Casual_First_Name'].' '.$row['Casual_Last_Name'].' - '.GetTextualDateTimeFromDatabaseDateTime($row['CasualExpense_Start']).' to '.GetTextualDateTimeFromDatabaseDateTime($row['casualExpense_End']).' - '.$row['CasualExpense_Hours'].' Hours</OPTION>';
    }
    echo '</SELECT>';     
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a phone extension selection list.                                 //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildExtensionSelector($TabIndex, $Name, $Class, $ExtensionID)                                     
  {
    $resultSet = ExecuteQuery('SELECT * FROM Phone WHERE Phone_Type = 0 ORDER BY Phone_Description, Phone_Keys ASC');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    {
      if ($row['Phone_ID'] == $ExtensionID)
        echo '<OPTION value="'.$row['Phone_ID'].'" SELECTED>'.$row['Phone_Description'].' - '.SubStr($row['Phone_Keys'], 0, 5).'</OPTION>';
      else
        echo '<OPTION value="'.$row['Phone_ID'].'">'.$row['Phone_Description'].' - '.SubStr($row['Phone_Keys'], 0, 5).'</OPTION>';
    }
    echo '</SELECT>';     
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a project type selection list.                                    //
  //////////////////////////////////////////////////////////////////////////////
  function BuildProjectTypeSelector($TabIndex, $Name, $Class, $TypeID)
  {
    $resultSet = ExecuteQuery('SELECT ProjectType.* FROM ProjectType ORDER BY ProjectType_Description ASC');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    {
      if ($row['ProjectType_ID'] == $TypeID)
        echo '<OPTION value="'.$row['ProjectType_ID'].'" SELECTED>'.$row['ProjectType_Description'].'</OPTION>';
      else
        echo '<OPTION value="'.$row['ProjectType_ID'].'">'.$row['ProjectType_Description'].'</OPTION>';
    }
    echo '</SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds an event selection list.                                          //
  //////////////////////////////////////////////////////////////////////////////
  function BuildEventSelector($TabIndex, $Name, $Class, $EventID)
  {
    $resultSet = ExecuteQuery('SELECT Event.* FROM Event ORDER BY Event_DateTime_Start, Event_Description ASC');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    {
      $description = StrLen($row['Event_Description']) > 30 ? SubStr($row['Event_Description'], 0, 30) : $row['Event_Description'];
      if ($row['Event_ID'] == $EventID)
        echo '<OPTION value="'.$row['Event_ID'].'" SELECTED>'.GetTextualDateFromDatabaseDate($row['Event_DateTime_Start']).' to '.GetTextualDateFromDatabaseDate($row['Event_DateTime_End']).' - '.$description.'</OPTION>';
      else
        echo '<OPTION value="'.$row['Event_ID'].'">'.GetTextualDateFromDatabaseDate($row['Event_DateTime_Start']).' to '.GetTextualDateFromDatabaseDate($row['Event_DateTime_End']).' - '.$description.'</OPTION>';
    }
    echo '</SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a qualification selection list.                                   //
  //////////////////////////////////////////////////////////////////////////////
  function BuildQualificationSelector($TabIndex, $Name, $Class, $QualType)
  {
    $resultSet = ExecuteQuery('SELECT QualificationType.* FROM QualificationType ORDER BY QualificationType_ID ASC');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    {
      if ($row['QualificationType_ID'] == $QualType)
        echo '<OPTION value="'.$row['QualificationType_ID'].'" SELECTED>'.$row['QualificationType_Description'].'</OPTION>';
      else
        echo '<OPTION value="'.$row['QualificationType_ID'].'">'.$row['QualificationType_Description'].'</OPTION>';
    }
    echo '</SELECT>';
  }
  
  function GetPosition($Position)
  {
    switch ($Position)
    {
      case '0':
        return 'Closed';
        break;
      case '1':
        return 'Open';
        break;
      case '2':
        return 'Taken';
        break;
      default:
        return 'Unknown';
        break;
    }
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds an application position selection list.                           //
  //////////////////////////////////////////////////////////////////////////////
  function BuildApplicationPositionSelector($TabIndex, $Name, $Class, $PositionID)
  {
    $resultSet = ExecuteQuery('SELECT ApplicationPosition.* FROM ApplicationPosition ORDER BY ApplicationPosition_Status, ApplicationPosition_Description');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    {
      if ($row['ApplicationPosition_ID'] == $PositionID)
        echo '<OPTION value="'.$row['ApplicationPosition_ID'].'" SELECTED>'.$row['ApplicationPosition_Description'].' ('.GetTextualDateFromDatabaseDate($row['ApplicationPosition_DateTime_Updated']).') - '.GetPosition($row['ApplicationPosition_Status']).'</OPTION>';
      else
        echo '<OPTION value="'.$row['ApplicationPosition_ID'].'">'.$row['ApplicationPosition_Description'].' ('.GetTextualDateFromDatabaseDate($row['ApplicationPosition_DateTime_Updated']).') - '.GetPosition($row['ApplicationPosition_Status']).'</OPTION>';
    }
    echo '</SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds an application position status selection list.                    //
  //////////////////////////////////////////////////////////////////////////////
  function BuildApplicationPositionStatusSelector($TabIndex, $Name, $Class, $Status)
  {
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>
            <OPTION value="0"';
      if ($Status == '0')
        echo ' SELECTED';
      echo '>Closed</OPTION>
            <OPTION value="1"';
      if ($Status == '1')
        echo ' SELECTED';
      echo '>Open</OPTION>
            <OPTION value="2"';
      if ($Status == '2')
        echo ' SELECTED';
      echo '>Taken</OPTION>
        </SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds an application selection list.                                    //
  //////////////////////////////////////////////////////////////////////////////
  function BuildApplicationSelector($TabIndex, $Name, $Class, $ApplicationID)
  {
    $resultSet = ExecuteQuery('SELECT Application.* FROM Application ORDER BY Application_Reference ASC');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    {
      if ($row['Application_ID'] == $ApplicationID)
        echo '<OPTION value="'.$row['Application_ID'].'" SELECTED>'.$row['Application_Reference'].'</OPTION>';
      else
        echo '<OPTION value="'.$row['Application_ID'].'">'.$row['Application_Reference'].'</OPTION>';
    }
    echo '</SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds an application status selection list.                             //
  //////////////////////////////////////////////////////////////////////////////
  function BuildApplicationStatusSelector($TabIndex, $Name, $Class, $Status)
  {
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>
            <OPTION value="0"';
      if ($Status == '0')
        echo ' SELECTED';
      echo '>Pending</OPTION>
            <OPTION value="2"';
      if ($Status == '2')
        echo ' SELECTED';
      echo '>Successful</OPTION>
            <OPTION value="1"';
      if ($Status == '1')
        echo ' SELECTED';
      echo '>Unsuccessful</OPTION>
        </SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a size selection list.                                            //
  //////////////////////////////////////////////////////////////////////////////
  function BuildSizeSelector($TabIndex, $Name, $Class, $Status)
  {
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="S"';
      if ($Status == 'S')
        echo ' SELECTED';
      echo '>S</OPTION>
            <OPTION value="M"';
      if ($Status == 'M')
        echo ' SELECTED';
      echo '>M</OPTION>
            <OPTION value="L"';
      if ($Status == 'L')
        echo ' SELECTED';
      echo '>L</OPTION>
            <OPTION value="XL"';
      if ($Status == 'XL')
        echo ' SELECTED';
      echo '>XL</OPTION>
            <OPTION value="XXL"';
      if ($Status == 'XXL')
        echo ' SELECTED';
      echo '>XXL</OPTION>
            <OPTION value="XXXL"';
      if ($Status == 'XXXL')
        echo ' SELECTED';
      echo '>XXXL</OPTION>
        </SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a marketing contact selection list.                               //
  //////////////////////////////////////////////////////////////////////////////
  function BuildMarketingContactSelector($TabIndex, $Name, $Class, $ContactID)
  {
    $resultSet = ExecuteQuery('SELECT MarketingContactPerson.*, MarketingCompany_Name FROM MarketingContactPerson, MarketingCompany WHERE MarketingContactPerson_Company = MarketingCompany_ID ORDER BY MarketingCompany_Name, MarketingContactPerson_Name ASC');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    {
      if ($row['MarketingContactPerson_ID'] == $ContactID)
        echo '<OPTION value="'.$row['MarketingContactPerson_ID'].'" SELECTED>'.$row['MarketingCompany_Name'].' - '.$row['MarketingContactPerson_Name'].'</OPTION>';
      else
        echo '<OPTION value="'.$row['MarketingContactPerson_ID'].'">'.$row['MarketingCompany_Name'].' - '.$row['MarketingContactPerson_Name'].'</OPTION>';
    }
    echo '</SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a marketing contact department selection list.                    //
  //////////////////////////////////////////////////////////////////////////////
  function BuildMarketingContactDepartmentSelector($TabIndex, $Name, $Class, $DepartmentID)
  {
    $resultSet = ExecuteQuery('SELECT MarketingContactDepartment.* FROM MarketingContactDepartment ORDER BY MarketingContactDepartment_Name ASC');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    {
      if ($row['MarketingContactDepartment_Name'] == $DepartmentID)
        echo '<OPTION value="'.$row['MarketingContactDepartment_Name'].'" SELECTED>'.$row['MarketingContactDepartment_Name'].'</OPTION>';
      else
        echo '<OPTION value="'.$row['MarketingContactDepartment_Name'].'">'.$row['MarketingContactDepartment_Name'].'</OPTION>';
    }
    echo '</SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a marketing contact selection list.                               //
  //////////////////////////////////////////////////////////////////////////////
  function BuildMarketingCompanySelector($TabIndex, $Name, $Class, $CompanyID)
  {
    $resultSet = ExecuteQuery('SELECT MarketingCompany.* FROM MarketingCompany ORDER BY MarketingCompany_Name ASC');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    {
      if ($row['MarketingCompany_ID'] == $CompanyID)
        echo '<OPTION value="'.$row['MarketingCompany_ID'].'" SELECTED>'.$row['MarketingCompany_Name'].'</OPTION>';
      else
        echo '<OPTION value="'.$row['MarketingCompany_ID'].'">'.$row['MarketingCompany_Name'].'</OPTION>';
    }
    echo '</SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a marketing product selection list.                               //
  //////////////////////////////////////////////////////////////////////////////
  function BuildMarketingProductSelector($TabIndex, $Name, $Class, $ProductID, $All = true)
  {
    if ($All)
      $resultSet = ExecuteQuery('SELECT MarketingProduct.* FROM MarketingProduct ORDER BY MarketingProduct_Name ASC');
    else
      $resultSet = ExecuteQuery('SELECT MarketingProduct.* FROM MarketingProduct WHERE MarketingProduct_Status = "1" ORDER BY MarketingProduct_Name ASC');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    {
      if ($row['MarketingProduct_ID'] == $ProductID)
        echo '<OPTION value="'.$row['MarketingProduct_ID'].'" SELECTED>'.$row['MarketingProduct_Name'].'</OPTION>';
      else
        echo '<OPTION value="'.$row['MarketingProduct_ID'].'">'.$row['MarketingProduct_Name'].'</OPTION>';
    }
    echo '</SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a marketing correspondence selection list.                        //
  //////////////////////////////////////////////////////////////////////////////
  function BuildMarketingCorrespondenceSelector($TabIndex, $Name, $Class, $CorrespondenceID, $FollowUp = false)
  {
    if ($FollowUp)
    {
      $resultSet = ExecuteQuery('SELECT MarketingCorrespondence.*, MarketingContactPerson_Name, MarketingCompany_Name FROM MarketingCorrespondence, MarketingContactPerson, MarketingCompany WHERE MarketingCorrespondence_FollowedUp = "0" AND MarketingCorrespondence_Name = MarketingContactPerson_ID AND MarketingCorrespondence_Company = MarketingCompany_ID ORDER BY MarketingCompany_Name, MarketingContactPerson_Name, MarketingCorrespondence_Topic ASC');
    } else
    {
      $date = ' AND MarketingCorrespondence_DateTime >= "'.Date('Y-m-d', MkTime(0, 0, 0, Date('m')-2, 0, Date('Y'))).' 23:59:59"';
      $resultSet = ExecuteQuery('SELECT MarketingCorrespondence.*, MarketingContactPerson_Name, MarketingCompany_Name FROM MarketingCorrespondence, MarketingContactPerson, MarketingCompany WHERE MarketingCorrespondence_Name = MarketingContactPerson_ID AND MarketingCorrespondence_Company = MarketingCompany_ID'.$date.' ORDER BY MarketingCompany_Name, MarketingContactPerson_Name, MarketingCorrespondence_Topic ASC');
    }
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    {
      if ($row['MarketingCorrespondence_ID'] == $CorrespondenceID)
        echo '<OPTION value="'.$row['MarketingCorrespondence_ID'].'" SELECTED>'.$row['MarketingCompany_Name'].' - '.$row['MarketingContactPerson_Name'].' - '.SubStr($row['MarketingCorrespondence_Topic'], 0, 30).'...</OPTION>';
      else
        echo '<OPTION value="'.$row['MarketingCorrespondence_ID'].'">'.$row['MarketingCompany_Name'].' - '.$row['MarketingContactPerson_Name'].' - '.SubStr($row['MarketingCorrespondence_Topic'], 0, 30).'...</OPTION>';
    }
    echo '</SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a marketing correspondence selection list.                        //
  //////////////////////////////////////////////////////////////////////////////
  function BuildMarketingCorrespondenceLeadSelector($TabIndex, $Name, $Class, $LeadID)
  {
    $resultSet = ExecuteQuery('SELECT MarketingCorrespondenceLead.* FROM MarketingCorrespondenceLead ORDER BY MarketingCorrespondenceLead_Description ASC');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    {
      if ($row['MarketingCorrespondenceLead_ID'] == $LeadID)
        echo '<OPTION value="'.$row['MarketingCorrespondenceLead_ID'].'" SELECTED>'.$row['MarketingCorrespondenceLead_Description'].'</OPTION>';
      else
        echo '<OPTION value="'.$row['MarketingCorrespondenceLead_ID'].'">'.$row['MarketingCorrespondenceLead_Description'].'</OPTION>';
    }
    echo '</SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a communication type selection list.                              //
  //////////////////////////////////////////////////////////////////////////////
  function BuildCommTypeSelector($TabIndex, $Name, $Class, $CommID)
  {
    $resultSet = ExecuteQuery('SELECT CommsType.* FROM CommsType ORDER BY CommsType_Description ASC');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    {
      if ($row['CommsType_ID'] == $CommID)
        echo '<OPTION value="'.$row['CommsType_ID'].'" SELECTED>'.$row['CommsType_Description'].'</OPTION>';
      else
        echo '<OPTION value="'.$row['CommsType_ID'].'">'.$row['CommsType_Description'].'</OPTION>';
    }
    echo '</SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a marketing correspondence status selection list.                 //
  //////////////////////////////////////////////////////////////////////////////
  function BuildMarketingCorrespondenceStatusSelector($TabIndex, $Name, $Class, $Status)
  {
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>
            <OPTION value="1"';
      if ($Status == '1')
        echo ' SELECTED';
      echo '>Followed Up</OPTION>
            <OPTION value="2"';
      if ($Status == '2')
        echo ' SELECTED';
      echo '>No Follow Up</OPTION>
            <OPTION value="0"';
      if ($Status == '0')
        echo ' SELECTED';
      echo '>Pending</OPTION>
        </SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a training group selection list.                                  //
  //////////////////////////////////////////////////////////////////////////////
  function BuildGroupSelector($TabIndex, $Name, $Class, $GroupID)
  {
    $resultSet = ExecuteQuery('SELECT TrainingGroup.* FROM TrainingGroup ORDER BY TrainingGroup_Name ASC');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    {
      if ($row['TrainingGroup_ID'] == $GroupID)
        echo '<OPTION value="'.$row['TrainingGroup_ID'].'" SELECTED>'.$row['TrainingGroup_Name'].'</OPTION>';
      else
        echo '<OPTION value="'.$row['TrainingGroup_ID'].'">'.$row['TrainingGroup_Name'].'</OPTION>';
    }
    echo '</SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a training staff selection list.                                  //
  //////////////////////////////////////////////////////////////////////////////
  function BuildGroupStaffSelector($TabIndex, $Name, $Class, $StaffID)
  {
    $resultSet = ExecuteQuery('SELECT DISTINCT Staff_First_Name, Staff_Last_Name, Staff_Code FROM TrainingGroupStaff, Staff WHERE TrainingGroupStaff_Staff = Staff_Code AND Staff_IsEmployee > 0 ORDER BY Staff_First_Name, Staff_Last_Name ASC');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    {
      if ($row['Staff_Code'] == $StaffID)
        echo '<OPTION value="'.$row['Staff_Code'].'" SELECTED>'.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].'</OPTION>';
      else
        echo '<OPTION value="'.$row['Staff_Code'].'">'.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].'</OPTION>';
    }
    echo '</SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a training group skill selection list.                            //
  //////////////////////////////////////////////////////////////////////////////
  function BuildGroupSkillSelector($TabIndex, $Name, $Class, $StaffID, $SkillID)
  {
    $resultSet = ExecuteQuery('SELECT TrainingGroupType_Type, TrainingType_Name FROM TrainingGroupType INNER JOIN TrainingType ON TrainingGroupType_Type = TrainingType_ID INNER JOIN TrainingGroupStaff ON TrainingGroupType_Group = TrainingGroupStaff_Group WHERE TrainingGroupStaff_Staff = "'.$StaffID.'" ORDER BY TrainingType_Name ASC');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    {
      if ($row['TrainingGroupType_Type'] == $SkillID)
        echo '<OPTION value="'.$row['TrainingGroupType_Type'].'" SELECTED>'.$row['TrainingType_Name'].'</OPTION>';
      else
        echo '<OPTION value="'.$row['TrainingGroupType_Type'].'">'.$row['TrainingType_Name'].'</OPTION>';
    }
    echo '</SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a skill selection list.                                           //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildGroupList($TabIndex, $Name, $Class, $Groups, $List = 'All')                                                                               
  {
    $resultSet = ExecuteQuery('SELECT TrainingGroup.* FROM TrainingGroup ORDER BY TrainingGroup_Name ASC');
    echo '<SELECT MULTIPLE size="10" tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'" SORTED>';
    while ($row = MySQL_Fetch_Array($resultSet))
    {
      switch ($List)
      {
        case 'All':
          echo '<OPTION value="'.$row['TrainingGroup_ID'].'">'.$row['TrainingGroup_Name'].'</OPTION>';
          break;
        case 'Exclude':
          if (!In_Array($row['TrainingGroup_ID'], $Groups))
            echo '<OPTION value="'.$row['TrainingGroup_ID'].'">'.$row['TrainingGroup_Name'].'</OPTION>';
          break;
        case 'Include':
          if (In_Array($row['TrainingGroup_ID'], $Groups))
            echo '<OPTION value="'.$row['TrainingGroup_ID'].'">'.$row['TrainingGroup_Name'].'</OPTION>';
          break;
        default:
          break;
      }
    }         
    echo '</SELECT>';                    
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a training skill selection list.                                  //
  //////////////////////////////////////////////////////////////////////////////
  function BuildSkillSelector($TabIndex, $Name, $Class, $CommID)
  {
    $resultSet = ExecuteQuery('SELECT TrainingType.* FROM TrainingType ORDER BY TrainingType_Name ASC');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    {
      if ($row['TrainingType_ID'] == $CommID)
        echo '<OPTION value="'.$row['TrainingType_ID'].'" SELECTED>'.$row['TrainingType_Name'].'</OPTION>';
      else
        echo '<OPTION value="'.$row['TrainingType_ID'].'">'.$row['TrainingType_Name'].'</OPTION>';
    }
    echo '</SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a skill selection list.                                           //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildSkillList($TabIndex, $Name, $Class, $Skills, $List = 'All')                                                                               
  {
    $resultSet = ExecuteQuery('SELECT TrainingType.* FROM TrainingType ORDER BY TrainingType_Name ASC');
    echo '<SELECT MULTIPLE size="10" tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'" SORTED>';
    while ($row = MySQL_Fetch_Array($resultSet))
    {
      switch ($List)
      {
        case 'All':
          echo '<OPTION value="'.$row['TrainingType_ID'].'">'.$row['TrainingType_Name'].'</OPTION>';
          break;
        case 'Exclude':
          if (!In_Array($row['TrainingType_ID'], $Skills))
            echo '<OPTION value="'.$row['TrainingType_ID'].'">'.$row['TrainingType_Name'].'</OPTION>';
          break;
        case 'Include':
          if (In_Array($row['TrainingType_ID'], $Skills))
            echo '<OPTION value="'.$row['TrainingType_ID'].'">'.$row['TrainingType_Name'].'</OPTION>';
          break;
        default:
          break;
      }
    }         
    echo '</SELECT>';                    
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a training skill level selection list.                            //
  //////////////////////////////////////////////////////////////////////////////
  function BuildSkillLevelSelector($TabIndex, $Name, $Class, $CommID)
  {
    $resultSet = ExecuteQuery('SELECT TrainingLevel.* FROM TrainingLevel ORDER BY TrainingLevel_ID ASC');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">';
    while ($row = MySQL_Fetch_Array($resultSet))
    {
      if ($row['TrainingLevel_ID'] == $CommID)
        echo '<OPTION value="'.$row['TrainingLevel_ID'].'" SELECTED>'.$row['TrainingLevel_Description'].'</OPTION>';
      else
        echo '<OPTION value="'.$row['TrainingLevel_ID'].'">'.$row['TrainingLevel_Description'].'</OPTION>';
    }
    echo '</SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a training skill level selection list.                            //
  //////////////////////////////////////////////////////////////////////////////
  function BuildTrainingRegisterSelector($TabIndex, $Name, $Class, $EntryID)
  {
    $loRS = ExecuteQuery('SELECT TrainingRegister_AutoID, TrainingRegister_Description, CONCAT(Staff_First_Name, " ", Staff_Last_Name) AS Name FROM TrainingRegister INNER JOIN Staff ON TrainingRegister_Staff = Staff_Code ORDER BY Name ASC');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($loRow = MySQL_Fetch_Array($loRS))
    {
      if ($loRow['TrainingRegister_AutoID'] == $EntryID)
        echo '<OPTION value="'.$loRow['TrainingRegister_AutoID'].'" SELECTED>'.$loRow['Name'].' - '.$loRow['TrainingRegister_Description'].'</OPTION>';
      else
        echo '<OPTION value="'.$loRow['TrainingRegister_AutoID'].'">'.$loRow['Name'].' - '.$loRow['TrainingRegister_Description'].'</OPTION>';
    }
    echo '</SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a contract selection list.                                        //
  //////////////////////////////////////////////////////////////////////////////
  function BuildCompanyContractSelector($TabIndex, $Name, $Class, $ContractID)
  {
    $resultSet = ExecuteQuery('SELECT Contract.* FROM Contract ORDER BY Contract_Name ASC');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    {
      if ($row['Contract_ID'] == $CommID)
        echo '<OPTION value="'.$row['Contract_ID'].'" SELECTED>'.$row['Contract_Name'].'</OPTION>';
      else
        echo '<OPTION value="'.$row['Contract_ID'].'">'.$row['Contract_Name'].'</OPTION>';
    }
    echo '</SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a DSA activity selection list.                                    //
  //////////////////////////////////////////////////////////////////////////////
  function BuildDSAActivitySelector($TabIndex, $Name, $Class, $ActType)
  {
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>
            <OPTION value="00000-000-103"';
    if ($ActType == '00000-000-103')
      echo ' SELECTED';
      echo '>00000-000-103 - Assist Colleague</OPTION>
            <OPTION value="00000-000-104"';
    if ($ActType == '00000-000-104')
      echo ' SELECTED';
      echo '>00000-000-104 - DSA Work: No DSA Project Number</OPTION>
          <OPTION value="00000-000-108"';
    if ($ActType == '00000-000-108')
      echo ' SELECTED';
      echo '>00000-000-108 - Holiday: Leave or Sick Leave</OPTION>
          <OPTION value="00000-000-100"';
    if ($ActType == '00000-000-100')
      echo ' SELECTED';
      echo '>00000-000-100 - Lunch</OPTION>
          <OPTION value="00000-000-106"';
    if ($ActType == '00000-000-106')
      echo ' SELECTED';
      echo '>00000-000-106 - S4 Work</OPTION>
          <OPTION value="00000-000-105"';
    if ($ActType == '00000-000-105')
      echo ' SELECTED';
      echo '>00000-000-105 - Stop Problem: Waiting for DSA</OPTION>
          <OPTION value="00000-000-107"';
    if ($ActType == '00000-000-107')
      echo ' SELECTED';
      echo '>00000-000-107 - Study: Not Working Time</OPTION>
          <OPTION value="00000-000-102"';
    if ($ActType == '00000-000-102')
      echo ' SELECTED';
      echo '>00000-000-102 - Troubleshoot: CTS Documentation</OPTION>
          <OPTION value="00000-000-101"';
    if ($ActType == '00000-000-101')
      echo ' SELECTED';
      echo '>00000-000-101 - Troubleshoot: ODX</OPTION>
        </SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a DSA staff selection list.                                       //
  //////////////////////////////////////////////////////////////////////////////
  function BuildDSAStaffSelector($TabIndex, $Name, $Class, $CommID = "")
  {
    $resultSet = array();
    $dsaGuys = GetDSAStaffCodes();
    foreach ($dsaGuys as $dude)
    {
      $rowStaff = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = '.$dude.''));
      $resultSet[$rowStaff['Staff_First_Name'].' '.$rowStaff['Staff_Last_Name']] = array($dude, $rowStaff['Staff_First_Name'].' '.$rowStaff['Staff_Last_Name']);
    }
    KSort($resultSet);
    
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    foreach ($resultSet as $dude)
    {
      if ($dude[0] == $CommID)
        echo '<OPTION value="'.$dude[0].'" SELECTED>'.$dude[1].'</OPTION>';
      else
        echo '<OPTION value="'.$dude[0].'">'.$dude[1].'</OPTION>';
    }
    echo '</SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a marital status selection list.                                  //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildMaritalSelector($TabIndex, $Name, $Class , $MaritalID)                                                                               
  {
    $resultSet = ExecuteQuery('SELECT * FROM Marital ORDER BY Marital_Description ASC');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    { 
      if ($row['Marital_ID'] == $MaritalID)
        echo '<OPTION value="'.$row['Marital_ID'].'" SELECTED>'.$row['Marital_Description'].'</OPTION>';
      else
        echo '<OPTION value="'.$row['Marital_ID'].'">'.$row['Marital_Description'].'</OPTION>';
    }         
    echo '</SELECT>';                    
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a team selection list.                                            //
  //////////////////////////////////////////////////////////////////////////////
  function BuildTeamSelector($paTabIndex, $paName, $paClass , $paTeamID)
  {                                        
    $loRS = ExecuteQuery('SELECT T.Team_ID, T.Name FROM Team T ORDER BY T.Name ASC');
    echo '<SELECT tabindex="'.$paTabIndex.'" name="'.$paName.'" class="'.$paClass.'">
            <OPTION value="">< ... ></OPTION>';
    while ($lpRow = MySQL_Fetch_Array($loRS))
    { 
      if ($lpRow['Team_ID'] == $paTeamID)
        echo '<OPTION value="'.$lpRow['Team_ID'].'" SELECTED>'.$lpRow['Name'].'</OPTION>';
      else
        echo '<OPTION value="'.$lpRow['Team_ID'].'">'.$lpRow['Name'].'</OPTION>';
    }         
    echo '</SELECT>';                    
  }
  
  function BuildCustomTeamSelector($paTabIndex, $paName, $paClass , $paTeamID)
  {                                        
    $loRS = ExecuteQuery('SELECT T.* FROM TeamStaff TS
                LEFT JOIN Team T ON TS.Team_ID = T.Team_ID 
                WHERE TS.Staff_Code  = "'.$_SESSION['cUID'].'"
                GROUP BY TS.Team_ID ORDER BY T.Name ASC');
    echo '<SELECT tabindex="'.$paTabIndex.'" name="'.$paName.'" class="'.$paClass.'">
            <OPTION value="">< ... ></OPTION>';
    while ($lpRow = MySQL_Fetch_Array($loRS))
    { 
      if ($lpRow['Team_ID'] == $paTeamID)
        echo '<OPTION value="'.$lpRow['Team_ID'].'" SELECTED>'.$lpRow['Name'].'</OPTION>';
      else
        echo '<OPTION value="'.$lpRow['Team_ID'].'">'.$lpRow['Name'].'</OPTION>';
    }         
    echo '</SELECT>';                    
  }
  
  
  
  
  function BuildSpesificTeamSelector($paTabIndex, $paName, $paClass , $paTeamID)  // cant use for edit method
  {                                        
    $loRS = ExecuteQuery('SELECT * FROM Team WHERE Team_ID in (SELECT Team_ID FROM TeamStaff WHERE Staff_Code = "'.$_SESSION['cUID'].'")');
    echo '<SELECT tabindex="'.$paTabIndex.'" name="'.$paName.'" class="'.$paClass.'">
            <OPTION value="">< ... ></OPTION>';
    while ($lpRow = MySQL_Fetch_Array($loRS))
    { 
        echo '<OPTION value="'.$lpRow['Team_ID'].'" SELECTED>'.$lpRow['Name'].'</OPTION>';
    }         
    echo '</SELECT>';                    
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a supplier category selection list.                               //
  //////////////////////////////////////////////////////////////////////////////
  function BuildSupplierCategorySelector($paTabIndex, $paName, $paClass, $paSupplierCategoryID)
  {                                        
    $loRS = ExecuteQuery('SELECT SC.Supplier_Category_ID, SC.Name FROM SupplierCategory SC ORDER BY SC.Name ASC');
    echo '<SELECT tabindex="'.$paTabIndex.'" name="'.$paName.'" class="'.$paClass.'">
            <OPTION value="">< ... ></OPTION>';
    while ($lpRow = MySQL_Fetch_Array($loRS))
    {
      if ($lpRow['Supplier_Category_ID'] == $paSupplierCategoryID)
        echo '<OPTION value="'.$lpRow['Supplier_Category_ID'].'" SELECTED>'.$lpRow['Name'].'</OPTION>';
      else
        echo '<OPTION value="'.$lpRow['Supplier_Category_ID'].'">'.$lpRow['Name'].'</OPTION>';
    }
    echo '</SELECT>';
  }
  /* //////////////////////////////////////////////////////////////////////// */
  /* //////////////////////////////////////////////////////////////////////// */
  
  /* SECTION - DATE AND TIME ///////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////////////// */
  // Creates a textual date string in the format '01 Jan 1901'.               //
  //////////////////////////////////////////////////////////////////////////////
  function GetTextualDateFromDatabaseDate($DateString)                                     
  {                        
    return SubStr($DateString, 8, 2).' '.Date('M', MKTime(0, 0, 0, SubStr($DateString, 5, 2) + 1, 0, 0)).' '.SubStr($DateString, 0, 4);
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Creates a session date string in the format '19010101'.                  //
  //////////////////////////////////////////////////////////////////////////////   
  function GetSessionDateFromDatabaseDate($DateString)                                     
  {                        
    return SubStr($DateString, 0, 4).SubStr($DateString, 5, 2).SubStr($DateString, 8, 2);
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Creates a session date string in the format '19010101235900'.            //
  //////////////////////////////////////////////////////////////////////////////   
  function GetSessionDateTimeFromDatabaseDateTime($DateString)                                     
  {                        
    return SubStr($DateString, 0, 4).SubStr($DateString, 5, 2).SubStr($DateString, 8, 2).SubStr($DateString, 11, 2).SubStr($DateString, 14, 2);
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Creates a textual datetime string in the format '01 Jan 1901 at 15:30'.  //
  //////////////////////////////////////////////////////////////////////////////   
  function GetTextualDateTimeFromDatabaseDateTime($DateString)                                     
  {                        
    return SubStr($DateString, 8, 2).' '.Date('M', MKTime(0, 0, 0, SubStr($DateString, 5, 2) + 1, 0, 0)).' '.SubStr($DateString, 0, 4).' at '.SubStr($DateString, 11, 5);
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Creates a textual datetime string in the format '01 Jan 1901 at 15:30'.  //
  //////////////////////////////////////////////////////////////////////////////   
  function GetTextualDateTimeFromSessionDateTime($DateString)                                     
  {                        
    return SubStr($DateString, 6, 2).' '.Date('M', MKTime(0, 0, 0, SubStr($DateString, 4, 2) + 1, 0, 0)).' '.SubStr($DateString, 0, 4).' at '.SubStr($DateString, 8, 2).':'.SubStr($DateString, 10, 2);
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Creates a textual date string in the format '01 Jan 1901'.               //
  //////////////////////////////////////////////////////////////////////////////   
  function GetTextualDateFromSessionDate($DateString)                                     
  {                        
    return SubStr($DateString, 6, 2).' '.Date('M', MKTime(0, 0, 0, SubStr($DateString, 4, 2) + 1, 0, 0)).' '.SubStr($DateString, 0, 4);
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Creates a database date string in the format '1901-01-01'.               //
  //////////////////////////////////////////////////////////////////////////////   
  function GetDatabaseDateFromSessionDate($DateString)                                     
  {                        
    return SubStr($DateString, 0, 4).'-'.SubStr($DateString, 4, 2).'-'.SubStr($DateString, 6, 2);
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Creates a database datetime string in the format '1901-01-01 01:01:01'.  //
  //////////////////////////////////////////////////////////////////////////////   
  function GetDatabaseDateTimeFromSessionDateTime($DateString)                                     
  {                        
    return SubStr($DateString, 0, 4).'-'.SubStr($DateString, 4, 2).'-'.SubStr($DateString, 6, 2).' '.SubStr($DateString, 8, 2).':'.SubStr($DateString, 10, 2).':00';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Checks if one date is later than the other. Uses database dates.         //
  //////////////////////////////////////////////////////////////////////////////   
  function DatabaseDateLater($DateOne, $DateTwo)                                     
  {                        
    return MKTime(0, 0, 0, SubStr($DateOne, 5, 2), SubStr($DateOne, 8, 2), SubStr($DateOne, 0, 4)) > MKTime(0, 0, 0, SubStr($DateTwo, 5, 2), SubStr($DateTwo, 8, 2), SubStr($DateTwo, 0, 4));
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Checks if one datetime is later than the other. Uses database datetimes. //
  //////////////////////////////////////////////////////////////////////////////   
  function DatabaseDateTimeLater($DateTimeOne, $DateTimeTwo)
  {                        
    return MKTime(SubStr($DateTimeOne, 11, 2), SubStr($DateTimeOne, 14, 2), 0, SubStr($DateTimeOne, 5, 2), SubStr($DateTimeOne, 8, 2), SubStr($DateTimeOne, 0, 4)) > MKTime(SubStr($DateTimeTwo, 11, 2), SubStr($DateTimeTwo, 14, 2), 0, SubStr($DateTimeTwo, 5, 2), SubStr($DateTimeTwo, 8, 2), SubStr($DateTimeTwo, 0, 4));
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Checks if one time is later than the other. Uses normal time formats.    //
  //////////////////////////////////////////////////////////////////////////////   
  function TimeLater($TimeOne, $TimeTwo)                                     
  {                        
    return MKTime(SubStr($TimeOne, 0, 2), SubStr($TimeOne, 3, 2), SubStr($TimeOne, 6, 2), 0, 0, 0) > MKTime(SubStr($TimeTwo, 0, 2), SubStr($TimeTwo, 3, 2), SubStr($TimeTwo, 6, 2), 0, 0, 0);
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Creates a textual date from the day, month and year.                     //
  //////////////////////////////////////////////////////////////////////////////   
  function GetTextualDate($Day, $Month, $Year)                                     
  {                        
    return SPrintf('%02d', $Day).' '.Date('M', MKTime(0, 0, 0, $Month + 1, 0, 0)).' '.SPrintf('%04d', $Year);
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Creates a textual datetime from the hour, minute, day, month and year.   //
  //////////////////////////////////////////////////////////////////////////////   
  function GetTextualDateTime($Hour, $Minute, $Day, $Month, $Year)                                     
  {                        
    return SPrintf('%02d', $Day).' '.Date('M', MKTime(0, 0, 0, $Month + 1, 0, 0)).' '.SPrintf('%04d', $Year).' at '.SPrintf('%02d', $Hour).':'.SPrintf('%02d', $Minute);
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Creates a database date from the day, month and year.                    //
  //////////////////////////////////////////////////////////////////////////////   
  function GetDatabaseDate($Day, $Month, $Year)                                     
  {                        
    return Date('Y-m-d', MKTime(0, 0, 0, $Month, $Day, $Year));
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Creates a database date from the day, month and year.                    //
  //////////////////////////////////////////////////////////////////////////////   
  function GetDatabaseDateTime($Hour, $Minute, $Day, $Month, $Year)                                     
  {                        
    return Date('Y-m-d H:i:s', MKTime($Hour, $Minute, 0, $Month, $Day, $Year));
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Creates a session date from the day, month and year.                     //
  //////////////////////////////////////////////////////////////////////////////   
  function GetSessionDate($Day, $Month, $Year)                                     
  {                        
    return Date('Ymd', MKTime(0, 0, 0, $Month, $Day, $Year));
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Creates a session date from the day, month and year.                     //
  //////////////////////////////////////////////////////////////////////////////   
  function GetSessionDateTime($Hour, $Minute, $Day, $Month, $Year)                                     
  {                        
    return Date('YmdHi', MKTime($Hour, $Minute, 0, $Month, $Day, $Year));
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Gets the day from a database date.                                       //
  //////////////////////////////////////////////////////////////////////////////   
  function GetDayFromDatabaseDate($DateString)                                     
  {                        
    return SubStr($DateString, 8, 2);
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Gets the day from a session date.                                        //
  //////////////////////////////////////////////////////////////////////////////   
  function GetDayFromSessionDate($DateString)                                     
  {                        
    return SubStr($DateString, 6, 2);
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Gets the month from a database date.                                     //
  //////////////////////////////////////////////////////////////////////////////   
  function GetMonthFromDatabaseDate($DateString)                                     
  {                        
    return SubStr($DateString, 5, 2);
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Gets the month from a session date.                                      //
  //////////////////////////////////////////////////////////////////////////////   
  function GetMonthFromSessionDate($DateString)                                     
  {                        
    return SubStr($DateString, 4, 2);
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Gets the year from a database date.                                      //
  //////////////////////////////////////////////////////////////////////////////   
  function GetYearFromDatabaseDate($DateString)                                     
  {                        
    return SubStr($DateString, 0, 4);
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Gets the year from a session date.                                       //
  //////////////////////////////////////////////////////////////////////////////   
  function GetYearFromSessionDate($DateString)                                     
  {                        
    return SubStr($DateString, 0, 4);
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Gets the hour from a database datetime.                                  //
  //////////////////////////////////////////////////////////////////////////////   
  function GetTimeFromDatabaseDateTime($DateTimeString)                                     
  {                        
    return SubStr($DateTimeString, 11, 8);
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Gets the hour from a database datetime.                                  //
  //////////////////////////////////////////////////////////////////////////////   
  function GetHourFromDatabaseDateTime($DateTimeString)                                     
  {                        
    return SubStr($DateTimeString, 11, 2);
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Gets the minute from a database datetime.                                //
  //////////////////////////////////////////////////////////////////////////////   
  function GetMinuteFromDatabaseDateTime($DateTimeString)                                     
  {                        
    return SubStr($DateTimeString, 14, 2);
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Gets the hour from a database datetime.                                  //
  //////////////////////////////////////////////////////////////////////////////   
  function GetTimeFromSessionDateTime($DateTimeString)                                     
  {                        
    return SubStr($DateTimeString, 8, 2).':'.SubStr($DateTimeString, 10, 2);
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Gets the hour from a database datetime.                                  //
  //////////////////////////////////////////////////////////////////////////////   
  function GetHourFromSessionDateTime($DateTimeString)                                     
  {                        
    return SubStr($DateTimeString, 8, 2);
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Gets the minute from a database datetime.                                //
  //////////////////////////////////////////////////////////////////////////////   
  function GetMinuteFromSessionDateTime($DateTimeString)                                     
  {                        
    return SubStr($DateTimeString, 10, 2);
  }
  /* //////////////////////////////////////////////////////////////////////// */
  /* //////////////////////////////////////////////////////////////////////// */
  
  /* SECTION - RANDOM FUNCTIONS ////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////////////// */
  // Parses reminders and builds querystring links to the sources.            //
  //////////////////////////////////////////////////////////////////////////////
  function DisplayReminderDescription($Reminder)
  {
    switch ($Reminder['Reminder_System_Reference'])
    {
      case 'RFQ':
        echo '<A href="RFQ.php?view='.$Reminder['Reminder_System_Reference_ID'].'">'.$Reminder['Reminder_Description'].'</A>';
        break;
      case 'Task':
        echo '<A href="Tasks.php?view='.$Reminder['Reminder_System_Reference_ID'].'">'.$Reminder['Reminder_Description'].'</A>';
        break;
      default:
        echo $Reminder['Reminder_Description'];
        break;
    }
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Gets the list of DSA codes.                                              //
  //////////////////////////////////////////////////////////////////////////////
  function GetDSACodes()
  {
    $loDSACodes = array();
    
    $loRS = ExecuteQuery('SELECT StaffDSA_DSACode FROM StaffDSA ORDER BY StaffDSA_DSACode ASC');
    while ($loRow = MySQL_Fetch_Array($loRS))
    {
      $loDSACodes[] = $loRow['StaffDSA_DSACode'];
    }
    return $loDSACodes;
    
    //return array('F2', 'F3', 'F4', 'F5', 'S41', 'S42', 'S43', 'S44', 'S45', 'S46', 'S47', 'S48', 'S49', 'S50', 'S51', 'S52', 'S53', 'S54', 'S55'); //This is in order according to the DSA employee roster.
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Gets the list of DSA staff codes.                                        //
  //////////////////////////////////////////////////////////////////////////////
  function GetDSAStaffCodes()
  {
    $loStaffCodes = array();
    
    $loRS = ExecuteQuery('SELECT StaffDSA_StaffCode FROM StaffDSA ORDER BY StaffDSA_DSACode ASC');
    while ($loRow = MySQL_Fetch_Array($loRS))
    {
      $loStaffCodes[] = $loRow['StaffDSA_StaffCode'];
    }
    return $loStaffCodes;
    
    //return array(16, 39, 54, 65, /*null,*/ 51, 56, 69, /*null,*/ /*null,*/ 07, /*null,*/ 73, 72, 74, /*42,*/ 61, /*null,*/ 62, 64); //This is in order according to the DSA employee roster.
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Gets the DSA staff number for a staff member.                            //
  //////////////////////////////////////////////////////////////////////////////
  function GetDSAStaffCode($paStaffCode)
  {
    $loRS = ExecuteQuery('SELECT StaffDSA_DSACode FROM StaffDSA WHERE StaffDSA_StaffCode = '.$paStaffCode.'');
    if ($loRow = MySQL_Fetch_Array($loRS))
      return $loRow['StaffDSA_DSACode'];
    else
      return 0;
    
    /*switch ($paStaffCode)
    {
      case 16: //Nico.
        return 's41';
        break;
      case 39: //Malcolm.
        return 's42';
        break;
      case 54: //Graham.
        return 's43';
        break;
      case 65: //Thando.
        return 's44';
        break;
      case null:
        return 's45';
        break;
      case 51: //Ludumo.
        return 's46';
        break;
      case 56: //Paul.
        return 's47';
        break;
      case 69: //Richard.
        return 's48';
        break;
      case null:
        return 's49';
        break;
      case null:
        return 's50';
        break;
      case 07: //Johannes.
        return 's51';
        break;
      case null:
        return 's52';
        break;
      case 73: //Ntombi.
        return 's53';
        break;
      case 72: //Abdul.
        return 's54';
        break;
      case 74: //Ralf.
        return 's55';
        break;
      case 61: //Brendan.
        return 'f2';
        break;
      case null:
        return 'f3';
        break;
      case 62: //Jason.
        return 'f4';
        break;
      case 64: //Justin.
        return 'f5';
        break;
      default:
        return 0;
        break;
    }*/
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Gets the staff code for a staff member.                                  //
  //////////////////////////////////////////////////////////////////////////////
  function GetStaffCodeDSA($paDSACode)
  {
    $loRS = ExecuteQuery('SELECT StaffDSA_StaffCode FROM StaffDSA WHERE StaffDSA_DSACode = "'.$paDSACode.'"');
    if ($loRow = MySQL_Fetch_Array($loRS))
      return $loRow['StaffDSA_StaffCode'];
    else
      return 0;
    
    /*switch ($paDSACode)
    {
      case 'S41': //Nico.
        return 16;
        break;
      case 'S42': //Malcolm.
        return 39;
        break;
      case 'S43': //Graham.
        return 54;
        break;
      case 'S44': //Thando.
        return 65;
        break;
      case 'S45':
        return null;
        break;
      case 'S46': //Ludumo.
        return 51;
        break;
      case 'S47': //Paul.
        return 56;
        break;
      case 'S48': //Richard.
        return 69;
        break;
      case 'S49':
        return null;
        break;
      case 'S50':
        return null;
        break;
      case 'S51': //Johannes.
        return 07;
        break;
      case 'S52':
        return null;
        break;
      case 'S53': //Ntombi.
        return 73;
        break;
      case 'S54': //Abdul.
        return 72;
        break;
      case 'S55': //Ralf.
        return 74;
        break;
      case 'F2': //Brendan.
        return 61;
        break;
      case 'F3':
        return null;
        break;
      case 'F4': //Jason.
        return 62;
        break;
      case 'F5': //Justin.
        return 64;
        break;
      default:
        return 0;
        break;
    }*/
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Gets the textual description of a DSA activity.                          //
  //////////////////////////////////////////////////////////////////////////////
  function GetDSAActivity($Activity)
  {
    switch ($Activity)
    {
      case '00000-000-100':
        return 'Lunch';
        break;
      case '00000-000-101':
        return 'Troubleshoot: ODX';
        break;
      case '00000-000-102':
        return 'Troubleshoot: CTS Documentation';
        break;
      case '00000-000-103':
        return 'Assist Colleague';
        break;
      case '00000-000-104':
        return 'No DSA Project Number';
        break;
      case '00000-000-105':
        return 'Stop Problem: Waiting for DSA';
        break;
      case '00000-000-106':
        return 'S4 Work';
        break;
      //case '00000-000-107':
      //  return 'Study: Not Working Time';
      //  break;
      case '00000-000-108':
        return 'Holiday: Leave or Sick Leave';
        break;
      default:
        return 'Unknown';
        break;
    }
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a DSA activity selection list.                                    //
  //////////////////////////////////////////////////////////////////////////////
  function BuildDSAActivitiesSelector($TabIndex, $Name, $Class, $ActType)
  {
    $resultSet = ExecuteQuery('SELECT * FROM DSAActivity ORDER BY DSAActivity_Code ASC');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    { 
      if ($row['DSAActivity_Code'] == $ActType)
        echo '<OPTION value="'.$row['DSAActivity_Code'].'" SELECTED>'.$row['DSAActivity_Code'].' - '.$row['DSAActivity_Description'].'</OPTION>';
      else
        echo '<OPTION value="'.$row['DSAActivity_Code'].'">'.$row['DSAActivity_Code'].' - '.$row['DSAActivity_Description'].'</OPTION>';
    }         
    echo '</SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Gets a DSA activity array.                                               //
  //////////////////////////////////////////////////////////////////////////////
  function GetsDSAActivitiesArray()
  {
    $resultSet = ExecuteQuery('SELECT * FROM DSAActivity ORDER BY DSAActivity_Code ASC');
    $DSAActivitiesArray = array();
    while ($row = MySQL_Fetch_Array($resultSet))
    {
        $DSAActivitiesArray[] = $row;
    }
    return $DSAActivitiesArray;
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Builds a DSA activity selection list.                                    //
  //////////////////////////////////////////////////////////////////////////////
  function BuildDSAActivitiesSelectorFromArray($TabIndex, $Name, $Class, $ActType, $DSAActivitiesArray)
  {
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';
    foreach($DSAActivitiesArray as $row)
    { 
      if ($row['DSAActivity_Code'] == $ActType)
        echo '<OPTION value="'.$row['DSAActivity_Code'].'" SELECTED>'.$row['DSAActivity_Code'].' - '.$row['DSAActivity_Description'].'</OPTION>';
      else
        echo '<OPTION value="'.$row['DSAActivity_Code'].'">'.$row['DSAActivity_Code'].' - '.$row['DSAActivity_Description'].'</OPTION>';
    }         
    echo '</SELECT>';
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Gets the date and time to trigger the RFQ reminder.                      //
  //////////////////////////////////////////////////////////////////////////////
  function GetReminderDate($DateTime)
  {
    switch (Date('l', MKTime(0, 0, 0, GetMonthFromDatabaseDate($DateTime), GetDayFromDatabaseDate($DateTime) - 1, GetYearFromDatabaseDate($DateTime))))
    {
      case 'Saturday':
        $dateReminder = GetDatabaseDate(GetDayFromDatabaseDate($DateTime) - 2, GetMonthFromDatabaseDate($DateTime), GetYearFromDatabaseDate($DateTime));
        break;
      case 'Sunday':
        $dateReminder = GetDatabaseDate(GetDayFromDatabaseDate($DateTime) - 3, GetMonthFromDatabaseDate($DateTime), GetYearFromDatabaseDate($DateTime));
        break;
      default:
        $dateReminder = GetDatabaseDate(GetDayFromDatabaseDate($DateTime) - 1, GetMonthFromDatabaseDate($DateTime), GetYearFromDatabaseDate($DateTime));
        break;
    }
    if (DatabaseDateLater(Date('Y-m-d'), $dateReminder))
      $dateReminder = Date('Y-m-d');
    return $dateReminder;
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Gets the date after a certain number of working days.                    //
  //////////////////////////////////////////////////////////////////////////////
  function GetDateAfterWorkingDays($paDate, $paWorkingDays)
  {
    //Get all public holidays.
    $loHolidays = array();
    
    $loRSPublicHolidays = ExecuteQuery('SELECT PublicHoliday_Date FROM PublicHoliday WHERE PublicHoliday_Date >= "'.$paDate.'"');
    while ($loRowPublicHoliday = MySQL_Fetch_Array($loRSPublicHolidays))
    {
      $loHolidays[] = $loRowPublicHoliday['PublicHoliday_Date'];
    }
    $loHolidays = Array_Unique($loHolidays);
    
    //Loop through the days following the specified date until the number of working days has been found.
    $loStart = MkTime(0, 0, 0, GetMonthFromDatabaseDate($paDate), GetDayFromDatabaseDate($paDate), GetYearFromDatabaseDate($paDate));
    $loDaysRemaining = $paWorkingDays;
    while ($loDaysRemaining > 0)
    {
      $loStart = StrToTime('+1 day', $loStart);
      
      $loDate = Date('Y-m-d', $loStart);
      $loDayOfWeek = Date('N', $loStart);
      if (!(In_Array($loDate, $loHolidays) || ($loDayOfWeek > 5))) //Check that the day is not a Saturday or Sunday and not a public holiday.
        $loDaysRemaining--;
    }
    
    return Date('Y-m-d', $loStart);
  }
  
  /////////////////////////////////////////////////////////////////////////// */
  // Replaces all carriage returns with <BR />.                               //
  //////////////////////////////////////////////////////////////////////////////
  function SetBreaks($Text)
  {
    return Str_Replace(Chr(10), '<BR />', $Text);
  }
  
  /////////////////////////////////////////////////////////////////////////// */
  // Adds commas to large numerical values to improve readability.            //
  // Eg: 1248999 becomes 1,248,999.                                           //
  //////////////////////////////////////////////////////////////////////////////
  function SetLargeValueSeparators($paValue)
  {
    $loParts = Explode('.', "".$paValue);
    
    $loDecimalPart = $loParts[1];
    $loIntegerPart = $loParts[0];
    
    $loIndex = 3;
    while ($loIndex < StrLen($loIntegerPart))
    {
      $loLength = StrLen($loIntegerPart);
      $loIntegerPart = SubStr($loIntegerPart, 0, $loLength - $loIndex).','.SubStr($loIntegerPart, $loLength - $loIndex);
      $loIndex += 4;
    }
    return $loIntegerPart.($loDecimalPart == "" ? "" : '.'.$loDecimalPart);
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Checks that the value is a boolean and returns true or false (1 or 0).   //
  //////////////////////////////////////////////////////////////////////////////
  function CheckBoolean($Value)
  {
    if (CheckNumeric($Value))
    {
      if ($Value == 0)
        return 0;
      else
        return 1;
    } else
      return 0;
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Checks that the value is numeric.                                        //
  //////////////////////////////////////////////////////////////////////////////
  function CheckNumeric($Value, $Minimum = 0)
  {
    if ($Value != "")
    {
      if (!Is_Numeric($Value))
        return false;
      
      if ($Value < $Minimum)
        return false;
    } else
      return false;
    
    return true;
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Replaces each instance of a string.                                      //
  //////////////////////////////////////////////////////////////////////////////
  function Replace($Search, $Replace, $HayStack)
  {
    if (Is_Array($HayStack))
    {
      $copy = array();
      foreach (Array_Keys($HayStack) as $key)
      {
        $needle = $HayStack[$key];
        if (Is_Array($needle))
        {
          $copy[$key] = Replace($Search, $Replace, $needle);
        } else
        {
          $copy[$key] = Str_Replace($Search, $Replace, $needle);
        }
      }
      return $copy;
    } else
    {
      return Str_Replace($Search, $Replace, $HayStack);
    }
  }
  
  
    //////////////////////////////////////////////////////////////////////////////
  // Builds a day label.                                             //
  //////////////////////////////////////////////////////////////////////////////
  function BuildFullDateLabel($fullDate)
  {
      $day=SubStr($fullDate, 8, 2);
      if ($day == ""){
          $day = Date('d');}
      
      $month=SubStr($fullDate, 5, 2);
      if ($month == ""){
          $month = Date('m');}
      $date = MkTime(0, 0, 0, $month + 1, 0, 0);
       
      $year=SubStr($fullDate, 0, 4);
      if ($year == ""){
          $year = Date('Y');}
          
      $hour=SubStr($fullDate, 11, 2);
      if ($hour == ""){
          $hour = Date('H');}
      
      $minute=SubStr($fullDate, 14, 2);
      if ($minute == ""){
          $minute = Date('i');}
   
    $message = $day." ".Date('M', $date)." ".$year." at ".$hour.":".$minute;
    echo '<label class="standard">'.$message.'</label>';
  }
  
  
   function BuildPurchaseOrderMultipleSelector($TabIndex, $Name, $Class , $ProjectID,$selectSize,$all_sub)                                                                                  
  { 
       
    $resultSet = ExecuteQuery('SELECT PurchaseOrder_ID, PurchaseOrder_Number, PurchaseOrder_Value FROM PurchaseOrder ORDER BY PurchaseOrder_Number');
    $testPO = ExecuteQuery('SELECT project_id, purchaseorder_id FROM Project_PurchaseOrder WHERE project_id="'.$ProjectID.'"');
    
    $subset = array();
    while ($row = mysql_fetch_assoc($testPO))
        $subset[] = $row["purchaseorder_id"]; 
    
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'" multiple size="'.$selectSize.'" style="width: 10em;" id="'.$all_sub.'">';
    if($all_sub == 'all'){
        while ($row = MySQL_Fetch_Array($resultSet))
              { 
                 if (!in_array($row['PurchaseOrder_ID'], $subset))
                    echo '<OPTION value="'.$row['PurchaseOrder_ID'].'" placeholder="'.$row['PurchaseOrder_Value'].'" >'.$row['PurchaseOrder_Number'].'</OPTION>'; 
             
              
               }
       }
     else
         if($all_sub == 'sub'){
             $totalCost=0;
             $all_selected_order_id ='';
             while ($row = MySQL_Fetch_Array($resultSet))
              { 
                 if (in_array($row['PurchaseOrder_ID'], $subset))
                    {
                        echo '<OPTION value="'.$row['PurchaseOrder_ID'].'" placeholder="'.$row['PurchaseOrder_Value'].'" >'.$row['PurchaseOrder_Number'].'</OPTION>'; 
                        $totalCost += $row['PurchaseOrder_Value'];
                        if($all_selected_order_id == '')
                        {
                            $all_selected_order_id = $row['PurchaseOrder_ID'];
                            $all_selected_order_value = $row['PurchaseOrder_Value'];
                        }
                        else{
                            $all_selected_order_id .= ','.$row['PurchaseOrder_ID'];
                            $all_selected_order_value .= ','.$row['PurchaseOrder_Value'];
                        }
                     }
               }
               $_SESSION['EditProject'][6] = $totalCost;
               $_SESSION['EditProject'][12] = $all_selected_order_id;
               $_SESSION['EditProject'][13] = $all_selected_order_value;
         }
    echo '</SELECT>'; 
   
  }
  

  
    //////////////////////////////////////////////////////////////////////////////
  // Builds a Storage Type selection list.                                             //
  //////////////////////////////////////////////////////////////////////////////
  function BuildStorageTypeSelector($TabIndex, $Name,$Class, $storageType)
  {
      $storageArray = array('TRA(Tracked)','RES(Resale Tracked)','UNT(Untracked / Resell Untracked)','CONS(Consumables)');
      //if ($storageType == "")
            //$storageType = '0';
      
      echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value=""';
            if ($storageType == "")
              echo ' SELECTED';
            echo'><...></OPTION>';
      $storageLength = count($storageArray);
        for ($i = 0; $i < $storageLength; $i++) {
            if (($i+1) == $storageType)
                echo'<OPTION value="'.($i+1).'" SELECTED>'.$storageArray[$i].'</OPTION>';
            else
                echo'<OPTION value="'.($i+1).'">'.$storageArray[$i].'</OPTION>';
        }
        
        if($storageType == 5){
            echo'<OPTION value="5" SELECTED>DC Item</OPTION>';
        }
        
        '</SELECT>';
        
          
  }
  
    //////////////////////////////////////////////////////////////////////////////
  // return storage type discription based on value.                                             //
  //////////////////////////////////////////////////////////////////////////////  
function RetutnStorageTypeDesc($StorageT)
{
    $displayType = "";
    $storageArray = array('TRA','RES','UNT','CONS','DC Item');
    $storageLength = count($storageArray);
     for ($i = 0; $i < $storageLength; $i++) {
          if (($i+1) == $StorageT)
          {
              $displayType = $storageArray[$i];
              break;
          }
     }
     return $displayType;
}

	//////////////////////////////////////////////////////////////////////////////
  // Builds a page list selector.    //
  //////////////////////////////////////////////////////////////////////////////   
  function BuildPagesSelector($TabIndex, $Name, $Class, $PageID)
  {
    $resultSet = ExecuteQuery('SELECT * FROM AccessPage ORDER BY AccessPage_Name ASC');

    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">';
    while ($row = MySQL_Fetch_Array($resultSet))
    { 
      if ((integer)$row['AccessPage_ID'] == (integer)$PageID)
        echo '<OPTION value="'.$row['AccessPage_ID'].'" SELECTED>'.$row['AccessPage_Name'].'</OPTION>';
      else
        echo '<OPTION value="'.$row['AccessPage_ID'].'">'.$row['AccessPage_Name'].'</OPTION>';
    }         
    echo '</SELECT>';               
  }

  /* //////////////////////////////////////////////////////////////////////// */
  function buildcolorLabel($text,$color){
      if($color==red){
      echo '
            <div style="
            color: #ec3e04;
            ">'.$text.'</div>';
      }
       if($color==green){
      echo '
            <div style="
            background-color: #56ff00;
            width:  20px;
            border-style: solid;
            border-width:0.1em;
            height: 20px;
            margin-right: 10px;
            float: left;
            text-align: center;
            ">'.$text.' </div>';
      }
       if($color==yellow){
      echo '<div style="
            background-color: yellow;
            border-style: solid;
            border-width:0.1em;
            width:  20px;
            height: 20px;
            margin-right: 10px;
            float: left;
            text-align: center;
            ">'.$text.'</div>';
      }
      
  }
  
  function BuildLevelSelector($TabIndex, $Name, $Class){
    
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">'
            . '<OPTION value="(0,1,2,3,4,5,6,7,8)" SELECTED>All</OPTION>'
            . '<OPTION value="(1,2,3,4,5)" >green_(1-5)</OPTION>'
            . '<OPTION value="(6,7,8)">yellow_(6-8)</OPTION>'
            . '<OPTION value="(0)">non compliant</OPTION>'
            . '</SELECT>';     
  }
  /* //////////////////////////////////////////////////////////////////////// */
  
  // build fiter for supplier name but only those who dont have a certificate
  function BuildNoCertificateSuppliers($TabIndex, $Name, $Class , $SupplierCode){

    $resultSet = ExecuteQuery('SELECT Supplier_Code, Supplier_Name FROM `Supplier` WHERE Supplier_Code NOT In ( SELECT Supplier_Code FROM BEE_Certificate) ORDER BY Supplier_Name');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';                  
    while ($row = MySQL_Fetch_Array($resultSet))
    { 
      if ($row['Supplier_Code'] == $SupplierCode)
        echo '<OPTION value="'.$row['Supplier_Code'].'" SELECTED>'.$row['Supplier_Name'].'</OPTION>';
      else
        echo '<OPTION value="'.$row['Supplier_Code'].'">'.$row['Supplier_Name'].'</OPTION>';
    }         
    echo '</SELECT>';   
  }
  
  /// build fiter for supplier name but only those who have a certificate
  function BuildCertificateSuppliers($TabIndex, $Name, $Class , $SupplierCode){

    $resultSet = ExecuteQuery('SELECT Supplier_Code, Supplier_Name FROM `Supplier` WHERE Supplier_Code In ( SELECT Supplier_Code FROM BEE_Certificate) ORDER BY Supplier_Name');
    echo '<SELECT tabindex="'.$TabIndex.'" name="'.$Name.'" class="'.$Class.'">
            <OPTION value="">< ... ></OPTION>';                  
    while ($row = MySQL_Fetch_Array($resultSet))
    { 
      if ($row['Supplier_Code'] == $SupplierCode)
        echo '<OPTION value="'.$row['Supplier_Code'].'" SELECTED>'.$row['Supplier_Name'].'</OPTION>';
      else
        echo '<OPTION value="'.$row['Supplier_Code'].'">'.$row['Supplier_Name'].'</OPTION>';
    }         
    echo '</SELECT>';   
  }
  
  
  // select color for expiry date
  
  function make_date($dateCheck){
      //check if expired
      // check if closse
    // #ff9933
      if (date($dateCheck) <= date("Y-m-d",time())){
          echo '<DIV style= "color:#ff0000" >'.$dateCheck.'</DIV>';
      }
      else {
          echo $dateCheck;
      }
      }
      
      // incase of change 
  function makeLevelGroup($level){
       if($level>0 && $level<6){
          echo '<span class = "greenhighlight">'.$level.'</span>';
      }
      else if($level>5 && $level<8){
             echo '<span class = "yellowhighlight">'.$level.'</span>';
      }
      else if($level == 9){
          echo '<span class = "greenhighlight">N/A - International</span>';
      }
       else if($level == 10){
          echo '<span class = "bluehighlight">Awaiting feedback</span>';
      }
      else {
          echo  '<span class = "redhighlight">non-compliant</span>';
      }
  }    
  
  function getActiveStaff_SMS($TabIndex, $id, $Class){
    $resultSet = ExecuteQuery('SELECT Staff_Phone_Mobile, concat(Staff_First_Name," ",Staff_Last_Name) as Staff_Member FROM Staff WHERE Staff_IsEmployee != 0 ORDER BY Staff_Member');
    echo '<SELECT MULTIPLE tabindex="'.$TabIndex.'" id="'.$id.'" class="'.$Class.'">
    <OPTION value="all"><span style="font-weight:bold;">All Staff</span></OPTION>';
    while ($row = MySQL_Fetch_Array($resultSet))
    {
        if(ValidNumber($row['Staff_Phone_Mobile']))
        {
            echo '<OPTION value="'.numberFormat($row['Staff_Phone_Mobile']).'">'.$row['Staff_Member'].'</OPTION>';
        }
    }
    echo '</SELECT>';   
 }
 
    // value is a array of values
   function getActiveStaffGroup_SMS($id, $Class, $name, $value, $multi, $incluedAll)
  {
       
    $resultSet = ExecuteQuery('SELECT smsApplicationGroup_ID,smsApplicationGroup_Name FROM smsApplicationGroup');
    $s = '<SELECT id = "'.$id.'" class="'.$Class.'" name="'.$name.'" value="'.$value.'">';
    if($multi){
         $s = '<SELECT multiple id = "'.$id.'" class="'.$Class.'" name="'.$name.'" value="'.$value.'">';
    }
    else{
         $s = '<SELECT id = "'.$id.'" class="'.$Class.'" name="'.$name.'" value="'.$value.'">';
    }
    
    if($incluedAll){
        echo $s.'<OPTION value="all"><span style="font-weight:bold;">All Staff</span></OPTION>';
  
    }
    else{
         echo $s.'<OPTION value="no"><span style="font-weight:bold;">< ... ></span></OPTION>';
    }
        while ($row = MySQL_Fetch_Array($resultSet))
        {
            if(in_array($row['smsApplicationGroup_ID'], $value)){
                echo '<OPTION SELECTED value="'.$row['smsApplicationGroup_ID'].'">'.$row['smsApplicationGroup_Name'].'</OPTION>';
            }
            else{
                   echo '<OPTION value="'.$row['smsApplicationGroup_ID'].'">'.$row['smsApplicationGroup_Name'].'</OPTION>';
            }
        }
    echo '</SELECT>';   
  }
  
 
  
  // keep ans for multiple
  function getActiveStaffGroup_SMSM($id, $Class, $name, $value)
  {
    $resultSet = ExecuteQuery('SELECT smsApplicationGroup_ID,smsApplicationGroup_Name FROM smsApplicationGroup');
    $s = '<SELECT id = "'.$id.'" class="'.$Class.'" name="'.$name.'" value="'.$value.'">';
    if($multi){
         $s = '<SELECT multiple id = "'.$id.'" class="'.$Class.'" name="'.$name.'" value="'.$value.'">';
    }
    else{
         $s = '<SELECT id = "'.$id.'" class="'.$Class.'" name="'.$name.'" value="'.$value.'">';
    }
    echo $s.'<OPTION value="allGroups"><span style="font-weight:bold;">< ... ></span></OPTION>';
        while ($row = MySQL_Fetch_Array($resultSet))
        {
            echo '<OPTION value="'.$row['smsApplicationGroup_ID'].'">'.$row['smsApplicationGroup_Name'].'</OPTION>';
        }
    echo '</SELECT>';   
  }
  
 
  function ValidNumber($number){
      $format = str_replace(' ','',$number);
      $format = str_replace('-','',$format);
     // if the number is +27.. + 27 609825985
       if($format[0]==='+'){   
            if(strlen($format)===11)
               return true;
           else
               return false;
       } 
       else{
           if(strlen($format)===10)
               return true;
           else
               return false;
       }
     //else number is 0...
 }
 
  function  numberFormat($number){   
    $format = str_replace(' ','',$number);
    $format = str_replace('-','',$format);
    $result = '';
    for($i =0 ; $i < strlen($format); $i++){
        if( $i==3 || $i==7){
           $result =  $result.' '.$format[$i];
        }
        else{
            $result =  $result.$format[$i];
        }
    }
     return $result;
}
 
  function getActiveStaffSMS($TabIndex, $id, $Class){
    $resultSet = ExecuteQuery('SELECT Staff_Phone_Mobile, concat(Staff_First_Name," ",Staff_Last_Name) as Staff_Member FROM Staff WHERE Staff_IsEmployee != 0 ORDER BY Staff_Member ');
    echo '<SELECT multiple tabindex="'.$TabIndex.'" id="'.$id.'" class="'.$Class.'">';
    while ($row = MySQL_Fetch_Array($resultSet))
        echo '<OPTION value="'.$row['Staff_Phone_Mobile'].'">'.$row['Staff_Member'].'</OPTION>';
    echo '</SELECT>';   
 }
 
 function getActiveStaff($TabIndex, $id, $Class){
    $resultSet = ExecuteQuery('SELECT Staff_Code, concat(Staff_First_Name," ",Staff_Last_Name) as Staff_Member FROM Staff WHERE Staff_IsEmployee != 0 ORDER BY Staff_Member ');
    echo '<SELECT multiple tabindex="'.$TabIndex.'" id="'.$id.'" class="'.$Class.'">';
    while ($row = MySQL_Fetch_Array($resultSet))
        echo '<OPTION value="'.$row['Staff_Code'].'">'.$row['Staff_Member'].'</OPTION>';
    echo '</SELECT>';   
 }
 
 
  function getGroups($TabIndex, getGroups$id, $Class)
  {
    $resultSet = ExecuteQuery('SELECT * FROM smsApplicationGroup WHERE smsApplicationGroup_Staff_Code ='.$_SESSION['cUID']);
    echo '<SELECT tabindex="'.$TabIndex.'" id="'.$id.'" class="'.$Class.'">';
    while ($row = MySQL_Fetch_Array($resultSet))
    {
        echo '<OPTION value="'.$row['smsApplicationGroup_ID'].'">'.$row['smsApplicationGroup_Name'].'</OPTION>';
    }
    echo '</SELECT>';   
  }
  
  
  function buildInvoiceReportContactList($TabIndex, $id, $Class, $stringOfStaffCodes){
    
    if($stringOfStaffCodes === ""){
        $stringOfStaffCodes = $_SESSION['cUID'];
    }  
      
    $resultSet = ExecuteQuery('SELECT concat(Staff_First_Name," ",Staff_Last_Name) as contact, Staff_Code FROM Staff WHERE Staff_Code IN ('.$stringOfStaffCodes.') ORDER BY contact');
    echo '<SELECT multiple tabindex="'.$TabIndex.'" id="'.$id.'" class="'.$Class.'">';
    while ($row = MySQL_Fetch_Array($resultSet))
    {
        echo '<OPTION value="'.$row['Staff_Code'].'">'.$row['contact'].'</OPTION>';
    }
    echo '</SELECT>';  
      
      
  }
  
    function buildAutonetProjectsNotMapped($TabIndex, $name, $Class, $project_Code){


        if(strcmp($project_Code, "") != 0){


            $resultSet = ExecuteQuery('SELECT *, concat(Project_Pastel_prefix," - ", Project_Description) as Project_name  FROM MMAdmin.Project WHERE Project_Code NOT IN (SELECT Project_CodeMM FROM CobotsAdmin.ProjectMapping WHERE Project_CodeMM IS NOT NULL)
                UNION ALL 
                SELECT * , concat(Project_Pastel_prefix," - ", Project_Description) as Project_name FROM MMAdmin.Project WHERE Project_Code ='.$project_Code);
        }
        else{
            $resultSet = ExecuteQuery('SELECT *, concat(Project_Pastel_prefix," - ", Project_Description) as Project_name  FROM MMAdmin.Project WHERE Project_Code NOT IN (SELECT Project_CodeMM FROM CobotsAdmin.ProjectMapping WHERE Project_CodeMM IS NOT NULL)');
        }

      
        echo '<SELECT name = "'.$name.'" tabindex="'.$TabIndex.'" class="'.$Class.'">
        <OPTION value="" SELECTED><...></OPTION>';
        while ($row = MySQL_Fetch_Array($resultSet))
        {
            if($row['Project_Code'] === $project_Code)
                echo '<OPTION value="'.$row['Project_Code'].'" SELECTED>'.$row['Project_name'].'</OPTION>';
            else
                echo '<OPTION value="'.$row['Project_Code'].'" >'.$row['Project_name'].'</OPTION>';
        }

        echo '</SELECT>';
    }
  
  
  function buildAutonetCustomer($TabIndex, $name, $Class, $Code){

      $resultSet = ExecuteQuery('SELECT Customer_Name, Customer_Code FROM MMAdmin.Customer ORDER BY Customer_Name ASC');
      echo '<SELECT name = "'.$name.'" tabindex="'.$TabIndex.'" class="'.$Class.'">
        <OPTION value="" SELECTED><...></OPTION>';
      while ($row = MySQL_Fetch_Array($resultSet))
      {
          if($row['Customer_Code'] === $Code)
              echo '<OPTION value="'.$row['Customer_Code'].'" SELECTED>'.$row['Customer_Name'].'</OPTION>';
          else
              echo '<OPTION value="'.$row['Customer_Code'].'" >'.$row['Customer_Name'].'</OPTION>';
      }

      echo '</SELECT>';
  }
  
    function buildAutonetPurchaseOrder($TabIndex, $name, $Class, $Code){

        $resultSet = ExecuteQuery('SELECT PurchaseOrder_ID, PurchaseOrder_Number FROM MMAdmin.PurchaseOrder ORDER BY PurchaseOrder_Number');
        echo '<SELECT name = "'.$name.'" tabindex="'.$TabIndex.'" class="'.$Class.'">
        <OPTION value="" SELECTED><...></OPTION>';
        while ($row = MySQL_Fetch_Array($resultSet))
        {
            if($row['PurchaseOrder_ID'] === $Code)
                echo '<OPTION value="'.$row['PurchaseOrder_ID'].'" SELECTED>'.$row['PurchaseOrder_Number'].'</OPTION>';
            else
                echo '<OPTION value="'.$row['PurchaseOrder_ID'].'" >'.$row['PurchaseOrder_Number'].'</OPTION>';
        }

        echo '</SELECT>';
  }
  
   function buildAutonetStaff($TabIndex, $name, $id, $Class, $Code){
        
        $resultSet = ExecuteQuery('SELECT Staff_First_Name, Staff_Last_Name, Staff_Code FROM MMAdmin.Staff WHERE Staff_IsEmployee > 0 ORDER BY Staff_First_Name ASC');
        echo '<SELECT name = "'.$name.'" id = "'.$id.'" tabindex="'.$TabIndex.'" class="'.$Class.'">
        <OPTION value="" SELECTED><...></OPTION>';
        while ($row = MySQL_Fetch_Array($resultSet))
        {
            if($row['Staff_Code'] === $Code)
              echo '<OPTION value="'.$row['Staff_Code'].'" SELECTED>'.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].'</OPTION>';
            else
             echo '<OPTION value="'.$row['Staff_Code'].'" >'.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].'</OPTION>';
        }

        echo '</SELECT>';  
  }
  
   function buildStaffDropdownWithMMMap($TabIndex, $name , $id, $Class, $Code){
       $sql = 'SELECT concat(s.Staff_First_Name," ",s.Staff_Last_Name) as `text`, s.Staff_Code , sm.Staff_CodeMM FROM Staff s 
                LEFT JOIN StaffMapping sm on s.Staff_Code = sm.Staff_CodeS4 
                WHERE Staff_IsEmployee > 0 ORDER BY Staff_First_Name ASC';   
        $resultSet = ExecuteQuery($sql);
        echo '<SELECT name = "'.$name.'" id = "'.$id.'"  tabindex="'.$TabIndex.'" class="'.$Class.'">
        <OPTION value="" SELECTED><...></OPTION>';
        while ($row = MySQL_Fetch_Array($resultSet))
        {
            if($row['Staff_Code'] === $Code)
              echo '<OPTION value="'.$row['Staff_Code'].'" data-MMCode = "'.$row['Staff_CodeMM'].'"  SELECTED>'.$row['text'].'</OPTION>';
            else
             echo '<OPTION value="'.$row['Staff_Code'].'" data-MMCode = "'.$row['Staff_CodeMM'].'" >'.$row['text'].'</OPTION>';
        }

        echo '</SELECT>';  

   } 
  
    
   function buildTheApprovalDropdown($TabIndex, $name, $Class, $all){
       $sql = 'SELECT OrderNo_Number, OrderNo_ReportCategory, Supplier_Name , Project.* FROM OrderNo 
            LEFT JOIN Project ON OrderNo_Project = Project_Code
            LEFT JOIN Supplier ON Supplier_Code = OrderNo_Supplier
            WHERE OrderNo_Approved = 0 
            ORDER BY OrderNo_Number DESC';
        $resultSet = ExecuteQuery($sql);
        
         echo '<SELECT name = "'.$name.'" tabindex="'.$TabIndex.'" class="'.$Class.'">
        <OPTION value="" SELECTED><...></OPTION>';
        while ($row = MySQL_Fetch_Array($resultSet))
        {
            $costManagerCategory =  "Project_CostManager".$row['OrderNo_ReportCategory'];
            
            if($all){
                echo '<OPTION value="'.$row['OrderNo_Number'].'">'.$row['OrderNo_Number'].' - '.$row['Supplier_Name'].'</OPTION>';
            }
            else{
                if($row[$costManagerCategory] == $_SESSION["UID"])
                    echo '<OPTION value="'.$row['OrderNo_Number'].'">'.$row['OrderNo_Number'].' - '.$row['Supplier_Name'].'</OPTION>';
            }
        }
        echo '</SELECT>';  
       
   }
   
  ?>