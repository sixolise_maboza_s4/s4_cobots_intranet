   

     
    $(document).ready(function() {
        var savedone = false; 
        var listErrors = [];  
        
        $(".seltest").chosen({
		no_results_text: "No results matched"
            });
            
        var cusheight = $(window).height() -254; 
        $("#makeList").on("click", function() {

           $.ajax({
               url: "Handlers/Suppliers_Handler.php", //refer to the file
               type: "POST", //send it through get method            

               data: { //arguments
                   function: 'certificates',
                   certificates: "all"
               },
               dataType: "json",

               success: function(response) { //if returns what must happen

                   try {

                      console.log(response); 

                      $('.content').html('<div id = "heading" class="left"><SPAN class="heading">View all certificates</SPAN><BR class="veryshort"/><HR></div>\n\
                       <div id="gridContainerSupplier"></div>\n\
                       <div id ="lastLine"  class = "right"><FORM method="post" action="Handlers/Suppliers_Handler.php">\n\
                            <INPUT name="Type" type="hidden" value="ViewBEEList">\n\
                            <INPUT id ="makeList" tabindex="7" name="Submit" type="Submit" class="button" value="Back" /> </FORM></div>');


                       var dataGrid = $("#gridContainerSupplier").dxDataGrid({
                           dataSource: response,
                           filterRow: {
                               visible: true,
                               applyFilter: "auto"
                           },

                           showColumnLines:true,
                           showRowLines:true,
                           rowAlternationEnabled: true,
                           hoverStateEnabled: true,

                           searchPanel: {
                               visible: true,
                               width: 240,
                               placeholder: "Search..."
                           },
                           headerFilter: {
                               visible: true
                           },
                            export:{
                                enabled: true,
                                fileName: "BEE certificate suppliers"
                             },
                           columns: [{
                                   dataField: "Supplier_name",                                
                                   caption: "Supplier name"

                               }, {
                                   dataField: "Expiry_date",
                                   alignment: "right",
                                   width: 140,
                                   dataType: 'date'

                               }, {
                                   dataField: "Level",
                                   dataType: 'number',
                                   alignment: "right",
                                   width: 130,
                                   headerFilter: {
                                       dataSource: [{
                                           text: "green (1-5)",
                                           value: [
                                               ["Level", "<", 6],
                                               ["Level", ">", 0]
                                           ]
                                       }, {

                                           text: "yellow (6-8)",
                                           value: [
                                               ["Level", ">=", 6],
                                               ["Level", "<", 8]
                                           ]
                                       }, {

                                           text: "non-compliant",
                                           value: ["Level", "=", 0]
                                       },{
                                           text: "N/A - International",
                                           value: ["Level", "=", 9]
                                       },{
                                           text: "Awaiting feedback",
                                           value: ["Level", "=", 10]
                                       }]
                                   }

                               }, {

                                   cellTemplate: function (container, options) {
                                       var p1 = "'Files/Intranet/BeeCertificate/"+ options.data.File+"'";
                                    console.log(p1);
                                     $('<a class="BEElink" onclick="window.open('+p1+',\'_blank\');" tabindex="-1" href="#">'+"Certificate"+'</a>').appendTo(container);
                                   },
                                   caption: "Certificate",
                                   dataField: "File"

                           }],

                      onCellPrepared: function (info) {

                           if(info.rowType === "data"){

                                if (info.column.dataField === "Expiry_date") { 
                                    var curDate = new Date();
                                    var expDate = new Date(info.data.Expiry_date);
                                    if(curDate > expDate ){ 
                                         info.cellElement.css("color","red");
                                    }
                                }

                                  if (info.column.dataField === "Level") { 

                                       var level = info.cellElement.html();
                                     if(level === "0")
                                     info.cellElement.html("non-compliant");
                                     else if(level === "9"){
                                          info.cellElement.html("N/A - International");
                                     }
                                     else if(level === "10"){
                                          info.cellElement.html("Awaiting feedback");
                                     }

                                }
                           }
                           else{

                           }
                       }
                       }).dxDataGrid("instance");


                   } catch (e) {
                       console.log(e);
                       console.log("did not load data");
                   }
               },

               error: function(xhr) {
                   console.log(xhr);
               }
           });
       });

        $("#NOCertificates").on("click", function() {

           $.ajax({
               url: "Handlers/Suppliers_Handler.php", //refer to the file
               type: "POST", //send it through get method            

               data: { //arguments
                   function: 'NOcertificates',
                   NOcertificates: "all"
               },
               dataType: "json",

               success: function(response) { //if returns what must happen

                   try {

                      console.log(response); 

                      $('.content').html('<div id = "heading" class="left"><SPAN class="heading">View all certificates</SPAN><BR class="veryshort"/><HR></div>\n\
                       <div id="gridContainerSupplier"></div>\n\
                       <div id ="lastLine"  class = "right"><FORM method="post" action="Handlers/Suppliers_Handler.php">\n\
                            <INPUT name="Type" type="hidden" value="ViewBEEList">\n\
                            <INPUT id ="makeList" tabindex="7" name="Submit" type="Submit" class="button" value="Back" /> </FORM></div>');


                       var dataGrid = $("#gridContainerSupplier").dxDataGrid({
                           dataSource: response,
                           filterRow: {
                               visible: true,
                               applyFilter: "auto"
                           },

                           showColumnLines:true,
                           showRowLines:true,
                           rowAlternationEnabled: true,
                           hoverStateEnabled: true,

                           searchPanel: {
                               visible: true,
                               width: 240,
                               placeholder: "Search..."
                           },
                           headerFilter: {
                               visible: true
                           },
                             export:{
                                enabled: true,
                                fileName: "Non - BEE certificate suppliers"
                             },
                           columns: [{
                                   dataField: "Supplier_Name"                              


                               }, {
                                   dataField: "Supplier_Email"


                               }, {
                                   dataField: "Supplier_Phone"

                               }]

                       }).dxDataGrid("instance");


                   } catch (e) {
                       console.log(e);
                       console.log("did not load data");
                   }
               },

               error: function(xhr) {
                   console.log(xhr);
               }
           });
       });
        
        $("#supplierStatus").on('click',function () {
            $(this).val(this.checked ? 1 : 0);
            console.log($(this).val());
        });


        //upadate supplier status





        ////////////////////////////////////////MAKE DC SUPLLIER TABLE//////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        if( $( "#DCSuppliers_dxDataGrid" ).length) { 
            $.ajax({
               url: "Handlers/Suppliers_Handler.php", //refer to the file
               type: "POST", //send it through get method            
               data: { //arguments
                   function: 'getDCSuppliers'
               },
               dataType: "json",
               success: function(response) {     
                   $("#Loader").hide();
                   console.log(response);
                   $('#DCSuppliers_dxDataGrid').dxDataGrid({
                         dataSource: response,
                         searchPanel: {
                             visible: true,
                             highlightSearchText: true
                             
                         },
                         height: cusheight,   
                         hoverStateEnabled: true,
                         filterRow: {
                            visible: true,
                            applyFilter: "auto"
                         },
                         editing: {
                             allowAdding: false,
                             allowDeleting: false,
                             allowUpdating: true,
                             mode: 'batch'
                         },
                         paging: {enabled: false},
                         columns: [{dataField: "Supplier_Name", allowEditing: false},{dataField: "Name",caption: 'Category', allowEditing: false} ,{dataField: "Supplier_IsDC", caption: 'DC',  width: 80 ,dataType: "boolean" }],
                         onRowUpdated: function(info){
      
                            $.ajax({
                                url: "Handlers/Suppliers_Handler.php", //refer to the file
                                type: "POST", //send it through get method            
                                data: { //arguments
                                    function: 'updateDCSuppliers',
                                    SupplierCode: info.key.Supplier_Code,
                                    Value: info.data.Supplier_IsDC
                                },
                                dataType: "json",
                                success: function(response) {         
                                    console.log(response[0]);
                                 //   console.log(response);
                                 var dataGrid = $('#DCSuppliers_dxDataGrid').dxDataGrid('instance');
                                  dataGrid.option('dataSource',response[0]);

                                   console.log(response[1]);
                                   if(response[1]!=="pass"){
                                      console.log("response has failed " + response[1] +"!== pass"  );
                                       listErrors.push(info.key.Supplier_Code);
                                   }
                                   else{
                                       savedone = true;
                                  }
                                  },
                                error: function(xhr) {
                                    console.log(xhr);
                                }}); 

                         }, 
                         onContentReady: function(info){
                            console.log("in context realdy");
                            if(listErrors.length != 0){
                                 console.log("ERRORS");
                                 $('#ERRERS').html("Requested operation failed. An error occurred during the submission process.")
                                 $('#ERRERS').removeClass('notify');
                                 $('#ERRERS').addClass('error');
                                 $('#ERRERS').show();
                                console.log(listErrors);
                                listErrors = [];
                                savedone = false;
                            }
                            else if(savedone){
                                  $('#ERRERS').html("Requested operation has completed successfully.")
                                  $('#ERRERS').removeClass('error');
                                   $('#ERRERS').addClass('notify');
                                  $('#ERRERS').show();
                                  savedone = false;
                            }
                            else{
                                 $('#ERRERS').hide();
                            }
                             
                         }
                         
                    });
                }, error: function(xhr) {
                        console.log(xhr);
                    }
                });   
        }
            
    });
    
     function CallME(e) {  
            console.log(e);
                 window.open($(e).attr("href"), '_blank ');
                 $(e).attr("href").val("#");
                 e.preventDefault();
                e.stopPropagation();
             }

             function updateSupplierStatus(){
                 $.ajax({
                     url: "Handlers/Suppliers_Handler.php", //refer to the file
                     type: "POST", //send it through get method
                     data: { //arguments
                         function: 'updateSuppliersStatus',
                         SupplierCode: info.key.Supplier_Code,
                         Value: info.data.Supplier_IsDC
                     },
                     dataType: "json",
                     success: function(response) {
                         console.log(response[0]);
                         //   console.log(response);
                         var dataGrid = $('#DCSuppliers_dxDataGrid').dxDataGrid('instance');
                         dataGrid.option('dataSource',response[0]);

                         console.log(response[1]);
                         if(response[1]!=="pass"){
                             console.log("response has failed " + response[1] +"!== pass"  );
                             listErrors.push(info.key.Supplier_Code);
                         }
                         else{
                             savedone = true;
                         }
                     },
                     error: function(xhr) {
                         console.log(xhr);
                     }});
             }
   
      