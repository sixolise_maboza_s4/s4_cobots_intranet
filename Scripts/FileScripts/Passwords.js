/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function() {

    $('input[name=NewPassword]').keyup(function() {
        var pswd = $(this).val();

        //validate length
        if ( pswd.length < 8 ) {
            $('#length i').removeClass('fa-check').addClass('fa-times');
            $('#length').removeClass('valid').addClass('invalid');
        } else {
            $('#length i').removeClass('fa-times').addClass('fa-check');
            $('#length').removeClass('invalid').addClass('valid');
        }

        //validate letter
        if ( pswd.match(/[A-z]/) ) {
            $('#letter i').removeClass('fa-times').addClass('fa-check');
            $('#letter').removeClass('invalid').addClass('valid');
        } else {
            $('#letter i').removeClass('fa-check').addClass('fa-times');
            $('#letter').removeClass('valid').addClass('invalid');
        }

        //validate capital letter
        if (pswd.match(/[A-Z]/) ) {
            $('#capital i').removeClass('fa-times').addClass('fa-check');
            $('#capital').removeClass('invalid').addClass('valid');
        } else {
            $('#capital i').removeClass('fa-check').addClass('fa-times');
            $('#capital').removeClass('valid').addClass('invalid');
        }

        //validate number
        if ( pswd.match(/\d/) ) {
            $('#number i').removeClass('fa-times').addClass('fa-check');
            $('#number').removeClass('invalid').addClass('valid');
        } else {
            $('#number i').removeClass('fa-check').addClass('fa-times');
            $('#number').removeClass('valid').addClass('invalid');
        }
        if ( pswd != $('input[name=NewPasswordConfirm]').val() ) {
            $('#confirmPswd i').removeClass('fa-check').addClass('fa-times');
            $('#confirmPswd').removeClass('valid').addClass('invalid');
        } else {
            $('#confirmPswd i').removeClass('fa-times').addClass('fa-check');
            $('#confirmPswd').removeClass('invalid').addClass('valid');
        }

        if($(".invalid").length == 0)
            $('input.button').removeClass('disabled');
        else
            $('input.button').removeClass('disabled').addClass('disabled');

    }).focus(function() {
        $('#pswd_info').show();
        $('#pswdLabel').css("vertical-align", "top");
    }).blur(function() {
        $('#pswd_info').hide();
        $('#pswdLabel').css("vertical-align", "center");
    });


    /**confirmation password**/
    $('input[name=NewPasswordConfirm]').keyup(function() {
        var cmfrmPswd = $('input[name=NewPassword]').val();
        var pswd = $(this).val();

        //validate length
        if ( pswd != cmfrmPswd ) {
            $('#confirmPswd i').removeClass('fa-check').addClass('fa-times');
            $('#confirmPswd').removeClass('valid').addClass('invalid');
        } else {
            $('#confirmPswd i').removeClass('fa-times').addClass('fa-check');
            $('#confirmPswd').removeClass('invalid').addClass('valid');
        }

        if($(".invalid").length == 0)
            $('input.button').removeClass('disabled');
        else
            $('input.button').removeClass('disabled').addClass('disabled');

    }).focus(function() {
        $('#cmfrm_pswd_info').show();
        $('#cmfrm_pswd_label').css("vertical-align", "top");
    }).blur(function() {
        $('#cmfrm_pswd_info').hide();
        $('#cmfrm_pswd_label').css("vertical-align", "center");
    });


    $('#bntReset').on('click', function(){
        var user = $("[name='Staff'] option:selected").text();
        var userId = $("[name='Staff'] option:selected").val();

        var r = confirm("Are you sure you want to reset password for "+user);

        if (r == true) {
            $.ajax({
                url: "Handlers/ResetPassword_handler.php", //refer to the file
                type: "POST",
                data:{
                    function:'ResetPassword',
                    userId:userId
                },
                success:function(response){
                    console.log(response);
                    if(response = 1){
                        $('#notifyDiv').show();
                        console.log('rep')
//alert user that you done
                    }else{
                        $('#errorDiv').show();
//alert
                    }

                },error:function(xhr){
                    console.log(xhr)
                }
            });

        } else {

        }
    });
});

