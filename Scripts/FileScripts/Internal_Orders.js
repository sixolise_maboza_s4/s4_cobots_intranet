    var expanded = false;
    var listOfOrders; 
       
    $(document).ready(function() { 
   
        // build all the autocomplets for the page with no set filter
        $(".autocomplete").dxAutocomplete({
            dataSource: [{Items_Description: "test", Items_Supplier_Code: "123"}],
            value: function(){
               // reads the value from the above text field that was hiden, cant refrence session, and don't want to make multiple ajax request on load. 
               var text =  $(this).closest(".testing").find(".text.standard").val();
               return text;
            }, 
            valueExpr: 'Items_Description',
            itemTemplate: function (itemData, itemIndex, itemElement) {
                 itemElement.append("<b>" + itemData.Items_Description + "</b><br/>\
                <span style=\"color:grey;\">Manufacturer Code: " + itemData.Items_Supplier_Code + "</span>");
                },
            onValueChanged: function(info){
               // update the hiden textbox 
               var text = info.element.closest(".testing").find(".autocomplete").dxAutocomplete("instance").option("value");
               $('input[name = "'+info.element[0].id+'"]').val(text);

           },
           onItemClick: function(info){
               // update the hiden textbox  and supplier code
               var idq = info.element[0].id;
               var name =  idq.replace("Description", "SupplierCode");

               var Code = info.itemData.Items_Supplier_Code;  // get ajax reqeust to get the supplier code
                $('input[name = "'+name+'"]').val(Code);

           }
        });

        // for when we reload to get the value again
         if($('#SupplierSelect').length){
            //var code = $('#SupplierSelect').val();
            var code = $('#SupplierSelect option:selected').val();
            if(code !== ''){ 
            $.ajax({
                url: "Handlers/InternalOrders_Handler.php", //refer to the file
                type: "GET", //send it through get method            
                dataType: "json",
                data: { //arguments
                    supplier_code: code
                },

                success: function(response) { //if returns what must happen
                  
                    // change color       
                    $('#bee').html(response[0]);
                    $('#bee').removeClass("red green yellow blue");
                    if(response[0]===null || response[0]==="non-compliant"){
                         $('#bee').addClass("red");
                    } 
                    else if(response[0]==="1" || response[0]==="2" || response[0]==="3" || response[0]==="4" || response[0]==="5" || response[0]==="N/A - International" ){
                          $('#bee').addClass("green");
                    }
                    else if(response[0]==="Awaiting feedback"){
                          $('#bee').addClass("blue");
                    }
                    else {
                         $('#bee').addClass("yellow");
                    }

                    //update autocomplete
                    $('#buildAuto').hide();
                    $('#Items_AddIternalOrder').removeClass('blurout');
                    resetAutocomplete(response[1]);

                },

                error: function(xhr) {
                    console.log(xhr);
                }
            });   
            }
            else{
              $('#bee').removeClass("red green yellow blue");
              $('#bee').html('non-compliant');
              $('#bee').addClass("red");
             }
        }
         // for when we reload to get the value again
        if($('#SupplierSelect2').length){
            var code = $('#SupplierSelect2').val();
            if(code !== ''){
        $.ajax({
            url: "Handlers/InternalOrders_Handler.php", //refer to the file
            type: "GET", //send it through get method            
            dataType: "json",
            data: { //arguments
                supplier_code: code
            },
          
            success: function(response) { //if returns what must happen
                // change color
                $('#bee2').html(response[0]);
                $('#bee2').removeClass("red green yellow blue");
                if(response[0]===null || response[0]==="non-compliant"){
                     $('#bee2').addClass("red");
                } 
                else if(response[0]==="1" || response[0]==="2" || response[0]==="3" || response[0]==="4" || response[0]==="5" || response[0]==="N/A - International" ){
                      $('#bee2').addClass("green");
                }
                else if(response[0]==="Awaiting feedback"){
                      $('#bee2').addClass("blue");
                }
                else {
                     $('#bee2').addClass("yellow");
                }
                
                //update autocomplete
                 resetAutocomplete( response[1]);
            },

            error: function(xhr) {
                console.log(xhr);
            }
        });   
    }   
            else{
          $('#bee2').removeClass("red green yellow blue");
          $('#bee2').addClass("red");
          $('#bee2').html('non-compliant');
    }
       }
     
        $('#SupplierSelect2').on('change', function() {
             var code = $(this).val();
            if(code!==""){
                $.ajax({
                url: "Handlers/InternalOrders_Handler.php", //refer to the file
                type: "GET", //send it through get method            
                dataType: "json",
                data: { //arguments
                    supplier_code: code
                },

                success: function(response) { //if returns what must happen
                    $('#bee2').html(response[0]);
                     $('#bee2').removeClass("red green yellow blue");
                        if(response[0]===null || response[0]==="non-compliant"){
                             $('#bee2').addClass("red");
                        } 
                        else if(response[0]==="1" || response[0]==="2" || response[0]==="3"  || response[0]==="4"  || response[0]==="5" || response[0]==="N/A - International" ){
                              $('#bee2').addClass("green");
                        }

                       else if(response[0]==="Awaiting feedback"){
                         $('#bee2').addClass("blue");
                       }    
                        else {
                             $('#bee2').addClass("yellow");
                        }
                        
                        //update autocomplete
                        resetAutocomplete( response[1] );

                },

                error: function(xhr) {
                    console.log(xhr);
                }
            });   
            }
        });
      
        $('#SupplierSelect').on('change', function() {
             var code = $(this).val();
             if(code!==""){
                $.ajax({
                url: "Handlers/InternalOrders_Handler.php", //refer to the file
                type: "GET", //send it through get method            
                data: { //arguments
                    supplier_code: code
                },
                dataType: "json",
                success: function(response) { //if returns what must happen
                    $('#bee').html(response[0]);
                     $('#bee').removeClass("red green yellow blue");
                    if(response[0]===null || response[0]==="non-compliant"){
                         $('#bee').addClass("red");
                    } 
                    else if(response[0]==="1" || response[0]==="2" || response[0]==="3" || response[0]==="4"  || response[0]==="5" || response[0]==="N/A - International" ){
                          $('#bee').addClass("green");
                    }

                    else if(response[0]==="Awaiting feedback"){
                         $('#bee').addClass("blue");
                     }
                    else {
                         $('#bee').addClass("yellow");
                    }
                    
                      //update autocomplete
                       $('#buildAuto').hide();
                       $('#Items_AddIternalOrder').removeClass('blurout');
                       resetAutocomplete(response[1] );

                    
                },

                error: function(xhr) {
                    console.log(xhr);
                }
            });   
            }
        });
        
        $("#MakeList").on("click", function() {
          
          var Project =  $("select[name=Project]").val();
          var OrderStatus = $("select[name=Status]").val(); 
          var Supplier = $("select[name=Supplier]").val();
          var dateStart = $('#start_date').val();
          var dateEnd = $('#end_date').val();
          var Deficit = $('#deficit').prop("checked");
          var title = "";
                    
          if(OrderStatus!==""){
              title += " - " +  $("select[name=Status]").find("option:selected").text();
          }
          
          if(Project!==""){
              title += " - " + $("select[name=Project]").find("option:selected").text();
          }
          if(Supplier!==""){
              title += " - " +  $("select[name=Supplier]").find("option:selected").text();
          }
            
             var p = '<div class="pager"><div style="height: 38px; text-align: center; float: right;">Number of Records: <div id="numberBoxContainer" style="display:inline-block;vertical-align:middle;"></div></div></div>'; 
             
             $('.content').html('<br/><br/><div id = "heading" class="left"><SPAN class="heading">View Internal Order List ' + title +'</SPAN><BR class="veryshort"/><HR></div>\
                    '+p+'<div id = "Loader" class="loader">Loading...</div><div id="gridContainerInternalOrderList"></div>\n\
                    <br/><div id ="lastLine"  class = "right">\n\
                    <INPUT id="reloadButton" name="Submit" class="button" type="button" value="Back" onclick = "reload()" />\n\
                  </div></div>');   
          
             ///////////////////////////////////////////////////////////// 
            $.ajax({
            url: "Handlers/InternalOrders_Handler.php", //refer to the file
            type: "GET", //send it through get method            

            data: { //arguments
                project:Project,
                orderStatus: OrderStatus, 
                supplier: Supplier, 
                dateS: dateStart,
                dateE: dateEnd,
                deficit: Deficit
                //create a date picker and deficit
            },
            dataType: "json",

            success: function(response) { //if returns what must happen
                listOfOrders = response; 
                try {         
  
                    $("#Loader").addClass("Hide");

                    $("#gridContainerInternalOrderList").dxDataGrid({
                    dataSource: {
                        store: {
                            type: "array",
                            key: "OrderNo_Number",
                            data: response
                        }
                    }, 
                    allowColumnResizing: true,
                   // columnAutoWidth: true,
                     loadPanel: {
                        height: 150,
                        width: 400,
                        text: 'Loading...'
                    },
                    height: "auto",
                     searchPanel: {
                        visible: true,
                        width: 250
                    },
                    paging: { pageSize: 17 },
                    columns: [{
                            dataField: "OrderNo_Number",
                            caption: "Order Number"

                        },{
                            dataField: "Project"
                        }, 
                        {
                            dataField: "Supplier_Name",
                             caption: "Supplier"

                        }, 
                        {
                            dataField: "quotation_number",
                            caption: "Quotation Number"
                          
                        }, 
                        {
                            dataField: "OrderNo_Date_Time",
                             dataType: "date",
                             caption: "Date Placed"
                        }, 
                        {
                            dataField: "Requested_By",
                            caption: "Requested By"
                        }, 
                        {
                            dataField: "SupplierInvoice",
                             caption: "Supplier Invoice"
                        }, 
                        {
                            dataField: "OrderNo_Original_Total_Cost",
                            caption: "Original Total",
                            dataType:"number"
                        }, 
                        {
                            dataField: "total",
                            caption: "Invoice Total",
                            dataType:"number"
                        }  
                    ],
                    onContentReady: function(e) {
                        var toolbar = e.element.find('.dx-datagrid-header-panel .dx-toolbar').dxToolbar('instance');
                        toolbar.on('optionChanged', function(arg) {
                           addCustomItem(toolbar);
                        });
                        addCustomItem(toolbar);
                    },
                    masterDetail: {
                        enabled: true,
                        height: "auto",
                        template: function(container, options) { 

                            var currentOrder = options.data;
                            container.addClass("internal-grid-container");
                            $("<div class='black'>").text("Items for: " + currentOrder.OrderNo_Number).appendTo(container);            
                            $("<div>")
                                .addClass("internal-grid")
                                .dxDataGrid({
                                    columnAutoWidth: true,
                                    allowColumnResizing: true,
                                    columns: [{
                                        dataField: "Items_Supplier_Code",
                                        caption: "Manufacturer Code"

                                    },{
                                      dataField: "Items_Description",
                                      caption: "Manufacturer Description"
                                    },{
                                      dataField: "Items_Backorder",
                                      calculateCellValue: function (rowData){
                                            if(rowData.Items_Backorder === 1)
                                            return "Yes";
                                            else 
                                                return  "No";
                                        }
                                    },{
                                      dataField: "Items_Value",                  
                                      caption: "Original Value (VAT excl.)"
                                    },{
                                     dataField: "Nett_Value",           
                                      caption: "Nett_Value (Discount Incl.)"
                                    },{
                                      dataField: "Latest_Value",                
                                      caption: "Invoice Value (Vat excl.)"
                                    },{
                                      dataField: "Items_Quantity",
                                      caption: "Quantity"
                                    },{
                                      dataField: "Items_Asset",
                                      calculateCellValue: function (rowData){
                                            if(rowData.Items_Backorder === 1)
                                            return "Yes";
                                            else 
                                                return  "No";
                                        }
                                    },{
                                      dataField: "Items_Received_Quantity",
                                      caption: "Invoice Capture"
                                    },{
                                      dataField: "Items_Storeman_Received",
                                      caption: "Storeman Received"
                                    },{
                                      dataField: "Items_StorageType",
                                      caption: "Storage Type"
                                    },{
                                      dataField: "Items_Discount",
                                      caption: "Discount %"  
                                    }
                                    ],
                                    dataSource: currentOrder.Items,
                                     onCellPrepared: function(e) { 

                                        if(e.rowType === 'data'){
                                            e.cellElement.addClass('size');
                                        }
                                        else{
                                               e.cellElement.addClass('white');
                                        } 

                                        if(e.rowType === 'data' && (e.column.dataField === 'Items_Value' || e.column.dataField === 'Nett_Value' || e.column.dataField === 'Latest_Value' )){
                                            if(e.text!=="")
                                              e.cellElement.text(options.data.Currency_Symbol +" "+ e.text);
                                         }

                                        if(e.rowType === 'data' && e.column.dataField === 'Items_Discount'){
                                             if(e.text!=="")
                                              e.cellElement.text(e.text + " %");
                                        }


                                   }
                                }).appendTo(container);
                        }

                    },
                    onCellPrepared: function(e) {   

                        if(e.rowType === 'data'){
                            e.cellElement.addClass('size');
                        }
                        else{
                               e.cellElement.addClass('white');
                        } 

                       if (e.rowType === 'data' && (e.column.dataField === 'OrderNo_Original_Total_Cost' || e.column.dataField === 'total')) {
                            if(e.text!=="")
                                e.cellElement.text(e.data.Currency_Symbol +" " + e.text);
                         }
                    }

                });

                  $("#numberBoxContainer").dxNumberBox({
                      value: 25,
                      step: 25,
                      showSpinButtons: true,
                      min: 25,
                      max: 1000,
                      height: 35,
                      width: 100,
                      onValueChanged: function (info) {
                          var dataGrid = $('#gridContainerInternalOrderList').dxDataGrid('instance');
                          dataGrid.pageSize(info.value);
                      }
                   });
               } 
              
                catch (e) {   
                    console.log(e);
                }

            },

            error: function(xhr) {
                console.log(xhr);
            }
        });   
            //////////////////////////////////////////////////////////////////
          });       
    ///////////////////////////////////////////////////////////////////////////
    });
     
    function GenerateDocument(verb, url, data, target) {
        var form = document.createElement("form");
        form.action = url;
        form.method = verb;
        form.target = target || "_self";
        if (data) {
          for (var key in data) {
            var input = document.createElement("textarea");
            input.name = key;
            input.value = typeof data[key] === "object" ? JSON.stringify(data[key]) : data[key];
            form.appendChild(input);
          }
        }
        form.style.display = "none";
        document.body.appendChild(form);
        form.submit();

      };
      
    function exportfile(){
        var FileN = 'Internal_Orders';
        GenerateDocument('POST', 'Handlers/ExcelExport.php', { SQlArray: JSON.stringify(listOfOrders), FileName: FileN }, '_blank'); 
    }  
       
    function reload(){
         location.reload();
    }
    
    function addCustomItem(toolbar) {
      var items = toolbar.option('items');
      var myItem = DevExpress.data.query(items).filter(function(item) {
        return item.name === 'myButton';
      }).toArray();
      if (!myItem.length) {
        items.unshift( {
          location: 'after',
          widget: 'dxButton',
          name: 'myButton',
          options: {
            icon:"export",
            text: "export",
            hint: "export records",
            onClick: function(e) {
             exportfile();
            }
          }
        },{
          location: 'after',
          widget: 'dxButton',
          name: 'myButton',
          options: {
            icon:"chevrondown",
            hint: "expand all",
            text: "expand all",
            onClick: function(e) {
                var dataGrid = $('#gridContainerInternalOrderList').dxDataGrid('instance');
                if(expanded){
                    dataGrid.collapseAll(-1);
                    e.component.option("icon", "chevrondown");
                    e.component.option("text", "expand all");
                    expanded = false;
                }
                else{
                    dataGrid.expandAll(-1);
                    e.component.option("icon", "chevronup");
                    e.component.option("text", "collapse all");
                    expanded = true;
               }
            }
          }
        });
        toolbar.option('items', items);
      }
    }
   
   function resetAutocomplete(List){
        $(".autocomplete").each(function() {
           $(this).dxAutocomplete('instance').option('dataSource', List ); 
        });
   }
    
