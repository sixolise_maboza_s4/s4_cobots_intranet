/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

 var HTMLgroup = "";
        var list; 
        var smsnumbs=1;
        var charcternumber=160;
        var unlock = true;
          
          
$(document).ready(function() {
       
       $(".seltest").chosen({
			no_results_text: "No results matched"
		});
            GetCredits();

            $.ajax({
                   url: "Handlers/Message_Handler.php", //refer to the file
                   type: "POST", //send it through get method       
                   dataType: "json",
                   data: { //arguments
                      function: "getList"
                   },
                   success: function(response) { 
                       $("#gridContainer").dxDataGrid({
                           dataSource: response[0],
                           grouping: {
                               autoExpandAll: true
                           },
                           editing: {
                               mode: 'form',
                               allowDeleting: true,
                               allowAdding: true
                           },
                           groupPanel: {
                             visible: true
                           },
                           paging:{
                               enabled: false
                           },
                           height: 510,
                           columns: 
                           [      
                               {   dataField: 'sms_GroupID',
                                   caption:'Group Name', 
                                   groupIndex: 0,
                                   validationRules: [{ type: "required" }],
                                   lookup: {
                                       dataSource: response[1],
                                       displayExpr: "smsApplicationGroup_Name",
                                       valueExpr: "smsApplicationGroup_ID"
                               } },
                               {   dataField: 'sms_Staff_Code', 
                                   caption:'Staff Member', 
                                   validationRules: [{ type: "required" }],
                                   lookup: {
                                       dataSource: response[2],
                                       displayExpr: "Staff_Member",
                                       valueExpr: "Staff_Code"
                               }}
                           ],
                           onRowRemoving: function(info)
                           {
                              $.ajax({
                                       url: "Handlers/Message_Handler.php", //refer to the file
                                       type: "POST", //send it through get method            
                                       data: { //arguments
                                           function: 'Delete',
                                           GroupID: info.data.smsApplicationGroup_ID,  
                                           Delete: info.data.sms_Staff_Group_ID  
                                       },
                                       success: function(response) { 
                                           console.log('Deleted');
                                       },
                                       error: function(xhr) {
                                           var locations = [];
                                           console.log(xhr);
                                       }
                                   });   

                           },
                           onRowInserted: function(info){
                               var name = "exits";
                                var groupId = info.key.sms_GroupID;
                                var a = [info.key.sms_Staff_Code];
                               EditGroup(name,groupId,a);

                           }

                       });

                   },
                   error: function(xhr) {
                     console.log("ERROR 201")  
                     console.log(xhr);
                   }
               }); 

            $('#saveNewGroup').on('click',function(){

                var name = $('#Grouptxt').val();  // name of group 
                var groupId = -1;
                var a = $('#Edit_contacts').val();// all the people in the group

                if(name!== "" && a != null){

                   //$("#popAlert").hide();
                   addGroup(name,groupId,a);
                }
                else{
                   addGroup("#FailedNAME***",groupId,['no']);
                }


            });

            $("#box").bind('input propertychange', function()    /*$("#box").keyup(function()*/{

                var number=$(this).val().length;
                smsnumbs=1;
                charcternumber=160;
                $("#count").html("<b>Characters left:</b> " + (charcternumber - number )+". "+smsnumbs+" SMS(s)");

                if(number>=160){
                    smsnumbs=2;
                    charcternumber =2;
                    charcternumber =306;
               } 

                 if(number>=306) {
                  smsnumbs=3; 
                  charcternumber=2;
                  charcternumber=459;
                }

                $("#count").html("<b>Characters left:</b> " + (charcternumber -number )+". "+smsnumbs+" SMS(s)");
                var exceed_num=charcternumber -number;
                if(exceed_num<0) {
                    $('#Send').prop('disabled', true); /*Disable send button*/  
                    $('#SendGroup').prop('disabled', true); /*Disable send button*/  
                }

                else{
                   $('#Send').prop('disabled', false); 
                   $('#SendGroup').prop('disabled', false);
                }
            });   

            $('#Send').on('click',function(){

                var message = $('#box').val();
                var number = $('#contacts').val(); 
                var lenght =  $("#box").val().length;
                var sms = 1;
                  if(lenght > 160 ){
                      sms = 2;

                      if(lenght > 306){ 
                         sms = 3;
                      }  
                  }
  
                $.ajax({
                   url: "Handlers/Message_Handler.php", //refer to the file
                   type: "POST", //send it through get method            
                   data: { //arguments
                      function: 'SendStaff',  
                      Message: message,
                      Number: number,
                      sms: sms
                   },
                   success: function(response) 
                   {   
                        console.log(response);
                        if(response === "Credits"){
                            $("#insufficientCrt").removeClass('hide');
                        }
                        else{
                          location.reload();
                        }
 
                   },
                   error: function(xhr) 
                   { 
                       console.log(xhr);
                   }
               });
            });       

            $('#SendGroup').on('click',function(){

                var message = $('#box').val();
                var Id = $('#Group').find('option:selected').val();  
  
                var lenght =  $("#box").val().length;
                var sms = 1;
                if(lenght > 160 ){
                    sms = 2;

                    if(lenght > 306){ 
                       sms = 3;
                    }  
                }

                $.ajax({
                    url: "Handlers/Message_Handler.php", //refer to the file
                    type: "POST", //send it through get method            
                    data: { //arguments
                        function: 'SendGroup', 
                        Message: message,
                        Id: Id,
                        sms: sms
                    },
                    success: function(response) { 
                        if(response === "Credits"){
                            $("#insufficientCrt").removeClass('hide');
                        }
                        else{
                          location.reload();
                        }
 
                    },
                    error: function(xhr) {
                        console.log(xhr);
                    }
                });         
              
            });       

            $('#contacts').on('change', function(){
                setCountForContacts();
             
            });
   
            $('#group').on('change', function(){
               setCountForGroups();
            });
    
         });

        function GetCredits()
        {
            
             $.ajax({
                url: "Handlers/Message_Handler.php", //refer to the file
                type: "POST", //send it through get method            
                data: { //arguments
                    function: "credits"
                },
                success: function(response) { 
                    if(response === "???"){
                       $('#ConnectionProb').removeClass('hide');
                    }
                    else{
                         $('#ConnectionProb').addClass('hide');
                    }
                    $('#Credit').html("<b>Credits(SMSs): </b> " +response);
                },
                error: function(xhr) {
                    console.log(xhr);
                }
            });         
         }
            
            

        function EditGroup(name,groupId,a)
        {
           
            $.ajax({
                url: "Handlers/Message_Handler.php",
                type: "POST", 
                dataType: "json",
                data: { 
                    function: "UpdateGroup",
                    Name: name,
                    groupID: groupId,
                    staff_Codes: a
                },

                success: function(response) 
                {   
                    console.log(response);
                    var dataGrid = $('#gridContainer').dxDataGrid('instance');
                    dataGrid.option('dataSource',response[1]);
                    list = response[1];  
                },
                error: function(xhr) 
                {
                   console.log(xhr);
                }
            });  
                  
       }
         
        function addGroup(name,groupId,a)
        {
           
            $.ajax({
                url: "Handlers/Message_Handler.php",
                type: "POST", 
                data: { 
                    function: "Duplicate",
                    Name: name  
                },
                
                success: function(response) 
                {   
                    if(response === "false"){
                        $.ajax({
                        url: "Handlers/Message_Handler.php",
                        type: "POST", 
                        data: { 
                            function: "AddGroup",
                            Name: name,
                            groupID: groupId,
                            staff_Codes: a
                        },

                        success: function(response) 
                        {   
                            console.log(response);
                            location.reload();
                        },
                        error: function(xhr) 
                        {
                           console.log(xhr);
                        }
                    });  
                    }
                    else if (response === "true"){
                        $('#duplicate_name').show();
                    }
                },
                error: function(xhr) 
                {
                }
            });         
       }

        function doalert(checkboxElem)
        {
         if(checkboxElem.checked) 
         {
           setCountForGroups();
           $('#Group').removeClass('Hide');  
           $('#Individual').addClass('Hide');
           $('#Send').addClass('Hide');
           $('#SendGroup').removeClass('Hide');
           $('#SendGroup').addClass('Button');
           $('#SendGroup').addClass('button');
           
         }
         else
         {  
            setCountForContacts(); 
            $('#Individual').removeClass('Hide'); 
            $('#Group').addClass('Hide');
            $('#SendGroup').addClass('Hide');
            $('#Send').removeClass('Hide');
            $('#Send').addClass('Button');
         }
       }
          
        function setCountForContacts(){
               var  values = $('#contacts').val();
                var size = 0;
                if(values!=null){
                  var size = values.length;
                }
             
                $('#NumberOfRecipients').html("<b>Recipients:</b> " + size);
         }
         
        function setCountForGroups(){
            $('#NumberOfRecipients').html("<b>Recipients:</b> " + 1);
            $.ajax({
               url: "Handlers/Message_Handler.php",
               type: "POST", 
               data: { 
                   function: "getGroupCount",
                   GroupId: $('#group').val()
               },

               success: function(response) 
               {   
                    $('#NumberOfRecipients').html("<b>Recipients:</b> " + response);
               },
               error: function(xhr) 
               {
               }
           });       
         }
