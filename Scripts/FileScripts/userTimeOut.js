$(document).ready(function () {
    /* popups a timeout dialog with 'Log out' and 'Stay Logged in' options after a certain idle time. If 'Log Out' is 
    clicked, the logout handler is called, the logout handler unsets the sessions,the page is redirected to the index page.
    If 'Stay Logged in' is clicked, a keep-alive URL is requested through AJAX. If no options is selected after another set amount of time, the page is automatically redirected to a timeout URL*/
        $(document).userTimeout({
            session: 240000, //4min
            logouturl:'index.php',
            notify:true,
            timer:true,
            ui:'jqueryui'
        });

});