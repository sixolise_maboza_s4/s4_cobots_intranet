/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

 $(document).ready(function(){
     
      $(".seltest").chosen({
		no_results_text: "No results matched"
            });
           $("#loadMoreManagers").change(function(){
               if(this.checked){
                   $("TR").removeClass("hide");
               
                   $("#loadMoreManagers").addClass("hide");
                   $("#showMore").addClass("hide");
               }
               else{
                   //$("TR").addClass("hide");
               }
           });
     
    	  // if the div exsist

              if($('#datagridmapping').length){
//                  console.log("Im here");
                  $.ajax({
                      url: "Handlers/Projects_Handler.php", //refer to the file
                      type: "GET", //send it through get method
                      dataType: "json",
                      data: { //arguments
                          function: 'getProjectMappingCode'
                      },

                      success: function(response) { //if returns what must happen
//                          console.log(response);
                          $('#datagridmapping').html("");
                          $('#datagridmapping').dxDataGrid({
                              dataSource: response,
                              filterRow: { visible: true },
                              columns: ['Project_NameS4', 'Project_NameMM'],
                              allowFiltering: false
                          });
                          $('#Loader').css("display","none");
                      },

                      beforeSend: function() {
                          $('#datagridmapping').html('<div id = "Loader" class="loader">Loading...</div>');
                      },

                      error: function(xhr) {
                          console.log(xhr);
                      }
                  });
              }

	  
              if($('#cbAddMM').prop("checked")){
                      $('#MapProjectSection').addClass('disable');
                       $(".AutonetProjectDetailsSection").removeClass('hide');
               }
              $('#cbAddMM').on('click', function(){
                  if($('#cbAddMM').prop("checked")){
                      $('#MapProjectSection').addClass('disable');
                      $('#cbAddMM').attr("value","checked");
                      $(".AutonetProjectDetailsSection").removeClass('hide');
                  }
                  else{
                    $('#MapProjectSection').removeClass('disable'); 
                    $('#cbAddMM').attr("value","Notchecked");
                    $(".AutonetProjectDetailsSection").addClass('hide');
                  }
              });
              $('[name="AddMMPrefix"]').on('change', function(){
                  console.log('found it');
//                  var value = $('[name="AddMMPrefix"]').find(":selected").val();
//                   console.log($('[name="AddMMPrefix"]').find(":selected").text());
////                    if(value === ""){
////                        
////                    }
////                    else{
////                        
////                    }
//                   console.log(value);
//                //$('#AddtoAutonetSection')..addClass('disable');
              });
           
              $('#S4ResponsibleS4A').on('change',function(){
                    if($('#MMResponsible :selected').text() === "<...>" ){
                         var MMCode =  $('#S4ResponsibleS4A').find(":selected").attr('data-MMCode');
                        $('#MMResponsible').val(MMCode);
                    }
              });
           
               $('#S4CostManager3').on('change', function(){
                   console.log("changed"); 
                   if($('#MMCostManager :selected').text() === "<...>" ){
                       console.log(" was null");
                        var MMCode =  $('#S4CostManager3').find(":selected").attr('data-MMCode');
                       $('#MMCostManager').val(MMCode);
                   }
                   
                });
           
           
              $('#S4CostManager').on('change',function(){
                  var MMCode =  $('#S4CostManager').find(":selected").attr('data-MMCode');
                   if(MMCode !== ""){
                        $('#MMCostManager').val(MMCode);
                   }
              });
           
      
});
