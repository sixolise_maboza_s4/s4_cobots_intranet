    
      var expanded = false;
    var listOfOrders; 
    var lableMark = true;
    $(document).ready(function() { 
  
    
      $(".seltest").chosen({
			no_results_text: "No results matched"
		});
		
        $('#deficit').on('click', function(){
            console.log("deficit clicked");
            if($('#deficit').prop("checked")){
              // console.log("true"); 
               $('#NotDeliverd').prop('checked', false);
            }
            else{
                //console.log("false");
            }
        });
        
        $('#NotDeliverd').on('click', function(){
               //console.log("NotDeliverd clicked");
                if($('#NotDeliverd').prop("checked")){
                   // console.log("true"); 
                   $('#deficit').prop('checked', false);
                }
                else{
                 //console.log("false");
                }
               
        });
		
		
        /////////////////////////////////////MAKE AUTO COMPLETE THINGS///////////////////////////////////////////////////// 
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
          // build all the autocomplets for the page with no set filter
        $(".autocomplete").dxAutocomplete({
            dataSource: [{Items_Description: "test", Items_Supplier_Code: "123"}],
            value: function(){
               // reads the value from the above text field that was hiden, cant refrence session, and don't want to make multiple ajax request on load. 
               var text =  $(this).closest(".test").find(".text.veryshort").val();
               return text;
            }, 
            valueExpr: 'Items_Supplier_Code',
            itemTemplate: function (itemData, itemIndex, itemElement) {
                 itemElement.append("<b>" + itemData.Items_Supplier_Code + "</b><br/>\
                <span style=\"color:grey;\">Manufacturer Description: " + itemData.Items_Description + "</span>");
                },
            onValueChanged: function(info){
               // update the hiden textbox 
               var text = info.element.closest(".test").find(".autocomplete").dxAutocomplete("instance").option("value");
               $('input[name = "'+info.element[0].id+'"]').val(text);

           },
           onItemClick: function(info){
               // update the hiden textbox  and supplier code
               var idq = info.element[0].id;
               var name =  idq.replace("SupplierCode", "Description");

               var Code = info.itemData.Items_Description;  // get ajax reqeust to get the supplier code
                $('input[name = "'+name+'"]').val(Code);

           }
        });

        $('#cancleUncontroledItems').on('click', function(){
             $('#coverAll').addClass('Hide');
             $('#UncontroledGoodsItems').addClass('Hide'); 
        });
 
       /////////////////////////////////////MAKE SUPPLIER THINGS////////////////////////////////////////////////////////
       ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // for when we reload to get the value again
        if($('#SupplierSelect').length){
            var code = $('#SupplierSelect option:selected').val();
            if(code !== ''){ 
            $.ajax({
                url: "Handlers/InternalOrders_Handler.php", //refer to the file
                type: "GET", //send it through get method            
                dataType: "json",
                data: { //arguments
                    function: 'getSuplierCode',
                    supplier_code: code
                },

                success: function(response) { //if returns what must happen
                  //  $('#SupplierSelect_chzn').addClass('blurout');
                    $(".veryshort.StorageADD" ).each(function() {
                            if(response[3] === '1'){
                                if($(this).find('option:contains("DC Item")').length <= 0)
                                    $(this).append('<option value=5>DC Item</option>');
                              if($(this).find(":selected").text() === "<...>"){
                                    $(".veryshort.StorageADD").val(5);
                                }
                            }
                            else{
                                if($(this).find('option:contains("DC Item")').length > 0)
                                 $(".veryshort.StorageADD option[value='5']").remove();
                             }
                        });
                       
                    
                    if(response[3] === '1'){
                            if( $(".veryshort.StorageADDF").find('option:contains("DC Item")').length <= 0)
                                    $(".veryshort.StorageADDF").append('<option value=5>DC Item</option>');
                            if($(".veryshort.StorageADDF option:selected").text() === "<...>"){
                             $(".veryshort.StorageADDF").val(5);
                            }    
                    }
                    else{
                        if( $(".veryshort.StorageADDF").find('option:contains("DC Item")').length > 0)
                        $(".veryshort.StorageADDF option[value='5']").remove();
                    }
                             
                   
                    
                    console.log(response);
                    // change color       
                    $('#bee').html(response[0]);
                    $('#bee').removeClass("red green yellow blue");
                    if(response[0]===null || response[0]==="non-compliant"){
                         $('#bee').addClass("red");
                    } 
                    else if(response[0]==="1" || response[0]==="2" || response[0]==="3" || response[0]==="4" || response[0]==="5" || response[0]==="N/A - International" ){
                          $('#bee').addClass("green");
                    }
                    else if(response[0]==="Awaiting feedback"){
                          $('#bee').addClass("blue");
                    }
                    else {
                         $('#bee').addClass("yellow");
                    }

                    //update autocomplete
                    $('#buildAuto').hide();
                    $('#Items_AddIternalOrder').removeClass('blurout');
                    resetAutocomplete(response[1]);
                                      
                    

                },

                error: function(xhr) {
                    console.log(xhr);
                }
            });   
            }
            else{
              $('#bee').removeClass("red green yellow blue");
              $('#bee').html('non-compliant');
              $('#bee').addClass("red");
              $('#buildAuto').html('Select to access "Item Details"');
             }
        }
         // for when we reload to get the value again
        if($('#SupplierSelect2').length){
            var code = $('#SupplierSelect2').find(":selected").val();
            if(code !== ''){
                
                console.log(code);
                $.ajax({
                    url: "Handlers/InternalOrders_Handler.php", //refer to the file
                    type: "GET", //send it through get method            
                    dataType: "json",
                    data: { //arguments
                        function: 'getSuplierCode',
                        supplier_code: code
                    },

                    success: function(response) { //if returns what must happen
                        // supplier
                      //  $('#SupplierSelect2_chzn').addClass('blurout');
                        console.log('code:' + code + " " + response);
                        $(".veryshort.StorageADD" ).each(function() {
                             if(response[3] === '1'){
                                 if($(this).find('option:contains("DC Item")').length <= 0)
                                     $(this).append('<option value=5>DC Item</option>');                                 
                                 if($(this).find(":selected").text() === "<...>"){
                                  $(this).val(5);
                                 }
                                 
                             }
                             else{
                                 if($(this).find('option:contains("DC Item")').length > 0)
                                  $(".veryshort.StorageADD option[value='5']").remove();
                              }
                         });

                        if(response[3] === '1'){
                            if( $(".veryshort.StorageADDF").find('option:contains("DC Item")').length <= 0)
                                     $(".veryshort.StorageADDF").append('<option value=5>DC Item</option>');
                            if($(".veryshort.StorageADDF option:selected").text() === "<...>"){
                             $(".veryshort.StorageADDF").val(5);
                            }
                                
                         }
                         else{
                             if( $(".veryshort.StorageADDF").find('option:contains("DC Item")').length > 0)
                                 $(".veryshort.StorageADDF option[value='5']").remove();
                         }                 
                
                
                        // change color
                        $('#bee2').html(response[0]);
                        $('#bee2').removeClass("red green yellow blue");
                        if(response[0]===null || response[0]==="non-compliant"){
                             $('#bee2').addClass("red");
                        } 
                        else if(response[0]==="1" || response[0]==="2" || response[0]==="3" || response[0]==="4" || response[0]==="5" || response[0]==="N/A - International" ){
                              $('#bee2').addClass("green");
                        }
                        else if(response[0]==="Awaiting feedback"){
                              $('#bee2').addClass("blue");
                        }
                        else {
                             $('#bee2').addClass("yellow");
                        }

                        //update autocomplete
                         resetAutocomplete( response[1]);
                    },

                    error: function(xhr) {
                        console.log(xhr);
                    }
        });   
    }   
            else{
          $('#bee2').removeClass("red green yellow blue");
          $('#bee2').addClass("red");
          $('#bee2').html('non-compliant');
    }
       }
     
        $('#SupplierSelect2').on('change', function() {
             var code = $(this).val();
            if(code!==""){
                $.ajax({
                url: "Handlers/InternalOrders_Handler.php", //refer to the file
                type: "GET", //send it through get method            
                dataType: "json",
                data: { //arguments
                    function: 'getSuplierCode',
                    supplier_code: code
                },

                success: function(response) { //if returns what must happen
                     console.log(response);
                    
                     //$('#SupplierSelect2_chzn').addClass('blurout');
                       
                       $(".veryshort.StorageADD" ).each(function() {
                            if(response[3] === '1'){
                                if($(this).find('option:contains("DC Item")').length <= 0)
                                    $(this).append('<option value=5>DC Item</option>');
                                 if($(this).find(":selected").text() === "<...>"){
                                  $(this).val(5);
                                }
                            }
                            else{
                                if($(this).find('option:contains("DC Item")').length > 0)
                                 $(".veryshort.StorageADD option[value='5']").remove();
                             }
                        });
                        
                       if(response[3] === '1'){
                            if( $(".veryshort.StorageADDF").find('option:contains("DC Item")').length <= 0)
                                    $(".veryshort.StorageADDF").append('<option value=5>DC Item</option>');
                             if($(".veryshort.StorageADDF option:selected").text() === "<...>"){
                                  $(".veryshort.StorageADDF").val(5);
                            }    
                        }
                        else{
                            if( $(".veryshort.StorageADDF").find('option:contains("DC Item")').length > 0)
                                $(".veryshort.StorageADDF option[value='5']").remove();
                        }                 
                    
                    $('#bee2').html(response[0]);
                     $('#bee2').removeClass("red green yellow blue");
                        if(response[0]===null || response[0]==="non-compliant"){
                             $('#bee2').addClass("red");
                        } 
                        else if(response[0]==="1" || response[0]==="2" || response[0]==="3"  || response[0]==="4"  || response[0]==="5" || response[0]==="N/A - International" ){
                              $('#bee2').addClass("green");
                        }

                       else if(response[0]==="Awaiting feedback"){
                         $('#bee2').addClass("blue");
                       }    
                        else {
                             $('#bee2').addClass("yellow");
                        }
                        
                        //update autocomplete
                        resetAutocomplete( response[1] );

                },

                error: function(xhr) {
                    console.log(xhr);
                }
            });   
            }
            else{
                $('#buildAuto').html('Select to access "Item Details"');
            }
        });
      
        $('#SupplierSelect').on('change', function() {
             var code = $(this).val();
             if(code!==""){
                $.ajax({
                url: "Handlers/InternalOrders_Handler.php", //refer to the file
                type: "GET", //send it through get method            
                data: { //arguments
                    function: 'getSuplierCode',
                    supplier_code: code
                },
                dataType: "json",
                success: function(response) { //if returns what must happen
                    console.log(response);
                    $('#bee').html(response[0]);
                     $('#bee').removeClass("red green yellow blue");
                    if(response[0]===null || response[0]==="non-compliant"){
                         $('#bee').addClass("red");
                    } 
                    else if(response[0]==="1" || response[0]==="2" || response[0]==="3" || response[0]==="4"  || response[0]==="5" || response[0]==="N/A - International" ){
                          $('#bee').addClass("green");
                    }

                    else if(response[0]==="Awaiting feedback"){
                         $('#bee').addClass("blue");
                     }
                    else {
                         $('#bee').addClass("yellow");
                    }
                    
                      //update autocomplete
                       $('#buildAuto').hide();
                       $('#Items_AddIternalOrder').removeClass('blurout');
                       resetAutocomplete(response[1] );
                       //$('#SupplierSelect_chzn').addClass('blurout');
                       
                       $(".veryshort.StorageADD").each(function() {
                            if(response[3] === '1'){
                                if($(this).find('option:contains("DC Item")').length <= 0)
                                    $(this).append('<option value=5>DC Item</option>');
                                if($(this).find(":selected").text() === "<...>"){
                                    console.log('onchange for add');
                                  $(this).val(5);
                                }   
                            }
                            else{
                                if($(this).find('option:contains("DC Item")').length > 0)
                                 $(".veryshort.StorageADD option[value='5']").remove();
                             }
                        });
                        
                       if(response[3] === '1'){
                            if( $(".veryshort.StorageADDF").find('option:contains("DC Item")').length <= 0)
                                    $(".veryshort.StorageADDF").append('<option value=5>DC Item</option>');  
                            if($(".veryshort.StorageADDF option:selected").text() === "<...>"){
                                  $(".veryshort.StorageADDF").val(5);
                            }
                        }
                        else{
                            if( $(".veryshort.StorageADDF").find('option:contains("DC Item")').length > 0)
                                $(".veryshort.StorageADDF option[value='5']").remove();
                        }                                                                
                },

                error: function(xhr) {
                    console.log(xhr);
                }
            });   
            }
        });
        
        
        ///////////////////////////////////MAKE VIEW INTERNAL ORDERS TABLE//////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        
       $('#allProjects').on('change', function(){
           if($('#allProjects').prop("checked")){
               $('#ViewMultiple_DatePickerRow').addClass('blurout');
           }
           else{
              $('#ViewMultiple_DatePickerRow').removeClass('blurout'); 
           }
       });
        
        
        $("#MakeList").on("click", function() {
          
          var Project =  $("select[name=Project]").val();
          var OrderStatus = $("select[name=Status]").val(); 
          var Supplier = $("select[name=Supplier]").val();
          var Category = $("select[name=Category]").val(); 
          var dateStart = $('#start_date').val();
          var dateEnd = $('#end_date').val();
          var Deficit = $('#deficit').prop("checked");
          var allProjectsOrders = $('#allProjects').prop("checked");  // all the orders for that project
          var NotDeliverd = $('#NotDeliverd').prop("checked");
          
         
          
          console.log("Value of NotDeliverd "+ NotDeliverd );
          var title = "";

          if(OrderStatus!==""){
              title += " - " +  $("select[name=Status]").find("option:selected").text();
          }
          
          if(Project!==""){
              title += " - " + $("select[name=Project]").find("option:selected").text();
          }
          if(Supplier!==""){
              title += " - " +  $("select[name=Supplier]").find("option:selected").text();
          }
            
             var p = '<div class="pager"><div style="height: 38px; text-align: center; float: right;">Number of Records: <div id="numberBoxContainer" style="display:inline-block;vertical-align:middle;"></div></div></div>'; 
             
             $('.content').html('<br/><br/><div id = "heading" class="left"><SPAN class="heading">View Internal Order List ' + title +'</SPAN><BR class="veryshort"/><HR></div>\
                    '+p+'<div id = "Loader" class="loader">Loading...</div><div id="gridContainerInternalOrderList"></div>\n\
                    <br/><div id ="lastLine"  class = "right">\n\
                    <INPUT id="reloadButton" name="Submit" class="button" type="button" value="Back" onclick = "reload()" />\n\
                  </div></div>');   
          
             ///////////////////////////////////////////////////////////// 
            $.ajax({
            url: "Handlers/InternalOrders_Handler.php", //refer to the file
            type: "GET", //send it through get method            

            data: { //arguments
                function: 'getMultipleOrderView',
                project:Project,
                orderStatus: OrderStatus, 
                supplier: Supplier, 
                dateS: dateStart,
                dateE: dateEnd,
                deficit: Deficit,
                NotDeliverd: NotDeliverd,
                Category: Category,
                allProjectsOrders: allProjectsOrders
                //create a date picker and deficit
            },
            dataType: "json",

            success: function(response) { //if returns what must happen
                listOfOrders = response; 
                console.log(response);
                try {         
  
                    $("#Loader").addClass("Hide");

                    $("#gridContainerInternalOrderList").dxDataGrid({
                    dataSource: {
                        store: {
                            type: "array",
                            key: "OrderNo_Number",
                            data: response
                        }
                    }, 
                    allowColumnResizing: true,
                   // columnAutoWidth: true,
                     loadPanel: {
                        height: 150,
                        width: 400,
                        text: 'Loading...'
                    },
                    height: "auto",
                     searchPanel: {
                        visible: true,
                        width: 250
                    },
                    filterRow: {
                        visible: true,
                        applyFilter: "auto"
                    },
                    headerFilter:{
                        visible: true
                    },
                    paging: { pageSize: 17 },
                    wordWrapEnabled: true,    
                    columns: [{
                            dataField: "OrderNo_Number",
                            caption: "Order Number"

                        },{
                            dataField: "Project"
                        },
                          {
                            dataField: "ReportsCategory_Name",
                            caption: "Category"
                          
                        },
                        {
                            dataField: "Supplier_Name",
                             caption: "Supplier"

                        }, 
                        {
                            dataField: "quotation_number",
                            caption: "Quotation Number"
                          
                        }, 
                        {
                            dataField: "OrderNo_Date_Time",
                             dataType: "date",
                             caption: "Date Placed"
                        }, 
                        {
                            dataField: "OrderNo_Date_FollowUp",
                             dataType: "date",
                             caption: "Follow Up Date"
                        },
                        {
                            dataField: "Requested_By",
                            caption: "Requested By"
                        }, 
                        {
                            dataField: "SupplierInvoice",
                             caption: "Supplier Invoice"
                        }, 
                        {
                            dataField: "OrderNo_Original_Total_Cost",
                            caption: "Original Total",
                            dataType:"number"
                        }, 
                        {
                            dataField: "total",
                            caption: "Invoice Total",
                            dataType:"number"
                        } ,{
                             caption: "Status",calculateDisplayValue: function(e){
                            if(e.OrderNo_Approved == 1){                 // was approved 
                                if(e.OrderNo_Complete == 2){
                                    return "Cancelled";
                                }
                                else if(e.OrderNo_Complete == 0){
                                        return "Incomplete";
                                }
                                else if(e.OrderNo_Complete == 1){
                                    return "Complete";
                                }
                                 else if(e.OrderNo_Complete == 3){
                                    return "Followed Up";
                                }
                                     
                            }
                            else if(e.OrderNo_Approved == 0){           // still pending
                                return "Pending";
                            }
                            else if(e.OrderNo_Approved == 2) {                              // denied
                                return "Order denied" ;
                            }
                            
                    }}         
                         
                    ],
                    onContentReady: function(e) {
                        var toolbar = e.element.find('.dx-datagrid-header-panel .dx-toolbar').dxToolbar('instance');
                        toolbar.on('optionChanged', function(arg) {
                           addCustomItem(toolbar);
                        });
                        addCustomItem(toolbar);
                        
                        $('#gridContainerInternalOrderList').closest('table').children('tr:first').css("background-color", "#295885");
                    },
                    masterDetail: {
                        enabled: true,
                        height: "auto",
                      
                        template: function(container, options) { 

                            var currentOrder = options.data;
                            container.addClass("internal-grid-container");
                            $("<div class='black'>").text("Items for: " + currentOrder.OrderNo_Number).appendTo(container);            
                            $("<div>")
                                .addClass("internal-grid")
                                .dxDataGrid({
                                    columnAutoWidth: true,
                                    allowColumnResizing: true,
                                      wordWrapEnabled: true,
                                    columns: [{
                                        dataField: "Items_Supplier_Code",
                                        caption: "Manufacturer Code"

                                    },{
                                      dataField: "Items_Description",
                                      caption: "Manufacturer Description"
                                    },{
                                      dataField: "Items_Backorder",
                                      calculateCellValue: function (rowData){
                                            if(rowData.Items_Backorder === 1)
                                            return "Yes";
                                            else 
                                                return  "No";
                                        }
                                    },{
                                      dataField: "Items_Value",                  
                                      caption: "Original Value (VAT excl.)"
                                    },{
                                     dataField: "Nett_Value",           
                                      caption: "Nett_Value (Discount Incl.)"
                                    },{
                                      dataField: "Latest_Value",                
                                      caption: "Invoice Value (Vat excl.)"
                                    },{
                                      dataField: "Items_Quantity",
                                      caption: "Quantity"
                                    },{
                                      dataField: "Items_Asset",
                                      calculateCellValue: function (rowData){
                                            if(rowData.Items_Backorder === 1)
                                            return "Yes";
                                            else 
                                                return  "No";
                                        }
                                    },{
                                      dataField: "Items_Received_Quantity",
                                      caption: "Invoice Capture"
                                    },{
                                      dataField: "Items_Storeman_Received",
                                      caption: "Storeman Received"
                                    },{
                                      dataField: "Items_StorageType",
                                      caption: "Storage Type"
                                    },{
                                      dataField: "Items_Discount",
                                      caption: "Discount %"  
                                    }
                                    ],
                                    dataSource: currentOrder.Items,
                                     onCellPrepared: function(e) { 

                                        if(e.rowType === 'data'){
                                            e.cellElement.addClass('size');
                                        }
                                        else{
                                               e.cellElement.addClass('white');
                                        } 

                                        if(e.rowType === 'data' && (e.column.dataField === 'Items_Value' || e.column.dataField === 'Nett_Value' || e.column.dataField === 'Latest_Value' )){
                                            if(e.text!=="")
                                              e.cellElement.text(options.data.Currency_Symbol +" "+ e.text);
                                         }

                                        if(e.rowType === 'data' && e.column.dataField === 'Items_Discount'){
                                             if(e.text!=="")
                                              e.cellElement.text(e.text + " %");
                                        }


                                   }
                                }).appendTo(container);
                        }

                    },
                    onCellPrepared: function(e) {   

                        if(e.rowType === 'data'){
                            e.cellElement.addClass('size');
                        }
                        else{
                               e.cellElement.addClass('white');
                        } 

                       if (e.rowType === 'data' && (e.column.dataField === 'OrderNo_Original_Total_Cost' || e.column.dataField === 'total')) {
                            if(e.text!=="")
                                e.cellElement.text(e.data.Currency_Symbol +" " + e.text);
                         }
                    }

                });

                  $("#numberBoxContainer").dxNumberBox({
                      value: 25,
                      step: 25,
                      showSpinButtons: true,
                      min: 25,
                      max: 1000,
                      height: 35,
                      width: 100,
                      onValueChanged: function (info) {
                          var dataGrid = $('#gridContainerInternalOrderList').dxDataGrid('instance');
                          dataGrid.pageSize(info.value);
                      }
                   });
               } 
              
                catch (e) {   
                    console.log(e);
                }

            },

            error: function(xhr) {
                console.log(xhr);
            }
        });   
            //////////////////////////////////////////////////////////////////
          });  
        
        ///////////////////////////////////MAKE  MARK MULTIPLE TABLE/////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
        if($('#MarkData').length){
          
             var Supplier = $('#MarkData').attr("data-StatusMark");
             var Status =  $('#MarkData').attr("data-SupplierMark");
             var startDate = $('#MarkData').attr("data-DateSMark");
             var endDate = $('#MarkData').attr("data-DateEMark");
             var complete = $('#MarkData').attr("data-PosComp");        // this is for view posible complete only 
          
            if(complete){
                  console.log('Get posible');
            }
            else{
                 console.log('Get normal');
             }
            
            $.ajax({
                url: "Handlers/InternalOrders_Handler.php", //refer to the file
                type: "GET", //send it through get method            
                data: { //arguments
                    function: 'getMultipleOrderView',
                    supplier: $('#MarkData').attr("data-SupplierMark"),
                    orderStatus: $('#MarkData').attr("data-StatusMark"),
                    dateS: $('#MarkData').attr("data-DateSMark"),
                    dateE: $('#MarkData').attr("data-DateEMark"),
                    project: '',
                    Category: '',
                    deficit: false,
                    
                    NotDeliverd: false
                },
                dataType: "json",
                success: function(response) { //if returns what must happen
                    console.log(response); 
                    $("#Loader").addClass("Hide");
                     $('#Mutiple_Mark_dxDataGrid').dxDataGrid({
                         dataSource: response,
                         editing: {
                             allowAdding: false,
                             allowDeleting: false,
                             allowUpdating: false,
                             mode: 'batch'
                         },
                         allowColumnResizing: true,
                         selection: {
                             allowSelectAll:true, 
                             mode: 'multiple',
                             selectAllMode: 'allPages',
                             showCheckBoxesMode: 'always'
                         },
                         filterRow: {
                            visible: true,
                            applyFilter: "auto"
                         },
                         searchPanel:{
                             visible: true
                         },
                           columns: [{
                            dataField: "OrderNo_Number",
                            caption: "Order Number",
                             allowEditing: false,
                             showCheckBoxesMode: 'always'

                        },{
                            dataField: "Project",allowEditing: false
                        }, 
                        {
                            dataField: "Supplier_Name",
                             caption: "Supplier",allowEditing: false

                        }, 
                        {
                            dataField: "quotation_number",
                            caption: "Quotation Number",allowEditing: false
                          
                        }, 
                        {
                            dataField: "OrderNo_Date_Time",
                             dataType: "date",
                             caption: "Date Placed",allowEditing: false
                        }, 
                         {
                            dataField: "OrderNo_Date_FollowUp",
                             dataType: "date",
                             caption: "Follow Up Date"
                        },                   
                        {
                            dataField: "Requested_By",
                            caption: "Requested By",allowEditing: false
                        }, 
                        {
                            dataField: "SupplierInvoice",
                             caption: "Supplier Invoice",allowEditing: false
                        }, 
                        {
                            dataField: "OrderNo_Original_Total_Cost",
                            caption: "Original Total",
                            dataType:"number",allowEditing: false
                        }, 
                        {
                            dataField: "total",
                            caption: "Invoice Total",
                            dataType:"number",allowEditing: false
                        } ,{
                             caption: "Status",allowEditing: false  ,calculateDisplayValue: function(e){
                            if(e.OrderNo_Approved == 1){                 // was approved 
                                if(e.OrderNo_Complete == 2){
                                    return "Cancelled";
                                }
                                else if(e.OrderNo_Complete == 0){
                                        return "Incomplete";
                                }
                                else if(e.OrderNo_Complete == 1){
                                    return "Complete";
                                }
                                 else if(e.OrderNo_Complete == 3){
                                    return "Followed Up";
                                }
                                     
                            }
                            else if(e.OrderNo_Approved == 0){ // still pending
                                return "Pending";
                            }
                            else if(e.OrderNo_Approved == 2) { // denied
                                return "Order denied" ;
                            }
                            
                    }}         
                         
                    ], masterDetail: {
                        enabled: true,
                        height: "auto",
                      
                        template: function(container, options) { 

                            var currentOrder = options.data;
                            container.addClass("internal-grid-container");
                            $("<div class='black'>").text("Items for: " + currentOrder.OrderNo_Number).appendTo(container);            
                            $("<div>")
                                .addClass("internal-grid")
                                .dxDataGrid({
                                    columnAutoWidth: true,
                                    allowColumnResizing: true,
                                      wordWrapEnabled: true,
                                    columns: [{
                                        dataField: "Items_Supplier_Code",
                                        caption: "Manufacturer Code",allowEditing: false

                                    },{
                                      dataField: "Items_Description",
                                      caption: "Manufacturer Description",allowEditing: false
                                    },{
                                      dataField: "Items_Backorder",allowEditing: false,
                                      calculateCellValue: function (rowData){
                                            if(rowData.Items_Backorder === 1)
                                            return "Yes";
                                            else 
                                                return  "No";
                                        }
                                    },{
                                      dataField: "Items_Value",                  
                                      caption: "Original Value (VAT excl.)",allowEditing: false
                                    },{
                                     dataField: "Nett_Value",           
                                      caption: "Nett_Value (Discount Incl.)",allowEditing: false
                                    },{
                                      dataField: "Latest_Value",                
                                      caption: "Invoice Value (Vat excl.)",allowEditing: false
                                    },{
                                      dataField: "Items_Quantity",
                                      caption: "Quantity",allowEditing: false
                                    },{
                                      dataField: "Items_Asset",allowEditing: false,
                                      calculateCellValue: function (rowData){
                                            if(rowData.Items_Backorder === 1)
                                            return "Yes";
                                            else 
                                                return  "No";
                                        }
                                    },{
                                      dataField: "Items_Received_Quantity",
                                      caption: "Invoice Capture",allowEditing: false
                                    },{
                                      dataField: "Items_Storeman_Received",
                                      caption: "Storeman Received",allowEditing: false
                                    },{
                                      dataField: "Items_StorageType",
                                      caption: "Storage Type",allowEditing: false
                                    },{
                                      dataField: "Items_Discount",
                                      caption: "Discount %"  ,allowEditing: false
                                    }
                                    ],
                                    dataSource: currentOrder.Items,
                                     onCellPrepared: function(e) { 

                                        if(e.rowType === 'data'){
                                            e.cellElement.addClass('size');
                                        }
                                        else{
                                               e.cellElement.addClass('white');
                                        } 

                                        if(e.rowType === 'data' && (e.column.dataField === 'Items_Value' || e.column.dataField === 'Nett_Value' || e.column.dataField === 'Latest_Value' )){
                                            if(e.text!=="")
                                              e.cellElement.text(options.data.Currency_Symbol +" "+ e.text);
                                         }

                                        if(e.rowType === 'data' && e.column.dataField === 'Items_Discount'){
                                             if(e.text!=="")
                                              e.cellElement.text(e.text + " %");
                                        }


                                   }
                                }).appendTo(container);
                        }

                    }, 
                    onContentReady: function(e) {
                        var toolbar = e.element.find('.dx-datagrid-header-panel .dx-toolbar').dxToolbar('instance');
                        toolbar.on('optionChanged', function(arg) {
                           addCustomItemMarkAll(toolbar);
                        });
                        addCustomItemMarkAll(toolbar);
                        
                        if(lableMark){
                            e.element.find('.dx-datagrid-header-panel .dx-toolbar').find('.dx-toolbar-before').prepend("<div id='lableMark'>Mark Selected As:</div>");
                            lableMark = false;
                        }
                    },
                    onCellPrepared: function(e) {   

                        if(e.rowType === 'data'){
                            e.cellElement.addClass('size');
                        }
                        else{
                               e.cellElement.addClass('white');
                        } 

                       if (e.rowType === 'data' && (e.column.dataField === 'OrderNo_Original_Total_Cost' || e.column.dataField === 'total')) {
                            if(e.text!=="")
                                e.cellElement.text(e.data.Currency_Symbol +" " + e.text);
                         }
                    }
                        
            }); 
            
            
                $("#numberBoxContainer").dxNumberBox({
                      value: 25,
                      step: 25,
                      showSpinButtons: true,
                      min: 25,
                      max: 1000,
                      height: 35,
                      width: 100,
                      onValueChanged: function (info) {
                          var dataGrid = $('#Mutiple_Mark_dxDataGrid').dxDataGrid('instance');
                          dataGrid.pageSize(info.value);
                      }
                   });
            
                },
                error: function(xhr) {
                    console.log(xhr);
                }
            });   

        }
        
         ///////////////////////////////////Email errors on view single/////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
        $('#ShowReportError').on('click', function(){ 
           $('#ERRORBOX').show();
           $(".seltest3").chosen({
			no_results_text: "No results matched"
            });  
        });
        
        $('#SendErrorEmail').on('click',function(){
            
            var message =  $('#box').val();
              $.ajax({
                         url: "Handlers/InternalOrders_Handler.php", //refer to the file
                         type: "GET", //send it through get method            
                         data: { //arguments
                             function: 'SendErrorEmail',
                             Message: message,
                             StaffCode: $('#contacts').val()      
                         },
                       
                         success: function(response) { //if returns what must happen
                             console.log(response);
                             location.reload();
                         },
                         error: function(xhr) {
                             console.log(xhr);
                         }
                     });   
        });  
 
    });



    
    function OpenUncontolledPopup(Mode){
       
         var allTypeSelected = true;
        $(".veryshort.StorageADD" ).each(function() {
               if($(this).val()===""){
                 allTypeSelected = false
               }      
         });


        
        if(allTypeSelected){
            $.ajax({
            url: "Handlers/InternalOrders_Handler.php", //refer to the file
            type: "GET", //send it through get method            
            data: { //arguments
                function: 'docheck',
                docheck: Mode,

            },
            success: function(response) {
                
                console.log(response + "____");
                
                if((response !== '<h4>There are no matches on the controlled goods sections.</h4>') && (response !== '<h4>Item(s) do not match any items on the database that have been ordered for this project.</h4><br/>')){
                    console.log(response);
                    $('#coverAll').removeClass('Hide');
                    $('#UncontroledGoodsItems').removeClass('Hide');
                    $('#items').html(response);
                }
                else{
                   $('#sendEditRequest').trigger("click");
                 //  $('#items').html(response);
                }
            },
            error: function(xhr) {
                console.log(xhr);
            }
        });   
        }
        
        else{
            alert('Some Items does not have a Storage Type selected. Please select a Storage Type and click update.');
            
        }
    }
    
    function GenerateDocument(verb, url, data, target) {
        var form = document.createElement("form");
        form.action = url;
        form.method = verb;
        form.target = target || "_self";
        if (data) {
          for (var key in data) {
            var input = document.createElement("textarea");
            input.name = key;
            input.value = typeof data[key] === "object" ? JSON.stringify(data[key]) : data[key];
            form.appendChild(input);
          }
        }
        form.style.display = "none";
        document.body.appendChild(form);
        form.submit();

      };
      
    function exportfile(){
        var FileN = 'Internal_Orders';
        GenerateDocument('POST', 'Handlers/ExcelExport.php', { SQlArray: JSON.stringify(listOfOrders), FileName: FileN }, '_blank'); 
    }  
       
    function reload(){
         location.reload();
    }
    
    function addCustomItem(toolbar) {
      var items = toolbar.option('items');
      var myItem = DevExpress.data.query(items).filter(function(item) {
        return item.name === 'myButton';
      }).toArray();
      if (!myItem.length) {
        items.unshift( {
          location: 'after',
          widget: 'dxButton',
          name: 'myButton',
          options: {
            icon:"export",
            text: "export",
            hint: "export records",
            onClick: function(e) {
             exportfile();
            }
          }
        },{
          location: 'after',
          widget: 'dxButton',
          name: 'myButton',
          options: {
            icon:"chevrondown",
            hint: "expand all",
            text: "expand all",
            onClick: function(e) {
                var dataGrid = $('#gridContainerInternalOrderList').dxDataGrid('instance');
                if(expanded){
                    dataGrid.collapseAll(-1);
                    e.component.option("icon", "chevrondown");
                    e.component.option("text", "expand all");
                    expanded = false;
                }
                else{
                    dataGrid.expandAll(-1);
                    e.component.option("icon", "chevronup");
                    e.component.option("text", "collapse all");
                    expanded = true;
               }
            }
          }
        });
        toolbar.option('items', items);
      }
    }
    
    
   function addCustomItemMarkAll(toolbar) {
      
      var markStatus = [
        { id: 0, status: "Incomplete" },
        { id: 1, status: "Complete" },
        { id: 2, status: "Cancelled"}, 
        { id: 3, status: "Followed"}
        ];
      
       //var markStatus = [{"StatusVal" : 0, "Status" : "Incomplete"},{"StatusVal" : 1, "Status" : "Complete"},{"StatusVal" : 2, "Status" : "Cancelled"},{"StatusVal" : 3, "Status" : "Followed Up"}];     
      var items = toolbar.option('items');
      var myItem = DevExpress.data.query(items).filter(function(item) {
        return item.name === 'myButton';
      }).toArray();
      if (!myItem.length) {
        items.unshift({
          location: 'after',
          widget: 'dxButton',
          name: 'myButton',
          options: {
            icon:"chevrondown",
            hint: "expand all",
            text: "expand all",
            onClick: function(e) {
                var dataGrid = $('#Mutiple_Mark_dxDataGrid').dxDataGrid('instance');
                if(expanded){
                    dataGrid.collapseAll(-1);
                    e.component.option("icon", "chevrondown");
                    e.component.option("text", "expand all");
                    expanded = false;
                }
                else{
                    dataGrid.expandAll(-1);
                    e.component.option("icon", "chevronup");
                    e.component.option("text", "collapse all");
                    expanded = true;
               }
            }
          }
        }, {
          location: 'before',
          widget: 'dxSelectBox',
          name: 'myButton',
          options: {
            dataSource: markStatus, 
            valueExpr: 'id',
            displayExpr: 'status',
            hint: "Mark Selected",
            onItemClick: function(e){
              
                var dataGrid = $('#Mutiple_Mark_dxDataGrid').dxDataGrid('instance');
                var List = dataGrid.getSelectedRowsData();
                var orders = [];
                List.forEach(function(order){
                   orders.push(order['OrderNo_Number']);
                });
               console.log(orders);     
             
                if(orders.length > 0){
                    $.ajax({
                         url: "Handlers/InternalOrders_Handler.php", //refer to the file
                         type: "GET", //send it through get method            
                         data: { //arguments
                             function: 'MarkOrderList',
                             OrdernumberList: orders,
                             Status: e.itemData.id      
                         },
                         dataType: "json",
                         success: function(response) { //if returns what must happen
                             console.log(response)
                             if(response[0]==="PASS"){
                                location.reload();
                             }
                             else{
                                alert('The following orders failed to update: ' + response[1]);
                                location.reload();
                             }
                         },
                         error: function(xhr) {
                             console.log(xhr);
                         }
                     });   
                }
            }
          }
        });
        toolbar.option('items', items);
      }
    }
   
   function resetAutocomplete(List){
        $(".autocomplete").each(function() {
           $(this).dxAutocomplete('instance').option('dataSource', List ); 
        });
   }
   
   
  
