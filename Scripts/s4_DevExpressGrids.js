/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function FilterGrid(s4_gridID,s4_data,prep,currentID,s4_columns)
  {
                 $("#"+s4_gridID).dxDataGrid({
                     dataSource: s4_data,
                     rowAlternationEnabled: true,
                     paging: {
                            pageSize: 5
                        },
                     filterRow: {
                           visible: true,
                           applyFilter: "auto"
                      },
                     searchPanel: {
                         visible: true,
                         width: 240,
                         placeholder: "Search..."
                     },
                     columns: s4_columns,
        rowPrepared: function (rowElement, rowInfo) {
            if(prep)
            {
               if (rowInfo.data.OvertimePreApp_ID == currentID)
                  rowElement.css("background", "yellow");
            }
        }  
,
                   }).dxDataGrid("instance");

                    var applyFilterTypes = [{
                        key: "auto",
                        name: "Immediately"
                    }, {
                        key: "onClick",
                        name: "On Button Click"
                    }]

                    $("#useFilterApplyButton").dxSelectBox({
                        items: applyFilterTypes,
                        value: applyFilterTypes[0].key,
                        valueExpr: "key",
                        displayExpr: "name",
                        onValueChanged: function(data) {
                            dataGrid.option("filterRow.applyFilter", data.value);
                        }
                    });
                    
                    //alert('#'+s4_gridID+' .dx-datagrid-content .dx-datagrid-table tr:visible:last');
      $('.dx-datagrid-rowsview .dx-datagrid-content .dx-datagrid-table tr:not(.dx-freespace-row):last').css('border-bottom', ' #0B3162 1px solid');
      //$('#'+s4_gridID+' .dx-datagrid-content .dx-datagrid-table tr').filter(':visible').last().css('border-bottom', ' #0B3162 1px solid');
      console.log($('.dx-datagrid-rowsview .dx-datagrid-content .dx-datagrid-table tr:not(.dx-freespace-row):last'));
  } 
  
function GetAjaxData(paUrl, paType, paData)
    {
        var output = new Object({});
        //$("#blocker").show();
        var loAjax = $.ajax(
                    {
                        url: paUrl,
                        type:paType,
                        data: paData,
                        dataType:'json',
                        async:false
                    });

        loAjax.done(
            function(data)
            {
                //$("#blocker").fadeOut(400);
                output = data;
            }
        );

        loAjax.fail(
            function(jqXHR, text)
            {
                //$("#blocker").fadeOut(400);
                output = text;
            }
        );

        return output;
    }
  
