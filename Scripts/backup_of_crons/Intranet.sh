################################################################################
#This shell script is used to create links to all Purchase Orders, Quotes and 
#RFQs so that they can be used by the intranet.
#Added s4i and s4a quotes dir, new ibay for the quotes to be used, just for the current quotes.
#Last Modified by Gerrit Ekkerd 21-Aug-2015.
################################################################################
archiveDir="/home/e-smith/files/ibays/s4_admin/files"
currentDir="/home/e-smith/files/ibays/s4_current/files/Current"
intranetDir="/home/e-smith/files/ibays/intranew/html/Files/Links"
archiveDirMM="/home/e-smith/files/ibays/mm_admin/files"
currentDirMM="/home/e-smith/files/ibays/mm_current/files/Current"
intranetDirMM="/home/e-smith/files/ibays/s4autonet/html/Files/Links"
s4iQuotesDir="/home/e-smith/files/ibays/quotes/files/S4I"
s4aQuotesDir="/home/e-smith/files/ibays/quotes/files/S4A"
################################################################################
# Create links to all stored files.
################################################################################
#All archived files. S4
echo "Linking archived files..."
Y=$(date +%Y)
for ((i=2004; $i<=($Y-1); i++)); do
  for j in ${archiveDir}/${i}/Current/POs/*; do
    if [ -f "${j}" ]; then
      echo "Linking $j"
      ln -f "${j}" "${intranetDir}/POs/"
    fi
  done
  for j in ${archiveDir}/${i}/Current/Quotes/*; do
    if [ -f "${j}" ]; then
      echo "Linking $j"
       ln -f "${j}" "${intranetDir}/Quotes/"
    fi
  done
  for j in ${archiveDir}/${i}/Current/RFQs/*; do
    if [ -f "${j}" ]; then
      echo "Linking $j"
      ln -f "${j}" "${intranetDir}/RFQs/"
    fi
  done
done
#All current files. S4
echo "Linking current files..."
for i in $currentDir/POs/*; do
  if [ -f "${i}" ]; then
    echo "Linking $i"
    ln -f "${i}" "${intranetDir}/POs/"
  fi
done
for i in $s4iQuotesDir/*; do
  if [ -f "${i}" ]; then
    echo "Linking $i"
    ln -f "${i}" "${intranetDir}/Quotes/"
  fi
done
for i in $currentDir/RFQs/*; do
  if [ -f "${i}" ]; then
    echo "Linking $i"
    ln -f "${i}" "${intranetDir}/RFQs/"
  fi
done

#All archived files. Mosmech
echo "Linking archived files..."
Y=$(date +%Y)
for ((i=2004; $i<=($Y-1); i++)); do
  for j in ${archiveDirMM}/${i}/Current/POs/*; do
    if [ -f "${j}" ]; then
      echo "Linking $j"
      ln -f "${j}" "${intranetDirMM}/POs/"
    fi
  done
  for j in ${archiveDirMM}/${i}/Current/Quotes/*; do
    if [ -f "${j}" ]; then
      echo "Linking $j"
       ln -f "${j}" "${intranetDirMM}/Quotes/"
    fi
  done
  for j in ${archiveDirMM}/${i}/Current/RFQs/*; do
    if [ -f "${j}" ]; then
      echo "Linking $j"
      ln -f "${j}" "${intranetDirMM}/RFQs/"
    fi
  done
done
#All current files. Mosmech
echo "Linking current files..."
for i in $currentDirMM/POs/*; do
  if [ -f "${i}" ]; then
    echo "Linking $i"
    ln -f "${i}" "${intranetDirMM}/POs/"
  fi
done
for i in $s4aQuotesDir/*; do
  if [ -f "${i}" ]; then
    echo "Linking $i"
    ln -f "${i}" "${intranetDirMM}/Quotes/"
  fi
done
for i in $currentDirMM/RFQs/*; do
  if [ -f "${i}" ]; then
    echo "Linking $i"
    ln -f "${i}" "${intranetDirMM}/RFQs/"
  fi
done