<?php
  // DETAILS ///////////////////////////////////////////////////////////////////
  //                                                                          //
  //                    Last Edited By: Gareth Ambrose                        //
  //                        Date: 05 December 2007                            //
  //                                                                          //
  ////////////////////////////////////////////////////////////////////////////// 
  // This scripting page allows for resizing images and storing them. It also //
  // provides functions for dislpaying images.                                //
  //////////////////////////////////////////////////////////////////////////////
  
  include 'Graphing_New.php';
  
  if ($_GET['image'] != "")
  {
    switch ($_GET['type'])
    {
      case 'part':
        DisplayPart($_GET['image']);  
        break;
      default:
        break;
    }
  }  
  
  //////////////////////////////////////////////////////////////////////////////
  // Creates a resized copy of an image.                                      //
  //////////////////////////////////////////////////////////////////////////////
  function ResizeWithMaximumValues($filenameOld, $filenameNew, $maxWidth, $maxHeight, $keepAspectRatio = true)                                                         
  {
    //Get the dimensions of the original image.
    $imageInfo = GetImageSize($filenameOld);
    
    $currentWidth = $imageInfo[0];
    $currentHeight = $imageInfo[1];
    
    //Calculate the ratio of the dimensions with respect to the maximum image size.
    $widthRatio = $maxWidth/$currentWidth;
    $heightRatio = $maxHeight/$currentHeight;
    
    //Calculate the dimensions of the new image.
    if ($keepAspectRatio)
    {
      $newRatio = $widthRatio > $heightRatio ? $heightRatio : $widthRatio;
      $newWidth = $currentWidth * $newRatio;
      $newHeight = $currentHeight * $newRatio;
    } else
    {
      $newWidth = $maxWidth;
      $newHeight = $maxHeight;
    }
    
    //Create a copy of the old image based on the type of image.
    $info = PathInfo($filenameOld);
    switch (StrToLower($info['extension']))
    {
      case 'bmp':
        $oldImage = ImageCreateFromBMP($filenameOld);
        break;
      case 'gif':
        $oldImage = ImageCreateFromGIF($filenameOld);
        break;
      case 'jpg':
      case 'jpeg':
        $oldImage = ImageCreateFromJPEG($filenameOld);
        break;
      case 'png':
        $oldImage = ImageCreateFromPNG($filenameOld);
        break;
      default:
        break;
    }
    
    //Create the new image and copy the old one. The new image uses maximum colour depth and resampling to be as accurate as possible.
    $newImage = ImageCreateTrueColor($newWidth, $newHeight);
    ImageCopyResampled($newImage, $oldImage, 0, 0, 0, 0, $newWidth, $newHeight, $currentWidth, $currentHeight);
    
    //Write the image file.
    switch (StrToLower($info['extension']))
    {
      case 'bmp':
        ImageBMP($newImage, $filenameNew);
        break;
      case 'gif':
        ImageGIF($newImage, $filenameNew);
        break;
      case 'jpg':
      case 'jpeg':
        ImageJPEG($newImage, $filenameNew);
        break;
      case 'png':
        ImagePNG($newImage, $filenameNew);
        break;
      default:
        break;
    }
    
    //Destroy the new and old images in memory.
    ImageDestroy($oldImage);
    ImageDestroy($newImage);
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Display the image for a part number.                                     //
  //////////////////////////////////////////////////////////////////////////////
  function DisplayPart($PartNumberID)
  {
    $row = MySQL_Fetch_Array(ExecuteQuery('SELECT Image FROM Part_Number WHERE Part_Number = "'.$PartNumberID.'"', 'S4DSA'));
    Header('Content-Type: image/jpeg');
    echo $row['Image'];
  }
?>