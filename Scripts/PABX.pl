#!/usr/bin/perl
################################################################################
# DEVELOPED BY:   Gareth Ambrose
# LAST UPDATED:   18 February 2010
# DESCRIPTION:    Receives info from the PABX system and stores it in the database
################################################################################
use strict;
use warnings;

use Device::SerialPort;
use DBI;
use Scalar::Util qw(looks_like_number);


################################################################################


#Communication constants.
use constant PORT   => "/dev/ttyS0";              #Serial port.  
use constant BAUD   => 9600;                      #BAUD Rate.
use constant PRTY   => "none";                    #Parity.
use constant DBTS   => 8;                         #Data bits.
use constant SBTS   => 1;                         #Stop bits.
use constant FLOW   => "none";                    #Flow control/handshake.

use constant MSGLEN => 82;                        #PABX message length.

my $Log               = "/var/log/PABX.log";      #System log file.
my $LOGG              = 0;                        #Whether to log to file or not.


################################################################################


#Check which arguments have been passed. GALCListener [Debug]
#   Debug - Y|y to enable logging.
#Example: GALCListener.pl y
#Example: GALCListener.pl Y
if (exists($ARGV[0]))
{
  $LOGG = lc($ARGV[0]) eq "y";
}

#Handles serial communication.
while (1)
{
  my $loSerial = new Device::SerialPort (PORT);
  
  #Configure the port.
  $loSerial->baudrate(BAUD)   || (Log("ERROR - ERR06 Failed setting baud rate.")    && die("ERROR - ERR06 Failed setting baud rate."));
  $loSerial->parity(PRTY)     || (Log("ERROR - ERR05 Failed setting parity.")       && die("ERROR - ERR05 Failed setting parity."));
  $loSerial->databits(DBTS)   || (Log("ERROR - ERR04 Failed setting data bits.")    && die("ERROR - ERR04 Failed setting data bits."));
  $loSerial->stopbits(SBTS)   || (Log("ERROR - ERR03 Failed setting stop bits.")    && die("ERROR - ERR03 Failed setting stop bits."));
  $loSerial->handshake(FLOW)  || (Log("ERROR - ERR02 Failed setting flow control.") && die("ERROR - ERR02 Failed setting flow control."));
  
  if (open(DEV, "< ".PORT.""))
  {
    my $loInfo = "";
    
    #Read incoming data.
    while($_ = <DEV>)
    {
      $loInfo .= $_;
      
      #####Log('Current: '.$loInfo.'');
      while (!((substr($loInfo, 0, 1) eq chr(18)) && (substr($loInfo, 1, 1) eq chr(32))) && (length($loInfo) > 0))
      {
        #####Log('Skipping: '.substr($loInfo, 0, 1).'');
        $loInfo = substr($loInfo, 1);
      }

        while (length($loInfo) >= MSGLEN)
        {      
          #####Log('Current: '.$loInfo.'');
          if ((substr($loInfo, 0, 1) eq chr(18)) && (substr($loInfo, 1, 1) eq chr(32)))
          {
            #####Log('Processing: '.substr($loInfo, 0, MSGLEN - 2).'');

            #Process the info and store it in the database.
            Process(substr($loInfo, 0, MSGLEN));
            
            $loInfo = substr($loInfo, MSGLEN);
          } else
          {
            ######Log('Skipping: '.substr($loInfo, 0, 1).'');
            $loInfo = substr($loInfo, 1);
          }
        }
    }
  } else
  {
    Log("ERROR - ERR01 Can't open ".PORT.": $!") && die("ERROR - ERR01 Can't open ".PORT.": $!");
  }
  
  undef $loSerial;
}

#Process the data and dump it in the database.
sub Process
{
  my $paInfo = shift;
  
  my $loConnection;
  if ($loConnection = DBI->connect('DBI:mysql:S4Admin:localhost', 's4mysql', 'mysql'))
  {
    my $loExtension = Trim(substr($paInfo, 2, 5));
    my $loRoute = looks_like_number(substr($paInfo, 7, 5)) ? int(substr($paInfo, 7, 5)) : substr($paInfo, 7, 5);
    my $loNumber = Trim(substr($paInfo, 14, 22));
    my $loAccount = looks_like_number(substr($paInfo, 35, 10)) ? int(substr($paInfo, 35, 10)) : substr($paInfo, 35, 10);
    my $loStart = substr($paInfo, 46, 5);
    my $loDurationMinutes = int(substr($paInfo, 55, 2));
    my $loDurationSeconds = int(substr($paInfo, 58, 2));
    my $loPulses = looks_like_number(substr($paInfo, 61, 5)) ? int(substr($paInfo, 61, 5)) : substr($paInfo, 61, 5);
    my $loCharge = int(substr($paInfo, 66, 5));
    my $loTrunk = int(substr($paInfo, 77, 2));
    
    my $loDuration = ($loDurationMinutes * 60) + $loDurationSeconds;
    
    $loConnection->do('INSERT INTO PABX VALUES ("", "'.$loExtension.'", "'.$loRoute.'", "'.$loNumber.'", "'.$loAccount.'", CONCAT(CURDATE(), " '.$loStart.':00"), "'.$loDuration.'", "'.$loPulses.'", "'.$loCharge.'", "'.$loTrunk.'")');
    
    $loConnection->disconnect;
  } else
  {
    Log("Couldn't connect to database: " . DBI->errstr);
  }
}

sub Log
{
  if ($LOGG)
  {
    my $paText = shift;
    
    my @loT = localtime();
    my $loLogTime = sprintf("%02d", $loT[3])."/".sprintf("%02d", 1+$loT[4])."/".(1900+$loT[5])." ".sprintf("%02d", $loT[2]).":".sprintf("%02d", $loT[1]).":".sprintf("%02d", $loT[0])."";
    
    open FILE, ">> $Log";
    print FILE "$loLogTime - $paText\n";
    close FILE;
  }
}

sub Trim
{
  my $loString = shift;
  $loString =~ s/^\s+//;
  $loString =~ s/\s+$//;
  return $loString;
}