<?php
  // DETAILS ///////////////////////////////////////////////////////////////////
  //                                                                          //
  //                    Last Edited By: Gareth Ambrose                        //
  //                        Date: 05 December 2007                            //
  //                                                                          //
  //////////////////////////////////////////////////////////////////////////////
  // This scripting page provides functionality for setting up and            //
  // controlling caching and sessions.                                        //
  //////////////////////////////////////////////////////////////////////////////
  
  
  //////////////////////////////////////////////////////////////////////////////
  // Sets the caching properties of the page header.                          //
  //////////////////////////////////////////////////////////////////////////////
  function SetCache()
  {
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Starts a new session.                                                    //
  //////////////////////////////////////////////////////////////////////////////
  function SetSession()
  {
    $currentParams = session_get_cookie_params();
    session_set_cookie_params($currentParams['lifetime'], '/cobotsweb/', $_SERVER['REMOTE_HOST'], $currentParams['secure'], $currentParams['httponly']);
    Session_Start();
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Initializes all the caching and session settings.                        //
  //////////////////////////////////////////////////////////////////////////////
  function SetSettings()
  {
    SetSession();
    SetCache();
  }
  
  function getS4ExternalLink()
  {
      return "http://portal.s4.co.za";
  }
?>