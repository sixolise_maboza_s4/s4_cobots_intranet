$(document).ready(
    function()
    {      
        var d = new Date();
        var m = ("0" + (d.getMonth() + 1)).slice(-2);
        var y = d.getFullYear();
        
        $("tr").click(function(){
            $('tr').css('background-color','white');
            var series = $(this).attr('series');
            $('tr[series=' + series + ']').css('background-color','lightblue'); 
            
        });
        
        
        
    $("form").submit(function(event)
    {
        event.stopPropagation();
        if (confirm("Are you sure?"))
            return true;
        return false;
    });   
    
    
    //AJAX START
    // variable to hold request
    var request;
    // bind to the submit event of our form
    $(".ajax").click(function(event){
        if ($(this).attr('dateDir') == "prev")
        {
            var mo = parseInt(m);
            mo--;
            if (mo < 1)
                {
                    y = y - 1;
                    mo = '12';
                }
            m = ("0" + mo).slice(-2);
        }
        if ($(this).attr('dateDir') == "next")
        {
            var mo = parseInt(m);
            mo++;
            if (mo > 12)
                {
                    y = y+1;
                    mo = 1;
                }
            m = ("0" + mo).slice(-2);
        }
        if ($(this).attr('dateDir') == "curr")
        {
            m = ("0" + (d.getMonth() + 1)).slice(-2);
            y = d.getFullYear();
        }
        
        // abort any pending request
        if (request) {
            request.abort();
        }
        
        // fire off the request to /form.php
        var request = $.ajax({
            url: "Scripts/Boardroom/calendar.php",
            type: "get",
            data: "month="+m+"&year="+y
        });

        // callback handler that will be called on success
        request.done(function (response, textStatus, jqXHR){
            // log a message to the console
            console.log(response);
            $('#contentDiv').html(response);
            $('#headingplaceholder').html("");
            $('#calendarheading').appendTo($('#headingplaceholder'));
            
            
            $("#calendarDiv").click(function(){
                $('#bookDiv').slideUp();
                $('#editDiv').slideUp();

            });
        });

        // callback handler that will be called on failure
        request.fail(function (jqXHR, textStatus, errorThrown){
            // log the error to the console
            console.error(
                "The following error occured: "+
                textStatus, errorThrown
            );
        });

        // callback handler that will be called regardless
        // if the request failed or succeeded
        request.always(function () {
            // reenable the inputs
            $(".addlink").click(function(event)
            {    
		  $('input[name=date_start][group=add]').val($(this).attr('id') + " 07:00:00");
                $('input[name=date_end][group=add]').val($(this).attr('id') + " 08:00:00");
                $("#editDiv").slideUp(function(){$('#bookDiv').slideDown();});
                
                event.stopPropagation();
            });
            
            $(".viewlink").click(function(e){
                
                
                e.stopPropagation();
                //Now call some external form php script to edit the date.
                
                var editRequest;
                
                if (editRequest) {
                    editRequest.abort();
                }
        
                // fire off the request to /form.php
                var editRequest = $.ajax({
                    url: "Scripts/Boardroom/edit_booking.php",
                    type: "get",
                    data: "id="+$(this).attr('id')
                });
                
                editRequest.done(function (response, textStatus, jqXHR){
                    // log a message to the console
                    console.log(response);
                    $('#editDiv').html(response);
                    $('#bookDiv').slideUp(function(){$('#editDiv').slideDown();});
                    $("form").submit(function(event)
                    {
                        event.stopPropagation();
                        if (confirm("Are you sure?"))
                            return true;
                        return false;
                    });
                    
                    // $('input[name=date_start]').datetimepicker( {
                    // hourMin: 7,
                    // hourMax: 23,
                    // showOn: 'button',
                    // buttonImage: 'Stylesheets/calendar-xxl.png',
                    // buttonImageOnly: true,
                    // defaultDate: '+1w',
                    // changeMonth: true,
                    // numberOfMonths: 1,
                    // dateFormat: 'yy-mm-dd',
                    // onSelect: function(){
                    //     var group = $(this).attr('group');
                    //     var endDateTextBox = $("input[name=date_end][group="+group+"]");
                    //     endDateTextBox.datetimepicker('option', 'maxDate', $(this).val());
                    // },
                    // onClose: function(dateText, inst) {
                    //     var startDateTextBox = $(this);
                    //     var group = $(this).attr('group');
                    //     var endDateTextBox = $("input[name=date_end][group="+group+"]");
                    //
                    //     if (endDateTextBox.val() != '') {
                    //             var testStartDate = startDateTextBox.datetimepicker('getDate');
                    //             var testEndDate = endDateTextBox.datetimepicker('getDate');
                    //             if (testStartDate > testEndDate)
                    //                     endDateTextBox.datetimepicker('setDate', testStartDate);
                    //     }
                    //     else {
                    //             endDateTextBox.val(dateText);
                    //     }
                    // }
                    //
                    // });
                
        // $('input[name=date_end]').datetimepicker( {
        //     hourMin: 7,
        //     hourMax: 23,
        //     defaultDate: '+1w',
        //     showOn: 'button',
        //     buttonImage: 'Stylesheets/calendar-xxl.png',
        //     buttonImageOnly: true,
        //     changeMonth: true,
        //     numberOfMonths: 1,
        //     dateFormat: 'yy-mm-dd',
        //     onClose: function(event, dateText, inst) {
        //         var endDateTextBox = $(this);
        //         var group = $(this).attr('group');
        //         var startDateTextBox = $("input[name=date_start][group="+group+"]");
        //
        //         if (startDateTextBox.val() != '') {
        //                 var testStartDate = startDateTextBox.datetimepicker('getDate');
        //                 var testEndDate = endDateTextBox.datetimepicker('getDate');
        //                 if (testStartDate > testEndDate)
        //                         startDateTextBox.datetimepicker('setDate', testEndDate);
        //         }
        //         else {
        //                 startDateTextBox.val(dateText);
        //         }
        //     }
        //     });//end second datetimepicker
                });
                
                editRequest.always(function(){
                $("#calendarDiv").click(function(){
                    $('#bookDiv').slideUp();
                    $('#editDiv').slideUp();
                });    
                    
                });
            });
        });

        // prevent default posting of form
        event.preventDefault();
    });//end click handler
    //
    $('#bookDiv').click(function(event){
        event.stopPropagation();
    });
    
    // $('input[name=date_start]').datetimepicker( {
    //                 hourMin: 7,
    //                 hourMax: 23,
    //                 showOn: 'button',
    //                 buttonImage: 'Stylesheets/calendar-xxl.png',
    //                 buttonImageOnly: true,
    //                 defaultDate: '+1w',
    //                 changeMonth: true,
    //                 numberOfMonths: 1,
    //                 dateFormat: 'yy-mm-dd',
    //                 onSelect: function(){
    //                     var group = $(this).attr('group');
    //                     var endDateTextBox = $("input[name=date_end][group="+group+"]");
    //                     endDateTextBox.datetimepicker('option', 'maxDate', $(this).val());
    //                 },
    //                 onClose: function(dateText, inst) {
    //                     var startDateTextBox = $(this);
    //                     var group = $(this).attr('group');
    //                     var endDateTextBox = $("input[name=date_end][group="+group+"]");
    //
    //                     if (endDateTextBox.val() != '') {
    //                             var testStartDate = startDateTextBox.datetimepicker('getDate');
    //                             var testEndDate = endDateTextBox.datetimepicker('getDate');
    //                             if (testStartDate > testEndDate)
    //                                     endDateTextBox.datetimepicker('setDate', testStartDate);
    //                     }
    //                     else {
    //                             endDateTextBox.val(dateText);
    //                     }
    //                 }
    //
    //                 });
                
        // $('input[name=date_end]').datetimepicker( {
        //     hourMin: 7,
        //     hourMax: 23,
        //     defaultDate: '+1w',
        //     showOn: 'button',
        //     buttonImage: 'Stylesheets/calendar-xxl.png',
        //     buttonImageOnly: true,
        //     changeMonth: true,
        //     numberOfMonths: 1,
        //     dateFormat: 'yy-mm-dd',
        //     onClose: function(event, dateText, inst) {
        //         var endDateTextBox = $(this);
        //         var group = $(this).attr('group');
        //         var startDateTextBox = $("input[name=date_start][group="+group+"]");
        //
        //         if (startDateTextBox.val() != '') {
        //                 var testStartDate = startDateTextBox.datetimepicker('getDate');
        //                 var testEndDate = endDateTextBox.datetimepicker('getDate');
        //                 if (testStartDate > testEndDate)
        //                         startDateTextBox.datetimepicker('setDate', testEndDate);
        //         }
        //         else {
        //                 startDateTextBox.val(dateText);
        //         }
        //     }
        //     });//end second datetimepicker
    
    $('#editDiv').click(function(event){
        event.stopPropagation();
    });
    //AJAX END
    $("span[dateDir=curr]").trigger('click');
    }
);








