
<?php
    include "mysql_connection.php";

    if ($_POST["boardroom"]) {
        $_SESSION['boardroom'] = $_POST["boardroom"];
    }

    $arrayResult = renderCalendar();

    function renderCalendar()
    {
        $dates = array();
        $boardroom = $_SESSION['boardroom'];
        
        $query = "SELECT 
                    Boardroom_Bookings.id AS 'bookID',
                    date_start AS 'Start', 
                    date_end AS 'End', 
                    Boardroom_Bookings.usage AS 'Used for', 
                    Boardroom_Bookings_id AS boardroom,
					(SELECT CONCAT(Staff_First_Name, ' ', Staff_Last_Name) FROM Staff, Boardroom_Bookings WHERE users_id = Staff_Code) AS staff
                    FROM Boardroom_Bookings
                    WHERE boardrooms_id = '$boardroom'
                    ORDER BY date_start";

        $result = mysql_query($query) or die('Query failed: ' . mysql_error());
        
        $resultArray = array();

        while($row = mysql_fetch_array($result))
        {
            $a=array("id"=>$row["id"], "text"=>$row["Used for"], "startDate"=>$row["Start"], "endDate"=>$row["End"], "seriesID"=>$row["boardroom"], "staff"=>$row["staff"]);
            array_push($resultArray, $a);

        }
        return $resultArray;
    }
?>