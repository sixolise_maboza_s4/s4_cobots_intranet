<?php
session_start();
include "../Database.php";
header('Content-Type: application/json');


function getResults($query)
{
    $result = ExecuteQuery($query);
    $rows = array();
    if(MySQL_Num_Rows($result) > 0){
        while ($row = MySQL_Fetch_Array($result))
            $rows[] = $row; 
     }
     return json_encode($rows,JSON_NUMERIC_CHECK);
}
function getCalloutDetails($status,$start,$end,$customer,$name)
{
    $query = 'SELECT Callout_DateTime AS `Date and Time`,CalloutSystem_ShortName AS `System`,CalloutWorkType_Description AS `Work`,Callout_ID AS `Call ID`,Callout_Duration AS `TimeOnCall`,(SELECT CalloutTime_Description FROM CalloutTime WHERE CalloutTime_Minutes = `Callout_Duration`) AS test,Callout_Duration_Repairs AS `TimeOnRepairs`,(SELECT CalloutTime_Description FROM CalloutTime WHERE CalloutTime_Minutes = `Callout_Duration_Repairs`) AS test2 FROM Callout, CalloutSystem,CalloutWorkType WHERE Callout_System = CalloutSystem_ID AND Callout_WorkType = CalloutWorkType_ID AND Callout_Status'.$status.' AND Callout_DateTime BETWEEN "'.$start.' 00:00:00" AND "'.$end.' 23:59:59"'.$name.$customer.' ORDER BY Callout_DateTime ASC';
     return getResults($query);
}
function getCalloutDetailsChart($status,$start,$end,$customer,$name)
{
    $query = 'SELECT CONCAT( DATE_FORMAT( Callout_DateTime,  "%d %b %Y" ) ," at ",DATE_FORMAT( Callout_DateTime,  "%H:%i" ),  " ", CalloutSystem_ShortName,  " ", CalloutWorkType_Description,  "(", Callout_ID,  ")" ) AS  `Callout` , Callout_Duration AS  `TimeOnCall` , Callout_Duration_Repairs AS  `TimeOnRepairs`,CalloutSystem_ShortName AS System,CalloutWorkType_Description AS Work,(((Callout_Duration + Callout_Duration_Repairs)/ (SELECT Sum(Callout_Duration+ Callout_Duration_Repairs) FROM Callout, CalloutSystem,CalloutWorkType WHERE Callout_System = CalloutSystem_ID AND Callout_WorkType = CalloutWorkType_ID AND Callout_Status'.$status.' AND Callout_DateTime BETWEEN "'.$start.' 00:00:00" AND "'.$end.' 23:59:59"'.$name.$customer.'))*100) As totalMinutes  FROM Callout, CalloutSystem,CalloutWorkType WHERE Callout_System = CalloutSystem_ID AND Callout_WorkType = CalloutWorkType_ID AND Callout_Status'.$status.' AND Callout_DateTime BETWEEN "'.$start.' 00:00:00" AND "'.$end.' 23:59:59"'.$name.$customer.' ORDER BY Callout_DateTime ASC';
     return getResults($query);
}
function getCalloutBySystem($status,$start,$end,$customer,$name)
{
    $query = 'SELECT '.$columns.' FROM Callout, CalloutSystem,CalloutWorkType WHERE Callout_System = CalloutSystem_ID AND Callout_WorkType = CalloutWorkType_ID AND Callout_Status'.$status.' AND Callout_DateTime BETWEEN "'.$start.' 00:00:00" AND "'.$end.' 23:59:59"'.$name.$customer.' ORDER BY Callout_DateTime ASC';
    return getResults($query);
}
function getCalloutByWork($status,$start,$end,$customer,$name)
{
    $query = 'SELECT '.$columns.' FROM Callout, CalloutSystem,CalloutWorkType WHERE Callout_System = CalloutSystem_ID AND Callout_WorkType = CalloutWorkType_ID AND Callout_Status'.$status.' AND Callout_DateTime BETWEEN "'.$start.' 00:00:00" AND "'.$end.' 23:59:59"'.$name.$customer.' ORDER BY Callout_DateTime ASC';
    return getResults($query);
}

if(isset($_GET['functionName']))
         echo call_user_func_array($_GET['functionName'], array($_GET['status'],$_GET['start'],$_GET['end'],$_GET['customer'],$_GET['name']));



?>