$(function(){
    var dataSource = new DevExpress.data.DataSource({
                store: bookingArray
    });

    $(".scheduler2").dxScheduler({
            dataSource: dataSource,
            views: ["month", "week", "day"],
            currentView: "month",
            currentDate: new Date().toJSON().slice(0,10).replace(/-/g,'/'),
            firstDayOfWeek: 1,
            startDayHour: 8,
            endDayHour: 18,
            showAllDayPanel: false,
            height: 500,
            onAppointmentAdded: function (e) {
                console.log(e);
            }
    });

    $('.dx-box-item:eq(0), .dx-box-item:eq(1), .dx-box-item:eq(2), .dx-box-item:eq(3), .dx-box-item:eq(4), .dx-box-item:eq(5), .dx-box-item:eq(6)').hide();
});