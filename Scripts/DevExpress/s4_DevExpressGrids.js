/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function ExporterGrid(dataSource,columns,gridID,daterange)
{
    var currentCulture = $("html").prop("lang");
      if (currentCulture) {
          Globalize.culture(currentCulture);
      }
    
    $(gridID).dxDataGrid({
        dataSource: dataSource,
        showColumnLines: true,
        showRowLines: true,
        rowAlternationEnabled: true,
        selection: {
            mode: "multiple"
        },
        "export": {
            enabled: true,
            fileName: "S4I_ProjectLabourCost - "+daterange,
            allowExportSelectedData: true
        },
        groupPanel: {
            visible: true
        },
        columns: columns
    });    
}

function summaryGrid(dataPath,columns,gridID,status,start,end,customer,name)
{
    /**var mySource = new DevExpress.data.CustomStore({
        load: function(loadOptions) {
            var def =  new $.Deferred();
            $.getJSON("Scripts/DevExpress/dxServices.php?functionName="+dataPath+"&status="+status+"&start="+start+"&end="+end+"&customer="+customer+"&name="+name).done(function(response){
                def.resolve(response, {totalCount: response.length });
            });
            return def.promise();
            
        }
   });
   var dataSource = new DevExpress.data.DataSource({
    store: mySource
   });*/
   
   var dataSource = new DevExpress.data.DataSource();
    $(gridID).dxDataGrid({
        dataSource: dataSource,
        showColumnLines: true,
        showRowLines: true,
        rowAlternationEnabled: true,
        selection: {
            mode: "multiple"
        },
        "export": {
            enabled: true,
            fileName: "CalloutReport - ",
            allowExportSelectedData: true
        },
        groupPanel: {
            visible: true
        },
        columns: columns,
        paging: { pageSize: 10 }
    });  
    
    $.getJSON("Scripts/DevExpress/dxServices.php?functionName="+dataPath+"&status="+status+"&start="+start+"&end="+end+"&customer="+customer+"&name="+name).done(function(response){
          $(gridID).dxDataGrid({dataSource: response});
    });
}

function summaryChart(dataPath,series,container,status,start,end,customer,name)
{
   var dataSource = new DevExpress.data.DataSource();
   $(container).dxChart({
        dataSource: dataSource,
        equalBarWidth: {
            width: 10
        },
        commonSeriesSettings: {
            argumentField: "Callout",
            type: "stackedBar"
        },
        series: [
            { valueField: "TimeOnCall", name: "Time Spent on Call(min)" },
            { valueField: "TimeOnRepairs", name: "Time Spent on Repars(min)" },
        ],
       legend: {
            horizontalAlignment: "left",
            position: "inside",
            border: { visible: true }
        },
        valueAxis: {
            title: {
                text: "minutes"
            },
            position: "left"
        },
        title: "",
        tooltip: {
            enabled: true,
            location: "edge",
            customizeTooltip: function (arg) {
                return {
                    text: arg.seriesName + " minutes: " + arg.valueText
                };
            }
        }
    }); 
    $.getJSON("Scripts/DevExpress/dxServices.php?functionName="+dataPath+"&status="+status+"&start="+start+"&end="+end+"&customer="+customer+"&name="+name).done(function(response){
        var newWidth = response.length * 25;
        if(newWidth > $(container).width())
            $(container).width(newWidth);
           
         $(container).dxChart({dataSource: response, size: {width: $(container).width()}});
         if(response.length > 0){
             summaryPieChart("#systemContainer","Callout By System","System",response);
             summaryPieChart("#workContainer","Callout By Work","Work",response);  
         } 
         else{
             $('#calloutPieChart section').removeClass('addborder');
             $('#scrolly').parent('section').removeClass('addborder');
             $('#calloutPieChart').hide();
             $('#scrolly').parent('section').hide();
         }
    });
}

function summaryPieChart(container,title,field,dataSource)
{

    //var dataSource = new DevExpress.data.DataSource();
    $(container).dxPieChart({
        dataSource: dataSource,
        title: title,
        legend: {
            horizontalAlignment: "center",
            verticalAlignment: "bottom"
        },
        tooltip: {
            enabled: true,
            customizeTooltip: function (arg) {
                return {
                    text: arg.argumentText + " - " + (Math.round(arg.valueText * 100) / 100) + "%"
                };
            }
       }
        ,
        series: [{
            argumentField: field,
            valueField: "totalMinutes",
            label: {
                visible: true,
                connector: {
                    visible: true,
                    width: 0.5
                },
                position: "columns",
                customizeText: function (point) {
                    return (Math.round(point.valueText * 100) / 100) + "%";
                }
            },
            smallValuesGrouping: {
                mode: "smallValueThreshold",
                threshold: 4.5
            },
        }]
    });
    
    /**$.getJSON("Scripts/DevExpress/dxServices.php?functionName="+dataPath+"&status="+status+"&start="+start+"&end="+end+"&customer="+customer+"&name="+name).done(function(response){
         $(container).dxChart({dataSource: response});
    });*/
}

function FilterGrid(s4_gridID,s4_data,prep,currentID,s4_columns)
  {
                 $("#"+s4_gridID).dxDataGrid({
                     dataSource: s4_data,
                     rowAlternationEnabled: true,
                     paging: {
                            pageSize: 5
                        },
                     filterRow: {
                           visible: true,
                           applyFilter: "auto"
                      },
                     searchPanel: {
                         visible: true,
                         width: 240,
                         placeholder: "Search..."
                     },
                     columns: s4_columns,
        rowPrepared: function (rowElement, rowInfo) {
            if(prep)
            {
               if (rowInfo.data.OvertimePreApp_ID == currentID)
                  rowElement.css("background", "yellow");
            }
        }  
,
                   }).dxDataGrid("instance");

                    var applyFilterTypes = [{
                        key: "auto",
                        name: "Immediately"
                    }, {
                        key: "onClick",
                        name: "On Button Click"
                    }]

                    $("#useFilterApplyButton").dxSelectBox({
                        items: applyFilterTypes,
                        value: applyFilterTypes[0].key,
                        valueExpr: "key",
                        displayExpr: "name",
                        onValueChanged: function(data) {
                            dataGrid.option("filterRow.applyFilter", data.value);
                        }
                    });
                    
                    //alert('#'+s4_gridID+' .dx-datagrid-content .dx-datagrid-table tr:visible:last');
      $('.dx-datagrid-rowsview .dx-datagrid-content .dx-datagrid-table tr:not(.dx-freespace-row):last').css('border-bottom', ' #0B3162 1px solid');
      //$('#'+s4_gridID+' .dx-datagrid-content .dx-datagrid-table tr').filter(':visible').last().css('border-bottom', ' #0B3162 1px solid');
      console.log($('.dx-datagrid-rowsview .dx-datagrid-content .dx-datagrid-table tr:not(.dx-freespace-row):last'));
  } 
  
function GetAjaxData(paUrl, paType, paData)
    {
        var output = new Object({});
        //$("#blocker").show();
        var loAjax = $.ajax(
                    {
                        url: paUrl,
                        type:paType,
                        data: paData,
                        dataType:'json',
                        async:false
                    });

        loAjax.done(
            function(data)
            {
                //$("#blocker").fadeOut(400);
                output = data;
            }
        );

        loAjax.fail(
            function(jqXHR, text)
            {
                //$("#blocker").fadeOut(400);
                output = text;
            }
        );

        return output;
    }
  
