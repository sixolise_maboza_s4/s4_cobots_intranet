<?php
  // DETAILS ///////////////////////////////////////////////////////////////////
  //                                                                          //
  //                    Last Edited By: Gareth Ambrose                        //
  //                        Date: 12 May 2008                                 //
  //                                                                          //
  //////////////////////////////////////////////////////////////////////////////
  // This page allows users to submit information on deductions.              //
  //////////////////////////////////////////////////////////////////////////////
  
  include 'Scripts/Include.php';
  SetSettings();
  CheckAuthorisation('Deductions.php');
  
  //////////////////////////////////////////////////////////////////////////////
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3c.org/TR/1999/REC-html401-19991224/loose.dtd">
<HTML>
  <HEAD>
    <?php
      // PHP SCRIPT ////////////////////////////////////////////////////////////
      BuildHead('Deductions');
    include('Scripts/header.php');
      //////////////////////////////////////////////////////////////////////////
    ?>
  </HEAD>
  <BODY>
    <?php
      // PHP SCRIPT ////////////////////////////////////////////////////////////
    BuildTopBar();
      BuildBanner();
      //////////////////////////////////////////////////////////////////////////
    ?>
    <DIV id="main">
      <?php
        // PHP SCRIPT //////////////////////////////////////////////////////////
        BuildMenu('Main', 'Deductions.php');

        ////////////////////////////////////////////////////////////////////////
      ?>
        <section id="content_wrapper">
            <?php BuildBreadCrumb($currentPath);?>
            <!-- -------------- Content -------------- -->
            <section id="content" class="table-layout">
                <!-- -------------- Column Center -------------- -->
                <div class="chute chute-center" style="height: 869px;">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel">
        <?php
          // PHP SCRIPT ////////////////////////////////////////////////////////
          BuildMessageSet('Deduction');
          //////////////////////////////////////////////////////////////////////
        ?>
        <?php
          // PHP SCRIPT ////////////////////////////////////////////////////////
          if (isset($_SESSION['AddDeduction']))
          {
            if ($_SESSION['cAuth'] & 64)
            {           
              $row = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = "'.$_SESSION['AddDeduction'][0].'"'));
              BuildContentHeader('Add Deduction - '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'], "", "", false);
            } else
              BuildContentHeader('Add Deduction', "", "", false);
            echo '<DIV class="contentflow">
                    <P>Enter the details of the deduction below. Ensure that all the required information is entered before submission and that this information is valid.</P>
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center" class="short">
                      <FORM method="post" action="Handlers/Deductions_Handler.php" enctype="multipart/form-data">
                        <INPUT name="Type" type="hidden" value="Add">
                        <TR>
                          <TD colspan="2" class="header">Deduction Details
                          </TD>
                        </TR>
                        <TR>
                          <TD class="short">Date:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                            BuildDaySelector(1, 'Day', GetDayFromSessionDate($_SESSION['AddDeduction'][1]));
                            echo '&nbsp;';
                            BuildMonthSelector(2, 'Month', GetMonthFromSessionDate($_SESSION['AddDeduction'][1]));
                            echo '&nbsp;';
                            BuildYearSelector(3, 'Year', GetYearFromSessionDate($_SESSION['AddDeduction'][1]));
                    echo '</TD>
                        </TR>
                        <TR>
                          <TD>Value:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD class="bold">
                            R <INPUT tabindex="4" name="Value" type="text" class="text veryshort" maxlength="7" value="'.$_SESSION['AddDeduction'][2].'" />
                          </TD>
                        </TR>
                        <TR>
                          <TD class="vtop">Description:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>
                            <TEXTAREA tabindex="5" name="Description" class="long" maxlength="100">'.$_SESSION['AddDeduction'][3].'</TEXTAREA>
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="2" class="center">
                            <INPUT tabindex="6" name="Submit" type="submit" class="button" value="Submit" />   
                            <INPUT tabindex="7" name="Submit" type="submit" class="button" value="Cancel" />                  
                          </TD>
                        </TR>
                      </FORM>
                    </TABLE>  
                  </DIV>  
                  <DIV>
                    <BR />
                    <SPAN class="note">*
                    </SPAN>
                    These fields are required.
                  </DIV>';
          } else
          if (isset($_SESSION['EditDeduction']))
          {          
            $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Deduction WHERE Deduction_ID = "'.$_SESSION['EditDeduction'][0].'"'));
            $staffCode = $rowTemp['Deduction_Name'];
            $date = GetTextualDateFromDatabaseDate($rowTemp['Deduction_Date']);
            
            if ($_SESSION['cAuth'] & 64)
            {           
              $row = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = '.$staffCode.''));
              BuildContentHeader('Edit Deduction - '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].' for '.$date, "", "", false);
            } else
              BuildContentHeader('Edit Deduction for '.$date, "", "", false);
            echo '<DIV class="contentflow">
                    <P>Once you have made changes, ensure that you submit them otherwise they will not take effect.</P>
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center" class="short">
                      <FORM method="post" action="Handlers/Deductions_Handler.php" enctype="multipart/form-data">
                        <INPUT name="Type" type="hidden" value="Edit">
                        <TR>
                          <TD colspan="2" class="header">Deduction Details
                          </TD>
                        </TR>
                        <TR>
                          <TD class="short">Date:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                            BuildDaySelector(1, 'Day', GetDayFromSessionDate($_SESSION['EditDeduction'][1]));
                            echo '&nbsp;';
                            BuildMonthSelector(2, 'Month', GetMonthFromSessionDate($_SESSION['EditDeduction'][1]));
                            echo '&nbsp;';
                            BuildYearSelector(3, 'Year', GetYearFromSessionDate($_SESSION['EditDeduction'][1]));
                    echo '</TD>
                        </TR>
                        <TR>
                          <TD>Value:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD class="bold">
                            R <INPUT tabindex="4" name="Value" type="text" class="text veryshort" value="'.$_SESSION['EditDeduction'][2].'" maxlength="7" />
                          </TD>
                        </TR>
                        <TR>
                          <TD class="vtop">Description:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>
                            <TEXTAREA tabindex="5" name="Description" class="long" maxlength="100">'.$_SESSION['EditDeduction'][3].'</TEXTAREA>
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="2" class="center">
                            <INPUT tabindex="6" name="Submit" type="submit" class="button" value="Submit" />   
                            <INPUT tabindex="7" name="Submit" type="submit" class="button" value="Cancel" />                  
                          </TD>
                        </TR>
                      </FORM>
                    </TABLE>  
                  </DIV>  
                  <DIV>
                    <BR />
                    <SPAN class="note">*
                    </SPAN>
                    These fields are required.
                  </DIV>';
          } else
          if (isset($_SESSION['RemoveDeduction']))
          {
            $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Deduction WHERE Deduction_ID = "'.$_SESSION['RemoveDeduction'][0].'"'));
            $staffCode = $rowTemp['Deduction_Name'];
            $date = GetTextualDateFromDatabaseDate($rowTemp['Deduction_Date']);
            
            if ($_SESSION['cAuth'] & 64)
            {
              $row = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = '.$staffCode.''));
              BuildContentHeader('Remove Deduction - '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].' for '.$date, "", "", false);
            } else
              BuildContentHeader('Remove Deduction for '.$date, "", "", false);
            
            echo '<DIV class="contentflow">
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center" class="short">
                      <TR>
                        <TD colspan="2" class="header">Deduction Details
                        </TD>
                      </TR>
                      <TR>
                        <TD class="short">Date:
                        </TD>
                        <TD class="bold short">'.GetTextualDateFromDatabaseDate($rowTemp['Deduction_Date']).'
                        </TD>
                      </TR>
                      <TR>
                        <TD>Value:
                        </TD>
                        <TD class="bold">R'.SPrintF('%02.2f', $rowTemp['Deduction_Value']).'
                        </TD>
                      </TR>
                      <TR>
                        <TD class="vtop">Description:
                        </TD>
                        <TD class="bold vtop">'.$rowTemp['Deduction_Description'].'
                        </TD>
                      </TR>
                    </TABLE>
                    <BR /><BR />
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center" class="short">
                      <FORM method="post" action="Handlers/Deductions_Handler.php">
                        <INPUT name="Type" type="hidden" value="Remove" />
                        <TR>
                          <TD colspan="4" class="header">Remove
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="4" class="center">Are you sure you wish to remove this deduction?
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="2" class="center">
                            <INPUT tabindex="1" name="Submit" type="submit" class="button" value="Yes" />   
                            <INPUT tabindex="2" name="Submit" type="submit" class="button" value="No" />                  
                          </TD>
                        </TR>
                      </FORM>
                    </TABLE>  
                  </DIV>';
          } else
          {
            BuildContentHeader('Maintenance', "", "", false);   
            echo '<DIV class="contentflow">
                    <P>Deductions can be recorded by using the form below. Details on deductions already listed can also be edited or removed.</P>
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center" class="standard">
                      <FORM method="post" action="Handlers/Deductions_Handler.php">
                        <INPUT name="Type" type="hidden" value="Maintain">
                        <TR>
                          <TD colspan="4" class="header">Add
                          </TD>
                        </TR>';
                        if ($_SESSION['cAuth'] & 64)
                        {
                          echo '<TR>
                                  <TD colspan="4">To record a deduction, specify the particulars, click Add and complete the form that is displayed.
                                  </TD>
                                </TR>
                                <TR>
                                  <TD class="short">Staff Name:
                                    <SPAN class="note">*
                                    </SPAN>
                                  </TD>
                                  <TD>';
                                    BuildStaffSelector(1, 'Staff', 'standard', $_SESSION['cUID'], true);
                            echo '</TD>
                                  <TD colspan="2" class="right">
                                    <INPUT tabindex="2" name="Submit" type="submit" class="button" value="Add" />                   
                                  </TD>
                                </TR>';
                        } else
                          echo '<TR>
                                  <TD colspan="3">To record a deduction, click Add and complete the form that is displayed.
                                  </TD>
                                  <TD class="right">
                                    <INPUT tabindex="1" name="Submit" type="submit" class="button" value="Add" />                   
                                  </TD>
                                </TR>';
                  echo '<TR>
                          <TD colspan="4" class="header">Edit
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="4">To edit a deduction, specify the particulars, click Edit and complete the form that is displayed.
                          </TD>
                        </TR>
                        <TR>
                          <TD class="short">Deduction:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                          if ($_SESSION['cAuth'] & 64)
                            BuildDeductionSelector(3, 'EditDeduction', 'long', "");
                          else
                            BuildDeductionSelector(3, 'EditDeduction', 'long', $_SESSION['cUID']);
                    echo '</TD>
                          <TD colspan="2" class="right">
                            <INPUT tabindex="4" name="Submit" type="submit" class="button" value="Edit" />                   
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="4" class="header">Remove
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="4">To remove a deduction, specify the particulars, click Remove and confirm when prompted.
                          </TD>
                        </TR>
                        <TR>
                          <TD class="short">Deduction:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                          if ($_SESSION['cAuth'] & 64)
                            BuildDeductionSelector(5, 'RemoveDeduction', 'long', "");
                          else
                            BuildDeductionSelector(5, 'RemoveDeduction', 'long', $_SESSION['cUID']);
                    echo '</TD>
                          <TD colspan="2" class="right">
                            <INPUT tabindex="6" name="Submit" type="submit" class="button" value="Remove" />                   
                          </TD>
                        </TR>
                      </FORM>
                    </TABLE>
                  </DIV>';         
            }
            ////////////////////////////////////////////////////////////////////
          ?>
                            </div>
                        </div>
                    </div>
            </section
    </DIV>
    <?php 
      // PHP SCRIPT ////////////////////////////////////////////////////////////
      BuildFooter();
      //////////////////////////////////////////////////////////////////////////
    ?>    
  </BODY>
</HTML>
