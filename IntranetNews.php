<?php
  // DETAILS ///////////////////////////////////////////////////////////////////
  //                                                                          //
  //                    Last Edited By: Gareth Ambrose                        //
  //                        Date: 12 May 2008                                 //
  //                                                                          //
  //////////////////////////////////////////////////////////////////////////////
  // This page allows users to manage intranet news.                          //
  //////////////////////////////////////////////////////////////////////////////
  
  include 'Scripts/Include.php';
  SetSettings();
  CheckAuthorisation('IntranetNews.php'); 
  
  //////////////////////////////////////////////////////////////////////////////
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3c.org/TR/1999/REC-html401-19991224/loose.dtd">
<HTML>
  <HEAD>
    <?php
      // PHP SCRIPT ////////////////////////////////////////////////////////////
      BuildHead('Intranet News');
    include ('Scripts/header.php');
      //////////////////////////////////////////////////////////////////////////
    ?>
  </HEAD>
  <BODY> 
    <?php
      // PHP SCRIPT ////////////////////////////////////////////////////////////
      BuildBanner();
      //////////////////////////////////////////////////////////////////////////
    ?>
    <DIV class="contentcontainer">
      <?php
        // PHP SCRIPT //////////////////////////////////////////////////////////
        BuildMenu('Main', 'IntranetNews.php');
        ////////////////////////////////////////////////////////////////////////
      ?>
      <DIV class="content">
        <BR /><BR />
        <?php
          // PHP SCRIPT ////////////////////////////////////////////////////////
          BuildMessageSet('IntranetNews');
          //////////////////////////////////////////////////////////////////////    
        ?>
        <?php
          // PHP SCRIPT ////////////////////////////////////////////////////////
          if (isset($_SESSION['AddIntranetNews']))
          {
            BuildContentHeader('Add Intranet News', "", "", false);
            echo '<DIV class="contentflow">
                    <P>Enter the details of the news below. Ensure that all the required information is entered before submission and that this information is valid.</P>
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center" class="short">
                      <FORM method="post" action="Handlers/IntranetNews_Handler.php">
                        <INPUT name="Type" type="hidden" value="Add">
                        <TR>
                          <TD colspan="2" class="header">News Details
                          </TD>
                        </TR>
                        <TR>
                          <TD class="short vtop">Description:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>
                            <TEXTAREA tabindex="1" name="Description" type="text" class="long"></TEXTAREA>
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="2" class="center">
                            <INPUT tabindex="2" name="Submit" type="submit" class="button" value="Submit" />   
                            <INPUT tabindex="3" name="Submit" type="submit" class="button" value="Cancel" />                  
                          </TD>
                        </TR>
                      </FORM>
                    </TABLE>  
                  </DIV>  
                  <DIV>
                    <BR />
                    <SPAN class="note">*
                    </SPAN>
                    These fields are required.
                  </DIV>';
          } else
          if (isset($_SESSION['RemoveIntranetNews']))
          {
            BuildContentHeader('Remove Intranet News', "", "", false);
            echo '<DIV class="contentflow">
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center" class="short">
                      <TR>
                        <TD colspan="2" class="header">News Details
                        </TD>
                      </TR>
                      <TR>
                        <TD class="short vtop">Description:
                        </TD>
                        <TD class="bold vtop">'.$_SESSION['RemoveIntranetNews'][1].'
                        </TD>
                      </TR>
                    </TABLE>
                    <BR /><BR />
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center" class="short">
                      <FORM method="post" action="Handlers/IntranetNews_Handler.php">
                        <INPUT name="Type" type="hidden" value="Remove" />
                        <TR>
                          <TD colspan="2" class="header">Remove
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="2" class="center">Are you sure you wish to remove this news entry?
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="2" class="center">
                            <INPUT tabindex="1" name="Submit" type="submit" class="button" value="Yes" />   
                            <INPUT tabindex="2" name="Submit" type="submit" class="button" value="No" />                  
                          </TD>
                        </TR>
                      </FORM>
                    </TABLE>
                  </DIV>';
          } else
          {
            BuildContentHeader('Maintenance', "", "", false);
            echo '<DIV class="contentflow">
                    <P>Intranet news can be added by using the form below. News already listed can also be removed.</P>
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center" class="standard">
                      <FORM method="post" action="Handlers/IntranetNews_Handler.php">
                        <INPUT name="Type" type="hidden" value="Maintain">
                        <TR>
                          <TD colspan="4" class="header">Add
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="2">To add intranet news, click Add and complete the form that is displayed.
                          </TD>
                          <TD colspan="2" class="right">
                            <INPUT tabindex="2" name="Submit" type="submit" class="button" value="Add" />                   
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="4" class="header">Remove
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="4">To remove a news entry, specify the particulars, click Remove and confirm when prompted.
                          </TD>
                        </TR>
                        <TR>
                          <TD class="short">News Entry:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                            BuildIntranetNewsSelector(2, 'RemoveIntranetNews', 'long', 'index.php');
                    echo '</TD>
                          <TD colspan="2" class="right">
                            <INPUT tabindex="3" name="Submit" type="submit" class="button" value="Remove" />                   
                          </TD>
                        </TR>
                      </FORM>
                    </TABLE>
                  </DIV>';
          }                
          //////////////////////////////////////////////////////////////////////
        ?>
        <BR /><BR />
      </DIV>
    </DIV>
    <?php
      // PHP SCRIPT ////////////////////////////////////////////////////////////
      BuildFooter();
      //////////////////////////////////////////////////////////////////////////
    ?>
  </BODY>
</HTML>