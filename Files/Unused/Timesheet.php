<?php 
  // DETAILS ///////////////////////////////////////////////////////////////////
  //                                                                          //
  //                    Last Edited By: Gareth Ambrose                        //
  //                        Date: 12 May 2008                                 //
  //                                                                          //
  ////////////////////////////////////////////////////////////////////////////// 
  // This page allows users to manage and view timesheets.                    //
  //////////////////////////////////////////////////////////////////////////////
   
  include 'Scripts/Include.php';
  SetSettings();
  CheckAuthorisation('Timesheet.php'); 
  
  //////////////////////////////////////////////////////////////////////////////
?>  
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3c.org/TR/1999/REC-html401-19991224/loose.dtd">
<HTML>
  <HEAD>
    <?php 
      // PHP SCRIPT ////////////////////////////////////////////////////////////
      BuildHead('Timesheet');
      //////////////////////////////////////////////////////////////////////////
    ?>   
  </HEAD>
  <BODY> 
    <?php 
      // PHP SCRIPT ////////////////////////////////////////////////////////////
      BuildBanner();
      //////////////////////////////////////////////////////////////////////////
    ?>    
    <DIV class="contentcontainer">
      <?php 
        // PHP SCRIPT //////////////////////////////////////////////////////////          
        BuildMenu('Main', 'Timesheet.php');                            
        ////////////////////////////////////////////////////////////////////////
      ?>
      <DIV class="content">
        <BR /><BR />
        <?php
          // PHP SCRIPT ////////////////////////////////////////////////////////
          BuildMessageSet('Timesheet');
          //////////////////////////////////////////////////////////////////////    
        ?>  
        <?php 
          // PHP SCRIPT ////////////////////////////////////////////////////////  
          if (isset($_SESSION['EditTimesheet']))
          {
            $dateRange = GetTextualDateFromSessionDate($_SESSION['EditTimesheet']);            
            BuildContentHeader('Edit Timesheet - '.$dateRange, "", "", false);
            echo '<DIV class="contentflow">
                    <P>Once you have made changes, ensure that you submit them otherwise they will not take effect. The time periods in blue are your expected working hours so always try to keep these accounted for.</P>
                    <BR /><BR />
                    <FORM method="post" action="Handlers/Timesheet_Handler.php">
                      <TABLE cellspacing="5" align="center" class="long">
                        <TR>
                          <TD colspan="4" class="header">AutoComplete Details
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="4">You can automatically complete your expected working hours by specifying the details in the fields below and clicking Complete. This only applies to hours that do not already have values recorded. Any changes made before using the AutoComplete will be lost if they are not saved.
                          </TD>
                        </TR>
                        <TR>
                          <TD class="subheader">Project
                            <SPAN class="note">**
                            </SPAN>
                          </TD>
                          <TD class="subheader">Task
                            <SPAN class="note">**
                            </SPAN>
                          </TD>
                          <TD class="subheader">Other
                            <SPAN class="note">**
                            </SPAN>
                          </TD>
                        </TR>
                        <TR>
                          <TD class="center">';
                            BuildProjectSelector(1, 'AutoProject'.$count, 'long', "", 'Open', false, true, false, true);
                    echo '</TD>
                          <TD class="center">';
                            BuildTaskSelector(2, 'AutoTask'.$count, 'standard', "", $_SESSION['cUID']);
                    echo '</TD>
                          <TD class="center">';
                            echo '<INPUT tabindex="3" name="AutoOther'.$count.'" class="text standard" maxlength="100" />';
                    echo '</TD>
                        </TR>
                        <TR>
                          <TD colspan="4" class="center">
                            <INPUT tabindex="4" name="Submit" type="submit" class="button" value="Complete" />               
                          </TD>
                        </TR>
                      </TABLE>
                      <BR /><BR />
                      <BR /><BR />
                      <TABLE cellspacing="5" align="center" class="long">
                        <INPUT name="Type" type="hidden" value="Edit">
                        <INPUT name="Timesheet" type="hidden" value="'.$_SESSION['EditTimesheet'].'">
                        <TR>
                          <TD colspan="4" class="header">Timesheet Details
                          </TD>
                        </TR>
                        <TR>
                          <TD class="subheader veryshort">Time Period
                          </TD>
                          <TD class="subheader">Project
                            <SPAN class="note">**
                            </SPAN>
                          </TD>
                          <TD class="subheader">Task
                            <SPAN class="note">**
                            </SPAN>
                          </TD>
                          <TD class="subheader">Other
                            <SPAN class="note">**
                            </SPAN>
                          </TD>
                        </TR>';
                        $resultSet = ExecuteQuery('SELECT * FROM WorkHours WHERE WorkHours_Name = "'.$_SESSION['cUID'].'"');
                        $row = MySQL_Fetch_Array($resultSet);
                        $tabIndex = 5;
                        for ($count = 0; $count < 24; $count++) 
                        {                         
                          if ($row[$count + 2] == 1)
                          {
                            $colour = 'rowA';
                            $project = $_SESSION['EditTimesheetComplete'][0];
                            $task = $_SESSION['EditTimesheetComplete'][1];
                            $text = $_SESSION['EditTimesheetComplete'][2];
                          }
                          else
                          {
                            $colour = 'rowB';
                            $project = "";
                            $task = "";
                            $text = "";
                          }
                            
                          $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM TimeSheet WHERE TimeSheet_Name = "'.$_SESSION['cUID'].'" AND TimeSheet_Date = "'.$_SESSION['Date'].'" AND TimeSheet_Hour = "'.$count.'"'));
                          echo '<TR>
                                  <TD class="'.$colour.' center">'.SPrintF('%02d', $count).':00 - '.SPrintF('%02d', $count + 1).':00
                                  </TD>
                                  <TD class="'.$colour.' center">';
                                    if (($rowTemp['TimeSheet_Project'] == "0") || ($rowTemp['TimeSheet_Project'] == ""))
                                      BuildProjectSelector($tabIndex++, 'Project'.$count, 'long', $project, 'Open', false, true, false, true);
                                    else
                                      BuildProjectSelector($tabIndex++, 'Project'.$count, 'long', $rowTemp['TimeSheet_Project'], 'Open', false, true, false, true);
                            echo '</TD>
                                  <TD class="'.$colour.' center">';
                                    if (($rowTemp['TimeSheet_Task'] == "0") || ($rowTemp['TimeSheet_Task'] == ""))
                                      BuildTaskSelector($tabIndex++, 'Task'.$count, 'standard', $task, $_SESSION['cUID']);
                                    else
                                      BuildTaskSelector($tabIndex++, 'Task'.$count, 'standard', $rowTemp['TimeSheet_Task'], $_SESSION['cUID']);
                            echo '</TD>
                                  <TD class="'.$colour.' center">';
                                    if (($rowTemp['TimeSheet_Text'] == "") || ($rowTemp['TimeSheet_Text'] == "P!") || ($rowTemp['TimeSheet_Text'] == "T!"))
                                      echo '<INPUT tabindex="'.($tabIndex++).'" name="Other'.$count.'" class="text standard" maxlength="100" value="'.$text.'"/>';
                                    else
                                      echo '<INPUT tabindex="'.($tabIndex++).'" name="Other'.$count.'" class="text standard" maxlength="100" value="'.$rowTemp['TimeSheet_Text'].'" />';
                            echo '</TD>
                                </TR>';
                        }
                  echo '<TR>
                          <TD colspan="4" class="center">
                            <INPUT tabindex="'.($tabIndex++).'" name="Submit" type="submit" class="button" value="Submit" />   
                            <INPUT tabindex="'.$tabIndex.'" name="Submit" type="submit" class="button" value="Cancel" />                  
                          </TD>
                        </TR>
                      </TABLE>
                    </FORM>
                  </DIV>  
                  <DIV>
                    <BR />
                    <SPAN class="note">**
                    </SPAN>
                    A time period only rquires one of its fields to be completed, otherwise leave it blank if it is not applicable. All three fields can be used if desired.
                  </DIV>';
            Session_Unregister('EditTimesheet');
            Session_Unregister('EditTimesheetComplete');
          } else
          if (isset($_SESSION['ViewTimesheet']))
          {
            if ($_SESSION['StartDate'] == $_SESSION['EndDate'])
              $dateRange = GetTextualDateFromSessionDate($_SESSION['StartDate']);
            else        
              $dateRange = GetTextualDateFromSessionDate($_SESSION['StartDate']).' to '.GetTextualDateFromSessionDate($_SESSION['EndDate']);
            
            if ($_SESSION['Auth'] & 64)
            {
              $row = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = "'.$_SESSION['ViewTimesheet'].'"'));                
              BuildContentHeader('Timesheet - '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].' for '.$dateRange, "", "", false);
            } else
              BuildContentHeader('Timesheet - '.$dateRange, "", "", false);
                        
            $resultSet = ExecuteQuery('SELECT * FROM TimeSheet WHERE TimeSheet_Name = "'.$_SESSION['ViewTimesheet'].'" AND TimeSheet_Date BETWEEN "'.$_SESSION['StartDate'].'" AND "'.$_SESSION['EndDate'].'" ORDER BY TimeSheet_Hour ASC');
            if (MySQL_Num_Rows($resultSet) > 0)   
            {        
              if ($_SESSION['StartDate'] == $_SESSION['EndDate'])
                echo '<DIV class="contentflow">
                        <P>This is the timesheet listed.</P>
                        <BR /><BR />';   
              else 
                echo '<DIV class="contentflow">
                        <P>These are the timesheets listed.</P>
                        <BR /><BR />';    
                      
              $startDate = GetDatabaseDateFromSessionDate($_SESSION['StartDate']);
              $endDate = GetDatabaseDateFromSessionDate($_SESSION['EndDate']);
              $days = 1;
              while ($startDate <= $endDate)
              {
                $resultSet = ExecuteQuery('SELECT * FROM TimeSheet WHERE TimeSheet_Name = "'.$_SESSION['ViewTimesheet'].'" AND TimeSheet_Date = "'.$startDate.'"');
                if (MySQL_Num_Rows($resultSet) > 0)
                {   
                  echo '<TABLE cellspacing="5" align="center" class="long">
                          <TR>
                            <TD colspan="4" class="header">Timesheet Details for '.GetTextualDateFromDatabaseDate($startDate).'
                            </TD>
                          </TR>
                          <TR>
                            <TD class="subheader veryshort">Time Period
                            </TD>
                            <TD class="subheader standard">Project
                            </TD>
                            <TD class="subheader standard">Task
                            </TD>
                            <TD class="subheader standard">Other
                            </TD>
                          </TR>';
                          $count = 0;
                          while ($row = MySQL_Fetch_Array($resultSet))
                          {
                            if ($count % 2 == 0)
                              $colour = 'rowA';
                            else
                              $colour = 'rowB';
                            
                            echo '<TR>
                                    <TD class="'.$colour.' center">'.SPrintF('%02d', $row['TimeSheet_Hour']).':00 - '.SPrintF('%02d', $row['TimeSheet_Hour']+1).':00
                                    </TD>
                                    <TD class="'.$colour.' center">';
                                      if ($rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT TimeSheet_Text, Project_Description FROM TimeSheet, Project WHERE TimeSheet_ID = "'.$row['TimeSheet_ID'].'" AND TimeSheet_Project = Project_Code')))
                                        echo $rowTemp['Project_Description'];
                              echo '</TD>
                                    <TD class="'.$colour.' center">';
                                     if ($rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT TimeSheet_Text, Task_Title FROM TimeSheet, Task WHERE TimeSheet_ID = "'.$row['TimeSheet_ID'].'" AND TimeSheet_Task = Task_ID')))
                                        echo $rowTemp['Task_Title'];
                              echo '</TD>
                                    <TD class="'.$colour.' center">';
                                     if ($rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT TimeSheet_Text FROM TimeSheet WHERE TimeSheet_ID = "'.$row['TimeSheet_ID'].'"')))
                                        if (($rowTemp['TimeSheet_Text'] != 'P!') && ($rowTemp['TimeSheet_Text'] != 'T!'))                                        
                                          echo $rowTemp['TimeSheet_Text'];
                              echo '</TD>     
                                  </TR>';
                            $count++;
                          }            
                  echo '</TABLE>
                        <BR /><BR />'; 
                }                             
                $startDate = GetDatabaseDate((GetDayFromSessionDate($_SESSION['StartDate']) + $days), GetMonthFromSessionDate($_SESSION['StartDate']), GetYearFromSessionDate($_SESSION['StartDate']));
                $days++;  
              }
            } else
              echo '<DIV class="contentflow">
                      <P>There are no timesheet entries listed for the given date range.</P>
                   </DIV>';   
            Session_Unregister('StartDate'); 
            Session_Unregister('EndDate');    
            Session_Unregister('ViewTimesheet');  
          } else
          {
            BuildContentHeader('Maintenance', "", "", false);   
            echo '<DIV class="contentflow">
                    <P>Your timesheet can be edited or updated by using the form below.</P>
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center" class="standard">
                      <FORM method="post" action="Handlers/Timesheet_Handler.php">
                        <INPUT name="Type" type="hidden" value="Maintain">
                        <TR>
                          <TD colspan="4" class="header">Edit
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="4">To edit your timesheet, specify the particulars, click Edit and complete the form that is displayed.
                          </TD>
                        </TR>
                        <TR>
                          <TD class="short">Date:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                            BuildDaySelector(1, 'Day', "");
                            echo '&nbsp;';
                            BuildMonthSelector(2, 'Month', "");
                            echo '&nbsp;';
                            BuildYearSelector(3, 'Year', "");
                    echo '</TD>
                          <TD colspan="2" align="right">
                            <INPUT tabindex="4" name="Submit" type="submit" class="button" value="Edit" />                   
                          </TD>
                        </TR>
                      </FORM>
                    </TABLE>
                  </DIV>';
                  
            BuildContentHeader('View Timesheet', "", "", true);          
            echo '<DIV class="contentflow">
                    <P>Timesheets can be viewed by using the form below.</P>
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center" class="standard">
                      <FORM method="post" action="Handlers/Timesheet_Handler.php">
                        <INPUT name="Type" type="hidden" value="View">
                        <TR>
                          <TD colspan="4" class="header">View
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="4">To view timesheets, specify the particulars and click View.
                          </TD>
                        </TR>';
                        if ($_SESSION['Auth'] & 64)
                        {
                          echo '<TR>
                                  <TD>Staff Name:
                                    <SPAN class="note">*
                                    </SPAN>
                                  </TD>
                                  <TD>';
                                    BuildStaffSelector(5, 'Staff', 'standard', $_SESSION['cUID']);
                            echo '</TD>
                                </TR>';
                        }
                  echo '<TR>
                          <TD class="short">Date Range:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                            BuildDaySelector(6, 'StartDay', "");
                            echo '&nbsp;';
                            BuildMonthSelector(7, 'StartMonth', "");
                            echo '&nbsp;';
                            BuildYearSelector(8, 'StartYear', "");
                            echo ' to ';
                            BuildDaySelector(9, 'EndDay', "");
                            echo '&nbsp;';
                            BuildMonthSelector(10, 'EndMonth', "");
                            echo '&nbsp;';
                            BuildYearSelector(11, 'EndYear', "");
                    echo '</TD>
                          <TD colspan="2" align="right">
                            <INPUT tabindex="12" name="Submit" type="submit" class="button" value="View" />                   
                          </TD>
                        </TR>
                      </FORM>
                    </TABLE>
                  </DIV>';                
          }               
          //////////////////////////////////////////////////////////////////////
        ?>
        <BR /><BR />
      </DIV>
    </DIV>
    <?php 
      // PHP SCRIPT ////////////////////////////////////////////////////////////
      BuildFooter();
      //////////////////////////////////////////////////////////////////////////
    ?>    
  </BODY>
</HTML>
