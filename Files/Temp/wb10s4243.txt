"25.10.10","1","100334","854","110","Engineering","","100","3020: Alt-BR: LH: Stellglied+CTS Analyse. Re-Review, task complete, open issue has been highlighted in the PDRS","07:00","16:00","3","60",
"25.10.10","1","100334","706","110","Engineering","","100","3080: Alt-BR: EZK: ODX Analyse. Review of ECU analysis for services and ODX, re-review required(task from s47)","","","4","0",
"25.10.10","1","100018","43","110","Engineering","","0","Wochenaufgaben AES. Project discussion on closed tasks","","","1","0",
"26.10.10","2","100334","750","110","Engineering","","100","6000: Alt-BR: RDK: ODX Analyse. Review of ECU analysis for services and ODX(from s41)","07:00","16:00","2","60",
"26.10.10","2","100334","751","110","Engineering","","100","6000: Alt-BR: RDK: Stellglied Analyse. Review of analysis documents and DEF-Tabels(from s41)","","","2","0",
"26.10.10","2","100334","469","110","Engineering","","100","4000: Alt-BR: ABS5: ODX Analyse. Review of general analysis document(from s41)","","","3","0",
"26.10.10","2","100334","62","101","PM/Mod/Meetings","","0","8501: Alt-BR: allg. Projekttätigkeit C1.3/ae14(WPos 110 - 101 not available). Updated PJWP_100334 document","","","1","0",
"27.10.10","3","100334","574","110","Engineering","","100","4000: Alt-BR: ABS5: CTS Analyse. Review of ECU Specific CTS analysis documents, asking questions - Stellglieder(from s41)","07:00","16:00","5","60",
"27.10.10","3","100334","62","101","PM/Mod/Meetings","","0","8501: Alt-BR: allg. Projekttätigkeit C1.3/ae14(WPos 110 - 101 not available). Documentation discussions and planning and updating references","","","2","0",
"27.10.10","3","100018","43","110","Engineering","","0","Wochenaufgaben AES. Timesheet assistance and checking","","","1","0",
"28.10.10","4","100334","807","110","Engineering","","10","4060: Alt-BR: ABS: Analyse Anp+Übernahme 9x6. Pre-analysis overview, cheking cross platform compatibility 9x6/ABR","07:00","16:00","3","60",
"28.10.10","4","100334","62","101","PM/Mod/Meetings","","0","8501: Alt-BR: allg. Projekttätigkeit C1.3/ae14(WPos 110 - 101 not available). Documentation discussion and finilizing suggestion with nc.","","","2","0",
"28.10.10","4","100025","313","170","Schulung","","0","Schulungen 2010. PIDT training and assisting with setup irregularities","","","2","0",
"28.10.10","4","100018","43","110","Engineering","","0","Wochenaufgaben AES. Task assignment and timesheet entry follow up","","","1","0",
"29.10.10","5","100018","43","110","Engineering","","0","Wochenaufgaben AES. Department meeting to discuss way for with ABR project, and Timesheet admin","07:00","16:00","2,5","60",
"29.10.10","5","100334","807","110","Engineering","","50","4060: Alt-BR: ABS: Analyse Anp+Übernahme 9x6. Reworking of 9x6 documents in order to get them to the new ABR standard","","","5,5","0",
