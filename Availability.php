<?php

  include 'Scripts/Include.php';
  SetSettings();
  CheckAuthorisation('Availability.php');
  
  //////////////////////////////////////////////////////////////////////////////
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3c.org/TR/1999/REC-html401-19991224/loose.dtd">
<HTML>
  <HEAD>
    <?php
      // PHP SCRIPT ////////////////////////////////////////////////////////////
      BuildHead('Availability');
      include('Scripts/header.php');
      //////////////////////////////////////////////////////////////////////////
    ?>
  </HEAD>
  <BODY>
    <?php
      // PHP SCRIPT ////////////////////////////////////////////////////////////
      BuildBanner();
      //////////////////////////////////////////////////////////////////////////
    ?>
    <DIV id="main">
      <?php
      BuildTopBar();
        // PHP SCRIPT //////////////////////////////////////////////////////////
        BuildMenu('Main', 'Availability.php');
        ////////////////////////////////////////////////////////////////////////
      ?>
        <section id="content_wrapper">
            <?php BuildBreadCrumb($currentPath);?>
            <!-- -------------- Content -------------- -->
            <section id="content" class="table-layout">
                <!-- -------------- Column Center -------------- -->
                <div class="chute chute-center" style="height: 869px;">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel">
        <?php
          // PHP SCRIPT ////////////////////////////////////////////////////////
          BuildMessageSet('Availability');
          //////////////////////////////////////////////////////////////////////    
        ?>
        <?php 
          // PHP SCRIPT //////////////////////////////////////////////////////// 
          if (isset($_SESSION['AddAvailability']))
          {
            if ($_SESSION['cAuth'] & 32)
            {           
              $row = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = "'.$_SESSION['AddAvailability'][0].'"'));
              BuildContentHeader('Add Availability - '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'], "", "", true);
            } else
              BuildContentHeader('Add Availability', "", "", true);
            echo '<DIV class="contentflow">
                    <P>Enter the details of the availability below. Ensure that all the required information is entered before submission and that this information is valid.</P>
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center" class="long">
                      <FORM method="post" action="Handlers/Availability_Handler.php">
                        <INPUT name="Type" type="hidden" value="Add">
                        <TR>
                          <TD colspan="4" class="header">Availability Details
                          </TD>
                        </TR>
                        <TR>
                          <TD class="short">Type:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                            BuildAvailabilityTypeSelector(1, 'AvailabilityType', 'standard', $_SESSION['AddAvailability'][1]);
                    echo '</TD>
                          <TD class="short">Start Date and Time:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                            BuildDaySelector(3, 'StartDay', GetDayFromSessionDate($_SESSION['AddAvailability'][2]));
                            echo '&nbsp;';
                            BuildMonthSelector(4, 'StartMonth', GetMonthFromSessionDate($_SESSION['AddAvailability'][2]));
                            echo '&nbsp;';
                            BuildYearSelector(5, 'StartYear', GetYearFromSessionDate($_SESSION['AddAvailability'][2]));
                            echo '&nbsp;at&nbsp;';
                            BuildHourSelector(6, 'StartHour', GetHourFromSessionDateTime($_SESSION['AddAvailability'][2]));
                            echo '&nbsp;:';
                            BuildMinuteSelector(7, 'StartMinute', GetMinuteFromSessionDateTime($_SESSION['AddAvailability'][2]));
                    echo '</TD>
                        </TR>
                        <TR>
                          <TD class="short">Description:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD rowspan="4">
                            <TEXTAREA tabindex="2" name="Description" class="standard">'.$_SESSION['AddAvailability'][3].'</TEXTAREA>
                          </TD>
                          <TD class="short">End Date and Time:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                            BuildDaySelector(8, 'EndDay', GetDayFromSessionDate($_SESSION['AddAvailability'][4]));
                            echo '&nbsp;';
                            BuildMonthSelector(9, 'EndMonth', GetMonthFromSessionDate($_SESSION['AddAvailability'][4]));
                            echo '&nbsp;';
                            BuildYearSelector(10, 'EndYear', GetYearFromSessionDate($_SESSION['AddAvailability'][4]));
                            echo '&nbsp;at&nbsp;';
                            BuildHourSelector(11, 'EndHour', GetHourFromSessionDateTime($_SESSION['AddAvailability'][4]));
                            echo '&nbsp;:';
                            BuildMinuteSelector(12, 'EndMinute', GetMinuteFromSessionDateTime($_SESSION['AddAvailability'][4]));
                    echo '</TD>
                        </TR>
                        <TR>
                          <TD>&nbsp;
                          </TD>
                        </TR>
                        <TR>
                          <TD>&nbsp;
                          </TD>
                        </TR>
                        <TR>
                          <TD>&nbsp;
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="4" class="center">
                            <INPUT tabindex="13" name="Submit" type="submit" class="button" value="Submit" />
                            <INPUT tabindex="14" name="Submit" type="submit" class="button" value="Cancel" />
                          </TD>
                        </TR>
                      </FORM>
                    </TABLE>
                  </DIV>
                  <DIV>
                    <BR />
                    <SPAN class="note">*
                    </SPAN>
                    These fields are required.
                  </DIV>';
          } else 
          if (isset($_SESSION['AddAvailabilityEvent']))
          {
            BuildContentHeader('Add Event', "", "", true);
            echo '<DIV class="contentflow">
                    <P>Enter the details of the event below. Ensure that all the required information is entered before submission and that this information is valid.</P>
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center">
                      <FORM method="post" action="Handlers/Availability_Handler.php">
                        <INPUT name="Type" type="hidden" value="AddEvent">
                        <TR>
                          <TD colspan="4" class="header">Event Details
                          </TD>
                        </TR>
                        <TR>
                          <TD class="short">Start Date and Time:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                            BuildDaySelector(1, 'StartDay', GetDayFromSessionDate($_SESSION['AddAvailabilityEvent'][0]));
                            echo '&nbsp;';
                            BuildMonthSelector(2, 'StartMonth', GetMonthFromSessionDate($_SESSION['AddAvailabilityEvent'][0]));
                            echo '&nbsp;';
                            BuildYearSelector(3, 'StartYear', GetYearFromSessionDate($_SESSION['AddAvailabilityEvent'][0]));
                            echo '&nbsp;at&nbsp;';
                            BuildHourSelector(4, 'EndHour', GetHourFromSessionDateTime($_SESSION['AddAvailabilityEvent'][0]));
                            echo '&nbsp;:';
                            BuildMinuteSelector(5, 'EndMinute', GetMinuteFromSessionDateTime($_SESSION['AddAvailabilityEvent'][0]));
                    echo '</TD>
                        </TR>
                        <TR>
                          <TD class="short">End Date and Time:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                            BuildDaySelector(6, 'EndDay', GetDayFromSessionDate($_SESSION['AddAvailabilityEvent'][1]));
                            echo '&nbsp;';
                            BuildMonthSelector(7, 'EndMonth', GetMonthFromSessionDate($_SESSION['AddAvailabilityEvent'][1]));
                            echo '&nbsp;';
                            BuildYearSelector(8, 'EndYear', GetYearFromSessionDate($_SESSION['AddAvailabilityEvent'][1]));
                            echo '&nbsp;at&nbsp;';
                            BuildHourSelector(9, 'EndHour', GetHourFromSessionDateTime($_SESSION['AddAvailabilityEvent'][1]));
                            echo '&nbsp;:';
                            BuildMinuteSelector(10, 'EndMinute', GetMinuteFromSessionDateTime($_SESSION['AddAvailabilityEvent'][1]));
                    echo '</TD>
                        </TR>
                        <TR>
                          <TD class="short vtop">Description:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>
                            <TEXTAREA tabindex="11" name="Description" class="long">'.$_SESSION['AddAvailabilityEvent'][2].'</TEXTAREA>
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="4" class="center">
                            <INPUT tabindex="12" name="Submit" type="submit" class="button" value="Submit" />
                            <INPUT tabindex="13" name="Submit" type="submit" class="button" value="Cancel" />
                          </TD>
                        </TR>
                      </FORM>
                    </TABLE>
                  </DIV>
                  <DIV>
                    <BR />
                    <SPAN class="note">*
                    </SPAN>
                    These fields are required.
                  </DIV>';
          } else
          if (isset($_SESSION['EditAvailability']))
          {
            if (GetDatabaseDateFromSessionDate($_SESSION['EditAvailability'][2]) == GetDatabaseDateFromSessionDate($_SESSION['EditAvailability'][4]))
              $dateRange = GetTextualDateFromSessionDate($_SESSION['EditAvailability'][2]);
            else
              $dateRange = GetTextualDateFromSessionDate($_SESSION['EditAvailability'][2]).' to '.GetTextualDateFromSessionDate($_SESSION['EditAvailability'][4]);
            
            if ($_SESSION['cAuth'] & 32)
            {
              $row = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = '.$_SESSION['EditAvailability'][5].''));
              BuildContentHeader('Edit Availability - '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].' for '.$dateRange, "", "", true);
            } else
              BuildContentHeader('Edit Availability for '.$dateRange, "", "", true);
            echo '<DIV class="contentflow">
                    <P>Once you have made changes, ensure that you submit them otherwise they will not take effect.</P>
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center" class="long">
                      <FORM method="post" action="Handlers/Availability_Handler.php">
                        <INPUT name="Type" type="hidden" value="Edit">
                        <TR>
                          <TD colspan="4" class="header">Availability Details
                          </TD>
                        </TR>
                        <TR>
                          <TD class="short">Type:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                            BuildAvailabilityTypeSelector(1, 'AvailabilityType', 'standard', $_SESSION['EditAvailability'][1]);
                    echo '</TD>
                          <TD class="short">Start Date and Time:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                            BuildDaySelector(3, 'StartDay', GetDayFromSessionDate($_SESSION['EditAvailability'][2]));
                            echo '&nbsp;';
                            BuildMonthSelector(4, 'StartMonth', GetMonthFromSessionDate($_SESSION['EditAvailability'][2]));
                            echo '&nbsp;';
                            BuildYearSelector(5, 'StartYear', GetYearFromSessionDate($_SESSION['EditAvailability'][2]));
                            echo '&nbsp;at&nbsp;';
                            BuildHourSelector(6, 'StartHour', GetHourFromSessionDateTime($_SESSION['EditAvailability'][2]));
                            echo '&nbsp;:';
                            BuildMinuteSelector(7, 'StartMinute', GetMinuteFromSessionDateTime($_SESSION['EditAvailability'][2]));
                    echo '</TD>
                        </TR>
                        <TR>
                          <TD class="short">Description:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD rowspan="4">
                            <TEXTAREA tabindex="2" name="Description" class="standard">'.$_SESSION['EditAvailability'][3].'</TEXTAREA>
                          </TD>
                          <TD class="short">End Date and Time:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                            BuildDaySelector(8, 'EndDay', GetDayFromSessionDate($_SESSION['EditAvailability'][4]));
                            echo '&nbsp;';
                            BuildMonthSelector(9, 'EndMonth', GetMonthFromSessionDate($_SESSION['EditAvailability'][4]));
                            echo '&nbsp;';
                            BuildYearSelector(10, 'EndYear', GetYearFromSessionDate($_SESSION['EditAvailability'][4]));
                            echo '&nbsp;at&nbsp;';
                            BuildHourSelector(11, 'EndHour', GetHourFromSessionDateTime($_SESSION['EditAvailability'][4]));
                            echo '&nbsp;:';
                            BuildMinuteSelector(12, 'EndMinute', GetMinuteFromSessionDateTime($_SESSION['EditAvailability'][4]));
                    echo '</TD>
                        </TR>
                        <TR>
                          <TD>&nbsp;
                          </TD>
                        </TR>
                        <TR>
                          <TD>&nbsp;
                          </TD>
                        </TR>
                        <TR>
                          <TD>&nbsp;
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="4" class="center">
                            <INPUT tabindex="13" name="Submit" type="submit" class="button" value="Submit" />   
                            <INPUT tabindex="14" name="Submit" type="submit" class="button" value="Cancel" />                  
                          </TD>
                        </TR>
                      </FORM>
                    </TABLE>  
                  </DIV>  
                  <DIV>
                    <BR />
                    <SPAN class="note">*
                    </SPAN>
                    These fields are required.
                  </DIV>';
          } else
          if (isset($_SESSION['EditAvailabilityEvent']))
          {
            if (GetDatabaseDateFromSessionDate($_SESSION['EditAvailabilityEvent'][1]) == GetDatabaseDateFromSessionDate($_SESSION['EditAvailabilityEvent'][2]))
              $dateRange = GetTextualDateFromSessionDate($_SESSION['EditAvailabilityEvent'][1]);
            else
              $dateRange = GetTextualDateFromSessionDate($_SESSION['EditAvailabilityEvent'][1]).' to '.GetTextualDateFromSessionDate($_SESSION['EditAvailability'][2]);
            
            BuildContentHeader('Edit Event for '.$dateRange, "", "", true);
            echo '<DIV class="contentflow">
                    <P>Once you have made changes, ensure that you submit them otherwise they will not take effect.</P>
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center">
                      <FORM method="post" action="Handlers/Availability_Handler.php">
                        <INPUT name="Type" type="hidden" value="EditEvent">
                        <TR>
                          <TD colspan="4" class="header">Event Details
                          </TD>
                        </TR>
                        <TR>
                          <TD class="short">Start Date and Time:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                            BuildDaySelector(1, 'StartDay', GetDayFromSessionDate($_SESSION['EditAvailabilityEvent'][1]));
                            echo '&nbsp;';
                            BuildMonthSelector(2, 'StartMonth', GetMonthFromSessionDate($_SESSION['EditAvailabilityEvent'][1]));
                            echo '&nbsp;';
                            BuildYearSelector(3, 'StartYear', GetYearFromSessionDate($_SESSION['EditAvailabilityEvent'][1]));
                            echo '&nbsp;at&nbsp;';
                            BuildHourSelector(4, 'EndHour', GetHourFromSessionDateTime($_SESSION['EditAvailabilityEvent'][1]));
                            echo '&nbsp;:';
                            BuildMinuteSelector(5, 'EndMinute', GetMinuteFromSessionDateTime($_SESSION['EditAvailabilityEvent'][1]));
                    echo '</TD>
                        </TR>
                        <TR>
                          <TD class="short">End Date and Time:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                            BuildDaySelector(6, 'EndDay', GetDayFromSessionDate($_SESSION['EditAvailabilityEvent'][2]));
                            echo '&nbsp;';
                            BuildMonthSelector(7, 'EndMonth', GetMonthFromSessionDate($_SESSION['EditAvailabilityEvent'][2]));
                            echo '&nbsp;';
                            BuildYearSelector(8, 'EndYear', GetYearFromSessionDate($_SESSION['EditAvailabilityEvent'][2]));
                            echo '&nbsp;at&nbsp;';
                            BuildHourSelector(9, 'EndHour', GetHourFromSessionDateTime($_SESSION['EditAvailabilityEvent'][2]));
                            echo '&nbsp;:';
                            BuildMinuteSelector(10, 'EndMinute', GetMinuteFromSessionDateTime($_SESSION['EditAvailabilityEvent'][2]));
                    echo '</TD>
                        </TR>
                        <TR>
                          <TD class="short vtop">Description:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>
                            <TEXTAREA tabindex="11" name="Description" class="long">'.$_SESSION['EditAvailabilityEvent'][3].'</TEXTAREA>
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="4" class="center">
                            <INPUT tabindex="12" name="Submit" type="submit" class="button" value="Submit" />
                            <INPUT tabindex="13" name="Submit" type="submit" class="button" value="Cancel" />
                          </TD>
                        </TR>
                      </FORM>
                    </TABLE>
                  </DIV>
                  <DIV>
                    <BR />
                    <SPAN class="note">*
                    </SPAN>
                    These fields are required.
                  </DIV>';
          } else
          if (isset($_SESSION['RemoveAvailability']))
          {
            $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Availability WHERE Availability_ID = "'.$_SESSION['RemoveAvailability'][0].'"'));
            $staffCode = $rowTemp['Availability_Name'];
            if ($rowTemp['Availability_Start'] == $rowTemp['Availability_End'])
              $dateRange = GetTextualDateFromDatabaseDate($rowTemp['Availability_Start']);
            else
              $dateRange = GetTextualDateFromDatabaseDate($rowTemp['Availability_Start']).' to '.GetTextualDateFromDatabaseDate($rowTemp['Availability_End']);
            $rowTempB = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM AvailabilityType WHERE AvailabilityType_ID = "'.$rowTemp['Availability_Type'].'"'));                    
            
            if ($_SESSION['cAuth'] & 32)
            {
              $row = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = '.$staffCode.''));
              BuildContentHeader('Remove Availability - '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].' for '.$dateRange, "", "", true);
            } else
              BuildContentHeader('Remove Availability for '.$dateRange, "", "", true);
            echo '<DIV class="contentflow">
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center" class="short">
                      <TR>
                        <TD colspan="4" class="header">Availability Details
                        </TD>
                      </TR>
                      <TR>
                        <TD class="short">Type:
                        </TD>
                        <TD class="bold">'.$rowTempB['AvailabilityType_Description'].'
                        </TD>
                      </TR>
                      <TR>
                        <TD class="short">Start Date and Time:
                        </TD>
                        <TD class="bold">'.GetTextualDateTimeFromDatabaseDateTime($rowTemp['Availability_Start']).'
                        </TD>
                      </TR>
                      <TR>
                        <TD class="vtop">End Date and Time:
                        </TD>
                        <TD class="vtop bold">'.GetTextualDateTimeFromDatabaseDateTime($rowTemp['Availability_End']).'
                        </TD>
                      </TR>
                      <TR>
                        <TD class="vtop">Description:
                        </TD>
                        <TD class="vtop bold">'.$rowTemp['Availability_Description'].'
                        </TD>
                      </TR>
                    </TABLE>
                    <BR /><BR />
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center" class="short">
                      <FORM method="post" action="Handlers/Availability_Handler.php">
                        <INPUT name="Type" type="hidden" value="Remove" />
                        <TR>
                          <TD colspan="4" class="header">Remove
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="4" class="center">Are you sure you wish to remove this availability?
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="2" class="center">
                            <INPUT tabindex="1" name="Submit" type="submit" class="button" value="Yes" />   
                            <INPUT tabindex="2" name="Submit" type="submit" class="button" value="No" />                  
                          </TD>
                        </TR>
                      </FORM>
                    </TABLE>  
                  </DIV>';
          } else
          if (isset($_SESSION['RemoveAvailabilityEvent']))
          {
            $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Event WHERE Event_ID = "'.$_SESSION['RemoveAvailabilityEvent'][0].'"'));
            if ($rowTemp['Event_DateTime_Start'] == $rowTemp['Event_DateTime_End'])
              $dateRange = GetTextualDateFromDatabaseDate($rowTemp['Event_DateTime_Start']);
            else
              $dateRange = GetTextualDateFromDatabaseDate($rowTemp['Event_DateTime_Start']).' to '.GetTextualDateFromDatabaseDate($rowTemp['Event_DateTime_End']);
            
            BuildContentHeader('Remove Event for '.$dateRange, "", "", true);
            echo '<DIV class="contentflow">
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center" class="short">
                      <TR>
                        <TD colspan="4" class="header">Event Details
                        </TD>
                      </TR>
                      <TR>
                        <TD class="short">Start Date and Time:
                        </TD>
                        <TD class="bold">'.GetTextualDateTimeFromDatabaseDateTime($rowTemp['Event_DateTime_Start']).'
                        </TD>
                      </TR>
                      <TR>
                        <TD class="vtop">End Date and Time:
                        </TD>
                        <TD class="vtop bold">'.GetTextualDateTimeFromDatabaseDateTime($rowTemp['Event_DateTime_End']).'
                        </TD>
                      </TR>
                      <TR>
                        <TD class="vtop">Description:
                        </TD>
                        <TD class="vtop bold">'.$rowTemp['Event_Description'].'
                        </TD>
                      </TR>
                    </TABLE>
                    <BR /><BR />
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center" class="short">
                      <FORM method="post" action="Handlers/Availability_Handler.php">
                        <INPUT name="Type" type="hidden" value="RemoveEvent" />
                        <TR>
                          <TD colspan="4" class="header">Remove
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="4" class="center">Are you sure you wish to remove this event?
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="2" class="center">
                            <INPUT tabindex="1" name="Submit" type="submit" class="button" value="Yes" />   
                            <INPUT tabindex="2" name="Submit" type="submit" class="button" value="No" />                  
                          </TD>
                        </TR>
                      </FORM>
                    </TABLE>  
                  </DIV>';
          } else
          if (isset($_SESSION['ViewAvailability']))
          {
            Session_Unregister('Graphs'); //This must be here or somewhere before otherwise there is a HUGE problem with caching and the session variables.  
            
            $startDate = GetDatabaseDateFromSessionDate($_SESSION['ViewAvailability'][0]);
            $endDate = GetDatabaseDateFromSessionDate($_SESSION['ViewAvailability'][1]);
            if ($_SESSION['ViewAvailability'][0] == $_SESSION['ViewAvailability'][1])
              $dateRange = GetTextualDateFromSessionDate($_SESSION['ViewAvailability'][0]);
            else        
              $dateRange = GetTextualDateFromSessionDate($_SESSION['ViewAvailability'][0]).' to '.GetTextualDateFromSessionDate($_SESSION['ViewAvailability'][1]);
            
            //Change start date to first day of the week.
            $startDate = Date('Y-m-d', MKTime(0, 0, 0, GetMonthFromDatabaseDate($startDate), GetDayFromDatabaseDate($startDate) + 1 - Date('N', MKTime(0, 0, 0, GetMonthFromDatabaseDate($startDate), GetDayFromDatabaseDate($startDate), GetYearFromDatabaseDate($startDate))), GetYearFromDatabaseDate($startDate)));
            
            $resultSetAvail = ExecuteQuery('SELECT * FROM AvailabilityType ORDER BY AvailabilityType_ID ASC');
            if (MySQL_Num_Rows($resultSetAvail) > 0)
            {
              $availabilities = array();
              while ($row = MySQL_Fetch_Array($resultSetAvail))
              {
                $availabilities[] = $row['AvailabilityType_Description'];
              }
            }
            
            $resultSetLeave = ExecuteQuery('SELECT * FROM LeaveType ORDER BY LeaveType_ID ASC');
            if (MySQL_Num_Rows($resultSetLeave) > 0)
            {
              $leaves = array();
              while ($row = MySQL_Fetch_Array($resultSetLeave))
              {
                $leaves[] = $row['LeaveType_Description'];
              }
            }
            
            $row = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = "'.$_SESSION['ViewAvailability'][3].'"'));
            $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM AvailabilityType WHERE AvailabilityType_ID = "'.$_SESSION['ViewAvailability'][2].'"'));                    
            
            if (Is_Numeric($_SESSION['ViewAvailability'][2]))
              $type = $rowTemp['AvailabilityType_Description'];
            else
              $type = $_SESSION['ViewAvailability'][2];
            
            $display = true;
            switch ($_SESSION['ViewAvailability'][3])
            { 
              case "":
                BuildContentHeader('Availability and Event Graphic Report ('.$type.') - '.$dateRange, "", "", true);
                switch ($_SESSION['ViewAvailability'][2])
                {
                  case 'All':
                    if ((MySQL_Num_Rows(ExecuteQuery('SELECT OvertimeBank.* FROM OvertimeBank WHERE (OvertimeBank_Start BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" OR OvertimeBank_End BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" OR (OvertimeBank_Start <= "'.$endDate.' 23:59:59" AND OvertimeBank_End >= "'.$startDate.'")) AND OvertimeBank_Payment = "3" AND OvertimeBank_IsApproved = "1" LIMIT 1')) > 0) ||
                        (MySQL_Num_Rows(ExecuteQuery('SELECT `Leave`.* FROM `Leave` WHERE ((Leave_Start >= "'.$startDate.'" AND Leave_Start <= "'.$endDate.'") OR (Leave_End >= "'.$startDate.'" AND Leave_End <= "'.$endDate.'") OR (Leave_Start <= "'.$endDate.' 23:59:59" AND Leave_End >= "'.$startDate.'")) AND Leave_Days > 0 AND Leave_IsApproved = "1" LIMIT 1')) > 0) ||
                        (MySQL_Num_Rows(ExecuteQuery('SELECT Availability.* FROM Availability WHERE (Availability_Start BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" OR Availability_End BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" OR (Availability_Start <= "'.$startDate.' 23:59:59" AND Availability_End >= "'.$endDate.'")) AND Availability_Type = "'.$_SESSION['ViewAvailability'][2].'" LIMIT 1')) > 0) ||
                        (MySQL_Num_Rows(ExecuteQuery('SELECT Event.* FROM Event WHERE (Event_DateTime_Start BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" OR Event_DateTime_End BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" OR (Event_DateTime_Start <= "'.$startDate.' 23:59:59" AND Event_DateTime_End >= "'.$endDate.'")) LIMIT 1')) > 0))
                      BuildGanttChart(array('SELECT CONCAT(Staff_First_Name, " ", SUBSTRING(Staff_Last_Name, 1, 1)) AS Staff_First_Name FROM Staff WHERE Staff_IsEmployee > 0 ORDER BY Staff_First_Name, Staff_Last_Name ASC',
                                            'SELECT CONCAT(Staff_First_Name, " ", SUBSTRING(Staff_Last_Name, 1, 1)) AS Staff_First_Name, OvertimeBank.* FROM OvertimeBank, Staff WHERE OvertimeBank_Name = Staff_Code AND (OvertimeBank_Start BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" OR OvertimeBank_End BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" OR (OvertimeBank_Start <= "'.$endDate.' 23:59:59" AND OvertimeBank_End >= "'.$startDate.'")) AND OvertimeBank_Payment = "3" AND OvertimeBank_IsApproved = "1" ORDER BY Staff_First_Name, Staff_Last_Name ASC',
                                            'SELECT CONCAT(Staff_First_Name, " ", SUBSTRING(Staff_Last_Name, 1, 1)) AS Staff_First_Name, `Leave`.* FROM `Leave`, Staff WHERE Leave_Employee = Staff_Code AND ((Leave_Start >= "'.$startDate.'" AND Leave_Start <= "'.$endDate.'") OR (Leave_End >= "'.$startDate.'" AND Leave_End <= "'.$endDate.'") OR (Leave_Start <= "'.$endDate.' 23:59:59" AND Leave_End >= "'.$startDate.'")) AND Leave_Days > 0 AND Leave_IsApproved = "1" ORDER BY Staff_First_Name, Staff_Last_Name ASC',
                                            'SELECT CONCAT(Staff_First_Name, " ", SUBSTRING(Staff_Last_Name, 1, 1)) AS Staff_First_Name, Availability.* FROM Availability, Staff WHERE Availability_Name = Staff_Code AND (Availability_Start BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" OR Availability_End BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" OR (Availability_Start <= "'.$startDate.' 23:59:59" AND Availability_End >= "'.$endDate.'")) ORDER BY Staff_First_Name, Staff_Last_Name ASC',
                                            'SELECT CONCAT(Staff_First_Name, " ", SUBSTRING(Staff_Last_Name, 1, 1)) AS Staff_First_Name, `Leave`.* FROM `Leave`, Staff WHERE Leave_Employee = Staff_Code AND ((Leave_Start >= "'.$startDate.'" AND Leave_Start <= "'.$endDate.'") OR (Leave_End >= "'.$startDate.'" AND Leave_End <= "'.$endDate.'") OR (Leave_Start <= "'.$endDate.' 23:59:59" AND Leave_End >= "'.$startDate.'")) AND Leave_Days > 0 AND Leave_IsApproved = "0" ORDER BY Staff_First_Name, Staff_Last_Name ASC',
                                            'SELECT CONCAT(Staff_First_Name, " ", SUBSTRING(Staff_Last_Name, 1, 1)) AS Staff_First_Name, OvertimeBank.* FROM OvertimeBank, Staff WHERE OvertimeBank_Name = Staff_Code AND (OvertimeBank_Start BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" OR OvertimeBank_End BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" OR (OvertimeBank_Start <= "'.$endDate.' 23:59:59" AND OvertimeBank_End >= "'.$startDate.'")) AND OvertimeBank_Payment = "3" AND OvertimeBank_IsApproved = "0" ORDER BY Staff_First_Name, Staff_Last_Name ASC',
                                            'SELECT " " AS Space',
                                            'SELECT IF(CHAR_LENGTH(Event_Description) > 20, SUBSTRING(Event_Description, 1, 15), Event_Description) AS Description, Event.* FROM Event WHERE (Event_DateTime_Start BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" OR Event_DateTime_End BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" OR (Event_DateTime_Start <= "'.$endDate.' 23:59:59" AND Event_DateTime_End >= "'.$startDate.'")) ORDER BY Event_Description ASC'), "", "", 'Availability for '.$dateRange, $startDate, $endDate, 'Monthly', 
                                      array(array('Staff_First_Name'),
                                            array('Staff_First_Name', 'OvertimeBank_Start', 'OvertimeBank_End', 'OvertimeBank_Payment'),
                                            array('Staff_First_Name', 'Leave_Start', 'Leave_End', 'Leave_Type'),
                                            array('Staff_First_Name', 'Availability_Start', 'Availability_End', 'Availability_Type'),
                                            array('Staff_First_Name', 'Leave_Start', 'Leave_End', 'Leave_Type'),
                                            array('Staff_First_Name', 'OvertimeBank_Start', 'OvertimeBank_End', 'OvertimeBank_Payment'),
                                            array('Space'),
                                            array('Description', 'Event_DateTime_Start', 'Event_DateTime_End')), 
                                      array(array(),
                                            array("", "", 'Banked Overtime'),
                                            $leaves,//array('Annual', 'Sick', 'Maternity/Paternity', 'Compassionate', 'Study', 'Unpaid', 'Other'),
                                            $availabilities,//array('Training', 'Travel', 'Visitor'),
                                            array('Pending'),
                                            array(),
                                            array(),
                                            array('Event')),
                                      array(array(),
                                            array("", "", '#FF511C'),
                                            array('#329E1F', '#03B272', '#0BB7AC', '#0B9BC6', '#0C61C9', '#6919FF', '#A215D6'),
                                            array('#FF11FB', '#FF0F83', '#DB0A2D', '#FFAA42', '#FFE049', '#B4FF32'),
                                            array('#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF'),
                                            array("", "", '#FFFFFF'),
                                            array(),
                                            array('#BFCDDB')));
                    else
                      $display = false;
                    break;
                  case 'Banked Overtime Use':
                    if (MySQL_Num_Rows(ExecuteQuery('SELECT CONCAT(Staff_First_Name, " ", SUBSTRING(Staff_Last_Name, 1, 1)) AS Staff_First_Name, OvertimeBank.* FROM OvertimeBank, Staff WHERE OvertimeBank_Name = Staff_Code AND (OvertimeBank_Start BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" OR OvertimeBank_End BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" OR (OvertimeBank_Start <= "'.$endDate.' 23:59:59" AND OvertimeBank_End >= "'.$startDate.'")) AND OvertimeBank_Payment = "3" AND OvertimeBank_IsApproved = "1" LIMIT 1')) > 0)
                      BuildGanttChart(array('SELECT CONCAT(Staff_First_Name, " ", SUBSTRING(Staff_Last_Name, 1, 1)) AS Staff_First_Name FROM Staff WHERE Staff_IsEmployee > 0 ORDER BY Staff_First_Name, Staff_Last_Name ASC',
                                            'SELECT CONCAT(Staff_First_Name, " ", SUBSTRING(Staff_Last_Name, 1, 1)) AS Staff_First_Name, OvertimeBank.* FROM OvertimeBank, Staff WHERE OvertimeBank_Name = Staff_Code AND (OvertimeBank_Start BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" OR OvertimeBank_End BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" OR (OvertimeBank_Start <= "'.$endDate.' 23:59:59" AND OvertimeBank_End >= "'.$startDate.'")) AND OvertimeBank_Payment = "3" AND OvertimeBank_IsApproved = "1" ORDER BY Staff_First_Name, Staff_Last_Name ASC',
                                            'SELECT CONCAT(Staff_First_Name, " ", SUBSTRING(Staff_Last_Name, 1, 1)) AS Staff_First_Name, `Leave`.* FROM `Leave`, Staff WHERE Leave_Employee = Staff_Code AND ((Leave_Start >= "'.$startDate.'" AND Leave_Start <= "'.$endDate.'") OR (Leave_End >= "'.$startDate.'" AND Leave_End <= "'.$endDate.'")) AND Leave_Days > 0 AND Leave_IsApproved = "0" ORDER BY Staff_First_Name, Staff_Last_Name ASC',
                                            'SELECT CONCAT(Staff_First_Name, " ", SUBSTRING(Staff_Last_Name, 1, 1)) AS Staff_First_Name, OvertimeBank.* FROM OvertimeBank, Staff WHERE OvertimeBank_Name = Staff_Code AND (OvertimeBank_Start BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" OR OvertimeBank_End BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" OR (OvertimeBank_Start <= "'.$endDate.' 23:59:59" AND OvertimeBank_End >= "'.$startDate.'")) AND OvertimeBank_Payment = "3" AND OvertimeBank_IsApproved = "0" ORDER BY Staff_First_Name, Staff_Last_Name ASC'), "", "", 'Banked Overtime Use for '.$dateRange, $startDate, $endDate, 'Monthly', 
                                      array(array('Staff_First_Name'),
                                            array('Staff_First_Name', 'OvertimeBank_Start', 'OvertimeBank_End', 'OvertimeBank_Payment'),
                                            array('Staff_First_Name', 'Leave_Start', 'Leave_End', 'Leave_Type'),
                                            array('Staff_First_Name', 'OvertimeBank_Start', 'OvertimeBank_End', 'OvertimeBank_Payment')), 
                                      array(array(),
                                            array("", "", 'Banked Overtime'),
                                            array('Pending'),
                                            array()), 
                                      array(array(),
                                            array("", "", '#FF511C'),
                                            array('#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF'),
                                            array("", "", '#FFFFFF')));                            
                    else
                      $display = false;
                    break;
                  case 'Event':
                    echo 'SELECT Event.* FROM Event WHERE (Event_DateTime_Start BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" OR Event_DateTime_End BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" OR (Event_DateTime_Start <= "'.$startDate.' 23:59:59" AND Event_DateTime_End >= "'.$endDate.'")) AND Event_DateTime_Type = "'.$_SESSION['ViewAvailability'][2].'" LIMIT 1';
                    if (MySQL_Num_Rows(ExecuteQuery('SELECT Event.* FROM Event WHERE (Event_DateTime_Start BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" OR Event_DateTime_End BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" OR (Event_DateTime_Start <= "'.$startDate.' 23:59:59" AND Event_DateTime_End >= "'.$endDate.'")) LIMIT 1')) > 0)
                      BuildGanttChart(array('SELECT IF(CHAR_LENGTH(Event_Description) > 20, SUBSTRING(Event_Description, 1, 15), Event_Description) AS Description, Event.* FROM Event WHERE (Event_DateTime_Start BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" OR Event_DateTime_End BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" OR (Event_DateTime_Start <= "'.$endDate.' 23:59:59" AND Event_DateTime_End >= "'.$startDate.'")) ORDER BY Event_Description ASC'), "", "", 'Events for '.$dateRange, $startDate, $endDate, 'Monthly', 
                                      array(array('Description', 'Event_DateTime_Start', 'Event_DateTime_End')), 
                                      array(array('Event')),
                                      array(array('#BFCDDB')));
                    else
                      $display = false;
                    break;
                  case 'Leave':
                    if (MySQL_Num_Rows(ExecuteQuery('SELECT CONCAT(Staff_First_Name, " ", SUBSTRING(Staff_Last_Name, 1, 1)) AS Staff_First_Name, `Leave`.* FROM `Leave`, Staff WHERE Leave_Employee = Staff_Code AND ((Leave_Start >= "'.$startDate.'" AND Leave_Start <= "'.$endDate.'") OR (Leave_End >= "'.$startDate.'" AND Leave_End <= "'.$endDate.'") OR (Leave_Start <= "'.$endDate.' 23:59:59" AND Leave_End >= "'.$startDate.'")) AND Leave_Days > 0 AND Leave_IsApproved = "1" ORDER BY Staff_First_Name, Staff_Last_Name ASC LIMIT 1')) > 0)
                      BuildGanttChart(array('SELECT CONCAT(Staff_First_Name, " ", SUBSTRING(Staff_Last_Name, 1, 1)) AS Staff_First_Name FROM Staff WHERE Staff_IsEmployee > 0 ORDER BY Staff_First_Name, Staff_Last_Name ASC',
                                            'SELECT CONCAT(Staff_First_Name, " ", SUBSTRING(Staff_Last_Name, 1, 1)) AS Staff_First_Name, `Leave`.* FROM `Leave`, Staff WHERE Leave_Employee = Staff_Code AND ((Leave_Start >= "'.$startDate.'" AND Leave_Start <= "'.$endDate.'") OR (Leave_End >= "'.$startDate.'" AND Leave_End <= "'.$endDate.'")) AND Leave_Days > 0 AND Leave_IsApproved = "1" ORDER BY Staff_First_Name, Staff_Last_Name ASC',
                                            'SELECT CONCAT(Staff_First_Name, " ", SUBSTRING(Staff_Last_Name, 1, 1)) AS Staff_First_Name, `Leave`.* FROM `Leave`, Staff WHERE Leave_Employee = Staff_Code AND ((Leave_Start >= "'.$startDate.'" AND Leave_Start <= "'.$endDate.'") OR (Leave_End >= "'.$startDate.'" AND Leave_End <= "'.$endDate.'")) AND Leave_Days > 0 AND Leave_IsApproved = "0" ORDER BY Staff_First_Name, Staff_Last_Name ASC',
                                            'SELECT CONCAT(Staff_First_Name, " ", SUBSTRING(Staff_Last_Name, 1, 1)) AS Staff_First_Name, OvertimeBank.* FROM OvertimeBank, Staff WHERE OvertimeBank_Name = Staff_Code AND (OvertimeBank_Start BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" OR OvertimeBank_End BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" OR (OvertimeBank_Start <= "'.$endDate.' 23:59:59" AND OvertimeBank_End >= "'.$startDate.'")) AND OvertimeBank_Payment = "3" AND OvertimeBank_IsApproved = "0" ORDER BY Staff_First_Name, Staff_Last_Name ASC'), "", "", 'Leave for '.$dateRange, $startDate, $endDate, 'Monthly', 
                                      array(array('Staff_First_Name'),
                                            array('Staff_First_Name', 'Leave_Start', 'Leave_End', 'Leave_Type'),
                                            array('Staff_First_Name', 'Leave_Start', 'Leave_End', 'Leave_Type'),
                                            array('Staff_First_Name', 'OvertimeBank_Start', 'OvertimeBank_End', 'OvertimeBank_Payment')),
                                      array(array(),
                                            $leaves,//array('Annual', 'Sick', 'Maternity/Paternity', 'Compassionate', 'Study', 'Unpaid', 'Other'),
                                            array('Pending'),
                                            array()),
                                      array(array(),
                                            array('#329E1F', '#03B272', '#0BB7AC', '#0B9BC6', '#0C61C9', '#6919FF', '#A215D6'),
                                            array('#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF'),
                                            array("", "", '#FFFFFF')));                            
                    else
                      $display = false;
                    break;
                  default:
                    if (MySQL_Num_Rows(ExecuteQuery('SELECT CONCAT(Staff_First_Name, " ", SUBSTRING(Staff_Last_Name, 1, 1)) AS Staff_First_Name, Availability.* FROM Availability, Staff WHERE Availability_Name = Staff_Code AND (Availability_Start BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" OR Availability_End BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" OR (Availability_Start <= "'.$startDate.' 23:59:59" AND Availability_End >= "'.$endDate.'")) AND Availability_Type = "'.$_SESSION['ViewAvailability'][2].'" LIMIT 1')) > 0)
                      BuildGanttChart(array('SELECT CONCAT(Staff_First_Name, " ", SUBSTRING(Staff_Last_Name, 1, 1)) AS Staff_First_Name FROM Staff WHERE Staff_IsEmployee > 0 ORDER BY Staff_First_Name, Staff_Last_Name ASC',
                                            'SELECT CONCAT(Staff_First_Name, " ", SUBSTRING(Staff_Last_Name, 1, 1)) AS Staff_First_Name, Availability.* FROM Availability, Staff WHERE Availability_Name = Staff_Code AND (Availability_Start BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" OR Availability_End BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" OR (Availability_Start <= "'.$startDate.' 23:59:59" AND Availability_End >= "'.$endDate.'")) AND Availability_Type = "'.$_SESSION['ViewAvailability'][2].'" ORDER BY Staff_First_Name, Staff_Last_Name ASC',
                                            'SELECT CONCAT(Staff_First_Name, " ", SUBSTRING(Staff_Last_Name, 1, 1)) AS Staff_First_Name, `Leave`.* FROM `Leave`, Staff WHERE Leave_Employee = Staff_Code AND ((Leave_Start >= "'.$startDate.'" AND Leave_Start <= "'.$endDate.'") OR (Leave_End >= "'.$startDate.'" AND Leave_End <= "'.$endDate.'")) AND Leave_Days > 0 AND Leave_IsApproved = "0" ORDER BY Staff_First_Name, Staff_Last_Name ASC',
                                            'SELECT CONCAT(Staff_First_Name, " ", SUBSTRING(Staff_Last_Name, 1, 1)) AS Staff_First_Name, OvertimeBank.* FROM OvertimeBank, Staff WHERE OvertimeBank_Name = Staff_Code AND (OvertimeBank_Start BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" OR OvertimeBank_End BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" OR (OvertimeBank_Start <= "'.$endDate.' 23:59:59" AND OvertimeBank_End >= "'.$startDate.'")) AND OvertimeBank_Payment = "3" AND OvertimeBank_IsApproved = "0" ORDER BY Staff_First_Name, Staff_Last_Name ASC'), "", "", $type.' for '.$dateRange, $startDate, $endDate, 'Monthly', 
                                      array(array('Staff_First_Name'),
                                            array('Staff_First_Name', 'Availability_Start', 'Availability_End', 'Availability_Type'),
                                            array('Staff_First_Name', 'Leave_Start', 'Leave_End', 'Leave_Type'),
                                            array('Staff_First_Name', 'OvertimeBank_Start', 'OvertimeBank_End', 'OvertimeBank_Payment')), 
                                      array(array(),
                                            $availabilities,//array('Training', 'Travel', 'Visitor'),
                                            array('Pending'),
                                            array()),
                                      array(array(),
                                            array('#FF11FB', '#FF0F83', '#DB0A2D', '#FFAA42', '#FFE049', '#B4FF32'),
                                            array('#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF'),
                                            array("", "", '#FFFFFF')));                            
                    else
                      $display = false;
                    break;
                }
                break;
              default:   
                BuildContentHeader('Availability and Event Graphic Report ('.$type.') - '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].' for '.$dateRange, "", "", true);
                switch ($_SESSION['ViewAvailability'][2])
                {
                  case 'All':
                    if ((MySQL_Num_Rows(ExecuteQuery('SELECT CONCAT(Staff_First_Name, " ", SUBSTRING(Staff_Last_Name, 1, 1)) AS Staff_First_Name, OvertimeBank.* FROM OvertimeBank, Staff WHERE OvertimeBank_Name = Staff_Code AND OvertimeBank_Name = '.$_SESSION['ViewAvailability'][3].' AND (OvertimeBank_Start BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" OR OvertimeBank_End BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" OR (OvertimeBank_Start <= "'.$endDate.' 23:59:59" AND OvertimeBank_End >= "'.$startDate.'")) AND OvertimeBank_Payment = "3" AND OvertimeBank_IsApproved = "1" LIMIT 1')) > 0) ||
                        (MySQL_Num_Rows(ExecuteQuery('SELECT CONCAT(Staff_First_Name, " ", SUBSTRING(Staff_Last_Name, 1, 1)) AS Staff_First_Name, `Leave`.* FROM `Leave`, Staff WHERE Leave_Employee = Staff_Code AND Leave_Employee = '.$_SESSION['ViewAvailability'][3].' AND ((Leave_Start >= "'.$startDate.'" AND Leave_Start <= "'.$endDate.'") OR (Leave_End >= "'.$startDate.'" AND Leave_End <= "'.$endDate.'") OR (Leave_Start <= "'.$endDate.' 23:59:59" AND Leave_End >= "'.$startDate.'")) AND Leave_Days > 0 AND Leave_IsApproved = "1" LIMIT 1')) > 0) ||
                        (MySQL_Num_Rows(ExecuteQuery('SELECT CONCAT(Staff_First_Name, " ", SUBSTRING(Staff_Last_Name, 1, 1)) AS Staff_First_Name, Availability.* FROM Availability, Staff WHERE Availability_Name = Staff_Code AND Availability_Name = '.$_SESSION['ViewAvailability'][3].' AND (Availability_Start BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" OR Availability_End BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" OR (Availability_Start <= "'.$startDate.' 23:59:59" AND Availability_End >= "'.$endDate.'")) LIMIT 1')) > 0))
                        BuildGanttChart(array('SELECT CONCAT(Staff_First_Name, " ", SUBSTRING(Staff_Last_Name, 1, 1)) AS Staff_First_Name, OvertimeBank.* FROM OvertimeBank, Staff WHERE OvertimeBank_Name = Staff_Code AND OvertimeBank_Name = '.$_SESSION['ViewAvailability'][3].' AND (OvertimeBank_Start BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" OR OvertimeBank_End BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" OR (OvertimeBank_Start <= "'.$endDate.' 23:59:59" AND OvertimeBank_End >= "'.$startDate.'")) AND OvertimeBank_Payment = "3" AND OvertimeBank_IsApproved = "1"',
                                              'SELECT CONCAT(Staff_First_Name, " ", SUBSTRING(Staff_Last_Name, 1, 1)) AS Staff_First_Name, `Leave`.* FROM `Leave`, Staff WHERE Leave_Employee = Staff_Code AND Leave_Employee = '.$_SESSION['ViewAvailability'][3].' AND ((Leave_Start >= "'.$startDate.'" AND Leave_Start <= "'.$endDate.'") OR (Leave_End >= "'.$startDate.'" AND Leave_End <= "'.$endDate.'") OR (Leave_Start <= "'.$endDate.' 23:59:59" AND Leave_End >= "'.$startDate.'")) AND Leave_Days > 0 AND Leave_IsApproved = "1"',
                                              'SELECT CONCAT(Staff_First_Name, " ", SUBSTRING(Staff_Last_Name, 1, 1)) AS Staff_First_Name, Availability.* FROM Availability, Staff WHERE Availability_Name = Staff_Code AND Availability_Name = '.$_SESSION['ViewAvailability'][3].' AND (Availability_Start BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" OR Availability_End BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" OR (Availability_Start <= "'.$startDate.' 23:59:59" AND Availability_End >= "'.$endDate.'"))',
                                              'SELECT CONCAT(Staff_First_Name, " ", SUBSTRING(Staff_Last_Name, 1, 1)) AS Staff_First_Name, `Leave`.* FROM `Leave`, Staff WHERE Leave_Employee = Staff_Code AND Leave_Employee = '.$_SESSION['ViewAvailability'][3].' AND ((Leave_Start >= "'.$startDate.'" AND Leave_Start <= "'.$endDate.'") OR (Leave_End >= "'.$startDate.'" AND Leave_End <= "'.$endDate.'") OR (Leave_Start <= "'.$endDate.' 23:59:59" AND Leave_End >= "'.$startDate.'")) AND Leave_Days > 0 AND Leave_IsApproved = "0"',
                                              'SELECT " " AS Space',
                                              'SELECT IF(CHAR_LENGTH(Event_Description) > 20, SUBSTRING(Event_Description, 1, 15), Event_Description) AS Description, Event.* FROM Event WHERE (Event_DateTime_Start BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" OR Event_DateTime_End BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" OR (Event_DateTime_Start <= "'.$endDate.' 23:59:59" AND Event_DateTime_End >= "'.$startDate.'")) ORDER BY Event_Description ASC'), "", "", 'Availability for '.$dateRange, $startDate, $endDate, 'Monthly', 
                                        array(array('Staff_First_Name', 'OvertimeBank_Start', 'OvertimeBank_End', 'OvertimeBank_Payment'),
                                              array('Staff_First_Name', 'Leave_Start', 'Leave_End', 'Leave_Type'),
                                              array('Staff_First_Name', 'Availability_Start', 'Availability_End', 'Availability_Type'),
                                              array('Staff_First_Name', 'Leave_Start', 'Leave_End', 'Leave_Type'),
                                              array('Space'),
                                              array('Description', 'Event_DateTime_Start', 'Event_DateTime_End')), 
                                        array(array("", "", 'Banked Overtime'),
                                              $leaves,//array('Annual', 'Sick', 'Maternity/Paternity', 'Compassionate', 'Study', 'Unpaid', 'Other'),
                                              $availabilities,//array('Training', 'Travel', 'Visitor'),
                                              array('Pending'),
                                              array(),
                                              array(),
                                              array('Event')),
                                        array(array("", "", '#FF511C'),
                                              array('#329E1F', '#03B272', '#0BB7AC', '#0B9BC6', '#0C61C9', '#6919FF', '#A215D6'),
                                              array('#FF11FB', '#FF0F83', '#DB0A2D', '#FFAA42', '#FFE049', '#B4FF32'),
                                              array('#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF'),
                                              array("", "", "", '#FFFFFF'),
                                              array(),
                                              array('#BFCDDB')));
                    else
                      $display = false;
                    $image = 'AvailabilityAll';
                    break;
                  case 'Banked Overtime Use':
                    if (MySQL_Num_Rows(ExecuteQuery('SELECT CONCAT(Staff_First_Name, " ", SUBSTRING(Staff_Last_Name, 1, 1)) AS Staff_First_Name, OvertimeBank.* FROM OvertimeBank, Staff WHERE OvertimeBank_Name = Staff_Code AND OvertimeBank_Name = '.$_SESSION['ViewAvailability'][3].' AND (OvertimeBank_Start BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" OR OvertimeBank_End BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" OR (OvertimeBank_Start <= "'.$endDate.' 23:59:59" AND OvertimeBank_End >= "'.$startDate.'")) AND OvertimeBank_Payment = "3" AND OvertimeBank_IsApproved = "1" LIMIT 1')) > 0)
                      BuildGanttChart(array('SELECT CONCAT(Staff_First_Name, " ", SUBSTRING(Staff_Last_Name, 1, 1)) AS Staff_First_Name, OvertimeBank.* FROM OvertimeBank, Staff WHERE OvertimeBank_Name = Staff_Code AND OvertimeBank_Name = '.$_SESSION['ViewAvailability'][3].' AND (OvertimeBank_Start BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" OR OvertimeBank_End BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" OR (OvertimeBank_Start <= "'.$endDate.' 23:59:59" AND OvertimeBank_End >= "'.$startDate.'")) AND OvertimeBank_Payment = "3" AND OvertimeBank_IsApproved = "1"',
                                            'SELECT CONCAT(Staff_First_Name, " ", SUBSTRING(Staff_Last_Name, 1, 1)) AS Staff_First_Name, `Leave`.* FROM `Leave`, Staff WHERE Leave_Employee = Staff_Code AND Leave_Employee = '.$_SESSION['ViewAvailability'][3].' AND ((Leave_Start >= "'.$startDate.'" AND Leave_Start <= "'.$endDate.'") OR (Leave_End >= "'.$startDate.'" AND Leave_End <= "'.$endDate.'") OR (Leave_Start <= "'.$endDate.' 23:59:59" AND Leave_End >= "'.$startDate.'")) AND Leave_Days > 0 AND Leave_IsApproved = "0"',
                                            'SELECT CONCAT(Staff_First_Name, " ", SUBSTRING(Staff_Last_Name, 1, 1)) AS Staff_First_Name, OvertimeBank.* FROM OvertimeBank, Staff WHERE OvertimeBank_Name = Staff_Code AND OvertimeBank_Name = '.$_SESSION['ViewAvailability'][3].' AND (OvertimeBank_Start BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" OR OvertimeBank_End BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" OR (OvertimeBank_Start <= "'.$endDate.' 23:59:59" AND OvertimeBank_End >= "'.$startDate.'")) AND OvertimeBank_Payment = "3" AND OvertimeBank_IsApproved = "0"'), "", "", 'Banked Overtime Use for '.$dateRange, $startDate, $endDate, 'Monthly', 
                                      array(array('Staff_First_Name', 'OvertimeBank_Start', 'OvertimeBank_End', 'OvertimeBank_Payment'),
                                            array('Staff_First_Name', 'Leave_Start', 'Leave_End', 'Leave_Type'),
                                            array('Staff_First_Name', 'OvertimeBank_Start', 'OvertimeBank_End', 'OvertimeBank_Payment')), 
                                      array(array("", "", 'Banked Overtime'),
                                            array('Pending'),
                                            array()), 
                                      array(array("", "", '#FF511C'),
                                            array('#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF'),
                                            array("", "", '#FFFFFF')));                            
                    else
                      $display = false;
                    break;
                  case 'Event':
                    //Not in use.
                    $display = false;
                    break;
                  case 'Leave':
                    if (MySQL_Num_Rows(ExecuteQuery('SELECT CONCAT(Staff_First_Name, " ", SUBSTRING(Staff_Last_Name, 1, 1)) AS Staff_First_Name, `Leave`.* FROM `Leave`, Staff WHERE Leave_Employee = Staff_Code AND Leave_Employee = '.$_SESSION['ViewAvailability'][3].' AND ((Leave_Start >= "'.$startDate.'" AND Leave_Start <= "'.$endDate.'") OR (Leave_End >= "'.$startDate.'" AND Leave_End <= "'.$endDate.'") OR (Leave_Start <= "'.$endDate.' 23:59:59" AND Leave_End >= "'.$startDate.'")) AND Leave_Days > 0 AND Leave_IsApproved = "1" ORDER BY Staff_First_Name, Staff_Last_Name ASC LIMIT 1')) > 0)
                      BuildGanttChart(array('SELECT CONCAT(Staff_First_Name, " ", SUBSTRING(Staff_Last_Name, 1, 1)) AS Staff_First_Name, `Leave`.* FROM `Leave`, Staff WHERE Leave_Employee = Staff_Code AND Leave_Employee = '.$_SESSION['ViewAvailability'][3].' AND ((Leave_Start >= "'.$startDate.'" AND Leave_Start <= "'.$endDate.'") OR (Leave_End >= "'.$startDate.'" AND Leave_End <= "'.$endDate.'") OR (Leave_Start <= "'.$endDate.' 23:59:59" AND Leave_End >= "'.$startDate.'")) AND Leave_Days > 0 AND Leave_IsApproved = "1"',
                                            'SELECT CONCAT(Staff_First_Name, " ", SUBSTRING(Staff_Last_Name, 1, 1)) AS Staff_First_Name, `Leave`.* FROM `Leave`, Staff WHERE Leave_Employee = Staff_Code AND Leave_Employee = '.$_SESSION['ViewAvailability'][3].' AND ((Leave_Start >= "'.$startDate.'" AND Leave_Start <= "'.$endDate.'") OR (Leave_End >= "'.$startDate.'" AND Leave_End <= "'.$endDate.'") OR (Leave_Start <= "'.$endDate.' 23:59:59" AND Leave_End >= "'.$startDate.'")) AND Leave_Days > 0 AND Leave_IsApproved = "0"',
                                            'SELECT CONCAT(Staff_First_Name, " ", SUBSTRING(Staff_Last_Name, 1, 1)) AS Staff_First_Name, OvertimeBank.* FROM OvertimeBank, Staff WHERE OvertimeBank_Name = Staff_Code AND OvertimeBank_Name = '.$_SESSION['ViewAvailability'][3].' AND (OvertimeBank_Start BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" OR OvertimeBank_End BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" OR (OvertimeBank_Start <= "'.$endDate.' 23:59:59" AND OvertimeBank_End >= "'.$startDate.'")) AND OvertimeBank_Payment = "3" AND OvertimeBank_IsApproved = "0"'), "", "", 'Leave for '.$dateRange, $startDate, $endDate, 'Monthly', 
                                      array(array('Staff_First_Name', 'Leave_Start', 'Leave_End', 'Leave_Type'),
                                            array('Staff_First_Name', 'Leave_Start', 'Leave_End', 'Leave_Type'),
                                            array('Staff_First_Name', 'OvertimeBank_Start', 'OvertimeBank_End', 'OvertimeBank_Payment')), 
                                      array($leaves,//array('Annual', 'Sick', 'Maternity/Paternity', 'Compassionate', 'Study', 'Unpaid', 'Other'),
                                            array('Pending'),
                                            array()), 
                                      array(array('#329E1F', '#03B272', '#0BB7AC', '#0B9BC6', '#0C61C9', '#6919FF', '#A215D6'),
                                            array('#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF'),
                                            array("", "", '#FFFFFF')));                            
                    else
                      $display = false;
                    break;
                  default:
                    if (MySQL_Num_Rows(ExecuteQuery('SELECT CONCAT(Staff_First_Name, " ", SUBSTRING(Staff_Last_Name, 1, 1)) AS Staff_First_Name, Availability.* FROM Availability, Staff WHERE Availability_Name = Staff_Code AND Availability_Name = '.$_SESSION['ViewAvailability'][3].' AND (Availability_Start BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" OR Availability_End BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" OR (Availability_Start <= "'.$startDate.' 23:59:59" AND Availability_End >= "'.$endDate.'")) AND Availability_Type = "'.$_SESSION['ViewAvailability'][2].'" LIMIT 1')) > 0)
                      BuildGanttChart(array('SELECT CONCAT(Staff_First_Name, " ", SUBSTRING(Staff_Last_Name, 1, 1)) AS Staff_First_Name, Availability.* FROM Availability, Staff WHERE Availability_Name = Staff_Code AND Availability_Name = '.$_SESSION['ViewAvailability'][3].' AND (Availability_Start BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" OR Availability_End BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" OR (Availability_Start <= "'.$startDate.' 23:59:59" AND Availability_End >= "'.$endDate.'")) AND Availability_Type = "'.$_SESSION['ViewAvailability'][2].'"',
                                            'SELECT CONCAT(Staff_First_Name, " ", SUBSTRING(Staff_Last_Name, 1, 1)) AS Staff_First_Name, `Leave`.* FROM `Leave`, Staff WHERE Leave_Employee = Staff_Code AND Leave_Employee = '.$_SESSION['ViewAvailability'][3].' AND ((Leave_Start >= "'.$startDate.'" AND Leave_Start <= "'.$endDate.'") OR (Leave_End >= "'.$startDate.'" AND Leave_End <= "'.$endDate.'") OR (Leave_Start <= "'.$endDate.' 23:59:59" AND Leave_End >= "'.$startDate.'")) AND Leave_Days > 0 AND Leave_IsApproved = "0"',
                                            'SELECT CONCAT(Staff_First_Name, " ", SUBSTRING(Staff_Last_Name, 1, 1)) AS Staff_First_Name, OvertimeBank.* FROM OvertimeBank, Staff WHERE OvertimeBank_Name = Staff_Code AND OvertimeBank_Name = '.$_SESSION['ViewAvailability'][3].' AND (OvertimeBank_Start BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" OR OvertimeBank_End BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" OR (OvertimeBank_Start <= "'.$endDate.' 23:59:59" AND OvertimeBank_End >= "'.$startDate.'")) AND OvertimeBank_Payment = "3" AND OvertimeBank_IsApproved = "0"'), "", "", $type.' for '.$dateRange, $startDate, $endDate, 'Monthly', 
                                      array(array('Staff_First_Name', 'Availability_Start', 'Availability_End', 'Availability_Type'),
                                            array('Staff_First_Name', 'Leave_Start', 'Leave_End', 'Leave_Type'),
                                            array('Staff_First_Name', 'OvertimeBank_Start', 'OvertimeBank_End', 'OvertimeBank_Payment')), 
                                      array($availabilities,//array('Training', 'Travel', 'Visitor'),
                                            array('Pending'),
                                            array()),
                                      array(array('#FF11FB', '#FF0F83', '#DB0A2D', '#FFAA42', '#FFE049', '#B4FF32'),
                                            array('#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF'),
                                            array("", "", '#FFFFFF')));                            
                    else
                      $display = false;
                    break;
                }
                break;
            }
            if ($display)
              echo '<DIV class="contentflow">
                      <P>These are the availability and events.</P>
                      <BR /><BR />
                      <COMMENT>
                        <IMG src="Scripts/Graphing.php?Graph=0&Random='.Rand().'" />
                      </COMMENT>
                      <!--[if lte IE 6]>
                        <IFRAME src="Scripts/Graphing.php?Graph=0&Random='.Rand().'" width="900" height="600" frameborder="0"></IFRAME>
                      <![endif]-->
                      <!--[if gt IE 6]>
                        <IMG src="Scripts/Graphing.php?Graph=0&Random='.Rand().'" />
                      <![endif]-->
                    </DIV>
                    <BR /><BR />'; 
            else
              echo '<DIV class="contentflow">
                      <P>No data has been recorded for the given date range.</P>
                    </DIV>';
            Session_Unregister('ViewAvailability');
          } else
          {
            BuildContentHeader('Maintenance - Availability', "", "", true);
            echo '<DIV class="contentflow">
                    <P>Availability can be recorded by using the form below. Details on availability already listed can also be edited or removed.</P>
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center" class="standard">
                      <FORM method="post" action="Handlers/Availability_Handler.php">
                        <INPUT name="Type" type="hidden" value="Maintain">
                        <TR>
                          <TD colspan="4" class="header">Add
                          </TD>
                        </TR>';
                        if ($_SESSION['cAuth'] & 32)
                        {
                          echo '<TR>
                                  <TD colspan="4">To record availability, specify the particulars, click Add and complete the form that is displayed.
                                  </TD>
                                </TR>
                                <TR>
                                  <TD class="short">Staff Name:
                                    <SPAN class="note">*
                                    </SPAN>
                                  </TD>
                                  <TD>';
                                    BuildStaffSelector(1, 'Staff', 'standard', $_SESSION['cUID'], true);
                            echo '</TD>
                                  <TD colspan="2" class="right">
                                    <INPUT tabindex="2" name="Submit" type="submit" class="button" value="Add" />                   
                                  </TD>
                                </TR>';
                        } else
                          echo '<TR>
                                  <TD colspan="3">To record availability, click Add and complete the form that is displayed.
                                  </TD>
                                  <TD class="right">
                                    <INPUT tabindex="1" name="Submit" type="submit" class="button" value="Add" />                   
                                  </TD>
                                </TR>';
                  echo '<TR>
                          <TD colspan="4" class="header">Edit
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="4">To edit availability, specify the particulars, click Edit and complete the form that is displayed.
                          </TD>
                        </TR>
                        <TR>
                          <TD class="short">Availability:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                            if ($_SESSION['cAuth'] & 32)
                              BuildAvailabilitySelector(3, 'EditAvailability', 'long', "");
                            else
                              BuildAvailabilitySelector(3, 'EditAvailability', 'long', $_SESSION['cUID']);
                    echo '</TD>
                          <TD colspan="2" class="right">
                            <INPUT tabindex="4" name="Submit" type="submit" class="button" value="Edit" />                   
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="4" class="header">Remove
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="4">To remove availability, specify the particulars, click Remove and confirm when prompted.
                          </TD>
                        </TR>
                        <TR>
                          <TD class="short">Availability:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                            if ($_SESSION['cAuth'] & 32)
                              BuildAvailabilitySelector(5, 'RemoveAvailability', 'long', "");
                            else
                              BuildAvailabilitySelector(5, 'RemoveAvailability', 'long', $_SESSION['cUID']);
                    echo '</TD>
                          <TD colspan="2" class="right">
                            <INPUT tabindex="6" name="Submit" type="submit" class="button" value="Remove" />                   
                          </TD>
                        </TR>
                      </FORM>
                    </TABLE>
                  </DIV>';
            
            BuildContentHeader('Maintenance - Events', "", "", true);
            echo '<DIV class="contentflow">
                    <P>Events can be recorded by using the form below. Details on events already listed can also be edited or removed.</P>
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center" class="standard">
                      <FORM method="post" action="Handlers/Availability_Handler.php">
                        <INPUT name="Type" type="hidden" value="MaintainEvents">
                        <TR>
                          <TD colspan="4" class="header">Add
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="3">To record availability, click Add and complete the form that is displayed.
                          </TD>
                          <TD class="right">
                            <INPUT tabindex="7" name="Submit" type="submit" class="button" value="Add" />                   
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="4" class="header">Edit
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="4">To edit availability, specify the particulars, click Edit and complete the form that is displayed.
                          </TD>
                        </TR>
                        <TR>
                          <TD class="short">Event:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                            BuildEventSelector(8, 'EditEvent', 'long', "");                              
                    echo '</TD>
                          <TD colspan="2" class="right">
                            <INPUT tabindex="9" name="Submit" type="submit" class="button" value="Edit" />                   
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="4" class="header">Remove
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="4">To remove an event, specify the particulars, click Remove and confirm when prompted.
                          </TD>
                        </TR>
                        <TR>
                          <TD class="short">Event:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                            BuildEventSelector(10, 'RemoveEvent', 'long', "");                              
                    echo '</TD>
                          <TD colspan="2" class="right">
                            <INPUT tabindex="11" name="Submit" type="submit" class="button" value="Remove" />                   
                          </TD>
                        </TR>
                      </FORM>
                    </TABLE>
                  </DIV>';
            
            BuildContentHeader('View Availability and Events', "", "", true);
            echo '<DIV class="contentflow">
                    <P>Availability and events can be viewed by using the form below.</P>
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center" class="standard">
                      <FORM method="post" action="Handlers/Availability_Handler.php">
                        <INPUT name="Type" type="hidden" value="View">
                        <TR>
                          <TD colspan="4" class="header">View
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="4">To view availability and events, specify the particulars and click View.
                          </TD>
                        </TR>
                        <TR>
                          <TD class="short">Type:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                            BuildAvailabilityReportTypeSelector(12, 'Availability', 'standard');
                    echo '</TD>
                        </TR>
                        <TR>
                          <TD>Staff Name:
                          </TD>
                          <TD>';
                            BuildStaffSelector(13, 'Staff', 'standard', "", true);
                    echo '</TD>
                        </TR>
                        <TR>
                          <TD>Date Range:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                            BuildDaySelector(14, 'StartDay', "");
                            echo '&nbsp;';
                            BuildMonthSelector(15, 'StartMonth', "");
                            echo '&nbsp;';
                            BuildYearSelector(16, 'StartYear', "");
                            echo ' to ';
                            $date = Date('Y-m-d', MKTime(0, 0, 0, Date('m') + 1, Date('d'), Date('y')));
                            BuildDaySelector(17, 'EndDay', GetDayFromDatabaseDate($date));
                            echo '&nbsp;';
                            BuildMonthSelector(18, 'EndMonth', GetMonthFromDatabaseDate($date));
                            echo '&nbsp;';
                            BuildYearSelector(19, 'EndYear', GetYearFromDatabaseDate($date));
                    echo '</TD>
                          <TD colspan="2" class="right">
                            <INPUT tabindex="20" name="Submit" type="submit" class="button" value="View" />                   
                          </TD>
                        </TR>
                      </FORM>
                    </TABLE> 
                  </DIV>';
            }                
            ////////////////////////////////////////////////////////////////////
          ?>
                            </div>
                        </div>
                    </div>
            </section>
    </DIV>
    <?php
      // PHP SCRIPT ////////////////////////////////////////////////////////////
      BuildFooter();
      //////////////////////////////////////////////////////////////////////////
    ?>
  </BODY>
</HTML>