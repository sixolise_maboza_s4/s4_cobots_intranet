<?php
  // DETAILS ///////////////////////////////////////////////////////////////////
  //                                                                          //
  //                    Last Edited By: Gareth Ambrose                        //
  //                        Date: 14 May 2009                                 //
  //                                                                          //
  //////////////////////////////////////////////////////////////////////////////
  // This page allows users to manage and view projects.                      //
  //////////////////////////////////////////////////////////////////////////////
  
  include 'Scripts/Include.php';
  SetSettings();
  CheckAuthorisation('Projects.php');
  
  //////////////////////////////////////////////////////////////////////////////
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3c.org/TR/1999/REC-html401-19991224/loose.dtd">
<HTML>
  <HEAD>
    <?php
      // PHP SCRIPT ////////////////////////////////////////////////////////////
      BuildHead('Projects');
      //////////////////////////////////////////////////////////////////////////
    ///
    include ('Scripts/header.php');
    ?>
      
	<script type="text/javascript">
  
		function selectElementContents(el) {
			var body = document.body, range, sel;
			if (document.createRange && window.getSelection) {
				range = document.createRange();
				sel = window.getSelection();
				sel.removeAllRanges();
				try {
					range.selectNodeContents(el);
					sel.addRange(range);
				} catch (e) {
					range.selectNode(el);
					sel.addRange(range);
				}
			} else if (body.createTextRange) {
				range = body.createTextRange();
				range.moveToElementText(el);
				range.select();
			}
		}
	</script>

        <script src="Scripts/MultipleSelect.js"></script>
      <script src="Scripts/DevExpress/js/jszip.min.js" type="text/javascript"></script>
      <script src="Scripts/DevExpress/js/globalize.min.js" type="text/javascript"></script>
      <script src="Scripts/DevExpress/js/datagrids.js" type="text/javascript"></script>


        <script>
  	$(document).ready(function(event) {
	    $(".seltest").chosen({
		no_results_text: "No results matched"
            });
           $("#loadMoreManagers").change(function(){
               if(this.checked){
                   $("TR").removeClass("hide");
               
                   $("#loadMoreManagers").addClass("hide");
                   $("#showMore").addClass("hide");
               }
               else{
                   //$("TR").addClass("hide");
               }
           });
 	});
       </script>
        
        
  </HEAD>
  <BODY>
    <?php
      // PHP SCRIPT ////////////////////////////////////////////////////////////
      BuildBanner();
      //////////////////////////////////////////////////////////////////////////
    ?>
    <DIV id="main">
      <?php
        // PHP SCRIPT //////////////////////////////////////////////////////////
        BuildTopBar();
        BuildMenu('Main', 'Projects.php');
        ////////////////////////////////////////////////////////////////////////
      ?>
        <section id="content_wrapper">
            <?php BuildBreadCrumb($currentPath);?>
            <!-- -------------- Content -------------- -->
            <section id="content" class="table-layout">
                <!-- -------------- Column Center -------------- -->
                <div class="chute chute-center" style="height: 869px;">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel">
        <?php
          // PHP SCRIPT ////////////////////////////////////////////////////////
          BuildMessageSet('Project');
          //////////////////////////////////////////////////////////////////////    
        ?>  
        <?php 
          // PHP SCRIPT //////////////////////////////////////////////////////// 
          if (isset($_SESSION['AddProject']))
          {
            BuildContentHeader('Add Project', "", "", true);
            echo '<DIV class="contentflow">
                    <P>Enter the details of the project below. Ensure that all the required information is entered before submission and that this information is valid.</P>
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center" class="long">
                      <FORM method="post" action="Handlers/Projects_Handler.php">
                        <INPUT name="Type" type="hidden" value="Add">
                        <TR>
                          <TD colspan="4" class="header">Project Details
                          </TD>
                        </TR>
                        <TR>
                          <TD class="short">Pastel Prefix:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>
                            <INPUT tabindex="1" name="Pastel" type="text" class="text veryshort" maxlength="5" value="'.$_SESSION['AddProject'][0].'" />
                          </TD>
                          <TD class="short">Description:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD rowspan="4" colspan="4">
                            <TEXTAREA tabindex="7" name="Description" class="standard" maxlength="500">'.$_SESSION['AddProject'][1].'</TEXTAREA>
                          </TD>
                        </TR>
                        <TR>
                          <TD class="short">Project Name:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>
                            <INPUT tabindex="2" name="Project" type="text" class="text standard" maxlength="35" value="'.$_SESSION['AddProject'][2].'" />
                          </TD>
                        </TR>
                        <TR>
                          <TD>PMS4I:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                            buildStaffDropdownWithMMMap(3, 'Responsible' , 'S4Responsible', 'standard seltest', $_SESSION['AddProject'][3]);
                       //     BuildStaffSelector(3, 'Responsible', 'standard seltest', $_SESSION['AddProject'][3], true);
                    echo '</TD>
                        </TR>
                        
                        <TR >
                          <TD>PMS4A:
                          </TD>
                          <TD>';
                            buildStaffDropdownWithMMMap(4, 'ResponsibleS4A' , 'S4ResponsibleS4A', 'standard seltest', $_SESSION['AddProject'][27]);
                       //     BuildStaffSelector(3, 'Responsible', 'standard seltest', $_SESSION['AddProject'][3], true);
                    echo '</TD>
                        </TR>    


                        <TR>
                          <TD>Customer:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                            BuildCustomerSelector(5, 'Customer', 'standard seltest', $_SESSION['AddProject'][4]);
                    echo '</TD>
                        </TR>
                        <TR>
                          <TD>Purchase Order:
                          </TD>
                          <TD>';
                            BuildPurchaseOrderSelector(6, 'PurchaseOrder', 'standard seltest', $_SESSION['AddProject'][5]);
                    echo '</TD>
                        </TR>
                        <TR>
                          <TD>Project Type:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
              BuildProjectTypeSelector(7, 'ProjectType', 'standard seltest', $_SESSION['AddProject'][6]);
              echo '</TD>
                        </TR>
                        <TR>
                        
                    <TR id="MapProjectSection" >
                          <TD>MM Project Map:
                          </TD>
                          <TD>';
              buildAutonetProjectsNotMapped(8, 'MMproject', 'standard seltest', $_SESSION['AddProject'][8]);
              echo '</TD>
                        </TR>

                       <TR><TD colspan = 4><hr></TD></TR> 
                       <TR>
                          <TD><b>Cost Managers</b>
                          </TD>
                        <TR>  
                        
                          <TR class="hide">
                             <TD>Unassigned:  </TD> <TD>';
              buildStaffDropdownWithMMMap(9, 'CostManager' , 'S4CostManager', 'standard seltest', $_SESSION['AddProject'][7]);
                    echo '</TD>
                        </TR>
                        
                         <TR class="hide">
                             <TD>Admin: </TD> <TD>'; 
                          buildStaffDropdownWithMMMap(10, 'CostManager1' , 'S4CostManager1', 'standard seltest', $_SESSION['AddProject'][16]);
                    echo '</TD>
                        </TR>
                        

                        <TR>
                             <TD>Electrical: <SPAN class="note">*</SPAN></TD> <TD>'; 
                          buildStaffDropdownWithMMMap(11, 'CostManager2' , 'S4CostManager2', 'standard seltest', $_SESSION['AddProject'][17]);
                    echo '</TD>
                        </TR>
                        
                        <TR>
                             <TD>Mechanical: <SPAN class="note">*</SPAN></TD> <TD>'; 
                          buildStaffDropdownWithMMMap(12, 'CostManager3' , 'S4CostManager3', 'standard seltest', $_SESSION['AddProject'][18]);
                    echo '</TD>
                        </TR>       
                        <TR class="hide">
                             <TD>IT: </TD> <TD>'; 
                          buildStaffDropdownWithMMMap(13, 'CostManager4' , 'S4CostManager4', 'standard seltest', $_SESSION['AddProject'][19]);
                    echo '</TD>
                        </TR>    
                        <TR class="hide">
                             <TD>HR: </TD> <TD>'; 
                          buildStaffDropdownWithMMMap(14, 'CostManager5' , 'S4CostManager5', 'standard seltest', $_SESSION['AddProject'][20]);
                    echo '</TD>
                        </TR>  
                        <TR class="hide">
                             <TD>Management: </TD> <TD>'; 
                          buildStaffDropdownWithMMMap(15, 'CostManager6' , 'S4CostManager6', 'standard seltest', $_SESSION['AddProject'][21]);
                    echo '</TD>
                        </TR>
                        

                        <TR class="hide">
                             <TD>Training: </TD> <TD>'; 
                          buildStaffDropdownWithMMMap(16, 'CostManager7' , 'S4CostManager7', 'standard seltest', $_SESSION['AddProject'][22]);
                    echo '</TD>
                        </TR>
                        
                        <TR>
                            <TD>Manufacturing: <SPAN class="note">*</SPAN></TD> <TD>'; 
                          buildStaffDropdownWithMMMap(17, 'CostManager8' , 'S4CostManager8', 'standard seltest', $_SESSION['AddProject'][23]);
                    echo '</TD>
                        </TR>
                        
                        <TR class="hide">
                            <TD>Sales and Marketing: </TD> <TD>'; 
                          buildStaffDropdownWithMMMap(18, 'CostManager9' , 'S4CostManager9', 'standard seltest', $_SESSION['AddProject'][24]);
                    echo '</TD>
                        </TR>
                        
                        <TR class="hide">
                             <TD>Software:</TD> <TD>'; 
                          buildStaffDropdownWithMMMap(19, 'CostManager10' , 'S4CostManager10', 'standard seltest', $_SESSION['AddProject'][25]);
                    echo '</TD>
                        </TR>

                        
                        <TR class="hide">
                             <TD>Design: </TD> <TD>'; 
                          buildStaffDropdownWithMMMap(20, 'CostManager12' , 'S4CostManager12', 'standard seltest', $_SESSION['AddProject'][26]);
                    echo '</TD>
                        </TR>


                       <TR><TD colspan = 4><hr></TD></TR>
                      <TR><TD colspan = 4><b>Autonet Details</b>';
                            if(strcmp($_SESSION['AddProject'][9], "checked") == 0){
                                echo' <input type = "checkbox" id="cbAddMM" name = "AddMM"  value ="'.$_SESSION['AddProject'][9].'" checked />';
                            }
                            else{
                                echo' <input type = "checkbox" id="cbAddMM" name = "AddMM"  value ="'.$_SESSION['AddProject'][9].'"/>';
                            }
                          echo '</TD>
                        </TR>
                         <TR class = "AutonetProjectDetailsSection hide" >
                            <TD>Pastel Prefix: <SPAN class="note">**
                            </SPAN></TD>
                            <TD>
                              <input  tabindex="9" type = "textbox" name = "MMPrefix" value = "'.$_SESSION['AddProject'][10].'" />';
                            echo'</TD>';
                        echo'</TR>

                        <TR class = "AutonetProjectDetailsSection hide">
                            <TD>PMS4A: <SPAN class="note">**
                            </SPAN></TD>
                            <TD>';

                            echo buildAutonetStaff(21, "MMResponsible", "MMResponsible", "standard", $_SESSION['AddProject'][13]);

                            echo'</TD>
                        </TR>


                         <TR class = "AutonetProjectDetailsSection hide">
                            <TD>Customer: <SPAN class="note">**
                            </SPAN></TD>
                            <TD> ';
                              echo buildAutonetCustomer(22, "MMCustomer", "standard seltest", $_SESSION['AddProject'][11]);
                            echo'</TD>
                        </TR>
                         <TR class = "AutonetProjectDetailsSection hide">
                            <TD>Purchase Order: </TD>
                            <TD> ';
                               echo buildAutonetPurchaseOrder(23, "MMPurchaseOrder", "standard seltest", $_SESSION['AddProject'][12]);
                            echo'</TD>
                        </TR>

                         <TR class = "AutonetProjectDetailsSection hide">
                            <TD>Cost Manager: <SPAN class="note">**
                            </SPAN></TD>
                            <TD> ';
                               echo buildAutonetStaff(24, "MMCostManager" ,"MMCostManager", "standard", $_SESSION['AddProject'][14]);
                            echo'</TD>
                        </TR>';
                        
                     echo'   <TR>
                          <TD colspan="4" class="center">
                            <INPUT tabindex="15" name="Submit" type="submit" class="button" value="Submit" />   
                            <INPUT tabindex="16" name="Submit" type="submit" class="button" value="Cancel" />                  
                          </TD>
                        </TR>
                      </FORM>
                    </TABLE>  
                  </DIV>  
                  <DIV>
                    <BR />
                    <SPAN class="note">*
                    </SPAN>
                    These fields are required.
                    <BR />
                     <SPAN class="note">**
                    </SPAN> 
                    These fields are required if Autonet is selected.

                   
                  </DIV>';
          } else
          if (isset($_SESSION['EditProject']))
          {
            $row = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Project WHERE Project_Code = "'.$_SESSION['EditProject'][0].'"'));        		    
            BuildContentHeader('Edit Project - '.$row['Project_Description'], "", "", true);
            echo '<DIV class="contentflow">
                    <P>Once you have made changes, ensure that you submit them otherwise they will not take effect.<P>
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center" class="long">
                      <FORM method="post" action="Handlers/Projects_Handler.php">
                        <INPUT name="Type" type="hidden" value="Edit">
                        <TR>
                          <TD colspan="5" class="header">Project Details
                          </TD>
                        </TR>
                        <TR>
                          <TD class="short">Pastel Prefix:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>
                            <INPUT tabindex="1" name="Pastel" type="text" class="text veryshort" maxlength="5" value="'.$_SESSION['EditProject'][1].'" />
                          </TD>
                          <TD class="short">Description:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD rowspan="4" colspan="2" >
                            <TEXTAREA tabindex="8" name="Description" class="standard" maxlength="500">'.$_SESSION['EditProject'][2].'</TEXTAREA>
                          </TD>
                        </TR>
                        <TR>
                          <TD class="short">Project Name:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>
                            <INPUT tabindex="2" name="Project" type="text" class="text standard" maxlength="35" value="'.$_SESSION['EditProject'][3].'" />
                          </TD>
                        </TR>
                         </TR>
                                <TR>
                          <TD class="short">Project Date:
                          </TD>
                          <TD>';
                               BuildFullDateLabel($row['Project_DateTime_Updated']);
                    echo'</TD>
                        </TR>
                        <TR>
                          <TD>PMS4I:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                            BuildStaffSelector(3, 'Responsible', 'standard seltest', $_SESSION['EditProject'][4], true);
                    echo '</TD>
                        </TR>
                        

                         <TR>
                          <TD>PMS4A:
                          </TD>
                          <TD colspan="4">';
                            BuildStaffSelector(3, 'ResponsibleS4A', 'standard seltest', $_SESSION['EditProject'][27], true);
                    echo '</TD>
                        </TR>
                        


                        <TR>
                          <TD>Customer:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD colspan="4">';
                            BuildCustomerSelector(4, 'Customer', 'standard seltest', $_SESSION['EditProject'][5]);
                    echo '</TD>
                        </TR>
                        <TR>
                          <TD>Purchase Order(s):
                          </TD>
                           <TD colspan="4"">
                               ';
                                  BuildPurchaseOrderMultipleSelector(5, 'Purchase', 'fleft', $row['Project_Code'],6,'sub');
                                echo'
                                <ul style="list-style: none; margin-right: 2.5em" class="fleft">
                                    <li><INPUT tabindex="" name="additems" type="Button"  value="<<" height="0.5em" id="add" onclick="AddButtonClicked()"/></li>  
                                    <li><INPUT tabindex="" name="removeitem" type="Button"  value=">>" height="0.5em" id="remove" onclick="RemoveButtonClicked()"/></li>
                                </ul>
                                ';
                                  BuildPurchaseOrderMultipleSelector(5, 'PurchaseOrder2', 'stand', $row['Project_Code'],10,'all');  
                            echo'<input type="hidden" value="'.$_SESSION['EditProject'][12].'" name="allvalues" id="allOrdes"/>
                                <input type="hidden" value="'.$_SESSION['EditProject'][13].'" name="allPurchaseOrderNumbers" id="all_order_text"/>
                           </TD>
                        </TR>
                        <TR>
                          <TD>Project Type:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                            BuildProjectTypeSelector(6, 'ProjectType', 'standard seltest', $_SESSION['EditProject'][11]);
                    echo '</TD>
                        <TD>Total Value (VAT excl.):
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD class="bold" colspan="2">
                            <label class="standard">R&nbsp;</label>
                            <label id="totalcosts" class="standard">'.$_SESSION['EditProject'][6].'</label>
                          </TD>
                        </TR>
                        <TR>
                          <TD>Orders Placed:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD class="bold">
                            <INPUT tabindex="6" name="Orders" type="text" class="text veryshort" maxlength="3" value="'.$_SESSION['EditProject'][8].'" /> %
                          </TD>
                          <TD>Work In Progress:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD class="bold" colspan="2">
                            <INPUT tabindex="9" name="WorkInProgress" type="text" class="text veryshort" maxlength="3" value="'.$_SESSION['EditProject'][9].'" /> %
                          </TD>
                        </TR>
                        <TR>
                          <TD>Project Complete:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD class="bold">
                            <INPUT tabindex="7" name="Complete" type="text" class="text veryshort" maxlength="3" value="'.$_SESSION['EditProject'][10].'" /> %
                          </TD>
                          <TD>Project Closed:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD colspan="2">';
                            BuildYesNoSelector(10, 'Closed', 'veryshort', $_SESSION['EditProject'][7]);
              echo '</TD>
                        </TR>
                        
                         <TR id="MapProjectSection">
                          <TD>MM Project Map:
                          </TD>
                          <TD colspan="4">';
              buildAutonetProjectsNotMapped(8, 'MMproject', 'standard seltest', $_SESSION['EditProject'][15]);
              echo '</TD>
                        </TR>
                     <TR><TD colspan = 5><hr></TD></TR> 
                         <TR>
                          <TD colspan="5"> <b>Cost Managers</b>
                          </TD>
                        </TR>

                         <TR class="hide">
                             <TD>Unassigned: </TD> <TD colspan="4">';
                           BuildStaffSelector(6, 'CostManager', 'standard seltest', $_SESSION['EditProject'][14], true);
                    echo '</TD>
                        </TR>
                        
                        <TR class="hide">
                             <TD>Admin:  </TD> <TD colspan="4">';
                           BuildStaffSelector(6, 'CostManager1', 'standard seltest', $_SESSION['EditProject'][16], true);
                    echo '</TD>
                        </TR>
                        
                        <TR>
                             <TD>Electrical:  <SPAN class="note">*</SPAN></TD> <TD colspan="4">';
                           BuildStaffSelector(6, 'CostManager2', 'standard seltest', $_SESSION['EditProject'][17], true);
                    echo '</TD>
                        </TR>

                        <TR>
                             <TD>Mechanical:  <SPAN class="note">*</SPAN></TD> <TD colspan="4">';
                           BuildStaffSelector(6, 'CostManager3', 'standard seltest', $_SESSION['EditProject'][18], true);
                    echo '</TD>
                        </TR>
                        
                        <TR class="hide">
                             <TD>IT: </TD> <TD colspan="4">';
                           BuildStaffSelector(6, 'CostManager4', 'standard seltest', $_SESSION['EditProject'][19], true);
                    echo '</TD>
                        </TR>
                        
                        <TR class="hide">
                             <TD>HR: </TD> <TD colspan="4">';
                           BuildStaffSelector(6, 'CostManager5', 'standard seltest', $_SESSION['EditProject'][20], true);
                    echo '</TD>
                        </TR>
                        
                        <TR class="hide"> 
                             <TD>Management:  </TD> <TD colspan="4">';
                           BuildStaffSelector(6, 'CostManager6', 'standard seltest', $_SESSION['EditProject'][21], true);
                    echo '</TD>
                        </TR>
                        
                        
                        <TR class="hide">
                             <TD>Training:  </TD> <TD colspan="4">';
                           BuildStaffSelector(6, 'CostManager7', 'standard seltest', $_SESSION['EditProject'][22], true);
                    echo '</TD>
                        </TR>
                        
                         <TR>
                             <TD>Manufacturing:  <SPAN class="note">*</SPAN></TD> <TD colspan="4">';
                           BuildStaffSelector(6, 'CostManager8', 'standard seltest', $_SESSION['EditProject'][23], true);
                    echo '</TD>
                        </TR>
                        
                        
                        
                        <TR class="hide">
                             <TD>Sales and Marketing:  </TD> <TD colspan="4">';
                           BuildStaffSelector(6, 'CostManager9', 'standard seltest', $_SESSION['EditProject'][24], true);
                    echo '</TD>
                        </TR>
                          
                        <TR class="hide">
                             <TD>Software:  </TD> <TD colspan="4">';
                           BuildStaffSelector(6, 'CostManager10', 'standard seltest', $_SESSION['EditProject'][25], true);
                    echo '</TD>
                        </TR>
                        
                        <TR class="hide">
                             <TD>Design: </TD> <TD colspan="4">';
                           BuildStaffSelector(6, 'CostManager12', 'standard seltest', $_SESSION['EditProject'][26], true);
                    echo '</TD>
                        </TR>
                         <TR>
                             <TD></TD> <TD colspan="4">';
                    echo '<input type="checkbox" id="loadMoreManagers"><span id="showMore">Show More</span></input></TD>
                        </TR>


                        <TR>
                          <TD colspan="5" class="center">
                            <INPUT tabindex="11" name="Submit" type="submit" class="button" value="Submit" />   
                            <INPUT tabindex="12" name="Submit" type="submit" class="button" value="Cancel" />                  
                          </TD>
                        </TR>
                      </FORM>
                    </TABLE>  
                  </DIV>  
                  <DIV>
                    <BR /><BR />
                    <SPAN class="note">*
                    </SPAN>
                    These fields are required.
                  </DIV>';
          } else
          if (isset($_SESSION['UpdateProject']))
          {
            BuildContentHeader('Update Project History', "", "", true);
            echo '<DIV class="contentflow">
                    <P>Enter the details of the project history below. Ensure that all the required information is entered before submission and that this information is valid.</P>
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center" class="short">
                      <FORM method="post" action="Handlers/Projects_Handler.php">
                        <INPUT name="Type" type="hidden" value="Update">
                        <TR>
                          <TD colspan="2" class="header">History Details
                          </TD>
                        </TR>
                        <TR>
                          <TD class="short vtop">Details:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>
                            <TEXTAREA tabindex="1" name="Details" class="long" maxlength="500">'.$_SESSION['UpdateProject'][0].'</TEXTAREA>
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="2" class="center">
                            <INPUT tabindex="2" name="Submit" type="submit" class="button" value="Submit" />   
                            <INPUT tabindex="3" name="Submit" type="submit" class="button" value="Cancel" />                  
                          </TD>
                        </TR>
                      </FORM>
                    </TABLE>  
                  </DIV>  
                  <DIV>
                    <BR />
                    <SPAN class="note">*
                    </SPAN>
                    These fields are required.
                  </DIV>';
          } else
          if (isset($_SESSION['ViewProject']))
          {
            switch ($_SESSION['ViewProject'][2])
            {
              case "":
                $responsible = ' - All Staff';
                $staff = 'WHERE 1 = 1';
                $colspan = 11;
                break;
              default:
                $row = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = '.$_SESSION['ViewProject'][2].''));
                $responsible = ' - '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].'';
                $staff = 'WHERE Project_Responsible = '.$_SESSION['ViewProject'][2].'';
                $colspan = 10;
                break;
            }
            
            switch ($_SESSION['ViewProject'][3])
            {
              case "":
                $project = ' - All Types';
                $type = "";
                break;
              default:
                $row = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM ProjectType WHERE ProjectType_ID = '.$_SESSION['ViewProject'][3].''));
                $project = ' - '.$row['ProjectType_Description'].'';
                $type = ' AND Project_Type = '.$_SESSION['ViewProject'][3].'';
                $colspan = 10;
                break;
            }
            
            if ($_SESSION['ViewProject'][0] == 1)
            {
              $dates = ' AND Project_DateTime_Updated BETWEEN "'.GetDatabaseDateFromSessionDate($_SESSION['ViewProject'][4]).' 00:00:00" AND "'.GetDatabaseDateFromSessionDate($_SESSION['ViewProject'][5]).' 23:59:59"';
            }
            
            switch ($_SESSION['ViewProject'][1])
            {
              case "":
                BuildContentHeader('All Projects'.$responsible.$project, "", "", true);
                $resultSet = ExecuteQuery('SELECT * FROM Project '.$staff.$type.$dates.' ORDER BY Project_Pastel_Prefix ASC');
                break;
              case 0:
                BuildContentHeader('Closed Projects'.$responsible.$project, "", "", true);
                $resultSet = ExecuteQuery('SELECT * FROM Project '.$staff.$type.$dates.' AND Project_Closed = "1" ORDER BY Project_Pastel_Prefix ASC');
                break;
              case 1:
                BuildContentHeader('Open Projects'.$responsible.$project, "", "", true);
                $resultSet = ExecuteQuery('SELECT * FROM Project '.$staff.$type.$dates.' AND Project_Closed = "0" ORDER BY Project_Pastel_Prefix ASC');
                break;
              default:
                echo 'Kyaaaaaaaaaah! *chop* Invalid report type! Error! Error!';   
                break;
            }
            if (MySQL_Num_Rows($resultSet) > 0)
            {
              switch ($_SESSION['ViewProject'][0])
              {
                case 0:
                  echo '<DIV class="contentflow">
                          <P>These are the projects listed.</P>
                          <BR /><BR />
                          <TABLE cellspacing="5" align="center" class="long">
                            <TR>
                              <TD colspan="'.($_SESSION['ViewProject'][2] == "" ? '10' : '9').'" class="header">Project Details
                              </TD>
                            </TR>
                            <TR>
                              <TD class="subheader small">Pastel Code
                              </TD>
                              <TD class="subheader small">Code
                              </TD>
                              <TD class="subheader ">Project Name
                              </TD>
                              <TD class="subheader ">Customer
                              </TD>';
                              if ($_SESSION['ViewProject'][2] == "")
                                echo '<TD class="subheader ">Responsible
                                      </TD>';
                        echo '<TD class="subheader small">Purchase Order
                              </TD>
                              <TD class="subheader small">Orders Placed
                              </TD>
                              <TD class="subheader small">Work In Progress
                              </TD>
                              <TD class="subheader small">Complete
                              </TD>
                              <TD class="subheader veryshrot">100%
                              </TD>
                            </TR>';
                            $count = 0;                          
                            while ($row = MySQL_Fetch_Array($resultSet))
                            { 
                              if ($count % 2 == 0)
                                $colour = 'rowA';
                              else
                                $colour = 'rowB'; 
                              $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Customer WHERE Customer_Code = "'.$row['Project_Customer'].'"'));   
                              $rowTempB = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = '.$row['Project_Responsible']));   
                              $rowTempC = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM PurchaseOrder WHERE PurchaseOrder_ID = "'.$row['Project_PurchaseOrder'].'"'));   
                              echo '<TR>
                                      <TD class="'.$colour.' center">'.$row['Project_Pastel_Prefix'].'
                                      </TD>
                                      <TD class="'.$colour.' center">'.$row['Project_Code'].'
                                      </TD>
                                      <TD class="'.$colour.'">'.$row['Project_Description'].'
                                      </TD>
                                      <TD class="'.$colour.'">'.$rowTemp['Customer_Name'].'
                                      </TD>';
                                      if ($_SESSION['ViewProject'][2] == "")
                                        echo '<TD class="'.$colour.'">'.$rowTempB['Staff_First_Name'].' '.$rowTempB['Staff_Last_Name'].'
                                              </TD>';
                                echo '<TD class="'.$colour.' center">'.$rowTempC['PurchaseOrder_Number'].'
                                      </TD>
                                      <TD class="'.$colour.' center">'.$row['Project_OrdersPlaced'].'%
                                      </TD>
                                      <TD class="'.$colour.' center">'.$row['Project_WorkInProgress'].'%
                                      </TD>
                                      <TD class="'.$colour.' center">'.$row['Project_Complete'].'%
                                      </TD>
                                      <TD class="'.$colour.' center">'.($row['Project_OrdersPlaced'] + $row['Project_WorkInProgress'] + $row['Project_Complete'] == 300 ? 'Yes' : 'No').'
                                      </TD>
                                    </TR>';                         
                              $count++;
                            }
                    echo '</TABLE> 
                        </DIV>';  
                  break;
                case 1:
				echo '<input type="button" value="Select Table" onclick="selectElementContents( document.getElementById(\'reportTable\') );">';
                  echo '<DIV class="contentflow">
                          <P>These are the projects listed.</P>
                          <BR /><BR />
                          <TABLE id="reportTable" cellspacing="5" align="center" class="long">
                            <TR>
                              <TD colspan="9" class="header">Project Details
                              </TD>
                            </TR>
                            <TR>
                              <TD class="small">
                              </TD>
                              <TD class="small">
                              </TD>
                              <TD class="short">
                              </TD>
                              <TD class="veryshort">
                              </TD>
                              <TD class="small">
                              </TD>
                              <TD class="subheader" colspan="3">Orders
                              </TD>
                              <TD class="small">
                              </TD>
                            </TR>
                            <TR>
                              <TD class="subheader">Pastel Code
                              </TD>
                              <TD class="subheader">Code
                              </TD>
                              <TD class="subheader">Project Name
                              </TD>
                              <TD class="subheader">Purchase Order
                              </TD>
                              <TD class="subheader">Purchase Order Value
                              </TD>
                              <TD class="subheader small">* Placed
                              </TD>
                              <TD class="subheader small">* Labour Cost
                              </TD>';
                    
                  //remove this 
//                            echo'<TD class="subheader small">Invoiced
//                              </TD>
//                              <TD class="subheader small">Balance
//                             </TD>
//                              <TD class="subheader">Overtime Hours
//                              </TD>
//                              <TD class="subheader">* Casual Hours
//                              </TD>
//                              <TD class="subheader">Standard Hours
//                              </TD>
//                              <TD class="subheader">* Project hours
//                              </TD>';
                                  
       			      echo'<TD class="subheader">Total expense
                              </TD>
                              <TD class="subheader">% Expense
                              </TD>
                            </TR>';
                            $count = 0;
                            $totalPurchase = 0;
                            $totalOrdersPlaced = 0;
                            $totalOrdersInvoiced = 0;
                            $totalOvers = 0;
                            $totalCasuals = 0;
                            $totalExpensesVal = 0;
                            $totalLabourCost = 0;
                            $expenceAvg = 0;
                            while ($row = MySQL_Fetch_Array($resultSet))
                            { 
                              if ($count % 2 == 0)
                                $colour = 'rowA';
                              else
                                $colour = 'rowB';  
                              $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM PurchaseOrder WHERE PurchaseOrder_ID = "'.$row['Project_PurchaseOrder'].'"'));   
                              $rowTempC = MySQL_Fetch_Array(ExecuteQuery('SELECT SUM(Overtime_Hours) AS Overs FROM Overtime WHERE Overtime_Project = '.$row['Project_Code'].''));  
                              $rowOversBanked = MySQL_Fetch_Array(ExecuteQuery('SELECT SUM(OvertimeBank_Hours) AS Overs FROM OvertimeBank WHERE OvertimeBank_Project = '.$row['Project_Code'].''));  
                              $rowTempD = MySQL_Fetch_Array(ExecuteQuery('SELECT SUM(CasualExpense_Total) AS Casuals FROM CasualExpense WHERE CasualExpense_Project = '.$row['Project_Code'].''));
                              $rowTempE = MySQL_Fetch_Array(ExecuteQuery('SELECT SUM(TimeSheetBasic_Hours) AS Standard FROM TimeSheetBasic WHERE TimeSheetBasic_Project = '.$row['Project_Code'].''));
                              $joinQuery = 'SELECT 
                                                SUM(S4Admin.ProjectTimeAllocation_Entry.ProjectTimeAllocation_Entry_Hours) 
                                                AS ProjectTime 
                                                FROM S4Admin.ProjectTimeAllocation_Entry 
                                                INNER JOIN S4Admin.ProjectTimeAllocation 
                                                ON S4Admin.ProjectTimeAllocation_Entry.ProjectTimeAllocation_Entry_ParentID = S4Admin.ProjectTimeAllocation.ProjectTimeAllocation_ID 
                                                WHERE S4Admin.ProjectTimeAllocation.ProjectTimeAllocation_Date 
                                                BETWEEN "'.GetDatabaseDateFromSessionDate($_SESSION['ViewProject'][4]).' 00:00:00" AND "'.GetDatabaseDateFromSessionDate($_SESSION['ViewProject'][5]).' 23:59:59"
                                                AND S4Admin.ProjectTimeAllocation_Entry.ProjectTimeAllocation_Entry_Project = '.$row['Project_Code'].'';
				$rowTempF = MySQL_Fetch_Array(ExecuteQuery($joinQuery));
                              
			      $ordersInvoicedValue = 0;
                              $ordersPlacedValue = 0;
                              $resultSetTemp = ExecuteQuery('SELECT OrderNo.* FROM OrderNo WHERE OrderNo_Project = '.$row['Project_Code'].' AND OrderNo_Complete <> "2" AND OrderNo_Approved = "1"');  
                              if (MySQL_Num_Rows($resultSetTemp) > 0)
                              {
                                while ($rowTempB = MySQL_Fetch_Array($resultSetTemp))
                                {
                                  $ordersPlacedValue += $rowTempB['OrderNo_Total_Cost'];
                                  $resultSetTempBTemp = ExecuteQuery('SELECT * FROM SupplierInvoice WHERE SupplierInvoice_S4OrderNo = "'.$rowTempB['OrderNo_Number'].'"');
                                  if (MySQL_Num_Rows($resultSetTempBTemp) > 0)
                                  {
                                    while($rowTempBTemp = MySQL_Fetch_Array($resultSetTempBTemp))
                                    {
                                      $ordersInvoicedValue += $rowTempBTemp['SupplierInvoice_Total'];
                                    }
                                  }
                                }
                              }
                              //$totalExpense = SPrintF('%02.2f', $ordersInvoicedValue + $rowTempD['Casuals'] + $rowTempF['ProjectTime']); //Removed because the invoices arrive too late
                              $totalExpense = SPrintF('%02.2f', $ordersPlacedValue + $rowTempD['Casuals'] + $rowTempF['ProjectTime']);
                              //Eliminate division by zero
                              $percExpense = "N/A";
                              $percColor = "yellow";
                              if ($rowTemp['PurchaseOrder_Value'] > 0) 
                              {
                                  
                                    $percExpense = SPrintF('%02.2f', ($LabourCost / $rowTemp['PurchaseOrder_Value']) * 100.00);
                                   // $percExpense = SPrintF('%02.2f', (($ordersPlacedValue + $rowTempD['Casuals'] + $rowTempF['ProjectTime']) / $rowTemp['PurchaseOrder_Value']) * 100.00);
                                $percColor = 'black';
                                if ($percExpense <= 80)
                                      $percColor = 'green';
                                if ($percExpense >= 100)
                                      $percColor = 'red';
                              }
                              
                                
                              $rowCost = MySQL_Fetch_Array(ExecuteQuery('select CONCAT(Project_Pastel_Prefix, " - ", Project_Description) as Project, IFNULL(SUM( (ts.hours * (SELECT Rate_Value FROM Rate WHERE Rate_Staff = ts.user ORDER BY abs(TIMESTAMPDIFF(second, `Rate_Date`, "'.GetDatabaseDateFromSessionDate($_SESSION['ViewProject'][4]).'")) LIMIT 1))),0) AS Cost from Project p LEFT JOIN ts_entries ts ON p.Project_Code = ts.project WHERE Project_Code = '.$row['Project_Code'].' AND (ts.`datetime` BETWEEN "'.GetDatabaseDateFromSessionDate($_SESSION['ViewProject'][4]).' 00:00:00" AND "'.GetDatabaseDateFromSessionDate($_SESSION['ViewProject'][5]).' 23:59:59")'));	
                              $LabourCost = $rowCost['Cost'];
							  
                              echo '<TR>
                                      <TD class="'.$colour.' center">'.$row['Project_Pastel_Prefix'].'
                                      </TD>
                                      <TD class="'.$colour.' center">'.$row['Project_Code'].'
                                      </TD>
                                      <TD class="'.$colour.'">'.$row['Project_Description'].'
                                      </TD>
                                      <TD class="'.$colour.' center">'.$rowTemp['PurchaseOrder_Number'].'
                                      </TD>
                                      <TD class="'.$colour.' center">R '.SPrintF('%02.2f', $rowTemp['PurchaseOrder_Value']).'
                                      </TD>
                                      <TD class="'.$colour.' center">R '.SPrintF('%02.2f', $ordersPlacedValue).'
                                      </TD>
                                      <TD class="'.$colour.' center">R '.SPrintF('%02.2f', $LabourCost).'
                                      </TD>';
           
                                      //remove this
//                                      echo'<TD class="'.$colour.' center">R '.SPrintF('%02.2f', $ordersInvoicedValue).'
//                                      </TD>
//                                      <TD class="'.$colour.' center">R '.SPrintF('%02.2f', $ordersPlacedValue - $ordersInvoicedValue).'
//                                      </TD>';
//                                      echo'<TD class="'.$colour.' center">'.SPrintF('%02.2f', $rowTempC['Overs'] + $rowOversBanked['Overs']).'
//                                      </TD>
//                                      <TD class="'.$colour.' center">R '.SPrintF('%02.2f', $rowTempD['Casuals']).'
//                                      </TD>
//                                      <TD class="'.$colour.' center">'.$rowTempE['Standard'].'
//                                      </TD>
//				      <TD class="'.$colour.' center">R '.$rowTempF['ProjectTime'].'
//                                      </TD>';
				      
                                      echo'<TD class="'.$colour.' center">R '.$totalExpense.'
                                      </TD>
				      <TD class="'.$colour.' center" style="font-weight:bold; color:'.$percColor.'">'.$percExpense.'
                                      </TD>
                                    </TR>';                         
                              $count++;
                              
                              $totalPurchase += $rowTemp['PurchaseOrder_Value'];
                              $totalOrdersPlaced += $ordersPlacedValue;
                              $totalOrdersInvoiced += $ordersInvoicedValue;
                              $totalOvers += $rowTempC['Overs'] + $rowOversBanked['Overs'];
                              $totalCasuals += $rowTempD['Casuals'];
                              $totalStandard += $rowTempE['Standard'];
                              $totalExpensesVal += $totalExpense;
                              $totalLabourCost+= $LabourCost;
                              $expenceAvg += $percExpense;
                        }
                            
                      echo '<TR>
                              <TD colspan="4" class="rowB bold">Total:
                              </TD> 
                              <TD class="rowB center bold">R '.SPrintF('%02.2f', $totalPurchase).'
                              </TD>
                              <TD class="rowB center bold">R '.SPrintF('%02.2f', $totalOrdersPlaced).'
                              </TD>';
                              
                      
                      //remove this
//                              echo'<TD class="rowB center bold">R '.SPrintF('%02.2f', $totalOrdersInvoiced).'
//                              </TD>
//                              <TD class="rowB center bold">R '.SPrintF('%02.2f', ($totalOrdersPlaced - $totalOrdersInvoiced)).'
//                              </TD>
//                              <TD class="rowB center bold">'.$totalOvers.'
//                              </TD>
//                              <TD class="rowB center bold">R '.SPrintF('%02.2f', $totalCasuals).'
//                              </TD>
//                              <TD class="rowB center bold">'.$totalStandard.'
//                              </TD>';
//                    $avg = $expenceAvg/$count;
                      $avg  = '';
                      echo '<TD class="rowB center bold">R '.SPrintF('%02.2f', $totalLabourCost) .'</TD>
                      <TD class="rowB center bold">R '.SPrintF('%02.2f', $totalExpensesVal).'</TD>
                      <TD class="rowB center bold"></TD>';
                          
                            echo'</TR>     
                          </TABLE> 
                        </DIV>';  
                  break;
                default:
                  break;              
              }
            } else
              echo '<DIV class="contentflow">
                      <P>There are no projects listed.</P>
                      <BR /><BR />
                    </DIV>';
            Session_Unregister('ViewProject');
          } else
          if (isset($_SESSION['ViewProjectSingle']))
          {
            $rowProject = MySQL_Fetch_Array(ExecuteQUery('SELECT *,
                (SELECT concat(Staff_First_Name," ", Staff_Last_Name) as CM FROM Staff WHERE Staff_Code = Project_CostManager) as CM,
                (SELECT concat(Staff_First_Name," ", Staff_Last_Name) as CM1 FROM Staff WHERE Staff_Code = Project_CostManager1) as CM1,
                (SELECT concat(Staff_First_Name," ", Staff_Last_Name) as CM2 FROM Staff WHERE Staff_Code = Project_CostManager2) as CM2,
                (SELECT concat(Staff_First_Name," ", Staff_Last_Name) as CM3 FROM Staff WHERE Staff_Code = Project_CostManager3) as CM3,
                (SELECT concat(Staff_First_Name," ", Staff_Last_Name) as CM4 FROM Staff WHERE Staff_Code = Project_CostManager4) as CM4,
                (SELECT concat(Staff_First_Name," ", Staff_Last_Name) as CM5 FROM Staff WHERE Staff_Code = Project_CostManager5) as CM5,
                (SELECT concat(Staff_First_Name," ", Staff_Last_Name) as CM6 FROM Staff WHERE Staff_Code = Project_CostManager6) as CM6,
                (SELECT concat(Staff_First_Name," ", Staff_Last_Name) as CM7 FROM Staff WHERE Staff_Code = Project_CostManager7) as CM7,
                (SELECT concat(Staff_First_Name," ", Staff_Last_Name) as CM8 FROM Staff WHERE Staff_Code = Project_CostManager8) as CM8,
                (SELECT concat(Staff_First_Name," ", Staff_Last_Name) as CM9 FROM Staff WHERE Staff_Code = Project_CostManager9) as CM9,
                (SELECT concat(Staff_First_Name," ", Staff_Last_Name) as CM10 FROM Staff WHERE Staff_Code = Project_CostManager10) as CM10,
                (SELECT concat(Staff_First_Name," ", Staff_Last_Name) as CM12 FROM Staff WHERE Staff_Code = Project_CostManager12) as CM12  
                FROM Project WHERE Project_Code = '.$_SESSION['ViewProjectSingle'][100].''));
            BuildContentHeader('View Project - '.$rowProject['Project_Description'], "", "", true);
            echo '<DIV class="contentflow">
                    <P>These are the project details.</P>
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center" class="long">
                      <TR>
                        <TD colspan="5" class="header">Project Details
                        </TD>
                      </TR>
                      <TR>
                        <TD class="short">Pastel Prefix:
                        </TD>
                        <TD class="short bold">'.$rowProject['Project_Pastel_Prefix'].'
                        </TD>
                        <TD class="short">Description:
                        </TD>
                        <TD rowspan="4" colspan="4" class="short bold vtop">'.$rowProject['Project_Long_Description'].'
                        </TD>
                      </TR>
                      <TR>
                        <TD>Responsible:
                        </TD>
                        <TD class="bold">';
                          $rowStaff = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = '.$rowProject['Project_Responsible'].''));
                     echo $rowStaff['Staff_First_Name'].' '.$rowStaff['Staff_Last_Name'].' 
                        </TD>
                      </TR>
                      <TR>
                        <TD>Customer:
                        </TD>
                        <TD class="bold">';
                          $rowCust = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Customer WHERE Customer_Code = '.$rowProject['Project_Customer'].''));
                     echo $rowCust['Customer_Name'].' 
                        </TD>
                      </TR>
                      <TR>
                        <TD>Purchase Order:
                        </TD>
                        <TD class="bold">';
                          $rowPOResults = ExecuteQuery('SELECT PurchaseOrder_Number FROM PurchaseOrder p LEFT JOIN Project_PurchaseOrder pr ON p.PurchaseOrder_ID = pr.purchaseorder_id WHERE pr.project_id = '.$_SESSION['ViewProjectSingle'][100].'');
                          $poArr = array();
                          while ($po_row = MySQL_Fetch_Array($rowPOResults)) {
                             $poArr[] = $po_row['PurchaseOrder_Number'];
                          }
                     echo implode(', ', $poArr).' 
                        </TD>
                      </TR>
                      <TR>
                        <TD>Project Type:
                        </TD>
                        <TD class="bold" colspan="6">';
                          $rowType = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM ProjectType WHERE ProjectType_ID = '.$rowProject['Project_Type'].''));
                     echo $rowType['ProjectType_Description'].' 
                        </TD>
                      </TR>               
                      <TR>
                        <TD>Orders Placed:
                        </TD>
                        <TD class="bold">'.$rowProject['Project_OrdersPlaced'].' %
                        </TD>
                        <TD>Work In Progress:
                        </TD>
                        <TD class="bold" colspan="4">'.$rowProject['Project_WorkInProgress'].' %
                        </TD>
                      </TR>
                      <TR>
                        <TD>Project Complete:
                        </TD>
                        <TD class="bold">'.$rowProject['Project_Complete'].' %
                        </TD>
                        <TD>Project Closed:
                        </TD>
                        <TD class="bold" colspan="4">'.($rowProject['Project_Closed'] ? 'Yes' : 'No').'
                        </TD>
                      </TR>
                      
                      <TR>
                        <TD colspan="7"><b>Cost Managers</b>
                        </TD>
                        </TR>
                        
                        <TR>
                            <TD> Unassigned:
                            </TD>
                            <TD class="bold" colspan="6">'.$rowProject['CM'].'
                            </TD>
                        </TR>
                        
                        <TR>
                            <TD> Admin:
                            </TD>
                            <TD class="bold" colspan="6">'.$rowProject['CM1'].'
                            </TD>
                        </TR>
                        
                        <TR>
                            <TD> Electrical:
                            </TD>
                            <TD class="bold" colspan="6">'.$rowProject['CM2'].'
                            </TD>
                        </TR>

                        <TR>
                            <TD> Mechanical:
                            </TD>
                            <TD class="bold" colspan="6">'.$rowProject['CM3'].'
                            </TD>
                        </TR>
                        
                        <TR>
                            <TD> IT:
                            </TD>
                            <TD class="bold" colspan="6">'.$rowProject['CM4'].'
                            </TD>
                        </TR>
                        
                        <TR>
                            <TD> HR:
                            </TD>
                            <TD class="bold" colspan="6">'.$rowProject['CM5'].'
                            </TD>
                        </TR>
                        
                        <TR>
                            <TD> Management:
                            </TD>
                            <TD class="bold" colspan="6">'.$rowProject['CM6'].'
                            </TD>
                        </TR>
                        
                        <TR>
                            <TD> Training:
                            </TD>
                            <TD class="bold" colspan="6">'.$rowProject['CM7'].'
                            </TD>
                        </TR>
                        
                        <TR>
                            <TD> Manufacturing:
                            </TD>
                            <TD class="bold" colspan="6">'.$rowProject['CM8'].'
                            </TD>
                        </TR>
                        
                        <TR>
                            <TD> Sales and Marketing:
                            </TD>
                            <TD class="bold" colspan="6">'.$rowProject['CM9'].'
                            </TD>
                        </TR>
                        
                        <TR>
                            <TD> Software:
                            </TD>
                            <TD class="bold" colspan="6">'.$rowProject['CM10'].'
                            </TD>
                        </TR>
                        
                        <TR>
                            <TD> Design:
                            </TD>
                            <TD class="bold" colspan="6">'.$rowProject['CM12'].'
                            </TD>
                        </TR>


                    </TABLE>'; 
                    $resultSetLog = ExecuteQuery('SELECT * FROM ProjectLog WHERE ProjectLog_Project = '.$_SESSION['ViewProjectSingle'][100].' ORDER BY ProjectLog_ID ASC');        		    
                    if (MySQL_Num_Rows($resultSetLog) > 0)
                    {  
                      echo '<BR /><BR />
                            <BR /><BR />
                            <TABLE cellspacing="5" align="center" class="long">
                              <TR>';
                                BuildLabelCell('History Details', 'header', 0, 6);
                        echo '</TR>
                              <TR>';
                                BuildLabelCell('Logged', 'subheader short', 0);
                                BuildLabelCell('Logged By', 'subheader short', 0);
                                BuildLabelCell('Description', 'subheader', 0);
                        echo '</TR>';
                              $alt = true;
                              while ($rowLog = MySQL_Fetch_Array($resultSetLog))
                              {                        
                                if ($alt)
                                  $colour = 'rowA';
                                else
                                  $colour = 'rowB';
                                
                                $rowStaff = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = '.$rowLog['ProjectLog_Name'].''));
                                echo '<TR>';
                                        BuildLabelCell(GetTextualDateTimeFromDatabaseDateTime($rowLog['ProjectLog_DateTime']), $colour, 0);
                                        BuildLabelCell($rowStaff['Staff_First_Name'].' '.$rowStaff['Staff_Last_Name'], $colour, 0);
                                        BuildLabelCell($rowLog['ProjectLog_Details'], $colour, 0);
                                echo '</TR>';
                                $alt = !$alt;
                              }
                          echo '</TABLE>';  
                        }
            echo '</DIV>';
            Session_Unregister('ViewProjectSingle');
          } else if (isset($_SESSION['ViewProjectMapping'])){
              BuildContentHeader('View Project Mapping', "", "", true);
              echo '<DIV id="datagridmapping"></DIV>';
              echo '
                    <FORM method="post" action="Handlers/Projects_Handler.php">
                          <INPUT name="Type" type="hidden" value="Mapping" >
					   <INPUT tabindex="1" name="Submit" type="submit" class="button" value="Back" style="float: right;"/>
					 
                        </FORM> ';
          }
          
          else if(isset($_SESSION['ViewCostMangers'])){
            
      
            BuildContentHeader('View Cost Managers', "", "", true);

            echo'<DIV class="contentflow">
                    <P>Once you have made changes, ensure that you submit them otherwise they will not take effect.<P>
                    <BR /><BR />
                <TABLE cellspacing="5" align="center" class="long">
                      <FORM method="post" action="Handlers/Projects_Handler.php">
                        <INPUT name="Type" type="hidden" value="CostManagers">
                        <TR>
                          <TD colspan="4" class="header">Cost Managers
                          </TD>
                        </TR>
                        
                          <TR>
                             <TD>Unassigned:  <SPAN class="note">*</SPAN></TD> <TD> '; 
                          buildStaffDropdownWithMMMap(7, 'CostManager' , 'S4CostManager', 'standard seltest', $_SESSION['ViewCostMangers'][0]);
                    echo '</TD>
                        </TR>
                        
                         <TR>
                             <TD>Admin: <SPAN class="note">*</SPAN></TD> <TD>'; 
                          buildStaffDropdownWithMMMap(7, 'CostManager1' , 'S4CostManager1', 'standard seltest', $_SESSION['ViewCostMangers'][1]);
                    echo '</TD>
                        </TR>
                        

                        <TR>
                             <TD>Electrical: <SPAN class="note">*</SPAN></TD> <TD>'; 
                          buildStaffDropdownWithMMMap(7, 'CostManager2' , 'S4CostManager2', 'standard seltest', $_SESSION['ViewCostMangers'][2]);
                    echo '</TD>
                        </TR>
                        
                        <TR>
                             <TD>Mechanical: <SPAN class="note">*</SPAN></TD> <TD>'; 
                          buildStaffDropdownWithMMMap(7, 'CostManager3' , 'S4CostManager3', 'standard seltest', $_SESSION['ViewCostMangers'][3]);
                    echo '</TD>
                        </TR>       
                        <TR>
                             <TD>IT: <SPAN class="note">*</SPAN></TD> <TD>'; 
                          buildStaffDropdownWithMMMap(7, 'CostManager4' , 'S4CostManager4', 'standard seltest', $_SESSION['ViewCostMangers'][4]);
                    echo '</TD>
                        </TR>    
                        <TR>
                             <TD>HR: <SPAN class="note">*</SPAN></TD> <TD>'; 
                          buildStaffDropdownWithMMMap(7, 'CostManager5' , 'S4CostManager5', 'standard seltest', $_SESSION['ViewCostMangers'][5]);
                    echo '</TD>
                        </TR>  
                        <TR>
                             <TD>Management: <SPAN class="note">*</SPAN></TD> <TD>'; 
                          buildStaffDropdownWithMMMap(7, 'CostManager6' , 'S4CostManager6', 'standard seltest', $_SESSION['ViewCostMangers'][6]);
                    echo '</TD>
                        </TR>
                        

                        <TR>
                             <TD>Training: <SPAN class="note">*</SPAN></TD> <TD>'; 
                          buildStaffDropdownWithMMMap(8, 'CostManager7' , 'S4CostManager7', 'standard seltest', $_SESSION['ViewCostMangers'][7]);
                    echo '</TD>
                        </TR>
                        
                        <TR>
                            <TD>Manufacturing: <SPAN class="note">*</SPAN></TD> <TD>'; 
                          buildStaffDropdownWithMMMap(8, 'CostManager8' , 'S4CostManager8', 'standard seltest', $_SESSION['ViewCostMangers'][8]);
                    echo '</TD>
                        </TR>
                        
                        <TR>
                            <TD>Sales and Marketing: <SPAN class="note">*</SPAN></TD> <TD>'; 
                          buildStaffDropdownWithMMMap(7, 'CostManager9' , 'S4CostManager9', 'standard seltest', $_SESSION['ViewCostMangers'][9]);
                    echo '</TD>
                        </TR>
                        
                        <TR>
                             <TD>Software: <SPAN class="note">*</SPAN></TD> <TD>'; 
                          buildStaffDropdownWithMMMap(7, 'CostManager10' , 'S4CostManager10', 'standard seltest', $_SESSION['ViewCostMangers'][10]);
                    echo '</TD>
                        </TR>

                        <TR>
                             <TD>Design: <SPAN class="note">*</SPAN></TD> <TD>'; 
                          buildStaffDropdownWithMMMap(7, 'CostManager12' , 'S4CostManager12', 'standard seltest', $_SESSION['ViewCostMangers'][12]);
                    echo '</TD>
                        </TR>
                        
                         <TR>
                          <TD colspan="4" class="center">
                            <INPUT tabindex="15" name="Submit" type="submit" class="button" value="Submit" />   
                            <INPUT tabindex="16" name="Submit" type="submit" class="button" value="Cancel" />                  
                          </TD>
                        </TR>


                      </FORM>
                    </TABLE></DIV>';  
              
              
          }
          
          else
          {
            if ($_SESSION['cAuth'] & 32)
            {
              BuildContentHeader('Maintenance', "", "", true);
              echo '<DIV class="contentflow">
                      <P>New projects can be added by using the form below. Details on projects already listed can also be edited.</P>
                      <BR /><BR />
                      <TABLE cellspacing="5" align="center" class="standard">
                        <FORM method="post" action="Handlers/Projects_Handler.php">
                          <INPUT name="Type" type="hidden" value="Maintain">
                          <TR>
                            <TD colspan="4" class="header">Add
                            </TD>
                          </TR>
                          <TR>
                            <TD colspan="3">To add a new project, click Add and complete the form that is displayed.
                            </TD>
                            <TD class="right">
                              <INPUT tabindex="1" name="Submit" type="submit" class="button" value="Add" />                   
                            </TD>
                          </TR>
                          <TR>
                            <TD colspan="4" class="header">Edit
                            </TD>
                          </TR>
                          <TR>
                            <TD colspan="4">To edit a project, specify the particulars, click Edit and complete the form that is displayed.
                            </TD>
                          </TR>
                          <TR>
                            <TD class="short">Project:
                              <SPAN class="note">*
                              </SPAN>
                            </TD>
                            <TD>';
           
                              BuildProjectSelector(2, 'ProjectEdit', 'seltest', "", 'All', true);
                      echo '</TD>
                            <TD colspan="2" class="right">
                              <INPUT tabindex="3" name="Submit" type="submit" class="button" value="Edit" />                   
                            </TD>
                          </TR>
                          <TR>
                            <TD colspan="4" class="header">History
                            </TD>
                          </TR>
                          <TR>
                            <TD colspan="4">To update project history, specify the particulars, click Update and complete the form that is displayed.
                            </TD>
                          </TR>
                          <TR>
                            <TD class="short">Project:
                              <SPAN class="note">*
                              </SPAN>
                            </TD>
                            <TD>';
                              BuildProjectSelector(4, 'ProjectUpdate', 'seltest', "", 'Open', true);
                      echo '</TD>
                            <TD colspan="2" class="right">
                              <INPUT tabindex="5" name="Submit" type="submit" class="button" value="Update" />                   
                            </TD>
                          </TR>
                           <TR>
                            <TD colspan="4" class="header">Cost Managers
                            </TD>
                          </TR>
                          <TR>
                            <TD colspan="3">Assign default cost managers.
                            </TD>
                            <TD class="right">
                              <INPUT tabindex="1" name="Submit" type="submit" class="button" value="Assign"/>                   
                            </TD>
                          </TR>
                          

                          
                        </FORM>
                      </TABLE>
                    </DIV>';
              BuildContentHeader('View Projects', "", "", true);
            } else
              BuildContentHeader('View Projects', "", "", true);
            if ($_SESSION['cAuth'] & 64)
            {
              echo '<DIV class="contentflow">
                      <P>Projects can be viewed by using the form below.</P>
                      <BR /><BR />
                      <TABLE cellspacing="5" align="center" class="standard">
                        <FORM method="post" action="Handlers/Projects_Handler.php">
                          <INPUT name="Type" type="hidden" value="View">
                            <TR>
                              <TD colspan="4" class="header">View
                              </TD>
                            </TR>
                            <TR>
                              <TD colspan="4">To view projects, specify the particulars and click View. The date range is only required when viewing a project summary by it\'s last update.
                              </TD>
                            </TR>
                            <TR>
                              <TD class="short">Report Type:
                                <SPAN class="note">*
                                </SPAN>
                              </TD>
                              <TD colspan="3">';
                                BuildProjectsReportTypeSelector(6, 'Project', 'standard');
                        echo '</TD>
                            </TR>
                            <TR>
                              <TD>Person Responsible:
                              </TD>
                              <TD colspan="3">';
                                BuildStaffSelector(7, 'Name', 'standard seltest', $_SESSION['cUID'], true);
                        echo '</TD>
                            </TR>
                            <TR>
                              <TD>Project Status:
                              </TD>
                              <TD colspan="3">';
                                BuildProjectsProjectTypeSelector(8, 'ProjectStatus', 'standard');
                        echo '</TD>
                            </TR>
                            <TR>
                              <TD>Project Type:
                              </TD>
                              <TD colspan="3">';
                                BuildProjectTypeSelector(9, 'ProjectType', 'standard', "");
                        echo '</TD>
                            </TR>
                            <TR>
                              <TD>Date Range:
                                <SPAN class="note">*
                                </SPAN>
                              </TD>
                              <TD>';
                                BuildDaySelector(10, 'StartDay', "");
                                echo '&nbsp;';
                                BuildMonthSelector(11, 'StartMonth', "");
                                echo '&nbsp;';
                                BuildYearSelector(12, 'StartYear', "");
                                echo ' to ';
                                BuildDaySelector(13, 'EndDay', "");
                                echo '&nbsp;';
                                BuildMonthSelector(14, 'EndMonth', "");
                                echo '&nbsp;';
                                BuildYearSelector(15, 'EndYear', "");
                        echo '</TD>
                              <TD colspan="2" class="right">
                                <INPUT tabindex="16" name="Submit" type="submit" class="button" value="View" />                   
                              </TD>
                            </TR>
                          </FORM>
                          <FORM method="post" action="Handlers/Projects_Handler.php">
                            <INPUT name="Type" type="hidden" value="ViewSingle">
                            <TR>';
                              BuildLabelCell('View Single', 'header', 0, 4);
                      echo '</TR>
                            <TR>';
                              BuildLabelCell('To view a project, specify the particulars and click View.', "", 0, 4);
                      echo '</TR>
                            <TR>';
                              BuildLabelCell('Project:', 'short');
                        echo '<TD>';
                                BuildProjectSelector(17, 'Project', 'long seltest', "", 'All', true);
                        echo '</TD>';
                              BuildSubmitCell(18, 'View', 'right', 2);
                      echo '</TR>

                    </FORM>
					  
					  <FORM method="post" action="Handlers/Projects_Handler.php">
                          <INPUT name="Type" type="hidden" value="ViewMapping">
					   <TR>
                            <TD colspan="4" class="header">View Project Mapping
                            </TD>
                          </TR>
                          <TR>
                            <TD colspan="3">To view project mapping, click View.
                            </TD>
                            <TD class="right">
                              <INPUT tabindex="1" name="Submit" type="submit" class="button" value="View" />                   
                            </TD>
                          </TR>
					 
                        </FORM>                        </TABLE>
                      </DIV>';
            } else
            {
              echo '<DIV class="contentflow">
                      <P>Projects can be viewed by using the form below.</P>
                      <BR /><BR />
                      <TABLE cellspacing="5" align="center" class="standard">
                        <FORM method="post" action="Handlers/Projects_Handler.php">
                          <INPUT name="Type" type="hidden" value="View">
                          <INPUT name="Project" type="hidden" value="0">
                            <TR>
                              <TD colspan="4" class="header">View
                              </TD>
                            </TR>
                            <TR>
                              <TD colspan="4">To view projects, specify the particulars and click View.
                              </TD>
                            </TR>
                            <TR>
                              <TD class="short">Project Status:
                              </TD>
                              <TD>';
                                BuildProjectsProjectTypeSelector(7, 'ProjectStatus', 'standard');
                        echo '</TD>
                              <TD colspan="2" class="right">
                                <INPUT name="Project" type="hidden" value="0" />
                                <INPUT tabindex="8" name="Submit" type="submit" class="button" value="View" />                   
                              </TD>
                            </TR>
                          </FORM>
                          <FORM method="post" action="Handlers/Projects_Handler.php">
                            <INPUT name="Type" type="hidden" value="ViewSingle">
                            <TR>';
                              BuildLabelCell('View Single', 'header', 0, 4);
                      echo '</TR>
                            <TR>';
                              BuildLabelCell('To view a project, specify the particulars and click View.', "", 0, 4);
                      echo '</TR>
                            <TR>';
                              BuildLabelCell('Project:', 'short');
                        echo '<TD>';
                                BuildProjectSelector(9, 'Project', 'long seltest', "", 'All', true);
                        echo '</TD>';
                              BuildSubmitCell(10, 'View', 'right', 2);
                      echo '</TR>
					  
					  </FORM>
					  
					  <FORM method="post" action="Handlers/Projects_Handler.php">
                          <INPUT name="Type" type="hidden" value="ViewMapping">
					   <TR>
                            <TD colspan="4" class="header">View Project Mapping
                            </TD>
                          </TR>
                          <TR>
                            <TD colspan="3">To view project mapping, click View.
                            </TD>
                            <TD class="right">
                              <INPUT tabindex="1" name="Submit" type="submit" class="button" value="View" />                   
                            </TD>
                          </TR>
					 
                        </FORM>
                        </TABLE>
                      </DIV>';
            }
          }
          //////////////////////////////////////////////////////////////////////
        ?>
      </div>
    </div>
    </div>
    </section
    </DIV>
    <?php
      // PHP SCRIPT ////////////////////////////////////////////////////////////
      BuildFooter();
      //////////////////////////////////////////////////////////////////////////
    ?>
      
      <SCRIPT>
          $(document).ready(function(){
              
			  
			  // if the div exsist

              if($('#datagridmapping').length){
//                  console.log("Im here");
                  $.ajax({
                      url: "Handlers/Projects_Handler.php", //refer to the file
                      type: "GET", //send it through get method
                      dataType: "json",
                      data: { //arguments
                          function: 'getProjectMappingCode'
                      },

                      success: function(response) { //if returns what must happen
//                          console.log(response);
                          $('#datagridmapping').html("");
                          $('#datagridmapping').dxDataGrid({
                              dataSource: response,
                              filterRow: { visible: true },
                              columns: ['Project_NameS4', 'Project_NameMM'],
                              allowFiltering: false
                          });
                          $('#Loader').css("display","none");
                      },

                      beforeSend: function() {
                          $('#datagridmapping').html('<div id = "Loader" class="loader">Loading...</div>');
                      },

                      error: function(xhr) {
                          console.log(xhr);
                      }
                  });
              }

	  
              if($('#cbAddMM').prop("checked")){
                      $('#MapProjectSection').addClass('disable');
                       $(".AutonetProjectDetailsSection").removeClass('hide');
               }
              $('#cbAddMM').on('click', function(){
                  if($('#cbAddMM').prop("checked")){
                      $('#MapProjectSection').addClass('disable');
                      $('#cbAddMM').attr("value","checked");
                      $(".AutonetProjectDetailsSection").removeClass('hide');
                  }
                  else{
                    $('#MapProjectSection').removeClass('disable'); 
                    $('#cbAddMM').attr("value","Notchecked");
                    $(".AutonetProjectDetailsSection").addClass('hide');
                  }
              });
              $('[name="AddMMPrefix"]').on('change', function(){
                  console.log('found it');
//                  var value = $('[name="AddMMPrefix"]').find(":selected").val();
//                   console.log($('[name="AddMMPrefix"]').find(":selected").text());
////                    if(value === ""){
////                        
////                    }
////                    else{
////                        
////                    }
//                   console.log(value);
//                //$('#AddtoAutonetSection')..addClass('disable');
              });
           
              $('#S4ResponsibleS4A').on('change',function(){
                    if($('#MMResponsible :selected').text() === "<...>" ){
                         var MMCode =  $('#S4ResponsibleS4A').find(":selected").attr('data-MMCode');
                        $('#MMResponsible').val(MMCode);
                    }
              });
           
               $('#S4CostManager3').on('change', function(){
                   console.log("changed"); 
                   if($('#MMCostManager :selected').text() === "<...>" ){
                       console.log(" was null");
                        var MMCode =  $('#S4CostManager3').find(":selected").attr('data-MMCode');
                       $('#MMCostManager').val(MMCode);
                   }
                   
                });
           
           
              $('#S4CostManager').on('change',function(){
                  var MMCode =  $('#S4CostManager').find(":selected").attr('data-MMCode');
                   if(MMCode !== ""){
                        $('#MMCostManager').val(MMCode);
                   }
              });
           
             
          });
             
          
      </SCRIPT>
      
  </BODY>
</HTML>