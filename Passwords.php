<?php
// DETAILS ///////////////////////////////////////////////////////////////////
//                                                                          //
//                    Last Edited By: Gareth Ambrose                        //
//                        Date: 12 May 2008                                 //
//                                                                          //
//////////////////////////////////////////////////////////////////////////////
// This page allows users to change their passwords.                        //
//////////////////////////////////////////////////////////////////////////////

include 'Scripts/Include.php';
SetSettings();
CheckAuthorisation('Passwords.php');

//////////////////////////////////////////////////////////////////////////////
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3c.org/TR/1999/REC-html401-19991224/loose.dtd">
<HTML>
<HEAD>
    <?php
    // PHP SCRIPT ////////////////////////////////////////////////////////////
    BuildHead('Passwords');
    include('Scripts/header.php');
    //////////////////////////////////////////////////////////////////////////
    ?>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <link rel="stylesheet" type="text/css" href="Stylesheets/font-awesome.min.css"/>
    <script src="Scripts/DevExpress/js/jquery-2.1.3.min.js" type="text/javascript"></script>
    <script src="Scripts/DevExpress/js/jszip.min.js" type="text/javascript"></script>
    <link href="Stylesheets/chosen.css" rel="stylesheet" type="text/css"/>
    <script src="Scripts//jquery-1.9.1.min.js"></script>
    <STYLE>
        ul, li {
            margin:0;
            padding:0;
            list-style-type:none;
        }

        form > ul > li {
            margin:10px 20px;

        }
        form > ul  > li:last-child {
            text-align:center;
            margin:20px 0 25px 0;
        }
        #notifyDiv,#errorDiv{
            display: none;
        }


        #pswd_info, #cmfrm_pswd_info {
            background:#fefefe;
            font-size:.875em;
        }
        #pswd_info h4 {
            margin:0 0 10px 0;
            padding:0;
            font-weight:normal;
        }
        #pswd_info::before, #cmfrm_pswd_info::before {
            content: "\25B2";
            position:absolute;
            top:-12px;
            left:45%;
            font-size:14px;
            line-height:14px;
            color:#ddd;
            text-shadow:none;
            display:block;
        }
        .invalid {
            color:#ec3f41;
        }
        .valid {
            color:#3a7d34;
        }
        #pswd_info, #cmfrm_pswd_info {
            display:none;
        }
        input.button{
            cursor: pointer;
        }
        input.button.disabled {
            opacity: 0.65;
            cursor: not-allowed;
        }
        .is-disabled {opacity: .5;pointer-events: none;}
    </STYLE>
    <SCRIPT src="Scripts/FileScripts/Passwords.js"></SCRIPT>
    <!--      <script>-->
    <!--          $(document).ready(function() {-->
    <!--              $(".chosen-select").chosen({-->
    <!--                  no_results_text: "No results matched"-->
    <!--              });-->
    <!--          });-->
    <!--      </script>-->
</HEAD>
<BODY>
<?php     // PHP SCRIPT ////////////////////////////////////////////////////////////
BuildBanner();
//////////////////////////////////////////////////////////////////////////
?>
<DIV class="contentcontainer">
    <?php
    // PHP SCRIPT //////////////////////////////////////////////////////////
    BuildMenu('Main', 'Passwords.php');
    ////////////////////////////////////////////////////////////////////////
    ?>
    <DIV class="content">
        <BR /><BR />
        <?php

        if(isset($_SESSION['PasswordsSuccess']))
            echo '<script> window.location.replace("index.php"); </script>';

        if(isset($_SESSION['PasswordsWeakPassword']))
            echo "<script>$('.navitop').addClass('is-disabled');</script>";

        // PHP SCRIPT ////////////////////////////////////////////////////////
        BuildMessageSet('Passwords');
        //$_SESSION['PasswordsWeakPassword'] = 'geh';
        //////////////////////////////////////////////////////////////////////
        ?>
        <?php
        // PHP SCRIPT ////////////////////////////////////////////////////////
        BuildContentHeader('Intranet', "", "", false);
        //////////////////////////////////////////////////////////////////////
        ?>
        <DIV class="contentflow">
            <?php
            // PHP SCRIPT ////////////////////////////////////////////////////////
            BuildMessageSet('Passwords');
            //////////////////////////////////////////////////////////////////////
            ?>
            <P>To change your intranet password use the form below.</P>
            <BR /><BR />
            <TABLE cellspacing="5" align="center" class="short">
                <FORM method="post" action="Handlers/Passwords_Handler.php">
                    <INPUT name="Type" type="hidden" value="Change">
                    <TR>
                        <TD colspan="2" class="header">Password Details
                        </TD>
                    </TR>
                    <TR>
                        <TD class="short">Old Password:
                            <SPAN class="note">*
                    </SPAN>
                        </TD>
                        <TD>
                            <INPUT tabindex="1" name="OldPassword" type="password" class="text long" />
                        </TD>
                    </TR>
                    <TR>
                        <TD id="pswdLabel">New Password:
                            <SPAN class="note">*
                    </SPAN>
                        </TD>
                        <TD>
                            <INPUT tabindex="2" name="NewPassword" type="password" class="text long" />
                            <div id="pswd_info">
                                <h4>Password must meet the following requirements:</h4>
                                <ul>
                                    <li id="letter" class="invalid"><i class="fa fa-times" aria-hidden="true"></i> At least <strong>one letter</strong></li>
                                    <li id="capital" class="invalid"><i class="fa fa-times" aria-hidden="true"></i> At least <strong>one capital letter</strong></li>
                                    <li id="number" class="invalid"><i class="fa fa-times" aria-hidden="true"></i> At least <strong>one number</strong></li>
                                    <li id="length" class="invalid"><i class="fa fa-times" aria-hidden="true"></i> Be at least <strong>8 characters</strong></li>
                                </ul>
                            </div>
                        </TD>
                    </TR>
                    <TR>
                        <TD id="cmfrm_pswd_label">Confirm New Password:
                            <SPAN class="note">*
                    </SPAN>
                        </TD>
                        <TD>
                            <INPUT tabindex="3" name="NewPasswordConfirm" type="password" class="text long" />

                            <div id="cmfrm_pswd_info">
                                <h4></h4>
                                <ul>
                                    <li id="confirmPswd" class="invalid"><i class="fa fa-times" aria-hidden="true"></i> Must match with the<strong> new password</strong></li>
                                </ul>
                            </div>
                        </TD>
                    </TR>
                    <TR>
                        <TD colspan="2" class="center">
                            <INPUT tabindex="4" type="submit" class="button disabled" value="Submit" />
                        </TD>
                    </TR>
                </FORM>
            </TABLE>
            <DIV>
                <BR>
                <SPAN class="note">*
            </SPAN>
                These fields are required.
            </DIV>
        </DIV>
        <?php
        if($_SESSION['cAuth'] & 64){
        BuildContentHeader('Reset Password', "", "", true);
        ?>

        <div class="contentflow">
            <DIV id="notifyDiv" class="notify">
                Requested operation has completed successfully.</DIV><DIV id="errorDiv" class="error">Requested operation failed. An error occurred during the submission process.
            </DIV>
            <BR /><BR />
            <p>To reset a user's password, use the form Below.</p>
            <br/>
            <br/>
            <TABLE cellspacing="5" align="center" class="short">
                <TR>
                    <TD colspan="2" class="header">Reset Password
                    </TD>
                </TR>
                <TR>
                    <TD >Staff Member:
                        <SPAN class="note">*
                    </SPAN>
                    </TD>
                    <TD>
                        <?php BuildStaffSelector(20, 'Staff', 'standard chosen-select', " ", true); ?>
                    </TD>
                </TR>
                <TR>
                    <TD colspan="2" class="center">
                        <input type="submit" tabindex="4" id="bntReset" class="button" value="Reset Password"/></TD>
                </TR>
            </TABLE>
            <?php } ?>
        </div>

        <?php
        // PHP SCRIPT ////////////////////////////////////////////////////////
        BuildContentHeader('Network and Mail', "", "", true);
        if (SubStr($_SERVER['REMOTE_ADDR'], 0, 10) == '192.168.0.')
        {
            echo '<DIV class="contentflow">
                    <P>To change your network and email password use the form below.</P>
                    <BR /><BR />
                    <IFRAME src="../user-password" width="95%" height="385" frameborder="0"></IFRAME>
                  </DIV>';
        } else
            echo '<DIV class="contentflow">
                    <P>Your network and email password can only be changed from Head Office\'s local network. If you would like it changed for you then please ask Andrew or Gareth to do so.</P>
                  </DIV>';
        //////////////////////////////////////////////////////////////////////
        ?>
        <BR /><BR />
    </DIV>
</DIV>
<?php
// PHP SCRIPT ////////////////////////////////////////////////////////////
BuildFooter();
//////////////////////////////////////////////////////////////////////////
?>
</BODY>
</HTML>