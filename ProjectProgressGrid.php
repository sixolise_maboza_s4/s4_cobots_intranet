<?php
require_once 'Scripts/jq-config.php';
require_once ABSPATH."php/jqGrid.php";
require_once ABSPATH."php/jqGridPdo.php";
require_once 'Scripts/Include.php'; 

//$conn = new PDO(DB_DSN,DB_USER,DB_PASSWORD);

$conn = mysql_connect('localhost', 's4mysql', 'mysql');

$conn->query("SET NAMES utf8");

$grid = new jqGridRender($conn);
$grid->SelectCommand = 'SELECT * FROM Project WHERE Project_Closed = 0';
// Set the table to where you add the data
$grid->table = 'Project';
//$grid->showError = true; 

// Set output format to json
$grid->dataType = 'json';

// Definition of the labels
$mylabels = array("Project_Code"=>"Project Code", 
                  "Project_DateTime_Created"=>"Created", 
                  "Project_DateTime_Updated"=>"Updated", 
                  "Project_PurchaseOrder"=>"Order", 
                  "Project_Pastel_Prefix"=>"Code", 
                  "Project_Description"=>"Description", 
                  "Project_Long_Description"=>"Comments",
                  "Project_Customer"=>"Customer", 
                  "Project_Responsible"=>"Responsible", 
                  "Project_WorkInProgress"=>"Progress", 
                  "Project_Complete"=>"Complete",
                  "Project_OrdersPlaced"=>"Orders Placed", 
                  "Project_Type"=>"Type", 
                  "Project_Closed"=>"Status",
		  "Project_MaterialBudget"=>"Material Budget",
		  "Project_LabourBudget"=>"Labour Budget");

// Let the grid create the model with the desired labels
$grid->setColModel(null, null, $mylabels);

// Set the url from where we obtain the data
$grid->setUrl('ProjectProgressGrid.php');


// javascriptCode for the datepicker
$mydatepicker = <<<DATEPICK
function(elm){
	setTimeout(function(){
       	jQuery(elm).datepicker({dateFormat:'yy-mm-dd'});
        jQuery('.ui-datepicker').css({'font-size':'90%'});
       },200);
}
DATEPICK;


$grid->setDbTime('Y-m-d H:i:s');
$grid->setDbDate('Y-m-d H:i:s');
$grid->setUserDate('Y-m-d');
$grid->setUserTime('Y-m-d');


$grid->setColProperty('Project_Code', array("hidden"=>true, "width"=>50, "editable"=>false));
$grid->setColProperty('Project_Description', array("frozen"=>true, "width"=>500));
$grid->setColProperty('Project_Long_Description', array("width"=>500));
$grid->setColProperty('Project_Customer', array("width"=>400, "editable"=>true));
$grid->setColProperty('Project_Responsible', array("width"=>300, "editable"=>true));
$grid->setColProperty('Project_DateTime_Created',array("editable"=>false, "formatter"=>"date","formatoptions"=>array("srcformat"=>"Y-m-d H:i:s", "newformat"=>"Y-m-d"),"editoptions"=>array("value"=>date("Y-m-d"), "readonly"=>false, "dataInit"=>"js:".$mydatepicker), "searchoptions"=>array("dataInit"=>"js:".$mydatepicker)));
$grid->setColProperty('Project_DateTime_Updated',array("formatter"=>"date","formatoptions"=>array("srcformat"=>"Y-m-d H:i:s", "newformat"=>"Y-m-d"), "editoptions"=>array("value"=>date("Y-m-d"), "readonly"=>false, "dataInit"=>"js:".$mydatepicker), "searchoptions"=>array("dataInit"=>"js:".$mydatepicker)));
$grid->setColProperty('Project_PurchaseOrder', array("width"=>300, "editable"=>true));

$grid->setSubGridGrid("ProjectProgressSubGrid.php", array('Project_Code', 'Project_MaterialBudget', 'Project_LabourBudget'));

// Set some grid options
$grid->setGridOptions(array(
    "rowNum"=>25,
    "rowList"=>array(25,50,100),
    "sortname"=>"Project_Code",
    "sortorder"=>"desc",
    "width"=>1500,
    "height"=>350,
));

//enable toolbarsearch
$grid->toolbarfilter = true;

$grid->setSelect("Project_Customer", "SELECT DISTINCT Customer_Code, Customer_Name FROM Customer", true, true, true, array(""=>"..."));
$grid->setSelect("Project_Responsible", "SELECT DISTINCT CAST(Staff_Code AS UNSIGNED), CONCAT_WS(' ', Staff_First_Name, Staff_Last_Name) FROM Staff", true, true, true, array(""=>"..."));
$grid->setSelect("Project_Type", "SELECT DISTINCT ProjectType_ID, ProjectType_Description FROM ProjectType", true, true, true, array(""=>"..."));
$grid->setSelect("Project_PurchaseOrder", "SELECT DISTINCT PurchaseOrder_ID, PurchaseOrder_Number FROM PurchaseOrder", true, true, true, array(""=>"..."));



$mystatus = array("0"=>"Open", "1"=>"Complete");
$grid->setSelect("Project_Closed", $mystatus , true, true, true, array(""=>"..."));


// add navigator with the default properties
$grid->navigator = true;

//but use single search
$grid->setNavOptions('navigator',array('add'=>false, 'edit'=>true, 'search'=>false, 'del'=>false));


$myevent = <<<ONSELECT
function(rowid, selected)
{
   if (rowid) {
    var value = $("#grid").jqGrid("getRowData",rowid);
	//console.log(value);
	var project = value['Project_Code'];
	var name = encodeURIComponent(value['Project_Description']);
	//alert("Project Code: " + project + ". Project Name: " + name);
	setStatusChart(project, name);
  }
}
ONSELECT;
$grid->setGridEvent('onSelectRow',$myevent);


$grid->renderGrid('#grid','#pager',true, null, null, true,true);

// Enjoy

$conn = null;
?>
