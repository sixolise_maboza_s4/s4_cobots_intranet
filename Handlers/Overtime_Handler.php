<?php
// DETAILS ///////////////////////////////////////////////////////////////////
//                                                                          //
//                    Last Edited By: Gareth Ambrose                        //
//                        Date: 28 September 2009                           //
//                                                                          //
//////////////////////////////////////////////////////////////////////////////
// This page handles the back-end for the Overtime page.                    //
//////////////////////////////////////////////////////////////////////////////

include '../Scripts/Include.php';
SetSettings();
CheckLoggedIn();
$_POST = Replace('"', '\'\'', $_POST);


if(isset($_GET['function'])){
    switch ($_GET['function']){
        case 'buildOvertimedatagrid':
            echo json_encode(GetDataForOvertimeApproveMulti($_GET['staff_code']));
            break;
        case 'SaveOvertimeMulti':
            ApproveSaveApproveOvertimeMultiple();
            break;
        case 'ViewReminderList':
            ViewReminderList();
            break;
        default:
            break;
    }
}
else if(isset($_POST['function'])){

    switch ($_POST['function']){
        case 'DeletePayoutReminder':
            DeletePayoutReminder();
            break;
        case 'UpdatePayoutReminder':
            UpdatePayoutReminder();
        default:
            break;
    }
}
else{
    switch ($_POST['Type'])
    {
        //User has submitted information for recording overtime.
        case 'Add':
            HandleAdd();
            break;
        //User has submitted information for approving banked overtime use.
        case 'ApproveBankedOvertime':
            HandleApproveBankedOvertime();
            break;
        //User has submitted information for approving overtime.
        case 'ApproveNormalOvertime':
            HandleApproveNormalOvertime();
            break;
        //User has submitted modified information for recorded overtime.
        case 'Edit':
            HandleEdit();
            break;
        //User has submitted information for transferring excess overtime.
        case 'Excess':
            HandleExcess();
            break;
        //User has selected to Approve or Request banked overtime use, Transfer or Transfer Excess banked overtime.
        case 'MaintainBankedOvertime':
            HandleMaintainBankedOvertime();
            break;
        //User has selected to Add, Approve, Edit or Request overtime.
        case 'MaintainNormalOvertime':
            HandleMaintainNormalOvertime();
            break;
        //User has submitted information for requesting banked overtime use.
        case 'RequestBankedOvertime':
            HandleRequestBankedOvertime();
            break;
        //User has submitted information for requesting overtime.
        case 'RequestNormalOvertime':
            HandleRequestNormalOvertime();
            break;
        //User has submitted information for transferring overtime.
        case 'Transfer':
            HandleTransfer();
            break;
        //User has selected to view overtime.
        case 'View':
        case 'ViewStatistics':
            HandleView();
            break;
        case 'CancleSesssoin':
            unsetSession();
            break;
        case 'SetOvertimeExpensesDueDate':
            //method
            SetExpensesReminder();
            break;
        //User has reached this page incorrectly. If they are not authorised they are redirected to the main page from the Overtime page.
        default:
            break;
    }
    Header('Location: ../Overtime.php?'.Rand());
}






//////////////////////////////////////////////////////////////////////////////
// Checks that all the required fields have values and that these values    //
// are valid.                                                               //
//////////////////////////////////////////////////////////////////////////////
function CheckFields()
{
    switch ($_POST['Type'])
    {
        case 'Add':
        case 'Edit':
            if ($_POST['Type'] == 'Add')
                $row = MySQL_Fetch_Array(ExecuteQuery('SELECT OvertimePreApp_Start, OvertimePreApp_End, (OvertimePreApp_Hours - OvertimePreApp_OvertimeHoursUsed) AS OvertimeAvail FROM OvertimePreApp WHERE OvertimePreApp_ID = "'.$_SESSION['AddOvertime'][0].'"'));
            else
            {
                $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM OvertimeBank WHERE OvertimeBank_ID = '.$_SESSION['EditOvertime'][0].''));
                $row = MySQL_Fetch_Array(ExecuteQuery('SELECT OvertimePreApp_Start, OvertimePreApp_End, (OvertimePreApp_Hours - OvertimePreApp_OvertimeHoursUsed + '.$rowTemp['OvertimeBank_Hours'].') AS OvertimeAvail FROM OvertimePreApp WHERE OvertimePreApp_ID = "'.$rowTemp['OvertimeBank_PreApproved'].'"'));
            }

            if ($_POST['Hours'] == "")
                return false;

            if (!Is_Numeric($_POST['Hours']))
                return false;

            if ($_POST['Hours'] <= 0)
                return false;

            if (!(CheckDate($_POST['StartMonth'], $_POST['StartDay'], $_POST['StartYear'])) || !(CheckDate($_POST['EndMonth'], $_POST['EndDay'], $_POST['EndYear'])))
                return false;

            if (DatabaseDateLater($row['OvertimePreApp_Start'], GetDatabaseDate($_POST['StartDay'], $_POST['StartMonth'], $_POST['StartYear'])) || DatabaseDateLater(GetDatabaseDate($_POST['EndDay'], $_POST['EndMonth'], $_POST['EndYear']), $row['OvertimePreApp_End']))
                return false;

            if (DatabaseDateTimeLater(GetDatabaseDateTime($_POST['StartHour'], $_POST['StartMinute'], $_POST['StartDay'], $_POST['StartMonth'], $_POST['StartYear']), GetDatabaseDateTime($_POST['EndHour'], $_POST['EndMinute'], $_POST['EndDay'], $_POST['EndMonth'], $_POST['EndYear'])))
                return false;
            break;
        case 'RequestBankedOvertime';
            if (($_POST['Hours'] == "") || ($_POST['Authorise'] == ""))
                return false;

            if (!Is_Numeric($_POST['Hours']))
                return false;

            //$row = MySQL_Fetch_Array(ExecuteQuery('SELECT SUM(OvertimeBank_Hours) AS Available FROM OvertimeBank WHERE OvertimeBank_Name = '.$_SESSION['cUID'].' AND OvertimeBank_IsApproved = "1"'));
            //if (($_POST['Hours'] <= 0) || ($_POST['Hours'] > $row['Available']))
            //  return false;

            if (!(CheckDate($_POST['StartMonth'], $_POST['StartDay'], $_POST['StartYear'])) || !(CheckDate($_POST['EndMonth'], $_POST['EndDay'], $_POST['EndYear'])))
                return false;

            if (DatabaseDateTimeLater(GetDatabaseDateTime($_POST['StartHour'], $_POST['StartMinute'], $_POST['StartDay'], $_POST['StartMonth'], $_POST['StartYear']), GetDatabaseDateTime($_POST['EndHour'], $_POST['EndMinute'], $_POST['EndDay'], $_POST['EndMonth'], $_POST['EndYear'])))
                return false;
            break;
        case 'RequestNormalOvertime';
            if (($_POST['Hours'] == "") || ($_POST['Description'] == "") || ($_POST['Authorise'] == "") || ($_POST['Project'] == ""))
                return false;

            if (!Is_Numeric($_POST['Hours']))
                return false;

            if ($_POST['Hours'] <= 0)
                return false;

            if (!(CheckDate($_POST['StartMonth'], $_POST['StartDay'], $_POST['StartYear'])) || !(CheckDate($_POST['EndMonth'], $_POST['EndDay'], $_POST['EndYear'])))
                return false;

            if (DatabaseDateTimeLater(GetDatabaseDateTime($_POST['StartHour'], $_POST['StartMinute'], $_POST['StartDay'], $_POST['StartMonth'], $_POST['StartYear']), GetDatabaseDateTime($_POST['EndHour'], $_POST['EndMinute'], $_POST['EndDay'], $_POST['EndMonth'], $_POST['EndYear'])))
                return false;

            //Can only request overtime up until 7 days after it has been worked.
            if (!($_SESSION['cAuth'] & 64))
                if (DatabaseDateTimeLater(Date('Y-m-d', MkTime(0, 0, 0, Date("m")  , Date("d") - 7, Date("Y"))), GetDatabaseDateTime($_POST['EndHour'], $_POST['EndMinute'], $_POST['EndDay'], $_POST['EndMonth'], $_POST['EndYear'])))
                    return false;

            break;
        case 'Transfer':
            if (($_POST['Hours'] == "") || ($_POST['Description'] == ""))
                return false;

            if (!Is_Numeric($_POST['Hours']))
                return false;

            $row = MySQL_Fetch_Array(ExecuteQuery('SELECT SUM(OvertimeBank_Hours) AS Available FROM OvertimeBank WHERE OvertimeBank_Name = "'.$_SESSION['TransferOvertime'][0].'" AND OvertimeBank_IsApproved = "1"'));
            if (($_POST['Hours'] <= 0) || ($_POST['Hours'] > $row['Available']))
                return false;
            break;
        case 'View':
            //if ($_SESSION['cAuth'] & 8)
            //  if ($_POST['Staff'] == "")
            //    return false;

            if (!(CheckDate($_POST['StartMonth'], $_POST['StartDay'], $_POST['StartYear'])) || !(CheckDate($_POST['EndMonth'], $_POST['EndDay'], $_POST['EndYear'])))
                return false;

            if (DatabaseDateLater(GetDatabaseDate($_POST['StartDay'], $_POST['StartMonth'], $_POST['StartYear']), GetDatabaseDate($_POST['EndDay'], $_POST['EndMonth'], $_POST['EndYear'])))
                return false;
            break;
        case 'ViewStatistics':
            if (DatabaseDateLater(GetDatabaseDate(1, $_POST['StartMonth'], $_POST['StartYear']), GetDatabaseDate(1, $_POST['EndMonth'], $_POST['EndYear'])))
                return false;
            break;
        default:
            break;
    }

    return true;
}

//////////////////////////////////////////////////////////////////////////////
// Handles the user's submission of information for recording overtime.     //
//////////////////////////////////////////////////////////////////////////////
function HandleAdd()
{
    $_SESSION['AddOvertime'][1] = $_POST['StartYear'].$_POST['StartMonth'].$_POST['StartDay'].$_POST['StartHour'].$_POST['StartMinute'];
    $_SESSION['AddOvertime'][2] = $_POST['Description'];
    $_SESSION['AddOvertime'][3] = $_POST['EndYear'].$_POST['EndMonth'].$_POST['EndDay'].$_POST['EndHour'].$_POST['EndMinute'];
    $_SESSION['AddOvertime'][4] = $_POST['Hours'];

    switch ($_POST['Submit'])
    {
        case 'Cancel':
            Session_Unregister('AddOvertime');
            break;
        case 'Submit':
            if (CheckFields())
            {
                $row = MySQL_Fetch_Array(ExecuteQuery('SELECT OvertimePreApp.*, (OvertimePreApp_Hours - OvertimePreApp_OvertimeHoursUsed) AS OvertimeAvail, Project_Description FROM OvertimePreApp, Project WHERE OvertimePreApp_ID = "'.$_SESSION['AddOvertime'][0].'" AND OvertimePreApp_Project = Project_Code'));

                if ($row['OvertimePreApp_Bank'] == '1')
                    $query = 'INSERT INTO OvertimeBank VALUES("'.$row['OvertimePreApp_Name'].'", "'.Date('Y-m-d H:i:s').'", "'.$_POST['Hours'].'", "'.$row['OvertimePreApp_Project'].'", "'.$_POST['Description'].'", "'.$row['OvertimePreApp_Authorised'].'", "", "'.GetDatabaseDateTime($_POST['StartHour'], $_POST['StartMinute'], $_POST['StartDay'], $_POST['StartMonth'], $_POST['StartYear']).'", "'.GetDatabaseDateTime($_POST['EndHour'], $_POST['EndMinute'], $_POST['EndDay'], $_POST['EndMonth'], $_POST['EndYear']).'", "0", "", "'.$row['OvertimePreApp_Billable'].'", "'.$_SESSION['AddOvertime'][0].'", "1")';

                if (ExecuteQuery($query))
                {
                    $hours = $row['OvertimePreApp_OvertimeHoursUsed'] + $_POST['Hours'];
                    if ($hours >= $row['OvertimePreApp_Hours'])
                        $used = '1';
                    else
                        $used = '0';

                    if ($row['OvertimePreApp_Multi'] == '1')
                        $query = 'UPDATE OvertimePreApp SET OvertimePreApp_OvertimeUsed = "'.$used.'", OvertimePreApp_OvertimeHoursUsed = "'.$hours.'" WHERE OvertimePreApp_ID = "'.$_SESSION['AddOvertime'][0].'"';
                    else
                        $query = 'UPDATE OvertimePreApp SET OvertimePreApp_OvertimeUsed = "1", OvertimePreApp_OvertimeHoursUsed = "'.$hours.'" WHERE OvertimePreApp_ID = "'.$_SESSION['AddOvertime'][0].'"';

                    if (ExecuteQuery($query))
                    {
                        $hoursRemaining = $row['OvertimeAvail'] - $_POST['Hours'];

                        $project = $row['Project_Description'];
                        $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = '.$row['OvertimePreApp_Authorised'].''));
                        $row = MySQL_Fetch_Array(ExecuteQuery('SELECT Staff_First_Name, Staff_Last_Name FROM Staff WHERE Staff_Code = "'.$row['OvertimePreApp_Name'].'"'));

                        $email = 'Overtime which you have authorised has been used. The details of the overtime are as follows:'.Chr(10).
                            'NAME:                   '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].Chr(10).
                            'PROJECT:                '.$project.Chr(10).
                            'PERIOD:                 '.GetTextualDateTime($_POST['StartHour'], $_POST['StartMinute'], $_POST['StartDay'], $_POST['StartMonth'], $_POST['StartYear']).' until '.GetTextualDateTime($_POST['EndHour'], $_POST['EndMinute'], $_POST['EndDay'], $_POST['EndMonth'], $_POST['EndYear']).Chr(10).
                            'HOURS:                  '.$_POST['Hours'].Chr(10).
                            'APPROVAL ID:            '.$_SESSION['AddOvertime'][0].Chr(10);

                        $html = 'Overtime which you have authorised has been used. The details of the overtime are as follows:
                      <BR /><BR />
                      <TABLE border=0>
                        <TR><TD><B>Name:</B></TD><TD>'.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].'</TD></TR>
                        <TR><TD><B>Project:</B></TD><TD>'.$project.'</TD></TR>
                        <TR><TD><B>Period:</B></TD><TD>'.GetTextualDateTime($_POST['StartHour'], $_POST['StartMinute'], $_POST['StartDay'], $_POST['StartMonth'], $_POST['StartYear']).' until '.GetTextualDateTime($_POST['EndHour'], $_POST['EndMinute'], $_POST['EndDay'], $_POST['EndMonth'], $_POST['EndYear']).'</TD></TR>
                        <TR><TD><B>Hours:</B></TD><TD>'.$_POST['Hours'].'</TD></TR>
                        <TR><TD><B>Approval ID:</B></TD><TD>'.$_SESSION['AddOvertime'][0].'</TD></TR>';

                        if ($hoursRemaining < 0)
                        {
                            $email .= 'EXCESS HOURS USED:      '.(-$hoursRemaining);
                            $html .= '<TR><TD><B>Excess Hours Used:</B></TD><TD>'.(-$hoursRemaining).'</TD></TR>';
                        }

                        $html .= '</TABLE>';

                        SendMailHTML($rowTemp['Staff_email'], 'Overtime Recorded', $email, $html);
                        $_SESSION['OvertimeSuccess'] = 'geh!';
                        Session_Unregister('AddOvertime');
                    } else
                        $_SESSION['OvertimeFail'] = 'geh!';
                } else
                    $_SESSION['OvertimeFail'] = 'geh!';
            } else
                $_SESSION['OvertimeIncomplete'] = 'geh!';
            break;
        default:
            break;
    }
}

//////////////////////////////////////////////////////////////////////////////
// Handles the user's submission of information for approving banked        //
// overtime use.                                                            //
//////////////////////////////////////////////////////////////////////////////
function HandleApproveBankedOvertime()
{
    $_SESSION['ApproveOvertimeBanked'][1] = $_POST['Reason'];

    switch ($_POST['Submit'])
    {
        case 'Approve':
            if (ExecuteQuery('UPDATE OvertimeBank SET OvertimeBank_IsApproved = "1" WHERE OvertimeBank_ID = "'.$_SESSION['ApproveOvertimeBanked'][0].'"'))
            {
                $row = MySQL_Fetch_Array(ExecuteQuery('SELECT OvertimeBank.* FROM OvertimeBank WHERE OvertimeBank_ID = "'.$_SESSION['ApproveOvertimeBanked'][0].'"'));
                $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT Staff_First_Name, Staff_Last_Name, Staff_email FROM Staff WHERE Staff_Code = '.$row['OvertimeBank_Name'].''));

                $email = 'The banked overtime use you requested has been approved. The details of the approved banked overtime use are as follows:'.Chr(10).
                    'NAME:                  '.$rowTemp['Staff_First_Name'].' '.$rowTemp['Staff_Last_Name'].Chr(10).
                    'PERIOD:                '.GetTextualDateTimeFromDatabaseDateTime($row['OvertimeBank_Start']).' until '.GetTextualDateTimeFromDatabaseDateTime($row['OvertimeBank_End']).Chr(10).
                    'HOURS:                 '.SPrintF('%02.2f', -$row['OvertimeBank_Hours']).Chr(10).
                    'DESCRIPTION:           '.$row['OvertimeBank_Description'];

                $html = 'The banked overtime use you requested has been approved. The details of the approved banked overtime use are as follows:
                  <BR /><BR />
                  <TABLE border=0>
                    <TR><TD><B>Name:</B></TD><TD>'.$rowTemp['Staff_First_Name'].' '.$rowTemp['Staff_Last_Name'].'</TD></TR>
                    <TR><TD><B>Period:</B></TD><TD>'.GetTextualDateTimeFromDatabaseDateTime($row['OvertimeBank_Start']).' until '.GetTextualDateTimeFromDatabaseDateTime($row['OvertimeBank_End']).'</TD></TR>
                    <TR><TD><B>Hours:</B></TD><TD>'.SPrintF('%02.2f', -$row['OvertimeBank_Hours']).'</TD></TR>
                    <TR><TD><B>Description:</B></TD><TD>'.$row['OvertimeBank_Description'].'</TD></TR>
                  </TABLE>';

                SendMailHTML($rowTemp['Staff_email'], 'Banked Overtime Use Approved', $email, $html);
                $_SESSION['OvertimeSuccess'] = 'geh!';
                Session_Unregister('ApproveOvertimeBanked');
            } else
                $_SESSION['OvertimeFail'] = 'geh!';
            break;
        case 'Cancel':
            Session_Unregister('ApproveOvertimeBanked');
            break;
        case 'Deny':
            if (ExecuteQuery('UPDATE OvertimeBank SET OvertimeBank_IsApproved = "2" WHERE OvertimeBank_ID = "'.$_SESSION['ApproveOvertimeBanked'][0].'"'))
            {
                $row = MySQL_Fetch_Array(ExecuteQuery('SELECT OvertimeBank.* FROM OvertimeBank WHERE OvertimeBank_ID = "'.$_SESSION['ApproveOvertimeBanked'][0].'"'));
                $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT Staff_First_Name, Staff_Last_Name, Staff_email FROM Staff WHERE Staff_Code = '.$row['OvertimeBank_Name'].''));

                $email = 'The banked overtime use you requested has been denied. The details of the denied banked overtime use are as follows:'.Chr(10).
                    'NAME:                  '.$rowTemp['Staff_First_Name'].' '.$rowTemp['Staff_Last_Name'].Chr(10).
                    'PERIOD:                '.GetTextualDateTimeFromDatabaseDateTime($row['OvertimeBank_Start']).' until '.GetTextualDateTimeFromDatabaseDateTime($row['OvertimeBank_End']).Chr(10).
                    'HOURS:                 '.SPrintF('%02.2f', -$row['OvertimeBank_Hours']).Chr(10).
                    'DESCRIPTION:           '.$row['OvertimeBank_Description'].Chr(10).
                    'REASON FOR DENIAL:     '.($_POST['Reason'] ? $_POST['Reason'] : 'Not specified');;

                $html = 'The banked overtime use you requested has been denied. The details of the denied banked overtime use are as follows:
                  <BR /><BR />
                  <TABLE border=0>
                    <TR><TD><B>Name:</B></TD><TD>'.$rowTemp['Staff_First_Name'].' '.$rowTemp['Staff_Last_Name'].'</TD></TR>
                    <TR><TD><B>Period:</B></TD><TD>'.GetTextualDateTimeFromDatabaseDateTime($row['OvertimeBank_Start']).' until '.GetTextualDateTimeFromDatabaseDateTime($row['OvertimeBank_End']).'</TD></TR>
                    <TR><TD><B>Hours:</B></TD><TD>'.SPrintF('%02.2f', -$row['OvertimeBank_Hours']).'</TD></TR>
                    <TR><TD><B>Description:</B></TD><TD>'.$row['OvertimeBank_Description'].'</TD></TR>
                    <TR><TD><B>Reason for Denial:</B></TD><TD>'.($_POST['Reason'] ? $_POST['Reason'] : 'Not specified').'</TD></TR>
                  </TABLE>';

                SendMailHTML($rowTemp['Staff_email'], 'Banked Overtime Use Denied', $email, $html);
                $_SESSION['OvertimeSuccess'] = 'geh!';
                Session_Unregister('ApproveOvertimeBanked');
            } else
                $_SESSION['OvertimeFail'] = 'geh!';
            break;
        default:
            break;
    }
}

//////////////////////////////////////////////////////////////////////////////
// Handles the user's submission of information for approving overtime.     //
//////////////////////////////////////////////////////////////////////////////
function HandleApproveNormalOvertime()
{
    $_SESSION['ApproveOvertimeNormal'][1] = $_POST['Reason'];

    switch ($_POST['Submit'])
    {
        case 'Approve':
            $date = Date('Y-m-d H:i:s');
            if (ExecuteQuery('UPDATE OvertimePreApp SET OvertimePreApp_IsApproved = "1", `OvertimePreApp_ApprovedDate` = "'.$date.'" WHERE OvertimePreApp_ID = "'.$_SESSION['ApproveOvertimeNormal'][0].'"'))
            {
                $row = MySQL_Fetch_Array(ExecuteQuery('SELECT OvertimePreApp.* FROM OvertimePreApp WHERE OvertimePreApp_ID = "'.$_SESSION['ApproveOvertimeNormal'][0].'"'));
                $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT Staff_First_Name, Staff_Last_Name, Staff_email FROM Staff WHERE Staff_Code = '.$row['OvertimePreApp_Name'].''));
                $rowTempB = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Project WHERE Project_Code = "'.$row['OvertimePreApp_Project'].'"'));

                $email = 'The overtime you requested has been approved. The details of the approved overtime are as follows:'.Chr(10).
                    'NAME:                   '.$rowTemp['Staff_First_Name'].' '.$rowTemp['Staff_Last_Name'].Chr(10).
                    'PERIOD:                 '.GetTextualDateTimeFromDatabaseDateTime($row['OvertimePreApp_Start']).' until '.GetTextualDateTimeFromDatabaseDateTime($row['OvertimePreApp_End']).Chr(10).
                    'PROJECT:                '.$rowTempB['Project_Description'].Chr(10).
                    'HOURS:                  '.SPrintF('%02.2f', $row['OvertimePreApp_Hours']).Chr(10).
                    'DESCRIPTION:            '.$row['OvertimePreApp_Description'].Chr(10).
                    'MULTIPLE ENTRIES:       '.($_POST['Multi'] ? 'Yes' : 'No');

                $html = 'The overtime you requested has been approved. The details of the approved overtime are as follows:
                  <BR /><BR />
                  <TABLE border=0>
                    <TR><TD><B>Name:</B></TD><TD>'.$rowTemp['Staff_First_Name'].' '.$rowTemp['Staff_Last_Name'].'</TD></TR>
                    <TR><TD><B>Period:</B></TD><TD>'.GetTextualDateTimeFromDatabaseDateTime($row['OvertimePreApp_Start']).' until '.GetTextualDateTimeFromDatabaseDateTime($row['OvertimePreApp_End']).'</TD></TR>
                    <TR><TD><B>Project:</B></TD><TD>'.$rowTempB['Project_Description'].'</TD></TR>
                    <TR><TD><B>Hours:</B></TD><TD>'.SPrintF('%02.2f', $row['OvertimePreApp_Hours']).'</TD></TR>
                    <TR><TD><B>Description:</B></TD><TD>'.$row['OvertimePreApp_Description'].'</TD></TR>
                    <TR><TD><B>Multiple Entries:</B></TD><TD>'.($_POST['Multi'] ? 'Yes' : 'No').'</TD></TR>
                  </TABLE>';

                SendMailHTML($rowTemp['Staff_email'], 'Overtime Approved', $email, $html);
                if(AddOvertime($_SESSION['ApproveOvertimeNormal'][0])){
                    $_SESSION['OvertimeSuccess'] = 'geh!';
                    Session_Unregister('ApproveOvertimeNormal');
                }
                else{
                    $_SESSION['OvertimeFail'] = 'geh!';
                }



            } else
                $_SESSION['OvertimeFail'] = 'geh!';
            break;
        case 'Cancel':
            Session_Unregister('ApproveOvertimeNormal');
            break;
        case 'Deny':
            $date = Date('Y-m-d H:i:s');
            if (ExecuteQuery('UPDATE OvertimePreApp SET OvertimePreApp_IsApproved = "2" ,`OvertimePreApp_ApprovedDate` = "'.$date.'" WHERE OvertimePreApp_ID = "'.$_SESSION['ApproveOvertimeNormal'][0].'"'))
            {
                $row = MySQL_Fetch_Array(ExecuteQuery('SELECT OvertimePreApp.* FROM OvertimePreApp WHERE OvertimePreApp_ID = "'.$_SESSION['ApproveOvertimeNormal'][0].'"'));
                $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT Staff_First_Name, Staff_Last_Name, Staff_email FROM Staff WHERE Staff_Code = '.$row['OvertimePreApp_Name'].''));
                $rowTempB = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Project WHERE Project_Code = "'.$row['OvertimePreApp_Project'].'"'));

                $email = 'The overtime you requested has been denied. The details of the denied overtime are as follows:'.Chr(10).
                    'NAME:                   '.$rowTemp['Staff_First_Name'].' '.$rowTemp['Staff_Last_Name'].Chr(10).
                    'PERIOD:                 '.GetTextualDateTimeFromDatabaseDateTime($row['OvertimePreApp_Start']).' until '.GetTextualDateTimeFromDatabaseDateTime($row['OvertimePreApp_End']).Chr(10).
                    'PROJECT:                '.$rowTempB['Project_Description'].Chr(10).
                    'HOURS:                  '.SPrintF('%02.2f', $row['OvertimePreApp_Hours']).Chr(10).
                    'DESCRIPTION:            '.$row['OvertimePreApp_Description'].Chr(10).
                    'MULTIPLE ENTRIES:       '.($_POST['Multi'] ? 'Yes' : 'No').Chr(10).
                    'REASON FOR DENIAL:      '.($_POST['Reason'] ? $_POST['Reason'] : 'Not specified');

                $html = 'The overtime you requested has been denied. The details of the denied overtime are as follows:
                  <BR /><BR />
                  <TABLE border=0>
                    <TR><TD><B>Name:</B></TD><TD>'.$rowTemp['Staff_First_Name'].' '.$rowTemp['Staff_Last_Name'].'</TD></TR>
                    <TR><TD><B>Period:</B></TD><TD>'.GetTextualDateTimeFromDatabaseDateTime($row['OvertimePreApp_Start']).' until '.GetTextualDateTimeFromDatabaseDateTime($row['OvertimePreApp_End']).'</TD></TR>
                    <TR><TD><B>Project:</B></TD><TD>'.$rowTempB['Project_Description'].'</TD></TR>
                    <TR><TD><B>Hours:</B></TD><TD>'.SPrintF('%02.2f', $row['OvertimePreApp_Hours']).'</TD></TR>
                    <TR><TD><B>Description:</B></TD><TD>'.$row['OvertimePreApp_Description'].'</TD></TR>
                    <TR><TD><B>Multiple Entries:</B></TD><TD>'.($_POST['Multi'] ? 'Yes' : 'No').'</TD></TR>
                    <TR><TD><B>Reason for Denial:</B></TD><TD>'.($_POST['Reason'] ? $_POST['Reason'] : 'Not specified').'</TD></TR>
                  </TABLE>';

                SendMailHTML($rowTemp['Staff_email'], 'Overtime Denied', $email, $html);
                $_SESSION['OvertimeSuccess'] = 'geh!';
                Session_Unregister('ApproveOvertimeNormal');
            } else
                $_SESSION['OvertimeFail'] = 'geh!';
            break;
        default:
            break;
    }
}

function HandleApproveNormalOvertimeMultiple(){
    $_SESSION['HandleApproveNormalOvertimeMultiple'] = $_POST['ApproveOvertimeMulty'];
}
//////////////////////////////////////////////////////////////////////////////
// Handles the user's submission of information for recording overtime.     //
//////////////////////////////////////////////////////////////////////////////
function HandleEdit()
{
    $_SESSION['EditOvertime'][1] = $_POST['StartYear'].$_POST['StartMonth'].$_POST['StartDay'].$_POST['StartHour'].$_POST['StartMinute'];
    $_SESSION['EditOvertime'][2] = $_POST['Description'];
    $_SESSION['EditOvertime'][3] = $_POST['EndYear'].$_POST['EndMonth'].$_POST['EndDay'].$_POST['EndHour'].$_POST['EndMinute'];
    $_SESSION['EditOvertime'][4] = $_POST['Hours'];

    switch ($_POST['Submit'])
    {
        case 'Cancel':
            Session_Unregister('EditOvertime');
            break;
        case 'Submit':
            if (CheckFields())
            {
                $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM OvertimeBank WHERE OvertimeBank_ID = '.$_SESSION['EditOvertime'][0].''));
                $row = MySQL_Fetch_Array(ExecuteQuery('SELECT OvertimePreApp.*, (OvertimePreApp_Hours - OvertimePreApp_OvertimeHoursUsed + '.$rowTemp['OvertimeBank_Hours'].') AS OvertimeAvail, Project_Description FROM OvertimePreApp, Project WHERE OvertimePreApp_ID = "'.$rowTemp['OvertimeBank_PreApproved'].'" AND OvertimePreApp_Project = Project_Code'));
                if (ExecuteQuery('UPDATE OvertimeBank SET OvertimeBank_Date_Log = "'.Date('Y-m-d H:i:s').'", OvertimeBank_Hours = "'.$_POST['Hours'].'", OvertimeBank_Start = "'.GetDatabaseDateTime($_POST['StartHour'], $_POST['StartMinute'], $_POST['StartDay'], $_POST['StartMonth'], $_POST['StartYear']).'", OvertimeBank_End = "'.GetDatabaseDateTime($_POST['EndHour'], $_POST['EndMinute'], $_POST['EndDay'], $_POST['EndMonth'], $_POST['EndYear']).'", OvertimeBank_Description = "'.$_POST['Description'].'" WHERE OvertimeBank_ID = "'.$_SESSION['EditOvertime'][0].'"'))
                {
                    $hours = $row['OvertimePreApp_OvertimeHoursUsed'] + $_POST['Hours'] - $rowTemp['OvertimeBank_Hours'];
                    if ($hours >= $row['OvertimePreApp_Hours'])
                        $used = '1';
                    else
                        $used = '0';

                    if ($row['OvertimePreApp_Multi'] == '1')
                        $query = 'UPDATE OvertimePreApp SET OvertimePreApp_OvertimeUsed = "'.$used.'", OvertimePreApp_OvertimeHoursUsed = "'.$hours.'" WHERE OvertimePreApp_ID = "'.$rowTemp['OvertimeBank_PreApproved'].'"';
                    else
                        $query = 'UPDATE OvertimePreApp SET OvertimePreApp_OvertimeUsed = "1", OvertimePreApp_OvertimeHoursUsed = "'.$hours.'" WHERE OvertimePreApp_ID = "'.$rowTemp['OvertimeBank_PreApproved'].'"';

                    if (ExecuteQuery($query))
                    {
                        $hoursRemaining = $row['OvertimeAvail'] - $_POST['Hours'];

                        $project = $row['Project_Description'];
                        $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = "'.$row['OvertimePreApp_Authorised'].'"'));
                        $row = MySQL_Fetch_Array(ExecuteQuery('SELECT Staff_First_Name, Staff_Last_Name, Staff_email FROM Staff WHERE Staff_Code = "'.$row['OvertimePreApp_Name'].'"'));

                        $email = 'Overtime recorded has been changed. The new details of the overtime are as follows:'.Chr(10).
                            'NAME:                   '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].Chr(10).
                            'PROJECT:                '.$project.Chr(10).
                            'PERIOD:                 '.GetTextualDateTime($_POST['StartHour'], $_POST['StartMinute'], $_POST['StartDay'], $_POST['StartMonth'], $_POST['StartYear']).' until '.GetTextualDateTime($_POST['EndHour'], $_POST['EndMinute'], $_POST['EndDay'], $_POST['EndMonth'], $_POST['EndYear']).Chr(10).
                            'HOURS:                  '.$_POST['Hours'].Chr(10).
                            'APPROVAL ID:            '.$_POST['Overtime'].Chr(10);

                        $html = 'Overtime recorded has been changed. The new details of the overtime are as follows:
                      <BR /><BR />
                      <TABLE border=0>
                        <TR><TD><B>Name:</B></TD><TD>'.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].'</TD></TR>
                        <TR><TD><B>Project:</B></TD><TD>'.$project.'</TD></TR>
                        <TR><TD><B>Period:</B></TD><TD>'.GetTextualDateTime($_POST['StartHour'], $_POST['StartMinute'], $_POST['StartDay'], $_POST['StartMonth'], $_POST['StartYear']).' until '.GetTextualDateTime($_POST['EndHour'], $_POST['EndMinute'], $_POST['EndDay'], $_POST['EndMonth'], $_POST['EndYear']).'</TD></TR>
                        <TR><TD><B>Hours:</B></TD><TD>'.$_POST['Hours'].'</TD></TR>
                        <TR><TD><B>Approval ID:</B></TD><TD>'.$_POST['Overtime'].'</TD></TR>';

                        if ($hoursRemaining < 0)
                        {
                            $email .= 'EXCESS HOURS USED:      '.(-$hoursRemaining);
                            $html .= '<TR><TD><B>Excess Hours Used:</B></TD><TD>'.(-$hoursRemaining).'</TD></TR>';
                        }

                        $html .= '</TABLE>';

                        SendMailHTML($rowTemp['Staff_email'], 'Overtime Recorded Updated', $email, $html);
                        $_SESSION['OvertimeSuccess'] = 'geh!';
                        Session_Unregister('EditOvertime');
                    } else
                        $_SESSION['OvertimeFail'] = 'geh!';
                } else
                    $_SESSION['OvertimeFail'] = 'geh!';
            } else
                $_SESSION['OvertimeIncomplete'] = 'geh!';
            break;
        default:
            break;
    }
}

//////////////////////////////////////////////////////////////////////////////
// Handles the user's submission of information for transferring excess     //
// overtime.                                                                //
//////////////////////////////////////////////////////////////////////////////
function HandleExcess()
{
    switch ($_POST['Submit'])
    {
        case 'No':
            Session_Unregister('ExcessOvertime');
            break;
        case 'Yes':
            $success = true;
            $resultSet = ExecuteQuery('SELECT * FROM Staff WHERE Staff_IsEmployee > 0');
            $dateTime = Date('Y-m-d H:i:s');
            while ($row = MySQL_Fetch_Array($resultSet))
            {
                $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT SUM(OvertimeBank_Hours) AS Available FROM OvertimeBank WHERE OvertimeBank_Name = '.$row['Staff_Code'].' AND OvertimeBank_IsApproved = "1"'));
                switch ($row['Staff_Payout_Overtime'])
                {
                    case 0: //Capped.
                        if ($rowTemp['Available'] > 100)
                        {
                            $success = $success &&
                                ExecuteQuery('INSERT INTO Overtime VALUES("'.$row['Staff_Code'].'", "'.$dateTime.'", "'.($rowTemp['Available'] - 100).'", "215", "S4 Intranet - CapTransfer", "", "", "'.Date('Y-m-d').' 00:00:00", "'.Date('Y-m-d').' 00:00:00", "", "", "0", "", "")') &&
                                ExecuteQuery('INSERT INTO OvertimeBank VALUES("'.$row['Staff_Code'].'", "'.$dateTime.'", "'.(100 - $rowTemp['Available']).'", "215", "S4 Intranet - CapTransfer", "", "", "'.Date('Y-m-d').' 00:00:00", "'.Date('Y-m-d').' 00:00:00", "1", "", "", "", "1")');
                        }
                        break;
                    case 1: //All.
                        if ($rowTemp['Available'] > 0)
                        {
                            $success = $success &&
                                ExecuteQuery('INSERT INTO Overtime VALUES("'.$row['Staff_Code'].'", "'.$dateTime.'", "'.($rowTemp['Available']).'", "215", "S4 Intranet - CapTransfer", "", "", "'.Date('Y-m-d').' 00:00:00", "'.Date('Y-m-d').' 00:00:00", "", "", "0", "", "")') &&
                                ExecuteQuery('INSERT INTO OvertimeBank VALUES("'.$row['Staff_Code'].'", "'.$dateTime.'", "'.(-$rowTemp['Available']).'", "215", "S4 Intranet - CapTransfer", "", "", "'.Date('Y-m-d').' 00:00:00", "'.Date('Y-m-d').' 00:00:00", "1", "", "", "", "1")');
                        }
                        break;
                    case 2: //Payout nothing.
                        break;
                    default:
                        break;
                }
            }
            if ($success)
            {
                $_SESSION['OvertimeSuccess'] = 'geh!';
                Session_Unregister('ExcessOvertime');
            } else
                $_SESSION['OvertimeFail'] = 'geh!';
            break;
        default:
            break;
    }
}

//////////////////////////////////////////////////////////////////////////////
// Handles the user's maintenance selection for banked overtime.            //
//////////////////////////////////////////////////////////////////////////////
function HandleMaintainBankedOvertime()
{
    switch ($_POST['Submit'])
    {
        case 'Approve':
            if ($_POST['ApproveOvertime'] == "")
                $_SESSION['OvertimeIncomplete'] = 'geh!';
            else
                $_SESSION['ApproveOvertimeBanked'] = array($_POST['ApproveOvertime']);
            break;
        case 'Excess':
            $_SESSION['ExcessOvertime'] = array();
            break;
        case 'Request':
            if($_SESSION['cAuth']&64){

                $_SESSION['RequestOvertimeBanked'] = array();
                $_SESSION['RequestOvertimeBanked'][100] = $_POST['RequestOvertimeBankedOvertime'];

            }else{
                $_SESSION['RequestOvertimeBanked'] = array();
                $_SESSION['RequestOvertimeBanked'][100] = $_SESSION['cUID'];

            }

            break;
        case 'Transfer':
            if ($_POST['TransferStaff'] == "")
                $_SESSION['OvertimeIncomplete'] = 'geh!';
            else
                $_SESSION['TransferOvertime'] = array($_POST['TransferStaff']);
            break;
        default:
            break;
    }
}

//////////////////////////////////////////////////////////////////////////////
// Handles the user's maintenance selection for normal overtime.            //
//////////////////////////////////////////////////////////////////////////////
function HandleMaintainNormalOvertime()
{
//    if($_POST['Type'] !== ""){    //ApproveOvertimeMulty
//
//        print_r('here1');
//       $_SESSION['ApproveOvertimeMulty'] = $_POST['ApproveOvertimeMulty'];
//    }



//    else{

    switch ($_POST['Submit'])
    {
        case 'Add':
            if ($_POST['AddOvertime'] == "")
                $_SESSION['OvertimeIncomplete'] = 'geh!';
            else
            {
                $row = MySQL_Fetch_Array(ExecuteQuery('SELECT OvertimePreApp.*, IF (OvertimePreApp_Bank = "1", "Yes", "No") AS Banked FROM OvertimePreApp WHERE OvertimePreApp_ID = "'.$_POST['AddOvertime'].'"'));
                $_SESSION['AddOvertime'] = array($_POST['AddOvertime']);
                $_SESSION['AddOvertime'][1] = GetSessionDateTimeFromDatabaseDateTime($row['OvertimePreApp_Start']);
                $_SESSION['AddOvertime'][2] = $row['OvertimePreApp_Description'];
                $_SESSION['AddOvertime'][3] = GetSessionDateTimeFromDatabaseDateTime($row['OvertimePreApp_End']);
                $_SESSION['AddOvertime'][4] = $row['OvertimePreApp_Hours'];
                if ($row['Banked'])
                    $_SESSION['AddOvertime'][5] = 'Yes';
                else
                    $_SESSION['AddOvertime'][5] = 'No';
            }
            break;
        case 'Approve':
            if ($_POST['ApproveOvertime'] == "")
                $_SESSION['OvertimeIncomplete'] = 'geh!';
            else
                $_SESSION['ApproveOvertimeNormal'] = array($_POST['ApproveOvertime']);

            break;

        case 'Approve Multiple':
            $_SESSION['ApproveOvertimeMulty'] = $_POST['ApproveOvertimeMulty'];
            break;

        case 'Edit':
            if ($_POST['EditOvertime'] == "")
                $_SESSION['OvertimeIncomplete'] = 'geh!';
            else
            {
                $row = MySQL_Fetch_Array(ExecuteQuery('SELECT OvertimeBank.* FROM OvertimeBank WHERE OvertimeBank_ID = "'.$_POST['EditOvertime'].'"'));
                $_SESSION['EditOvertime'] = array($_POST['EditOvertime']);
                $_SESSION['EditOvertime'][1] = GetSessionDateTimeFromDatabaseDateTime($row['OvertimeBank_Start']);
                $_SESSION['EditOvertime'][2] = $row['OvertimeBank_Description'];
                $_SESSION['EditOvertime'][3] = GetSessionDateTimeFromDatabaseDateTime($row['OvertimeBank_End']);
                $_SESSION['EditOvertime'][4] = $row['OvertimeBank_Hours'];
            }
            break;
        case 'Request':


            if ($_SESSION['cAuth'] & 64)
            {
                if ($_POST['RequestOvertimeNormal'] == "")
                    $_SESSION['OvertimeIncomplete'] = 'geh!';
                else
                {
                    $_SESSION['RequestOvertimeNormal'] = array();
                    $_SESSION['RequestOvertimeNormal'][100] = $_POST['RequestOvertimeNormal'];
                }
            } else
            {
                $_SESSION['RequestOvertimeNormal'] = array();
                $_SESSION['RequestOvertimeNormal'][100] = $_SESSION['cUID'];
            }
            break;
        default:
            break;
    }
    //  }
}

//////////////////////////////////////////////////////////////////////////////
// Handles the user's submission of information for requesting banked       //
// overtime use.                                                            //
//////////////////////////////////////////////////////////////////////////////
function HandleRequestBankedOvertime()
{
    $_SESSION['RequestOvertimeBanked'][0] = $_POST['StartYear'].$_POST['StartMonth'].$_POST['StartDay'].$_POST['StartHour'].$_POST['StartMinute'];
    $_SESSION['RequestOvertimeBanked'][1] = $_POST['Description'];
    $_SESSION['RequestOvertimeBanked'][2] = $_POST['EndYear'].$_POST['EndMonth'].$_POST['EndDay'].$_POST['EndHour'].$_POST['EndMinute'];
    $_SESSION['RequestOvertimeBanked'][3] = $_POST['Hours'];
    $_SESSION['RequestOvertimeBanked'][4] = $_POST['Authorise'];

    switch ($_POST['Submit'])
    {
        case 'Cancel':
            Session_Unregister('RequestOvertimeBanked');
            break;
        case 'Submit':
            if (CheckFields())
            {
                $date = Date('Y-m-d H:i:s');
                if (ExecuteQuery('INSERT INTO OvertimeBank VALUES("'.$_SESSION['RequestOvertimeBanked'][100].'", "'.$date.'", "'.(-$_POST['Hours']).'", "215", "'.$_POST['Description'].'", "'.$_POST['Authorise'].'", "", "'.GetDatabaseDateTime($_POST['StartHour'], $_POST['StartMinute'], $_POST['StartDay'], $_POST['StartMonth'], $_POST['StartYear']).'", "'.GetDatabaseDateTime($_POST['EndHour'], $_POST['EndMinute'], $_POST['EndDay'], $_POST['EndMonth'], $_POST['EndYear']).'", "3", "", "", "", "0")'))
                {
                    $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT Staff_First_Name, Staff_Last_Name, Staff_email FROM Staff WHERE Staff_Code = "'.$_SESSION['RequestOvertimeBanked'][100].'"'));
                    $rowTempC = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM OvertimeBank WHERE OvertimeBank_Name = "'.$_SESSION['cUID'].'" AND OvertimeBank_Date_Log = "'.$date.'" AND OvertimeBank_IsApproved = "0" ORDER BY OvertimeBank_ID DESC'));

                    $email = 'Banked overtime use has been requested which requires your authorisation. It can be authorised using the intranet. The details of the banked overtime use are as follows:'.Chr(10).Chr(10).
                        'NAME:                   '.$rowTemp['Staff_First_Name'].' '.$rowTemp['Staff_Last_Name'].Chr(10).
                        'PERIOD:                 '.GetTextualDateTimeFromSessionDateTime($_SESSION['RequestOvertimeBanked'][0]).' until '.GetTextualDateTimeFromSessionDateTime($_SESSION['RequestOvertimeBanked'][2]).Chr(10).
                        'HOURS:                  '.SPrintF('%02.2f', $_POST['Hours']).Chr(10).
                        'DESCRIPTION:            '.$_POST['Description'];

                    $html = 'Banked overtime use has been requested which requires your authorisation. It can be authorised using the intranet. The details of the banked overtime use are as follows:
                    <BR /><BR />
                    <TABLE border=0>
                      <TR><TD><B>Name:</B></TD><TD>'.$rowTemp['Staff_First_Name'].' '.$rowTemp['Staff_Last_Name'].'</TD></TR>
                      <TR><TD><B>Period:</B></TD><TD>'.GetTextualDateTimeFromSessionDateTime($_SESSION['RequestOvertimeBanked'][0]).' until '.GetTextualDateTimeFromSessionDateTime($_SESSION['RequestOvertimeBanked'][2]).'</TD></TR>
                      <TR><TD><B>Hours:</B></TD><TD>'.SPrintF('%02.2f', $_POST['Hours']).'</TD></TR>
                      <TR><TD><B>Description:</B></TD><TD>'.$_POST['Description'].'</TD></TR>
                    </TABLE>';

                    $rand = Rand();
                    while (StrLen($rand) != 5)
                    {
                        if (StrLen($rand) > 5)
                            $rand = SubStr($rand, 0, 5);
                        else
                            $rand = Rand();
                    }
                    $html .= '<BR /><BR /><BR />Approve this request <A href="'.getS4ExternalLink().'/intranew/Overtime.php?type=approve&id='.$rand.'0'.$rowTempC['OvertimeBank_ID'].'">here</A>.';
                    $html .= '<BR />Deny this request <A href="'.getS4ExternalLink().'/intranew/Overtime.php?type=deny&id='.$rand.'0'.$rowTempC['OvertimeBank_ID'].'">here</A>.';

                    $email .= Chr(10).Chr(10).Chr(10).'Approve this request here '.getS4ExternalLink().'/intranew/Overtime.php?type=approve&id='.$rand.'0'.$rowTempC['OvertimeBank_ID'].' here.';
                    $email .= Chr(10).'Deny this request here '.getS4ExternalLink().'/cobotsweb/Overtime.php?type=deny&id='.$rand.'0'.$rowTempC['OvertimeBank_ID'].'here.';

                    $rowStaff = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = '.$_POST['Authorise'].''));
                    SendMailHTML($rowStaff['Staff_email'], 'Banked Overtime Use Requested', $email, $html);
                    $_SESSION['OvertimeSuccess'] = 'geh!';
                    Session_Unregister('RequestOvertimeBanked');
                } else
                    $_SESSION['OvertimeFail'] = 'geh!';
            } else
                $_SESSION['OvertimeIncomplete'] = 'geh!';
            break;
        default:
            break;
    }
}

//////////////////////////////////////////////////////////////////////////////
// Handles the user's submission of information for requesting overtime.    //
//////////////////////////////////////////////////////////////////////////////
function HandleRequestNormalOvertime()
{
    $_SESSION['RequestOvertimeNormal'][0] = $_POST['Authorise'];
    $_SESSION['RequestOvertimeNormal'][1] = $_POST['Description'];
    $_SESSION['RequestOvertimeNormal'][2] = $_POST['Project'];
    $_SESSION['RequestOvertimeNormal'][3] = $_POST['StartYear'].$_POST['StartMonth'].$_POST['StartDay'].$_POST['StartHour'].$_POST['StartMinute'];
    $_SESSION['RequestOvertimeNormal'][4] = $_POST['EndYear'].$_POST['EndMonth'].$_POST['EndDay'].$_POST['EndHour'].$_POST['EndMinute'];
    $_SESSION['RequestOvertimeNormal'][5] = $_POST['Hours'];
    $_SESSION['RequestOvertimeNormal'][6] = $_POST['Multi'];

    switch ($_POST['Submit'])
    {
        case 'Cancel':
            Session_Unregister('RequestOvertimeNormal');
            break;
        case 'Submit':
            if (CheckFields())
            {
                $date = Date('Y-m-d H:i:s');
                if (ExecuteQuery('INSERT INTO OvertimePreApp VALUES("'.$_SESSION['RequestOvertimeNormal'][100].'", "'.$date.'", "'.$_POST['Hours'].'", "'.$_POST['Project'].'", "'.$_POST['Description'].'", "'.$_POST['Authorise'].'", "", "'.GetDatabaseDateTime($_POST['StartHour'], $_POST['StartMinute'], $_POST['StartDay'], $_POST['StartMonth'], $_POST['StartYear']).'", "'.GetDatabaseDateTime($_POST['EndHour'], $_POST['EndMinute'], $_POST['EndDay'], $_POST['EndMonth'], $_POST['EndYear']).'", "", "", "0", "", "", "", "1", "'.$_POST['Multi'].'", "", "0","0000-00-00")'))
                {
                    $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT Staff_First_Name, Staff_Last_Name, Staff_email FROM Staff WHERE Staff_Code = '.$_SESSION['RequestOvertimeNormal'][100].''));
                    $rowTempB = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Project WHERE Project_Code = "'.$_POST['Project'].'"'));
                    $rowTempC = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM OvertimePreApp WHERE OvertimePreApp_Name = "'.$_SESSION['RequestOvertimeNormal'][100].'" AND OvertimePreApp_Authorised = "'.$_POST['Authorise'].'" AND OvertimePreApp_Date_Log = "'.$date.'" AND OvertimePreApp_IsApproved = "0" ORDER BY OvertimePreApp_ID DESC'));

                    $ccemail = 'Overtime has been requested. The details of the overtime are as follows:'.Chr(10).Chr(10);
                    $email = 'Overtime has been requested which requires your authorisation. It can be authorised using the intranet. The details of the overtime are as follows:'.Chr(10).Chr(10);
                    $bodyContent= 'NAME:                   '.$rowTemp['Staff_First_Name'].' '.$rowTemp['Staff_Last_Name'].Chr(10).
                        'PERIOD:                 '.GetTextualDateTimeFromSessionDateTime($_SESSION['RequestOvertimeNormal'][3]).' until '.GetTextualDateTimeFromSessionDateTime($_SESSION['RequestOvertimeNormal'][4]).Chr(10).
                        'PROJECT:                '.$rowTempB['Project_Description'].Chr(10).
                        'HOURS:                  '.SPrintF('%02.2f', $_POST['Hours']).Chr(10).
                        'DESCRIPTION:            '.$_POST['Description'].Chr(10).
                        'MULTIPLE ENTRIES:       '.($_POST['Multi'] ? 'Yes' : 'No');

                    $ccemail .= $bodyContent;
                    $email .= $bodyContent;
                    $ccHtml = 'Overtime has been requested. The details of the overtime are as follows:';
                    $html = 'Overtime has been requested which requires your authorisation. It can be authorised using the intranet. The details of the overtime are as follows:';
                    $htmlBody = '<BR /><BR />
                    <TABLE border=0>
                      <TR><TD><B>Name:</B></TD><TD>'.$rowTemp['Staff_First_Name'].' '.$rowTemp['Staff_Last_Name'].'</TD></TR>
                      <TR><TD><B>Period:</B></TD><TD>'.GetTextualDateTimeFromSessionDateTime($_SESSION['RequestOvertimeNormal'][3]).' until '.GetTextualDateTimeFromSessionDateTime($_SESSION['RequestOvertimeNormal'][4]).'</TD></TR>
                      <TR><TD><B>Project:</B></TD><TD>'.$rowTempB['Project_Description'].'</TD></TR>
                      <TR><TD><B>Hours:</B></TD><TD>'.SPrintF('%02.2f', $_POST['Hours']).'</TD></TR>
                      <TR><TD><B>Description:</B></TD><TD>'.$_POST['Description'].'</TD></TR>
                      <TR><TD><B>Multiple Entries:</B></TD><TD>'.($_POST['Multi'] ? 'Yes' : 'No').'</TD></TR>
                    </TABLE>';
                    $html .= $htmlBody;
                    $ccHtml .= $htmlBody;

                    $rand = Rand();
                    while (StrLen($rand) != 5)
                    {
                        if (StrLen($rand) > 5)
                            $rand = SubStr($rand, 0, 5);
                        else
                            $rand = Rand();
                    }
                    $html .= '<BR /><BR /><BR />Approve this request <A href="'.getS4ExternalLink().'/cobotsweb/Overtime.php?type=approve&id='.$rand.'1'.$rowTempC['OvertimePreApp_ID'].'">here</A>.';
                    $html .= '<BR />Deny this request <A href="'.getS4ExternalLink().'/cobotsweb/Overtime.php?type=deny&id='.$rand.'1'.$rowTempC['OvertimePreApp_ID'].'">here</A>.';

                    $email .= Chr(10).Chr(10).Chr(10).'Approve this request here '.getS4ExternalLink().'/cobotsweb/Overtime.php?type=approve&id='.$rand.'1'.$rowTempC['OvertimePreApp_ID'].'here.';
                    $email .= Chr(10).'Deny this request here '.getS4ExternalLink().'/cobotsweb/Overtime.php?type=deny&id='.$rand.'1'.$rowTempC['OvertimePreApp_ID'].' here.';

                    $ccEmailAddress = $rowTemp['Staff_email'];
                    $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT Staff_First_Name, Staff_Last_Name, Staff_email FROM Staff WHERE Staff_Code = '.$_POST['Authorise'].''));
                    SendMailHTML($rowTemp['Staff_email'], 'Overtime Requested', $email, $html);
                    if($ccEmailAddress != $rowTemp['Staff_email'] && $ccEmailAddress != "")
                        SendMailHTML($ccEmailAddress, 'Overtime Requested', $ccemail, $ccHtml);

                    $_SESSION['OvertimeSuccess'] = 'geh!';
                    Session_Unregister('RequestOvertimeNormal');
                } else
                    $_SESSION['OvertimeFail'] = 'geh!';
            } else
                $_SESSION['OvertimeIncomplete'] = 'geh!';
            break;
        default:
            break;
    }
}

//////////////////////////////////////////////////////////////////////////////
// Handles the user's submission of information for transferring overtime.  //
//////////////////////////////////////////////////////////////////////////////
function HandleTransfer()
{
    $_SESSION['TransferOvertime'][1] = $_POST['Hours'];
    $_SESSION['TransferOvertime'][2] = $_POST['Description'];

    switch ($_POST['Submit'])
    {
        case 'Cancel':
            Session_Unregister('TransferOvertime');
            break;
        case 'Submit':
            if (CheckFields())
            {
                $dateTime = Date('Y-m-d H:i:s');
                if (ExecuteQuery('INSERT INTO OvertimeBank VALUES("'.$_SESSION['TransferOvertime'][0].'", "'.$dateTime.'", "'.(-$_POST['Hours']).'", "215", "'.$_POST['Description'].'", "'.$_SESSION['cUID'].'", "", "'.Date('Y-m-d').' 00:00:00", "'.Date('Y-m-d').' 00:00:00", "1", "", "", "", "1")') &&
                    ExecuteQuery('INSERT INTO Overtime VALUES("'.$_SESSION['TransferOvertime'][0].'", "'.$dateTime.'", "'.$_POST['Hours'].'", "215", "'.$_POST['Description'].'", "'.$_SESSION['cUID'].'", "", "'.Date('Y-m-d').' 00:00:00", "'.Date('Y-m-d').' 00:00:00", "", "", "0", "", "")'))
                {
                    $_SESSION['OvertimeSuccess'] = 'geh!';
                    Session_Unregister('TransferOvertime');
                } else
                    $_SESSION['OvertimeFail'] = 'geh!';
            } else
                $_SESSION['OvertimeIncomplete'] = 'geh!';
            break;
        default:
            break;
    }
}

//////////////////////////////////////////////////////////////////////////////
// Handles the user's submission of an overtime selection.                  //
//////////////////////////////////////////////////////////////////////////////
function HandleView()
{
    if (CheckFields())
    {
        switch ($_POST['Type'])
        {
            case 'View':
                $_SESSION['ViewOvertime'] = array($_POST['StartYear'].$_POST['StartMonth'].$_POST['StartDay']);
                $_SESSION['ViewOvertime'][1] = $_POST['EndYear'].$_POST['EndMonth'].$_POST['EndDay'];
                $_SESSION['ViewOvertime'][2] = $_POST['Overtime'];
                if (($_SESSION['cAuth'] & 64) || ($_SESSION['cUID'] == 30)|| ($_SESSION['cUID'] == 409)) //or Stefan Buys
                    $_SESSION['ViewOvertime'][3] = $_POST['Staff'];
                else
                    $_SESSION['ViewOvertime'][3] = $_SESSION['cUID'];
                break;
            case 'ViewStatistics':
                $_SESSION['ViewOvertimeStatistics'] = array($_POST['StartYear'].$_POST['StartMonth'].'01');
                $_SESSION['ViewOvertimeStatistics'][1] = $_POST['EndYear'].$_POST['EndMonth'].'01';
                $_SESSION['ViewOvertimeStatistics'][2] = $_POST['Staff'];
                break;
            default:
                break;
        }
    } else
        $_SESSION['OvertimeIncomplete'] = 'geh!';
}

function unsetSession(){
    unset($_SESSION['ApproveOvertimeMulty']);
}

function GetDataForOvertimeApproveMulti($staffCode){
    // mayApprove 1 true 0 false 2 has been approved already
    $time = strtotime('10/16/2003');
    $timeB = strtotime('10/16/3003');
    $biggestEnd  = date('Y-m-d',$time);
    $smallestStart = date('Y-m-d',$timeB);
    if($staffCode !== ""){
        $resultSet = ExecuteQuery('SELECT SUM(e.hours) as timesheetHrs,Date(OPA.OvertimePreApp_Start),OPA.OvertimePreApp_Project,OPA.OvertimePreApp_Multi,TIMEDIFF(OPA.OvertimePreApp_End,
                OPA.OvertimePreApp_Start) AS EsitmateHours,OPA.OvertimePreApp_ID,OPA.OvertimePreApp_Authorised,OPA.OvertimePreApp_IsApproved,
        OPA.OvertimePreApp_Start,OPA.OvertimePreApp_End,OPA.OvertimePreApp_Hours,P.Project_Description,CONCAT(S.Staff_First_Name, \' \', S.Staff_Last_Name) AS personAuthorized,
        OPA.OvertimePreApp_Date_Log, OPA.OvertimePreApp_Description
    FROM OvertimePreApp OPA LEFT JOIN Staff S ON OPA.OvertimePreApp_Authorised = S.Staff_Code
            LEFT JOIN Staff SS ON OPA.OvertimePreApp_Name = S.Staff_Code
            LEFT JOIN  Project P ON OPA.OvertimePreApp_Project = P.Project_Code
            left join ts_entries e on e.user = OPA.OvertimePreApp_Name and Date(e.datetime) = Date(OPA.OvertimePreApp_Start)
     WHERE OPA.OvertimePreApp_Name = '.$staffCode.' AND OPA.OvertimePreApp_IsApproved = 0 GROUP BY OPA.OvertimePreApp_Start ORDER BY OPA.OvertimePreApp_Start');

        $overtimeToBeApproved = array();
        while ($row = MySQL_Fetch_Array($resultSet)) {

            $row["Approve"] = false;
            $row["Deny"] = false;
            $row['Duplicate'] = getEstimatedhours( $row["OvertimePreApp_Start"], $row["OvertimePreApp_End"], $row["OvertimePreApp_Hours"],$row["EsitmateHours"]);
            $row["Reason"] = "";
            $row['OvertimePreApp_Description'] = utf8_encode($row['OvertimePreApp_Description']);
            $row['OvertimePreApp_Date_Log'] = GetTextualDateTimeFromDatabaseDateTime($row['OvertimePreApp_Date_Log']);
            $row['Period'] = GetTextualDateTimeFromDatabaseDateTime($row['OvertimePreApp_Start']).' to '.GetTextualDateTimeFromDatabaseDateTime($row['OvertimePreApp_End']);
            if($_SESSION['cAuth'] & 64){
                $row['Overtime'] = 1;
            }
            else if($row['OvertimePreApp_Authorised'] == $_SESSION['cUID']  ){
                $row['Overtime'] = 1;
            }
            else{
                $row['Overtime'] = 0;
            }

            $resultSetSub = ExecuteQuery('SELECT OvertimePreApp_Project,OPA.OvertimePreApp_Multi , TIMEDIFF(OPA.OvertimePreApp_End,OPA.OvertimePreApp_Start) as EsitmateHours , OPA.OvertimePreApp_ID ,OPA.OvertimePreApp_Authorised, OPA.OvertimePreApp_IsApproved,OPA.OvertimePreApp_Start, OPA.OvertimePreApp_End, OPA.OvertimePreApp_Hours, P.Project_Description, concat(Staff_First_Name," ",Staff_Last_Name) as `Staff` , OPA.OvertimePreApp_Date_Log, OPA.OvertimePreApp_Description FROM OvertimePreApp OPA
            LEFT JOIN Staff S on OPA.OvertimePreApp_Name = S.Staff_Code
            LEFT JOIN Project P on  OPA.OvertimePreApp_Project = P.Project_Code
            WHERE 
            EXTRACT(YEAR_MONTH FROM OvertimePreApp_Start) = EXTRACT(YEAR_MONTH FROM "'.$row['OvertimePreApp_Start'].'") AND
            EXTRACT(DAY FROM OvertimePreApp_Start) = EXTRACT(DAY FROM "'.$row['OvertimePreApp_Start'].'") AND OvertimePreApp_Project = '.$row['OvertimePreApp_Project'].'  AND
            OPA.OvertimePreApp_Name != '.$staffCode.'         
            ORDER BY OPA.OvertimePreApp_Start  ');

            $overtimeSub = array();
            while ($rowsub = MySQL_Fetch_Array($resultSetSub)) {
                $rowsub['Period'] = GetTextualDateTimeFromDatabaseDateTime($rowsub['OvertimePreApp_Start']).' to '.GetTextualDateTimeFromDatabaseDateTime($rowsub['OvertimePreApp_End']);
                array_push($overtimeSub,$rowsub);
            }

            $row['OtherOvertime'] = $overtimeSub;

            array_push($overtimeToBeApproved,$row);

            if($row['OvertimePreApp_Start'] < $smallestStart ){
                $smallestStart = $row['OvertimePreApp_Start'];
            }
            if($row['OvertimePreApp_End'] > $biggestEnd ){
                $biggestEnd = $row['OvertimePreApp_End'];
            }

        }

        //get Approved overtime
        $startDate = date('Y-m-d', strtotime(GetSessionDateFromDatabaseDate($smallestStart).' - 4 week'));
        $endDate = date('Y-m-d', strtotime(GetSessionDateFromDatabaseDate($biggestEnd).' + 4 week'));
        $dateRange = GetTextualDateFromDatabaseDate($startDate).' to '.GetTextualDateFromDatabaseDate($endDate);

        $resultSet = ExecuteQuery('SELECT OPA.OvertimePreApp_Multi, TIMEDIFF(OPA.OvertimePreApp_End,OPA.OvertimePreApp_Start) as EsitmateHours , OPA.OvertimePreApp_ID, OPA.OvertimePreApp_Authorised, OPA.OvertimePreApp_IsApproved,OPA.OvertimePreApp_Start, OPA.OvertimePreApp_End, OPA.OvertimePreApp_Hours, P.Project_Description, concat(Staff_First_Name," ",Staff_Last_Name) as personAuthorized , OPA.OvertimePreApp_Date_Log, OPA.OvertimePreApp_Description FROM OvertimePreApp OPA
               LEFT JOIN Staff S on OPA.OvertimePreApp_Authorised = S.Staff_Code
               LEFT JOIN Project P on  OPA.OvertimePreApp_Project = P.Project_Code
               WHERE OPA.OvertimePreApp_Name = '.$staffCode.' AND OPA.OvertimePreApp_IsApproved = 1  AND OPA.OvertimePreApp_Start >= "'.$startDate.'" AND OPA.OvertimePreApp_End <= "'.$endDate.'" ORDER BY OPA.OvertimePreApp_Start');

        while ($row = MySQL_Fetch_Array($resultSet)) {
            $row["Approve"] = false;
            $row["Deny"] = false;
            $row['Duplicate'] = 'No hours';
            $row["Reason"] = "";
            $row['Period'] = GetTextualDateTimeFromDatabaseDateTime($row['OvertimePreApp_Start']).' to '.GetTextualDateTimeFromDatabaseDateTime($row['OvertimePreApp_End']);
            $row['Overtime'] = 2;
            array_push($overtimeToBeApproved,$row);
        }
        $result = array($overtimeToBeApproved, GetTextualDateFromDatabaseDate($smallestStart), GetTextualDateFromDatabaseDate($biggestEnd),$dateRange);
        return $result;
    }
}

function getEstimatedhours($start, $end, $OriginalH,$estimate){

    if(date('Y-m-d', strtotime($start)) != date('Y-m-d', strtotime($end))){
        $dayofweek1 = date('l', strtotime($start));
        $dayofweek2 = date('l', strtotime($end));
        $cal = 'Not the same day: '.$dayofweek1.' to '.$dayofweek2.': ' .$estimate.' (h:m:s)';
        $result = array(0,$cal,true);
        return $result;
    }
    else{
        $dayofweek = date('l', strtotime($start));
        $hourEstimate =  date('H', strtotime($estimate));  // end - start in hours   1.5  -> 1
        $minEstimate =  date('i', strtotime($estimate));    //  end - start in min     30
        $fractionmin = $minEstimate/60;                      // fraction min             0.5

        $ExpectedMin = $hourEstimate*60 + $minEstimate;


        $startMin =  date('i', strtotime($start));
        $endMin =  date('i', strtotime($end));
        $startHours =  date('H', strtotime($start));
        $endHours =  date('H', strtotime($end));

        $minRepSTART =  $startHours*60 + $startMin;
        $minRepEND =   $endHours * 60 + $endMin;

        $rate = 1;
        $dayofweek =  $dayofweek.'';                // turn into string

        $min = ($hourEstimate*60+$minEstimate);

        if(strcmp($dayofweek,'Sunday') == 0){                            //////
            $rate = 2;
            $TotalOvertime = ($min*$rate)/60;

        }
        else if(strcmp($dayofweek,'Saturday') == 0){
            $rate = 1.5;
            $TotalOvertime = ($min*$rate)/60;
        }

        else{

            $rate = 1.5;
            $hourStart = date('H', strtotime($start));
            $hourEnd = date('H', strtotime($end));

            $minBeforeWork = 0;
            $minAfterWork = 0;
            $hours = 0;

            if(strcmp($dayofweek,'Friday') == 0){
                if($minRepSTART <= 420){      //7*60                    // before work
                    if($minRepEND <= 420) {                      // not overflow
                        $minBeforeWork = $ExpectedMin;
                    }
                    else{                                         //overflow
                        $minBeforeWork = 420 - $minRepSTART;
                        if($minRepEND > 780){
                            $minAfterWork = $minRepEND - 780;
                        }
                    }
                }
                else{
                    if($minRepSTART > 780) {                 // not overflow
                        $minAfterWork = $ExpectedMin;
                    }
                    else if($minRepEND > 780){                                //overflow
                        $minAfterWork = $minRepEND - 780;
                    }
                }
            }
            else{         // day Monday - Thurday
                if($minRepSTART <= 420){      //7*60                    // before work
                    if($minRepEND <= 420) {                      // not overflow
                        $minBeforeWork = $ExpectedMin;
                    }
                    else{                                         //overflow
                        $minBeforeWork = 420 - $minRepSTART;
                        if($minRepEND > 960){
                            $minAfterWork = $minRepEND - 960;
                        }
                    }
                }
                else{
                    if($minRepSTART > 960) {                 // not overflow
                        $minAfterWork = $ExpectedMin;
                    }
                    else if($minRepEND > 960){                                //overflow
                        $minAfterWork = $minRepEND - 960;
                    }
                }
            }

            $min = $minBeforeWork + $minAfterWork;
            $TotalOvertime = ($min*$rate)/60;

        }


        $cal = $dayofweek.': '.($min/60).' X '.$rate.' = '.$TotalOvertime;   //H:i:s


        $error = true;
        if($OriginalH == $TotalOvertime){
            $error = false;
        }

        $result = array($TotalOvertime,$cal,$error);

        return $result;
    }

}

function ApproveSaveApproveOvertimeMultiple(){

    // $_GET['OvertimeId']  $_GET['status']  $_GET['Reason']  staffCode


    if($_GET['OvertimeId'] === "16254"){
        $result = array(GetDataForOvertimeApproveMulti($_GET['staffCode']),"fail");
        echo json_encode($result);
    }
    else{
        if(isset($_GET['OvertimeId']) && isset($_GET['status']) && isset($_GET['Reason']) && isset($_GET['Multi']) ){

            $status = "";

            switch ($_GET['status'])
            {
                case 'Approve':
                    $date = Date('Y-m-d H:i:s');
                    if (ExecuteQuery('UPDATE OvertimePreApp SET OvertimePreApp_IsApproved = "1", `OvertimePreApp_ApprovedDate` = "'.$date.'" WHERE OvertimePreApp_ID = "'.$_GET['OvertimeId'].'"'))
                    {
                        $row = MySQL_Fetch_Array(ExecuteQuery('SELECT OvertimePreApp.* FROM OvertimePreApp WHERE OvertimePreApp_ID = "'.$_GET['OvertimeId'].'"'));
                        $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT Staff_First_Name, Staff_Last_Name, Staff_email FROM Staff WHERE Staff_Code = '.$row['OvertimePreApp_Name'].''));
                        $rowTempB = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Project WHERE Project_Code = "'.$row['OvertimePreApp_Project'].'"'));

                        $email = 'The overtime you requested has been approved. The details of the approved overtime are as follows:'.Chr(10).
                            'NAME:                   '.$rowTemp['Staff_First_Name'].' '.$rowTemp['Staff_Last_Name'].Chr(10).
                            'PERIOD:                 '.GetTextualDateTimeFromDatabaseDateTime($row['OvertimePreApp_Start']).' until '.GetTextualDateTimeFromDatabaseDateTime($row['OvertimePreApp_End']).Chr(10).
                            'PROJECT:                '.$rowTempB['Project_Description'].Chr(10).
                            'HOURS:                  '.SPrintF('%02.2f', $row['OvertimePreApp_Hours']).Chr(10).
                            'DESCRIPTION:            '.$row['OvertimePreApp_Description'].Chr(10).
                            'MULTIPLE ENTRIES:       '.($_GET['Multi'] ? 'Yes' : 'No');

                        $html = 'The overtime you requested has been approved. The details of the approved overtime are as follows:
                             <BR /><BR />
                             <TABLE border=0>
                               <TR><TD><B>Name:</B></TD><TD>'.$rowTemp['Staff_First_Name'].' '.$rowTemp['Staff_Last_Name'].'</TD></TR>
                               <TR><TD><B>Period:</B></TD><TD>'.GetTextualDateTimeFromDatabaseDateTime($row['OvertimePreApp_Start']).' until '.GetTextualDateTimeFromDatabaseDateTime($row['OvertimePreApp_End']).'</TD></TR>
                               <TR><TD><B>Project:</B></TD><TD>'.$rowTempB['Project_Description'].'</TD></TR>
                               <TR><TD><B>Hours:</B></TD><TD>'.SPrintF('%02.2f', $row['OvertimePreApp_Hours']).'</TD></TR>
                               <TR><TD><B>Description:</B></TD><TD>'.$row['OvertimePreApp_Description'].'</TD></TR>
                               <TR><TD><B>Multiple Entries:</B></TD><TD>'.($_POST['Multi'] ? 'Yes' : 'No').'</TD></TR>
                             </TABLE>';

                        SendMailHTML($rowTemp['Staff_email'], 'Overtime Approved', $email, $html);
                        if(AddOvertime($_GET['OvertimeId']))
                            $status = "pass";
                        else
                            $status = 'fail';


                    } else{
                        //  $_SESSION['OvertimeFail'] = 'geh!';
                        $status = 'fail';
                    }
                    break;
                case 'Deny':
                    $date = Date('Y-m-d H:i:s');
                    if (ExecuteQuery('UPDATE OvertimePreApp SET OvertimePreApp_IsApproved = "2" ,`OvertimePreApp_ApprovedDate` = "'.$date.'" WHERE OvertimePreApp_ID = "'.$_GET['OvertimeId'].'"'))
                    {
                        $row = MySQL_Fetch_Array(ExecuteQuery('SELECT OvertimePreApp.* FROM OvertimePreApp WHERE OvertimePreApp_ID = "'.$_GET['OvertimeId'].'"'));
                        $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT Staff_First_Name, Staff_Last_Name, Staff_email FROM Staff WHERE Staff_Code = '.$row['OvertimePreApp_Name'].''));
                        $rowTempB = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Project WHERE Project_Code = "'.$row['OvertimePreApp_Project'].'"'));

                        $email = 'The overtime you requested has been denied. The details of the denied overtime are as follows:'.Chr(10).
                            'NAME:                   '.$rowTemp['Staff_First_Name'].' '.$rowTemp['Staff_Last_Name'].Chr(10).
                            'PERIOD:                 '.GetTextualDateTimeFromDatabaseDateTime($row['OvertimePreApp_Start']).' until '.GetTextualDateTimeFromDatabaseDateTime($row['OvertimePreApp_End']).Chr(10).
                            'PROJECT:                '.$rowTempB['Project_Description'].Chr(10).
                            'HOURS:                  '.SPrintF('%02.2f', $row['OvertimePreApp_Hours']).Chr(10).
                            'DESCRIPTION:            '.$row['OvertimePreApp_Description'].Chr(10).
                            'MULTIPLE ENTRIES:       '.( $_GET['Multi']  ? 'Yes' : 'No').Chr(10).
                            'REASON FOR DENIAL:      '.($_GET['Reason'] ? $_POST['Reason'] : 'Not specified');

                        $html = 'The overtime you requested has been denied. The details of the denied overtime are as follows:
                             <BR /><BR />
                             <TABLE border=0>
                               <TR><TD><B>Name:</B></TD><TD>'.$rowTemp['Staff_First_Name'].' '.$rowTemp['Staff_Last_Name'].'</TD></TR>
                               <TR><TD><B>Period:</B></TD><TD>'.GetTextualDateTimeFromDatabaseDateTime($row['OvertimePreApp_Start']).' until '.GetTextualDateTimeFromDatabaseDateTime($row['OvertimePreApp_End']).'</TD></TR>
                               <TR><TD><B>Project:</B></TD><TD>'.$rowTempB['Project_Description'].'</TD></TR>
                               <TR><TD><B>Hours:</B></TD><TD>'.SPrintF('%02.2f', $row['OvertimePreApp_Hours']).'</TD></TR>
                               <TR><TD><B>Description:</B></TD><TD>'.$row['OvertimePreApp_Description'].'</TD></TR>
                               <TR><TD><B>Multiple Entries:</B></TD><TD>'.( $_GET['Multi'] ? 'Yes' : 'No').'</TD></TR>
                               <TR><TD><B>Reason for Denial:</B></TD><TD>'.($_GET['Reason'] ? $_GET['Reason'] : 'Not specified').'</TD></TR>
                             </TABLE>';

                        SendMailHTML($rowTemp['Staff_email'], 'Overtime Denied', $email, $html);
                        $status = 'pass';

                    }  else{
//                        $_SESSION['OvertimeFail'] = 'geh!';
                        $status = 'fail';
                    }
                    break;
                default:
                    break;
            }

            $result = array(GetDataForOvertimeApproveMulti($_GET['staffCode']),$status);
            echo json_encode($result);

        }

        else {
            $result = array(GetDataForOvertimeApproveMulti($_GET['staffCode']),"failed");
            echo json_encode($result);

        }
    }

}

function AddOvertime($PerApprovedID){

    $row = MySQL_Fetch_Array(ExecuteQuery('SELECT OvertimePreApp.*, (OvertimePreApp_Hours - OvertimePreApp_OvertimeHoursUsed) AS OvertimeAvail, Project_Description FROM OvertimePreApp, Project WHERE OvertimePreApp_ID = "'.$PerApprovedID.'" AND OvertimePreApp_Project = Project_Code'));
    if($row['OvertimePreApp_OvertimeHoursUsed'] == 0){
        if ($row['OvertimePreApp_Bank'] == '1')
            $query = 'INSERT INTO OvertimeBank VALUES("'.$row['OvertimePreApp_Name'].'", "'.Date('Y-m-d H:i:s').'", "'.$row['OvertimePreApp_Hours'].'", "'.$row['OvertimePreApp_Project'].'", "auto add", "'.$row['OvertimePreApp_Authorised'].'", "", "'.$row['OvertimePreApp_Start'].'", "'.$row['OvertimePreApp_End'].'", "0", "", "'.$row['OvertimePreApp_Billable'].'", "'.$PerApprovedID.'", "1")';
        if (ExecuteQuery($query))
        {
            $hours = $row['OvertimePreApp_OvertimeHoursUsed'] + $row['OvertimePreApp_Hours'];   //$row['OvertimePreApp_OvertimeHoursUsed'] == 0
            if ($hours >= $row['OvertimePreApp_Hours'])
                $used = '1';                         // always here
            else
                $used = '0';

            if ($row['OvertimePreApp_Multi'] == '1')
                $query = 'UPDATE OvertimePreApp SET OvertimePreApp_OvertimeUsed = "'.$used.'", OvertimePreApp_OvertimeHoursUsed = "'.$hours.'" WHERE OvertimePreApp_ID = "'.$PerApprovedID.'"';
            else
                $query = 'UPDATE OvertimePreApp SET OvertimePreApp_OvertimeUsed = "1", OvertimePreApp_OvertimeHoursUsed = "'.$hours.'" WHERE OvertimePreApp_ID = "'.$PerApprovedID.'"';

            if (ExecuteQuery($query))
            {
//              $hoursRemaining = $row['OvertimeAvail'] -  $row['OvertimePreApp_Hours'];   // set this to  0
//
//              $project = $row['Project_Description'];
//              $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = '.$row['OvertimePreApp_Authorised'].''));
//              $row1 = MySQL_Fetch_Array(ExecuteQuery('SELECT Staff_First_Name, Staff_Last_Name FROM Staff WHERE Staff_Code = "'.$row['OvertimePreApp_Name'].'"'));
//
//              $email = 'Overtime which you have authorised has been used. The details of the overtime are as follows:'.Chr(10).
//                       'NAME:                   '.$row1['Staff_First_Name'].' '.$row1['Staff_Last_Name'].Chr(10).
//                       'PROJECT:                '.$project.Chr(10).
//                       'PERIOD:                 '.$row['OvertimePreApp_Start'].' until '.$row['OvertimePreApp_End'].Chr(10).
//                       'HOURS:                  '.$row['OvertimePreApp_Hours'].Chr(10).
//                       'APPROVAL ID:            '.$PerApprovedID.Chr(10);
//
//              $html = 'Overtime which you have authorised has been used. The details of the overtime are as follows:
//                      <BR /><BR />
//                      <TABLE border=0>
//                        <TR><TD><B>Name:</B></TD><TD>'.$row1['Staff_First_Name'].' '.$row1['Staff_Last_Name'].'</TD></TR>
//                        <TR><TD><B>Project:</B></TD><TD>'.$project.'</TD></TR>
//                        <TR><TD><B>Period:</B></TD><TD>'.$row['OvertimePreApp_Start'].' until '.$row['OvertimePreApp_End'].'</TD></TR>
//                        <TR><TD><B>Hours:</B></TD><TD>'.$row['OvertimePreApp_Hours'].'</TD></TR>
//                        <TR><TD><B>Approval ID:</B></TD><TD>'.$PerApprovedID.'</TD></TR>';
//
//              if ($hoursRemaining < 0)
//              {
//                $email .= 'EXCESS HOURS USED:      '.(-$hoursRemaining);
//                $html .= '<TR><TD><B>Excess Hours Used:</B></TD><TD>'.(-$hoursRemaining).'</TD></TR>';
//              }
//
//              $html .= '</TABLE>';
                //SendMailHTML($rowTemp['Staff_email'], 'Overtime Recorded', $email, $html);
                return true;
            } else
                return false;
        } else
            return false;
    }
    return false;
}

//six
function ViewReminderList(){

    $Result = ExecuteQuery('SELECT `OvertimeReminder`.`reminder_id`,
                                        `OvertimeReminder`.`reminder_date`
                                         FROM `S4Admin`.`OvertimeReminder`
                                         order by reminder_date Asc');
    $reminderList = array();
    $count = 1;
    while($row = MySQL_Fetch_Array($Result)){
        $row['reminder_date'] = GetTextualDateFromDatabaseDate($row['reminder_date']);
        $row['reminder_id'] = $row['reminder_id'];
        $row['count'] = $count;

        array_push($reminderList,$row);
        $count++;
    }
    echo json_encode($reminderList);


}
function SetExpensesReminder(){
    $error_list = "";
    $Date = GetDatabaseDate($_POST['ExpensesDay'], $_POST['ExpensesMonth'], $_POST['ExpensesYear']);
    if(!checkdate($_POST['ExpensesMonth'],$_POST['ExpensesDay'], $_POST['ExpensesYear']))
        $error_list .= "Date Error";

    $todays_date = date( "Y-m-d" );
    $reminderDate = date('Y-m-d', strtotime('-2 days', strtotime($Date)));
    $time = "08h00" ;
    $day = date('l', $Date);
    $reminderDay = date('l', $reminderDate);

    if($reminderDate  <= $todays_date)
        $error_list .="Reminder Date is not a future date<br />".$reminderDate;

    if(empty($error_list)){
        $row = "INSERT INTO OvertimeReminder(reminder_date)VALUES('".$Date."')";
        if(ExecuteQuery($row)){

            $_SESSION['OvertimeSuccess'] = 'geh!';
        }
        else{
            $_SESSION['OvertimeIncomplete'] = 'geh!';
            $_SESSION['ReminderError'] = $row;

        }
    }
    else{

        $_SESSION['ReminderError'] = $error_list;
    }
    //set footer to louises email address not andrews
    //see if you can schedule a job for this
}
function DeletePayoutReminder(){
    $reminder_id = $_POST['Delete'];

    $row = "DELETE FROM OvertimeReminder WHERE reminder_id = ".$reminder_id;
    if(ExecuteQuery($row)){
        echo true;
    }
    else {
        echo false;
    }

}
function UpdatePayoutReminder(){
    $reminder_id = $_POST['reminder_id'];
    $reminder_date =  $_POST['reminder_date'];
    $date2 = date("Y-m-d", strtotime($reminder_date));

    $row = "UPDATE OvertimeReminder
                SET  reminder_date = '".$date2."'
                WHERE reminder_id = ".$reminder_id;
    if(ExecuteQuery($row)){
        echo $date2;
    }else{
        echo false;
    }
}


?>