<?php
  include '../Scripts/Include.php';
  SetSettings();
  CheckLoggedIn();
  $_POST = Replace('"', '\'\'', $_POST);
  
  switch ($_POST['Type'])
  {
    //User has submitted information for a new staff member.
    case 'Add':
      HandleAdd();
    	break;
    //User has submitted modified information for a staff member.
    case 'Edit':
      HandleEdit();
    	break;
    //User has selected to Add or Edit a staff member.
    case 'Maintain':
      HandleMaintain();
    	break;
    //User has selected to view a staff member.
    case 'ViewList':
    case 'ViewSingle':
      HandleView();
    	break;
    //User has reached this page incorrectly. If they are not authorised they are redirected to the main page from the Staff page.
    default:
    	break;
  }
  Header('Location: ../Staff.php?'.Rand());
  
  //////////////////////////////////////////////////////////////////////////////
  // Checks that all the required fields have values and that these values    //
  // are valid.                                                               //
  //////////////////////////////////////////////////////////////////////////////
  function CheckFields()
  {
    switch ($_POST['Type'])
    {
      case 'Add':
      case 'Edit':
        if (($_POST['FirstName'] == "") || ($_POST['LastName'] == "") || ($_POST['IDNumber'] == ""))
          return false;
        
        if (($_POST['DemogGender'] == "") || ($_POST['DemogRace'] == "") || ($_POST['DemogForeign'] == "") || ($_POST['DemogDisabled'] == "") || ($_POST['DemogSmoker'] == ""))
          return false;
        
        if (($_POST['Street1'] == "") || ($_POST['Suburb'] == "") || ($_POST['TownCity'] == "") || ($_POST['Country'] == "") || ($_POST['PostalCode'] == "") || !Is_Numeric($_POST['PostalCode']))
          return false;
        
        if (($_POST['EmergencyFirstName'] == "") || ($_POST['EmergencyLastName'] == "") || ($_POST['EmergencyRelation'] == ""))
          return false;
        
        if ($_POST['BranchCode'] != "")
        {
          if (!Is_Numeric($_POST['BranchCode']))
            return false;
        }
        
        if (($_POST['JobDescription'] == "") || ($_POST['ReportsTo'] == ""))
          return false;
        
        if ((($_POST['PassportType1'] == "") && ($_POST['PassportNumber1'] != "")) || (($_POST['PassportType1'] != "") && ($_POST['PassportNumber1'] == "")))
          return false;
        
        if ((($_POST['PassportType2'] == "") && ($_POST['PassportNumber2'] != "")) || (($_POST['PassportType2'] != "") && ($_POST['PassportNumber2'] == "")))
          return false;
        
        if (!(CheckDate($_POST['JoinedMonth'], $_POST['JoinedDay'], $_POST['JoinedYear'])) || !(CheckDate($_POST['PassportMonth1'], $_POST['PassportDay1'], $_POST['PassportYear1'])) || !(CheckDate($_POST['PassportMonth2'], $_POST['PassportDay2'], $_POST['PassportYear2'])))
          return false;
        
        if ($_POST['QualType'] == "")
          return false;
        else
          {
            if (($_POST['QualType'] != "1") && ($_POST['QualDescription'] == ""))
              return false;
          }
        break;
      case 'ViewList':
        break;
      case 'ViewSingle':
        if ($_POST['Staff'] == "" && ($_SESSION['cUID'] == 15 || $_SESSION['cUID'] == 318 || $_SESSION['cUID'] == 1 || $_SESSION['cUID'] == 282))
          return false;
        break;
      default:
        return false;
        break;
    }
    
    return true;
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Handles the user's submission of information for a new staff member.     //
  //////////////////////////////////////////////////////////////////////////////
  function HandleAdd()
  {
    $_SESSION['AddStaff'][0] = $_POST['FirstName'];
    $_SESSION['AddStaff'][1] = $_POST['ContactHome'];
    $_SESSION['AddStaff'][2] = $_POST['LastName'];
    $_SESSION['AddStaff'][3] = $_POST['ContactCell'];
    $_SESSION['AddStaff'][4] = $_POST['IDNumber'];
    $_SESSION['AddStaff'][5] = $_POST['ContactWork'];
    $_SESSION['AddStaff'][6] = $_POST['ITNumber'];
    $_SESSION['AddStaff'][7] = $_POST['Email'];
    $_SESSION['AddStaff'][8] = $_POST['Street1'];
    $_SESSION['AddStaff'][9] = $_POST['TownCity'];
    $_SESSION['AddStaff'][10] = $_POST['Street2'];
    $_SESSION['AddStaff'][11] = $_POST['Country'];
    $_SESSION['AddStaff'][12] = $_POST['Suburb'];
    $_SESSION['AddStaff'][13] = $_POST['PostalCode'];
    $_SESSION['AddStaff'][14] = $_POST['EmergencyFirstName'];
    $_SESSION['AddStaff'][15] = $_POST['EmergencyContactHome'];
    $_SESSION['AddStaff'][16] = $_POST['EmergencyLastName'];
    $_SESSION['AddStaff'][17] = $_POST['EmergencyContactCell'];
    $_SESSION['AddStaff'][18] = $_POST['EmergencyRelation'];
    $_SESSION['AddStaff'][19] = $_POST['EmergencyContactWork'];
    $_SESSION['AddStaff'][20] = $_POST['BankName'];
    $_SESSION['AddStaff'][21] = $_POST['AccountNumber'];
    $_SESSION['AddStaff'][22] = $_POST['BranchCode'];
    $_SESSION['AddStaff'][23] = $_POST['JobDescription'];
    $_SESSION['AddStaff'][24] = $_POST['ReportsTo'];
    $_SESSION['AddStaff'][25] = $_POST['VoyagerNumber'];
    $_SESSION['AddStaff'][26] = $_POST['PassportType1'];
    $_SESSION['AddStaff'][27] = $_POST['PassportType2'];
    $_SESSION['AddStaff'][28] = $_POST['PassportNumber1'];
    $_SESSION['AddStaff'][29] = $_POST['PassportNumber2'];
    $_SESSION['AddStaff'][30] = $_POST['PassportYear1'].$_POST['PassportMonth1'].$_POST['PassportDay1'];
    $_SESSION['AddStaff'][31] = $_POST['PassportYear2'].$_POST['PassportMonth2'].$_POST['PassportDay2'];
    $_SESSION['AddStaff'][32] = $_POST['Size'];
    $_SESSION['AddStaff'][33] = $_POST['QualType'];
    $_SESSION['AddStaff'][34] = $_POST['QualDescription'];
    $_SESSION['AddStaff'][35] = $_POST['JoinedYear'].$_POST['JoinedMonth'].$_POST['JoinedDay'];
    //Demographic Details
    $_SESSION['AddStaff'][36] = $_POST['DemogGender'];
    $_SESSION['AddStaff'][37] = $_POST['DemogForeign'];
    $_SESSION['AddStaff'][38] = $_POST['DemogRace'];
    $_SESSION['AddStaff'][39] = $_POST['DemogDisabled'];
    $_SESSION['AddStaff'][40] = $_POST['DemogMarital'];
    $_SESSION['AddStaff'][41] = $_POST['DemogSmoker'];
    $_SESSION['AddStaff'][42] = $_POST['SMSGroups'];
              
    switch ($_POST['Submit'])
    {
      case 'Cancel':
        Session_Unregister('AddStaff');
        break;
      case 'Submit':
        if (CheckFields())
        {
          $row = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code < 100 OR Staff_Code > 200 ORDER BY Staff_Code DESC LIMIT 1'));
          $code = SPrintF('%03d', (integer)$row['Staff_Code'] + 1);
          if ($code == 100)
            $code = 201;
          
          if (ExecuteQuery('INSERT INTO Staff VALUES("'.$code.'","'.$_POST['FirstName'].'","'.$_POST['LastName'].'","'.$_POST['ContactHome'].'","'.$_POST['ContactWork'].'","'.$_POST['ContactCell'].'","'.$_POST['Street1'].'","'.$_POST['Suburb'].'","'.$_POST['TownCity'].'","'.$_POST['Country'].'","'.$_POST['PostalCode'].'","'.$_POST['Street2'].'","'.$_POST['IDNumber'].'","'.$_POST['ITNumber'].'", "0", "'.$_POST['EmergencyFirstName'].','.$_POST['EmergencyLastName'].'","'.$_POST['EmergencyRelation'].'","'.$_POST['EmergencyContactHome'].'","'.$_POST['EmergencyContactWork'].'","'.$_POST['EmergencyContactCell'].'","'.$_POST['JobDescription'].'","'.$_POST['Email'].'", old_password("'.StrToLower(SubStr($_POST['FirstName'], 0, 3)).'"), "5", "1900-01-01", "0", "1", "'.$_POST['ReportsTo'].'","'.$_POST['BankName'].'","'.$_POST['BranchCode'].'","'.$_POST['AccountNumber'].'","1.25","'.$_POST['JoinedYear'].'-'.$_POST['JoinedMonth'].'-'.$_POST['JoinedDay'].'", "0", "0", "0", "0", "0", "0", "0", "'.$_POST['Size'].'", "'.$_POST['QualType'].'", "'.$_POST['QualDescription'].'", "'.$_POST['DemogGender'].'", "'.$_POST['DemogRace'].'", "'.$_POST['DemogMarital'].'", "'.$_POST['DemogForeign'].'", "'.$_POST['DemogDisabled'].'", "'.$_POST['DemogSmoker'].'", "0", "0", "0", "0", "0", "0", "1900-01-01",1)'))
    	    {
                $inserts = "";
                if(isset($_SESSION['AddStaff'][42])){
                foreach ($_SESSION['AddStaff'][42] as $selectedOption)
                $inserts .= '("'.$code.'",'.$selectedOption.'),';
                // remove last ,
                $inserts = rtrim($inserts,',');
                
                }
               
                $pass = true;
                if($inserts !== ""){
                  $sql = 'INSERT INTO sms_Staff_Group (sms_Staff_Code, sms_GroupID) VALUES '.$inserts; 
                  if (!ExecuteQuery($sql)){
                      $pass = false; 
                  }
                }
                if($pass){
                
                if (ExecuteQuery('INSERT INTO StaffTravel VALUES("'.$code.'", "'.$_POST['VoyagerNumber'].'", "'.$_POST['PassportType1'].'", "'.$_POST['PassportNumber1'].'", "'.$_POST['PassportType2'].'", "'.$_POST['PassportNumber2'].'", "'.$_POST['PassportYear1'].'-'.$_POST['PassportMonth1'].'-'.$_POST['PassportDay1'].' 00:00:00", "'.$_POST['PassportYear2'].'-'.$_POST['PassportMonth2'].'-'.$_POST['PassportDay2'].' 00:00:00")'))
                    {
                        $row = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = '.$_SESSION['cUID'].''));

                        $email = 'Staff details have been added. The details are as follows:'.Chr(10).
                                 'STAFF NAME:            '.$_POST['FirstName'].' '.$_POST['LastName'].Chr(10).
                                 'ADDED BY:              '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].Chr(10).Chr(10).
                                 'Please review the new details to ensure that everything is in order.'.Chr(10).Chr(10);

                        $html = 'Staff details have been added. The details are as follows:
                                <BR /><BR />
                                <TABLE border=0>
                                  <TR><TD><B>Staff Name:</B></TD><TD>'.$_POST['FirstName'].' '.$_POST['LastName'].'</TD></TR>
                                  <TR><TD><B>Added By:</B></TD><TD>'.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].'</TD></TR>
                                </TABLE>
                                <BR />
                                Please review the new details to ensure that everything is in order.
                                <BR /><BR />';

                        if ($_SESSION['cUID'] != '32')
                          SendMailHTML('cherie@s4integration.co.za', 'Staff Added', $email, $html);

                        $email .= 'Once the details are confirmed to be correct they can be forwarded to AFB for inclusion on their system. The RA must also be set up.';
                        $html .= 'Once the details are confirmed to be correct they can be forwarded to AFB for inclusion on their system. The RA must also be set up.
                                <BR /><BR />';

                        SendMailHTML('management@s4integration.co.za', 'Staff Added', $email, $html);

                        //Mail staff the vCard.
                        $loFileName = '../Files/Temp/'.$code.'.vcf';
                        $loContents = 'BEGIN:VCARD'.Chr(10).
                                      'VERSION:2.1'.Chr(10).
                                      'N:'.$_POST['LastName'].';'.$_POST['FirstName'].Chr(10).
                                      'FN:'.$_POST['FirstName'].Chr(10).
                                      'TEL;HOME;VOICE:'.$_POST['ContactHome'].Chr(10).
                                      'TEL;CELL;VOICE:'.$_POST['ContactCell'].Chr(10).
                                      'EMAIL;PREF;INTERNET:'.$_POST['Email'].Chr(10).
                                      'END:VCARD';
                        $loSuccess = File_Put_Contents($loFileName, $loContents);

                        $email = 'A new staff member has joined S4. Here is a vCard with their contact details that you can use together with your email client.'.Chr(10).
                                  'To add it to your email client save it to your hard drive, open it and click Add to Address Book and complete the form that is displayed.';
                        $html = 'A new staff member has joined S4. Here is a vCard with their contact details that you can use together with your email client.'.
                                  '<BR />'.
                                  'To add it to your email client save it to your hard drive, open it and click Add to Address Book and complete the form that is displayed.';

                                    // Split staff emails to send S4 to S4 and S4 Automation to S4 Automation
                        //SendMailHTML('staff@s4integration.co.za', 'New Staff - '.$_POST['FirstName'].' '.$_POST['LastName'].(!$loSuccess ? 'poo!' : ' yay!'), $email, $html, $loFileName);
                        SendMailHTML('s4.integration@s4integration.co.za', 'New Staff - '.$_POST['FirstName'].' '.$_POST['LastName'].(!$loSuccess ? ' ERR' : ' '), $email, $html, $loFileName);

                        $_SESSION['StaffSuccess'] = 'geh!';
                        Session_Unregister('AddStaff');
                        return;
                      } 
                else{
                        ExecuteQuery('DELETE FROM Staff WHERE Staff_Code = '.$code.''); 
                        ExecuteQuery('DELETE FROM `sms_Staff_Group` WHERE `sms_Staff_Code` = "'.$code.'"');
                   }

                }
                else{
                    ExecuteQuery('DELETE FROM Staff WHERE Staff_Code = '.$code.''); 
                }
                                      
          } else
            $_SESSION['StaffFail'] = 'geh!';
        } else
          $_SESSION['StaffIncomplete'] = 'geh!';
        break;
      default:
        break;
    }
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Handles the user's submission of modified information for a staff        //
  // member.                                                                  //
  //////////////////////////////////////////////////////////////////////////////
  function HandleEdit()
  {
    $_SESSION['EditStaff'][1] = $_POST['FirstName'];
    $_SESSION['EditStaff'][2] = $_POST['ContactHome'];
    $_SESSION['EditStaff'][3] = $_POST['LastName'];
    $_SESSION['EditStaff'][4] = $_POST['ContactCell'];
    $_SESSION['EditStaff'][5] = $_POST['IDNumber'];
    $_SESSION['EditStaff'][6] = $_POST['ContactWork'];
    $_SESSION['EditStaff'][7] = $_POST['ITNumber'];
    $_SESSION['EditStaff'][8] = $_POST['Email'];
    $_SESSION['EditStaff'][9] = $_POST['Street1'];
    $_SESSION['EditStaff'][10] = $_POST['TownCity'];
    $_SESSION['EditStaff'][11] = $_POST['Street2'];
    $_SESSION['EditStaff'][12] = $_POST['Country'];
    $_SESSION['EditStaff'][13] = $_POST['Suburb'];
    $_SESSION['EditStaff'][14] = $_POST['PostalCode'];
    $_SESSION['EditStaff'][15] = $_POST['EmergencyFirstName'];
    $_SESSION['EditStaff'][16] = $_POST['EmergencyContactHome'];
    $_SESSION['EditStaff'][17] = $_POST['EmergencyLastName'];
    $_SESSION['EditStaff'][18] = $_POST['EmergencyContactCell'];
    $_SESSION['EditStaff'][19] = $_POST['EmergencyRelation'];
    $_SESSION['EditStaff'][20] = $_POST['EmergencyContactWork'];
    $_SESSION['EditStaff'][21] = $_POST['BankName'];
    $_SESSION['EditStaff'][22] = $_POST['AccountNumber'];
    $_SESSION['EditStaff'][23] = $_POST['BranchCode'];
    $_SESSION['EditStaff'][24] = $_POST['JobDescription'];
    $_SESSION['EditStaff'][25] = $_POST['ReportsTo'];
    $_SESSION['EditStaff'][26] = $_POST['VoyagerNumber'];
    $_SESSION['EditStaff'][27] = $_POST['PassportType1'];
    $_SESSION['EditStaff'][28] = $_POST['PassportType2'];
    $_SESSION['EditStaff'][29] = $_POST['PassportNumber1'];
    $_SESSION['EditStaff'][30] = $_POST['PassportNumber2'];
    $_SESSION['EditStaff'][31] = $_POST['PassportYear1'].$_POST['PassportMonth1'].$_POST['PassportDay1'];
    $_SESSION['EditStaff'][32] = $_POST['PassportYear2'].$_POST['PassportMonth2'].$_POST['PassportDay2'];
    $_SESSION['EditStaff'][33] = $_POST['Size'];
    $_SESSION['EditStaff'][34] = $_POST['QualType'];
    $_SESSION['EditStaff'][35] = $_POST['QualDescription'];
    $_SESSION['EditStaff'][36] = $_POST['JoinedYear'].$_POST['JoinedMonth'].$_POST['JoinedDay'];
    //Demographic Details
    $_SESSION['EditStaff'][37] = $_POST['DemogGender'];
    $_SESSION['EditStaff'][38] = $_POST['DemogForeign'];
    $_SESSION['EditStaff'][39] = $_POST['DemogRace'];
    $_SESSION['EditStaff'][40] = $_POST['DemogDisabled'];
    $_SESSION['EditStaff'][41] = $_POST['DemogMarital'];
    $_SESSION['EditStaff'][42] = $_POST['DemogSmoker'];
    
    switch ($_POST['Submit'])
    {
      case 'Cancel':
        Session_Unregister('EditStaff');
        break;
      case 'Submit':
        if (CheckFields())
        {
          $success = false;
          if (ExecuteQuery('UPDATE Staff SET Staff_First_Name = "'.$_POST['FirstName'].'", Staff_Last_Name = "'.$_POST['LastName'].'", Staff_Phone_Home = "'.$_POST['ContactHome'].'", Staff_Phone_Work = "'.$_POST['ContactWork'].'", Staff_Phone_Mobile = "'.$_POST['ContactCell'].'", Staff_Home_Address_Street = "'.$_POST['Street1'].'", Staff_Home_Address_Suburb = "'.$_POST['Suburb'].'", Staff_Home_Address_City = "'.$_POST['TownCity'].'", Staff_Home_Address_Country = "'.$_POST['Country'].'", Staff_Home_Address_Code = "'.$_POST['PostalCode'].'", Staff_Home_Address_Street2 = "'.$_POST['Street2'].'", Staff_ID_Number = "'.$_POST['IDNumber'].'", Staff_IT_Number = "'.$_POST['ITNumber'].'", Staff_Emergency_Name = "'.$_POST['EmergencyFirstName'].','.$_POST['EmergencyLastName'].'", Staff_Emergency_Relation = "'.$_POST['EmergencyRelation'].'", Staff_Emergency_Phone_Home = "'.$_POST['EmergencyContactHome'].'", Staff_Emergency_Phone_Work = "'.$_POST['EmergencyContactWork'].'", Staff_Emergency_Phone_Cell = "'.$_POST['EmergencyContactCell'].'", Staff_Job_Desc = "'.$_POST['JobDescription'].'", Staff_Start_Date = "'.$_POST['JoinedYear'].'-'.$_POST['JoinedMonth'].'-'.$_POST['JoinedDay'].'", Staff_email = "'.$_POST['Email'].'", Staff_Reports_To = "'.$_POST['ReportsTo'].'", Staff_Bank_Name = "'.$_POST['BankName'].'", Staff_Bank_Branch = "'.$_POST['BranchCode'].'", Staff_Bank_Account = "'.$_POST['AccountNumber'].'", Staff_Size = "'.$_POST['Size'].'", Staff_Qualification_Type = "'.$_POST['QualType'].'", Staff_Qualification_Description = "'.$_POST['QualDescription'].'", Staff_Demographics_Gender = "'.$_POST['DemogGender'].'", Staff_Demographics_Race = "'.$_POST['DemogRace'].'", Staff_Demographics_Marital = "'.$_POST['DemogMarital'].'", Staff_Demographics_Foreign = "'.$_POST['DemogForeign'].'", Staff_Demographics_Disabled = "'.$_POST['DemogDisabled'].'", Staff_Demographics_Smoker = "'.$_POST['DemogSmoker'].'" WHERE Staff_Code = "'.$_SESSION['EditStaff'][0].'"'))
    	    {
    	      $resultSet = ExecuteQuery('SELECT * FROM StaffTravel WHERE StaffTravel_Name = "'.$_SESSION['EditStaff'][0].'"');
            if (MySQL_Num_Rows($resultSet) > 0)
            {
              if (ExecuteQuery('UPDATE StaffTravel SET StaffTravel_Voyager_Number = "'.$_POST['VoyagerNumber'].'", StaffTravel_Passport1_Type = "'.$_POST['PassportType1'].'", StaffTravel_Passport1_Number = "'.$_POST['PassportNumber1'].'", StaffTravel_Passport2_Type = "'.$_POST['PassportType2'].'", StaffTravel_Passport2_Number = "'.$_POST['PassportNumber2'].'", StaffTravel_Passport1_ExpiryDate = "'.$_POST['PassportYear1'].'-'.$_POST['PassportMonth1'].'-'.$_POST['PassportDay1'].' 00:00:00", StaffTravel_Passport2_ExpiryDate = "'.$_POST['PassportYear2'].'-'.$_POST['PassportMonth2'].'-'.$_POST['PassportDay2'].' 00:00:00" WHERE StaffTravel_Name = "'.$_SESSION['EditStaff'][0].'"'))
        	    {
                $_SESSION['StaffSuccess'] = 'geh!';
                Session_Unregister('EditStaff');
                $success = true;
              }
            } else
            if (ExecuteQuery('INSERT INTO StaffTravel VALUES("'.$_SESSION['EditStaff'][0].'", "'.$_POST['VoyagerNumber'].'", "'.$_POST['PassportType1'].'", "'.$_POST['PassportNumber1'].'", "'.$_POST['PassportType2'].'", "'.$_POST['PassportNumber2'].'", "'.$_POST['PassportYear1'].'-'.$_POST['PassportMonth1'].'-'.$_POST['PassportDay1'].' 00:00:00","'.$_POST['PassportYear2'].'-'.$_POST['PassportMonth2'].'-'.$_POST['PassportDay2'].' 00:00:00")'))
      	    {
              $_SESSION['StaffSuccess'] = 'geh!';
              Session_Unregister('EditStaff');
              $success = true;
            }
            if ($success)
            {
              if ($_SESSION['cUID'] != '32')
              {
                $row = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = '.$_SESSION['cUID'].''));
                
                $email = 'Staff details have been updated. The details are as follows:'.Chr(10).
                         'STAFF NAME:            '.$_POST['FirstName'].' '.$_POST['LastName'].Chr(10).
                         'UPDATED BY:            '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].Chr(10).Chr(10).
                         'Please review the updated details to ensure that everything is in order.';
                
                $html = 'Staff details have been updated. The details are as follows:
                        <BR /><BR />
                        <TABLE border=0>
                          <TR><TD><B>Staff Name:</B></TD><TD>'.$_POST['FirstName'].' '.$_POST['LastName'].'</TD></TR>
                          <TR><TD><B>Updated By:</B></TD><TD>'.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].'</TD></TR>
                        </TABLE>
                        <BR />
                        Please review the updated details to ensure that everything is in order.
                        <BR /><BR />';
                
                SendMailHTML('cherie@s4integration.co.za', 'Staff Updated', $email, $html);
              }
              return;
            }
          }
    	    $_SESSION['StaffFail'] = 'geh!';
        } else
          $_SESSION['StaffIncomplete'] = 'geh!';
        break;
      default:
        break;
    }
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Handles the user's maintenance selection.                                //
  //////////////////////////////////////////////////////////////////////////////
  function HandleMaintain()
  {
    switch ($_POST['Submit'])
    {
      case 'Add':
        $_SESSION['AddStaff'] = array();
        $_SESSION['AddStaff'][1] = '___-_______';
        $_SESSION['AddStaff'][3] = '___-_______';
        $_SESSION['AddStaff'][5] = '___-_______';
        $_SESSION['AddStaff'][15] = '___-_______';
        $_SESSION['AddStaff'][17] = '___-_______';
        $_SESSION['AddStaff'][19] = '___-_______';
        break;
      case 'Edit':
        if (in_array($_SESSION['cUID'], ManagementAccess()))
          if ($_POST['Staff'] == "")
            $_SESSION['StaffIncomplete'] = 'geh!';
          else
            $_SESSION['EditStaff'] = array($_POST['Staff']);
        else
          $_SESSION['EditStaff'] = array($_SESSION['cUID']);
        if (!IsSet($_SESSION['StaffIncomplete']))
        {
          $row = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = '.$_SESSION['EditStaff'][0].''));
          $_SESSION['EditStaff'][1] = $row['Staff_First_Name'];
          $_SESSION['EditStaff'][2] = $row['Staff_Phone_Home'];
          $_SESSION['EditStaff'][3] = $row['Staff_Last_Name'];
          $_SESSION['EditStaff'][4] = $row['Staff_Phone_Mobile'];
          $_SESSION['EditStaff'][5] = $row['Staff_ID_Number'];
          $_SESSION['EditStaff'][6] = $row['Staff_Phone_Work'];
          $_SESSION['EditStaff'][7] = $row['Staff_IT_Number'];
          $_SESSION['EditStaff'][8] = $row['Staff_email'];
          $_SESSION['EditStaff'][9] = $row['Staff_Home_Address_Street'];
          $_SESSION['EditStaff'][10] = $row['Staff_Home_Address_City'];
          $_SESSION['EditStaff'][11] = $row['Staff_Home_Address_Street2'];
          $_SESSION['EditStaff'][12] = $row['Staff_Home_Address_Country'];
          $_SESSION['EditStaff'][13] = $row['Staff_Home_Address_Suburb'];
          $_SESSION['EditStaff'][14] = $row['Staff_Home_Address_Code'];
          $_SESSION['EditStaff'][15] = SubStr($row['Staff_Emergency_Name'], 0, StrPos($row['Staff_Emergency_Name'], ','));
          $_SESSION['EditStaff'][16] = $row['Staff_Emergency_Phone_Home'];
          $_SESSION['EditStaff'][17] = Trim(StrStr($row['Staff_Emergency_Name'], ','),',');
          $_SESSION['EditStaff'][18] = $row['Staff_Emergency_Phone_Cell'];
          $_SESSION['EditStaff'][19] = $row['Staff_Emergency_Relation'];
          $_SESSION['EditStaff'][20] = $row['Staff_Emergency_Phone_Work'];
          $_SESSION['EditStaff'][21] = $row['Staff_Bank_Name'];
          $_SESSION['EditStaff'][22] = $row['Staff_Bank_Account'];
          $_SESSION['EditStaff'][23] = $row['Staff_Bank_Branch'];
          $_SESSION['EditStaff'][24] = $row['Staff_Job_Desc'];
          $_SESSION['EditStaff'][25] = $row['Staff_Reports_To'];
          $_SESSION['EditStaff'][33] = $row['Staff_Size'];
          $_SESSION['EditStaff'][34] = $row['Staff_Qualification_Type'];
          $_SESSION['EditStaff'][35] = $row['Staff_Qualification_Description'];
          $_SESSION['EditStaff'][36] = GetSessionDateFromDatabaseDate($row['Staff_Start_Date']);
          //Demographic Details
          $_SESSION['EditStaff'][37] = $row['Staff_Demographics_Gender'];
          $_SESSION['EditStaff'][38] = $row['Staff_Demographics_Foreign'];
          $_SESSION['EditStaff'][39] = $row['Staff_Demographics_Race'];
          $_SESSION['EditStaff'][40] = $row['Staff_Demographics_Disabled'];
          $_SESSION['EditStaff'][41] = $row['Staff_Demographics_Marital'];
          $_SESSION['EditStaff'][42] = $row['Staff_Demographics_Smoker'];

          
          $row = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM StaffTravel WHERE StaffTravel_Name = '.$_SESSION['EditStaff'][0].''));
          $_SESSION['EditStaff'][26] = $row['StaffTravel_Voyager_Number'];
          $_SESSION['EditStaff'][27] = $row['StaffTravel_Passport1_Type'];
          $_SESSION['EditStaff'][28] = $row['StaffTravel_Passport2_Type'];
          $_SESSION['EditStaff'][29] = $row['StaffTravel_Passport1_Number'];
          $_SESSION['EditStaff'][30] = $row['StaffTravel_Passport2_Number'];
          $_SESSION['EditStaff'][31] = GetSessionDateFromDatabaseDate($row['StaffTravel_Passport1_ExpiryDate']);
          $_SESSION['EditStaff'][32] = GetSessionDateFromDatabaseDate($row['StaffTravel_Passport2_ExpiryDate']);
        }
        break;
      default:
        break;
    }
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Handles the user's submission of a staff member selection.               //
  //////////////////////////////////////////////////////////////////////////////
  function HandleView()
  {
    if (CheckFields())
    {
      switch ($_POST['Type'])
      {
        case 'ViewList':
          $_SESSION['ViewStaffList'] = array();
          break;
        case 'ViewSingle':
          if (in_array($_SESSION['cUID'], ManagementAccess()))
              $_SESSION['ViewStaffSingle'] = array($_POST['Staff']);
          else
              $_SESSION['ViewStaffSingle'] = array($_SESSION['cUID']);
          break;
        default:
          return false;
          break;
      }
    } else
      $_SESSION['StaffIncomplete'] = 'geh!';
  }
?>