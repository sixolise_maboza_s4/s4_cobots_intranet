<?php


require('../PHPExcel.php');

if(isset($_POST['SQlArray']) && isset($_POST['FileName'])){
  
 $outline = array(
    'borders' => array(
        'outline' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN
            
        ),
    ),
);  
    
$styleArray_Header1 = array(
    'font'  => array(
        'bold'  => true,
        'size'  => 15
         
    ),'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => '6f8bb7'),
                
          
    ),
    'borders' => array(
            'allborders' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN
            )
        )
);

$styleArray_Header2 = array(
    'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'd3dbe8')
    ),
    'borders' => array(
            'allborders' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN
            )
        )
);

$styleArray_TableHeader = array(
    'font'  => array(
        'bold'  => true
    ),'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'eff0f1')
    ),
    'borders' => array(
            'allborders' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN
            )
        )
);

$styleArray_TableContent = array(
    'borders' => array(
            'allborders' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN
            )
        )
);

$styleArray_TableContentDefaultAnswer = array(
    'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'e0ffe1')
    ),
    'borders' => array(
            'allborders' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN
            )
        )
);


$objPHPExcel = new PHPExcel();
header('Content-type: application/vnd.ms-excel');
header('Content-Disposition: attachment; filename="'.$_POST['FileName'].'_s4.xlsx"');

  $objPHPExcel->getActiveSheet()->getStyle('I')->getNumberFormat()->setFormatCode('#,##0.00');
   $objPHPExcel->getActiveSheet()->getStyle('J')->getNumberFormat()->setFormatCode('#,##0.00');

//$datau = utf8_encode($_POST['SQlArray']);                               // incase fail use this  totaly remove 
$valueA = $_POST['SQlArray'];
$data = json_decode(stripcslashes($valueA));
 
 ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////**********************************method
//echo sizeof($data) ;
 //starting pos
 $curCol = 0;
 $currow = 1;
 
$colnames = array('Order Number'=>'OrderNo_Number',
        'Project'=> 'Project',
        'Supplier'=>'Supplier_Name',
        'Quotation  number'=>'quotation_number',
        'Date Placed'=>'OrderNo_Date_Time',
        'Requested By'=>'Requested_By',
        'Supplier Invoice'=>'SupplierInvoice',
	'Currency Symbol'=>'Currency_Symbol',
        'Original Total'=>'OrderNo_Original_Total_Cost',
        //'SixOriginalTotalCost'=>'SixOriginalTotalCost',
        'Invoice Total'=>'total');
 
 //sub col names
 $Sub_colnames = array('Supplier Code'=>'Items_Supplier_Code',
     'Description'=>'Items_Description',
      'Item Backorder'=>'Items_Backorder',                  //this is bit. value is changed in output   
      'Original Value (VAT excl.)'=>'Items_Value',
      'Nett_Value (Discount Incl.)'=>'Nett_Value',        
      'Invoice Value (Vat excl.)'=>'Latest_Value',
      'Quantity'=>'Items_Quantity',
      'Item Asset'=>'Items_Asset',                //this is bit. value is changed in output
      'Invoice Capture'=>'Items_Received_Quantity',
      'Storeman Received'=>'Items_Storeman_Received',
      'Storage Type'=>'Items_StorageType',
      'Discount %'=>'Items_Discount');
 
 
 //make the headers
  foreach( $colnames as $key => $value){
      $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($curCol, $currow, $key);
      $curCol++;
  }
 // format the headers
    $curCol--;
    $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(0, $currow ,$curCol, $currow)->applyFromArray($styleArray_Header1);
    
    $curCol = 0;
    $currow++;
  
 
    ///load all the data   
    for ($i = 0; $i < sizeof($data) ; $i++){

        foreach($colnames as $key => $value){
            if($key === "Original Total"){
              // $fmt = new NumberFormatter( 'de_DE', NumberFormatter::CURRENCY );
//                $currency = $fmt->formatCurrency($value, "EUR");
                $entry = $data[$i]->{iconv('Windows-1252', 'UTF-8//TRANSLIT',$value)};
            }else{
                $entry = $data[$i]->{iconv('Windows-1252', 'UTF-8//TRANSLIT',$value)};
            }
                
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($curCol, $currow,$entry); 
            $curCol++;
         }
         $currow++;
         $curCol=0;  
         
         // make the sub table
         $subArray = $data[$i]->{'Items'};
         if(sizeof($subArray) >= 1){
             // make sub headings
             $firstrow = $currow;
             $curCol=1;
             foreach($Sub_colnames as $key => $value){
                 if($key === "Supplier Code"){
                     $a = "Manufacturer Code";
                     $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($curCol, $currow,$a); 
                 }
                 else if($key === "Description"){
                      $a = "Manufacturer Description";
                     $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($curCol, $currow,$a); 
                 }
                 else{
                     $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($curCol, $currow,$key); 
                 }
                $curCol++;
             }
             $curCol--;
             $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(1, $currow ,$curCol, $currow)->applyFromArray($styleArray_Header2);
             $currow++;
             $curCol=1;
             for ($x = 0; $x < sizeof( $subArray) ; $x++){
                 
                foreach($Sub_colnames as $key => $value){
                    $sub_entry = $data[$i]->{'Items'}[$x]->{$value};
                   if($key === "Item Backorder" || $key === "Item Asset" ){
                        if( $sub_entry == 1){
                            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($curCol, $currow,'Yes'); 
                        }
                        else{
                            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($curCol, $currow,'No'); 
                        }
                    }
                   else{
                       $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($curCol, $currow,$sub_entry); 
                     }
                     
                     if($key === "Original Value (VAT excl.)" || $key === "Nett_Value (Discount Incl.)" || $key= "Invoice Value (Vat excl.)"){
                        $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(1,$firstrow  ,$lastcol, $currow)->getNumberFormat()->setFormatCode('_-* #,##0.00\ ');
                          $objPHPExcel->getActiveSheet()->getStyle("H")->getNumberFormat()->setFormatCode('_-* #,##0\ ');
                    }
                    $lastcol = $curCol;
                    $curCol++;
                 }
                   $currow++;
                   $curCol=1;
             }
              $curCol--;
              $currow--;
              $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(1, $firstrow  ,$lastcol, $currow)->applyFromArray($outline);
         
              // add in the grouping 
               for ($row = $firstrow; $row <= $currow; ++$row) {
                  $objPHPExcel->getActiveSheet()
                  ->getRowDimension($row)
                  ->setOutlineLevel(1)
                  ->setVisible(false)
                  ->setCollapsed(true);
                }
              $currow++;
             
              $curCol=0;
   
         }
    }

   //set defualt spacing 
   $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
   $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
   $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
   $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
   $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
   $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
   $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
   $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
   $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
   $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
   $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
   $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
   $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
  // $objPHPExcel->getActiveSheet()->getStyle('I')->getNumberFormat()->setFormatCode('#,###.00');
  // $objPHPExcel->getActiveSheet()->getStyle('J')->getNumberFormat()->setFormatCode('#,###.00');
   
   
   $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
   $objWriter->save('php://output');
}
