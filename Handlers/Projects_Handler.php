<?php
  // DETAILS ///////////////////////////////////////////////////////////////////
  //                                                                          //
  //                    Last Edited By: Gareth Ambrose                        //
  //                        Date: 15 July 2009                                //
  //                                                                          //
  //////////////////////////////////////////////////////////////////////////////
  // This page handles the back-end for the Projects page.                    //
  //////////////////////////////////////////////////////////////////////////////
  

  include '../Scripts/Include.php';
  SetSettings();
  CheckLoggedIn();
  $_POST = Replace('"', '\'\'', $_POST);
  

  if(isset($_GET['function'])){
   switch ($_GET['function']){
       case 'getProjectMappingCode':
            getProjectMappingCode() ;  // make the function
            break;
        default:
            break;
     }
  
  }
  else{
  switch ($_POST['Type'])
  {
    //User has submitted information for a new project.
    case 'Add':
      HandleAdd();
    	break;
    //User has submitted modified information for a project.
    case 'Edit':
      HandleEdit();
    	break;
    //User has selected to Add or Edit a project.
    case 'Maintain':
      HandleMaintain();
      break;
    //User has submitted information for project history.
    case 'Update':
      HandleUpdate();
    	break;
      case 'Mapping':
          HandleMapping();
          break;
    //User has selected to view a project.
    case 'View':
    case 'ViewSingle':
      case 'ViewMapping':
      HandleView();
    	break;
    case 'CostManagers':
        HandleCostManager();
    //User has reached this page incorrectly. If they are not authorised they are redirected to the main page from the Projects page.
    default:
    	break;
  }
   Header('Location: ../Projects.php?'.Rand());
  }
  //////////////////////////////////////////////////////////////////////////////
  // Checks that all the required fields have values and that these values    //
  // are valid.                                                               //
  //////////////////////////////////////////////////////////////////////////////
  function CheckFields()
  {
      
    switch ($_POST['Type'])
    {
      case 'Edit':         
        if (($_POST['Orders'] == "") || ($_POST['WorkInProgress'] == "") || ($_POST['Complete'] == "") || ($_POST['CostManager2'] == "")|| ($_POST['CostManager3'] == "") || ($_POST['CostManager8'] == "") )
          return false;
        
       if (!Is_Numeric($_POST['Orders']) || !Is_Numeric($_POST['WorkInProgress']) || !Is_Numeric($_POST['Complete']))
          return false;
        
       if (($_POST['Orders'] < 0) || ($_POST['WorkInProgress'] < 0) || ($_POST['Complete'] < 0))
         return false;
       
        return true;
        
      case 'Add':
         if(strcmp($_POST['AddMM'], 'checked') == 0){
              if (($_POST['MMResponsible'] == "")  || ($_POST['MMPrefix'] == "") || ($_POST['MMCustomer'] == "") || ($_POST['MMCostManager'] == "") )
                 return false;
         }
          
        if (($_POST['Responsible'] == "") || ($_POST['CostManager2'] == "")|| ($_POST['CostManager3'] == "") || ($_POST['CostManager8'] == "") || ($_POST['Pastel'] == "") || ($_POST['Customer'] == "") || ($_POST['Description'] == "") || ($_POST['Project'] == "") || ($_POST['ProjectType'] == ""))
          return false;
        break;
      case 'Update':
        if ($_POST['Details'] == "")
          return false;
        break;
      case 'View':
        if ($_POST['Project'] == "")
          return false;
          
        if ($_SESSION['cAuth'] & 64)
        {
          if (!(CheckDate($_POST['StartMonth'], $_POST['StartDay'], $_POST['StartYear'])) || !(CheckDate($_POST['EndMonth'], $_POST['EndDay'], $_POST['EndYear'])))
            return false;
          
          if (DatabaseDateLater(GetDatabaseDate($_POST['StartDay'], $_POST['StartMonth'], $_POST['StartYear']), GetDatabaseDate($_POST['EndDay'], $_POST['EndMonth'], $_POST['EndYear'])))
            return false;
        }
        break;
      case 'ViewSingle':
        if ($_POST['Project'] == "")
          return false;
        break;
        case 'ViewMapping':
            return true;
            break;
         case 'CostManagers':
            if (($_POST['CostManager'] == "") || ($_POST['CostManager1'] == "") || ($_POST['CostManager2'] == "") || ($_POST['CostManager3'] == "") || ($_POST['CostManager4'] == "") || ($_POST['CostManager5'] == "") || ($_POST['CostManager6'] == "") || ($_POST['CostManager7'] == "") || ($_POST['CostManager8'] == "") || ($_POST['CostManager9'] == "") || ($_POST['CostManager10'] == "") || ($_POST['CostManager12'] == ""))
                 return false; 
            else
                return true;
            break;
      default:
        return false;
        break;
    }
    
    return true;
  }


  function HandleMapping(){
      unset($_SESSION['ViewProjectMapping']);
  }

    function getProjectMappingCode(){
      $sqlString = "SELECT Project_Code as `S4` , (null) as `MM` ,  concat(Project_Pastel_Prefix,' -' ,Project_Description) as Project_NameS4 , '<not mapped>' as `Project_NameMM` FROM S4Admin.Project WHERE Project_Code NOT IN (SELECT Project_CodeS4 FROM S4Admin.ProjectMapping  WHERE Project_CodeS4 IS NOT NULL) 
                    UNION ALL
                    SELECT  (null) as `S4` , Project_Code as `MM` , '<not mapped>' as `Project_NameS4` ,concat(Project_Pastel_Prefix,' -' ,Project_Description) as Project_NameMM  FROM MMAdmin.Project WHERE Project_Code NOT IN (SELECT Project_CodeMM FROM S4Admin.ProjectMapping  WHERE Project_CodeMM IS NOT NULL) 
                    UNION ALL
                    SELECT Project_CodeS4 as `S4` , Project_CodeMM as `MM`, Project_NameS4,Project_NameMM FROM (SELECT * , 
                    IF(Project_CodeS4 IS NULL , NULL, 
                    (SELECT concat(Project_Pastel_Prefix,' -',Project_Description) FROM S4Admin.Project WHERE Project_Code =  Project_CodeS4)) as Project_NameS4,
                    IF(Project_CodeMM IS NULL , NULL, 
                    (SELECT concat(Project_Pastel_Prefix,' -' ,Project_Description) FROM MMAdmin.Project WHERE Project_Code =  Project_CodeMM)) as Project_NameMM  
                    FROM S4Admin.ProjectMapping) as s";

        $resultSet = ExecuteQuery($sqlString);
        $projects = array();
        while ($row = MySQL_Fetch_Array($resultSet)){
            array_push($projects,$row);
        }

        echo json_encode($projects);
    }

  //////////////////////////////////////////////////////////////////////////////
  // Handles the user's submission of information for a new project.          //
  //////////////////////////////////////////////////////////////////////////////
  function HandleAdd()
  {
    $_SESSION['AddProject'][0] = $_POST['Pastel'];
    $_SESSION['AddProject'][1] = $_POST['Description'];
    $_SESSION['AddProject'][2] = $_POST['Project'];
    $_SESSION['AddProject'][3] = $_POST['Responsible'];
    $_SESSION['AddProject'][4] = $_POST['Customer'];
    $_SESSION['AddProject'][5] = $_POST['PurchaseOrder'];
    $_SESSION['AddProject'][6] = $_POST['ProjectType'];
    $_SESSION['AddProject'][7] = $_POST['CostManager'];
    $_SESSION['AddProject'][8] = $_POST['MMproject'];
    $_SESSION['AddProject'][9] = $_POST['AddMM'];
    $_SESSION['AddProject'][10] = $_POST['MMPrefix'];
    $_SESSION['AddProject'][11] = $_POST['MMCustomer'];
    $_SESSION['AddProject'][12] = $_POST['MMPurchaseOrder'];
    $_SESSION['AddProject'][13] = $_POST['MMResponsible'];
    $_SESSION['AddProject'][14] = $_POST['MMCostManager'];
    $_SESSION['AddProject'][15] = $_POST['MMDescription'];
    $_SESSION['AddProject'][16] = $_POST['CostManager1'];
    $_SESSION['AddProject'][17] = $_POST['CostManager2'];
    $_SESSION['AddProject'][18] = $_POST['CostManager3'];
    $_SESSION['AddProject'][19] = $_POST['CostManager4'];
    $_SESSION['AddProject'][20] = $_POST['CostManager5'];
    $_SESSION['AddProject'][21] = $_POST['CostManager6'];
    $_SESSION['AddProject'][22] = $_POST['CostManager7'];
    $_SESSION['AddProject'][23] = $_POST['CostManager8'];
    $_SESSION['AddProject'][24] = $_POST['CostManager9'];
    $_SESSION['AddProject'][25] = $_POST['CostManager10'];
    $_SESSION['AddProject'][26] = $_POST['CostManager12'];
    $_SESSION['AddProject'][27] = $_POST['ResponsibleS4A'];
    
         
    switch ($_POST['Submit'])
    {
      case 'Cancel':
        Session_Unregister('AddProject');
        break;
      case 'Submit':
        if (CheckFields())
        {

            // update empty to be person andrew
            if(strcmp($_POST['CostManager'], "") == 0){
                $_POST['CostManager'] = "015"; 
            }
            
             if(strcmp($_POST['CostManager1'], "") == 0){
                $_POST['CostManager1'] = "015"; 
            }
            
             if(strcmp($_POST['CostManager4'], "") == 0){
                $_POST['CostManager4'] = "015"; 
            }
            
             if(strcmp($_POST['CostManager5'], "") == 0){
                $_POST['CostManager5'] = "015"; 
            }
            
             if(strcmp($_POST['CostManager6'], "") == 0){
                $_POST['CostManager6'] = "015"; 
            }
            
             if(strcmp($_POST['CostManager7'], "") == 0){
                $_POST['CostManager7'] = "015"; 
            }
            
              if(strcmp($_POST['CostManager8'], "") == 0){
                $_POST['CostManager8'] = "015"; 
            }
            
              if(strcmp($_POST['CostManager9'], "") == 0){
                $_POST['CostManager9'] = "015"; 
            }
            
              if(strcmp($_POST['CostManager10'], "") == 0){
                $_POST['CostManager10'] = "015"; 
            }
            
              if(strcmp($_POST['CostManager12'], "") == 0){
                $_POST['CostManager12'] = "015"; 
            }
            
         $ProjectMM_code =   $_POST['MMproject']; 
       
          $date = Date('Y-m-d H:i:s');
          if (ExecuteQuery('INSERT INTO Project VALUES("", "'.$_POST['Project'].'", "'.$_POST['Customer'].'", "", "'.$date.'", "'.$date.'", "'.$_POST['PurchaseOrder'].'", "'.$_POST['Pastel'].'", "'.$_POST['Description'].'", "'.$_POST['Responsible'].'", "'.$_POST['ProjectType'].'", "0", "0", "0", "0", "0","'.$_POST['CostManager'].'","'.$_POST['CostManager1'].'","'.$_POST['CostManager2'].'","'.$_POST['CostManager3'].'","'.$_POST['CostManager4'].'","'.$_POST['CostManager5'].'","'.$_POST['CostManager6'].'","'.$_POST['CostManager7'].'","'.$_POST['CostManager8'].'","'.$_POST['CostManager9'].'","'.$_POST['CostManager10'].'","'.$_POST['CostManager12'].'","'.$_POST['ResponsibleS4A'].'" )'))
    	    {
               $ProjectS4_code = mysql_insert_id();
              
              // add to autonet as well
               $pass = true;
              if(strcmp($_POST['AddMM'], 'checked') == 0){
                  
                if (ExecuteQuery('INSERT INTO MMAdmin.Project VALUES("", "'.$_POST['Project'].'", "'.$_POST['MMCustomer'].'", "", "'.$date.'", "'.$date.'", "'.$_POST['MMPurchaseOrder'].'", "'.$_POST['MMPrefix'].'", "'.$_POST['Description'].'", "'.$_POST['MMResponsible'].'", "'.$_POST['ProjectType'].'", "0", "0", "0","'.$_POST['MMCostManager'].'" ,"'.$_POST['MMCostManager'].'" ,"'.$_POST['MMCostManager'].'" ,"'.$_POST['MMCostManager'].'" ,"'.$_POST['MMCostManager'].'" ,"'.$_POST['MMCostManager'].'" ,"'.$_POST['MMCostManager'].'" ,"'.$_POST['MMCostManager'].'" ,"'.$_POST['MMCostManager'].'" ,"'.$_POST['MMCostManager'].'" ,"'.$_POST['MMCostManager'].'" ,"'.$_POST['MMCostManager'].'")')){
    	   
                  
                $ProjectMM_code = mysql_insert_id();
                $rowProj = MySQL_Fetch_Array(ExecuteQuery('SELECT *,
                (SELECT concat(Staff_First_Name," ", Staff_Last_Name) as CM FROM MMAdmin.Staff WHERE Staff_Code = Project_CostManager) as CM,
                (SELECT concat(Staff_First_Name," ", Staff_Last_Name) as CM1 FROM MMAdmin.Staff WHERE Staff_Code = Project_CostManager1) as CM1,
                (SELECT concat(Staff_First_Name," ", Staff_Last_Name) as CM2 FROM MMAdmin.Staff WHERE Staff_Code = Project_CostManager2) as CM2,
                (SELECT concat(Staff_First_Name," ", Staff_Last_Name) as CM3 FROM MMAdmin.Staff WHERE Staff_Code = Project_CostManager3) as CM3,
                (SELECT concat(Staff_First_Name," ", Staff_Last_Name) as CM4 FROM MMAdmin.Staff WHERE Staff_Code = Project_CostManager4) as CM4,
                (SELECT concat(Staff_First_Name," ", Staff_Last_Name) as CM5 FROM MMAdmin.Staff WHERE Staff_Code = Project_CostManager5) as CM5,
                (SELECT concat(Staff_First_Name," ", Staff_Last_Name) as CM6 FROM MMAdmin.Staff WHERE Staff_Code = Project_CostManager6) as CM6,
                (SELECT concat(Staff_First_Name," ", Staff_Last_Name) as CM7 FROM MMAdmin.Staff WHERE Staff_Code = Project_CostManager7) as CM7,
                (SELECT concat(Staff_First_Name," ", Staff_Last_Name) as CM8 FROM MMAdmin.Staff WHERE Staff_Code = Project_CostManager8) as CM8,
                (SELECT concat(Staff_First_Name," ", Staff_Last_Name) as CM9 FROM MMAdmin.Staff WHERE Staff_Code = Project_CostManager9) as CM9,
                (SELECT concat(Staff_First_Name," ", Staff_Last_Name) as CM10 FROM MMAdmin.Staff WHERE Staff_Code = Project_CostManager10) as CM10,
                (SELECT concat(Staff_First_Name," ", Staff_Last_Name) as CM12 FROM MMAdmin.Staff WHERE Staff_Code = Project_CostManager12) as CM12  
                FROM MMAdmin.Project WHERE Project_Code =' .$ProjectMM_code.'  ORDER BY Project_Code DESC'));
                    $rowStaff = MySQL_Fetch_Array(ExecuteQuery('SELECT Staff_First_Name, Staff_Last_Name, Staff_email FROM MMAdmin.Staff WHERE Staff_Code = '.$_POST['MMResponsible'].''));
                    $rowCM = MySQL_Fetch_Array(ExecuteQuery('SELECT Staff_First_Name, Staff_Last_Name, Staff_email FROM MMAdmin.Staff WHERE Staff_Code = '.$_POST['MMCostManager'].''));
                    $rowCust = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM MMAdmin.Customer WHERE Customer_Code = "'.$_POST['MMCustomer'].'"'));
                    $rowPO = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM MMAdmin.PurchaseOrder WHERE PurchaseOrder_ID = "'.$_POST['MMPurchaseOrder'].'"'));
                    //add to Project Purchase order table
                    ExecuteQuery('INSERT INTO MMAdmin.Project_PurchaseOrder  values("'.$rowProj['Project_Code'].'", "'.$_POST['MMPurchaseOrder'].'")');
            
                  
                    $content='PROJECT:      '.$rowProj['Project_Pastel_Prefix'].' - '.$rowProj['Project_Description'].Chr(10).
                    'PURCHASE ORDER NUMBER: '.$rowPO['PurchaseOrder_Number'].Chr(10).
                    'RESPONSIBLE:           '.$rowStaff['Staff_First_Name'].' '.$rowStaff['Staff_Last_Name'].Chr(10).
                    'COST MANAGERS:         '.Chr(10).
                    'Unassigned:            '.$rowProj['CM'].Chr(10). 
                    'Admin:                 '.$rowProj['CM1'].Chr(10). 
                    'Electrical:            '.$rowProj['CM2'].Chr(10). 
                    'Mechanical:            '.$rowProj['CM3'].Chr(10).         
                    'IT:                    '.$rowProj['CM4'].Chr(10). 
                    'HR:                    '.$rowProj['CM5'].Chr(10). 
                    'Management:            '.$rowProj['CM6'].Chr(10).         
                    'Training:              '.$rowProj['CM7'].Chr(10). 
                    'Manufacturing:         '.$rowProj['CM8'].Chr(10). 
                    'Sales and Marketing:   '.$rowProj['CM9'].Chr(10).      
                    'Software:              '.$rowProj['CM10'].Chr(10).   
                    'Design:                '.$rowProj['CM12'].Chr(10).    
                    'CUSTOMER:              '.$rowCust['Customer_Name'].Chr(10).
                    'DESCRIPTION:           '.$_POST['Description'];
            
                    $email = 'You have been made responsible for a project. The details of the project are as follows:'.Chr(10).$content;
                    $emailCM = 'You have been made a cost manager for a project. The details of the project are as follows:'.Chr(10).$content;

                    
                    $contentHtml= '<BR /><BR />
                    <TABLE border=0>
                      <TR><TD><B>Project:</B></TD><TD>'.$rowProj['Project_Pastel_Prefix'].' - '.$rowProj['Project_Description'].'</TD></TR>
                      <TR><TD><B>Purchase Order Number:</B></TD><TD>'.$rowPO['PurchaseOrder_Number'].'</TD></TR>
                      <TR><TD><B>Responsible:</B></TD><TD>'.$rowStaff['Staff_First_Name'].' '.$rowStaff['Staff_Last_Name'].'</TD></TR>
                      <TR><TD><B>Cost Managers:</B></TD><TD></TD></TR>
                      
                       <TR><TD><B>Unassigned:</B></TD><TD>'.$rowProj['CM'].'</TD></TR>
                       <TR><TD><B>Admin:</B></TD><TD>'.$rowProj['CM1'].'</TD></TR>
                       <TR><TD><B>Electrical:</B></TD><TD>'.$rowProj['CM2'].'</TD></TR>
                       <TR><TD><B>Mechanical:</B></TD><TD>'.$rowProj['CM3'].'</TD></TR>
                       <TR><TD><B>IT:</B></TD><TD>'.$rowProj['CM4'].'</TD></TR>
                       <TR><TD><B>HR:</B></TD><TD>'.$rowProj['CM5'].'</TD></TR>
                       <TR><TD><B>Management:</B></TD><TD>'.$rowProj['CM6'].'</TD></TR>
                       <TR><TD><B>Training:</B></TD><TD>'.$rowProj['CM7'].'</TD></TR>
                       <TR><TD><B>Manufacturing:</B></TD><TD>'.$rowProj['CM8'].'</TD></TR>
                       <TR><TD><B>Sales and Marketing:</B></TD><TD>'.$rowProj['CM9'].'</TD></TR>
                       <TR><TD><B>Software:</B></TD><TD>'.$rowProj['CM10'].'</TD></TR>
                       <TR><TD><B>Design:</B></TD><TD>'.$rowProj['CM12'].'</TD></TR>
     
                      <TR><TD><B>Customer:</B></TD><TD>'.$rowCust['Customer_Name'].'</TD></TR>
                      <TR><TD><B>Description:</B></TD><TD>'.$_POST['Description'].'</TD></TR>
                    </TABLE>';
                    
                    $html = 'You have been made responsible for a project. The details of the project are as follows:'.$contentHtml;
                    $htmlCM = 'You have been made a cost manager for a project. The details of the project are as follows:'.$contentHtml;

                    SendMailHTML($rowStaff['Staff_email'], 'Project Added - '.$rowCust['Customer_Name'].' - '.$rowProj['Project_Code'], $email, $html);
                    SendMailHTML($rowCM['Staff_email'], 'Project Added - '.$rowCust['Customer_Name'].' - '.$rowProj['Project_Code'], $emailCM, $htmlCM);
                    CCSomeone($rowStaff['Staff_email'], 'stefan@s4integration.co.za', $rowStaff['Staff_email'].'Project Added - '.$rowCust['Customer_Name'].' - '.$rowProj['Project_Code'], $email, $html);

                       $row = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = '.$_SESSION['cUID'].''));
              
                        $email = 'Project details have been added. The details are as follows:'.Chr(10).
                       'PROJECT:               '.$_POST['Project'].Chr(10).
                       'PASTEL PREFIX:         '.$_POST['MMPastel'].Chr(10).
                       'ADDED BY:              '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].Chr(10).Chr(10).
                       'Please review the new details to ensure that everything is in order.';
              
                        $html = 'Project details have been added. The details are as follows:
                        <BR /><BR />
                        <TABLE border=0>
                          <TR><TD><B>Project:</B></TD><TD>'.$_POST['Project'].'</TD></TR>
                          <TR><TD><B>Pastel Prefix:</B></TD><TD>'.$_POST['MMPastel'].'</TD></TR>
                          <TR><TD><B>Added By:</B></TD><TD>'.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].'</TD></TR>
                        </TABLE>
                        <BR />
                        Please review the new details to ensure that everything is in order.
                        <BR /><BR />';
              
                        CCSomeone($row['Staff_email'], 'cherie@s4integration.co.za', $row['Staff_email'].'Project Added - '.$rowCust['Customer_Name'].' - '.$rowProj['Project_Code'], $email, $html);
                
                    if ($_POST['MMPurchaseOrder'] != "")
                    {
                        ExecuteQuery('UPDATE MMAdmin.PurchaseOrder SET PurchaseOrder_Complete = "0" WHERE PurchaseOrder_ID = "'.$_POST['MMPurchaseOrder'].'"');
                    }
                }    
              
                else{
                    $pass = false;
                }
                
               }  
            
              if($pass){
  
      	        $rowProj = MySQL_Fetch_Array(ExecuteQuery('SELECT *,
                (SELECT concat(Staff_First_Name," ", Staff_Last_Name) as CM FROM Staff WHERE Staff_Code = Project_CostManager) as CM,
                (SELECT concat(Staff_First_Name," ", Staff_Last_Name) as CM1 FROM Staff WHERE Staff_Code = Project_CostManager1) as CM1,
                (SELECT concat(Staff_First_Name," ", Staff_Last_Name) as CM2 FROM Staff WHERE Staff_Code = Project_CostManager2) as CM2,
                (SELECT concat(Staff_First_Name," ", Staff_Last_Name) as CM3 FROM Staff WHERE Staff_Code = Project_CostManager3) as CM3,
                (SELECT concat(Staff_First_Name," ", Staff_Last_Name) as CM4 FROM Staff WHERE Staff_Code = Project_CostManager4) as CM4,
                (SELECT concat(Staff_First_Name," ", Staff_Last_Name) as CM5 FROM Staff WHERE Staff_Code = Project_CostManager5) as CM5,
                (SELECT concat(Staff_First_Name," ", Staff_Last_Name) as CM6 FROM Staff WHERE Staff_Code = Project_CostManager6) as CM6,
                (SELECT concat(Staff_First_Name," ", Staff_Last_Name) as CM7 FROM Staff WHERE Staff_Code = Project_CostManager7) as CM7,
                (SELECT concat(Staff_First_Name," ", Staff_Last_Name) as CM8 FROM Staff WHERE Staff_Code = Project_CostManager8) as CM8,
                (SELECT concat(Staff_First_Name," ", Staff_Last_Name) as CM9 FROM Staff WHERE Staff_Code = Project_CostManager9) as CM9,
                (SELECT concat(Staff_First_Name," ", Staff_Last_Name) as CM10 FROM Staff WHERE Staff_Code = Project_CostManager10) as CM10,
                (SELECT concat(Staff_First_Name," ", Staff_Last_Name) as CM12 FROM Staff WHERE Staff_Code = Project_CostManager12) as CM12  
                FROM Project WHERE Project_Description = "'.$_POST['Project'].'" AND Project_Pastel_Prefix = "'.$_POST['Pastel'].'" ORDER BY Project_Code DESC'));
            $rowStaff = MySQL_Fetch_Array(ExecuteQuery('SELECT Staff_First_Name, Staff_Last_Name, Staff_email FROM Staff WHERE Staff_Code = '.$_POST['Responsible'].''));
            $rowStaffS4A = MySQL_Fetch_Array(ExecuteQuery('SELECT Staff_First_Name, Staff_Last_Name, Staff_email FROM Staff WHERE Staff_Code = '.$_POST['ResponsibleS4A'].''));
            $rowCM = MySQL_Fetch_Array(ExecuteQuery('SELECT Staff_First_Name, Staff_Last_Name, Staff_email FROM Staff WHERE Staff_Code = '.$_POST['CostManager'].''));
            $rowCust = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Customer WHERE Customer_Code = "'.$_POST['Customer'].'"'));
            $rowPO = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM PurchaseOrder WHERE PurchaseOrder_ID = "'.$_POST['PurchaseOrder'].'"'));
            //add to Project Purchase order table
            ExecuteQuery('INSERT INTO Project_PurchaseOrder  values("'.$rowProj['Project_Code'].'", "'.$_POST['PurchaseOrder'].'")');
            

            $content='PROJECT:               '.$rowProj['Project_Pastel_Prefix'].' - '.$rowProj['Project_Description'].Chr(10).
                     'PURCHASE ORDER NUMBER: '.$rowPO['PurchaseOrder_Number'].Chr(10).
                     'PMS4I:           '.$rowStaff['Staff_First_Name'].' '.$rowStaff['Staff_Last_Name'].Chr(10).
                     'PMS4A:           '.$rowStaffS4A['Staff_First_Name'].' '.$rowStaffS4A['Staff_Last_Name'].Chr(10).
                    'COST MANAGERS:         '.Chr(10).
                    'Unassigned:            '.$rowProj['CM'].Chr(10). 
                    'Admin:                 '.$rowProj['CM1'].Chr(10). 
                    'Electrical:            '.$rowProj['CM2'].Chr(10). 
                    'Mechanical:            '.$rowProj['CM3'].Chr(10).         
                    'IT:                    '.$rowProj['CM4'].Chr(10). 
                    'HR:                    '.$rowProj['CM5'].Chr(10). 
                    'Management:            '.$rowProj['CM6'].Chr(10).         
                    'Training:              '.$rowProj['CM7'].Chr(10). 
                    'Manufacturing:         '.$rowProj['CM8'].Chr(10). 
                    'Sales and Marketing:   '.$rowProj['CM9'].Chr(10).      
                    'Software:              '.$rowProj['CM10'].Chr(10).   
                    'Design:                '.$rowProj['CM12'].Chr(10).    
                     'CUSTOMER:              '.$rowCust['Customer_Name'].Chr(10).
                     'DESCRIPTION:           '.$_POST['Description'];
            
            $email = 'You have been made responsible for a project. The details of the project are as follows:'.Chr(10).$content;
            $emailCM = 'You have been made a cost manager for a project. The details of the project are as follows:'.Chr(10).$content;
            
       $contentHtml= '<BR /><BR />
                    <TABLE border=0>
                      <TR><TD><B>Project:</B></TD><TD>'.$rowProj['Project_Pastel_Prefix'].' - '.$rowProj['Project_Description'].'</TD></TR>
                      <TR><TD><B>Purchase Order Number:</B></TD><TD>'.$rowPO['PurchaseOrder_Number'].'</TD></TR>
                      <TR><TD><B>PMS4I:</B></TD><TD>'.$rowStaff['Staff_First_Name'].' '.$rowStaff['Staff_Last_Name'].'</TD></TR>
                      <TR><TD><B>PMS4A:</B></TD><TD>'.$rowStaffS4A['Staff_First_Name'].' '.$rowStaffS4A['Staff_Last_Name'].'</TD></TR>    
                      <TR><TD><B>Cost Managers:</B></TD><TD></TD></TR>
                      
                       <TR><TD><B>Unassigned:</B></TD><TD>'.$rowProj['CM'].'</TD></TR>
                       <TR><TD><B>Admin:</B></TD><TD>'.$rowProj['CM1'].'</TD></TR>
                       <TR><TD><B>Electrical:</B></TD><TD>'.$rowProj['CM2'].'</TD></TR>
                       <TR><TD><B>Mechanical:</B></TD><TD>'.$rowProj['CM3'].'</TD></TR>
                       <TR><TD><B>IT:</B></TD><TD>'.$rowProj['CM4'].'</TD></TR>
                       <TR><TD><B>HR:</B></TD><TD>'.$rowProj['CM5'].'</TD></TR>
                       <TR><TD><B>Management:</B></TD><TD>'.$rowProj['CM6'].'</TD></TR>
                       <TR><TD><B>Training:</B></TD><TD>'.$rowProj['CM7'].'</TD></TR>
                       <TR><TD><B>Manufacturing:</B></TD><TD>'.$rowProj['CM8'].'</TD></TR>
                       <TR><TD><B>Sales and Marketing:</B></TD><TD>'.$rowProj['CM9'].'</TD></TR>
                       <TR><TD><B>Software:</B></TD><TD>'.$rowProj['CM10'].'</TD></TR>
                       <TR><TD><B>Design:</B></TD><TD>'.$rowProj['CM12'].'</TD></TR>

                      <TR><TD><B>Customer:</B></TD><TD>'.$rowCust['Customer_Name'].'</TD></TR>
                      <TR><TD><B>Description:</B></TD><TD>'.$_POST['Description'].'</TD></TR>
                    </TABLE>';
            $html = 'You have been made responsible for a project. The details of the project are as follows:'.$contentHtml;
            $htmlCM = 'You have been made a cost manager for a project. The details of the project are as follows:'.$contentHtml;
            
            SendMailHTML($rowStaff['Staff_email'], 'Project Added - '.$rowCust['Customer_Name'].' - '.$rowProj['Project_Code'], $email, $html);
            SendMailHTML($rowStaffS4A['Staff_email'], 'Project Added - '.$rowCust['Customer_Name'].' - '.$rowProj['Project_Code'], $email, $html);
            
            
             // LOOP TO ALL COSTMANAGERS
            $sqlAllEmails = 'SELECT (SELECT Staff_email as CM FROM Staff WHERE Staff_Code = Project_CostManager) as CM,
                (SELECT Staff_email as CM1 FROM Staff WHERE Staff_Code = Project_CostManager1) as CM1,
                (SELECT Staff_email as CM2 FROM Staff WHERE Staff_Code = Project_CostManager2) as CM2,
                (SELECT Staff_email as CM3 FROM Staff WHERE Staff_Code = Project_CostManager3) as CM3,
                (SELECT Staff_email  as CM4 FROM Staff WHERE Staff_Code = Project_CostManager4) as CM4,
                (SELECT Staff_email as CM5 FROM Staff WHERE Staff_Code = Project_CostManager5) as CM5,
                (SELECT Staff_email  as CM6 FROM Staff WHERE Staff_Code = Project_CostManager6) as CM6,
                (SELECT Staff_email as CM7 FROM Staff WHERE Staff_Code = Project_CostManager7) as CM7,
                (SELECT Staff_email as CM8 FROM Staff WHERE Staff_Code = Project_CostManager8) as CM8,
                (SELECT Staff_email as CM9 FROM Staff WHERE Staff_Code = Project_CostManager9) as CM9,
                (SELECT Staff_email as CM10 FROM Staff WHERE Staff_Code = Project_CostManager10) as CM10,
                (SELECT Staff_email as CM12 FROM Staff WHERE Staff_Code = Project_CostManager12) as CM12,
                (SELECT Staff_email as CM12 FROM Staff WHERE Staff_Code = "310") as Shawn ,
                (SELECT Staff_email as CM12 FROM Staff WHERE Staff_Code = "030") as Stefan
                FROM Project WHERE Project_Code = "'.$rowProj['Project_Code'].'"';
     
            $resultEmails = ExecuteQuery($sqlAllEmails); 
            $emails = array();
             while($row = MySQL_Fetch_Array($resultEmails)){
                // array_push($emails, $row['CM']);
               //  array_push($emails, $row['CM1']);
                  array_push($emails, $row['CM2']);
                  array_push($emails, $row['CM3']);
               //  array_push($emails, $row['CM4']);
                // array_push($emails, $row['CM5']);
            //     array_push($emails, $row['CM6']);
             //    array_push($emails, $row['CM7']);
                 array_push($emails, $row['CM8']); 
              //   array_push($emails, $row['CM9']); 
              //   array_push($emails, $row['CM10']);
              //   array_push($emails, $row['CM12']);
                  array_push($emails, $row['Shawn']);
                   array_push($emails, $row['Stefan']);
             }
            
            $emails = array_unique($emails);
            
            foreach ($emails as $email){
                
                if(strcmp($email, "") != 0){
         
                    SendMailHTML($email, 'Project Added - '.$rowCust['Customer_Name'].' - '.$rowProj['Project_Code'], $emailCM, $htmlCM);
                }

            }
  
            
            CCSomeone($rowStaff['Staff_email'], 'stefan@s4integration.co.za', 'Project Added - '.$rowCust['Customer_Name'].' - '.$rowProj['Project_Code'], $email, $html);
            
            //if ($_SESSION['cUID'] != '32')
           // {
              $row = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = '.$_SESSION['cUID'].''));
              
              $email = 'Project details have been added. The details are as follows:'.Chr(10).
                       'PROJECT:               '.$_POST['Project'].Chr(10).
                       'PASTEL PREFIX:         '.$_POST['Pastel'].Chr(10).
                       'ADDED BY:              '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].Chr(10).Chr(10).
                       'Please review the new details to ensure that everything is in order.';
              
              $html = 'Project details have been added. The details are as follows:
                      <BR /><BR />
                      <TABLE border=0>
                        <TR><TD><B>Project:</B></TD><TD>'.$_POST['Project'].'</TD></TR>
                        <TR><TD><B>Pastel Prefix:</B></TD><TD>'.$_POST['Pastel'].'</TD></TR>
                        <TR><TD><B>Added By:</B></TD><TD>'.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].'</TD></TR>
                      </TABLE>
                      <BR />
                      Please review the new details to ensure that everything is in order.
                      <BR /><BR />';
              
              CCSomeone($row['Staff_email'], 'cherie@s4integration.co.za', 'Project Added - '.$rowCust['Customer_Name'].' - '.$rowProj['Project_Code'], $email, $html);
              //CCSomeone($row['Staff_email'], 'stefan@s4integration.co.za', 'Project Added', $email, $html);
            //}
             
              
 
            if ($_POST['PurchaseOrder'] != "")
            {
              ExecuteQuery('UPDATE PurchaseOrder SET PurchaseOrder_Complete = "0" WHERE PurchaseOrder_ID = "'.$_POST['PurchaseOrder'].'"');
            }
            
             // add into map  s4 mm
            if (strcmp($ProjectMM_code , "") != 0){
                    if (ExecuteQuery('INSERT INTO S4Admin.ProjectMapping (`Project_CodeS4`, `Project_CodeMM`) VALUES('.$ProjectS4_code.', '.$ProjectMM_code.')'))
                    {
                         $_SESSION['ProjectSuccess'] = 'geh!';
                         Session_Unregister('AddProject');
                    }
                    else{
                        $_SESSION['ProjectFail'] = 'geh!';
                    }
            }
            
            $_SESSION['ProjectSuccess'] = 'geh!';
            Session_Unregister('AddProject');
            }else{
                $_SESSION['ProjectFail'] = 'geh!';
            }
          } else
    	      $_SESSION['ProjectFail'] = 'geh!';
        } else
          $_SESSION['ProjectIncomplete'] = 'geh!';
        break;
      default:
        break;
    }
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Handles the user's submission of modified information for a project.     //
  //////////////////////////////////////////////////////////////////////////////
  function HandleEdit()
  {
    $_SESSION['EditProject'][1] = $_POST['Pastel'];
    $_SESSION['EditProject'][2] = $_POST['Description'];
    $_SESSION['EditProject'][3] = $_POST['Project'];
    $_SESSION['EditProject'][4] = $_POST['Responsible'];
    $_SESSION['EditProject'][5] = $_POST['Customer'];
    $_SESSION['EditProject'][6] = $_POST['PurchaseOrder'];
    $_SESSION['EditProject'][7] = $_POST['Closed'];
    $_SESSION['EditProject'][8] = $_POST['Orders'];
    $_SESSION['EditProject'][9] = $_POST['WorkInProgress'];
    $_SESSION['EditProject'][10] = $_POST['Complete'];
    $_SESSION['EditProject'][11] = $_POST['ProjectType'];
    $_SESSION['EditProject'][14] = $_POST['CostManager'];
    $_SESSION['EditProject'][15] = $_POST['MMproject'];
    
    $_SESSION['EditProject'][16] = $_POST['CostManager1'];
    $_SESSION['EditProject'][17] = $_POST['CostManager2'];
    $_SESSION['EditProject'][18] = $_POST['CostManager3'];
    $_SESSION['EditProject'][19] = $_POST['CostManager4'];
    $_SESSION['EditProject'][20] = $_POST['CostManager5'];
    $_SESSION['EditProject'][21] = $_POST['CostManager6'];
    $_SESSION['EditProject'][22] = $_POST['CostManager7'];
    $_SESSION['EditProject'][23] = $_POST['CostManager8'];
    $_SESSION['EditProject'][24] = $_POST['CostManager9'];
    $_SESSION['EditProject'][25] = $_POST['CostManager10'];
    $_SESSION['EditProject'][26] = $_POST['CostManager12'];
    
    $_SESSION['EditProject'][27] = $_POST['ResponsibleS4A'];
   
    
    switch ($_POST['Submit'])
    {
      case 'Cancel':
        Session_Unregister('EditProject');
        break;
      case 'Submit':
        if (CheckFields())
        {
 
             // update empty to be person responsible
            if(strcmp($_POST['CostManager'], "") == 0){
                $_POST['CostManager'] = "015"; 
            }
            
             if(strcmp($_POST['CostManager1'], "") == 0){
                $_POST['CostManager1'] = "015"; 
            }
            
             if(strcmp($_POST['CostManager4'], "") == 0){
                $_POST['CostManager4'] = "015"; 
            }
            
             if(strcmp($_POST['CostManager5'], "") == 0){
                $_POST['CostManager5'] = "015"; 
            }
            
             if(strcmp($_POST['CostManager6'], "") == 0){
                $_POST['CostManager6'] = "015"; 
            }
            
             if(strcmp($_POST['CostManager7'], "") == 0){
                $_POST['CostManager7'] = "015"; 
            }
            
              if(strcmp($_POST['CostManager8'], "") == 0){
                $_POST['CostManager8'] = "015"; 
            }
            
              if(strcmp($_POST['CostManager9'], "") == 0){
                $_POST['CostManager9'] = "015"; 
            }
            
              if(strcmp($_POST['CostManager10'], "") == 0){
                $_POST['CostManager10'] = "015"; 
            }
            
              if(strcmp($_POST['CostManager12'], "") == 0){
                $_POST['CostManager12'] = "015"; 
            }
            
          $row = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Project WHERE Project_Code = "'.$_SESSION['EditProject'][0].'"'));
          $previousNumber = $row['Project_Description'];
          $previousResponsible = $row['Project_Responsible'];
          $previousResponsibleS4A = $row['Project_ResponsibleS4A'];
          $previousCM = $row['Project_CostManager'];
          
          
          $sqlAllEmails = 'SELECT (SELECT Staff_email as CM FROM Staff WHERE Staff_Code = Project_CostManager) as CM,
                (SELECT Staff_email as CM1 FROM Staff WHERE Staff_Code = Project_CostManager1) as CM1,
                (SELECT Staff_email as CM2 FROM Staff WHERE Staff_Code = Project_CostManager2) as CM2,
                (SELECT Staff_email as CM3 FROM Staff WHERE Staff_Code = Project_CostManager3) as CM3,
                (SELECT Staff_email  as CM4 FROM Staff WHERE Staff_Code = Project_CostManager4) as CM4,
                (SELECT Staff_email as CM5 FROM Staff WHERE Staff_Code = Project_CostManager5) as CM5,
                (SELECT Staff_email  as CM6 FROM Staff WHERE Staff_Code = Project_CostManager6) as CM6,
                (SELECT Staff_email as CM7 FROM Staff WHERE Staff_Code = Project_CostManager7) as CM7,
                (SELECT Staff_email as CM8 FROM Staff WHERE Staff_Code = Project_CostManager8) as CM8,
                (SELECT Staff_email as CM9 FROM Staff WHERE Staff_Code = Project_CostManager9) as CM9,
                (SELECT Staff_email as CM10 FROM Staff WHERE Staff_Code = Project_CostManager10) as CM10,
                (SELECT Staff_email as CM12 FROM Staff WHERE Staff_Code = Project_CostManager12) as CM12  
                FROM Project WHERE Project_Code = "'.$rowProj['Project_Code'].'"';
     
            $resultEmails = ExecuteQuery($sqlAllEmails); 
            $emails = array();
             while($rowe = MySQL_Fetch_Array($resultEmails)){
                 //array_push($emails, $rowe['CM']);
                 //array_push($emails, $rowe['CM1']);
                 array_push($emails, $rowe['CM2']);
                 array_push($emails, $rowe['CM3']);
               //  array_push($emails, $rowe['CM4']);
                // array_push($emails, $rowe['CM5']);
                // array_push($emails, $rowe['CM6']);
                // array_push($emails, $rowe['CM7']);
                array_push($emails, $rowe['CM8']); 
                // array_push($emails, $rowe['CM9']); 
               //  array_push($emails, $rowe['CM10']);
                // array_push($emails, $rowe['CM12']);
             }
            
          
          
          if(ExecuteQuery('DELETE FROM Project_PurchaseOrder WHERE project_id = "'.$_SESSION['EditProject'][0].'"'))
          {
    
	      $purchase_orders= explode(",", $_POST['allvalues']);
              $purchaseOrder_Number= $_POST['allPurchaseOrderNumbers'];
     
	      if ($purchase_orders){
	          foreach ($purchase_orders as $order)
                    {
                       ExecuteQuery('INSERT INTO Project_PurchaseOrder  values("'.$_SESSION['EditProject'][0].'", "'.$order.'")');
                    }
	         }
          }
          
          
           if(ExecuteQuery('DELETE FROM S4Admin.ProjectMapping WHERE Project_CodeS4 = "'.$_SESSION['EditProject'][0].'"'))
          {
             if($_POST['MMproject'] !== ""){
                if (!ExecuteQuery('INSERT INTO S4Admin.ProjectMapping (`Project_CodeS4`, `Project_CodeMM`) VALUES('.$_SESSION['EditProject'][0].', '.$_POST['MMproject'].')'))
                {
                     $_SESSION['ProjectFail'] = 'geh!';
                }  
             }
          }
          
          if (ExecuteQuery('UPDATE Project SET Project_Description = "'.$_POST['Project'].'", Project_Pastel_Prefix = "'.$_POST['Pastel'].'", Project_Customer = "'.$_POST['Customer'].'", Project_Long_Description = "'.$_POST['Description'].'", Project_Responsible = "'.$_POST['Responsible'].'", Project_ResponsibleS4A = "'.$_POST['ResponsibleS4A'].'", Project_Closed = "'.$_POST['Closed'].'", Project_OrdersPlaced = "'.$_POST['Orders'].'", Project_WorkInProgress = "'.$_POST['WorkInProgress'].'", Project_Complete = "'.$_POST['Complete'].'", Project_Type = "'.$_POST['ProjectType'].'", Project_DateTime_Updated = "'.Date('Y-m-d').'", Project_CostManager = "'.$_POST['CostManager'].'" , Project_CostManager1 = "'.$_POST['CostManager1'].'" , Project_CostManager2 = "'.$_POST['CostManager2'].'", Project_CostManager3 = "'.$_POST['CostManager3'].'" , Project_CostManager4 = "'.$_POST['CostManager4'].'" , Project_CostManager5 = "'.$_POST['CostManager5'].'" , Project_CostManager6 = "'.$_POST['CostManager6'].'" , Project_CostManager7 = "'.$_POST['CostManager7'].'" , Project_CostManager8 = "'.$_POST['CostManager8'].'" , Project_CostManager9 = "'.$_POST['CostManager9'].'" , Project_CostManager10 = "'.$_POST['CostManager10'].'" , Project_CostManager12 = "'.$_POST['CostManager12'].'" WHERE Project_Code = "'.$_SESSION['EditProject'][0].'"'))
    	    {
        	  $rowProj = MySQL_Fetch_Array(ExecuteQuery('SELECT *,
                (SELECT concat(Staff_First_Name," ", Staff_Last_Name) as CM FROM Staff WHERE Staff_Code = Project_CostManager) as CM,
                (SELECT concat(Staff_First_Name," ", Staff_Last_Name) as CM1 FROM Staff WHERE Staff_Code = Project_CostManager1) as CM1,
                (SELECT concat(Staff_First_Name," ", Staff_Last_Name) as CM2 FROM Staff WHERE Staff_Code = Project_CostManager2) as CM2,
                (SELECT concat(Staff_First_Name," ", Staff_Last_Name) as CM3 FROM Staff WHERE Staff_Code = Project_CostManager3) as CM3,
                (SELECT concat(Staff_First_Name," ", Staff_Last_Name) as CM4 FROM Staff WHERE Staff_Code = Project_CostManager4) as CM4,
                (SELECT concat(Staff_First_Name," ", Staff_Last_Name) as CM5 FROM Staff WHERE Staff_Code = Project_CostManager5) as CM5,
                (SELECT concat(Staff_First_Name," ", Staff_Last_Name) as CM6 FROM Staff WHERE Staff_Code = Project_CostManager6) as CM6,
                (SELECT concat(Staff_First_Name," ", Staff_Last_Name) as CM7 FROM Staff WHERE Staff_Code = Project_CostManager7) as CM7,
                (SELECT concat(Staff_First_Name," ", Staff_Last_Name) as CM8 FROM Staff WHERE Staff_Code = Project_CostManager8) as CM8,
                (SELECT concat(Staff_First_Name," ", Staff_Last_Name) as CM9 FROM Staff WHERE Staff_Code = Project_CostManager9) as CM9,
                (SELECT concat(Staff_First_Name," ", Staff_Last_Name) as CM10 FROM Staff WHERE Staff_Code = Project_CostManager10) as CM10,
                (SELECT concat(Staff_First_Name," ", Staff_Last_Name) as CM12 FROM Staff WHERE Staff_Code = Project_CostManager12) as CM12  
                FROM Project WHERE Project_Code = "'.$_SESSION['EditProject'][0].'"'));
            $rowStaff = MySQL_Fetch_Array(ExecuteQuery('SELECT Staff_First_Name, Staff_Last_Name, Staff_email FROM Staff WHERE Staff_Code = "'.$_POST['Responsible'].'"'));
            $rowStaffS4A = MySQL_Fetch_Array(ExecuteQuery('SELECT Staff_First_Name, Staff_Last_Name, Staff_email FROM Staff WHERE Staff_Code = "'.$_POST['ResponsibleS4A'].'"'));
            $rowCM = MySQL_Fetch_Array(ExecuteQuery('SELECT Staff_First_Name, Staff_Last_Name, Staff_email FROM Staff WHERE Staff_Code = "'.$_POST['CostManager'].'"'));
            $rowCust = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Customer WHERE Customer_Code = "'.$_POST['Customer'].'"'));
            $rowPO = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM PurchaseOrder WHERE PurchaseOrder_ID = "'.$_POST['PurchaseOrder'].'"'));
            
            if ($previousResponsible != $_POST['Responsible'])
            {
              $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT Staff_First_Name, Staff_Last_Name, Staff_email FROM Staff WHERE Staff_Code = "'.$previousResponsible.'"'));
              $email = 'You are no longer responsible for the following project: '.$previousNumber.'. This project has been handed over to '.$rowStaff['Staff_First_Name'].' '.$rowStaff['Staff_Last_Name'].'.';
              $html = 'You are no longer responsible for the following project: <B>'.$previousNumber.'</B>. This project has been handed over to <B>'.$rowStaff['Staff_First_Name'].' '.$rowStaff['Staff_Last_Name'].'</B>.';
               if(strcmp($rowTemp['Staff_email'], "") != 0){
                SendMailHTML($rowTemp['Staff_email'], 'Project Updated - '.$rowCust['Customer_Name'].' - '.$rowProj['Project_Code'], $email, $html);
               }
            }   
            
            if ($previousResponsibleS4A != $_POST['ResponsibleS4A'])
            {
              $rowTemp1 = MySQL_Fetch_Array(ExecuteQuery('SELECT Staff_First_Name, Staff_Last_Name, Staff_email FROM Staff WHERE Staff_Code = "'.$previousResponsibleS4A.'"'));
              $email = 'You are no longer responsible for the following project: '.$previousNumber.'. This project has been handed over to '.$rowStaffS4A['Staff_First_Name'].' '.$rowStaffS4A['Staff_Last_Name'].'.';
              $html = 'You are no longer responsible for the following project: <B>'.$previousNumber.'</B>. This project has been handed over to <B>'.$rowStaffS4A['Staff_First_Name'].' '.$rowStaffS4A['Staff_Last_Name'].'</B>.';
               if(strcmp($rowTemp1['Staff_email'], "") != 0){
                SendMailHTML($rowTemp1['Staff_email'], 'Project Updated - '.$rowCust['Customer_Name'].' - '.$rowProj['Project_Code'], $email, $html);
               }
            }    
            

              $email = 'The following project has been changed: '.$previousNumber.'. The new details of the project are as follows:'.Chr(10).
                       'PROJECT:                  '.$rowProj['Project_Pastel_Prefix'].' - '.$rowProj['Project_Description'].Chr(10).
                       'PURCHASE ORDER NUMBER(S): '.$purchaseOrder_Number.Chr(10).
                       'PMS4I:                    '.$rowStaff['Staff_First_Name'].' '.$rowStaff['Staff_Last_Name'].Chr(10).
                       'PMS4A:                    '.$rowStaffS4A['Staff_First_Name'].' '.$rowStaffS4A['Staff_Last_Name'].Chr(10).
                       'COST MANAGERS:            '.Chr(10).
                       'Unassigned:               '.$rowProj['CM'].Chr(10). 
                       'Admin:                    '.$rowProj['CM1'].Chr(10). 
                       'Electrical:               '.$rowProj['CM2'].Chr(10). 
                       'Mechanical:               '.$rowProj['CM3'].Chr(10).         
                       'IT:                       '.$rowProj['CM4'].Chr(10). 
                       'HR:                       '.$rowProj['CM5'].Chr(10). 
                       'Management:               '.$rowProj['CM6'].Chr(10).         
                       'Training:                 '.$rowProj['CM7'].Chr(10). 
                       'Manufacturing:            '.$rowProj['CM8'].Chr(10). 
                       'Sales and Marketing:      '.$rowProj['CM9'].Chr(10).      
                       'Software:                 '.$rowProj['CM10'].Chr(10).   
                       'Design:                   '.$rowProj['CM12'].Chr(10).    
                      
                       'CUSTOMER:                 '.$rowCust['Customer_Name'].Chr(10).
                       'DESCRIPTION:              '.$_POST['Description'];
              
              $html = 'The following project has been changed: '.$previousNumber.'. The new details of the project are as follows:
                      <BR /><BR />
                      <TABLE border=0>
                        <TR><TD><B>Project:</B></TD><TD>'.$rowProj['Project_Pastel_Prefix'].' - '.$rowProj['Project_Description'].'</TD></TR>
                        <TR><TD><B>Purchase Order Number(s):</B></TD><TD>'.$purchaseOrder_Number.'</TD></TR>
                        <TR><TD><B>PMS4I:</B></TD><TD>'.$rowStaff['Staff_First_Name'].' '.$rowStaff['Staff_Last_Name'].'</TD></TR>
                        <TR><TD><B>PMS4A:</B></TD><TD>'.$rowStaffS4A['Staff_First_Name'].' '.$rowStaffS4A['Staff_Last_Name'].'</TD></TR>
                         
                        <TR><TD><B>Cost Managers:</B></TD><TD></TD></TR>
                      
                       <TR><TD><B>Unassigned:</B></TD><TD>'.$rowProj['CM'].'</TD></TR>
                       <TR><TD><B>Admin:</B></TD><TD>'.$rowProj['CM1'].'</TD></TR>
                       <TR><TD><B>Electrical:</B></TD><TD>'.$rowProj['CM2'].'</TD></TR>
                       <TR><TD><B>Mechanical:</B></TD><TD>'.$rowProj['CM3'].'</TD></TR>
                       <TR><TD><B>IT:</B></TD><TD>'.$rowProj['CM4'].'</TD></TR>
                       <TR><TD><B>HR:</B></TD><TD>'.$rowProj['CM5'].'</TD></TR>
                       <TR><TD><B>Management:</B></TD><TD>'.$rowProj['CM6'].'</TD></TR>
                       <TR><TD><B>Training:</B></TD><TD>'.$rowProj['CM7'].'</TD></TR>
                       <TR><TD><B>Manufacturing:</B></TD><TD>'.$rowProj['CM8'].'</TD></TR>
                       <TR><TD><B>Sales and Marketing:</B></TD><TD>'.$rowProj['CM9'].'</TD></TR>
                       <TR><TD><B>Software:</B></TD><TD>'.$rowProj['CM10'].'</TD></TR>
                       <TR><TD><B>Design:</B></TD><TD>'.$rowProj['CM12'].'</TD></TR>

                        <TR><TD><B>Customer:</B></TD><TD>'.$rowCust['Customer_Name'].'</TD></TR>
                        <TR><TD><B>Description:</B></TD><TD>'.$_POST['Description'].'</TD></TR>
                      </TABLE>';
              
//                SendMailHTML($rowStaff['Staff_email'], 'Project Updated - '.$rowCust['Customer_Name'].' - '.$rowProj['Project_Code'], $email, $html);
//                SendMailHTML($rowStaffS4A['Staff_email'], 'Project Updated - '.$rowCust['Customer_Name'].' - '.$rowProj['Project_Code'], $email, $html);
                
                    array_push($emails, $rowStaff['Staff_email']);
                    array_push($emails, $rowStaffS4A['Staff_email']);
              
              
                 $sqlAllEmails = 'SELECT (SELECT Staff_email as CM FROM Staff WHERE Staff_Code = Project_CostManager) as CM,
                (SELECT Staff_email as CM1 FROM Staff WHERE Staff_Code = Project_CostManager1) as CM1,
                (SELECT Staff_email as CM2 FROM Staff WHERE Staff_Code = Project_CostManager2) as CM2,
                (SELECT Staff_email as CM3 FROM Staff WHERE Staff_Code = Project_CostManager3) as CM3,
                (SELECT Staff_email  as CM4 FROM Staff WHERE Staff_Code = Project_CostManager4) as CM4,
                (SELECT Staff_email as CM5 FROM Staff WHERE Staff_Code = Project_CostManager5) as CM5,
                (SELECT Staff_email  as CM6 FROM Staff WHERE Staff_Code = Project_CostManager6) as CM6,
                (SELECT Staff_email as CM7 FROM Staff WHERE Staff_Code = Project_CostManager7) as CM7,
                (SELECT Staff_email as CM8 FROM Staff WHERE Staff_Code = Project_CostManager8) as CM8,
                (SELECT Staff_email as CM9 FROM Staff WHERE Staff_Code = Project_CostManager9) as CM9,
                (SELECT Staff_email as CM10 FROM Staff WHERE Staff_Code = Project_CostManager10) as CM10,
                (SELECT Staff_email as CM12 FROM Staff WHERE Staff_Code = Project_CostManager12) as CM12  
                FROM Project WHERE Project_Code = "'.$rowProj['Project_Code'].'"';
     
                $resultEmails = ExecuteQuery($sqlAllEmails);               
                while($rowe = MySQL_Fetch_Array($resultEmails)){
//                    array_push($emails, $rowe['CM']);
//                    array_push($emails, $rowe['CM1']);
                    array_push($emails, $rowe['CM2']);
                    array_push($emails, $rowe['CM3']);
//                    array_push($emails, $rowe['CM4']);
//                    array_push($emails, $rowe['CM5']);
//                    array_push($emails, $rowe['CM6']);
//                    array_push($emails, $rowe['CM7']);
                    array_push($emails, $rowe['CM8']); 
//                    array_push($emails, $rowe['CM9']); 
//                    array_push($emails, $rowe['CM10']);
//                    array_push($emails, $rowe['CM12']);
                }
              
                $emails = array_unique($emails);
                foreach ($emails as $email){
                    if(strcmp($email, "") != 0){
                         SendMailHTML($email, 'Project Updated - '.$rowCust['Customer_Name'].' - '.$rowProj['Project_Code'], $email, $html);
                    }
                }

              CCSomeone($rowStaff['Staff_email'], 'stefan@s4integration.co.za', 'Project Updated - '.$rowCust['Customer_Name'].' - '.$rowProj['Project_Code'], $email, $html);
            
            //if ($_SESSION['cUID'] != '32')
            {
              $row = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = '.$_SESSION['cUID'].''));
              
              $email = 'Project details have been updated. The details are as follows:'.Chr(10).
                       'PROJECT:               '.$_POST['Project'].Chr(10).
                       'PASTEL PREFIX:         '.$_POST['Pastel'].Chr(10).
                       'UPDATED BY:            '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].Chr(10).Chr(10).
                       'Please review the updated details to ensure that everything is in order.';
              
              $html = 'Project details have been updated. The details are as follows:
                      <BR /><BR />
                      <TABLE border=0>
                        <TR><TD><B>Project:</B></TD><TD>'.$_POST['Project'].'</TD></TR>
                        <TR><TD><B>Pastel Prefix:</B></TD><TD>'.$_POST['Pastel'].'</TD></TR>
                        <TR><TD><B>Updated By:</B></TD><TD>'.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].'</TD></TR>
                      </TABLE>
                      <BR />
                      Please review the updated details to ensure that everything is in order.
                      <BR /><BR />';
              
              CCSomeone($row['Staff_email'], 'cherie@s4integration.co.za', 'Project Updated - '.$rowCust['Customer_Name'].' - '.$rowProj['Project_Code'], $email, $html);
              CCSomeone($row['Staff_email'], 'stefan@s4integration.co.za', 'Project Updated - '.$rowCust['Customer_Name'].' - '.$rowProj['Project_Code'], $email, $html);
            }
            
            if ($_POST['Closed'] == '1')
            {
              $resultSet = ExecuteQuery('SELECT * FROM Project WHERE Project_PurchaseOrder = "'.$_POST['PurchaseOrder'].'" AND Project_Complete <> "1"');
              if (MySQL_Num_Rows($resultSet) > 0)
                ExecuteQuery('UPDATE PurchaseOrder SET PurchaseOrder_Complete = "0" WHERE PurchaseOrder_ID = "'.$_POST['PurchaseOrder'].'"');
              else
                ExecuteQuery('UPDATE PurchaseOrder SET PurchaseOrder_Complete = "1" WHERE PurchaseOrder_ID = "'.$_POST['PurchaseOrder'].'"');
              
              
              //$rowStaff = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = '.$_POST['Responsible'].''));    
              $email = 'Project have been closed. The details are as follows:'.Chr(10).
                       'PROJECT:               '.$_POST['Project'].Chr(10).
                       'PASTEL PREFIX:         '.$_POST['Pastel'].Chr(10).
                       'CLOSED BY:            '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].Chr(10).Chr(10).
                       'The following items are left-over goods and may be reassigned immediately.';
              
              $html = 'Project have been closed. The details are as follows:
                      <BR /><BR />
                      <TABLE border=0>
                        <TR><TD><B>Project:</B></TD><TD>'.$_POST['Project'].'</TD></TR>
                        <TR><TD><B>Pastel Prefix:</B></TD><TD>'.$_POST['Pastel'].'</TD></TR>
                        <TR><TD><B>Closed By:</B></TD><TD>'.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].'</TD></TR>
                      </TABLE>
                      <BR />
                      The following items are left-over goods and may be reassigned immediately.
                      <BR /><BR />';
              
              $email .= 'Item          '.Chr(9).Chr(9).'Location'.Chr(9).Chr(9).'Store-room  '.Chr(10);
              $html .= '<TABLE id="storeman" style="border: 1px solid rgb(204,204,204); border-collapse: collapse;text-align: center;">'
                      . '   <THEAD><TR><TH style="border: 1px solid rgb(204,204,204);text-align: center;background-color: rgb(20, 70, 120);color:#fff;">Item</TH><TH style="border: 1px solid rgb(204,204,204);text-align: center;background-color: rgb(20, 70, 120);color:#fff;">Location</TH><TH style="border: 1px solid rgb(204,204,204);text-align: center;background-color: rgb(20, 70, 120);color:#fff;">Store-room</TH></TR></THEAD<TBODY>';
        
              $id=19;
              $storeman_items = ExecuteQuery('SELECT i.Items_Description,s.barcode AS `Shelf barcode`,s.description AS `Shelf Description` ,l.name
                                                FROM S4Admin.Project p 
                                                INNER JOIN OrderNo o ON (o.OrderNo_Project = p.Project_Code)
                                                INNER JOIN Items i ON (i.Items_Order_Number = o.OrderNo_Number)
                                                INNER JOIN stock_control.items_barcode ib ON (ib.items_id=i.Items_ID)
                                                LEFT JOIN stock_control.shelf s ON (s.id=ib.shelf_id)
                                                INNER JOIN stock_control.location l ON (l.id = s.location_id)
                                                WHERE ib.project_code = "'.$id.'" AND s.location_id IS NOT NULL');  
     
              while ($items_row = MySQL_Fetch_Array($storeman_items))
              {
                $email .= $items_row['Items_Description'].Chr(9).Chr(9).Chr(9).$items_row['Shelf barcode'].' '.$items_row['Shelf Description'].Chr(9).Chr(9).Chr(9).$items_row['name'].Chr(10).Chr(10);
                $html .= '<TR><TD style="border: 1px solid rgb(204,204,204);text-align: center;">'.$items_row['Items_Description'].'</TD><TD style="border: 1px solid rgb(204,204,204);text-align: center;">'.$items_row['Shelf barcode'].' '.$items_row['Shelf Description'].'</TD><TD style="border: 1px solid rgb(204,204,204);text-align: center;">'.$items_row['name'].'</TD></TR>';
              }
              $html .= '</TBODY></TABLE>';
              
              
              //echo $rowStaff['Staff_email'];
              //SendMailHTML('luthando.ntsekwa@s4integration.co.za', 'Project Closed', $email, $html);
            }
            
            $_SESSION['ProjectSuccess'] = 'geh!';
            Session_Unregister('EditProject');
          } else
    	      $_SESSION['ProjectFail'] = 'geh!';
        } else
          $_SESSION['ProjectIncomplete'] = 'geh!';
        break;
      default:
        break;
    }
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Handles the user's maintenance selection.                                //
  //////////////////////////////////////////////////////////////////////////////
  function HandleMaintain()
  {
    switch ($_POST['Submit'])
    {
      case 'Add':
        $_SESSION['AddProject'] = array();
        $_SESSION['AddProject'][3] = $_SESSION['cUID'];
        $_SESSION['AddProject'][10] = "MS";
        $_SESSION['AddProject'][11] = "178";
        
        
        // 7  and  16 - 26
        
          $sql = 'SELECT  * FROM ReportsCategory';
           $resultset = ExecuteQuery($sql);

            while($row = MySQL_Fetch_Array($resultset)){
                $id = (int)$row['ReportsCategory_ID'];
                if($id == 0){
                    $_SESSION['AddProject'][7] = $row['Person_Responsible'];
                }
                else if($id == 12){
                      $id = $id + 14;
                     $_SESSION['AddProject'][$id] = $row['Person_Responsible'];
                }
                else{
                    $id = $id + 15;
                     $_SESSION['AddProject'][$id] = $row['Person_Responsible'];
                }
            }
            
            $sql = 'SELECT distinct * FROM StaffMapping WHERE Staff_CodeS4 = "'.$_SESSION['AddProject'][18].'"';
            $row = MySQL_Fetch_Array(ExecuteQuery($sql));
            $_SESSION['AddProject'][14] = $row['Staff_CodeMM'] ; 
            
        break;
      case 'Edit':
        if ($_POST['ProjectEdit'] == "")
          $_SESSION['ProjectIncomplete'] = 'geh!';
        else
        {
            
            $sql = 'SELECT  * , 
               IF(
               (SELECT COUNT(Project_CodeMM)  FROM S4Admin.ProjectMapping WHERE Project_CodeS4 = '.$_POST['ProjectEdit'].') = 0, 
               NULL, 
               (SELECT Project_CodeMM FROM S4Admin.ProjectMapping WHERE Project_CodeS4 = '.$_POST['ProjectEdit'].')  ) as MMProjectCode FROM Project WHERE Project_Code ='.$_POST['ProjectEdit'];
 
          $row = MySQL_Fetch_Array(ExecuteQuery($sql));
         
          
          $project_purchase_orders = ExecuteQuery('SELECT project_id, purchaseorder_id FROM Project_PurchaseOrder WHERE project_id="'.$_POST['ProjectEdit'].'"');
          
          $_SESSION['EditProject'] = array($_POST['ProjectEdit']);
          $_SESSION['EditProject'][1] = $row['Project_Pastel_Prefix'];
          $_SESSION['EditProject'][2] = $row['Project_Long_Description'];
          $_SESSION['EditProject'][3] = $row['Project_Description'];
          $_SESSION['EditProject'][4] = $row['Project_Responsible'];
          $_SESSION['EditProject'][5] = $row['Project_Customer'];
          $_SESSION['EditProject'][6] = $row['Project_PurchaseOrder'];
          $_SESSION['EditProject'][7] = $row['Project_Closed'];
          $_SESSION['EditProject'][8] = $row['Project_OrdersPlaced'];
          $_SESSION['EditProject'][9] = $row['Project_WorkInProgress'];
          $_SESSION['EditProject'][10] = $row['Project_Complete'];
          $_SESSION['EditProject'][11] = $row['Project_Type'];
          $_SESSION['EditProject'][14] = $row['Project_CostManager'];
          $_SESSION['EditProject'][15] = $row['MMProjectCode'];
          $_SESSION['EditProject'][16] = $row['Project_CostManager1'];
          $_SESSION['EditProject'][17] = $row['Project_CostManager2'];
          $_SESSION['EditProject'][18] = $row['Project_CostManager3'];
          $_SESSION['EditProject'][19] = $row['Project_CostManager4'];
          $_SESSION['EditProject'][20] = $row['Project_CostManager5'];
          $_SESSION['EditProject'][21] = $row['Project_CostManager6'];
          $_SESSION['EditProject'][22] = $row['Project_CostManager7'];
          $_SESSION['EditProject'][23] = $row['Project_CostManager8'];
          $_SESSION['EditProject'][24] = $row['Project_CostManager9'];
          $_SESSION['EditProject'][25] = $row['Project_CostManager10'];
          $_SESSION['EditProject'][26] = $row['Project_CostManager12'];
          $_SESSION['EditProject'][27] = $row['Project_ResponsibleS4A']; 
          
        }
        break;
      case 'Update':
        if ($_POST['ProjectUpdate'] == "")
          $_SESSION['ProjectIncomplete'] = 'geh!';
        else
        {
          $_SESSION['UpdateProject'] = array();
          $_SESSION['UpdateProject'][100] = $_POST['ProjectUpdate'];
        }
        break;
      case 'Assign':
               
           $sql = 'SELECT  * FROM ReportsCategory';
           $resultset = ExecuteQuery($sql);

            while($row = MySQL_Fetch_Array($resultset)){
                $id = (int)$row['ReportsCategory_ID'];
                $_SESSION['ViewCostMangers'][$id] = $row['Person_Responsible'];
            }   

        break;   
      default:
        break;
    }
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Handles the user's submission of information for project history.        //
  //////////////////////////////////////////////////////////////////////////////
  function HandleUpdate()
  {
    $_SESSION['UpdateProject'][0] = $_POST['Details'];
    
    switch ($_POST['Submit'])
    {
      case 'Cancel':
        Session_Unregister('UpdateProject');
        break;
      case 'Submit':
        if (CheckFields())
        {
          $date = Date('Y-m-d H:i:s');
          if (ExecuteQuery('INSERT INTO ProjectLog VALUES("", "'.$_SESSION['UpdateProject'][100].'", "'.$_POST['Details'].'", "'.$_SESSION['cUID'].'", "'.$date.'")'))
    	    {
            $_SESSION['ProjectSuccess'] = 'geh!';
            Session_Unregister('UpdateProject');
          } else
    	      $_SESSION['ProjectFail'] = 'geh!';
        } else
          $_SESSION['ProjectIncomplete'] = 'geh!';
        break;
      default:
        break;
    }
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Handles the user's submission of a project selection.                    //
  //////////////////////////////////////////////////////////////////////////////
  function HandleView()
  {
    if (CheckFields())
    {
      switch ($_POST['Type'])
      {
        case 'View':
          $_SESSION['ViewProject'] = array($_POST['Project']);
          $_SESSION['ViewProject'][1] = $_POST['ProjectStatus'];
          $_SESSION['ViewProject'][2] = $_POST['Name'];
          $_SESSION['ViewProject'][3] = $_POST['ProjectType'];
          $_SESSION['ViewProject'][4] = $_POST['StartYear'].$_POST['StartMonth'].$_POST['StartDay'];
          $_SESSION['ViewProject'][5] = $_POST['EndYear'].$_POST['EndMonth'].$_POST['EndDay'];
          break;
        case 'ViewSingle':
          $_SESSION['ViewProjectSingle'] = array();
          $_SESSION['ViewProjectSingle'][100] = $_POST['Project'];
          break;
          case 'ViewMapping':
              $_SESSION['ViewProjectMapping'] = array();
              break;
        default:
          break;
      }
    } else
      $_SESSION['ProjectIncomplete'] = 'geh!';
  }
  
  
  function HandleCostManager(){

       switch ($_POST['Submit'])
      {
        case 'Cancel':
         unset($_SESSION['ViewCostMangers']);
          break;
        case 'Submit':
        
   
          $_SESSION['ViewCostMangers'][0] =  $_POST['CostManager'];
          $_SESSION['ViewCostMangers'][1] =  $_POST['CostManager1'];
          $_SESSION['ViewCostMangers'][2] =  $_POST['CostManager2'];
          $_SESSION['ViewCostMangers'][3] =  $_POST['CostManager3'];
          $_SESSION['ViewCostMangers'][4] =  $_POST['CostManager4'];
          $_SESSION['ViewCostMangers'][5] =  $_POST['CostManager5'];
          $_SESSION['ViewCostMangers'][6] =  $_POST['CostManager6'];
          $_SESSION['ViewCostMangers'][7] =  $_POST['CostManager7'];
          $_SESSION['ViewCostMangers'][8] =  $_POST['CostManager8'];
          $_SESSION['ViewCostMangers'][9] =  $_POST['CostManager9'];
          $_SESSION['ViewCostMangers'][10] =  $_POST['CostManager10'];
          $_SESSION['ViewCostMangers'][12] =  $_POST['CostManager12'];
            
           if(CheckFields()){ 
               
//                ExecuteQuery('UPDATE ReportsCategory SET Person_Responsible = "'.$_POST['CostManager'].'" WHERE ReportsCategory_ID = 0');  
//                ExecuteQuery('UPDATE ReportsCategory SET Person_Responsible = "'.$_POST['CostManager1'].'" WHERE ReportsCategory_ID = 1');  
//                ExecuteQuery('UPDATE ReportsCategory SET Person_Responsible = "'.$_POST['CostManager2'].'" WHERE ReportsCategory_ID = 2');  
//                ExecuteQuery('UPDATE ReportsCategory SET Person_Responsible = "'.$_POST['CostManager3'].'" WHERE ReportsCategory_ID = 3');
//                ExecuteQuery('UPDATE ReportsCategory SET Person_Responsible = "'.$_POST['CostManager4'].'" WHERE ReportsCategory_ID = 4');
//                ExecuteQuery('UPDATE ReportsCategory SET Person_Responsible = "'.$_POST['CostManager5'].'" WHERE ReportsCategory_ID = 5');
//                ExecuteQuery('UPDATE ReportsCategory SET Person_Responsible = "'.$_POST['CostManager6'].'" WHERE ReportsCategory_ID = 6');
//                ExecuteQuery('UPDATE ReportsCategory SET Person_Responsible = "'.$_POST['CostManager7'].'" WHERE ReportsCategory_ID = 7');
//                ExecuteQuery('UPDATE ReportsCategory SET Person_Responsible = "'.$_POST['CostManager8'].'" WHERE ReportsCategory_ID = 8');
//                ExecuteQuery('UPDATE ReportsCategory SET Person_Responsible = "'.$_POST['CostManager9'].'" WHERE ReportsCategory_ID = 9');
//                ExecuteQuery('UPDATE ReportsCategory SET Person_Responsible = "'.$_POST['CostManager10'].'" WHERE ReportsCategory_ID = 10');
//                ExecuteQuery('UPDATE ReportsCategory SET Person_Responsible = "'.$_POST['CostManager12'].'" WHERE ReportsCategory_ID = 12');

               
               $sql = 'UPDATE ReportsCategory
                        SET Person_Responsible = (case when ReportsCategory_ID = 0 then "'.$_POST['CostManager'].'"
                                             when ReportsCategory_ID = 1 then "'.$_POST['CostManager1'].'"
                                             when ReportsCategory_ID = 2 then "'.$_POST['CostManager2'].'"
                                             when ReportsCategory_ID = 3 then "'.$_POST['CostManager3'].'"
                                             when ReportsCategory_ID = 4 then "'.$_POST['CostManager4'].'" 
                                             when ReportsCategory_ID = 5 then "'.$_POST['CostManager5'].'"
                                             when ReportsCategory_ID = 6 then "'.$_POST['CostManager6'].'"
                                             when ReportsCategory_ID = 7 then "'.$_POST['CostManager7'].'"
                                             when ReportsCategory_ID = 8 then "'.$_POST['CostManager8'].'"    
                                             when ReportsCategory_ID = 9 then "'.$_POST['CostManager9'].'"
                                             when ReportsCategory_ID = 10 then "'.$_POST['CostManager10'].'"
                                             when ReportsCategory_ID = 12 then "'.$_POST['CostManager12'].'"
  
                                        end)';
               if(ExecuteQuery($sql)){
                   unset($_SESSION['ViewCostMangers']);  
                   $_SESSION['ProjectSuccess'] = 'geh!'; 
               }
               else{
                     $_SESSION['ProjectFail'] = 'geh!'; 
               } 
           }
           else{
                $_SESSION['ProjectIncomplete'] = 'geh!';
           }
          
          break;
        default:
          break;
      }
  }
  
?>