<?php
  // DETAILS ///////////////////////////////////////////////////////////////////
  //                                                                          //
  //                    Last Edited By: Gareth Ambrose                        //
  //                        Date: 23 January 2008                             //
  //                                                                          //
  ////////////////////////////////////////////////////////////////////////////// 
  // This page handles the back-end for the Passwords page.                   //
  //////////////////////////////////////////////////////////////////////////////  
     
  include '../Scripts/Include.php';
  SetSettings();
  CheckLoggedIn();
  
  switch ($_POST['Type']) 
  {	     	
    //User has submitted information for changing a password.
    case 'Change':
      HandleChange();
    	break;  
    //User has reached this page incorrectly. If they are not authorised they are redirected to the main page from Passwords page.
    default:  	
    	break;
  }  
  Header('Location: ../Passwords.php?'.Rand());	 

  ////////////////////////////////////////////////////////////////////////////// 
  // Checks that all the required fields have values and that these values    //
  // are valid.                                                               //
  ////////////////////////////////////////////////////////////////////////////// 
  function CheckFields()
  {  
    if (($_POST['OldPassword'] == "") || ($_POST['NewPassword'] == "") || ($_POST['NewPasswordConfirm'] == ""))
      return false;
    
    return true;
  }
  
  ////////////////////////////////////////////////////////////////////////////// 
  // Handles the user's submission of information for changing a password.    //
  ////////////////////////////////////////////////////////////////////////////// 
  function HandleChange()
  {    
    if (CheckFields())
    {
      $resultSet = ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = '.$_SESSION['cUID'].' AND Staff_Password = old_password("'.$_POST['OldPassword'].'")');
      if (MySQL_Num_Rows($resultSet) == 0)
      {
        $_SESSION['PasswordsOld'] = 'geh!';
        return;
      }
      
      if ($_POST['NewPassword'] != $_POST['NewPasswordConfirm'])
      {
        $_SESSION['PasswordsNew'] = 'geh!';
        return;
      }
      
      if (ExecuteQuery('UPDATE Staff SET Staff_Password = old_password("'.$_POST['NewPassword'].'") WHERE Staff_Code = "'.$_SESSION['cUID'].'"'))
      {
        unset($_SESSION[PasswordsWeakPassword]);
        $_SESSION['PasswordsSuccess'] = 'geh!';
        return;
      }
      $_SESSION['PasswordsFail'] = 'geh!';
    } else
      $_SESSION['PasswordsIncomplete'] = 'geh!';
  }
?>
