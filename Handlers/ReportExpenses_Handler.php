<?php
  // DETAILS ///////////////////////////////////////////////////////////////////
  //                                                                          //
  //                    Last Edited By: Gareth Ambrose                        //
  //                        Date: 23 November 2009                            //
  //                                                                          //
  //////////////////////////////////////////////////////////////////////////////
  // This page handles the back-end for the Expenses Report page.             //
  //////////////////////////////////////////////////////////////////////////////
  
  include '../Scripts/Include.php';
  SetSettings();
  CheckLoggedIn();
  $_POST = Replace('"', '\'\'', $_POST);
  
  switch ($_POST['Type'])
  {
    case 'Finalise': //User has submitted information for finalising expenses.
      {
        HandleFinalise();
      }
      break;
    case 'Maintain': //User has selected to Pay or Review expenses.
      {
        HandleMaintain();
      }
      break;
    case 'Pay': //User has submitted information for marking expenses as paid.
      {
        HandlePay();
      }
      break;
    case 'Review': //User has submitted information for marking expenses as reviewed.
      {
        HandleReview();
      }
      break;
    case 'View': //User has selected to view expenses.
      {
        HandleView();
      }
      break;
    default: //User has reached this page incorrectly. If they are not authorised they are redirected to the main page from the Expenses Report page.
    	break;
  }
  Header('Location: ../ReportExpenses.php?'.Rand());
  
  //////////////////////////////////////////////////////////////////////////////
  // Checks that all the required fields have values and that these values    //
  // are valid.                                                               //
  //////////////////////////////////////////////////////////////////////////////
  function CheckFields()
  {
    switch ($_POST['Type'])
    {
      case 'View':
        {
          if ($_POST['ReportExpenses'] == "")
            return false;
          
          if ($_POST['Payment'] != '2')
          {
            if (!(CheckDate($_POST['StartMonth'], $_POST['StartDay'], $_POST['StartYear'])) || !(CheckDate($_POST['EndMonth'], $_POST['EndDay'], $_POST['EndYear'])))
              return false;
            
            if (DatabaseDateLater(GetDatabaseDate($_POST['StartDay'], $_POST['StartMonth'], $_POST['StartYear']), GetDatabaseDate($_POST['EndDay'], $_POST['EndMonth'], $_POST['EndYear'])))
              return false;
          }
        }
        break;
      default:
        return false;
        break;
    }
    
    return true;
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Handles the user's submission of information for finalising expenses.    //
  //////////////////////////////////////////////////////////////////////////////
  function HandleFinalise()
  {
    switch ($_POST['Submit'])
    {
      case 'No':
        {
          Session_Unregister('FinaliseReportExpenses');
        }
        break;
      case 'Yes':
        {
          $success = true;
          $date = Date('Y-m-t');
          
          $row = MySQL_Fetch_Array(ExecuteQuery('SELECT COUNT(*) AS Number FROM LeaveDue WHERE LeaveDue_Date = "'.$date.'"'));
          if ($row['Number'] == 0)
          {
            //Check if new person. If new, calculate new rate.
            $resultSet = ExecuteQuery('SELECT Staff_Code, Staff_Leave_Allocation, Staff_Start_Date FROM Staff WHERE Staff_IsEmployee > "0"');
            while ($row = MySQL_Fetch_Array($resultSet))
            {
              //Check if the staff member is new.
              $loRowLeave = MySQL_Fetch_Array(ExecuteQuery('SELECT COUNT(*) FROM LeaveDue WHERE LeaveDue_Employee = "'.$row['Staff_Code'].'"'));
              if ($loRowLeave[0] == 0)
              {
                $loStartDate = $row['Staff_Start_Date'];
                
                $loDay = SubStr($loStartDate, 8, 2);
                $loMonth = SubStr($loStartDate, 5, 2);
                $loYear = SubStr($loStartDate, 0, 4);
                
                $loEndDate = Date('Y-m-t', MkTime(0, 0, 0, $loMonth, $loDay, $loYear));
                
                $loDays = 0;
                while ($loStartDate <= $loEndDate)
                {
                  $loDay = SubStr($loStartDate, 8, 2);
                  $loMonth = SubStr($loStartDate, 5, 2);
                  $loYear = SubStr($loStartDate, 0, 4);
                  
                  $loDate = MkTime(0, 0, 0, $loMonth, $loDay, $loYear);
                  if (!((Date('w', $loDate) == 0) || (Date('w', $loDate) == 6)))
                    $loDays++;
                  
                  $loStartDate = Date('Y-m-d', MkTime(0, 0, 0, $loMonth, $loDay + 1, $loYear));
                }
                
                $success = $success && ExecuteQuery('INSERT INTO LeaveDue VALUES("", "'.$row['Staff_Code'].'", "", "'.$date.'", "'.(($row['Staff_Leave_Allocation']/21.67)*$loDays).'", "", "", "")');
              } else
                $success = $success && ExecuteQuery('INSERT INTO LeaveDue VALUES("", "'.$row['Staff_Code'].'", "", "'.$date.'", "'.$row['Staff_Leave_Allocation'].'", "", "", "")');
            }
          }
          
          $resultSet = ExecuteQuery('SELECT * FROM Staff WHERE Staff_IsEmployee > 0');
          $dateTime = Date('Y-m-d H:i:s');
          while ($row = MySQL_Fetch_Array($resultSet))
          {
            $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT SUM(OvertimeBank_Hours) AS Available FROM OvertimeBank WHERE OvertimeBank_Name = '.$row['Staff_Code'].' AND OvertimeBank_IsApproved = "1"'));
            switch ($row['Staff_Payout_Overtime'])
            {
              case 0: //Capped.
                if ($rowTemp['Available'] > 100)
                {
                  $success = $success &&
                             ExecuteQuery('INSERT INTO Overtime VALUES("'.$row['Staff_Code'].'", "'.$dateTime.'", "'.($rowTemp['Available'] - 100).'", "215", "S4 Intranet - CapTransfer", "", "", "'.Date('Y-m-d').' 00:00:00", "'.Date('Y-m-d').' 00:00:00", "", "", "0", "", "")') &&
                             ExecuteQuery('INSERT INTO OvertimeBank VALUES("'.$row['Staff_Code'].'", "'.$dateTime.'", "'.(100 - $rowTemp['Available']).'", "215", "S4 Intranet - CapTransfer", "", "", "'.Date('Y-m-d').' 00:00:00", "'.Date('Y-m-d').' 00:00:00", "1", "", "", "", "1")');
                }
                break;
              case 1: //All.
                if ($rowTemp['Available'] > 0)
                {
                  $success = $success &&
                             ExecuteQuery('INSERT INTO Overtime VALUES("'.$row['Staff_Code'].'", "'.$dateTime.'", "'.($rowTemp['Available']).'", "215", "S4 Intranet - CapTransfer", "", "", "'.Date('Y-m-d').' 00:00:00", "'.Date('Y-m-d').' 00:00:00", "", "", "0", "", "")') &&
                             ExecuteQuery('INSERT INTO OvertimeBank VALUES("'.$row['Staff_Code'].'", "'.$dateTime.'", "'.(-$rowTemp['Available']).'", "215", "S4 Intranet - CapTransfer", "", "", "'.Date('Y-m-d').' 00:00:00", "'.Date('Y-m-d').' 00:00:00", "1", "", "", "", "1")');
                }
                break;
              case 2: //Payout nothing.
                break;
              default:
                break;
            }
          }
          
          //Calculate monthly social club contribution.
          $loDate = Date('Y-m-t');
          $rowSocial = MySQL_Fetch_Array(ExecuteQuery('SELECT COUNT(*) FROM SocialClub WHERE SocialClub_Description = "Monthly Contribution" AND SocialClub_Date = "'.$loDate.'"'));
          
          //Check that the monthly contribution for the current month has not already been added.
          if ($rowSocial[0] == 0)
          {
            $rowSocial = MySQL_Fetch_Array(ExecuteQuery('SELECT COUNT(*) FROM Staff WHERE Staff_Social_Member = "1" AND Staff_IsEmployee > "0"'));
            ExecuteQuery('INSERT INTO SocialClub VALUES("", "Monthly Contribution", "'.($rowSocial[0]*20).'", "'.$loDate.'")');
          }
          
          if ($success)
          {
            $_SESSION['ReportExpensesSuccess'] = 'geh!';
            Session_Unregister('FinaliseReportExpenses');
          } else
            $_SESSION['ReportExpensesFail'] = 'geh!';
        }
        break;
      default:
        break;
    }
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Handles the user's maintenance selection.                                //
  //////////////////////////////////////////////////////////////////////////////
  function HandleMaintain()
  {
    switch ($_POST['Submit'])
    {
      case 'Finalise':
        {
          $_SESSION['FinaliseReportExpenses'] = 'geh!';
        }
        break;
      case 'Pay':
        {
          $_SESSION['PayReportExpenses'] = 'geh!';
        }
        break;
      case 'Review':
        {
          $_SESSION['ReviewReportExpenses'] ='geh!';
        }
        break;
      default:
        break;
    }
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Handles the user's submission of information for marking expenses as     //
  // paid.                                                                    //
  //////////////////////////////////////////////////////////////////////////////
  function HandlePay()
  {
    switch ($_POST['Submit'])
    {
      case 'No':
        {
          Session_Unregister('PayReportExpenses');
        }
        break;
      case 'Yes':
        {
          $date = Date('Y-m-d H:i:s');
          if (ExecuteQuery('UPDATE Deduction SET Deduction_Payment = "1", Deduction_DateTime_Paid = "'.$date.'" WHERE Deduction_Payment = "2"') &&
              ExecuteQuery('UPDATE ReimbursiveAllowance SET ReimbursiveAllowance_DateTime_Paid = "'.$date.'", ReimbursiveAllowance_Payment = "1" WHERE ReimbursiveAllowance_Payment = "2"') &&
              ExecuteQuery('UPDATE ShiftAllowance SET ShiftAllowance_Payment = "1", ShiftAllowance_DateTime_Paid = "'.$date.'" WHERE ShiftAllowance_Payment = "2"') &&
              ExecuteQuery('UPDATE Standby SET Standby_Payment = "1", Standby_DateTime_Paid = "'.$date.'" WHERE Standby_Payment = "2"') &&
              ExecuteQuery('UPDATE TravelAllowance SET TravelAllowance_Payment = "1", TravelAllowance_DateTime_Paid = "'.$date.'" WHERE TravelAllowance_Payment = "2"') &&
              ExecuteQuery('UPDATE Overtime SET Overtime_Payment = "1", Overtime_DateTime_Paid = "'.$date.'" WHERE Overtime_Payment = "2"') &&
              ExecuteQuery('UPDATE OutOfTownAllowance SET OutOfTownAllowance_Payment = "1", OutOfTownAllowance_DateTime_Paid = "'.$date.'" WHERE OutOfTownAllowance_Payment = "2"'))
          {
            $_SESSION['ReportExpensesSuccess'] = 'geh!';
            Session_Unregister('PayReportExpenses');
          } else
            $_SESSION['ReportExpensesFail'] = 'geh!';
        }
        break;
      default:
        break;
    }
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Handles the user's submission of information for marking expenses as     //
  // reviwed.                                                                 //
  //////////////////////////////////////////////////////////////////////////////
  function HandleReview()
  {
    switch ($_POST['Submit'])
    {
      case 'No':
        {
          Session_Unregister('ReviewReportExpenses');
        }
        break;
      case 'Yes':
        {
          if (// ExecuteQuery('UPDATE Deduction SET Deduction_Payment = "2" WHERE Deduction_Payment = "0"') &&
              // ExecuteQuery('UPDATE ReimbursiveAllowance SET ReimbursiveAllowance_Payment = "2" WHERE ReimbursiveAllowance_Payment = "0"') &&
              // ExecuteQuery('UPDATE ShiftAllowance SET ShiftAllowance_Payment = "2" WHERE ShiftAllowance_Payment = "0"') &&
              // ExecuteQuery('UPDATE Standby SET Standby_Payment = "2" WHERE Standby_Payment = "0"') &&
              // ExecuteQuery('UPDATE TravelAllowance SET TravelAllowance_Payment = "2" WHERE TravelAllowance_Payment = "0"') &&
              // ExecuteQuery('UPDATE Overtime SET Overtime_Payment = "2" WHERE Overtime_Payment = "0"') &&
              // ExecuteQuery('UPDATE OutOfTownAllowance SET OutOfTownAllowance_Payment = "2" WHERE OutOfTownAllowance_Payment = "0"'))
              ExecuteQuery('UPDATE Deduction SET Deduction_Payment = "2", Deduction_DateTime_Paid = "'.$date.'" WHERE Deduction_Payment = "0"') &&
              ExecuteQuery('UPDATE ReimbursiveAllowance SET ReimbursiveAllowance_DateTime_Paid = "'.$date.'", ReimbursiveAllowance_Payment = "2" WHERE ReimbursiveAllowance_Payment = "0"') &&
              ExecuteQuery('UPDATE ShiftAllowance SET ShiftAllowance_Payment = "2", ShiftAllowance_DateTime_Paid = "'.$date.'" WHERE ShiftAllowance_Payment = "0"') &&
              ExecuteQuery('UPDATE Standby SET Standby_Payment = "2", Standby_DateTime_Paid = "'.$date.'" WHERE Standby_Payment = "0"') &&
              ExecuteQuery('UPDATE TravelAllowance SET TravelAllowance_Payment = "2", TravelAllowance_DateTime_Paid = "'.$date.'" WHERE TravelAllowance_Payment = "0"') &&
              ExecuteQuery('UPDATE Overtime SET Overtime_Payment = "2", Overtime_DateTime_Paid = "'.$date.'" WHERE Overtime_Payment = "0"') &&
              ExecuteQuery('UPDATE OutOfTownAllowance SET OutOfTownAllowance_Payment = "2", OutOfTownAllowance_DateTime_Paid = "'.$date.'" WHERE OutOfTownAllowance_Payment = "0"'))
          {
            $_SESSION['ReportExpensesSuccess'] = 'geh!';
            Session_Unregister('ReviewReportExpenses');
          } else
            $_SESSION['ReportExpensesFail'] = 'geh!';
        }
        break;
      default:
        break;
    }
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Handles the user's submission of an expenses selection.                  //
  //////////////////////////////////////////////////////////////////////////////
  function HandleView()
  {
    if (CheckFields())
    {
      $_SESSION['ViewReportExpenses'] = array($_POST['StartYear'].$_POST['StartMonth'].$_POST['StartDay']);
      $_SESSION['ViewReportExpenses'][1] = $_POST['EndYear'].$_POST['EndMonth'].$_POST['EndDay'];
      $_SESSION['ViewReportExpenses'][2] = $_POST['ReportExpenses'];
      if ($_SESSION['cAuth'] & 64)
        $_SESSION['ViewReportExpenses'][3] = $_POST['Staff'];
      else
        $_SESSION['ViewReportExpenses'][3] = $_SESSION['cUID'];
      $_SESSION['ViewReportExpenses'][4] = $_POST['Payment'];
    } else
      $_SESSION['ReportExpensesIncomplete'] = 'geh!';
  }
?>