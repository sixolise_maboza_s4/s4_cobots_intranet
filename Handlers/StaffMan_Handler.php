<?php
  //////////////////////////////////////////////////////////////////////////////
  // This page handles the back-end for the Management Staff page and allows  //
  // users to add and edit staff.                                             //
  //////////////////////////////////////////////////////////////////////////////
  
  include '../Scripts/Include.php';
  SetSettings();
  CheckLoggedIn();
  $_POST = Replace('"', '\'\'', $_POST);
  
  switch ($_POST['Type'])
  {
    //User has submitted modified information for a staff member.
    case 'Edit':
      HandleEdit();
    	break;
    //User has selected to Add or Edit a staff member.
    case 'Maintain':
      HandleMaintain();
    	break;
    //User has selected to view a staff member.
    case 'ViewDemographics':
    case 'ViewSingle':
      HandleView();               
    	break;    	
    //User has reached this page incorrectly. If they are not authorised they are redirected to the main page from the Staff page.
    default:
    	break;
  }
  Header('Location: ../StaffMan.php?'.Rand());
  
  //////////////////////////////////////////////////////////////////////////////
  // Checks that all the required fields have values and that these values    //
  // are valid.                                                               //
  //////////////////////////////////////////////////////////////////////////////
  function CheckFields()
  {
    switch ($_POST['Type'])
    {
      case 'Edit':
        if (($_POST['Contract'] == "") || ($_POST['Position'] == "") || ($_POST['Function'] == "") || ($_POST['Rate'] == "") || ($_POST['AFBNumber'] == "") || ($_POST['Cellphone'] == "") || ($_POST['Leave'] == "") || ($_POST['TravelAmount'] == "") || ($_POST['TravelDuration'] == "") || ($_POST['RAIncluded'] == ""))
          return false;
        
        if ((!Is_Numeric($_POST['Rate'])) || (!Is_Numeric($_POST['AFBNumber'])) || (!Is_Numeric($_POST['Cellphone'])) || (!Is_Numeric($_POST['Leave'])) || (!Is_Numeric($_POST['TravelAmount'])) || (!Is_Numeric($_POST['TravelDuration'])) || (!Is_Numeric($_POST['Salary'])) || (!Is_Numeric($_POST['TravelAllowance'])) || (!Is_Numeric($_POST['RAContribution'])))
          return false;
        
        if (($_POST['Rate'] < 0) || ($_POST['AFBNumber'] < 0) || ($_POST['Cellphone'] < 0) || ($_POST['Leave'] < 0) || ($_POST['TravelAmount'] < 0) || ($_POST['TravelDuration'] < 0))
          return false;
        
        if ($_POST['HoursStart'] > $_POST['HoursEnd'])
          return false;
        
        if (!(CheckDate($_POST['ProbeMonth'], $_POST['ProbeDay'], $_POST['ProbeYear'])))
          return false;
        
        if (($_POST['Current']) && ($_POST['Contract'] == 6))
        {
          if (DatabaseDateLater(Date('Y-m-d'), GetDatabaseDate($_POST['ProbeDay'], $_POST['ProbeMonth'], $_POST['ProbeYear'])))
            return false;
        }
        break;
      case 'ViewDemographics':
        break;
      case 'ViewSingle':
        if ($_POST['Staff'] == "" && ($_SESSION['cUID'] == 15 || $_SESSION['cUID'] == 318 || $_SESSION['cUID'] == 1 || $_SESSION['cUID'] == 282))
          return false;
        break;
      default:
        return false;
        break;
    }
    
    return true;
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Handles the user's submission of modified information for a staff        //
  // member.                                                                  //
  //////////////////////////////////////////////////////////////////////////////
  function HandleEdit()
  {
    $_SESSION['EditStaffMan'][1] = $_POST['AFBNumber'];
    $_SESSION['EditStaffMan'][2] = $_POST['Cellphone'];
    $_SESSION['EditStaffMan'][3] = $_POST['Leave'];
    $_SESSION['EditStaffMan'][4] = $_POST['AuthLevel'];
    $_SESSION['EditStaffMan'][5] = $_POST['Rate'];
    $_SESSION['EditStaffMan'][6] = $_POST['HoursStart'];
    $_SESSION['EditStaffMan'][7] = $_POST['HoursEnd'];
    $_SESSION['EditStaffMan'][8] = $_POST['TravelAmount'];
    $_SESSION['EditStaffMan'][9] = $_POST['Contract'];
    $_SESSION['EditStaffMan'][10] = $_POST['TravelDuration'];
    $_SESSION['EditStaffMan'][11] = $_POST['Current'] ? $_POST['Current'] : 0;
    $_SESSION['EditStaffMan'][12] = $_POST['HoursAccountable'];
    $_SESSION['EditStaffMan'][13] = $_POST['PayoutOvertime'];
    $_SESSION['EditStaffMan'][14] = $_POST['Social'];
    $_SESSION['EditStaffMan'][15] = $_POST['Position'];
    $_SESSION['EditStaffMan'][16] = $_POST['Function'];
    $_SESSION['EditStaffMan'][17] = $_POST['Salary'];
    $_SESSION['EditStaffMan'][18] = $_POST['TravelAllowance'];
    $_SESSION['EditStaffMan'][19] = $_POST['RAContribution'];
    $_SESSION['EditStaffMan'][20] = $_POST['ProbeYear'].$_POST['ProbeMonth'].$_POST['ProbeDay'];
    $_SESSION['EditStaffMan'][21] = $_POST['RAIncluded'];
    
    switch ($_POST['Submit'])
    {
      case 'Cancel':
        Session_Unregister('EditStaffMan');
        break;
      case 'Submit':
        if (CheckFields())
        {
          $dateTime = Date('Y-m-d H:i:s');
          
          $row = MySQL_Fetch_Array(ExecuteQuery('SELECT Staff_First_Name, Staff_Last_Name, Staff_IsEmployee, Staff_Salary FROM Staff WHERE Staff_Code = '.$_SESSION['EditStaffMan'][0].''));
          if ($_POST['Current'])
            $current = ($row['Staff_IsEmployee'] ? $row['Staff_IsEmployee'] : $_POST['Current']);
          else
            $current = 0;
          
          if (ExecuteQuery('UPDATE Staff SET Staff_Auth_Level = "'.$_POST['AuthLevel'].'", Staff_Contract = "'.$_POST['Contract'].'", Staff_IsEmployee = "'.$current.'", Staff_Social_Member = "'.$_POST['Social'].'", Staff_Payout_Overtime = "'.$_POST['PayoutOvertime'].'", Staff_OT_Rate = "'.$_POST['Rate'].'", Staff_AFB_Number = "'.$_POST['AFBNumber'].'", Staff_Cellphone_Allowance = "'.$_POST['Cellphone'].'", Staff_Leave_Allocation = "'.$_POST['Leave'].'", Staff_Travel_Amount = "'.$_POST['TravelAmount'].'", Staff_Travel_Duration = "'.$_POST['TravelDuration'].'", Staff_Position = "'.$_POST['Position'].'", Staff_Function = "'.$_POST['Function'].'", Staff_Salary = "'.$_POST['Salary'].'", Staff_Travel_Allowance = "'.$_POST['TravelAllowance'].'", Staff_RA_Contribution = "'.$_POST['RAContribution'].'", Staff_RA_Included = "'.$_POST['RAIncluded'].'", Staff_Probation_Date_Expires = "'.GetDatabaseDate($_POST['ProbeDay'], $_POST['ProbeMonth'], $_POST['ProbeYear']).'" WHERE Staff_Code = '.$_SESSION['EditStaffMan'][0].''))
    	    {
            // Delete any probation expiry reminders.
            ExecuteQuery('DELETE FROM Reminder WHERE (Reminder_System_Reference = "Management") AND (Reminder_System_Reference_ID = '.$_SESSION['EditStaffMan'][0].')');
            
            // Temporary staff must have a reminder for the probation expiry date.
            if ($current && ($_POST['Contract'] == 6))
            {
              ExecuteQuery('INSERT INTO Reminder VALUES("", "'.$_SESSION['cUID'].'", "'.$dateTime.'", "0", "'.GetReminderDate(GetDatabaseDate($_POST['ProbeDay'], $_POST['ProbeMonth'], $_POST['ProbeYear'])).'", "2", "4", "64", "Probation period has expired for <B>'.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].' - '.GetTextualDate($_POST['ProbeDay'], $_POST['ProbeMonth'], $_POST['ProbeYear']).'</B>.", "Management", "'.$_SESSION['EditStaffMan'][0].'", "", "")');
            }
            
            $resultSet = ExecuteQuery('SELECT * FROM WorkHours WHERE WorkHours_Name = '.$_SESSION['EditStaffMan'][0].'');
    	      if (MySQL_Num_Rows($resultSet) > 0)
            {
              $query = 'UPDATE WorkHours SET WorkHours_Accountable = "'.$_POST['HoursAccountable'].'"';
              for ($count = 0; $count < 24; $count++)
              {
                if (($count >= $_POST['HoursStart']) && ($count < $_POST['HoursEnd']))
                  $query .= ', WorkHours_Hour'.$count.' = "1"';
                else
                  $query .= ', WorkHours_Hour'.$count.' = "0"';
              }
              $query .= ' WHERE WorkHours_Name = '.$_SESSION['EditStaffMan'][0];
            }
            else
            {
              $query = 'INSERT INTO WorkHours VALUES("", "'.$_SESSION['EditStaffMan'][0].'"';
              for ($count = 0; $count < 24; $count++)
              {
                if (($count >= $_POST['HoursStart']) && ($count < $_POST['HoursEnd']))
                  $query .= ', "1"';
                else
                  $query .= ', "0"';
              }
              $query .= ', "'.$_POST['HoursAccountable'].'")';
            }
            ExecuteQuery($query);
            
            $loCurrent = ($row['Staff_IsEmployee'] > 0);
            if ($loCurrent <> $_POST['Current'])
            {
              $email = 'Staff details have been updated. The details are as follows:'.Chr(10).
                        'STAFF NAME:            '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].Chr(10).Chr(10);
              
              $html = 'Staff details have been updated. The details are as follows:
                      <BR /><BR />
                      <TABLE border=0>
                        <TR><TD><B>Staff Name:</B></TD><TD>'.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].'</TD></TR>
                      </TABLE>
                      <BR />';
              
              if ($loCurrent)
              {
                $email .= 'Is no longer a current staff member.'.Chr(10).Chr(10);
                $html .= 'Is no longer a current staff member.
                          <BR /><BR />';
              } else
              {
                $email .= 'Is now a current staff member.'.Chr(10).Chr(10);
                $html .= 'Is now a current staff member.
                          <BR /><BR />';
              }
              
              $email .= 'Once the details are confirmed to be correct they can be forwarded to AFB to update their system. The RA must also be updated.'.Chr(10).Chr(10);
              $html .= 'Once the details are confirmed to be correct they can be forwarded to AFB to update their system. The RA must also be updated.
                      <BR /><BR />';
              
              SendMailHTML('management@s4integration.co.za', 'Staff Updated', $email, $html);
            }
            
            $loSalary = (int)$_POST['Salary'];
            if ($loSalary <> $row['Staff_Salary'])
            {
              $email = 'Staff details have been updated. The details are as follows:'.Chr(10).
                        'STAFF NAME:            '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].Chr(10).Chr(10).
                        'This staff member\'s salary has changed.'.Chr(10).Chr(10).
                        'Once the details are confirmed to be correct they can be forwarded to AFB to update their system. The RA must also be updated.'.Chr(10).Chr(10);
              
              $html = 'Staff details have been updated. The details are as follows:
                      <BR /><BR />
                      <TABLE border=0>
                        <TR><TD><B>Staff Name:</B></TD><TD>'.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].'</TD></TR>
                      </TABLE>
                      <BR />
                      This staff member\'s salary has changed.
                      <BR /><BR />
                      Once the details are confirmed to be correct they can be forwarded to AFB to update their system. The RA must also be updated.
                      <BR /><BR />';
              
              SendMailHTML('management@s4integration.co.za', 'Staff Updated', $email, $html);
            }
            
            Session_Unregister('EditStaffMan');
            $_SESSION['StaffManSuccess'] = 'geh!';
          } else
            $_SESSION['StaffManFail'] = 'geh!';
        } else
          $_SESSION['StaffManIncomplete'] = 'geh!';
        break;
      default:
        break;
    }
  }
  
  ////////////////////////////////////////////////////////////////////////////// 
  // Handles the user's maintenance selection.                                //
  ////////////////////////////////////////////////////////////////////////////// 
  function HandleMaintain() 
  {  
    switch ($_POST['Submit'])
    {
      case 'Edit':
        if (!in_array($_SESSION['cUID'], ManagementAccess()))
           $_POST['Staff'] = $_SESSION['cUID'];
          
        if ($_POST['Staff'] == "")
          $_SESSION['StaffManIncomplete'] = 'geh!'; 
        else
        {
          $row = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = '.$_POST['Staff'].''));
          $resultSet = ExecuteQuery('SELECT * FROM WorkHours WHERE WorkHours_Name = '.$_POST['Staff'].'');
          $start = 0;
          $end = 0;
          if (MySQL_Num_Rows($resultSet) > 0)
          {
            $rowTemp = MySQL_Fetch_Array($resultSet);
            $startFound = false;
            for ($count = 0; $count < 24; $count++)
            {
              if ($startFound)
              {
                if ($rowTemp['WorkHours_Hour'.$count] == '0')
                {
                  $end = $count;
                  break;
                }
              } else
                if ($rowTemp['WorkHours_Hour'.$count] == '1')
                {
                  $start = $count;
                  $startFound = true;
                }
            }
            $accountable = $rowTemp['WorkHours_Accountable'];
          }
          
          $_SESSION['EditStaffMan'] = array($_POST['Staff']);
          $_SESSION['EditStaffMan'][1] = $row['Staff_AFB_Number'];
          $_SESSION['EditStaffMan'][2] = $row['Staff_Cellphone_Allowance'];
          $_SESSION['EditStaffMan'][3] = $row['Staff_Leave_Allocation'];
          $_SESSION['EditStaffMan'][4] = $row['Staff_Auth_Level'];
          $_SESSION['EditStaffMan'][5] = $row['Staff_OT_Rate'];
          $_SESSION['EditStaffMan'][6] = $start;
          $_SESSION['EditStaffMan'][7] = $end;
          $_SESSION['EditStaffMan'][8] = $row['Staff_Travel_Amount'];
          $_SESSION['EditStaffMan'][9] = $row['Staff_Contract'];
          $_SESSION['EditStaffMan'][10] = $row['Staff_Travel_Duration'];
          $_SESSION['EditStaffMan'][11] = $row['Staff_IsEmployee'];
          $_SESSION['EditStaffMan'][12] = $accountable;
          $_SESSION['EditStaffMan'][13] = $row['Staff_Payout_Overtime'];
          $_SESSION['EditStaffMan'][14] = $row['Staff_Social_Member'];
          $_SESSION['EditStaffMan'][15] = $row['Staff_Position'];
          $_SESSION['EditStaffMan'][16] = $row['Staff_Function'];
          $_SESSION['EditStaffMan'][17] = $row['Staff_Salary'];
          $_SESSION['EditStaffMan'][18] = $row['Staff_Travel_Allowance'];
          $_SESSION['EditStaffMan'][19] = $row['Staff_RA_Contribution'];
          $_SESSION['EditStaffMan'][20] = GetSessionDateFromDatabaseDate($row['Staff_Probation_Date_Expires']);
          $_SESSION['EditStaffMan'][21] = $row['Staff_RA_Included'];
        }
        break;
      default:
        break;
    }                          
  }
  
  ////////////////////////////////////////////////////////////////////////////// 
  // Handles the user's submission of a staff member selection.               //
  ////////////////////////////////////////////////////////////////////////////// 
  function HandleView() 
  {
    if (CheckFields())
    {
      switch ($_POST['Type'])
      {
        case 'ViewDemographics':
          $_SESSION['ViewStaffManDemographics'] = array();
          break;
        case 'ViewSingle':
          if (in_array($_SESSION['cUID'], ManagementAccess()))
              $_SESSION['ViewStaffManSingle'] = array($_POST['Staff']);
          else
                $_SESSION['ViewStaffManSingle'] = array($_SESSION['cUID']);
          
          break;
        default:
          return false;
          break;
      }
    } else
      $_SESSION['StaffManIncomplete'] = 'geh!';
  }
?>