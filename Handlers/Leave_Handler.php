<?php
  //////////////////////////////////////////////////////////////////////////////
  // This page handles the back-end for the Leave page.                       //
  //////////////////////////////////////////////////////////////////////////////
  
  include '../Scripts/Include.php';
  SetSettings();
  CheckLoggedIn();
  $_POST = Replace('"', '\'\'', $_POST);
  
  switch ($_POST['Type'])
  {
    //User has submitted information for allocating leave.
    case 'Allocate':
      HandleAllocate();
    	break;
    //User has submitted information for approving leave.
    case 'Approve':
      HandleApprove();
    	break;
    //User has submitted information for a leave cancellation.
    case 'Cancel':
      HandleCancel();
    	break;
    //User has submitted modified information for leave.
    case 'Edit':
      HandleEdit();
    	break;
    //User has selected to Allocate, Approve or Request leave.
    case 'Maintain':
      HandleMaintain();
    	break;
    //User has submitted information for requesting leave.
    case 'Request':
      HandleRequest();
    	break;
    //User has selected to view leave.
    case 'View':
    case 'Cycle':        
      HandleView();
    	break;
    //User has reached this page incorrectly. If they are not authorised they are redirected to the main page from the Leave page.
    default:
    	break;
  }
  Header('Location: ../Leave.php?'.Rand());
  
  //////////////////////////////////////////////////////////////////////////////
  // Checks that all the required fields have values and that these values    //
  // are valid.                                                               //
  //////////////////////////////////////////////////////////////////////////////
  function CheckFields()
  {
    switch ($_POST['Type'])
    {
      case 'Approve':
        {
          $loRow = MySQL_Fetch_Array(ExecuteQuery('SELECT L.Leave_Start FROM `Leave` L WHERE (L.Leave_ID = "'.$_SESSION['ApproveLeave'][0].'")'));
          
          // Leave can only be approved 6 weeks in advance, no earlier.
          if (DatabaseDateLater($loRow['Leave_Start'], GetDatabaseDate(Date('d')+42, Date('m'), Date('Y'))))
            return false;
          break;
        }
      case 'Edit':
      case 'Request';
        if (($_POST['Authorise'] == "") || ($_POST['Leave'] == "") || ($_POST['Days'] == ""))
          return false;
        
        if (!Is_Numeric($_POST['Days']))
          return false;
        
        if ($_POST['Days'] <= 0)
          return false;
        
        if (!(CheckDate($_POST['StartMonth'], $_POST['StartDay'], $_POST['StartYear'])) || !(CheckDate($_POST['EndMonth'], $_POST['EndDay'], $_POST['EndYear'])))
          return false;
        
        if (DatabaseDateLater(GetDatabaseDate($_POST['StartDay'], $_POST['StartMonth'], $_POST['StartYear']), GetDatabaseDate($_POST['EndDay'], $_POST['EndMonth'], $_POST['EndYear'])))
          return false;
        break;
      case 'View':
        if ($_POST['Leave'] == "")
          return false;
        
        if (!(CheckDate($_POST['StartMonth'], $_POST['StartDay'], $_POST['StartYear'])) || !(CheckDate($_POST['EndMonth'], $_POST['EndDay'], $_POST['EndYear'])))
          return false;
        
        if (DatabaseDateLater(GetDatabaseDate($_POST['StartDay'], $_POST['StartMonth'], $_POST['StartYear']), GetDatabaseDate($_POST['EndDay'], $_POST['EndMonth'], $_POST['EndYear'])))
          return false;
        break;
      case 'Cycle':
        if ($_POST['Cycles'] == "")
          return false;
        break;
      default:
        return false;
        break;
    }
    
    return true;
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Handles the user's submission of information for allocating leave.       //
  //////////////////////////////////////////////////////////////////////////////
  function HandleAllocate()
  {
    $_SESSION['AllocateLeave'][0] = $_POST['Year'].$_POST['Month']."99";
    
    switch ($_POST['Submit'])
    {
      case 'Cancel':
        Session_Unregister('AllocateLeave');
        break;
      case 'Submit':
        $success = true;
        $date = Date('Y-m-t', MKTime(0, 0, 0, $_POST['Month'], 1, $_POST['Year']));
        
        $row = MySQL_Fetch_Array(ExecuteQuery('SELECT COUNT(*) AS All FROM LeaveDue WHERE LeaveDue_Date = "'.$date.'"'));
        if ($row['All'] == 0)
        {        
          $resultSet = ExecuteQuery('SELECT Staff_Code, Staff_Leave_Allocation, Staff_Start_Date FROM Staff WHERE Staff_IsEmployee > "0"');
          while ($row = MySQL_Fetch_Array($resultSet))
          {
            //Check if the staff member is new.
            $loRowLeave = MySQL_Fetch_Array(ExecuteQuery('SELECT COUNT(*) FROM LeaveDue WHERE LeaveDue_Employee = "'.$row['Staff_Code'].'"'));
            if ($loRowLeave[0] == 0)
            {
              $loStartDate = $row['Staff_Start_Date'];
              
              $loDay = SubStr($loStartDate, 8, 2);
              $loMonth = SubStr($loStartDate, 5, 2);
              $loYear = SubStr($loStartDate, 0, 4);
              
              $loEndDate = Date('Y-m-t', MkTime(0, 0, 0, $loMonth, $loDay, $loYear));
              
              $loDays = 0;
              while ($loStartDate <= $loEndDate)
              {
                $loDay = SubStr($loStartDate, 8, 2);
                $loMonth = SubStr($loStartDate, 5, 2);
                $loYear = SubStr($loStartDate, 0, 4);
                
                $loDate = MkTime(0, 0, 0, $loMonth, $loDay, $loYear);
                if (!((Date('w', $loDate) == 0) || (Date('w', $loDate) == 6)))
                  $loDays++;
                
                $loStartDate = Date('Y-m-d', MkTime(0, 0, 0, $loMonth, $loDay + 1, $loYear));
              }
              
              $success = $success && ExecuteQuery('INSERT INTO LeaveDue VALUES("", "'.$row['Staff_Code'].'", "", "'.$date.'", "'.(($row['Staff_Leave_Allocation']/21.67)*$loDays).'", "", "", "")');
            } else
              $success = $success && ExecuteQuery('INSERT INTO LeaveDue VALUES("", "'.$row['Staff_Code'].'", "", "'.$date.'", "'.$row['Staff_Leave_Allocation'].'", "", "", "")');
          }
      	}
        if ($success)
        {
          $_SESSION['LeaveSuccess'] = 'geh!';
          Session_Unregister('AllocateLeave');
        } else
          $_SESSION['LeaveFail'] = 'geh!';
        break;
      default:
        break;
    }
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Handles the user's submission of information for approving leave.        //
  //////////////////////////////////////////////////////////////////////////////
  function HandleApprove()
  {
    switch ($_POST['Submit'])
    {
      case 'Approve':
        if (CheckFields())
        {
          if (ExecuteQuery('UPDATE `Leave` SET Leave_IsApproved = "1", Leave_Date_Approved = "'.Date('Y-m-d H:i:s').'" WHERE Leave_ID = "'.$_SESSION['ApproveLeave'][0].'"'))
          {
            $row = MySQL_Fetch_Array(ExecuteQuery('SELECT `Leave`.*, IF (Leave_Doctor = "1", "Yes", "No") AS Doctor FROM `Leave` WHERE Leave_ID = "'.$_SESSION['ApproveLeave'][0].'"'));
            $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT Staff_First_Name, Staff_Last_Name, Staff_email FROM Staff WHERE Staff_Code = "'.$row['Leave_Employee'].'"'));
            $rowTempB = MySQL_Fetch_Array(ExecuteQuery('SELECT LeaveType_Description FROM LeaveType WHERE LeaveType_ID = "'.$row['Leave_Type'].'"'));
            //Andrew's email
						//$rowTempC = MySQL_Fetch_Array(ExecuteQuery('SELECT Staff_email FROM Staff WHERE Staff_Code = "015"'));
            //Nadia's email
		$rowTempC = MySQL_Fetch_Array(ExecuteQuery('SELECT Staff_email FROM Staff WHERE Staff_Code = "216"'));
			
            $email = 'The leave you requested has been approved. The details of the approved leave are as follows:'.Chr(10).
                     'NAME:                  '.$rowTemp['Staff_First_Name'].' '.$rowTemp['Staff_Last_Name'].Chr(10).
                     'LEAVE TYPE:            '.$rowTempB['LeaveType_Description'].Chr(10).
                     'LEAVE PERIOD:          '.GetTextualDateFromDatabaseDate($row['Leave_Start']).' until '.GetTextualDateFromDatabaseDate($row['Leave_End']).Chr(10).
                     'DAYS:                  '.SPrintF('%02.2f', $row['Leave_Days']).Chr(10).
                     'DOCTOR\'S CERTIFICATE:  '.$row['Doctor'].Chr(10).
                     'DESCRIPTION:           '.$row['Leave_Comments'];
            
            $html = 'The leave you requested has been approved. The details of the approved leave are as follows:
                    <BR /><BR />
                    <TABLE border=0>
                      <TR><TD><B>Name:</B></TD><TD>'.$rowTemp['Staff_First_Name'].' '.$rowTemp['Staff_Last_Name'].'</TD></TR>
                      <TR><TD><B>Leave Type:</B></TD><TD>'.$rowTempB['LeaveType_Description'].'</TD></TR>
                      <TR><TD><B>Leave Period:</B></TD><TD>'.GetTextualDateFromDatabaseDate($row['Leave_Start']).' until '.GetTextualDateFromDatabaseDate($row['Leave_End']).'</TD></TR>
                      <TR><TD><B>Days:</B></TD><TD>'.SPrintF('%02.2f', $row['Leave_Days']).'</TD></TR>
                      <TR><TD><B>Doctor\'s Certificate:</B></TD><TD>'.$row['Doctor'].'</TD></TR>
                      <TR><TD><B>Description:</B></TD><TD>'.$row['Leave_Comments'].'</TD></TR>
                    </TABLE>';
            
           $unpaid_leave=  'NAME:                  '.$rowTemp['Staff_First_Name'].' '.$rowTemp['Staff_Last_Name'].Chr(10).
                           'LEAVE PERIOD:          '.GetTextualDateFromDatabaseDate($row['Leave_Start']).' until '.GetTextualDateFromDatabaseDate($row['Leave_End']).Chr(10).
                           'DAYS:                  '.SPrintF('%02.2f', $row['Leave_Days']).Chr(10).
                           'DESCRIPTION:           '.$row['Leave_Comments'];
            
           $unpaid_leave_html = '<TABLE border=0>
                                    <TR><TD><B>Name:</B></TD><TD>'.$rowTemp['Staff_First_Name'].' '.$rowTemp['Staff_Last_Name'].'</TD></TR>
                                    <TR><TD><B>Leave Period:</B></TD><TD>'.GetTextualDateFromDatabaseDate($row['Leave_Start']).' until '.GetTextualDateFromDatabaseDate($row['Leave_End']).'</TD></TR>
                                    <TR><TD><B>Days:</B></TD><TD>'.SPrintF('%02.2f', $row['Leave_Days']).'</TD></TR>
                                    <TR><TD><B>Description:</B></TD><TD>'.$row['Leave_Comments'].'</TD></TR>
                                  </TABLE>';
            
            SendMailHTML($rowTemp['Staff_email'], 'Leave Approved', $email, $html);
			
						//LEGEND: Added 2013/04/06 - Email unapid leave to Andrew
						//if($row['Leave_Type'] == 'Unpaid')
							//SendMailHTML($rowTempC['Staff_email'], 'Unpaid Leave Approved', $email, $html);
                                                //Added 2014/12/06 - Email unpaid leave to Nadia
                                                if($rowTempB['LeaveType_Description'] == 'Unpaid')
                                                        SendMailHTML($rowTempC['Staff_email'], 'Unpaid Leave Approved', $unpaid_leave, $unpaid_leave_html);
			
            $_SESSION['LeaveSuccess'] = 'geh!';
            Session_Unregister('ApproveLeave');
			/*if ($_POST['Type'] == 'unpaid')
			{
				$content = $rowTemp['Staff_First_Name'] && '  ' && $row['Leave_Comments'];
				SendMailHTML('andrew@s4integration.co.za', $content );
			}*/
			
          } else
            $_SESSION['LeaveFail'] = 'geh!';
          break;
        } else
          $_SESSION['LeaveIncomplete'] = 'geh!';
        break;
      case 'Cancel':
        Session_Unregister('ApproveLeave');
        break;
      case 'Deny':
        if (ExecuteQuery('UPDATE `Leave` SET Leave_IsApproved = "2", Leave_Date_Approved = "'.Date('Y-m-d H:i:s').'" WHERE Leave_ID = "'.$_SESSION['ApproveLeave'][0].'"'))
  	    {
          $row = MySQL_Fetch_Array(ExecuteQuery('SELECT `Leave`.*, IF (Leave_Doctor = "1", "Yes", "No") AS Doctor FROM `Leave` WHERE Leave_ID = "'.$_SESSION['ApproveLeave'][0].'"'));
          $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT Staff_First_Name, Staff_Last_Name, Staff_email FROM Staff WHERE Staff_Code = "'.$row['Leave_Employee'].'"'));
          $rowTempB = MySQL_Fetch_Array(ExecuteQuery('SELECT LeaveType_Description FROM LeaveType WHERE LeaveType_ID = "'.$row['Leave_Type'].'"'));
          
          $email = 'The leave you requested has been denied. The details of the denied leave are as follows:'.Chr(10).
                   'NAME:                  '.$rowTemp['Staff_First_Name'].' '.$rowTemp['Staff_Last_Name'].Chr(10).
                   'LEAVE TYPE:            '.$rowTempB['LeaveType_Description'].Chr(10).
                   'LEAVE PERIOD:          '.GetTextualDateFromDatabaseDate($row['Leave_Start']).' until '.GetTextualDateFromDatabaseDate($row['Leave_End']).Chr(10).
                   'DAYS:                  '.SPrintF('%02.2f', $row['Leave_Days']).Chr(10).
                   'DOCTOR\'S CERTIFICATE:  '.$row['Doctor'].Chr(10).
                   'DESCRIPTION:           '.$row['Leave_Comments'];
          
          $html = 'The leave you requested has been denied. The details of the denied leave are as follows:
                  <BR /><BR />
                  <TABLE border=0>
                    <TR><TD><B>Name:</B></TD><TD>'.$rowTemp['Staff_First_Name'].' '.$rowTemp['Staff_Last_Name'].'</TD></TR>
                    <TR><TD><B>Leave Type:</B></TD><TD>'.$rowTempB['LeaveType_Description'].'</TD></TR>
                    <TR><TD><B>Leave Period:</B></TD><TD>'.GetTextualDateFromDatabaseDate($row['Leave_Start']).' until '.GetTextualDateFromDatabaseDate($row['Leave_End']).'</TD></TR>
                    <TR><TD><B>Days:</B></TD><TD>'.SPrintF('%02.2f', $row['Leave_Days']).'</TD></TR>
                    <TR><TD><B>Doctor\'s Certificate:</B></TD><TD>'.$row['Doctor'].'</TD></TR>
                    <TR><TD><B>Description:</B></TD><TD>'.$row['Leave_Comments'].'</TD></TR>
                  </TABLE>';
          
          SendMailHTML($rowTemp['Staff_email'], 'Leave Denied', $email, $html);
          $_SESSION['LeaveSuccess'] = 'geh!';
          Session_Unregister('ApproveLeave');
        } else
          $_SESSION['LeaveFail'] = 'geh!';
        break;
      default:
        break;
    }
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Handles the user's submission of a leave cancellation.                   //
  //////////////////////////////////////////////////////////////////////////////
  function HandleCancel()
  {
    switch ($_POST['Submit'])
    {
      case 'No':
        Session_Unregister('CancelLeave');
        break;
      case 'Yes':
        if (ExecuteQuery('UPDATE `Leave` SET Leave_IsApproved = "3" WHERE Leave_ID = '.$_SESSION['CancelLeave'][0].''))
        {
          $row = MySQL_Fetch_Array(ExecuteQuery('SELECT `Leave`.*, IF (Leave_Doctor = "1", "Yes", "No") AS Doctor FROM `Leave` WHERE Leave_ID = '.$_SESSION['CancelLeave'][0].''));
          $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT Staff_First_Name, Staff_Last_Name, Staff_email FROM Staff WHERE Staff_Code = '.$row['Leave_Employee'].''));
          $rowTempB = MySQL_Fetch_Array(ExecuteQuery('SELECT LeaveType_Description FROM LeaveType WHERE LeaveType_ID = '.$row['Leave_Type'].''));
          
          $email = 'Leave of yours has been cancelled. The details of the cancelled leave are as follows:'.Chr(10).
                   'NAME:                  '.$rowTemp['Staff_First_Name'].' '.$rowTemp['Staff_Last_Name'].Chr(10).
                   'LEAVE TYPE:            '.$rowTempB['LeaveType_Description'].Chr(10).
                   'LEAVE PERIOD:          '.GetTextualDateFromDatabaseDate($row['Leave_Start']).' until '.GetTextualDateFromDatabaseDate($row['Leave_End']).Chr(10).
                   'DAYS:                  '.SPrintF('%02.2f', $row['Leave_Days']).Chr(10).
                   'DOCTOR\'S CERTIFICATE:  '.$row['Doctor'].Chr(10).
                   'DESCRIPTION:           '.$row['Leave_Comments'];
          
          $html = 'Leave of yours has been cancelled. The details of the cancelled leave are as follows:
                  <BR /><BR />
                  <TABLE border=0>
                    <TR><TD><B>Name:</B></TD><TD>'.$rowTemp['Staff_First_Name'].' '.$rowTemp['Staff_Last_Name'].'</TD></TR>
                    <TR><TD><B>Leave Type:</B></TD><TD>'.$rowTempB['LeaveType_Description'].'</TD></TR>
                    <TR><TD><B>Leave Period:</B></TD><TD>'.GetTextualDateFromDatabaseDate($row['Leave_Start']).' until '.GetTextualDateFromDatabaseDate($row['Leave_End']).'</TD></TR>
                    <TR><TD><B>Days:</B></TD><TD>'.SPrintF('%02.2f', $row['Leave_Days']).'</TD></TR>
                    <TR><TD><B>Doctor\'s Certificate:</B></TD><TD>'.$row['Doctor'].'</TD></TR>
                    <TR><TD><B>Description:</B></TD><TD>'.$row['Leave_Comments'].'</TD></TR>
                  </TABLE>';
          
          SendMailHTML($rowTemp['Staff_email'], 'Leave Cancelled', $email, $html);
          
          $_SESSION['LeaveSuccess'] = 'geh!';
          Session_Unregister('CancelLeave');
        } else
          $_SESSION['LeaveFail'] = 'geh!';
        break;
      default:
        break;
    }
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Handles the user's submission of modified information for leave.         //
  //////////////////////////////////////////////////////////////////////////////
  function HandleEdit()
  {
    $_SESSION['EditLeave'][2] = $_POST['Authorise'];
    $_SESSION['EditLeave'][3] = $_POST['Description'];
    $_SESSION['EditLeave'][4] = $_POST['Leave'];
    $_SESSION['EditLeave'][5] = $_POST['StartYear'].$_POST['StartMonth'].$_POST['StartDay'];
    $_SESSION['EditLeave'][6] = $_POST['EndYear'].$_POST['EndMonth'].$_POST['EndDay'];
    $_SESSION['EditLeave'][7] = $_POST['Days'];
    $_SESSION['EditLeave'][8] = $_POST['Doctor'];
    
    switch ($_POST['Submit'])
    {
      case 'Cancel':
        Session_Unregister('EditLeave');
        break;
      case 'Submit':
        if (CheckFields())
        {
          if (ExecuteQuery('UPDATE `Leave` SET Leave_Approved = "'.$_POST['Authorise'].'", Leave_Type = "'.$_POST['Leave'].'", Leave_Start = "'.GetDatabaseDateFromSessionDate($_SESSION['EditLeave'][5]).'", Leave_End = "'.GetDatabaseDateFromSessionDate($_SESSION['EditLeave'][6]).'", Leave_Days = "'.$_POST['Days'].'", Leave_Doctor = "'.$_POST['Doctor'].'", Leave_Comments = "'.$_POST['Description'].'" WHERE Leave_ID = "'.$_SESSION['EditLeave'][0].'"'))
    	    {
            $rowStaff = MySQL_Fetch_Array(ExecuteQuery('SELECT Staff_First_Name, Staff_Last_Name, Staff_email FROM Staff WHERE Staff_Code = "'.$_SESSION['EditLeave'][1].'"'));
            $rowAuth = MySQL_Fetch_Array(ExecuteQuery('SELECT Staff_First_Name, Staff_Last_Name, Staff_email FROM Staff WHERE Staff_Code = "'.$_POST['Authorise'].'"'));
						$rowAndrew = MySQL_Fetch_Array(ExecuteQuery('SELECT Staff_First_Name, Staff_Last_Name, Staff_email FROM Staff WHERE Staff_Code = 015'));
            $rowType = MySQL_Fetch_Array(ExecuteQuery('SELECT LeaveType_Description FROM LeaveType WHERE LeaveType_ID = "'.$_POST['Leave'].'"'));
            
            $email = 'Leave has been updated. The details of the updated leave are as follows:'.Chr(10).
                     'NAME:                  '.$rowStaff['Staff_First_Name'].' '.$rowStaff['Staff_Last_Name'].Chr(10).
                     'LEAVE TYPE:            '.$rowType['LeaveType_Description'].Chr(10).
                     'LEAVE PERIOD:          '.GetTextualDateFromSessionDate($_SESSION['EditLeave'][5]).' until '.GetTextualDateFromSessionDate($_SESSION['EditLeave'][6]).Chr(10).
                     'DAYS:                  '.SPrintF('%02.2f', $_POST['Days']).Chr(10).
                     'DOCTOR\'S CERTIFICATE:  '.($_POST['Doctor'] ? 'Yes' : 'No').Chr(10).
                     'DESCRIPTION:           '.$_POST['Description'];
            
            $html = 'Leave has been updated. The details of the updated leave are as follows:
                    <BR /><BR />
                    <TABLE border=0>
                      <TR><TD><B>Name:</B></TD><TD>'.$rowStaff['Staff_First_Name'].' '.$rowStaff['Staff_Last_Name'].'</TD></TR>
                      <TR><TD><B>Leave Type:</B></TD><TD>'.$rowType['LeaveType_Description'].'</TD></TR>
                      <TR><TD><B>Leave Period:</B></TD><TD>'.GetTextualDateFromSessionDate($_SESSION['EditLeave'][5]).' until '.GetTextualDateFromSessionDate($_SESSION['EditLeave'][6]).'</TD></TR>
                      <TR><TD><B>Days:</B></TD><TD>'.SPrintF('%02.2f', $_POST['Days']).'</TD></TR>
                      <TR><TD><B>Doctor\'s Certificate:</B></TD><TD>'.($_POST['Doctor'] ? 'Yes' : 'No').'</TD></TR>
                      <TR><TD><B>Description:</B></TD><TD>'.$_POST['Description'].'</TD></TR>
                    </TABLE>';
            
            SendMailHTML($rowStaff['Staff_email'], 'Leave Updated', $email, $html);
            if($_POST['Leave'] == '2')
                SendMailHTML('humanresources@s4integration.co.za', 'Leave Updated', $email, $html);
            else
                SendMailHTML($rowAuth['Staff_email'], 'Leave Updated', $email, $html);
            
	    SendMailHTML($rowAndrew['Staff_email'], 'Leave Updated', $email, $html);
            $_SESSION['LeaveSuccess'] = 'geh!';
            Session_Unregister('EditLeave');
          } else
            $_SESSION['LeaveFail'] = 'geh!';
        } else
          $_SESSION['LeaveIncomplete'] = 'geh!';
        break;
      default:
        break;
    }
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Handles the user's maintenance selection.                                //
  //////////////////////////////////////////////////////////////////////////////
  function HandleMaintain()
  {
    switch ($_POST['Submit'])
    {
      case 'Allocate':
        $_SESSION['AllocateLeave'] = array();
        break;
      case 'Approve':
        if ($_POST['ApproveLeave'] == "")
          $_SESSION['LeaveIncomplete'] = 'geh!';
        else
          $_SESSION['ApproveLeave'] = array($_POST['ApproveLeave']);
        break;
      case 'Cancel':
        if ($_POST['CancelLeave'] == "")
          $_SESSION['LeaveIncomplete'] = 'geh!';
        else
          $_SESSION['CancelLeave'] = array($_POST['CancelLeave']);
        break;
      case 'Edit':
        if ($_POST['EditLeave'] == "")
          $_SESSION['LeaveIncomplete'] = 'geh!';
        else
        {
          $row = MySQL_Fetch_Array(ExecuteQuery('SELECT `Leave`.*, Staff_First_Name, Staff_Last_Name FROM `Leave`, Staff WHERE Leave_Employee = Staff_Code AND Leave_ID = "'.$_POST['EditLeave'].'"'));
          $_SESSION['EditLeave'] = array($_POST['EditLeave']);
          $_SESSION['EditLeave'][1] = $row['Leave_Employee'];
          $_SESSION['EditLeave'][2] = $row['Leave_Approved'];
          $_SESSION['EditLeave'][3] = $row['Leave_Comments'];
          $_SESSION['EditLeave'][4] = $row['Leave_Type'];
          $_SESSION['EditLeave'][5] = GetSessionDateFromDatabaseDate($row['Leave_Start']);
          $_SESSION['EditLeave'][6] = GetSessionDateFromDatabaseDate($row['Leave_End']);
          $_SESSION['EditLeave'][7] = $row['Leave_Days'];
          $_SESSION['EditLeave'][8] = $row['Leave_Doctor'];
          $_SESSION['EditLeave'][100] = $row['Staff_First_Name'].' '.$row['Staff_First_Name'].' - '.GetTextualDateFromDatabaseDate($row['Leave_Start']).' to '.GetTextualDateFromDatabaseDate($row['Leave_End']);
        }
        break;
      case 'Request':
        if ($_SESSION['cAuth'] & 8)
        {
          if ($_POST['RequestLeave'] == "")
            $_SESSION['LeaveIncomplete'] = 'geh!';
          else
            $_SESSION['RequestLeave'] = array($_POST['RequestLeave']);
        } else
          $_SESSION['RequestLeave'] = array($_SESSION['cUID']);
        break;
      default:
        break;
    }
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Handles the user's submission of information for a leave request.        //
  //////////////////////////////////////////////////////////////////////////////
  function HandleRequest()
  {
    $_SESSION['RequestLeave'][1] = $_POST['Authorise'];
    $_SESSION['RequestLeave'][2] = $_POST['Description'];
    $_SESSION['RequestLeave'][3] = $_POST['Leave'];
    $_SESSION['RequestLeave'][4] = $_POST['StartYear'].$_POST['StartMonth'].$_POST['StartDay'];
    $_SESSION['RequestLeave'][5] = $_POST['EndYear'].$_POST['EndMonth'].$_POST['EndDay'];
    $_SESSION['RequestLeave'][6] = $_POST['Days'];
    $_SESSION['RequestLeave'][7] = $_POST['Doctor'];
    
    switch ($_POST['Submit'])
    {
      case 'Cancel':
        Session_Unregister('RequestLeave');
        break;
      case 'Submit':
        if (CheckFields())
        {
          if (ExecuteQuery('INSERT INTO `Leave` VALUES("", "'.$_SESSION['RequestLeave'][0].'", "'.$_POST['Authorise'].'", "'.$_POST['Leave'].'", "'.GetDatabaseDate($_POST['StartDay'], $_POST['StartMonth'], $_POST['StartYear']).'", "'.GetDatabaseDate($_POST['EndDay'], $_POST['EndMonth'], $_POST['EndYear']).'", "'.$_POST['Days'].'", "'.$_POST['Doctor'].'", "'.$_POST['Description'].'", "0", "'.Date('Y-m-d H:i:s').'", "")'))
    	    {
            $row = MySQL_Fetch_Array(ExecuteQuery('SELECT Staff_First_Name, Staff_Last_Name, Staff_email FROM Staff WHERE Staff_Code = "'.$_SESSION['RequestLeave'][0].'"'));
            $rowTempB = MySQL_Fetch_Array(ExecuteQuery('SELECT Staff_First_Name, Staff_Last_Name, Staff_email FROM Staff WHERE Staff_Code = "'.$_POST['Authorise'].'"'));
            $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT LeaveType_Description FROM LeaveType WHERE LeaveType_ID = "'.$_POST['Leave'].'"'));
            $rowTempC = MySQL_Fetch_Array(ExecuteQuery('SELECT `Leave`.* FROM `Leave` WHERE Leave_Type = "'.$_POST['Leave'].'" AND Leave_Employee = "'.$_SESSION['RequestLeave'][0].'" AND Leave_IsApproved = "0" ORDER BY Leave_ID DESC'));

            //Sixolise
            $row1 = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = '.$_SESSION['RequestLeave'][0].''));
            $staffStartDate = $row1['Staff_Start_Date'];
            $OriginalDate = $staffStartDate;
            $flag=false;
            $cycleCount=1;
            $currentYear = Date('Y-m-d');
            $startYear = Date('Y-m-d', MkTime(0, 0, 0, SubStr($staffStartDate, 5, 2), SubStr($staffStartDate, 8, 2), SubStr($staffStartDate, 0, 4)));
            
            while(!$flag)
                    {
                        $nextCycle = Date('Y-m-d', MkTime(0, 0, 0, SubStr($startYear, 5, 2), SubStr($startYear, 8, 2), SubStr($startYear, 0, 4)+1));
                        
                        $cycleEnd = $nextCycle;
                        if($nextCycle >= $currentYear)
                        {
                            $flag =true;
                            $staffStartDate = $startYear;
                            $cycleEnd = $currentYear;
                        }
                        else $startYear = $nextCycle;

                    }

                $rowSickCount = MySQL_Fetch_Array(ExecuteQuery('SELECT SUM(Leave_Days) AS SickTaken FROM `Leave`
                                                                                WHERE   Leave_Employee = "'.$_SESSION['RequestLeave'][0].'" AND Leave_IsApproved = "1" 
                                                                                AND Leave_Type = "2" AND (Leave_Start BETWEEN "'.$startYear.'" AND "'.$cycleEnd.'" OR Leave_End BETWEEN "'.$startYear.'" AND "'.$cycleEnd.'") ORDER BY Leave_Start'));

                       $newSickDays = $rowSickCount['SickTaken'] + $_POST['Days'];

                       if($newSickDays >= 10.00 && $newSickDays < 13.00 ){
                           sendConcerningEmail($row1['Staff_First_Name'],$row1['Staff_Last_Name'],$startYear,$rowSickCount['SickTaken'],$_POST['Days']);
                       }
                       else if($newSickDays >= 20.00 && $newSickDays < 23.00){
                           sendConcerningEmail($row1['Staff_First_Name'],$row1['Staff_Last_Name'],$startYear,$rowSickCount['SickTaken'],$_POST['Days']);

                       }
                       else if($newSickDays >=30){
                           sendConcerningEmail($row1['Staff_First_Name'],$row1['Staff_Last_Name'],$startYear,$rowSickCount['SickTaken'],$_POST['Days']);
                       }

            
            $email = 'Leave has been requested which requires your authorisation. It can be authorised using the intranet or by clicking the links at the bottom of this mail. The details of the requested leave are as follows:'.Chr(10).
                     'NAME:                  '.$row1['Staff_First_Name'].' '.$row1['Staff_Last_Name'].Chr(10).
                     'LEAVE TYPE:            '.$rowTemp['LeaveType_Description'].Chr(10).
                     'LEAVE PERIOD:          '.GetTextualDate($_POST['StartDay'], $_POST['StartMonth'], $_POST['StartYear']).' until '.GetTextualDate($_POST['EndDay'], $_POST['EndMonth'], $_POST['EndYear']).Chr(10).
                     'DAYS:                  '.SPrintF('%02.2f', $_POST['Days']).Chr(10).
                     'DOCTOR\'S CERTIFICATE:  '.($_POST['Doctor'] ? 'Yes' : 'No').Chr(10).
                     'DESCRIPTION:           '.$_POST['Description'];
            
            $html = 'Leave has been requested which requires your authorisation. It can be authorised using the intranet or by clicking the links at the bottom of this mail. The details of the requested leave are as follows:
                    <BR /><BR />
                    <TABLE border=0>
                      <TR><TD><B>Name:</B></TD><TD>'.$row1['Staff_First_Name'].' '.$row1['Staff_Last_Name'].'</TD></TR>
                      <TR><TD><B>Leave Type:</B></TD><TD>'.$rowTemp['LeaveType_Description'].'</TD></TR>
                      <TR><TD><B>Leave Period:</B></TD><TD>'.GetTextualDate($_POST['StartDay'], $_POST['StartMonth'], $_POST['StartYear']).' until '.GetTextualDate($_POST['EndDay'], $_POST['EndMonth'], $_POST['EndYear']).'</TD></TR>
                      <TR><TD><B>Days:</B></TD><TD>'.SPrintF('%02.2f', $_POST['Days']).'</TD></TR>
                      <TR><TD><B>Doctor\'s Certificate:</B></TD><TD>'.($_POST['Doctor'] ? 'Yes' : 'No').'</TD></TR>
                      <TR><TD><B>Description:</B></TD><TD>'.$_POST['Description'].'</TD></TR>
                    </TABLE>';
            
            $email .= Chr(10).Chr(10).Chr(10).Chr(10).Chr(10).Chr(10).Chr(10).'___________________________________'.Chr(10).Chr(10).$rowTempB['Staff_First_Name'].' '.$rowTempB['Staff_Last_Name'];
            $html .= '<BR /><BR /><BR /><BR /><BR /><BR /><BR />___________________________________<BR /><BR />'.$rowTempB['Staff_First_Name'].' '.$rowTempB['Staff_Last_Name'];
            
            $rand = Rand();
            while (StrLen($rand) != 5)
            {
              if (StrLen($rand) > 5)
                $rand = SubStr($rand, 0, 5);
              else
                $rand = Rand();
            }
            $email .= Chr(10).Chr(10).Chr(10).'Approve or deny this request here '.getS4ExternalLink().'/cobotsweb/Leave.php?id='.$rand.$rowTempC['Leave_ID'].' here. If the leave is approved, print and sign a copy of this and ensure admin gets it.';
            $html .= '<BR /><BR /><BR />Approve or deny this request <A href="'.getS4ExternalLink().'/cobotsweb/Leave.php?id='.$rand.$rowTempC['Leave_ID'].'">here</A>. If the leave is approved, print and sign a copy of this and ensure admin gets it.';
            
            if($_POST['Leave'] == '2')
                SendMailHTML('humanresources@s4integration.co.za', 'Leave Requested', $email, $html); //
            else    
                SendMailHTML($rowTempB['Staff_email'], 'Leave Requested', $email, $html);
            
            $_SESSION['LeaveSuccess'] = 'geh!';
            Session_Unregister('RequestLeave');
          } else
            $_SESSION['LeaveFail'] = 'geh!';
        } else
          $_SESSION['LeaveIncomplete'] = 'geh!';
        break;
      default:
        break;
    }
  }


  function sendConcerningEmail($staffName,$lastName,$startYear,$noOfDaysSick,$daysRequested){

      $email = 'The employee  below has reached their Sick days limit for their cycle '.Chr(10).
          'NAME:         '.$staffName.' '.$lastName.Chr(10).
          'CYCLE STARTED:'.$startYear.Chr(10).
          'SICK TAKEN: '.$noOfDaysSick.Chr(10). //should be current balance
          'REQUESTING: '.$daysRequested.Chr(10).'(days)';

      $contents2html = 'The employee  below has reached their Sick days limit for their cycle</br></br>
                           <TABLE border=0>
                                    <TR><TD><B>NAME:</B></TD><TD>'.$staffName.'</TD></TR>
                                    <TR><TD><B>CYCLE STARTED:</B></TD><TD>'.$startYear.'</TD></TR>
                                    <TR><TD><B>SICK TAKEN:</B></TD><TD>'.$noOfDaysSick.'</TD></TR>
                                    <TR><TD><B>REQUESTING:</B></TD><TD>'.$daysRequested.'</TD></TR>
                                </TABLE>';

      SendMailHTML('humanresources@s4integration.co.za', 'Concerning Sick Leave Request', $email, $contents2html);

  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Handles the user's submission of leave selection.                        //
  //////////////////////////////////////////////////////////////////////////////
  function HandleView()
  {
    if (CheckFields())
    {
      switch ($_POST['Type'])
      {
        case 'Cycle':
            $_SESSION['ViewLeaveCycle'] = array($_POST['Cycles']);
            if ($_SESSION['cAuth'] & 8)
              $_SESSION['ViewLeaveCycle'][1] = $_POST['Staff'];
            else
              $_SESSION['ViewLeaveCycle'][1] = $_SESSION['cUID'];
          break;
        default :
            $_SESSION['ViewLeave'] = array($_POST['StartYear'].$_POST['StartMonth'].$_POST['StartDay']);
            $_SESSION['ViewLeave'][1] = $_POST['EndYear'].$_POST['EndMonth'].$_POST['EndDay'];
            $_SESSION['ViewLeave'][2] = $_POST['Leave'];
            if ($_SESSION['cAuth'] & 8)
              $_SESSION['ViewLeave'][3] = $_POST['Staff'];
            else
              $_SESSION['ViewLeave'][3] = $_SESSION['cUID'];
        break;
      }
    } else
      $_SESSION['LeaveIncomplete'] = 'geh!';
  }
?>