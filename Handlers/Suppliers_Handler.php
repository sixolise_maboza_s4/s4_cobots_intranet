<?php
  //////////////////////////////////////////////////////////////////////////////
  // This page handles the back-end for the Suppliers page.                   //
  /////////////////////////////////////////////////////////////////////////////
  include '../Scripts/Include.php';
  ob_start("ob_gzhandler");
  SetSettings();
  CheckLoggedIn();
  $_POST = Replace('"', '\'\'', $_POST);
 // print_r("date: ".$_SESSION['EditSupplier'][2]." level: ".$_SESSION['EditSupplier'][1]);
  
  
  
if(isset($_POST['function'])){
    //getSuplierCode() doCheck() getMultipleOrderView()
     switch ($_POST['function']){
       case 'certificates':
            getCertificates() ;
            break;
        case 'NOcertificates':
            getNOcertificates() ;
            break;
        case 'getDCSuppliers':
             echo json_encode(getDCSuppliers());
            break;  
         case 'updateDCSuppliers':
            updateDCSuppliers() ;
            break;
        default:
            break;
     }
}

else{
 
  switch ($_POST['Type'])
  {
    //User has submitted information for a new supplier.
    case 'Add':
      HandleAdd();
    	break;
    //User has submitted modified information for a supplier.
    case 'Edit':
      HandleEdit();
    	break;
    //User has selected to Add or Edit a supplier.
    case 'Maintain':
      HandleMaintain();
    	break;
    //User has selected to view a supplier.
    case 'ViewSingle':
    //User has selected to view suppliers.
    case 'ViewList':
      HandleView();
    	break;
    case 'ManageBEECertificates':
        Handle_certificate();
        break;
    case 'ManageBEECertificates_Edit':
        HandleEdit_certificate();
        break;
    case 'ManageBEECertificates_Add':
        HandleAdd_certificate();
        break;
    case 'ViewBEESingle':
        HandleViewBEESingle();
        break;
    case 'ViewBEEList':
        HandleViewBEEList();
        break;
    case 'Remove':
        HandleRemove();
        break;
    case 'Export':
        HandleDownload();
        break;
     case 'DCSuppliers':
        unset($_SESSION['DCSuppliersInternalOrders']);
        break;
    //User has reached this page incorrectly. If they are not authorised they are redirected to the main page from the Suppliers page.
    default:
    	break;
  }
  Header('Location: ../Suppliers.php?'.Rand());
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Checks that all the required fields have values and that these values    //
  // are valid.                                                               //
  //////////////////////////////////////////////////////////////////////////////
  function CheckFields()
  {
    switch ($_POST['Type'])
    {
      case 'Add':
      case 'Edit':
        if (($_POST['CompanyName'] == "") || ($_POST['Description'] == "") || ($_POST['Contact'] == ""))
          return false;
        break;
      case 'ViewSingle':
        if ($_POST['Supplier'] == "")
          return false;
        break;
      case 'ViewList':
        break;
      case 'ManageBEECertificates_Add':
    if (($_POST['Supplier'] == "") || (date('Y-m-d', strtotime($_POST['dateselected'])) == date("Y-m-d",time())) || ($_FILES['Upload']['tmp_name'] == "") )
               return false;
        break;
     case 'ManageBEECertificates':
        if (($_POST['Supplier'] == ""))
               return false;
        break;
         case 'ViewBEESingle':
        if (($_POST['Supplier'] == "")){
               return false;
        }
        break;
      default:
        return false;
        break;
    }
    
    return true;
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Handles the user's submission of information for a new supplier.         //
  //////////////////////////////////////////////////////////////////////////////
  function HandleAdd()
  {
    $_SESSION['AddSupplier'][0] = $_POST['CompanyName'];
    $_SESSION['AddSupplier'][1] = $_POST['Description'];
    $_SESSION['AddSupplier'][2] = $_POST['Payment'];
    $_SESSION['AddSupplier'][3] = $_POST['Account'];
    $_SESSION['AddSupplier'][4] = $_POST['Contact'];
    $_SESSION['AddSupplier'][5] = $_POST['ContactWork'];
    $_SESSION['AddSupplier'][6] = $_POST['Email'];
    $_SESSION['AddSupplier'][7] = $_POST['ContactFax'];
    $_SESSION['AddSupplier'][8] = $_POST['PhysicalStreet1'];
    $_SESSION['AddSupplier'][9] = $_POST['PhysicalTownCity'];
    $_SESSION['AddSupplier'][10] = $_POST['PhysicalStreet2'];
    $_SESSION['AddSupplier'][11] = $_POST['PhysicalCountry'];
    $_SESSION['AddSupplier'][12] = $_POST['PhysicalSuburb'];
    $_SESSION['AddSupplier'][13] = $_POST['PhysicalPostalCode'];
    $_SESSION['AddSupplier'][14] = $_POST['PostalStreet1'];
    $_SESSION['AddSupplier'][15] = $_POST['PostalTownCity'];
    $_SESSION['AddSupplier'][16] = $_POST['PostalStreet2'];
    $_SESSION['AddSupplier'][17] = $_POST['PostalCountry'];
    $_SESSION['AddSupplier'][18] = $_POST['PostalSuburb'];
    $_SESSION['AddSupplier'][19] = $_POST['PostalPostalCode'];
    $_SESSION['AddSupplier'][20] = $_POST['SupplierCategory'];
    $_SESSION['AddSupplier'][21] = $_POST['SupplierBEElevel'];
    $_SESSION['AddSupplier'][22] = $_POST['DCSupplier'];
    
    switch ($_POST['Submit'])
    {
      case 'Cancel':
        Session_Unregister('AddSupplier');
        break;
      case 'Submit':
        if (CheckFields())
        {
          //Check no duplicate supplier exists.
          if (MySQL_Num_Rows(ExecuteQuery('SELECT * FROM Supplier WHERE LOWER(Supplier_Name) = "'.StrToLower($_POST['CompanyName']).'"')) == 0)
          {
               $DCsupplier = 'false';
              if($_POST['DCSupplier']){
                  $DCsupplier = 'true';
              }
		
              
            if (ExecuteQuery('INSERT INTO Supplier VALUES(null, "'.$_POST['CompanyName'].'", "'.$_POST['ContactWork'].'", "'.$_POST['ContactFax'].'", "'.$_POST['Email'].'", "'.$_POST['Contact'].'", "'.$_POST['PhysicalStreet1'].'", "'.$_POST['PhysicalSuburb'].'", "'.$_POST['PhysicalTownCity'].'", "'.$_POST['PhysicalCountry'].'", "'.$_POST['PhysicalPostalCode'].'", "'.$_POST['Payment'].'", "", "'.$_POST['PhysicalStreet2'].'", "'.$_POST['Description'].'", "'.$_POST['Account'].'", "'.$_POST['PostalStreet1'].'", "'.$_POST['PostalStreet2'].'", "'.$_POST['PostalSuburb'].'", "'.$_POST['PostalTownCity'].'", "'.$_POST['PostalCountry'].'", "'.$_POST['PostalPostalCode'].'", "'.$_POST['SupplierCategory'].'",'.$DCsupplier.',"0")'))
    	      {
              if ($_SESSION['UID'] != '32')
              {
                $row = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = "'.$_SESSION['UID'].'"'));
                
                $email = 'Supplier details have been added. The details are as follows:'.Chr(10).
                         'SUPPLIER:              '.$_POST['CompanyName'].Chr(10).
                         'ADDED BY:              '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].Chr(10).Chr(10).
                         'Please review the new details to ensure that everything is in order.';
                
                $html = 'Supplier details have been added. The details are as follows:
                        <BR /><BR />
                        <TABLE border=0>
                          <TR><TD><B>Supplier:</B></TD><TD>'.$_POST['CompanyName'].'</TD></TR>
                          <TR><TD><B>Added By:</B></TD><TD>'.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].'</TD></TR>
                        </TABLE>
                        <BR />
                        Please review the new details to ensure that everything is in order.
                        <BR /><BR />';
                
                SendMailHTML('cherie@s4integration.co.za', 'Supplier Added', $email, $html);
              }
              
              $_SESSION['SupplierSuccess'] = 'geh!';
              Session_Unregister('AddSupplier');
            } else
              $_SESSION['SupplierFail'] = 'geh!';
          } else
    	      $_SESSION['SupplierFail'] = 'geh!';
        } else
          $_SESSION['SupplierIncomplete'] = 'geh!';
        break;
      default:
        break;
    }
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Handles the user's submission of information for a new supplier.         //
  //////////////////////////////////////////////////////////////////////////////
  function HandleAdd_certificate(){
    
    $_SESSION['AddBEECertificate'][0] = $_POST['Supplier'];
    $_SESSION['AddBEECertificate'][1] = $_POST['Level'];     
    $_SESSION['AddBEECertificate'][2] = $_POST['dateselected']; 
    $_SESSION['AddBEECertificate'][3] = $_POST['Upload'];   
    
    switch ($_POST['Submit'])
    {
      
      case 'Cancel':
          unset($_SESSION['AddBEECertificate']);
          break;
      case 'Submit':

     if (CheckFields()) {
        
         //  make the folder in the server
           if(!file_exists ( '../Files/Intranet/BeeCertificate')){  
                mkdir('../Files/Intranet/BeeCertificate',07777); 
           }  
         
           $info = PathInfo($_FILES['Upload']['name']);
     
          if (CheckFile(StrToLower($info['extension'])))
          {
            
            $filename = Str_Replace(" ", "_", $info['basename']);
             $row = MySQL_Fetch_Array(ExecuteQuery('SELECT `Supplier_Name` FROM Supplier WHERE `Supplier_Code` = '.$_SESSION['AddBEECertificate'][0]));
              $convert =  iconv('Windows-1252', 'UTF-8//TRANSLIT',$row['Supplier_Name']);
              $name =  $convert.".".StrToLower($info['extension']);
               
            
            $resultSet = ExecuteQuery('SELECT File FROM `BEE_Certificate` WHERE File = "'.$name.'"');
            
            if (MySQL_Num_Rows($resultSet) > 0){
             
                $_SESSION['FileExist'] = 'geh!';
            }
            
            else{ 
                
              
              if (Move_Uploaded_File($_FILES['Upload']['tmp_name'], '../Files/Intranet/BeeCertificate/'.$name)){
                 
                  if( ExecuteQuery('INSERT INTO `BEE_Certificate`(`Id`, `Level`, `Expiry_date`, `Supplier_code`, `File`) VALUES (null,'.$_SESSION['AddBEECertificate'][1].',"'.$_SESSION['AddBEECertificate'][2].'",'.$_SESSION['AddBEECertificate'][0].',"'.$name.'")'))
        	    {
                
                  
                $_SESSION['SupplierSuccess'] = 'geh!';
                Session_Unregister('AddBEECertificate');
            } }else
                $_SESSION['SupplierFail'] = 'geh!';
            }
          } else
            $_SESSION['SupplierSizeType'] = 'geh!';
       } else
        $_SESSION['SupplierIncomplete'] = 'geh!';
           
          break;
      default: 
        break;
    }
  }
  
  function CheckFile($Extension)
  {
    //Size is 1048576 * NumberOfMB.
    $isValid = true;
    if (FileSize($_FILES['Upload']['tmp_name']) <= 5242880)
      $isValid =  true;
    else
      $isValid = false;
    
    
    if($isValid)
        if($Extension != "pdf" && $Extension != "doc" && $Extension != "docx")
            $isValid = false;
        
        
    return $isValid;
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Handles the user's submission of modified information for a supplier.    //
  //////////////////////////////////////////////////////////////////////////////
  function HandleEdit()
  {
    $_SESSION['EditSupplier'][1] = $_POST['CompanyName'];
    $_SESSION['EditSupplier'][2] = $_POST['Description'];
    $_SESSION['EditSupplier'][3] = $_POST['Payment'];
    $_SESSION['EditSupplier'][4] = $_POST['Account'];
    $_SESSION['EditSupplier'][5] = $_POST['Contact'];
    $_SESSION['EditSupplier'][6] = $_POST['ContactWork'];
    $_SESSION['EditSupplier'][7] = $_POST['Email'];
    $_SESSION['EditSupplier'][8] = $_POST['ContactFax'];
    $_SESSION['EditSupplier'][9] = $_POST['PhysicalStreet1'];
    $_SESSION['EditSupplier'][10] = $_POST['PhysicalTownCity'];
    $_SESSION['EditSupplier'][11] = $_POST['PhysicalStreet2'];
    $_SESSION['EditSupplier'][12] = $_POST['PhysicalCountry'];
    $_SESSION['EditSupplier'][13] = $_POST['PhysicalSuburb'];
    $_SESSION['EditSupplier'][14] = $_POST['PhysicalPostalCode'];
    $_SESSION['EditSupplier'][15] = $_POST['PostalStreet1'];
    $_SESSION['EditSupplier'][16] = $_POST['PostalTownCity'];
    $_SESSION['EditSupplier'][17] = $_POST['PostalStreet2'];
    $_SESSION['EditSupplier'][18] = $_POST['PostalCountry'];
    $_SESSION['EditSupplier'][19] = $_POST['PostalSuburb'];
    $_SESSION['EditSupplier'][20] = $_POST['PostalPostalCode'];
    $_SESSION['EditSupplier'][21] = $_POST['SupplierCategory'];
    $_SESSION['EditSupplier'][23] = $_POST['SupplierIsAInactive'];

   
    switch ($_POST['Submit'])
    {
      case 'Cancel':
        Session_Unregister('EditSupplier');
        break;
      case 'Submit':
        if (CheckFields())
        {
          //Check no duplicate supplier exists.
          if (MySQL_Num_Rows(ExecuteQuery('SELECT * FROM Supplier WHERE LOWER(Supplier_Name) = "'.StrToLower($_POST['CompanyName']).'" AND NOT Supplier_Code = "'.$_SESSION['EditSupplier'][0].'"')) == 0)
          {
             
              $DCsupplier = 'false';
              if($_POST['DCSupplier']){
                  $DCsupplier = 'true';
              }

//              $SupplierIsAInactive = 'false';//0
////              if($_POST['SupplierIsAInactive']){
////                  $SupplierIsAInactive = 'true';
////              }
            if (ExecuteQuery('UPDATE Supplier SET Supplier_IsInactive = "'.$_POST['SupplierIsAInactive'].'" ,Supplier_IsDC = '.$DCsupplier.' , Supplier_Name = "'.$_POST['CompanyName'].'", Supplier_Phone = "'.$_POST['ContactWork'].'", Supplier_Fax = "'.$_POST['ContactFax'].'", Supplier_Email = "'.$_POST['Email'].'", Supplier_Contact = "'.$_POST['Contact'].'", Supplier_Address_Street = "'.$_POST['PhysicalStreet1'].'", Supplier_Address_Suburb = "'.$_POST['PhysicalSuburb'].'", Supplier_Address_City = "'.$_POST['PhysicalTownCity'].'", Supplier_Address_Country = "'.$_POST['PhysicalCountry'].'", Supplier_Address_Code = "'.$_POST['PhysicalPostalCode'].'", Supplier_Payment_Method = "'.$_POST['Payment'].'", Supplier_Address_Street2 = "'.$_POST['PhysicalStreet2'].'", Supplier_Information = "'.$_POST['Description'].'", Supplier_Account = "'.$_POST['Account'].'", Postal_Address_Street = "'.$_POST['PostalStreet1'].'", Postal_Address_Street2 = "'.$_POST['PostalStreet2'].'", Postal_Address_Suburb = "'.$_POST['PostalSuburb'].'", Postal_Address_City = "'.$_POST['PostalTownCity'].'", Postal_Address_Country = "'.$_POST['PostalCountry'].'", Postal_Address_Code = "'.$_POST['PostalPostalCode'].'", Supplier_Category_id = "'.$_POST['SupplierCategory'].'" WHERE Supplier_Code = "'.$_SESSION['EditSupplier'][0].'"'))
    	      {
              if ($_SESSION['UID'] != '32')
              {
                $row = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = "'.$_SESSION['UID'].'"'));
                
                $email = 'Supplier details have been updated. The details are as follows:'.Chr(10).
                         'SUPPLIER:              '.$_POST['CompanyName'].Chr(10).
                         'UPDATED BY:            '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].Chr(10).Chr(10).
                         'Please review the updated details to ensure that everything is in order.';
                
                $html = 'Supplier details have been updated. The details are as follows:
                        <BR /><BR />
                        <TABLE border=0>
                          <TR><TD><B>Supplier:</B></TD><TD>'.$_POST['CompanyName'].'</TD></TR>
                          <TR><TD><B>Updated By:</B></TD><TD>'.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].'</TD></TR>
                        </TABLE>
                        <BR />
                        Please review the updated details to ensure that everything is in order.
                        <BR /><BR />';
                
                SendMailHTML('cherie@s4integration.co.za', 'Supplier Updated', $email, $html);
              }
              $_SESSION['SupplierSuccess'] = 'geh!';
              Session_Unregister('EditSupplier');
            } else
      	      $_SESSION['SupplierFail'] = 'geh!';
          } else
      	      $_SESSION['SupplierFail'] = 'geh!';          
        } else
          $_SESSION['SupplierIncomplete'] = 'geh!';
        break;
      default:
        break;
    }
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Handles the user's maintenance selection.                                //
  //////////////////////////////////////////////////////////////////////////////
  function HandleMaintain()
  {
    switch ($_POST['Submit'])
    {
      case 'Add':
        $_SESSION['AddSupplier'] = array();
        $_SESSION['AddSupplier'][5] = '+__-__-_______';
        $_SESSION['AddSupplier'][7] = '+__-__-_______';
        break;
      case 'Edit':
          if ($_POST['Supplier'] == "")
            $_SESSION['SupplierIncomplete'] = 'geh!';
          else
          {
            $row = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Supplier WHERE Supplier_Code = "'.$_POST['Supplier'].'"'));
            $_SESSION['EditSupplier'] = array($_POST['Supplier']);
            $_SESSION['EditSupplier'][1] = $row['Supplier_Name'];
            $_SESSION['EditSupplier'][2] = $row['Supplier_Information'];
            $_SESSION['EditSupplier'][3] = $row['Supplier_Payment_Method'];
            $_SESSION['EditSupplier'][4] = $row['Supplier_Account'];
            $_SESSION['EditSupplier'][5] = $row['Supplier_Contact'];
            $_SESSION['EditSupplier'][6] = $row['Supplier_Phone'];
            $_SESSION['EditSupplier'][7] = $row['Supplier_Email'];
            $_SESSION['EditSupplier'][8] = $row['Supplier_Fax'];
            $_SESSION['EditSupplier'][9] = $row['Supplier_Address_Street'];
            $_SESSION['EditSupplier'][10] = $row['Supplier_Address_City'];
            $_SESSION['EditSupplier'][11] = $row['Supplier_Address_Street2'];
            $_SESSION['EditSupplier'][12] = $row['Supplier_Address_Country'];
            $_SESSION['EditSupplier'][13] = $row['Supplier_Address_Suburb'];
            $_SESSION['EditSupplier'][14] = $row['Supplier_Address_Code'];
            $_SESSION['EditSupplier'][15] = $row['Postal_Address_Street'];
            $_SESSION['EditSupplier'][16] = $row['Postal_Address_City'];
            $_SESSION['EditSupplier'][17] = $row['Postal_Address_Street2'];
            $_SESSION['EditSupplier'][18] = $row['Postal_Address_Country'];
            $_SESSION['EditSupplier'][19] = $row['Postal_Address_Suburb'];
            $_SESSION['EditSupplier'][20] = $row['Postal_Address_Code'];
	        $_SESSION['EditSupplier'][21] = $row['Supplier_Category_ID'];
            $_SESSION['EditSupplier'][22] = $row['Supplier_IsDC'];
            $_SESSION['EditSupplier'][23] = $row['Supplier_IsInactive'];
            
          }
        break;
      case 'Manage':{
        $_SESSION['DCSuppliersInternalOrders'] = 'geh!';
      } 
      break;
      default:
        break;
    }
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Handles the user's submission of a supplier selection.                   //
  //////////////////////////////////////////////////////////////////////////////
  
  function Handle_certificate(){
   
      switch($_POST['Submit'])
      {
        case 'Add':    
             $_SESSION['AddBEECertificate'][0] = array();
            break;
        case 'Edit':
             if (CheckFields())
             {
                if ($_POST['Supplier'] == "")
                    $_SESSION['SupplierIncomplete'] = 'geh!';
                else
                {
                    $row = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM BEE_Certificate WHERE Supplier_Code = "'.$_POST['Supplier'].'"'));
                    $_SESSION['EditBEECertificate'][0] = $_POST['Supplier'];
                    $_SESSION['EditBEECertificate'][1] = $row['Level'];
                    $_SESSION['EditBEECertificate'][2] = $row['Expiry_date'];
                    $_SESSION['EditBEECertificate'][3] = $row['File'];
                }
                
              } else
                     $_SESSION['SupplierIncomplete'] = 'geh!';
          break;
          case 'Remove':
                 if ($_POST['Supplier'] == "")
                     $_SESSION['FileIncomplete'] = 'geh!';
                else{
                    $_SESSION['RemoveCertificate'][0] = $_POST['Supplier'];
                }
        default:
          break;
      } 
  }
  
  function HandleView()
  {
    if (CheckFields())
    {
      switch ($_POST['Type'])
      {
        case 'ViewSingle':
          $_SESSION['ViewSupplierSingle'] = array($_POST['Supplier']);
          break;
        case 'ViewList':
          $_SESSION['ViewSupplierList'] = array($_POST['SupplierCategory']);
          break;
        default:
          break;
      }
    } else
      $_SESSION['SupplierIncomplete'] = 'geh!';
  }

  function HandleEdit_certificate()
  {
    
        // supplier Id
      $_SESSION['EditBEECertificate'][1] = $_POST['Level'];
      $_SESSION['EditBEECertificate'][2] = $_POST['dateselected'];      
      $_SESSION['EditBEECertificate'][3] = $_POST['Upload'];     
      
      switch($_POST['Submit'])
      {
        case 'Cancel':    
            unset($_SESSION['EditBEECertificate']);
            break;
        case 'Submit':
        //check if the file needs to change
       
            
        if($_FILES['Upload']['tmp_name'] != ""){    
            // first remove the original file...    
         
            $resultSet = ExecuteQuery('SELECT File FROM `BEE_Certificate` WHERE `Supplier_code` = '.$_SESSION['EditBEECertificate'][0]);
            $row = MySQL_Fetch_Array($resultSet);          
            unlink('../Files/Intranet/BeeCertificate/'.$row['File']);
            
//            $fileNameTEST = str_replace(" ","",$row['File']);
//            $length = strlen ($fileNameTEST);
//          
//            $sub = substr($fileNameTEST , $length-12 , $length-1);
//           
//            if($sub === '_certificate'){
//                 $fileName = substr($fileNameTEST , 0 , $length-12);
//            }
//            
//            else{
//                 $fileName = $row['File'].'_certificate';
//            }
            
            // get part before . 
            
            $newname = explode('.',$row['File']);
            $info = PathInfo($_FILES['Upload']['name']);
            $fileName = $newname[0].".".StrToLower($info['extension']);

            //link the new file 
            if (CheckFile(StrToLower($info['extension']))){
                $filename = Str_Replace(" ", "_", $info['basename']);
                if (Move_Uploaded_File($_FILES['Upload']['tmp_name'], '../Files/Intranet/BeeCertificate/'.$fileName)){ 
                    if (ExecuteQuery('UPDATE BEE_Certificate SET Level ='.$_SESSION['EditBEECertificate'][1].' ,Expiry_date ="'.$_SESSION['EditBEECertificate'][2].'", File ="' .$fileName. '"  WHERE Supplier_Code ='.$_SESSION['EditBEECertificate'][0])){
                        unset($_SESSION['EditBEECertificate']);
                    }
                }
            }
            
        }
        //update the datebase  
        else{
           
             if (ExecuteQuery('UPDATE BEE_Certificate SET Level ='.$_SESSION['EditBEECertificate'][1].' ,Expiry_date ="'.$_SESSION['EditBEECertificate'][2].'" WHERE Supplier_Code ='.$_SESSION['EditBEECertificate'][0])){
               
                 unset($_SESSION['EditBEECertificate']);
             }
             else
                $_SESSION['SupplierFail'] = 'geh!';
        }
        break;
        default:
          break;
      }
  }
  
  function HandleViewBEESingle()
  {
    
       switch($_POST['Submit'])
      {
        case 'Cancel': 
                unset( $_SESSION['ViewSingleCertifiacte']);
                break;

        case 'View':               
            if (CheckFields())
            {
                $_SESSION['ViewSingleCertifiacte'][0] = $_POST['Supplier'];
                try{
                $row =   $row = MySQL_Fetch_Array(ExecuteQuery('SELECT `BEE_Certificate`.*, Supplier_name FROM `BEE_Certificate`, Supplier WHERE  `BEE_Certificate`.Supplier_code = Supplier.Supplier_code AND Supplier.Supplier_code ='.$_SESSION['ViewSingleCertifiacte'][0]));
                $_SESSION['ViewSingleCertifiacte'][1] = $row['Level'];
                $_SESSION['ViewSingleCertifiacte'][2] = $row['Expiry_date'];
                $_SESSION['ViewSingleCertifiacte'][3] = $row['Supplier_name'];   
                $_SESSION['ViewSingleCertifiacte'][4] =  iconv('UTF-8//TRANSLIT', 'Windows-1252',$row['File']);       
                }
                catch (Exception $e){
                    $_SESSION['SupplierFail'];
                }
            }

            else{
                $_SESSION['SupplierIncomplete'] = 'geh!';
            }
            break;
        default :
        break;
    }
  }
  
  function HandleViewBEEList()
  {
        $_SESSION['ViewBEECertificate_list'][0]  =  $_POST['Level'];
        
        if ($_POST['Submit']=="Back"){
           
            unset($_SESSION['ViewBEECertificate_list']);
        }
   
        
  } 
  
  function HandleRemove()
  {
    switch ($_POST['Submit'])
    {
      case 'No':
         
        unset($_SESSION['RemoveCertificate']);
        break;
      case 'Yes':
        $resultSet = ExecuteQuery('SELECT File FROM `BEE_Certificate` WHERE `Supplier_Code` = '.$_SESSION['RemoveCertificate'][0]);
       
        $row = MySQL_Fetch_Array($resultSet);          
       
        if (ExecuteQuery('DELETE FROM `BEE_Certificate` WHERE `Supplier_code` ='.$_SESSION['RemoveCertificate'][0]))
        {
            unlink('../Files/Intranet/BeeCertificate/'.$row['File']);
          $_SESSION['FileSuccess'] = 'geh!';
           unset($_SESSION['RemoveCertificate']);
        } else
          $_SESSION['SupplierFail'] = 'geh!';
        break;
      default:
        break;
    }
  } 
  
  function HandleDownload(){
      
         $filename = '../Files/Intranet/Bee_Certificates.zip';
      
        $rootPath = realpath('../Files/Intranet/BeeCertificate');
        $zip = new ZipArchive();
        $zip->open($filename, ZipArchive::CREATE | ZipArchive::OVERWRITE);

        // Create recursive directory iterator
        /** @var SplFileInfo[] $files */
        $files = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($rootPath),
            RecursiveIteratorIterator::LEAVES_ONLY
        );

        
        foreach ($files as $name => $file)
        {  
            // Skip directories (they would be added automatically)
            if (!$file->isDir())
            {   // Get real and relative path for current file
                $filePath = $file->getRealPath();
                $relativePath = substr($filePath, strlen($rootPath) + 1);
                // Add current file to archive
                $zip->addFile($filePath, $relativePath);
            }
        }
        // Zip archive will be created only after closing object
        $zip->close();

          chmod($filename, 0777);
        // make user download file: 
    
          if(!empty($filename)){
            // Specify file path.
            $path = ''; // '/uplods/'
            $download_file =  $path.$filename;
            // Check file is exists on given path.
            if(file_exists($download_file))
            {
              // Getting file extension.
              $extension = explode('.',$filename);
              $extension = $extension[count($extension)-1]; 
              // For Gecko browsers
              header('Content-Transfer-Encoding: binary');  
              header('Last-Modified: ' . gmdate('D, d M Y H:i:s', filemtime($path)) . ' GMT');
              // Supports for download resume
              header('Accept-Ranges: bytes');  
              // Calculate File size
              header('Content-Length: ' . filesize($download_file));  
              header('Content-Encoding: none');
              // Change the mime type if the file is not PDF
              header('Content-Type: application/'.$extension);  
              // Make the browser display the Save As dialog
              header('Content-Disposition: attachment; filename=' . $filename);  
              readfile($download_file); 
              unlink($filename);
              exit;
            }
          
 }
}

  function getDCSuppliers(){
      $sql2='SELECT S.*, C.Name FROM Supplier  S LEFT JOIN SupplierCategory C ON C.Supplier_Category_ID = S.Supplier_Category_ID ORDER BY Supplier_Name';
      $resultset = ExecuteQuery($sql2);
      $rows = array();
      if (  MySQL_Num_Rows($resultset) > 0){
         
           while ($row = MySQL_Fetch_Array($resultset)) { 
              $row['Supplier_name'] =  iconv('Windows-1252', 'UTF-8//TRANSLIT',$row['Supplier_name']);
               if($row['Supplier_IsDC'] === "1"){
                   $row['Supplier_IsDC'] = true;
               }
               else{
                   $row['Supplier_IsDC'] = false;
               }
               
              array_push($rows, $row);
           }
        }
       return $rows;  
  }
  
  function updateDCSuppliers(){

       if(isset($_POST['SupplierCode']) && isset($_POST['Value']) ){
            if(ExecuteQuery('UPDATE `Supplier` SET Supplier_IsDC = '.$_POST['Value'].' WHERE Supplier_Code = '.$_POST['SupplierCode'])){
                 if ($_SESSION['UID'] != '32')
                    {
                      $row = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = "'.$_SESSION['UID'].'"'));
                      $rowS = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Supplier WHERE Supplier_Code = '.$_POST['SupplierCode']));

                      $email = 'Supplier details have been updated. The details are as follows:'.Chr(10).
                               'SUPPLIER:              '.$rowS['Supplier_Name'].Chr(10).
                               'UPDATED BY:            '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].Chr(10).Chr(10).
                               'Please review the updated details to ensure that everything is in order.';

                      $html = 'Supplier details have been updated. The details are as follows:
                              <BR /><BR />
                              <TABLE border=0>
                                <TR><TD><B>Supplier:</B></TD><TD>'.$rowS['Supplier_Name'].'</TD></TR>
                                <TR><TD><B>Updated By:</B></TD><TD>'.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].'</TD></TR>
                              </TABLE>
                              <BR />
                              Please review the updated details to ensure that everything is in order.
                              <BR /><BR />';

                      SendMailHTML('cherie@s4integration.co.za', 'Supplier Updated', $email, $html);
                    }
                  $list = array(getDCSuppliers(),"pass");
                  echo json_encode($list);

            }
       
             else{
                 $list = array(getDCSuppliers(),"fail");
                 echo json_encode($list);
             }
        }
        else{
            $list = array(getDCSuppliers(),"fail");
              echo json_encode($list);
        }
      
  }

  function getCertificates(){
      $sql2='SELECT `BEE_Certificate`.* , `Supplier_name` FROM `BEE_Certificate`, Supplier WHERE Supplier.`Supplier_code` = `BEE_Certificate`.`Supplier_code`';
      $resultset = ExecuteQuery($sql2);
      $rows = array();
    
     if (  MySQL_Num_Rows($resultset) > 0){
         
           while ($row = MySQL_Fetch_Array($resultset)) { 
              $row['Supplier_name'] =  iconv('Windows-1252', 'UTF-8//TRANSLIT',$row['Supplier_name']);
              array_push($rows, $row);
           }
        }
   
       echo json_encode($rows);
  }
  
  function getNOcertificates(){
      
      //SELECT `Supplier_Name`, `Supplier_Phone`, `Supplier_Email` FROM `Supplier` WHERE `Supplier_Code` NOT IN ((SELECT `Supplier_code` FROM BEE_Certificate)) Order By Supplier_Name
    
    $sql2='SELECT `Supplier_Name`, `Supplier_Phone`, `Supplier_Email` FROM `Supplier` WHERE `Supplier_Code` NOT IN ((SELECT `Supplier_code` FROM BEE_Certificate)) Order By Supplier_Name';
    $resultset = ExecuteQuery($sql2);
    $rows = array();
    
     if (  MySQL_Num_Rows($resultset) > 0){
         
           while ($row = MySQL_Fetch_Array($resultset)) { 
              $row['Supplier_name'] =  iconv('Windows-1252', 'UTF-8//TRANSLIT',$row['Supplier_name']);
              array_push($rows, $row);
           }
        }
   
       echo json_encode($rows);
  }  
?>