<?php
  // DETAILS ///////////////////////////////////////////////////////////////////
  //                                                                          //
  //                    Last Edited By: Gareth Ambrose                        //
  //                        Date: 01 February 2008                            //
  //                                                                          //
  ////////////////////////////////////////////////////////////////////////////// 
  // This page handles the back-end for the News page.                        //
  //////////////////////////////////////////////////////////////////////////////
  
  include '../Scripts/Include.php';
  SetSettings();
  CheckLoggedIn();
  $_POST = Replace('"', '\'\'', $_POST);
  
  switch ($_POST['Type'])
  {
    //User has submitted information for new news.
    case 'Add':
      HandleAdd();
    	break;
    //User has selected to Add news.
    case 'Maintain':
      HandleMaintain();
    	break;
    //User has reached this page incorrectly. If they are not authorised they are redirected to the main page from the News page.
    default:
    	break;
  }
  Header('Location: ../News.php?'.Rand());
  
  //////////////////////////////////////////////////////////////////////////////
  // Checks that all the required fields have values and that these values    //
  // are valid.                                                               //
  //////////////////////////////////////////////////////////////////////////////
  function CheckFields()
  {
    switch ($_POST['Type'])
    {
      case 'Add':
        if (($_POST['Title'] == "") || ($_POST['Description'] == ""))
          return false;
      	break;
      default:
      	break;
    }
    
    return true;
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Handles the user's submission of information for a new meeting item.     //
  //////////////////////////////////////////////////////////////////////////////
  function HandleAdd() 
  {
    $_SESSION['AddNews'][0] = $_POST['Title'];
    $_SESSION['AddNews'][1] = $_POST['Description'];
    
    switch ($_POST['Submit'])
    {
      case 'Cancel':
        Session_Unregister('AddNews');
        break;
      case 'Submit':  
        if (CheckFields())
        {
          if (ExecuteQuery('INSERT INTO News VALUES("", "'.Date('Y-m-d H:i:s').'", "'.$_POST['Title'].'", "'.$_POST['Description'].'", "'.$_SESSION['cUID'].'")'))
    	    {
            $_SESSION['NewsSuccess'] = 'geh!';
            Session_Unregister('AddNews');
          } else
            $_SESSION['NewsFail'] = 'geh!';
        } else
          $_SESSION['NewsIncomplete'] = 'geh!';
        break;
      default:
        break;
    }
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Handles the user's maintenance selection.                                //
  //////////////////////////////////////////////////////////////////////////////
  function HandleMaintain()
  {
    switch ($_POST['Submit'])
    {
      case 'Add':
        $_SESSION['AddNews'] = array();
        break;
      default:
        break;
    }
  }
?>