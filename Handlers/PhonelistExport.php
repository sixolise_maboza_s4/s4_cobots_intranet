<?php
/**
 * Created by PhpStorm.
 * User: S4
 * Date: 2018/06/08
 * Time: 08:48
 */

require('../Scripts/PhonelistPdf.php');

$valueA =  stripslashes($_POST['SQlArray']);
$data = json_decode($valueA, true);
//print_r($data[0]["Location"]);

$columnHeader = array("Name ",
        'Ext.',
        'Tel.',
        'Cel',
        'Location',
        'Floor');



function setData($data){
    $userData = array();
    for($i = 0; $i< sizeof($data);$i++){
        $userData[] =array($data[$i]["Staff_member"],$data[$i]["Extension"],$data[$i]["Tel"],$data[$i]["Cel"],$data[$i]["Location"],$data[$i]["Floor"]);
    }
return $userData;
}


// Instanciation of inherited class
$pdf = new PhonelistPdf();
$pdf->AliasNbPages();
$pdf->AddFont('Roboto-Regular','','Roboto-Regular.php');
$header = $columnHeader;
$loadeddata = setData($data);
$pdf->SetFont('Roboto-Regular','',12);
$pdf->AddPage();
$pdf->printTable($header,$loadeddata);
$pdf->Output();
?>