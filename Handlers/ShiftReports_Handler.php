<?php
  //////////////////////////////////////////////////////////////////////////////
  // This page handles the back-end for the Shift Reports page.               //
  //////////////////////////////////////////////////////////////////////////////
  
  include '../Scripts/Include.php';
  SetSettings();
  CheckLoggedIn();
  $_POST = Replace('"', '\'\'', $_POST);
  
  switch ($_POST['Type'])
  {
    //User has submitted information for a shift report.
    case 'Add':
      HandleAdd();
    	break;
    //User has selected to Add a shift report.
    case 'Maintain':
      HandleMaintain();
      break;
    //User has selected to View shift reports.
    case 'ViewOverview':
    case 'ViewList':
      HandleView();
    	break;
    //User has reached this page incorrectly. If they are not authorised they are redirected to the main page from the Shift Reports page.
    default:
    	break;
  }
  Header('Location: ../ShiftReports.php?'.Rand());
  
  //////////////////////////////////////////////////////////////////////////////
  // Checks that all the required fields have values and that these values    //
  // are valid.                                                               //
  //////////////////////////////////////////////////////////////////////////////
  function CheckFields()
  {
    switch ($_POST['Type'])
    {
      case 'Add':
        if (($_POST['Customer'] == "") || ($_POST['Shift'] == "") || ($_POST['Description'] == ""))
          return false;
        
        if (!CheckDate($_POST['Month'], $_POST['Day'], $_POST['Year']))
          return false;
        break;
      case 'ViewList':
        if (!(CheckDate($_POST['StartMonth'], $_POST['StartDay'], $_POST['StartYear'])) || !(CheckDate($_POST['EndMonth'], $_POST['EndDay'], $_POST['EndYear'])))
          return false;
        
        if (DatabaseDateLater(GetDatabaseDate($_POST['StartDay'], $_POST['StartMonth'], $_POST['StartYear']), GetDatabaseDate($_POST['EndDay'], $_POST['EndMonth'], $_POST['EndYear'])))
          return false;
        break;
      case 'ViewOverview':
        if (!(CheckDate($_POST['StartMonth'], $_POST['StartDay'], $_POST['StartYear'])) || !(CheckDate($_POST['EndMonth'], $_POST['EndDay'], $_POST['EndYear'])))
          return false;
        
        if (DatabaseDateLater(GetDatabaseDate($_POST['StartDay'], $_POST['StartMonth'], $_POST['StartYear']), GetDatabaseDate($_POST['EndDay'], $_POST['EndMonth'], $_POST['EndYear'])))
          return false;
        break;
      default:
        return false;
        break;
    }
    
    return true;
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Handles the user's submission of information for a new shift report.     //
  //////////////////////////////////////////////////////////////////////////////
  function HandleAdd()
  {
    $_SESSION['AddShiftReport'][0] = $_POST['Year'].$_POST['Month'].$_POST['Day'];
    $_SESSION['AddShiftReport'][1] = $_POST['Customer'];
    $_SESSION['AddShiftReport'][2] = $_POST['Shift'];
    $_SESSION['AddShiftReport'][3] = $_POST['Description'];
    $_SESSION['AddShiftReport'][4] = $_POST['Team'];
    
    if(strcmp($_POST['Team'],"") == 0 ){
        $team = 'null';
    }
    else{
        $team = $_POST['Team'];
    }
    
    switch ($_POST['Submit'])
    {
      case 'Cancel':
        Session_Unregister('AddShiftReport');
        break;
      case 'Submit':
        if (CheckFields())
        {
          if (ExecuteQuery('INSERT INTO ShiftReport VALUES("", "'.$_SESSION['cUID'].'", "'.GetDatabaseDate($_POST['Day'], $_POST['Month'], $_POST['Year']).'", "'.$_POST['Customer'].'", "'.$_POST['Shift'].'", "'.$_POST['Description'].'",'.$team.')'))
    	    {
            $_SESSION['ShiftReportSuccess'] = 'geh!';
            Session_Unregister('AddShiftReport');
          } else
    	      $_SESSION['ShiftReportFail'] = 'geh!';
        } else
          $_SESSION['ShiftReportIncomplete'] = 'geh!';
        break;
      default:
        break;
    }
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Handles the user's maintenance selection.                                //
  //////////////////////////////////////////////////////////////////////////////
  function HandleMaintain()
  {
    switch ($_POST['Submit'])
    {
      case 'Add':
        $_SESSION['AddShiftReport'] = array();
        break;
      default:
        break;
    }
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Handles the user's submission of a shift report selection.               //
  //////////////////////////////////////////////////////////////////////////////
  function HandleView()
  {
    if (CheckFields())
    {
      switch ($_POST['Type'])
      {
        case 'ViewList':
          {
            $_SESSION['ViewShiftReportList'] = array($_POST['StartYear'].$_POST['StartMonth'].$_POST['StartDay']);
            $_SESSION['ViewShiftReportList'][1] = $_POST['EndYear'].$_POST['EndMonth'].$_POST['EndDay'];
            $_SESSION['ViewShiftReportList'][2] = $_POST['Staff'];
            $_SESSION['ViewShiftReportList'][3] = $_POST['Customer'];
            $_SESSION['ViewShiftReportList'][4] = $_POST['Description'];
            $_SESSION['ViewShiftReportList'][5] = $_POST['Team'];
            break;
          }
        case 'ViewOverview':
          {
            $_SESSION['ViewShiftReportOverview'] = array($_POST['StartYear'].$_POST['StartMonth'].$_POST['StartDay']);
            $_SESSION['ViewShiftReportOverview'][1] = $_POST['EndYear'].$_POST['EndMonth'].$_POST['EndDay'];
            $_SESSION['ViewShiftReportOverview'][2] = $_POST['Team'];
            break;
          }
        default:
          break;
      }
    } else
      $_SESSION['ShiftReportIncomplete'] = 'geh!';
  }
?>