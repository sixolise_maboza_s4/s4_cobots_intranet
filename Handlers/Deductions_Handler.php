<?php
  // DETAILS ///////////////////////////////////////////////////////////////////
  //                                                                          //
  //                    Last Edited By: Gareth Ambrose                        //
  //                        Date: 23 January 2008                             //
  //                                                                          //
  //////////////////////////////////////////////////////////////////////////////
  // This page handles the back-end for the Deductions page.                  //
  //////////////////////////////////////////////////////////////////////////////
  
  include '../Scripts/Include.php';
  SetSettings();
  CheckLoggedIn();
  $_POST = Replace('"', '\'\'', $_POST);
  
  switch ($_POST['Type'])
  {
    //User has submitted information for a deduction.
    case 'Add':
      HandleAdd();
    	break;
    //User has submitted modified information for a deduction.
    case 'Edit':
      HandleEdit();
    	break;
    //User has selected to Add, Edit or Remove a deduction.
    case 'Maintain':
      HandleMaintain();
    	break;
    //User has selected to remove a deduction.
    case 'Remove':
      HandleRemove();
    	break;
    //User has reached this page incorrectly. If they are not authorised they are redirected to the main page from the Deductions page.
    default:
    	break;
  }
  Header('Location: ../Deductions.php?'.Rand());
  
  //////////////////////////////////////////////////////////////////////////////
  // Checks that all the required fields have values and that these values    //
  // are valid.                                                               //
  //////////////////////////////////////////////////////////////////////////////
  function CheckFields()
  {
    switch ($_POST['Type'])
    {
      case 'Add':
      case 'Edit':
        if (($_POST['Description'] == "") || ($_POST['Value'] == "") || (!Is_Numeric($_POST['Value'])))
          return false;
        
        if (!CheckDate($_POST['Month'], $_POST['Day'], $_POST['Year']))
          return false;
        break;
      default:
        return false;
        break;
    }
    
    return true;
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Handles the user's submission of information for a new deduction.        //
  //////////////////////////////////////////////////////////////////////////////
  function HandleAdd()
  {
    $_SESSION['AddDeduction'][1] = $_POST['Year'].$_POST['Month'].$_POST['Day'];
    $_SESSION['AddDeduction'][2] = $_POST['Value'];
    $_SESSION['AddDeduction'][3] = $_POST['Description'];
  
    switch ($_POST['Submit'])
    {
      case 'Cancel':
        Session_Unregister('AddDeduction');
        break;
      case 'Submit':
        if (CheckFields())
        {
          if (ExecuteQuery('INSERT INTO Deduction VALUES("", "'.$_SESSION['AddDeduction'][0].'", "'.$_POST['Description'].'", "'.$_POST['Value'].'", "'.GetDatabaseDate($_POST['Day'], $_POST['Month'], $_POST['Year']).'", "0", "")'))
    	    {
            $_SESSION['DeductionSuccess'] = 'geh!';
            Session_Unregister('AddDeduction');
          } else
            $_SESSION['DeductionFail'] = 'geh!';
        } else
          $_SESSION['DeductionIncomplete'] = 'geh!';
        break;
      default:
        break;
    }
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Handles the user's submission of modified information for a deduction.   //
  //////////////////////////////////////////////////////////////////////////////
  function HandleEdit()
  {
    $_SESSION['EditDeduction'][1] = $_POST['Year'].$_POST['Month'].$_POST['Day'];
    $_SESSION['EditDeduction'][2] = $_POST['Value'];
    $_SESSION['EditDeduction'][3] = $_POST['Description'];
  
    switch ($_POST['Submit'])
    {
      case 'Cancel':
        Session_Unregister('EditDeduction');
        break;
      case 'Submit':
        if (CheckFields())
        {
          if (ExecuteQuery('UPDATE Deduction SET Deduction_Date = "'.GetDatabaseDate($_POST['Day'], $_POST['Month'], $_POST['Year']).'", Deduction_Description = "'.$_POST['Description'].'", Deduction_Value = "'.$_POST['Value'].'" WHERE Deduction_ID = "'.$_SESSION['EditDeduction'][0].'"'))
    	    {
            $_SESSION['DeductionSuccess'] = 'geh!';
            Session_Unregister('EditDeduction');
          } else
            $_SESSION['DeductionFail'] = 'geh!';
        } else
          $_SESSION['DeductionIncomplete'] = 'geh!';
        break;
      default:
        break;
    }
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Handles the user's maintenance selection.                                //
  //////////////////////////////////////////////////////////////////////////////
  function HandleMaintain()
  {
    switch ($_POST['Submit'])
    {
      case 'Add':
        if ($_SESSION['cAuth'] & 64)
          if ($_POST['Staff'] == "")
            $_SESSION['DeductionIncomplete'] = 'geh!';
          else
            $_SESSION['AddDeduction'] = array($_POST['Staff']);
        else
          $_SESSION['AddDeduction'] = array($_SESSION['cUID']);
        break;
      case 'Edit':
        if ($_POST['EditDeduction'] == "")
          $_SESSION['DeductionIncomplete'] = 'geh!';
        else
        {
          $row = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Deduction WHERE Deduction_ID = "'.$_POST['EditDeduction'].'"'));
          $_SESSION['EditDeduction'] = array($_POST['EditDeduction']);
          $_SESSION['EditDeduction'][1] = GetSessionDateFromDatabaseDate($row['Deduction_Date']);
          $_SESSION['EditDeduction'][2] = $row['Deduction_Value'];
          $_SESSION['EditDeduction'][3] = $row['Deduction_Description'];
        }
        break;
      case 'Remove':
        if ($_POST['RemoveDeduction'] == "")
          $_SESSION['DeductionIncomplete'] = 'geh!';
        else
          $_SESSION['RemoveDeduction'] = array($_POST['RemoveDeduction']);
        break;
      default:
        break;
    }
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Handles the user's submission to remove a deduction.                     //
  //////////////////////////////////////////////////////////////////////////////
  function HandleRemove()
  {
    switch ($_POST['Submit'])
    {
      case 'No':
        Session_Unregister('RemoveDeduction');
        break;
      case 'Yes':
        if (ExecuteQuery('DELETE FROM Deduction WHERE Deduction_ID ="'.$_SESSION['RemoveDeduction'][0].'"'))
  	    {
          $_SESSION['DeductionSuccess'] = 'geh!';
          Session_Unregister('RemoveDeduction');
        } else
          $_SESSION['DeductionFail'] = 'geh!';
        break;
      default:
        break;
    }
  }
?>