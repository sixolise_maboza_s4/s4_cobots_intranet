<?php
  // DETAILS ///////////////////////////////////////////////////////////////////
  //                                                                          //
  //                    Last Edited By: Gareth Ambrose                        //
  //                        Date: 23 January 2008                             //
  //                                                                          //
  //////////////////////////////////////////////////////////////////////////////
  // This page handles the back-end for the Availability page.                //
  //////////////////////////////////////////////////////////////////////////////
  
  include '../Scripts/Include.php';
  SetSettings();
  CheckLoggedIn();
  $_POST = Replace('"', '\'\'', $_POST);
  
  switch ($_POST['Type'])
  {
    //User has submitted information for availability.
    case 'Add':
      HandleAdd();
    	break;
    //User has submitted information for an event.
    case 'AddEvent':
      HandleAddEvent();
    	break;
    //User has submitted modified information for availability.
    case 'Edit':
      HandleEdit();
    	break;
    //User has submitted modified information for an event.
    case 'EditEvent':
      HandleEditEvent();
    	break;
    //User has selected to Add, Edit or Remove availability.
    case 'Maintain':
      HandleMaintain();
    	break;
    //User has selected to Add, Edit or Remove events.
    case 'MaintainEvents':
      HandleMaintainEvents();
    	break;
    //User has selected to remove availability.
    case 'Remove':
      HandleRemove();
    	break;
    //User has selected to remove an event.
    case 'RemoveEvent':
      HandleRemoveEvent();
    	break;
    //User has selected to view availability and events.
    case 'View':
      HandleView();
    	break;
    //User has reached this page incorrectly. If they are not authorised they are redirected to the main page from Availability page.
    default:
    	break;
  }
  Header('Location: ../Availability.php?'.Rand());

  //////////////////////////////////////////////////////////////////////////////
  // Checks that all the required fields have values and that these values    //
  // are valid.                                                               //
  //////////////////////////////////////////////////////////////////////////////
  function CheckFields()
  {
    switch ($_POST['Type'])
    {
      case 'Add':
      case 'Edit':
        if (($_POST['Description'] == "") || ($_POST['AvailabilityType'] == ""))
          return false;
        
        if (!(CheckDate($_POST['StartMonth'], $_POST['StartDay'], $_POST['StartYear'])) || !(CheckDate($_POST['EndMonth'], $_POST['EndDay'], $_POST['EndYear'])))
          return false;
        
        if (DatabaseDateTimeLater(GetDatabaseDateTime($_POST['StartHour'], $_POST['StartMinute'], $_POST['StartDay'], $_POST['StartMonth'], $_POST['StartYear']), GetDatabaseDateTime($_POST['EndHour'], $_POST['EndMinute'], $_POST['EndDay'], $_POST['EndMonth'], $_POST['EndYear'])))
          return false;
        break;
      case 'AddEvent':
      case 'EditEvent':
        if ($_POST['Description'] == "")
          return false;
        
        if (!(CheckDate($_POST['StartMonth'], $_POST['StartDay'], $_POST['StartYear'])) || !(CheckDate($_POST['EndMonth'], $_POST['EndDay'], $_POST['EndYear'])))
          return false;
        
        if (DatabaseDateTimeLater(GetDatabaseDateTime($_POST['StartHour'], $_POST['StartMinute'], $_POST['StartDay'], $_POST['StartMonth'], $_POST['StartYear']), GetDatabaseDateTime($_POST['EndHour'], $_POST['EndMinute'], $_POST['EndDay'], $_POST['EndMonth'], $_POST['EndYear'])))
          return false;
        break;
      case 'View':
        if ($_POST['Availability'] == "")
          return false;
        
        if (!(CheckDate($_POST['StartMonth'], $_POST['StartDay'], $_POST['StartYear'])) || !(CheckDate($_POST['EndMonth'], $_POST['EndDay'], $_POST['EndYear'])))
          return false;
        
        if (DatabaseDateLater(GetDatabaseDate($_POST['StartDay'], $_POST['StartMonth'], $_POST['StartYear']), GetDatabaseDate($_POST['EndDay'], $_POST['EndMonth'], $_POST['EndYear'])))
          return false;
        break;
      default:
        return false;
        break;
    }
    
    return true;
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Handles the user's submission of information for availability.           //
  //////////////////////////////////////////////////////////////////////////////
  function HandleAdd()
  {
    $_SESSION['AddAvailability'][1] = $_POST['AvailabilityType'];
    $_SESSION['AddAvailability'][2] = $_POST['StartYear'].$_POST['StartMonth'].$_POST['StartDay'].$_POST['StartHour'].$_POST['StartMinute'];
    $_SESSION['AddAvailability'][3] = $_POST['Description'];
    $_SESSION['AddAvailability'][4] = $_POST['EndYear'].$_POST['EndMonth'].$_POST['EndDay'].$_POST['EndHour'].$_POST['EndMinute'];
    
    switch ($_POST['Submit'])
    {
      case 'Cancel':
        Session_Unregister('AddAvailability');
        break;
      case 'Submit':
        if (CheckFields())
        {
          if (ExecuteQuery('INSERT INTO Availability VALUES("", "'.$_SESSION['AddAvailability'][0].'", "'.$_POST['Description'].'", "'.$_POST['AvailabilityType'].'", "'.GetDatabaseDateTime($_POST['StartHour'], $_POST['StartMinute'], $_POST['StartDay'], $_POST['StartMonth'], $_POST['StartYear']).'", "'.GetDatabaseDateTime($_POST['EndHour'], $_POST['EndMinute'], $_POST['EndDay'], $_POST['EndMonth'], $_POST['EndYear']).'", "'.$_SESSION['cUID'].'", "'.Date('Y-m-d H:i:s').'")'))
    	    {
            $_SESSION['AvailabilitySuccess'] = 'geh!';
            Session_Unregister('AddAvailability');
          } else
            $_SESSION['AvailabilityFail'] = 'geh!';
        } else
          $_SESSION['AvailabilityIncomplete'] = 'geh!';
        break;
      default:
        break;
    }
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Handles the user's submission of information for an event.               //
  //////////////////////////////////////////////////////////////////////////////
  function HandleAddEvent()
  {
    $_SESSION['AddAvailabilityEvent'][0] = $_POST['StartYear'].$_POST['StartMonth'].$_POST['StartDay'].$_POST['StartHour'].$_POST['StartMinute'];
    $_SESSION['AddAvailabilityEvent'][1] = $_POST['EndYear'].$_POST['EndMonth'].$_POST['EndDay'].$_POST['EndHour'].$_POST['EndMinute'];
    $_SESSION['AddAvailabilityEvent'][2] = $_POST['Description'];
    
    switch ($_POST['Submit'])
    {
      case 'Cancel':
        Session_Unregister('AddAvailabilityEvent');
        break;
      case 'Submit':
        if (CheckFields())
        {
          if (ExecuteQuery('INSERT INTO Event VALUES("", "'.$_POST['Description'].'", "'.GetDatabaseDateTime($_POST['StartHour'], $_POST['StartMinute'], $_POST['StartDay'], $_POST['StartMonth'], $_POST['StartYear']).'", "'.GetDatabaseDateTime($_POST['EndHour'], $_POST['EndMinute'], $_POST['EndDay'], $_POST['EndMonth'], $_POST['EndYear']).'", "'.$_SESSION['cUID'].'", "'.Date('Y-m-d H:i:s').'")'))
    	    {
            $_SESSION['AvailabilitySuccess'] = 'geh!';
            Session_Unregister('AddAvailabilityEvent');
          } else
            $_SESSION['AvailabilityFail'] = 'geh!';
        } else
          $_SESSION['AvailabilityIncomplete'] = 'geh!';
        break;
      default:
        break;
    }
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Handles the user's submission of modified information for availability.  //
  //////////////////////////////////////////////////////////////////////////////
  function HandleEdit()
  {
    $_SESSION['EditAvailability'][1] = $_POST['AvailabilityType'];
    $_SESSION['EditAvailability'][2] = $_POST['StartYear'].$_POST['StartMonth'].$_POST['StartDay'].$_POST['StartHour'].$_POST['StartMinute'];
    $_SESSION['EditAvailability'][3] = $_POST['Description'];
    $_SESSION['EditAvailability'][4] = $_POST['EndYear'].$_POST['EndMonth'].$_POST['EndDay'].$_POST['EndHour'].$_POST['EndMinute'];
    
    switch ($_POST['Submit'])
    {
      case 'Cancel':
        Session_Unregister('EditAvailability');
        break;
      case 'Submit':
        if (CheckFields())
        {
          if (ExecuteQuery('UPDATE Availability SET Availability_Description = "'.$_POST['Description'].'", Availability_Type = "'.$_POST['AvailabilityType'].'", Availability_Start = "'.GetDatabaseDateTime($_POST['StartHour'], $_POST['StartMinute'], $_POST['StartDay'], $_POST['StartMonth'], $_POST['StartYear']).'", Availability_End = "'.GetDatabaseDateTime($_POST['EndHour'], $_POST['EndMinute'], $_POST['EndDay'], $_POST['EndMonth'], $_POST['EndYear']).'", Availability_By = "'.$_SESSION['cUID'].'", Availability_Updated = "'.Date('Y-m-d H:i:s').'" WHERE Availability_ID = "'.$_SESSION['EditAvailability'][0].'"'))
    	    {
            $_SESSION['AvailabilitySuccess'] = 'geh!';
            Session_Unregister('EditAvailability');
          } else
            $_SESSION['AvailabilityFail'] = 'geh!';
        } else
          $_SESSION['AvailabilityIncomplete'] = 'geh!';
        break;
      default:
        break;
    }
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Handles the user's submission of modified information for an event.      //
  //////////////////////////////////////////////////////////////////////////////
  function HandleEditEvent()
  {
    $_SESSION['EditAvailabilityEvent'][1] = $_POST['StartYear'].$_POST['StartMonth'].$_POST['StartDay'].$_POST['StartHour'].$_POST['StartMinute'];
    $_SESSION['EditAvailabilityEvent'][2] = $_POST['EndYear'].$_POST['EndMonth'].$_POST['EndDay'].$_POST['EndHour'].$_POST['EndMinute'];
    $_SESSION['EditAvailabilityEvent'][3] = $_POST['Description'];
    
    switch ($_POST['Submit'])
    {
      case 'Cancel':
        Session_Unregister('EditAvailabilityEvent');
        break;
      case 'Submit':
        if (CheckFields())
        {
          if (ExecuteQuery('UPDATE Event SET Event_Description = "'.$_POST['Description'].'", Event_DateTime_Start = "'.GetDatabaseDateTime($_POST['StartHour'], $_POST['StartMinute'], $_POST['StartDay'], $_POST['StartMonth'], $_POST['StartYear']).'", Event_DateTime_End = "'.GetDatabaseDateTime($_POST['EndHour'], $_POST['EndMinute'], $_POST['EndDay'], $_POST['EndMonth'], $_POST['EndYear']).'", Event_By = "'.$_SESSION['cUID'].'", Event_Updated = "'.Date('Y-m-d H:i:s').'" WHERE Event_ID = "'.$_SESSION['EditAvailabilityEvent'][0].'"'))
    	    {
            $_SESSION['AvailabilitySuccess'] = 'geh!';
            Session_Unregister('EditAvailabilityEvent');
          } else
            $_SESSION['AvailabilityFail'] = 'geh!';
        } else
          $_SESSION['AvailabilityIncomplete'] = 'geh!';
        break;
      default:
        break;
    }
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Handles the user's availability maintenance selection.                   //
  //////////////////////////////////////////////////////////////////////////////
  function HandleMaintain()
  {
    switch ($_POST['Submit'])
    {
      case 'Add':
        if ($_SESSION['cAuth'] & 32)
          if ($_POST['Staff'] == "")
            $_SESSION['AvailabilityIncomplete'] = 'geh!';
          else
            $_SESSION['AddAvailability'] = array($_POST['Staff']);
        else
          $_SESSION['AddAvailability'] = array($_SESSION['cUID']);
        break;
      case 'Edit':
        if ($_POST['EditAvailability'] == "")
          $_SESSION['AvailabilityIncomplete'] = 'geh!';
        else
        {
          $row = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Availability WHERE Availability_ID = "'.$_POST['EditAvailability'].'"'));
          $_SESSION['EditAvailability'] = array($_POST['EditAvailability']);
          $_SESSION['EditAvailability'][1] = $row['Availability_Type'];
          $_SESSION['EditAvailability'][2] = GetSessionDateTimeFromDatabaseDateTime($row['Availability_Start']);
          $_SESSION['EditAvailability'][3] = $row['Availability_Description'];
          $_SESSION['EditAvailability'][4] = GetSessionDateTimeFromDatabaseDateTime($row['Availability_End']);
          $_SESSION['EditAvailability'][5] = $row['Availability_Name'];
        }
        break;
      case 'Remove':
        if ($_POST['RemoveAvailability'] == "")
          $_SESSION['AvailabilityIncomplete'] = 'geh!';
        else
          $_SESSION['RemoveAvailability'] = array($_POST['RemoveAvailability']);
        break;
      default:
        break;
    }
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Handles the user's event maintenance selection.                          //
  //////////////////////////////////////////////////////////////////////////////
  function HandleMaintainEvents()
  {
    switch ($_POST['Submit'])
    {
      case 'Add':
        $_SESSION['AddAvailabilityEvent'] = array("");
        break;
      case 'Edit':
        if ($_POST['EditEvent'] == "")
          $_SESSION['AvailabilityIncomplete'] = 'geh!';
        else
        {
          $row = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Event WHERE Event_ID = "'.$_POST['EditEvent'].'"'));
          $_SESSION['EditAvailabilityEvent'] = array($_POST['EditEvent']);
          $_SESSION['EditAvailabilityEvent'][1] = GetSessionDateTimeFromDatabaseDateTime($row['Event_DateTime_Start']);
          $_SESSION['EditAvailabilityEvent'][2] = GetSessionDateTimeFromDatabaseDateTime($row['Event_DateTime_End']);
          $_SESSION['EditAvailabilityEvent'][3] = $row['Event_Description'];
        }
        break;
      case 'Remove':
        if ($_POST['RemoveEvent'] == "")
          $_SESSION['AvailabilityIncomplete'] = 'geh!';
        else
          $_SESSION['RemoveAvailabilityEvent'] = array($_POST['RemoveEvent']);
        break;
      default:
        break;
    }
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Handles the user's submission to remove availability.                    //
  //////////////////////////////////////////////////////////////////////////////
  function HandleRemove()
  {
    switch ($_POST['Submit'])
    {
      case 'No':
        Session_Unregister('RemoveAvailability');
        break;
      case 'Yes':
        if (ExecuteQuery('DELETE FROM Availability WHERE Availability_ID ="'.$_SESSION['RemoveAvailability'][0].'"'))
  	    {
          $_SESSION['AvailabilitySuccess'] = 'geh!';
          Session_Unregister('RemoveAvailability');
        } else
          $_SESSION['AvailabilityFail'] = 'geh!';
        break;
      default:
        break;
    }
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Handles the user's submission to remove an event.                        //
  //////////////////////////////////////////////////////////////////////////////
  function HandleRemoveEvent()
  {
    switch ($_POST['Submit'])
    {
      case 'No':
        Session_Unregister('RemoveAvailabilityEvent');
        break;
      case 'Yes':
        if (ExecuteQuery('DELETE FROM Event WHERE Event_ID ="'.$_SESSION['RemoveAvailabilityEvent'][0].'"'))
  	    {
          $_SESSION['AvailabilitySuccess'] = 'geh!';
          Session_Unregister('RemoveAvailabilityEvent');
        } else
          $_SESSION['AvailabilityFail'] = 'geh!';
        break;
      default:
        break;
    }
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Handles the user's submission of an availability selection.              //
  //////////////////////////////////////////////////////////////////////////////
  function HandleView()
  {
    if (CheckFields())
    {
      $_SESSION['ViewAvailability'] = array($_POST['StartYear'].$_POST['StartMonth'].$_POST['StartDay']);
      $_SESSION['ViewAvailability'][1] = $_POST['EndYear'].$_POST['EndMonth'].$_POST['EndDay'];
      $_SESSION['ViewAvailability'][2] = $_POST['Availability'];
      $_SESSION['ViewAvailability'][3] = $_POST['Staff'];
      
      /*$_SESSION['StartDate'] = $_POST['StartYear'].$_POST['StartMonth'].$_POST['StartDay'];
      $_SESSION['EndDate'] = $_POST['EndYear'].$_POST['EndMonth'].$_POST['EndDay'];
      $_SESSION['ViewAvailability'] = $_POST['Availability'];
      $_SESSION['ViewAvailabilityID'] = $_POST['Staff'];*/
    } else
      $_SESSION['AvailabilityIncomplete'] = 'geh!';
  }
?>