<?php 
  // DETAILS ///////////////////////////////////////////////////////////////////
  //                                                                          //
  //                    Last Edited By: Gareth Ambrose                        //
  //                        Date: 05 December 2007                            //
  //                                                                          //
  ////////////////////////////////////////////////////////////////////////////// 
  // This page manages user authentication and logging out.                   //
  //////////////////////////////////////////////////////////////////////////////  
   
  include '../Scripts/Include.php';
  SetSettings();
                                                                                                                                                                                                                                                                                                                                                                                                                                                                           
  if (isset($_SESSION['cAuth']))
  {
    Session_Unregister('cAuth');
    Session_Unregister('cUID');
  } 
  
  Header('Location: ../Login.php');
?>
