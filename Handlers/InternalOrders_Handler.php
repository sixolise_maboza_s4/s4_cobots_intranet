<?php
  //////////////////////////////////////////////////////////////////////////////
  // This page handles the back-end for the Internal Orders page.             //
  //////////////////////////////////////////////////////////////////////////////
  ob_start("ob_gzhandler"); 
  include '../Scripts/Include.php';
  include '../Scripts/PDF.php';
  require('../PHPExcel.php');
  SetSettings();

  CheckLoggedIn();
  $_POST = Replace('"', '\'\'', $_POST);

  //Register some globals used throughout the various functions.

global $email, $emailC, $emailReminder, $html, $htmlC, $htmlx, $htmlReminder,$approver,$InternalList;

function GetNewOrderNumber()
{
	$newOrder = "";
	$yearWeek = "";
	$row = MySQL_Fetch_Array(ExecuteQuery('SELECT OrderNo_Number FROM OrderNo ORDER BY OrderNo_Number DESC LIMIT 1'));
	if ((Date('W') == 1) && (Date('m') == 12)) //The last week of year becomes the first week of the next year.
		$yearWeek = SPrintF('%02d', Date('y')+1).SPrintF('%02d', Date('W'));
	else
		$yearWeek = Date('y').SPrintF('%02d', Date('W'));

	if (SubStr($row['OrderNo_Number'], 0, 4) != $yearWeek )
		$newOrder = $yearWeek.'101';
	else
		$newOrder = $yearWeek.SPrintF('%03d', SubStr($row['OrderNo_Number'], 4, 3) + 1);

	return $newOrder;
}

function GetCashValue($paInput)
{
    $loCash = 1;
    if ($paInput == '8')
            $loCash = 0;

    return $loCash;
}
  
function InsertOrder($paOrderNumber, $paDate, $paCost, $paCash)
{
    console.log('fail to enter query adding new Order');
    //console.log('INSERT INTO OrderNo VALUES("'.$paOrderNumber.'", "'.$paDate.'", "'.$_POST['Requested'].'", "'.$_POST['Supplier'].'", "'.$_POST['Project'].'", "", "'.$paCost.'", "'.$_POST['Comments'].'", "'.$_SESSION['cUID'].'", "'.$_POST['Delivery'].'", "'.$_POST['OrderType'].'", "", "", "", "'.$paCash.'", "", "0", "0", "0", "", "", "", "1", "", "", "", "", "'.GetDatabaseDateFromSessionDate($_SESSION['AddInternalOrder'][14]).'", "'.GetDatabaseDateFromSessionDate($_SESSION['AddInternalOrder'][13]).'", "'.$_POST['CostCurrency'].'", "'.$_POST['Cost'].'","'.$_POST['ReportCategory'].'","'.$_POST['quotation_number'].'",1)');
    return ExecuteQuery('INSERT INTO OrderNo VALUES("'.$paOrderNumber.'", "'.$paDate.'", "'.$_POST['Requested'].'", "'.$_POST['Supplier'].'", "'.$_POST['Project'].'", "", "'.$paCost.'", "'.$_POST['Comments'].'", "'.$_SESSION['cUID'].'", "'.$_POST['Delivery'].'", "'.$_POST['OrderType'].'", "", "", "", "'.$paCash.'", "", "0", "0", "0", "", "", "", "1", "", "", "", "", "'.$_SESSION['AddInternalOrder'][14].'", "'.$_SESSION['AddInternalOrder'][13].'", "'.$_POST['CostCurrency'].'", "'.$_POST['Cost'].'","'.$_POST['ReportCategory'].'","'.$_POST['quotation_number'].'")'); //$_POST['quotation_number']
}

function GetCurrencyRow($paCurrency)
{
    return MySQL_Fetch_Array(ExecuteQuery('SELECT C.Currency_Rate, C.Currency_Symbol FROM Currency C WHERE Currency_ID = "'.$paCurrency.'"'));
}

function InsertOrderItems($paOrderNumber, $paRate, $paDate, $paSessionArray, $paCurrency)
{
    if (SizeOf($paSessionArray) > 20)
    {
        for ($count = 20; $count < SizeOf($paSessionArray); $count++)
        {
            if ($paSessionArray[$count] != "")
            {
                    $loCost = $paSessionArray[$count][2] * $paRate;
                    $loTotal = $loCost * $paSessionArray[$count][3];
                    ExecuteQuery('INSERT INTO Items VALUES("'.$paOrderNumber.'", "'.$paSessionArray[$count][0].'", "'.$paSessionArray[$count][3].'", "'.$paSessionArray[$count][4].'", "", "'.$loCost.'", "", "", "'.$paSessionArray[$count][1].'", "", "", "", "", "'.$loTotal.'", "'.$paDate.'", "'.$paSessionArray[$count][5].'", "'.$paCurrency.'", "'.$paSessionArray[$count][2].'", "'.($paSessionArray[$count][2] * $paSessionArray[$count][3]).'","'.$paSessionArray[$count][6].'","","'.$paSessionArray[$count][7].'")');
                    ExecuteQuery('INSERT INTO Items_tmp VALUES("'.$paOrderNumber.'", "'.$paSessionArray[$count][0].'", "'.$paSessionArray[$count][3].'", "'.$paSessionArray[$count][4].'", "", "'.$loCost.'", "", "", "'.$paSessionArray[$count][1].'", "", "", "", "", "'.$loTotal.'", "'.$paDate.'", "'.$paSessionArray[$count][5].'", "'.$paCurrency.'", "'.$paSessionArray[$count][2].'", "'.($paSessionArray[$count][2] * $paSessionArray[$count][3]).'","NULL","NULL","NULL")');
            }
        }
    }
}


function BuildEmailReachedLimit($paOrderNumber,$cost){
     /*this function sends an email when an order has reached 50 000*/
    global $html, $htmlx,$approver;
    $message = "Order S4-".$paOrderNumber.Chr(10)." total is ".$cost.Chr(10)." excluding VAT";
    SendMailHTML('renaldo.oconnor@s4integration.co.za', 'Order Over R50 000.00 - S4 - '.$paOrderNumber, "", $message);
}

function BuildWaitApprovalEmails($paOrderNumber, $paSessionArray, $paCurrencySymbol, $paDate)
{
	global $email, $html, $htmlx, $approver;
	
	$row = MySQL_Fetch_Array(ExecuteQuery('SELECT OrderNo.OrderNo_ReportCategory, OrderNo.`quotation_number`, Staff_First_Name, Staff_Last_Name, Supplier_Name,Supplier_Code, Supplier_Phone, Project.* , OrderNo_Comments  FROM Staff, Supplier, Project, OrderNo WHERE Staff_Code = OrderNo_Requested AND Supplier_Code = OrderNo_Supplier AND Project_Code = OrderNo_Project AND OrderNo_Number= "'.$paOrderNumber.'"'));
        $row2 = MySQL_Fetch_Array(ExecuteQuery('SELECT `Level` FROM BEE_Certificate WHERE Supplier_Code = '.$row['Supplier_Code']));
        $levelBEE = 'non-compliant';
        if($row2['Level']!=null){
              if($row2['Level'] == 9){
                       $levelBEE = "N/A - International";
                    }
                    
                     else if($row2['Level'] == 10){
                           $levelBEE = "Awaiting feedback";
                    }
                    
                    else if($row2['Level'] == 0){
                       $levelBEE = "non-compliant";
                    }
                    
                    else
                        $levelBEE = $row2['Level'];
        }
          $tdLevel = '<TD style="font-weight: bold; background-color: rgba(255, 0, 0, 0.32); padding-left: 0.5em; padding-right: 0.5em;" >non-compliant</TD>';
              if($row2['Level']!=null){
                   if($row2['Level'] == 9){
                        $tdLevel = '<TD style="font-weight: bold; background-color: rgba(10, 148, 23, 0.32); padding-left: 0.5em; padding-right: 0.5em;" >N/A - International</TD>';
                    }
                    
                     else if($row2['Level'] == 10){
                         $tdLevel = '<TD style="font-weight: bold; background-color: rgba(10, 115, 254, 0.58); padding-left: 0.5em; padding-right: 0.5em; " >Awaiting feedback</TD>';
                    } 
                    
                    else if($row2['Level'] == 0){
                        $tdLevel = '<TD style="font-weight: bold; background-color: rgba(255, 0, 0, 0.32); padding-left: 0.5em; padding-right: 0.5em; " >non-compliant</TD>';
                    }
                    
                    else if($row2['Level'] < 6) {  // 1 2 3 4 5 
                        $tdLevel = '<TD style="font-weight: bold; background-color: rgba(10, 148, 23, 0.32); padding-left: 0.5em; padding-right: 0.5em;" >'.$row2['Level'].'</TD>';
                    }
                    
                    else {   // 6 7 8
                          $tdLevel = '<TD style="font-weight: bold; background-color: rgba(245, 253, 21, 0.58); padding-left: 0.5em; padding-right: 0.5em;" >'.$row2['Level'].'</TD>';
                    }
            }
         $qnumber2 = "";
         $qNumber = $row['quotation_number'];
            if($qNumber!= null || $qNumber!= ''){
                
                $qNumber = 'QUOTATION NUMBER:               '.$row['quotation_number'];
                $qnumber2 = '<TR><TD><B>Quotation number:</B></TD><TD>'.$row['quotation_number'].'</TR>';
            }
        
        if( strcmp($row['OrderNo_ReportCategory'], "0") == 0){
            $costManagerOnCategory  = 'Project_CostManager';
        }
        else{       
            $costManagerOnCategory  = 'Project_CostManager'.$row['OrderNo_ReportCategory'];  
        }
        if($row[$costManagerOnCategory]){
            $cmRow = MySQL_Fetch_Array(ExecuteQuery('SELECT Staff_First_Name, Staff_Last_Name,Staff_email FROM Staff WHERE Staff_Code = '.$row[$costManagerOnCategory]));
            $approver = $cmRow['Staff_email'];
            $ApproverName = $cmRow['Staff_First_Name'].' '.$cmRow['Staff_Last_Name'];
        }
	$rowB = MySQL_Fetch_Array(ExecuteQuery('SELECT Staff_First_Name, Staff_Last_Name,Staff_email FROM Staff WHERE Staff_Code = '.$row['Project_Responsible']));
        if(!$approver)
        {
            $approver = $rowB['Staff_email'];
            $ApproverName = $rowB['Staff_First_Name'].' '.$rowB['Staff_Last_Name'];
        }
				
	$email = 'The following order requires approval:'.Chr(10).
			 'PENDING ORDER NO:       '.$paOrderNumber.Chr(10).
			 'DATE:                   '.GetTextualDateFromDatabaseDate($paDate).Chr(10).
			 'REQUESTED BY:           '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].Chr(10).
			 'PROJECT:                '.$row['Project_Pastel_Prefix'].' - '.$row['Project_Description'].Chr(10).
			 'SUPPLIER:               '.$row['Supplier_Name'].' - '.$row['Supplier_Phone'].Chr(10).
                         'BEE Level:              '.$levelBEE.
                         $qNumber.Chr(10).
			 'COMMENTS:               '.$row['OrderNo_Comments'].Chr(10).
			 'FOLLOW UP DATE:         '.GetTextualDateFromDatabaseDate($paSessionArray[13]).Chr(10).
			 'DELIVERY DATE:          '.GetTextualDateFromDatabaseDate($paSessionArray[14]).Chr(10).
			 'TO BE APPROVED BY:      '.$ApproverName.Chr(10).
			 'TOTAL (EXCL. VAT):      '.$paCurrencySymbol.$_POST['Cost'].Chr(10).Chr(10);

	$html = 'The following order requires approval:
		<BR /><BR />
		<TABLE border=0>
		  <TR><TD><B>Pending Order No:</B></TD><TD>'.$paOrderNumber.'</TD></TR>
		  <TR><TD><B>Date:</B></TD><TD>'.GetTextualDateFromDatabaseDate($paDate).'</TD></TR>
		  <TR><TD><B>Requested By:</B></TD><TD>'.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].'</TD></TR>
		  <TR><TD><B>Project:</B></TD><TD>'.$row['Project_Pastel_Prefix'].' - '.$row['Project_Description'].'</TD></TR>
		  <TR><TD><B>Supplier:</B></TD><TD>'.$row['Supplier_Name'].' - '.$row['Supplier_Phone'].'</TD></TR>
                  <TR><TD><B>BEE Level:</B></TD>'.$tdLevel.'</TR>
                  '.$qnumber2.'
		  <TR><TD><B>Comments:</B></TD><TD>'.$row['OrderNo_Comments'].'</TD></TR>
		  <TR><TD><B>Follow Up Date:</B></TD><TD>'.GetTextualDateFromDatabaseDate($paSessionArray[13]).'</TD></TR>
		  <TR><TD><B>Delivery Date:</B></TD><TD>'.GetTextualDateFromDatabaseDate($paSessionArray[14]).'</TD></TR>
		  <TR><TD><B>To Be Approved By:</B></TD><TD>'.$ApproverName.'</TD></TR>
		  <TR><TD><B>Total (Excl. VAT):</B></TD><TD>'.$paCurrencySymbol.$_POST['Cost'].'</TD></TR>
		</TABLE>
		<BR /><BR />';
		
	$email .= 'QTY          '.Chr(9).'SUPPLIER CODE'.Chr(9).'DESCRIPTION  '.Chr(9).Chr(9).'VALUE        '.Chr(9).'NETT VALUE        '.Chr(9).'DISCOUNT        '.Chr(9).'TOTAL        '.Chr(10);
	$html .= 'Items highlighted in Orange may be in the uncontrolled goods area of the store</br></br><TABLE border=1><TR><TD>QTY</TD><TD>SUPPLIER CODE</TD><TD>DESCRIPTION</TD><TD>VALUE</TD><TD>NETT VALUE</TD><TD>DISCOUNT</TD><TD>TOTAL</TD><TD>UNCONTROLLED GOODS QTY</TD><TD>MATCHED ON</TD></TR>';
	
        $uncontrolledGoods = getUncontrolledGoodsList();
        $resultSet = ExecuteQuery('SELECT * FROM Items WHERE Items_Order_Number = "'.$paOrderNumber.'"');
	while ($rowC = MySQL_Fetch_Array($resultSet))
	{
            $nett_value_mail = $rowC['Items_Original_Value'];
            $discount = 0;
                                if($rowC['Items_Discount'] > 0)
                                {
                                    $nett_value_mail -= (($rowC['Items_Discount']/100) * $nett_value_mail);
                                    $discount = ($rowC['Items_Discount']/100) * $rowC['Items_Original_Total'];
                                }
                                
		$email .= $rowC['Items_Quantity'].Chr(9).Chr(9).Chr(9).$rowC['Items_Supplier_Code'].Chr(9).Chr(9).Chr(9).$rowC['Items_Description'].Chr(9).Chr(9).Chr(9).Chr(9).Chr(9).$paCurrencySymbol.SPrintF('%02.2f', $rowC['Items_Original_Value']).Chr(9).Chr(9).$paCurrencySymbol.SPrintF('%02.2f', $nett_value_mail).Chr(9).Chr(9).$rowC['Items_Discount'].'%'.Chr(9).Chr(9).$paCurrencySymbol.SPrintF('%02.2f', ($rowC['Items_Original_Total'] - $discount)).Chr(10).Chr(10);
		
                $theQTYValue =  isInUncontrolled($rowC['Items_Supplier_Code'], $rowC['Items_Description'], $uncontrolledGoods);
                
                if($theQTYValue == 0){
                     $html .= '<TR><TD>'.$rowC['Items_Quantity'].'</TD><TD>'.$rowC['Items_Supplier_Code'].'</TD><TD>'.$rowC['Items_Description'].'</TD><TD>'.$paCurrencySymbol.SPrintF('%02.2f', $rowC['Items_Original_Value']).'</TD><TD>'.$paCurrencySymbol.SPrintF('%02.2f', $nett_value_mail).'</TD><TD>'.$rowC['Items_Discount'].'%'.'</TD><TD>'.$paCurrencySymbol.SPrintF('%02.2f', ($rowC['Items_Original_Total']*((100-$rowC['Items_Discount'])/100))  ).'</TD><TD></TD><TD></TD></TR>';
                }
                else{
                     $html .= '<TR style="background-color: rgba(255, 122, 0, 0.61);" ><TD>'.$rowC['Items_Quantity'].'</TD><TD>'.$rowC['Items_Supplier_Code'].'</TD><TD>'.$rowC['Items_Description'].'</TD><TD>'.$paCurrencySymbol.SPrintF('%02.2f', $rowC['Items_Original_Value']).'</TD><TD>'.$paCurrencySymbol.SPrintF('%02.2f', $nett_value_mail).'</TD><TD>'.$rowC['Items_Discount'].'%'.'</TD><TD>'.$paCurrencySymbol.SPrintF('%02.2f', ($rowC['Items_Original_Total']*((100-$rowC['Items_Discount'])/100)) ).'</TD><TD>'. $theQTYValue.'</TD><TD>'.matchedon($rowC['Items_Supplier_Code'], $rowC['Items_Description'], $uncontrolledGoods).'</TD></TR>';
                }  
	}
	$email .= Chr(10).Chr(10).Chr(10).'___________________________________'.Chr(10).Chr(10).$rowB['Staff_First_Name'].' '.$rowB['Staff_Last_Name'];
	$html .= '</TABLE><BR /><BR /><BR /><BR />___________________________________<BR /><BR />'.$rowB['Staff_First_Name'].' '.$rowB['Staff_Last_Name'];

	$rand = Rand();
	while (StrLen($rand) != 5)
	{
		if (StrLen($rand) > 5)
			$rand = SubStr($rand, 0, 5);
		else
			$rand = Rand();
	}
	$htmlx = $html;
	$email .= Chr(10).Chr(10).Chr(10).'Approve this request here '.getS4ExternalLink().'/intranew/InternalOrders.php?type=approve&id='.$rand.$paOrderNumber.' here.';
	$html .= '<BR /><BR /><BR />Approve this request <A href="'.getS4ExternalLink().'/intranew/InternalOrders.php?type=approve&id='.$rand.$paOrderNumber.'">here</A>.';
}

function SendWaitApprovalEmails($paOrderNumber)
{
    global $email, $html, $htmlx,$approver;
//    $people = array('cherie','andrew','arthur','johannes','stefan','gideon');
//    foreach ($people as $person)
//    {
//        SendMailHTML($person.'@s4integration.co.za', 'Order Waiting Approval - S4 - '.$paOrderNumber, $email, $html);
//    }
    SendMailHTML($approver, 'Order Waiting Approval - S4 - '.$paOrderNumber, $email, $html);
    SendMailHTML('zane@s4integration.co.za', 'Check for parts already in stock - S4 - '.$paOrderNumber, $email, $htmlx);
}

function UpdateAutomaticApprovalStatus($paOrderNumber, $paDate )
{
    $_SESSION['ApproveInternalOrder'] = array($paOrderNumber);
    $_SESSION['ApproveInternalOrder'][1] = $_SESSION['cUID'];
    $_SESSION['ApproveInternalOrder'][3] = 2;
    
    return ExecuteQuery('UPDATE OrderNo SET OrderNo_Approved = "1", OrderNo_Approved_By = "'.$_SESSION['cUID'].'", OrderNo_Approved_Date = "'.$paDate.'", OrderNo_Approved_Comment = "'.'AUTOMATIC APPROVAL'.'", OrderNo_Approved_Type = "'.'2'.'" WHERE OrderNo_Number = "'.$paOrderNumber.'"');
}

function BuildAndSendAutomaticApprovalEmail($paOrderNumber, $paSessionArray)
{
    global $email, $emailC, $emailReminder, $html, $htmlC, $htmlReminder, $autoApproved;

    $row = MySQL_Fetch_Array(ExecuteQuery('SELECT OrderNo.OrderNo_ReportCategory, OrderNo.`quotation_number`, Staff_First_Name, Staff_Last_Name, Staff_email, Staff_Phone_Work,  Supplier_Name, Supplier_Phone, Supplier_Address_Street, Supplier_Code, Supplier_Address_Suburb, Supplier_Address_City, Supplier_Address_Code, Project.* , OrderNo.* FROM Staff, Supplier, Project, OrderNo WHERE Staff_Code = OrderNo_Requested AND Supplier_Code = OrderNo_Supplier AND Project_Code = OrderNo_Project AND OrderNo_Number= "'.$paOrderNumber.'"'));
    $row2 = MySQL_Fetch_Array(ExecuteQuery('SELECT `Level` FROM BEE_Certificate WHERE Supplier_Code = '.$row['Supplier_Code']));
    $levelBEE = 'non-compliant';
     if($row2['Level']!=null ){
             if($row2['Level'] == 9){
                       $levelBEE = "N/A - International";
                    }
                     else if($row2['Level'] == 10){
                           $levelBEE = "Awaiting feedback";
                    }
                    
                    else if($row2['Level'] == 0){
                       $levelBEE = "non-compliant";
                    }
                    
                    else
                        $levelBEE = $row2['Level'];
        }
          $tdLevel = '<TD style="font-weight: bold; background-color: rgba(255, 0, 0, 0.32); padding-left: 0.5em; padding-right: 0.5em; " >non-compliant</TD>';
              if($row2['Level']!=null){
                   if($row2['Level'] == 9){
                        $tdLevel = '<TD style="font-weight: bold; background-color: rgba(10, 148, 23, 0.32); padding-left: 0.5em; padding-right: 0.5em; " >N/A - International</TD>';
                    }
         
                     else if($row2['Level'] == 10){
                         $tdLevel = '<TD style="font-weight: bold; background-color: rgba(10, 115, 254, 0.58); padding-left: 0.5em; padding-right: 0.5em; " >Awaiting feedback</TD>';
                    } 
                    
                    else if($row2['Level'] == 0){
                        $tdLevel = '<TD style="font-weight: bold; background-color: rgba(255, 0, 0, 0.32); padding-left: 0.5em; padding-right: 0.5em; " >non-compliant</TD>';
                    }
                    
                    else if($row2['Level'] < 6) { // 1 2 3 4 5 
                        $tdLevel = '<TD style="font-weight: bold; background-color: rgba(10, 148, 23, 0.32); padding-left: 0.5em; padding-right: 0.5em; " >'.$row2['Level'].'</TD>';
                    }
                    
                    else {   // 6 7 8
                          $tdLevel = '<TD style="font-weight: bold; background-color: rgba(245, 253, 21, 0.58); padding-left: 0.5em; padding-right: 0.5em; " >'.$row2['Level'].'</TD>';
                    }
            }
        
         $qNumber = $row['quotation_number'];
            if($qNumber!= null || $qNumber!= ''){
                
                $qNumber = 'QUOTATION NUMBER:               '.$row['quotation_number'];
                $qnumber2 = '<TR><TD><B>Quotation number:</B></TD><TD>'.$row['quotation_number'].'</TR>';
            }
    $rowB = MySQL_Fetch_Array(ExecuteQuery('SELECT Staff_First_Name, Staff_Last_Name FROM Staff WHERE Staff_Code = '.$_SESSION['cUID']));
    $approvalUser = $rowB['Staff_First_Name'].' '.$rowB['Staff_Last_Name'];
    if($autoApproved) 
    {
       if( strcmp($row['OrderNo_ReportCategory'], "0") == 0){
        
        $costManagerOnCategory = 'Project_CostManager'; 
       }
       else{
            $costManagerOnCategory = 'Project_CostManager'.$row['OrderNo_ReportCategory']; 
       }
        
        $autoUserID =  ($row[$costManagerOnCategory] != "") ? $row[$costManagerOnCategory] : $row['Project_Responsible'];
        if($autoUserID != "")
        {
            $rowAuto = MySQL_Fetch_Array(ExecuteQuery('SELECT Staff_First_Name, Staff_Last_Name FROM Staff WHERE Staff_Code = '.$autoUserID));
            $approvalUser = $rowAuto['Staff_First_Name'].' '.$rowAuto['Staff_Last_Name'];
        }
        
    }
    $loCurrencyRow = MySQL_Fetch_Array(ExecuteQuery('SELECT C.Currency_Rate, C.Currency_Symbol FROM Currency C WHERE Currency_ID = "'.$row['OrderNo_Original_Currency'].'"'));
    $loCurrencySymbol = $loCurrencyRow['Currency_Symbol'];

    $email = 'The following order has been approved:'.Chr(10).
                             'ORDER NO:               '.$row['OrderNo_Number'].Chr(10).
                             'DATE:                   '.GetTextualDateFromDatabaseDate($row['OrderNo_Date_Time']).Chr(10).
                             'PROJECT:                '.$row['Project_Pastel_Prefix'].' - '.$row['Project_Description'].Chr(10).
                             'SUPPLIER:               '.$row['Supplier_Name'].' - '.$row['Supplier_Phone'].Chr(10).
                             'BEE Level:              '.$levelBEE.
                             $qNumber.
                             'FOLLOW UP DATE:         '.$row['OrderNo_Comments'].Chr(10).
                             'DELIVERY DATE:          '.$row['OrderNo_Date_Delivery'].Chr(10).
                             'APPROVED BY:            '.$approvalUser.
                             'TOTAL (EXCL. VAT):      '.$loCurrencySymbol.$row['OrderNo_Original_Total_Cost'].Chr(10).Chr(10);
    $emailC = 'The following order has been approved:'.Chr(10).
                             'ORDER NO:               '.$row['OrderNo_Number'].Chr(10).
                             'DATE:                   '.GetTextualDateFromDatabaseDate($row['OrderNo_Date_Time']).Chr(10).
                             'REQUESTED BY:           '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].Chr(10).
                             'PROJECT:                '.$row['Project_Pastel_Prefix'].' - '.$row['Project_Description'].Chr(10).
                             'SUPPLIER:               '.$row['Supplier_Name'].' - '.$row['Supplier_Phone'].Chr(10).
                             'BEE Level:              '.$levelBEE.
                             $qNumber.
                             'FOLLOW UP DATE:         '.$row['OrderNo_Date_FollowUp'].Chr(10).
                             'DELIVERY DATE:          '.$row['OrderNo_Date_Delivery'].Chr(10).
                             'APPROVED BY:            '.$approvalUser.
                             'TOTAL (EXCL. VAT):      '.$loCurrencySymbol.$row['OrderNo_Original_Total_Cost'].Chr(10).Chr(10);
    $emailReminder = Chr(10).'ORDER NO:               '.$row['OrderNo_Number'].Chr(10).
                             'DATE:                   '.GetTextualDateFromDatabaseDate($row['OrderNo_Date_Time']).Chr(10).
                             'PROJECT:                '.$row['Project_Pastel_Prefix'].' - '.$row['Project_Description'].Chr(10).
                             'SUPPLIER:               '.$row['Supplier_Name'].' - '.$row['Supplier_Phone'].Chr(10).
                             'BEE Level:              '.$levelBEE.
                              $qNumber.  
                             'FOLLOW UP DATE:         '.$row['OrderNo_Date_FollowUp'].Chr(10).
                             'DELIVERY DATE:          '.$row['OrderNo_Date_Delivery'].Chr(10).
                             'APPROVED BY:            '.$approvalUser.
                             'TOTAL (EXCL. VAT):      '.$loCurrencySymbol.$row['OrderNo_Original_Total_Cost'].Chr(10).Chr(10);
    
    $emailLargeAmount = 'The following order\'s total amount is greater than R100,000.00:'.Chr(10).
                             'ORDER NO:               '.$row['OrderNo_Number'].Chr(10).
                             'DATE:                   '.GetTextualDateFromDatabaseDate($row['OrderNo_Date_Time']).Chr(10).
                             'REQUESTED BY:           '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].Chr(10).
                             'PROJECT:                '.$row['Project_Pastel_Prefix'].' - '.$row['Project_Description'].Chr(10).
                             'SUPPLIER:               '.$row['Supplier_Name'].' - '.$row['Supplier_Phone'].Chr(10).
                             'BEE Level:              '.$levelBEE.
                             $qNumber.   
                             'APPROVED BY:            '.$approvalUser.
                             'TOTAL (EXCL. VAT):      '.$loCurrencySymbol.$row['OrderNo_Original_Total_Cost'].Chr(10).Chr(10);
    
    $htmlLargeAmount = 'The following order\'s total amount is greater than R100,000.00:
                            <BR /><BR />
                            <TABLE border=0>
                              <TR><TD><B>Order No:</B></TD><TD>'.$row['OrderNo_Number'].'</TD></TR>
                              <TR><TD><B>Date:</B></TD><TD>'.GetTextualDateFromDatabaseDate($row['OrderNo_Date_Time']).'</TD></TR>
                              <TR><TD><B>Requested By:</B></TD><TD>'.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].'</TD></TR>
                              <TR><TD><B>Project:</B></TD><TD>'.$row['Project_Pastel_Prefix'].' - '.$row['Project_Description'].'</TD></TR>
                              <TR><TD><B>Supplier:</B></TD><TD>'.$row['Supplier_Name'].' - '.$row['Supplier_Phone'].'</TD></TR>
                              <TR><TD><B>BEE Level:</B></TD>'.$tdLevel.'</TR>
                                  '.$qnumber2.'
                              <TR><TD><B>Approved By:</B></TD><TD>'.$approvalUser.'</TD></TR>
                              <TR><TD><B>Total (Excl. VAT):</B></TD><TD>'.$loCurrencySymbol.$row['OrderNo_Original_Total_Cost'].'</TD></TR>
                            </TABLE>
                            <BR /><BR />';
    
    $html = 'The following order has been approved:
                            <BR /><BR />
                            <TABLE border=0>
                              <TR><TD><B>Order No:</B></TD><TD>'.$row['OrderNo_Number'].'</TD></TR>
                              <TR><TD><B>Date:</B></TD><TD>'.GetTextualDateFromDatabaseDate($row['OrderNo_Date_Time']).'</TD></TR>
                              <TR><TD><B>Project:</B></TD><TD>'.$row['Project_Pastel_Prefix'].' - '.$row['Project_Description'].'</TD></TR>
                              <TR><TD><B>Supplier:</B></TD><TD>'.$row['Supplier_Name'].' - '.$row['Supplier_Phone'].'</TD></TR>
                              <TR><TD><B>BEE Level:</B></TD>'.$tdLevel.'</TR>
                                   '.$qnumber2.'
                              <TR><TD><B>Follow Up Date:</B></TD><TD>'.GetTextualDateFromDatabaseDate($row['OrderNo_Date_FollowUp']).'</TD></TR>
                              <TR><TD><B>Delivery Date:</B></TD><TD>'.GetTextualDateFromDatabaseDate($row['OrderNo_Date_Delivery']).'</TD></TR>
                              <TR><TD><B>Approved By:</B></TD><TD>'.$approvalUser.'</TD></TR>
                              <TR><TD><B>Total (Excl. VAT):</B></TD><TD>'.$loCurrencySymbol.$row['OrderNo_Original_Total_Cost'].'</TD></TR>
                            </TABLE>
                            <BR /><BR />';
    $htmlC = 'The following order has been approved:
                            <BR /><BR />
                            <TABLE border=0>
                              <TR><TD><B>Order No:</B></TD><TD>'.$row['OrderNo_Number'].'</TD></TR>
                              <TR><TD><B>Date:</B></TD><TD>'.GetTextualDateFromDatabaseDate($row['OrderNo_Date_Time']).'</TD></TR>
                              <TR><TD><B>Requested By:</B></TD><TD>'.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].'</TD></TR>
                              <TR><TD><B>Project:</B></TD><TD>'.$row['Project_Pastel_Prefix'].' - '.$row['Project_Description'].'</TD></TR>
                              <TR><TD><B>Supplier:</B></TD><TD>'.$row['Supplier_Name'].' - '.$row['Supplier_Phone'].'</TD></TR>
                              <TR><TD><B>BEE Level:</B></TD>'.$tdLevel.'</TR>
                                   '.$qnumber2.'
                              <TR><TD><B>Follow Up Date:</B></TD><TD>'.GetTextualDateFromDatabaseDate($row['OrderNo_Date_FollowUp']).'</TD></TR>
                              <TR><TD><B>Delivery Date:</B></TD><TD>'.GetTextualDateFromDatabaseDate($row['OrderNo_Date_Delivery']).'</TD></TR>
                              <TR><TD><B>Approved By:</B></TD><TD>'.$approvalUser.'</TD></TR>
                              <TR><TD><B>Total (Excl. VAT):</B></TD><TD>'.$loCurrencySymbol.$row['OrderNo_Original_Total_Cost'].'</TD></TR>

                            </TABLE>
                            <BR /><BR />';
    $htmlReminder = '<BR /><BR />
                            <TABLE border=0>
                              <TR><TD><B>Order No:</B></TD><TD>'.$row['OrderNo_Number'].'</TD></TR>
                              <TR><TD><B>Date:</B></TD><TD>'.GetTextualDateFromDatabaseDate($row['OrderNo_Date_Time']).'</TD></TR>
                              <TR><TD><B>Project:</B></TD><TD>'.$row['Project_Pastel_Prefix'].' - '.$row['Project_Description'].'</TD></TR>
                              <TR><TD><B>Supplier:</B></TD><TD>'.$row['Supplier_Name'].' - '.$row['Supplier_Phone'].'</TD></TR>
                              <TR><TD><B>BEE Level:</B></TD>'.$tdLevel.'</TR>    
                                   '.$qnumber2.'
                              <TR><TD><B>Follow Up Date:</B></TD><TD>'.GetTextualDateFromDatabaseDate($row['OrderNo_Date_FollowUp']).'</TD></TR>
                              <TR><TD><B>Delivery Date:</B></TD><TD>'.GetTextualDateFromDatabaseDate($row['OrderNo_Date_Delivery']).'</TD></TR>
                              <TR><TD><B>Approved By:</B></TD><TD>'.$approvalUser.'</TD></TR>
                              <TR><TD><B>Total (Excl. VAT):</B></TD><TD>'.$loCurrencySymbol.$row['OrderNo_Original_Total_Cost'].'</TD></TR>
                            </TABLE>
                            <BR /><BR />';

    $email .= 'QTY          '.Chr(9).'SUPPLIER CODE'.Chr(9).'DESCRIPTION  '.Chr(9).Chr(9).'VALUE        '.Chr(9).'NETT VALUE        '.Chr(9).'DISCOUNT        '.Chr(9).'TOTAL        '.Chr(10);
    $emailC .= 'QTY          '.Chr(9).'SUPPLIER CODE'.Chr(9).'DESCRIPTION  '.Chr(9).Chr(9).'VALUE        '.Chr(9).'NETT VALUE        '.Chr(9).'DISCOUNT        '.Chr(9).'TOTAL        '.Chr(10);
    $emailReminder .= 'QTY          '.Chr(9).'SUPPLIER CODE'.Chr(9).'DESCRIPTION  '.Chr(9).Chr(9).'VALUE        '.Chr(9).'NETT VALUE        '.Chr(9).'DISCOUNT        '.Chr(9).'TOTAL        '.Chr(10);
    $emailLargeAmount .= 'QTY          '.Chr(9).'SUPPLIER CODE'.Chr(9).'DESCRIPTION  '.Chr(9).Chr(9).'VALUE        '.Chr(9).'NETT VALUE        '.Chr(9).'DISCOUNT        '.Chr(9).'TOTAL        '.Chr(10);
    $html .= '<TABLE border=1><TR><TD>QTY</TD><TD>SUPPLIER CODE</TD><TD>DESCRIPTION</TD><TD>VALUE</TD><TD>NETT VALUE</TD><TD>DISCOUNT</TD><TD>TOTAL</TD></TR>';
    $htmlC .= '<TABLE border=1><TR><TD>QTY</TD><TD>SUPPLIER CODE</TD><TD>DESCRIPTION</TD><TD>VALUE</TD><TD>NETT VALUE</TD><TD>DISCOUNT</TD><TD>TOTAL</TD></TR>';
    $htmlReminder .= '<TABLE border=1><TR><TD>QTY</TD><TD>SUPPLIER CODE</TD><TD>DESCRIPTION</TD><TD>VALUE</TD><TD>NETT VALUE</TD><TD>DISCOUNT</TD><TD>TOTAL</TD></TR>';
    $htmlLargeAmount .= '<TABLE border=1><TR><TD>QTY</TD><TD>SUPPLIER CODE</TD><TD>DESCRIPTION</TD><TD>VALUE</TD><TD>NETT VALUE</TD><TD>DISCOUNT</TD><TD>TOTAL</TD></TR>';

    $resultSet = ExecuteQuery('SELECT * FROM Items WHERE Items_Order_Number = "'.$paSessionArray[0].'"');
    $data = array();
    while ($rowC = MySQL_Fetch_Array($resultSet))
    {
            $nett_value_mail = $rowC['Items_Original_Value'];
            $discount = 0;
                                if($rowC['Items_Discount'] > 0)
                                {
                                    $nett_value_mail -= (($rowC['Items_Discount']/100) * $nett_value_mail);
                                    $discount = ($rowC['Items_Discount']/100) * $rowC['Items_Original_Total'];
                                }
            $data[] = array(floatval($rowC['Items_Quantity']), $rowC['Items_Supplier_Code'],$rowC['Items_Description'],$loCurrencySymbol.SPrintF('%02.2f', $rowC['Items_Original_Value']),floatval($rowC['Items_Discount']).'%',$loCurrencySymbol.SPrintF('%02.2f', ($rowC['Items_Original_Total'] - $discount)));                    
                                
            $email .= $rowC['Items_Quantity'].Chr(9).Chr(9).Chr(9).$rowC['Items_Supplier_Code'].Chr(9).Chr(9).Chr(9).$rowC['Items_Description'].Chr(9).Chr(9).Chr(9).Chr(9).Chr(9).$loCurrencySymbol.SPrintF('%02.2f', $rowC['Items_Original_Value']).Chr(9).Chr(9).$loCurrencySymbol.SPrintF('%02.2f', $nett_value_mail).Chr(9).Chr(9).$rowC['Items_Discount'].'%'.Chr(9).Chr(9).$loCurrencySymbol.SPrintF('%02.2f', ($rowC['Items_Original_Total'] - $discount)).Chr(10).Chr(10);

            $emailC .= $rowC['Items_Quantity'].Chr(9).Chr(9).Chr(9).$rowC['Items_Supplier_Code'].Chr(9).Chr(9).Chr(9).$rowC['Items_Description'].Chr(9).Chr(9).Chr(9).Chr(9).Chr(9).$loCurrencySymbol.SPrintF('%02.2f', $rowC['Items_Original_Value']).Chr(9).Chr(9).$loCurrencySymbol.SPrintF('%02.2f', $nett_value_mail).Chr(9).Chr(9).$rowC['Items_Discount'].'%'.Chr(9).Chr(9).$loCurrencySymbol.SPrintF('%02.2f', ($rowC['Items_Original_Total'] - $discount)).Chr(10).Chr(10);

            $emailReminder .= $rowC['Items_Quantity'].Chr(9).Chr(9).Chr(9).$rowC['Items_Supplier_Code'].Chr(9).Chr(9).Chr(9).$rowC['Items_Description'].Chr(9).Chr(9).Chr(9).Chr(9).Chr(9).$loCurrencySymbol.SPrintF('%02.2f', $rowC['Items_Original_Value']).Chr(9).Chr(9).$loCurrencySymbol.SPrintF('%02.2f', $nett_value_mail).Chr(9).Chr(9).$rowC['Items_Discount'].'%'.Chr(9).Chr(9).$loCurrencySymbol.SPrintF('%02.2f', ($rowC['Items_Original_Total'] - $discount)).Chr(10).Chr(10);
            
            $emailLargeAmount .= $rowC['Items_Quantity'].Chr(9).Chr(9).Chr(9).$rowC['Items_Supplier_Code'].Chr(9).Chr(9).Chr(9).$rowC['Items_Description'].Chr(9).Chr(9).Chr(9).Chr(9).Chr(9).$loCurrencySymbol.SPrintF('%02.2f', $rowC['Items_Original_Value']).Chr(9).Chr(9).$loCurrencySymbol.SPrintF('%02.2f', $nett_value_mail).Chr(9).Chr(9).$rowC['Items_Discount'].'%'.Chr(9).Chr(9).$loCurrencySymbol.SPrintF('%02.2f', ($rowC['Items_Original_Total'] - $discount)).Chr(10).Chr(10);

            $html .= '<TR><TD>'.$rowC['Items_Quantity'].'</TD><TD>'.$rowC['Items_Supplier_Code'].'</TD><TD>'.$rowC['Items_Description'].'</TD><TD>'.$loCurrencySymbol.SPrintF('%02.2f', $rowC['Items_Original_Value']).'</TD><TD>'.$loCurrencySymbol.SPrintF('%02.2f', $nett_value_mail).'</TD><TD>'.$rowC['Items_Discount'].'%'.'</TD><TD>'.$loCurrencySymbol.SPrintF('%02.2f', ($rowC['Items_Original_Total'] - $discount)).'</TD></TR>';

            $htmlC .= '<TR><TD>'.$rowC['Items_Quantity'].'</TD><TD>'.$rowC['Items_Supplier_Code'].'</TD><TD>'.$rowC['Items_Description'].'</TD><TD>'.$loCurrencySymbol.SPrintF('%02.2f', $rowC['Items_Original_Value']).'</TD><TD>'.$loCurrencySymbol.SPrintF('%02.2f', $nett_value_mail).'</TD><TD>'.$rowC['Items_Discount'].'%'.'</TD><TD>'.$loCurrencySymbol.SPrintF('%02.2f', ($rowC['Items_Original_Total'] - $discount)).'</TD></TR>';

            $htmlReminder .= '<TR><TD>'.$rowC['Items_Quantity'].'</TD><TD>'.$rowC['Items_Supplier_Code'].'</TD><TD>'.$rowC['Items_Description'].'</TD><TD>'.$loCurrencySymbol.SPrintF('%02.2f', $rowC['Items_Original_Value']).'</TD><TD>'.$loCurrencySymbol.SPrintF('%02.2f', $nett_value_mail).'</TD><TD>'.$rowC['Items_Discount'].'%'.'</TD><TD>'.$loCurrencySymbol.SPrintF('%02.2f', ($rowC['Items_Original_Total'] - $discount)).'</TD></TR>';
            
            $htmlLargeAmount .= '<TR><TD>'.$rowC['Items_Quantity'].'</TD><TD>'.$rowC['Items_Supplier_Code'].'</TD><TD>'.$rowC['Items_Description'].'</TD><TD>'.$loCurrencySymbol.SPrintF('%02.2f', $rowC['Items_Original_Value']).'</TD><TD>'.$loCurrencySymbol.SPrintF('%02.2f', $nett_value_mail).'</TD><TD>'.$rowC['Items_Discount'].'%'.'</TD><TD>'.$loCurrencySymbol.SPrintF('%02.2f', ($rowC['Items_Original_Total'] - $discount)).'</TD></TR>';
    }
    $htmlLargeAmount .= '</TABLE>';
    $email .= Chr(10).Chr(10).Chr(10).'___________________________________'.Chr(10).Chr(10).$approvalUser;
    $emailC .= Chr(10).Chr(10).Chr(10).'___________________________________'.Chr(10).Chr(10).$approvalUser;
    $html .= '</TABLE><BR /><BR /><BR /><BR />___________________________________<BR /><BR />'.$approvalUser;
    $htmlC .= '</TABLE><BR /><BR /><BR /><BR />___________________________________<BR /><BR />'.$approvalUser;

    //PDF Attachment   
     $pdf_filename= '../Files/Temp/'.$row['OrderNo_Number'].'.pdf';
     $columnLabels = array( "Qty", "Supplier Code", "Description","Price","Discount","Total" );
     $pdf = GeneratePdf($pdf_filename,$columnLabels,$data,$row,$rowB,$loCurrencySymbol);
     $pdf->Output($pdf_filename, "F");
    
    SendMailHTML($row['Staff_email'], 'Order Approved - S4 - '.$row['OrderNo_Number'], $email, $html,$pdf_filename);
    $people = array('arthur','johannes','gideon','stefan','roline','amelia');
    $management = array('vaughn','andrew');
    foreach ($people as $person) 
        CCSomeone($row['Staff_email'], $person.'@s4integration.co.za', 'Order Approved - S4 - '.$row['OrderNo_Number'], $emailC, $htmlC);
    
    if($row['OrderNo_Original_Total_Cost'] > 100000)
    {
        foreach ($management as $pers) 
            SendMailHTML($pers.'@s4integration.co.za', 'Order Greater than R100,000.00 - S4 - '.$row['OrderNo_Number'], $emailLargeAmount, $htmlLargeAmount,$pdf_filename);
    }

    
    unlink($pdf_filename);
    $today = Date('Y-m-d');
//    if (!DatabaseDateLater($row['OrderNo_Date_Delivery'], $today) && ($row['OrderNo_Date_Delivery'] != $today))
//        ExecuteQuery('INSERT INTO Reminder VALUES("", "'.$_SESSION['cUID'].'", "'.Date('Y-m-d H:i:s').'", "'.$row['OrderNo_Requested'].'", "'.GetReminderDate($row['OrderNo_Date_Delivery']).'", "4", "5", "32", "Order <B>'.$row['OrderNo_Number'].'</B> due for delivery on '.GetTextualDateFromDatabaseDate($row['OrderNo_Date_Delivery']).'. '.$row['Supplier_Name'].' for '.$row['Project_Description'].'.", "IO", "'.$row['OrderNo_Number'].'", "Reminder for the delivery of the following order: '.$emailReminder.'", "Reminder for the delivery of the following order: '.$htmlReminder.'")');
//
//    if (!DatabaseDateLater($row['OrderNo_Date_FollowUp'], $today) && ($row['OrderNo_Date_FollowUp'] != $today))
//        ExecuteQuery('INSERT INTO Reminder VALUES("", "'.$_SESSION['cUID'].'", "'.Date('Y-m-d H:i:s').'", "'.$row['OrderNo_Requested'].'", "'.GetReminderDate($row['OrderNo_Date_FollowUp']).'", "4", "5", "32", "Order <B>'.$row['OrderNo_Number'].'</B> due for follow up on '.GetTextualDateFromDatabaseDate($row['OrderNo_Date_FollowUp']).'. '.$row['Supplier_Name'].' for '.$row['Project_Description'].'.", "IO", "'.$row['OrderNo_Number'].'", "Reminder for the follow up of the following order: '.$emailReminder.'", "Reminder for the follow up of the following order: '.$htmlReminder.'")');

}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//function to add new item to the array with spesific key else make new array with that key

function getUncontrolledGoodsList(){
    $Uncontrolledgoods = array();
    $Resultlist = ExecuteQuery('SELECT result.Items_Supplier_Code , count(result.Items_Supplier_Code) as Quantity FROM ((SELECT i.Items_Supplier_Code FROM `S4Admin`.Items i LEFT JOIN stock_control.items_barcode ib ON (i.Items_ID = ib.items_id) LEFT JOIN S4Admin.OrderNo o on o.OrderNo_Number = i.Items_Order_Number LEFT JOIN S4Admin.Supplier s  on o.OrderNo_Supplier = s.Supplier_Code WHERE ib.items_barcode_id in (SELECT items_barcode_id FROM stock_control.items_barcode WHERE shelf_id = 153 AND db = "s4")) UNION ALL (SELECT i.Items_Supplier_Code FROM `CobotsAdmin`.Items i LEFT JOIN stock_control.items_barcode ib ON (i.Items_ID = ib.items_id) LEFT JOIN CobotsAdmin.OrderNo o on o.OrderNo_Number = i.Items_Order_Number LEFT JOIN CobotsAdmin.Supplier s  on o.OrderNo_Supplier = s.Supplier_Code WHERE ib.items_barcode_id in (SELECT items_barcode_id FROM stock_control.items_barcode WHERE shelf_id = 153 AND db = "mm")) UNION ALL (SELECT i.part_number as `Items_Supplier_Code` FROM `stock_control`.no_order_items i LEFT JOIN stock_control.items_barcode ib ON (i.Items_ID = ib.items_id) WHERE ib.items_barcode_id in (SELECT items_barcode_id FROM stock_control.items_barcode WHERE shelf_id = 153 AND db = "no"))) result GROUP BY result.Items_Supplier_Code ORDER BY Quantity Desc');
    while ($row = MySQL_Fetch_Array($Resultlist)) {
          $Uncontrolledgoods[strtolower($row['Items_Supplier_Code'])] = $row['Quantity'];   
    }
     return $Uncontrolledgoods;
}

// return quatity if key exsit else 0  these two methods must be identical: isInUncontrolled and matchedon also on  internalorders.php
function isInUncontrolled($curCode,$disc,$list){
    
       
    $UncontrolledQuantity1 = 0;
                                 
    if(strtolower($curCode)!== ""){
        if (array_key_exists(strtolower($curCode),$list))
         {
           $UncontrolledQuantity1 = $list[strtolower($curCode)];
        }
    }

    else{
        $length = 0;
        foreach ($list as $code => $CurUncontrolledQuantity){  
            if($code!=""){
                $c = '/'.$code.'/';
                if(preg_match($c,strtolower($disc))){
                    if(strlen($code) > $length){
                        $length = strlen($code);
                        $UncontrolledQuantity1 = $CurUncontrolledQuantity; 
                    }
                } 
            }
        }        
    }
    return $UncontrolledQuantity1;                          
    
}

// these two methods must be identical: matchedon  and isInUncontrolled 
function matchedon($curCode,$disc,$list){
     $supplierCode = "no match";                            
    if(strtolower($curCode)!== ""){
        if (array_key_exists(strtolower($curCode),$list))
         {
             $supplierCode = $curCode;
        }
    }

    else{
        $length = 0;
        foreach ($list as $code => $CurUncontrolledQuantity){  
            if($code!=""){
                $c = '/'.$code.'/';
                if(preg_match($c,strtolower($disc))){
                   if(strlen($code) > $length){
                    $length = strlen($code);
                    $supplierCode = $code;
                   }
                } 
            }
        }        
    }
    return $supplierCode;                     
}

if(isset($_GET['function'])){
    //getSuplierCode() doCheck() getMultipleOrderView()
     switch ($_GET['function']){
       case 'getSuplierCode':
            getSuplierCode() ;
            break;
        case 'docheck':
            doCheck() ;
            break;
        case 'getMultipleOrderView':
            getMultipleOrderView() ;
            break; 
        case 'MarkOrderList':
            MarkOrdersList($_GET['OrdernumberList'],$_GET['Status']) ;
            break;
          case 'SendErrorEmail':
            SendErrorEmail($_GET['Message'],$_GET['StaffCode']) ;
            break; 
        default:
            break;
     }
}

else{
    switch ($_POST['Type'])
    {
    //User has submitted information for an internal order.   
    case 'Add':
      HandleAdd();
    	break;
    //User has submitted information for approving an internal order.
    case 'Approve':
      HandleApprove();
    	break;
    //User has submitted modified information for an internal order.
    case 'Edit':
      HandleEdit();
    	break;
    //User has selected to Add, Approve or Edit an internal order.
    case 'Maintain':
      HandleMaintain();
      break;
    case 'MaintainMarkMultiple':
        HandleMarkMultiple();
        break;
    //User has selected to Mark an internal order.
    case 'Mark':
      HandleMark();
      break;
    case 'MarkMultipleList':
     unset($_SESSION['MarkInternalOrderMultipleList']);
      break;
   case 'ViewSingleOrders':
     Session_Unregister('ViewInternalOrderSingle');
     break;
    //User has selected to View internal orders.
    case 'ViewExceeded':
    case 'ViewSingle':
    case 'ViewList':
    case 'ViewOutstanding':
      HandleView();
    	break;
    //User has reached this page incorrectly. If they are not authorised they are redirected to the main page from the Internal Orders page.
    default:
    	break;
  }
  
    Header('Location: ../InternalOrders.php?'.Rand());
}
  //////////////////////////////////////////////////////////////////////////////
  // Checks that all the required fields have values and that these values    //
  // are valid.                                                               //
  //////////////////////////////////////////////////////////////////////////////
  function CheckFields()
  {
    switch ($_POST['Type'])
    {
      case 'Add':
      case 'Edit':
        {
          if ($_POST['Submit'] == 'Add')
          {
            if (($_POST['Description'] == "") || ($_POST['Value'] == "") || ($_POST['Quantity'] == "") || ($_POST['Backorder'] == "") || ($_POST['Asset'] == "") || ($_POST['StorageType'] == "") || ($_POST['Discount'] == ""))
              return false;
            
            if ((!Is_Numeric($_POST['Value'])) || (!Is_Numeric($_POST['Quantity'])) || (!Is_Numeric($_POST['Discount'])))
              return false;
            
            if (($_POST['Value'] <= 0) || ($_POST['Quantity'] <= 0) || ($_POST['Discount'] < 0))
              return false;
          } else
        if ( ($_POST['Submit'] == 'Submit') || ($_POST['Submit'] == 'AutoApprove')) 
          {
            if (($_POST['Requested'] == "") || ($_POST['Supplier'] == "") || ($_POST['Project'] == "") || ($_POST['CostCurrency'] == "") || ($_POST['OrderType'] == "") || ($_POST['ReportCategory'] == ""))
              return false;
            
            if ($_POST['Cost'] == 0)
              return false;
            
            if ($_POST['FollowDate'] == "" || $_POST['DeliveryDate'] == "")
              return false;
            
            if (DatabaseDateLater($_POST['FollowDate'], $_POST['DeliveryDate']))
              return false;
            
            $items = false;
            switch ($_POST['Type'])
            {
              case 'Add':
                for ($count = 21; $count < SizeOf($_SESSION['AddInternalOrder']); $count++)
                {
                  if ($_SESSION['AddInternalOrder'][$count] != "")
                  {
                    $items = true;
                    break;
                  }
                }
                break;
              case 'Edit':
                for ($count = 23; $count <= SizeOf($_SESSION['EditInternalOrder']); $count++)
                {
                  if ($_SESSION['EditInternalOrder'][$count] != "")
                  {
                    if ($_SESSION['EditInternalOrder'][$count][1] != "")
                    {
                      $items = true;
                      break;
                    }
                  }
                }
                break;
              default:
                break;
            }
            if (!$items)
              return false;
          } else
          {
            switch ($_POST['Type'])
            {
              case 'Add':
                for ($count = 21; $count < SizeOf($_SESSION['AddInternalOrder']); $count++)
                {
                  if ($_POST['Update'.$count] == 'Update')
                  {
                    if (($_POST['Description'.$count] == "") || ($_POST['Value'.$count] == "") || ($_POST['Quantity'.$count] == "") || ($_POST['Backorder'.$count] == "") || ($_POST['Asset'.$count] == "") || ($_POST['StorageType'.$count] == "") || ($_POST['Discount'.$count] == ""))
                      return false;
                    
                    if ((!Is_Numeric($_POST['Value'.$count])) || (!Is_Numeric($_POST['Quantity'.$count])) || (!Is_Numeric($_POST['Discount'.$count])))
                      return false;
                    
                    if (($_POST['Value'.$count] <= 0) || ($_POST['Quantity'.$count] <= 0) || ($_POST['Discount'.$count] < 0))
                      return false;
                  }
                }
                break;
              case 'Edit':
                for ($count = 23; $count <= SizeOf($_SESSION['EditInternalOrder']); $count++)
                {
                  if ($_POST['Update'.$count] == 'Update')
                  {
                    if (($_POST['Description'.$count] == "") || ($_POST['Value'.$count] == "") || ($_POST['Quantity'.$count] == "") || ($_POST['Backorder'.$count] == "") || ($_POST['Asset'.$count] == "") || ($_POST['StorageType'.$count] == "") || ($_POST['Discount'.$count] == ""))
                      return false;
                    
                    if ((!Is_Numeric($_POST['Value'.$count])) || (!Is_Numeric($_POST['Quantity'.$count])) || (!Is_Numeric($_POST['Discount'.$count])))
                      return false;
                    
                    if (($_POST['Value'.$count] <= 0) || ($_POST['Quantity'.$count] <= 0) || ($_POST['Discount'.$count] < 0))
                      return false;
                  }
                }
                break;
              default:
                break;
            }
          }
        }
        break;
      case 'Approve':
        if (($_POST['Approved'] == "") || ($_POST['ApprovedType'] == ""))
          return false;
        break;
      case 'Mark':
        if ($_POST['Status'] == "")
          return false;
        break;
      case 'ViewExceeded':
      case 'ViewOutstanding':
        break;
      case 'ViewList':
        {
          if (!(CheckDate($_POST['StartMonth'], $_POST['StartDay'], $_POST['StartYear'])) || !(CheckDate($_POST['EndMonth'], $_POST['EndDay'], $_POST['EndYear'])))
            return false;
          
          if (DatabaseDateLater(GetDatabaseDate($_POST['StartDay'], $_POST['StartMonth'], $_POST['StartYear']), GetDatabaseDate($_POST['EndDay'], $_POST['EndMonth'], $_POST['EndYear'])))
            return false;
        }
        break;
      case 'ViewSingle':
        {
          if (!CheckNumeric($_POST['InternalOrder']) || (StrLen($_POST['InternalOrder']) != 7))
            return false;
          
          $loRS = ExecuteQuery('SELECT * FROM OrderNo WHERE OrderNo_Number = "'.$_POST['InternalOrder'].'"');
          if (MySQL_Num_Rows($loRS) == 0)
            return false;
        }
        break;
      default:
        return false;
        break;
    }
    
    return true;
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Handles the user's submission of information for a new internal order.   //
  //////////////////////////////////////////////////////////////////////////////
  function HandleAdd()
  {
    global $autoApproved;
    $autoApproved = false;
    $_SESSION['AddInternalOrder'][0] = $_POST['Requested'];
    $_SESSION['AddInternalOrder'][1] = $_POST['Supplier'];
    $_SESSION['AddInternalOrder'][2] = $_POST['Project'];
    $_SESSION['AddInternalOrder'][3] = $_POST['OrderType'];
    $_SESSION['AddInternalOrder'][4] = $_POST['CostCurrency'];
    $_SESSION['AddInternalOrder'][5] = $_POST['Cost'];
    $_SESSION['AddInternalOrder'][6] = $_POST['Comments'];
    $_SESSION['AddInternalOrder'][7] = $_POST['Delivery'];
    $_SESSION['AddInternalOrder'][8] = $_POST['SupplierCode'];
    $_SESSION['AddInternalOrder'][9] = $_POST['Description'];
    $_SESSION['AddInternalOrder'][10] = $_POST['Backorder'];
    $_SESSION['AddInternalOrder'][11] = $_POST['Value'];
    $_SESSION['AddInternalOrder'][12] = $_POST['Quantity'];
    $_SESSION['AddInternalOrder'][13] = $_POST['FollowDate'];
    $_SESSION['AddInternalOrder'][14] = $_POST['DeliveryDate'];
    $_SESSION['AddInternalOrder'][15] = $_POST['Asset'];
    $_SESSION['AddInternalOrder'][16] = $_POST['StorageType'];
    $_SESSION['AddInternalOrder'][17] = $_POST['Discount'];
    $_SESSION['AddInternalOrder'][18] = $_POST['ReportCategory'];
    $_SESSION['AddInternalOrder'][19] = $_POST['quotation_number'];
    $_SESSION['AddInternalOrder'][20] = $_POST['BEE_Id'];
    if($_SESSION['AddInternalOrder'][17] ==0)
        $_SESSION['AddInternalOrder'][17]="";
    
    if($_SESSION['AddInternalOrder'][1] == 955 && $_POST['Submit'] == "Submit") 
    {
        $autoApproved = true;
        $_POST['Submit'] = "AutoApprove";
    }
    switch ($_POST['Submit'])
    {
      case 'Add':
	  {
		//Inject from the session variable [clipboardData]
		  if (isset($_SESSION["clipboardDataInternalOrders"]))
		  {
			$_SESSION['AddInternalOrder'] = array_merge($_SESSION['AddInternalOrder'], $_SESSION["clipboardDataInternalOrders"]);
			unset($_SESSION["clipboardDataInternalOrders"]);
		  }
        elseif (CheckFields())
        {
          $_SESSION['AddInternalOrder'][] = array($_POST['SupplierCode'], $_POST['Description'], $_POST['Value'], $_POST['Quantity'], $_POST['Backorder'], $_POST['Asset'],$_POST['StorageType'],$_POST['Discount']);
          $_SESSION['AddInternalOrder'][8] = "";
          $_SESSION['AddInternalOrder'][9] = "";
          $_SESSION['AddInternalOrder'][10] = "";
          $_SESSION['AddInternalOrder'][11] = "";
          $_SESSION['AddInternalOrder'][12] = "";
          $_SESSION['AddInternalOrder'][15] = "";
          $_SESSION['AddInternalOrder'][16] = "";
          $_SESSION['AddInternalOrder'][17] = "";
        } else{
              $_SESSION['AddInternalOrder'][17] = "";
              $_SESSION['InternalOrderIncomplete'] = 'geh!';
            }
	  }
        break;
      case 'Cancel':
        break;
      case 'AutoApprove':
      case 'Submit':
        break;
      default: //Remove or Update.
        for ($count = 21; $count < SizeOf($_SESSION['AddInternalOrder']); $count++)
        {
          if ($_POST['Remove'.$count] == 'Remove')
          {
            $_SESSION['AddInternalOrder'][$count] = "";
            break;
          } else
          if ($_POST['Update'.$count] == 'Update')
          {
            if (CheckFields())
              $_SESSION['AddInternalOrder'][$count] = array($_POST['SupplierCode'.$count], $_POST['Description'.$count], $_POST['Value'.$count], $_POST['Quantity'.$count], $_POST['Backorder'.$count], $_POST['Asset'.$count], $_POST['StorageType'.$count], $_POST['Discount'.$count]);
            else
              $_SESSION['InternalOrderIncomplete'] = 'geh!';
            break;
          }
        }
        break;
    }
    $total = 0;
    for ($count = 21; $count < SizeOf($_SESSION['AddInternalOrder']); $count++)
    {
      if ($_SESSION['AddInternalOrder'][$count] != "")
      {
          $total += $_SESSION['AddInternalOrder'][$count][2] * $_SESSION['AddInternalOrder'][$count][3];
          if($_SESSION['AddInternalOrder'][$count][7] > 0)
             $total -= (($_SESSION['AddInternalOrder'][$count][2]/100)*$_SESSION['AddInternalOrder'][$count][7]) * $_SESSION['AddInternalOrder'][$count][3];   
      }
    }
    $_SESSION['AddInternalOrder'][5] = $total;
    
    switch ($_POST['Submit'])
    {
      case 'Add':
        break;
      case 'Cancel':
        Session_Unregister('AddInternalOrder');
        break;
      case 'AutoApprove':
	if (CheckFields())//Check all inputs
	{
            $newOrder = GetNewOrderNumber();
            $cash = GetCashValue($_POST['Project']);
            $date = Date('Y-m-d H:i:s');
            $loCurrencyRow = MySQL_Fetch_Array(ExecuteQuery('SELECT C.Currency_Rate, C.Currency_Symbol FROM Currency C WHERE Currency_ID = "'.$_POST['CostCurrency'].'"'));
            $cost = $_POST['Cost'] * $loCurrencyRow['Currency_Rate'];
            $loCurrencySymbol = $loCurrencyRow['Currency_Symbol'];

            if (InsertOrder($newOrder, $date, $cost, $cash))
            {
                if($cost >= 50000){
                    BuildEmailReachedLimit($newOrder,$cost);
                }
                InsertOrderItems($newOrder, $loCurrencyRow['Currency_Rate'], $date, $_SESSION['AddInternalOrder'], $_POST['CostCurrency']);//InsertOrderItems();
                $_SESSION['InternalOrderSuccess'] = 'geh!';
                Session_Unregister('AddInternalOrder');
            }
            else //Failed inserting new order
            {  
                $_SESSION['InternalOrderFail'] = 'geh!';
                break;
            }

            if (UpdateAutomaticApprovalStatus($newOrder, $date))
            {
                BuildAndSendAutomaticApprovalEmail($newOrder, $_SESSION['ApproveInternalOrder']);

                $_SESSION['InternalOrderSuccess'] = 'geh!';
                Session_Unregister('ApproveInternalOrder');
            }
            else //Failed automatically updating the order details to APPROVED, send email as per normal procedure...
            {
                BuildWaitApprovalEmails($newOrder, $_SESSION['AddInternalOrder'], $loCurrencySymbol, $date);
                SendWaitApprovalEmails($newOrder);
                $_SESSION['InternalOrderSuccess'] = 'geh!';
                Session_Unregister('AddInternalOrder');
            }
	} 
	else //FAILED Checking all input fields
	{
            $_SESSION['InternalOrderIncomplete'] = 'geh!';
	}
	break; //End case 'AutoApprove'
          
      case 'Submit': 
        if (CheckFields())
        {
          $newOrder = GetNewOrderNumber();
          $cash = GetCashValue($_POST['Project']);
          $date = Date('Y-m-d H:i:s');
          $loCurrencyRow = GetCurrencyRow($_POST['CostCurrency']);
          $cost = $_POST['Cost'] * $loCurrencyRow['Currency_Rate'];
          $loCurrencySymbol = $loCurrencyRow['Currency_Symbol'];
          
          if (InsertOrder($newOrder, $date, $cost, $cash))
    	  {
    	      if($cost >= 50000){
                  BuildEmailReachedLimit($newOrder, $cost);
              }
            InsertOrderItems($newOrder, $loCurrencyRow['Currency_Rate'], $date, $_SESSION['AddInternalOrder'], $_POST['CostCurrency']);

            BuildWaitApprovalEmails($newOrder, $_SESSION['AddInternalOrder'], $loCurrencySymbol, $date);
            SendWaitApprovalEmails($newOrder);

            $_SESSION['InternalOrderSuccess'] = 'geh!';
            Session_Unregister('AddInternalOrder');
          } 
          else //insert failed
    	      $_SESSION['InternalOrderFail'] = 'geh!';
        }
        else
            $_SESSION['InternalOrderIncomplete'] = 'geh!';
        
        break;
      default: //Remove
        break;
    }
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Handles the user's submission of information for approving an internal   //
  // order.                                                                   //
  //////////////////////////////////////////////////////////////////////////////
  function HandleApprove()
  {
    $_SESSION['ApproveInternalOrder'][1] = $_POST['Approved'];
    $_SESSION['ApproveInternalOrder'][2] = $_POST['Comments'];
    //$_SESSION['ApproveInternalOrder'][3] = $_POST['ApprovedType'];
    $_SESSION['ApproveInternalOrder'][3] = 2;
    $_SESSION['ApproveInternalOrder'][4] = $_POST['Discrepency'];
  
    switch ($_POST['Submit'])
    {
      case 'Cancel':
        Session_Unregister('ApproveInternalOrder');
        break;
      case 'Deny':
          $date = Date('Y-m-d H:i:s');
          
          
          if (ExecuteQuery('UPDATE OrderNo SET OrderNo_Approved = "2", OrderNo_Approved_By = "'.$_SESSION['cUID'].'", OrderNo_Approved_Date = "'.$date.'", OrderNo_Approved_Comment = "'.$_POST['Comments'].'" WHERE OrderNo_Number = "'.$_SESSION['ApproveInternalOrder'][0].'"'))
    	    {
            $row = MySQL_Fetch_Array(ExecuteQuery('SELECT OrderNo.`quotation_number`, Staff_First_Name, Staff_Last_Name, Staff_email, Supplier_Code, Supplier_Name, Supplier_Phone, Project_Description, Project_Pastel_Prefix, Project_Responsible, OrderNo.* FROM Staff, Supplier, Project, OrderNo WHERE Staff_Code = OrderNo_Requested AND Supplier_Code = OrderNo_Supplier AND Project_Code = OrderNo_Project AND OrderNo_Number= "'.$_SESSION['ApproveInternalOrder'][0].'"'));
            $row2 = MySQL_Fetch_Array(ExecuteQuery('SELECT `Level` FROM BEE_Certificate WHERE Supplier_Code = '.$row['Supplier_Code']));
            $levelBEE = 'non-compliant';
            if($row2['Level']!=null){
                    if($row2['Level'] == 9){
                       $levelBEE = "N/A - International";
                    }
                    
                     else if($row2['Level'] == 10){
                       $levelBEE = "Awaiting feedback";
                    }
                   
                    else if($row2['Level'] == 0){
                       $levelBEE = "non-compliant";
                    }
                    
                    else
                        $levelBEE = $row2['Level'];
             }
        
               $tdLevel = '<TD style="font-weight: bold; background-color: rgba(255, 0, 0, 0.32); padding-left: 0.5em; padding-right: 0.5em; " >non-compliant</TD>';
              if($row2['Level']!=null){
                   if($row2['Level'] == 9){
                        $tdLevel = '<TD style="font-weight: bold; background-color: rgba(10, 148, 23, 0.32); padding-left: 0.5em; padding-right: 0.5em; " >N/A - International</TD>';
                    }
                    
                    else if($row2['Level'] == 10){
                         $tdLevel = '<TD style="font-weight: bold; background-color: rgba(10, 115, 254, 0.58); padding-left: 0.5em; padding-right: 0.5em; " >Awaiting feedback</TD>';
                    }
                    
                    else if($row2['Level'] == 0){
                        $tdLevel = '<TD style="font-weight: bold; background-color: rgba(255, 0, 0, 0.32); padding-left: 0.5em; padding-right: 0.5em;" >non-compliant</TD>';
                    }
                    
                    else if($row2['Level'] < 6) { // 1 2 3 4 5 
                        $tdLevel = '<TD style="font-weight: bold; background-color: rgba(10, 148, 23, 0.32); padding-left: 0.5em; padding-right: 0.5em; " >'.$row2['Level'].'</TD>';
                    }
                    
                    else {   // 6 7 8
                          $tdLevel = '<TD style="font-weight: bold; background-color: rgba(245, 253, 21, 0.58); padding-left: 0.5em; padding-right: 0.5em; " >'.$row2['Level'].'</TD>';
                    }
            }
            $qNumber = $row['quotation_number'];
            $qnumber2 = "";
            if($qNumber!= null || $qNumber!= '' ){
                
                $qNumber = 'QUOTATION NUMBER:               '.$row['quotation_number'];
                $qnumber2 = '<TR><TD><B>Quotation number:</B></TD><TD>'.$row['quotation_number'].'</TR>';
            }
            $rowB = MySQL_Fetch_Array(ExecuteQuery('SELECT Staff_First_Name, Staff_Last_Name FROM Staff WHERE Staff_Code = '.$_SESSION['cUID']));
            $loCurrencyRow = MySQL_Fetch_Array(ExecuteQuery('SELECT C.Currency_Rate, C.Currency_Symbol FROM Currency C WHERE Currency_ID = "'.$row['OrderNo_Original_Currency'].'"'));
            $loCurrencySymbol = $loCurrencyRow['Currency_Symbol'];
            
            
            $email = 'The following order has been denied:'.Chr(10).
                     'DATE:                   '.GetTextualDateFromDatabaseDate($row['OrderNo_Date_Time']).Chr(10).
                     'PROJECT:                '.$row['Project_Pastel_Prefix'].' - '.$row['Project_Description'].Chr(10).
                     'SUPPLIER:               '.$row['Supplier_Name'].' - '.$row['Supplier_Phone'].Chr(10).
                     'BEE Level:              '.$levelBEE.
                      $qNumber.
                     'COMMENTS:               '.$row['OrderNo_Date_FollowUp'].Chr(10).
                     'FOLLOW UP DATE:         '.$row['OrderNo_Comments'].Chr(10).
                     'DELIVERY DATE:          '.$row['OrderNo_Date_Delivery'].Chr(10).
                     /*'DENIED BY:              '.$rowB['Staff_First_Name'].' '.$rowB['Staff_Last_Name'].Chr(10).*/
                     'TOTAL (EXCL. VAT):      '.$loCurrencySymbol.$row['OrderNo_Original_Total_Cost'].Chr(10).Chr(10);
            $emailC = 'The following order has been denied:'.Chr(10).
                     'DATE:                   '.GetTextualDateFromDatabaseDate($row['OrderNo_Date_Time']).Chr(10).
                     'REQUESTED BY:           '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].Chr(10).
                     'PROJECT:                '.$row['Project_Pastel_Prefix'].' - '.$row['Project_Description'].Chr(10).
                     'SUPPLIER:               '.$row['Supplier_Name'].' - '.$row['Supplier_Phone'].Chr(10).
                     'BEE Level:              '.$levelBEE.
                        $qNumber.
                     'COMMENTS:               '.$row['OrderNo_Date_FollowUp'].Chr(10).
                     'FOLLOW UP DATE:         '.$row['OrderNo_Comments'].Chr(10).
                     'DELIVERY DATE:          '.$row['OrderNo_Date_Delivery'].Chr(10).
                     'DENIED BY:              '.$rowB['Staff_First_Name'].' '.$rowB['Staff_Last_Name'].Chr(10).
                     'TOTAL (EXCL. VAT):      '.$loCurrencySymbol.$row['OrderNo_Original_Total_Cost'].Chr(10).Chr(10);
            
            $html = 'The following order has been denied:
                    <BR /><BR />
                    <TABLE border=0>
                      <TR><TD><B>Date:</B></TD><TD>'.GetTextualDateFromDatabaseDate($row['OrderNo_Date_Time']).'</TD></TR>
                      <TR><TD><B>Project:</B></TD><TD>'.$row['Project_Pastel_Prefix'].' - '.$row['Project_Description'].'</TD></TR>
                      <TR><TD><B>Supplier:</B></TD><TD>'.$row['Supplier_Name'].' - '.$row['Supplier_Phone'].'</TD></TR>
                      <TR><TD><B>BEE Level:</B></TD>'.$tdLevel.'</TR>
                           '.$qnumber2.'
                      <TR><TD><B>Comments:</B></TD><TD>'.$row['OrderNo_Comments'].'</TD></TR>
                      <TR><TD><B>Follow Up Date:</B></TD><TD>'.GetTextualDateFromDatabaseDate($row['OrderNo_Date_FollowUp']).'</TD></TR>
                      <TR><TD><B>Delivery Date:</B></TD><TD>'.GetTextualDateFromDatabaseDate($row['OrderNo_Date_Delivery']).'</TD></TR>'.
                      /*<TR><TD><B>Denied By:</B></TD><TD>'.$rowB['Staff_First_Name'].' '.$rowB['Staff_Last_Name'].'</TD></TR>*/
                      '<TR><TD><B>Total (Excl. VAT):</B></TD><TD>'.$loCurrencySymbol.$row['OrderNo_Original_Total_Cost'].'</TD></TR>
                    </TABLE>
                    <BR /><BR />';
            $htmlC = 'The following order has been denied:
                    <BR /><BR />
                    <TABLE border=0>
                      <TR><TD><B>Date:</B></TD><TD>'.GetTextualDateFromDatabaseDate($row['OrderNo_Date_Time']).'</TD></TR>
                      <TR><TD><B>Requested By:</B></TD><TD>'.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].'</TD></TR>
                      <TR><TD><B>Project:</B></TD><TD>'.$row['Project_Pastel_Prefix'].' - '.$row['Project_Description'].'</TD></TR>
                      <TR><TD><B>Supplier:</B></TD><TD>'.$row['Supplier_Name'].' - '.$row['Supplier_Phone'].'</TD></TR>
                      <TR><TD><B>BEE Level:</B></TD>'.$tdLevel.'</TR>
                           '.$qnumber2.'
                      <TR><TD><B>Comments:</B></TD><TD>'.$row['OrderNo_Comments'].'</TD></TR>
                      <TR><TD><B>Follow Up Date:</B></TD><TD>'.GetTextualDateFromDatabaseDate($row['OrderNo_Date_FollowUp']).'</TD></TR>
                      <TR><TD><B>Delivery Date:</B></TD><TD>'.GetTextualDateFromDatabaseDate($row['OrderNo_Date_Delivery']).'</TD></TR>
                      <TR><TD><B>Denied By:</B></TD><TD>'.$rowB['Staff_First_Name'].' '.$rowB['Staff_Last_Name'].'</TD></TR>
                      <TR><TD><B>Total (Excl. VAT):</B></TD><TD>'.$loCurrencySymbol.$row['OrderNo_Original_Total_Cost'].'</TD></TR>
                    </TABLE>
                    <BR /><BR />';
            
            $email .= 'QTY          '.Chr(9).'SUPPLIER CODE'.Chr(9).'DESCRIPTION  '.Chr(9).Chr(9).'VALUE        '.Chr(9).'NETT VALUE        '.Chr(9).'DISCOUNT        '.Chr(9).'TOTAL        '.Chr(10);
            $emailC .= 'QTY          '.Chr(9).'SUPPLIER CODE'.Chr(9).'DESCRIPTION  '.Chr(9).Chr(9).'VALUE        '.Chr(9).'NETT VALUE        '.Chr(9).'DISCOUNT        '.Chr(9).'TOTAL        '.Chr(10);
            $html .= '<TABLE border=1><TR><TD>QTY</TD><TD>SUPPLIER CODE</TD><TD>DESCRIPTION</TD><TD>VALUE</TD><TD>NETT VALUE</TD><TD>DISCOUNT</TD><TD>TOTAL</TD></TR>';
            $htmlC .= '<TABLE border=1><TR><TD>QTY</TD><TD>SUPPLIER CODE</TD><TD>DESCRIPTION</TD><TD>VALUE</TD><TD>NETT VALUE</TD><TD>DISCOUNT</TD><TD>TOTAL</TD></TR>';
            
            $resultSet = ExecuteQuery('SELECT * FROM Items WHERE Items_Order_Number = "'.$_SESSION['ApproveInternalOrder'][0].'"');
            while ($rowC = MySQL_Fetch_Array($resultSet))
            {
              $nett_value_mail = $rowC['Items_Original_Value'];
            $discount = 0;
                                if($rowC['Items_Discount'] > 0)
                                {
                                    $nett_value_mail -= (($rowC['Items_Discount']/100) * $nett_value_mail);
                                    $discount = ($rowC['Items_Discount']/100) * $rowC['Items_Original_Total'];
                                }
                                
              $email .= $rowC['Items_Quantity'].Chr(9).Chr(9).Chr(9).$rowC['Items_Supplier_Code'].Chr(9).Chr(9).Chr(9).$rowC['Items_Description'].Chr(9).Chr(9).Chr(9).Chr(9).Chr(9).$loCurrencySymbol.SPrintF('%02.2f', $rowC['Items_Original_Value']).Chr(9).Chr(9).$loCurrencySymbol.SPrintF('%02.2f', $nett_value_mail).Chr(9).Chr(9).$rowC['Items_Discount'].'%'.Chr(9).Chr(9).$loCurrencySymbol.SPrintF('%02.2f', ($rowC['Items_Original_Total'] - $discount)).Chr(10).Chr(10);
              $emailC .= $rowC['Items_Quantity'].Chr(9).Chr(9).Chr(9).$rowC['Items_Supplier_Code'].Chr(9).Chr(9).Chr(9).$rowC['Items_Description'].Chr(9).Chr(9).Chr(9).Chr(9).Chr(9).$loCurrencySymbol.SPrintF('%02.2f', $rowC['Items_Original_Value']).Chr(9).Chr(9).$loCurrencySymbol.SPrintF('%02.2f', $nett_value_mail).Chr(9).Chr(9).$rowC['Items_Discount'].'%'.Chr(9).Chr(9).$loCurrencySymbol.SPrintF('%02.2f', ($rowC['Items_Original_Total'] - $discount)).Chr(10).Chr(10);
              $html .= '<TR><TD>'.$rowC['Items_Quantity'].'</TD><TD>'.$rowC['Items_Supplier_Code'].'</TD><TD>'.$rowC['Items_Description'].'</TD><TD>'.$loCurrencySymbol.SPrintF('%02.2f', $rowC['Items_Original_Value']).'</TD><TD>'.$loCurrencySymbol.SPrintF('%02.2f', $nett_value_mail).'</TD><TD>'.$rowC['Items_Discount'].'%'.'</TD><TD>'.$loCurrencySymbol.SPrintF('%02.2f', ($rowC['Items_Original_Total'] - $discount)).'</TD></TR>';
              $htmlC .= '<TR><TD>'.$rowC['Items_Quantity'].'</TD><TD>'.$rowC['Items_Supplier_Code'].'</TD><TD>'.$rowC['Items_Description'].'</TD><TD>'.$loCurrencySymbol.SPrintF('%02.2f', $rowC['Items_Original_Value']).'</TD><TD>'.$loCurrencySymbol.SPrintF('%02.2f', $nett_value_mail).'</TD><TD>'.$rowC['Items_Discount'].'%'.'</TD><TD>'.$loCurrencySymbol.SPrintF('%02.2f', ($rowC['Items_Original_Total'] - $discount)).'</TD></TR>';
            }
//            SendMailHTML($row['Staff_email'], 'Order Denied', $email, $html);
//            if ($row['Staff_email'] != 'cherie@s4integration.co.za')
//              SendMailHTML('cherie@s4integration.co.za', 'Order Denied', $emailC, $htmlC);
            if ($row['Staff_email'] != 'arthur@s4integration.co.za')
              SendMailHTML('arthur@s4integration.co.za', 'Order Denied', $emailC, $htmlC);
            if ($row['Staff_email'] != 'johannes@s4integration.co.za')
              SendMailHTML('johannes@s4integration.co.za', 'Order Denied', $emailC, $htmlC);
			  if ($row['Staff_email'] != 'malcolm@s4integration.co.za')
              SendMailHTML('malcolm@s4integration.co.za', 'Order Denied', $emailC, $htmlC);
			  if ($row['Staff_email'] != 'stefan@s4integration.co.za')
              SendMailHTML('stefan@s4integration.co.za', 'Order Denied', $emailC, $htmlC);
            if ($row['Staff_email'] != 'gideon@s4integration.co.za')
              SendMailHTML('gideon@s4integration.co.za', 'Order Denied', $emailC, $htmlC);
            $_SESSION['InternalOrderSuccess'] = 'geh!';
            Session_Unregister('ApproveInternalOrder');
          } else
    	      $_SESSION['InternalOrderFail'] = 'geh!';
        break;
      case 'Approve':
        if (CheckFields())
        {
          $date = Date('Y-m-d H:i:s');
          if (ExecuteQuery('UPDATE OrderNo SET OrderNo_Approved = "1", OrderNo_Approved_By = "'.$_POST['Approved'].'", OrderNo_Approved_Date = "'.Date('Y-m-d H:i:s').'", OrderNo_Approved_Comment = "'.$_POST['Comments'].'", OrderNo_Approved_Type = "'.$_POST['ApprovedType'].'" WHERE OrderNo_Number = "'.$_SESSION['ApproveInternalOrder'][0].'"'))
    	    {
            $row = MySQL_Fetch_Array(ExecuteQuery('SELECT OrderNo.OrderNo_ReportCategory, OrderNo.`quotation_number`, Staff_First_Name, Staff_Last_Name, Staff_email, Staff_Phone_Work, Supplier_Name, Supplier_Phone, Supplier_Code, Supplier_Address_Street, Supplier_Address_Suburb, Supplier_Address_City, Supplier_Address_Code, Project.* , OrderNo.* FROM Staff, Supplier, Project, OrderNo WHERE Staff_Code = OrderNo_Requested AND Supplier_Code = OrderNo_Supplier AND Project_Code = OrderNo_Project AND OrderNo_Number= "'.$_SESSION['ApproveInternalOrder'][0].'"'));
            $row2 = MySQL_Fetch_Array(ExecuteQuery('SELECT `Level` FROM BEE_Certificate WHERE Supplier_Code = '.$row['Supplier_Code']));
            
            $levelBEE = 'non-compliant';
            if($row2['Level']!=null){
                   if($row2['Level'] == 9){
                       $levelBEE = "N/A - International";
                    }
                    
                      else if($row2['Level'] == 10){
                           $levelBEE = "Awaiting feedback";
                    }
                    
                    else if($row2['Level'] == 0){
                       $levelBEE = "non-compliant";
                    }
                    
                    else
                        $levelBEE = $row2['Level'];
            }
            
            
            // red
            $tdLevel = '<TD style="font-weight: bold; background-color: rgba(255, 0, 0, 0.32); padding-left: 0.5em; padding-right: 0.5em; " >non-compliant</TD>';
              if($row2['Level']!=null){
                   if($row2['Level'] == 9){
                        $tdLevel = '<TD style="font-weight: bold; background-color: rgba(10, 148, 23, 0.32); padding-left: 0.5em; padding-right: 0.5em; " >N/A - International</TD>';
                    }
                    
                    else if($row2['Level'] == 10){
                         $tdLevel = '<TD style="font-weight: bold; background-color: rgba(10, 115, 254, 0.58); padding-left: 0.5em; padding-right: 0.5em; " >Awaiting feedback</TD>';
                    } 
                    
                    
                    else if($row2['Level'] == 0){
                        $tdLevel = '<TD style="font-weight: bold; background-color: rgba(255, 0, 0, 0.32); padding-left: 0.5em; padding-right: 0.5em; " >non-compliant</TD>';
                    }
                    
                    else if($row2['Level'] < 6) { // 1 2 3 4 5 
                        $tdLevel = '<TD style="font-weight: bold; background-color: rgba(10, 148, 23, 0.32); padding-left: 0.5em; padding-right: 0.5em; " >'.$row2['Level'].'</TD>';
                    }
                    
                    else {   // 6 7 8
                          $tdLevel = '<TD style="font-weight: bold; background-color: rgba(245, 253, 21, 0.58); padding-left: 0.5em; padding-right: 0.5em; " >'.$row2['Level'].'</TD>';
                    }
            }
            
            
            $rowB = MySQL_Fetch_Array(ExecuteQuery('SELECT `Staff_Code` ,Staff_First_Name, Staff_Last_Name FROM Staff WHERE Staff_Code = '.$_POST['Approved']));
            $loCurrencyRow = MySQL_Fetch_Array(ExecuteQuery('SELECT C.Currency_Rate, C.Currency_Symbol FROM Currency C WHERE Currency_ID = "'.$row['OrderNo_Original_Currency'].'"'));
            $loCurrencySymbol = $loCurrencyRow['Currency_Symbol'];
            
            $qNumber = $row['quotation_number'];
            $qnumber2 = "";
            if($qNumber!= null || $qNumber!= '' ){
                
                $qNumber = 'QUOTATION NUMBER:               '.$row['quotation_number'];
                $qnumber2 = '<TR><TD><B>Quotation number:</B></TD><TD>'.$row['quotation_number'].'</TR>';
            }
            
            //get the string Name (Cost Manager: name)
         
              if( strcmp($row['OrderNo_ReportCategory'], "0") == 0){
                  $costManagerOnCategory =  'Project_CostManager';
              }
              else{
                $costManagerOnCategory =  'Project_CostManager'.$row['OrderNo_ReportCategory'];
              }
            $rowC = MySQL_Fetch_Array(ExecuteQuery('SELECT `Staff_Code`, `Staff_First_Name`, `Staff_Last_Name` FROM Staff WHERE `Staff_Code` = (SELECT IFNULL(`'.$costManagerOnCategory.'`,`Project_Responsible`) FROM Project WHERE `Project_Code` = '.$row['Project_Code'].')'));
            
            $Approved = "";
            if($rowB['Staff_Code'] != $rowC['Staff_Code']){
               
                $Approved = $rowB['Staff_First_Name'].' '.$rowB['Staff_Last_Name'].' (Cost Manager: '.$rowC['Staff_First_Name'].' '.$rowC['Staff_Last_Name'].')';
            }
            
            else{
                $Approved = $rowB['Staff_First_Name'].' '.$rowB['Staff_Last_Name'];
            }
            
            
            
            $email = 'The following order has been approved:'.Chr(10).
                     'ORDER NO:               '.$row['OrderNo_Number'].Chr(10).
                     'DATE:                   '.GetTextualDateFromDatabaseDate($row['OrderNo_Date_Time']).Chr(10).
                     'PROJECT:                '.$row['Project_Pastel_Prefix'].' - '.$row['Project_Description'].Chr(10).
                     'SUPPLIER:               '.$row['Supplier_Name'].' - '.$row['Supplier_Phone'].Chr(10).
                     'BEE Level:             '.$levelBEE.
                      $qNumber.Chr(10).
                     'FOLLOW UP DATE:         '.$row['OrderNo_Date_FollowUp'].Chr(10).
                     'DELIVERY DATE:          '.$row['OrderNo_Date_Delivery'].Chr(10).
                     'APPROVED BY:            '.$Approved.
                     'TOTAL (EXCL. VAT):      '.$loCurrencySymbol.$row['OrderNo_Original_Total_Cost'].Chr(10).Chr(10);
            $emailC = 'The following order has been approved:'.Chr(10).
                     'ORDER NO:               '.$row['OrderNo_Number'].Chr(10).
                     'DATE:                   '.GetTextualDateFromDatabaseDate($row['OrderNo_Date_Time']).Chr(10).
                     'REQUESTED BY:           '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].Chr(10).
                     'PROJECT:                '.$row['Project_Pastel_Prefix'].' - '.$row['Project_Description'].Chr(10).
                     'SUPPLIER:               '.$row['Supplier_Name'].' - '.$row['Supplier_Phone'].Chr(10).
                     'BEE Level:              '.$levelBEE.
                     $qNumber.Chr(10).
                     'FOLLOW UP DATE:         '.$row['OrderNo_Date_FollowUp'].Chr(10).
                     'DELIVERY DATE:          '.$row['OrderNo_Date_Delivery'].Chr(10).
                     'APPROVED BY:            '.$Approved.
                     'TOTAL (EXCL. VAT):      '.$loCurrencySymbol.$row['OrderNo_Original_Total_Cost'].Chr(10).Chr(10);
            $emailReminder = Chr(10).'ORDER NO:               '.$row['OrderNo_Number'].Chr(10).
                     'DATE:                   '.GetTextualDateFromDatabaseDate($row['OrderNo_Date_Time']).Chr(10).
                     'PROJECT:                '.$row['Project_Pastel_Prefix'].' - '.$row['Project_Description'].Chr(10).
                     'SUPPLIER:               '.$row['Supplier_Name'].' - '.$row['Supplier_Phone'].Chr(10).
                     'BEE Level:              '.$levelBEE.
                     $qNumber.Chr(10).
                     'COMMENTS:               '.$row['OrderNo_Comments'].Chr(10).
                     'FOLLOW UP DATE:         '.$row['OrderNo_Date_FollowUp'].Chr(10).
                     'DELIVERY DATE:          '.$row['OrderNo_Date_Delivery'].Chr(10).
                     'APPROVED BY:            '.$Approved.
                     'TOTAL (EXCL. VAT):      '.$loCurrencySymbol.$row['OrderNo_Original_Total_Cost'].Chr(10).Chr(10);
            
            $html = 'The following order has been approved:
                    <BR /><BR />
                    <TABLE border=0>
                      <TR><TD><B>Order No:</B></TD><TD>'.$row['OrderNo_Number'].'</TD></TR>
                      <TR><TD><B>Date:</B></TD><TD>'.GetTextualDateFromDatabaseDate($row['OrderNo_Date_Time']).'</TD></TR>
                      <TR><TD><B>Project:</B></TD><TD>'.$row['Project_Pastel_Prefix'].' - '.$row['Project_Description'].'</TD></TR>
                      <TR><TD><B>Supplier:</B></TD><TD>'.$row['Supplier_Name'].' - '.$row['Supplier_Phone'].'</TD></TR>
                      <TR><TD><B>BEE Level:</B></TD>'.$tdLevel.'</TR>'.
                      $qnumber2.
                      '<TR><TD><B>Follow Up Date:</B></TD><TD>'.GetTextualDateFromDatabaseDate($row['OrderNo_Date_FollowUp']).'</TD></TR>
                      <TR><TD><B>Delivery Date:</B></TD><TD>'.GetTextualDateFromDatabaseDate($row['OrderNo_Date_Delivery']).'</TD></TR>
                      <TR><TD><B>Approved By:</B></TD><TD>'.$Approved.'</TD></TR>
                      <TR><TD><B>Total (Excl. VAT):</B></TD><TD>'.$loCurrencySymbol.$row['OrderNo_Original_Total_Cost'].'</TD></TR>
                    </TABLE>
                    <BR /><BR />';
            $htmlC = 'The following order has been approved:
                    <BR /><BR />
                    <TABLE border=0>
                      <TR><TD><B>Order No:</B></TD><TD>'.$row['OrderNo_Number'].'</TD></TR>
                      <TR><TD><B>Date:</B></TD><TD>'.GetTextualDateFromDatabaseDate($row['OrderNo_Date_Time']).'</TD></TR>
                      <TR><TD><B>Requested By:</B></TD><TD>'.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].'</TD></TR>
                      <TR><TD><B>Project:</B></TD><TD>'.$row['Project_Pastel_Prefix'].' - '.$row['Project_Description'].'</TD></TR>
                      <TR><TD><B>Supplier:</B></TD><TD>'.$row['Supplier_Name'].' - '.$row['Supplier_Phone'].'</TD></TR>
                      <TR><TD><B>BEE Level:</B></TD>'.$tdLevel.'</TR>'.
                     $qnumber2.
                      '<TR><TD><B>Follow Up Date:</B></TD><TD>'.GetTextualDateFromDatabaseDate($row['OrderNo_Date_FollowUp']).'</TD></TR>
                      <TR><TD><B>Delivery Date:</B></TD><TD>'.GetTextualDateFromDatabaseDate($row['OrderNo_Date_Delivery']).'</TD></TR>
                      <TR><TD><B>Approved By:</B></TD><TD>'.$Approved.'</TD></TR>
                      <TR><TD><B>Total (Excl. VAT):</B></TD><TD>'.$loCurrencySymbol.$row['OrderNo_Original_Total_Cost'].'</TD></TR>

                    </TABLE>
                    <BR /><BR />';
            $htmlReminder = '<BR /><BR />
                    <TABLE border=0>
                      <TR><TD><B>Order No:</B></TD><TD>'.$row['OrderNo_Number'].'</TD></TR>
                      <TR><TD><B>Date:</B></TD><TD>'.GetTextualDateFromDatabaseDate($row['OrderNo_Date_Time']).'</TD></TR>
                      <TR><TD><B>Project:</B></TD><TD>'.$row['Project_Pastel_Prefix'].' - '.$row['Project_Description'].'</TD></TR>
                      <TR><TD><B>Supplier:</B></TD><TD>'.$row['Supplier_Name'].' - '.$row['Supplier_Phone'].'</TD></TR>
                      <TR><TD><B>BEE Level:</B></TD>'.$tdLevel.'</TR>'.
                     $qnumber2.
                      '<TR><TD><B>Comments:</B></TD><TD>'.$row['OrderNo_Comments'].'</TD></TR>
                      <TR><TD><B>Follow Up Date:</B></TD><TD>'.GetTextualDateFromDatabaseDate($row['OrderNo_Date_FollowUp']).'</TD></TR>
                      <TR><TD><B>Delivery Date:</B></TD><TD>'.GetTextualDateFromDatabaseDate($row['OrderNo_Date_Delivery']).'</TD></TR>
                      <TR><TD><B>Approved By:</B></TD><TD>'.$Approved.'</TD></TR>
                      <TR><TD><B>Total (Excl. VAT):</B></TD><TD>'.$loCurrencySymbol.$row['OrderNo_Original_Total_Cost'].'</TD></TR>
                    </TABLE>
                    <BR /><BR />';
            
            $emailLargeAmount = 'The following order\'s total amount is greater than R100,000.00:'.Chr(10).
                             'ORDER NO:               '.$row['OrderNo_Number'].Chr(10).
                             'DATE:                   '.GetTextualDateFromDatabaseDate($row['OrderNo_Date_Time']).Chr(10).
                             'REQUESTED BY:           '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].Chr(10).
                             'PROJECT:                '.$row['Project_Pastel_Prefix'].' - '.$row['Project_Description'].Chr(10).
                             'SUPPLIER:               '.$row['Supplier_Name'].' - '.$row['Supplier_Phone'].Chr(10).
                             'BEE Level:              '.$levelBEE.   
                              $qNumber.Chr(10).
                             'COMMENTS:               '.$row['OrderNo_Comments'].Chr(10).
                             'APPROVED BY:            '.$Approved.
                             'TOTAL (EXCL. VAT):      '.$loCurrencySymbol.$row['OrderNo_Original_Total_Cost'].Chr(10).Chr(10);
    
            $htmlLargeAmount = 'The following order\'s total amount is greater than R100,000.00:
                            <BR /><BR />
                            <TABLE border=0>
                              <TR><TD><B>Order No:</B></TD><TD>'.$row['OrderNo_Number'].'</TD></TR>
                              <TR><TD><B>Date:</B></TD><TD>'.GetTextualDateFromDatabaseDate($row['OrderNo_Date_Time']).'</TD></TR>
                              <TR><TD><B>Requested By:</B></TD><TD>'.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].'</TD></TR>
                              <TR><TD><B>Project:</B></TD><TD>'.$row['Project_Pastel_Prefix'].' - '.$row['Project_Description'].'</TD></TR>
                              <TR><TD><B>Supplier:</B></TD><TD>'.$row['Supplier_Name'].' - '.$row['Supplier_Phone'].'</TD></TR>
                              <TR><TD><B>BEE Level:</B></TD>'.$tdLevel.'</TR>'.
                             $qnumber2.
                             '<TR><TD><B>Comments:</B></TD><TD>'.$row['OrderNo_Comments'].'</TD></TR>
                              <TR><TD><B>Approved By:</B></TD><TD>'.$Approved.'</TD></TR>
                              <TR><TD><B>Total (Excl. VAT):</B></TD><TD>'.$loCurrencySymbol.$row['OrderNo_Original_Total_Cost'].'</TD></TR>
                            </TABLE>
                            <BR /><BR />';
            
            $email .= 'QTY          '.Chr(9).'SUPPLIER CODE'.Chr(9).'DESCRIPTION  '.Chr(9).Chr(9).'VALUE        '.Chr(9).'NETT VALUE        '.Chr(9).'DISCOUNT        '.Chr(9).'TOTAL        '.Chr(10);
            $emailC .= 'QTY          '.Chr(9).'SUPPLIER CODE'.Chr(9).'DESCRIPTION  '.Chr(9).Chr(9).'VALUE        '.Chr(9).'NETT VALUE        '.Chr(9).'DISCOUNT        '.Chr(9).'TOTAL        '.Chr(10);
            $emailReminder .= 'QTY          '.Chr(9).'SUPPLIER CODE'.Chr(9).'DESCRIPTION  '.Chr(9).Chr(9).'VALUE        '.Chr(9).'NETT VALUE        '.Chr(9).'DISCOUNT        '.Chr(9).'TOTAL        '.Chr(10);
            $html .= '<TABLE border=1><TR><TD>QTY</TD><TD>SUPPLIER CODE</TD><TD>DESCRIPTION</TD><TD>VALUE</TD><TD>NETT VALUE</TD><TD>DISCOUNT</TD><TD>TOTAL</TD></TR>';
            $htmlC .= '<TABLE border=1><TR><TD>QTY</TD><TD>SUPPLIER CODE</TD><TD>DESCRIPTION</TD><TD>VALUE</TD><TD>NETT VALUE</TD><TD>DISCOUNT</TD><TD>TOTAL</TD></TR>';
            $htmlReminder .= '<TABLE border=1><TR><TD>QTY</TD><TD>SUPPLIER CODE</TD><TD>DESCRIPTION</TD><TD>VALUE</TD><TD>NETT VALUE</TD><TD>DISCOUNT</TD><TD>TOTAL</TD></TR>';
            $emailLargeAmount .= 'QTY          '.Chr(9).'SUPPLIER CODE'.Chr(9).'DESCRIPTION  '.Chr(9).Chr(9).'VALUE        '.Chr(9).'NETT VALUE        '.Chr(9).'DISCOUNT        '.Chr(9).'TOTAL        '.Chr(10);
            $htmlLargeAmount .= '<TABLE border=1><TR><TD>QTY</TD><TD>SUPPLIER CODE</TD><TD>DESCRIPTION</TD><TD>VALUE</TD><TD>NETT VALUE</TD><TD>DISCOUNT</TD><TD>TOTAL</TD></TR>';
            
            $resultSet = ExecuteQuery('SELECT * FROM Items WHERE Items_Order_Number = "'.$_SESSION['ApproveInternalOrder'][0].'"');
            $data = array();
            while ($rowC = MySQL_Fetch_Array($resultSet))
            {
              $nett_value_mail = $rowC['Items_Original_Value'];
              $discount = 0;
                                if($rowC['Items_Discount'] > 0)
                                {
                                    $nett_value_mail -= (($rowC['Items_Discount']/100) * $nett_value_mail);
                                    $discount = ($rowC['Items_Discount']/100) * $rowC['Items_Original_Total'];
                                }
                
              $data[] = array(
                  
                  floatval($rowC['Items_Quantity']), $rowC['Items_Supplier_Code'],$rowC['Items_Description'],$loCurrencySymbol.SPrintF('%02.2f', $rowC['Items_Original_Value']),floatval($rowC['Items_Discount']).'%',$loCurrencySymbol.SPrintF('%02.2f', ($rowC['Items_Original_Total'] - $discount)));                  
              $email .= $rowC['Items_Quantity'].Chr(9).Chr(9).Chr(9).$rowC['Items_Supplier_Code'].Chr(9).Chr(9).Chr(9).$rowC['Items_Description'].Chr(9).Chr(9).Chr(9).Chr(9).Chr(9).$loCurrencySymbol.SPrintF('%02.2f', $rowC['Items_Original_Value']).Chr(9).Chr(9).$loCurrencySymbol.SPrintF('%02.2f', $nett_value_mail).Chr(9).Chr(9).$rowC['Items_Discount'].'%'.Chr(9).Chr(9).$loCurrencySymbol.SPrintF('%02.2f', ($rowC['Items_Original_Total'] - $discount)).Chr(10).Chr(10);
              $emailC .= $rowC['Items_Quantity'].Chr(9).Chr(9).Chr(9).$rowC['Items_Supplier_Code'].Chr(9).Chr(9).Chr(9).$rowC['Items_Description'].Chr(9).Chr(9).Chr(9).Chr(9).Chr(9).$loCurrencySymbol.SPrintF('%02.2f', $rowC['Items_Original_Value']).Chr(9).Chr(9).$loCurrencySymbol.SPrintF('%02.2f', $nett_value_mail).Chr(9).Chr(9).$rowC['Items_Discount'].'%'.Chr(9).Chr(9).$loCurrencySymbol.SPrintF('%02.2f', ($rowC['Items_Original_Total'] - $discount)).Chr(10).Chr(10);
              $emailReminder .= $rowC['Items_Quantity'].Chr(9).Chr(9).Chr(9).$rowC['Items_Supplier_Code'].Chr(9).Chr(9).Chr(9).$rowC['Items_Description'].Chr(9).Chr(9).Chr(9).Chr(9).Chr(9).$loCurrencySymbol.SPrintF('%02.2f', $rowC['Items_Original_Value']).Chr(9).Chr(9).$loCurrencySymbol.SPrintF('%02.2f', $nett_value_mail).Chr(9).Chr(9).$rowC['Items_Discount'].'%'.Chr(9).Chr(9).$loCurrencySymbol.SPrintF('%02.2f', ($rowC['Items_Original_Total'] - $discount)).Chr(10).Chr(10);
              $html .= '<TR><TD>'.$rowC['Items_Quantity'].'</TD><TD>'.$rowC['Items_Supplier_Code'].'</TD><TD>'.$rowC['Items_Description'].'</TD><TD>'.$loCurrencySymbol.SPrintF('%02.2f', $rowC['Items_Original_Value']).'</TD><TD>'.$loCurrencySymbol.SPrintF('%02.2f', $nett_value_mail).'</TD><TD>'.$rowC['Items_Discount'].'%'.'</TD><TD>'.$loCurrencySymbol.SPrintF('%02.2f', ($rowC['Items_Original_Total'] - $discount)).'</TD></TR>';
              $htmlC .= '<TR><TD>'.$rowC['Items_Quantity'].'</TD><TD>'.$rowC['Items_Supplier_Code'].'</TD><TD>'.$rowC['Items_Description'].'</TD><TD>'.$loCurrencySymbol.SPrintF('%02.2f', $rowC['Items_Original_Value']).'</TD><TD>'.$loCurrencySymbol.SPrintF('%02.2f', $nett_value_mail).'</TD><TD>'.$rowC['Items_Discount'].'%'.'</TD><TD>'.$loCurrencySymbol.SPrintF('%02.2f', ($rowC['Items_Original_Total'] - $discount)).'</TD></TR>';
              $htmlReminder .= '<TR><TD>'.$rowC['Items_Quantity'].'</TD><TD>'.$rowC['Items_Supplier_Code'].'</TD><TD>'.$rowC['Items_Description'].'</TD><TD>'.$loCurrencySymbol.SPrintF('%02.2f', $rowC['Items_Original_Value']).'</TD><TD>'.$loCurrencySymbol.SPrintF('%02.2f', $nett_value_mail).'</TD><TD>'.$rowC['Items_Discount'].'%'.'</TD><TD>'.$loCurrencySymbol.SPrintF('%02.2f', ($rowC['Items_Original_Total'] - $discount)).'</TD></TR>';
              $emailLargeAmount .= $rowC['Items_Quantity'].Chr(9).Chr(9).Chr(9).$rowC['Items_Supplier_Code'].Chr(9).Chr(9).Chr(9).$rowC['Items_Description'].Chr(9).Chr(9).Chr(9).Chr(9).Chr(9).$loCurrencySymbol.SPrintF('%02.2f', $rowC['Items_Original_Value']).Chr(9).Chr(9).$loCurrencySymbol.SPrintF('%02.2f', $nett_value_mail).Chr(9).Chr(9).$rowC['Items_Discount'].'%'.Chr(9).Chr(9).$loCurrencySymbol.SPrintF('%02.2f', ($rowC['Items_Original_Total'] - $discount)).Chr(10).Chr(10);
              $htmlLargeAmount .= '<TR><TD>'.$rowC['Items_Quantity'].'</TD><TD>'.$rowC['Items_Supplier_Code'].'</TD><TD>'.$rowC['Items_Description'].'</TD><TD>'.$loCurrencySymbol.SPrintF('%02.2f', $rowC['Items_Original_Value']).'</TD><TD>'.$loCurrencySymbol.SPrintF('%02.2f', $nett_value_mail).'</TD><TD>'.$rowC['Items_Discount'].'%'.'</TD><TD>'.$loCurrencySymbol.SPrintF('%02.2f', ($rowC['Items_Original_Total'] - $discount)).'</TD></TR>';
            }
            $email .= Chr(10).Chr(10).Chr(10).'___________________________________'.Chr(10).Chr(10).$rowB['Staff_First_Name'].' '.$rowB['Staff_Last_Name'];
            $emailC .= Chr(10).Chr(10).Chr(10).'___________________________________'.Chr(10).Chr(10).$rowB['Staff_First_Name'].' '.$rowB['Staff_Last_Name'];
            $html .= '</TABLE><BR /><BR /><BR /><BR />___________________________________<BR /><BR />'.$rowB['Staff_First_Name'].' '.$rowB['Staff_Last_Name'];
            $htmlC .= '</TABLE><BR /><BR /><BR /><BR />___________________________________<BR /><BR />'.$rowB['Staff_First_Name'].' '.$rowB['Staff_Last_Name'];
            $htmlLargeAmount .= '</TABLE>';
            
            //PDF Attachment   
             $pdf_filename= '../Files/Temp/'.$row['OrderNo_Number'].'.pdf';
             $columnLabels = array( "Qty", "Supplier Code","Description","Price","Discount","Total" );
             $pdf = GeneratePdf($pdf_filename,$columnLabels,$data,$row,$rowB,$loCurrencySymbol);
             $pdf->Output($pdf_filename, "F");
             
            SendMailHTML($row['Staff_email'], 'Order Approved - S4 - '.$row['OrderNo_Number'], $email, $html,$pdf_filename);
//            if ($row['Staff_email'] != 'cherie@s4integration.co.za')
//              SendMailHTML('cherie@s4integration.co.za', 'Order Approved - S4 - '.$row['OrderNo_Number'], $emailC, $htmlC,$pdf_filename);
            if ($row['Staff_email'] != 'arthur@s4integration.co.za')
              SendMailHTML('arthur@s4integration.co.za', 'Order Approved - S4 - '.$row['OrderNo_Number'], $emailC, $htmlC,$pdf_filename);
            if ($row['Staff_email'] != 'johannes@s4integration.co.za')
              SendMailHTML('johannes@s4integration.co.za', 'Order Approved - S4 - '.$row['OrderNo_Number'], $emailC, $htmlC,$pdf_filename);
			if ($row['Staff_email'] != 'gideon@s4integration.co.za')
              SendMailHTML('gideon@s4integration.co.za', 'Order Approved - S4 - '.$row['OrderNo_Number'], $emailC, $htmlC,$pdf_filename);
            if ($row['Staff_email'] != 'malcolm@s4integration.co.za')
              SendMailHTML('malcolm@s4integration.co.za', 'Order Approved - S4 - '.$row['OrderNo_Number'], $emailC, $htmlC,$pdf_filename);
            if ($row['Staff_email'] != 'stefan@s4integration.co.za')
              SendMailHTML('stefan@s4integration.co.za', 'Order Approved - S4 - '.$row['OrderNo_Number'], $emailC, $htmlC,$pdf_filename);
            if ($row['Staff_email'] != 'office.admin@s4.co.za')
              SendMailHTML('office.admin@s4.co.za', 'Order Approved - S4 - '.$row['OrderNo_Number'], $emailC, $htmlC,$pdf_filename);
            if ($row['Staff_email'] != 'amelia@s4integration.co.za')
              SendMailHTML('amelia@s4integration.co.za', 'Order Approved - S4 - '.$row['OrderNo_Number'], $emailC, $htmlC,$pdf_filename);
            
            $management = array('vaughn','andrew');
            if($row['OrderNo_Original_Total_Cost'] > 100000)
            {
                foreach ($management as $pers) 
                SendMailHTML($pers.'@s4integration.co.za', 'Order Greater than R100,000.00 - S4 - '.$row['OrderNo_Number'], $emailLargeAmount, $htmlLargeAmount,$pdf_filename);
            }
            
            unlink($pdf_filename);
            //$today = Date('Y-m-d');
            //if (!DatabaseDateLater($row['OrderNo_Date_Delivery'], $today) && ($row['OrderNo_Date_Delivery'] != $today))
            //  ExecuteQuery('INSERT INTO Reminder VALUES("", "'.$_SESSION['cUID'].'", "'.Date('Y-m-d H:i:s').'", "'.$row['OrderNo_Requested'].'", "'.GetReminderDate($row['OrderNo_Date_Delivery']).'", "4", "5", "32", "Order <B>'.$row['OrderNo_Number'].'</B> due for delivery on '.GetTextualDateFromDatabaseDate($row['OrderNo_Date_Delivery']).'. '.$row['Supplier_Name'].' for '.$row['Project_Description'].'.", "IO", "'.$row['OrderNo_Number'].'", "", "")');
            //else
              //ExecuteQuery('INSERT INTO Reminder VALUES("", "'.$_SESSION['cUID'].'", "'.Date('Y-m-d H:i:s').'", "'.$row['OrderNo_Requested'].'", "'.GetReminderDate($row['OrderNo_Date_Delivery']).'", "4", "5", "32", "Order <B>'.$row['OrderNo_Number'].'</B> due for delivery on '.GetTextualDateFromDatabaseDate($row['OrderNo_Date_Delivery']).'. '.$row['Supplier_Name'].' for '.$row['Project_Description'].'.", "IO", "'.$row['OrderNo_Number'].'", "Reminder for the delivery of the following order: '.$emailReminder.'", "Reminder for the delivery of the following order: '.$htmlReminder.'")');
            //if (!DatabaseDateLater($row['OrderNo_Date_FollowUp'], $today) && ($row['OrderNo_Date_FollowUp'] != $today))
            //  ExecuteQuery('INSERT INTO Reminder VALUES("", "'.$_SESSION['cUID'].'", "'.Date('Y-m-d H:i:s').'", "'.$row['OrderNo_Requested'].'", "'.GetReminderDate($row['OrderNo_Date_FollowUp']).'", "4", "5", "32", "Order <B>'.$row['OrderNo_Number'].'</B> due for follow up on '.GetTextualDateFromDatabaseDate($row['OrderNo_Date_FollowUp']).'. '.$row['Supplier_Name'].' for '.$row['Project_Description'].'.", "IO", "'.$row['OrderNo_Number'].'", "", "")');
            //else
             // ExecuteQuery('INSERT INTO Reminder VALUES("", "'.$_SESSION['cUID'].'", "'.Date('Y-m-d H:i:s').'", "'.$row['OrderNo_Requested'].'", "'.GetReminderDate($row['OrderNo_Date_FollowUp']).'", "4", "5", "32", "Order <B>'.$row['OrderNo_Number'].'</B> due for follow up on '.GetTextualDateFromDatabaseDate($row['OrderNo_Date_FollowUp']).'. '.$row['Supplier_Name'].' for '.$row['Project_Description'].'.", "IO", "'.$row['OrderNo_Number'].'", "Reminder for the follow up of the following order: '.$emailReminder.'", "Reminder for the follow up of the following order: '.$htmlReminder.'")');
            
            if ($_POST['Discrepency'] != "")
            {
              $row = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM OrderNo WHERE OrderNo_Number = "'.$_SESSION['ApproveInternalOrder'][0].'"'));
              $rowTempB = MySQL_Fetch_Array(ExecuteQuery('SELECT Staff_First_Name, Staff_Last_Name FROM Staff WHERE Staff_Code = '.$row['OrderNo_Requested'].''));
              $rowTempC = MySQL_Fetch_Array(ExecuteQuery('SELECT OrderDiscrepencyType.* FROM OrderDiscrepencyType WHERE OrderDiscrepencyType_ID = '.$_POST['Discrepency'].''));
              $rowTempD = MySQL_Fetch_Array(ExecuteQuery('SELECT Supplier.* FROM Supplier WHERE Supplier_Code = '.$row['OrderNo_Supplier'].''));
              $rowTempE = MySQL_Fetch_Array(ExecuteQuery('SELECT Project.* FROM Project WHERE Project_Code = '.$row['OrderNo_Project'].''));
              
              $email = 'There is a discrepency with an order. The details are as follows:'.Chr(10).
                       'REQUESTED BY:           '.$rowTempB['Staff_First_Name'].' '.$rowTempB['Staff_Last_Name'].Chr(10).
                       'ORDER NUMBER:           '.$_SESSION['ApproveInternalOrder'][0].Chr(10).
                       'SUPPLIER:               '.$rowTempD['Supplier_Name'][0].Chr(10).
                       'BEE Level               '.$levelBEE. 
                       $qNumber.Chr(10).
                       'PROJECT:                '.$rowTempE['Project_Description'][0].Chr(10).
                       'INVOICE:                -'.Chr(10).
                       'DATE RECORDED:          '.GetTextualDateTimeFromDatabaseDateTime($date).Chr(10).
                       'DISCREPENCY:            '.$rowTempC['OrderDiscrepencyType_Description'].Chr(10).
                       'DESCRIPTION:            -';
              
              $html = 'There is a discrepency with an order. The details are as follows:
                      <BR /><BR />
                      <TABLE border=0>
                        <TR><TD><B>Requested By:</B></TD><TD>'.$rowTempB['Staff_First_Name'].' '.$rowTempB['Staff_Last_Name'].'</TD></TR>
                        <TR><TD><B>Order Number:</B></TD><TD>'.$_SESSION['ApproveInternalOrder'][0].'</TD></TR>
                        <TR><TD><B>Supplier:</B></TD><TD>'.$rowTempD['Supplier_Name'][0].'</TD></TR>
                        <TR><TD><B>Bee Level:</B></TD>'.$tdLevel.'</TR>'.
                       $qnumber2.
                        '<TR><TD><B>Project:</B></TD><TD>'.$rowTempE['Project_Description'][0].'</TD></TR>
                        <TR><TD><B>Invoice:</B></TD><TD>-</TD></TR>
                        <TR><TD><B>Date Recorded:</B></TD><TD>'.GetTextualDateTimeFromDatabaseDateTime($date).'</TD></TR>
                        <TR><TD><B>Discrepency:</B></TD><TD>'.$rowTempC['OrderDiscrepencyType_Description'].'</TD></TR>
                        <TR><TD><B>Description:</B></TD><TD>-</TD></TR>
                      </TABLE>';
              
      	      SendMailHTML('andrew@s4integration.co.za', 'Internal Order Discrepency', $email, $html);
      	      ExecuteQuery('DELETE FROM OrderDiscrepency WHERE OrderDiscrepency_Type = "'.$_POST['Discrepency'].'" AND OrderDiscrepency_Order = "'.$_SESSION['ApproveInternalOrder'][0].'" AND OrderDiscrepency_Invoice = "0" AND OrderDiscrepency_Description = ""');
              ExecuteQuery('INSERT INTO OrderDiscrepency VALUES("", "'.$_POST['Discrepency'].'", "'.$_SESSION['ApproveInternalOrder'][0].'", "", "")');
            }
             
            
            $_SESSION['InternalOrderSuccess'] = 'geh!';
            Session_Unregister('ApproveInternalOrder');
          } else
    	      $_SESSION['InternalOrderFail'] = 'geh!';
        } else
          $_SESSION['InternalOrderIncomplete'] = 'geh!';
        break;
      default:
        break;
    }
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Handles the user's submission of modified information for an internal    //
  // order.                                                                   //
  //////////////////////////////////////////////////////////////////////////////
  function HandleEdit()
  {
    $_SESSION['EditInternalOrder'][0] = $_POST['Requested'];
    $_SESSION['EditInternalOrder'][1] = $_POST['Supplier'];
    $_SESSION['EditInternalOrder'][2] = $_POST['Project'];
    $_SESSION['EditInternalOrder'][3] = $_POST['OrderType'];
    $_SESSION['EditInternalOrder'][4] = $_POST['CostCurrency'];
    $_SESSION['EditInternalOrder'][5] = $_POST['Cost'];
    $_SESSION['EditInternalOrder'][6] = $_POST['Comments'];
    $_SESSION['EditInternalOrder'][8] = $_POST['SupplierInvoice'];
    $_SESSION['EditInternalOrder'][9] = 'geh!';
    $_SESSION['EditInternalOrder'][10] = $_POST['Delivery'];
    $_SESSION['EditInternalOrder'][11] = $_POST['SupplierCode'];
    $_SESSION['EditInternalOrder'][12] = $_POST['Description'];
    $_SESSION['EditInternalOrder'][13] = $_POST['Backorder'];
    $_SESSION['EditInternalOrder'][14] = $_POST['Value'];
    $_SESSION['EditInternalOrder'][15] = $_POST['Quantity'];
    $_SESSION['EditInternalOrder'][16] = $_POST['FollowDate'];
    $_SESSION['EditInternalOrder'][17] = $_POST['DeliveryDate'];
    $_SESSION['EditInternalOrder'][18] = $_POST['Asset'];
    $_SESSION['EditInternalOrder'][20] = $_POST['StorageType'];
    $_SESSION['EditInternalOrder'][21] = $_POST['Discount'];
    $_SESSION['EditInternalOrder'][22] = $_POST['ReportCategory'];
    $_SESSION['EditInternalOrder'][23] = $_POST['quotation_number'];
    //$_SESSION['EditInternalOrder'][24] = $_POST['BEE_Id'];
    if($_SESSION['EditInternalOrder'][21] ==0)
        $_SESSION['EditInternalOrder'][21]="";
    
    switch ($_POST['Submit'])
    {
      case 'Add':
        if (CheckFields())
        {
          $_SESSION['EditInternalOrder'][] = array($_POST['SupplierCode'], $_POST['Description'], $_POST['Value'], $_POST['Quantity'], $_POST['Backorder'], $_POST['Asset'], "",$_POST['StorageType'],$_POST['Discount']);
          $_SESSION['EditInternalOrder'][11] = "";
          $_SESSION['EditInternalOrder'][12] = "";
          $_SESSION['EditInternalOrder'][13] = "";
          $_SESSION['EditInternalOrder'][14] = "";
          $_SESSION['EditInternalOrder'][15] = "";
          $_SESSION['EditInternalOrder'][18] = "";
          $_SESSION['EditInternalOrder'][20] = "";
          $_SESSION['EditInternalOrder'][21] = "";
        } else
           {
               $_SESSION['EditInternalOrder'][21] = "";
              $_SESSION['InternalOrderIncomplete'] = 'geh!';
           }
        break;
      case 'Cancel':
        break;
      //case 'AutoApprove':
      case 'Submit':
        break;
      default: //Remove or Update.
        for ($count = 24; $count <= SizeOf($_SESSION['EditInternalOrder']); $count++)
        {
          if ($_POST['Remove'.$count] == 'Remove')
          {
            if ($_SESSION['EditInternalOrder'][$count][6] == "")
              $_SESSION['EditInternalOrder'][$count] = "";
            else
              $_SESSION['EditInternalOrder'][$count][1] = "";
            break;
          } else
          if ($_POST['Update'.$count] == 'Update')
          {
            if (CheckFields())
              $_SESSION['EditInternalOrder'][$count] = array($_POST['SupplierCode'.$count], $_POST['Description'.$count], $_POST['Value'.$count], $_POST['Quantity'.$count], $_POST['Backorder'.$count], $_POST['Asset'.$count], $_SESSION['EditInternalOrder'][$count][6], $_POST['StorageType'.$count], $_POST['Discount'.$count]);
            else
              $_SESSION['InternalOrderIncomplete'] = 'geh!';
            break;
          }
        }
        break;
    }
    
    $total = 0;
    for ($count = 24; $count <= SizeOf($_SESSION['EditInternalOrder']); $count++)
    {
      if ($_SESSION['EditInternalOrder'][$count] != "")
        if ($_SESSION['EditInternalOrder'][$count][1] != "")
        {
            $total += $_SESSION['EditInternalOrder'][$count][2] * $_SESSION['EditInternalOrder'][$count][3];
            if($_SESSION['EditInternalOrder'][$count][8] > 0)
             $total -= (($_SESSION['EditInternalOrder'][$count][2]/100)*$_SESSION['EditInternalOrder'][$count][8]) * $_SESSION['EditInternalOrder'][$count][3];
        }
           
    }
    $_SESSION['EditInternalOrder'][5] = $total;
    
    switch ($_POST['Submit'])
    {
      case 'Add':
        break;
      case 'Cancel':
        Session_Unregister('EditInternalOrder');
        break;
      //case 'AutoApprove': 
      case 'Submit':
        if (CheckFields())
        {   
//          if(needsReaproval()){
//                 
          $loCurrencyRow = MySQL_Fetch_Array(ExecuteQuery('SELECT C.Currency_Rate, C.Currency_Symbol FROM Currency C WHERE Currency_ID = "'.$_POST['CostCurrency'].'"'));
          $cost = $_POST['Cost'] * $loCurrencyRow['Currency_Rate'];
          $loCurrencySymbol = $loCurrencyRow['Currency_Symbol'];
          
          $rowCheck = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM OrderNo WHERE OrderNo_Number = "'.$_SESSION['EditInternalOrder'][7].'"'));
          $approval = $rowCheck['OrderNo_Approved'];
//	 if (($_POST['Requested'] != $rowCheck['OrderNo_Requested']) || ($_POST['Supplier'] != $rowCheck['OrderNo_Supplier']) || ($_POST['Project'] != $rowCheck['OrderNo_Project']) || ($_POST['CostCurrency'] != 1) || ($rowCheck['OrderNo_Total_Cost'] != $cost) || !($_SESSION['cAuth'] & 32) || ($_POST['quotation_number'] != $rowCheck['quotation_number']) || ($_POST['ReportCategory'] != $rowCheck['OrderNo_ReportCategory']) || ($_POST['OrderType'] != $rowCheck['OrderNo_Type']) )
//	          $approval = 0;
          if(needsReaproval())
              $approval = 0;
          
            if (ExecuteQuery('UPDATE OrderNo SET OrderNo_Approved = "'.$approval.'", OrderNo_Complete = "0", OrderNo_Requested = "'.$_POST['Requested'].'", OrderNo_Delivery = "'.$_POST['Delivery'].'", OrderNo_Type = "'.$_POST['OrderType'].'", OrderNo_Supplier = "'.$_POST['Supplier'].'", OrderNo_Comments = "'.$_POST['Comments'].'", OrderNo_Ent = "'.$_SESSION['cUID'].'", OrderNo_Supplier_Invoice = "'.$_POST['SupplierInvoice'].'", OrderNo_Project = "'.$_POST['Project'].'", OrderNo_Total_Cost = "'.$cost.'", OrderNo_Date_Delivery = "'.$_SESSION['EditInternalOrder'][17].'", OrderNo_Date_FollowUp = "'.$_SESSION['EditInternalOrder'][16].'", OrderNo_Original_Currency = "'.$_POST['CostCurrency'].'", OrderNo_Original_Total_Cost = "'.$_POST['Cost'].'", OrderNo_ReportCategory = "'.$_POST['ReportCategory'].'", quotation_number = "'.$_POST['quotation_number'].'" WHERE OrderNo_Number = "'.$_SESSION['EditInternalOrder'][7].'"'))
    	    {
//            ExecuteQuery('DELETE FROM Reminder WHERE Reminder_System_Reference = "IO" AND Reminder_System_Reference_ID = "'.$_SESSION['EditInternalOrder'][7].'"');
            
    	      $date = Date('Y-m-d H:i:s');
              
    	      if (SizeOf($_SESSION['EditInternalOrder']) > 23)
    	      {
              for ($count = 24; $count <= SizeOf($_SESSION['EditInternalOrder']); $count++)
              {
                if ($_SESSION['EditInternalOrder'][$count] != "")
                {
                  if ($_SESSION['EditInternalOrder'][$count][1] == "")
                    ExecuteQuery('DELETE FROM Items WHERE Items_ID = "'.$_SESSION['EditInternalOrder'][$count][6].'"');
                  else
                  {
                    $cost = $_SESSION['EditInternalOrder'][$count][2] * $loCurrencyRow['Currency_Rate'];
                    $total = $cost * $_SESSION['EditInternalOrder'][$count][3];
                    
                    if ($_SESSION['EditInternalOrder'][$count][6] == "")
                     ExecuteQuery('INSERT INTO Items VALUES("'.$_SESSION['EditInternalOrder'][7].'", "'.$_SESSION['EditInternalOrder'][$count][0].'", "'.$_SESSION['EditInternalOrder'][$count][3].'", "'.$_SESSION['EditInternalOrder'][$count][4].'", "", "'.$cost.'", "", "", "'.$_SESSION['EditInternalOrder'][$count][1].'", "", "", "", "", "'.$total.'", "'.$date.'", "'.$_SESSION['EditInternalOrder'][$count][5].'", "'.$_POST['CostCurrency'].'", "'.$_SESSION['EditInternalOrder'][$count][2].'", "'.($_SESSION['EditInternalOrder'][$count][2] * $_SESSION['EditInternalOrder'][$count][3]).'","'.$_SESSION['EditInternalOrder'][$count][7].'","","'.$_SESSION['EditInternalOrder'][$count][8].'")');
                    else
                    {
                      ExecuteQuery('UPDATE Items SET Items_Supplier_Code = "'.$_SESSION['EditInternalOrder'][$count][0].'", Items_Quantity = "'.$_SESSION['EditInternalOrder'][$count][3].'", Items_Backorder = "'.$_SESSION['EditInternalOrder'][$count][4].'", Items_Value = "'.$cost.'", Items_Description = "'.$_SESSION['EditInternalOrder'][$count][1].'", Items_Total = "'.$total.'", Items_Due_Date = "'.$date.'", Items_Asset = "'.$_SESSION['EditInternalOrder'][$count][5].'", Items_Original_Currency = "'.$_POST['CostCurrency'].'", Items_Original_Value = "'.$_SESSION['EditInternalOrder'][$count][2].'", Items_Original_Total = "'.($_SESSION['EditInternalOrder'][$count][2] * $_SESSION['EditInternalOrder'][$count][3]).'", Items_StorageType = "'.$_SESSION['EditInternalOrder'][$count][7].'", Items_Discount = "'.$_SESSION['EditInternalOrder'][$count][8].'" WHERE Items_ID = '.$_SESSION['EditInternalOrder'][$count][6].'');

                      ExecuteQuery('UPDATE SupplierInvoiceItems SET SupplierInvoiceItems_Value = "'.$cost.'",SupplierInvoiceItems_Quantity = "'.$_SESSION['EditInternalOrder'][$count][3].'",SupplierInvoiceItems_Total ="'.($_SESSION['EditInternalOrder'][$count][2] * $_SESSION['EditInternalOrder'][$count][3]).'" WHERE SupplierInvoiceItems_Items_ID = '.$_SESSION['EditInternalOrder'][$count][6].'');
                    }
                      
                    }
                }
              }
                }
         if ($approval == 0) {
                $row = MySQL_Fetch_Array(ExecuteQuery('SELECT OrderNo.OrderNo_ReportCategory, OrderNo.`quotation_number`, Staff_First_Name, Staff_Last_Name, Supplier_Code, Supplier_Name, Supplier_Phone, Project.*, OrderNo_Comments 
                        FROM Staff, Supplier, Project, OrderNo 
                        WHERE Staff_Code = OrderNo_Requested AND Supplier_Code = OrderNo_Supplier AND Project_Code = OrderNo_Project AND OrderNo_Number= "'.$_SESSION['EditInternalOrder'][7].'"'));
            
                $row2 = MySQL_Fetch_Array(ExecuteQuery('SELECT `Level` FROM BEE_Certificate WHERE Supplier_Code = '.$row['Supplier_Code']));
                $levelBEE = 'non-compliant';
                 if($row2['Level']!=null){
                       if($row2['Level'] == 9){
                       $levelBEE = "N/A - International";
                    }
                      else if($row2['Level'] == 10){
                           $levelBEE = "Awaiting feedback";
                    }
                    
                    else if($row2['Level'] == 0){
                       $levelBEE = "non-compliant";
                    }
                    
                    else
                        $levelBEE = $row2['Level'];
                }
                
                  $tdLevel = '<TD style="font-weight: bold; background-color: rgba(255, 0, 0, 0.32); padding-left: 0.5em; padding-right: 0.5em;" >non-compliant</TD>';
              if($row2['Level']!=null){
                   if($row2['Level'] == 9){
                        $tdLevel = '<TD style="font-weight: bold; background-color: rgba(10, 148, 23, 0.32); padding-left: 0.5em; padding-right: 0.5em; " >N/A - International</TD>';
                    }
                    else if($row2['Level'] == 10){
                         $tdLevel = '<TD style="font-weight: bold; background-color: rgba(10, 115, 254, 0.58); padding-left: 0.5em; padding-right: 0.5em; " >Awaiting feedback</TD>';
                    } 
                    
                    
                    else if($row2['Level'] == 0){
                        $tdLevel = '<TD style="font-weight: bold; background-color: rgba(255, 0, 0, 0.32); padding-left: 0.5em; padding-right: 0.5em; " >non-compliant</TD>';
                    }
                    
                    else if($row2['Level'] < 6){  // 1 2 3 4 5 
                        $tdLevel = '<TD style="font-weight: bold; background-color: rgba(10, 148, 23, 0.32); padding-left: 0.5em; padding-right: 0.5em; " >'.$row2['Level'].'</TD>';
                    }
                    
                    else {   // 6 7 8
                          $tdLevel = '<TD style="font-weight: bold; background-color: rgba(245, 253, 21, 0.58); padding-left: 0.5em; padding-right: 0.5em; " >'.$row2['Level'].'</TD>';
                    }
            }
                
                $qNumber2 = "";
                $qNumber = $row['quotation_number'];
                if($qNumber!= null || $qNumber!= '' ){

                     $qNumber = 'QUOTATION NUMBER:               '.$row['quotation_number'];
                     $qNumber2 = '<TR><TD><B>Quotation number:</B></TD><TD>'.$row['quotation_number'].'</TR>';
                }
              
               if( strcmp($row['OrderNo_ReportCategory'], "0") == 0){
                   $costManagerOnCategory = 'Project_CostManager'; 
               }
               else{
                  $costManagerOnCategory = 'Project_CostManager'.$row['OrderNo_ReportCategory']; 
               }
              $rowB = MySQL_Fetch_Array(ExecuteQuery('SELECT Staff_First_Name, Staff_Last_Name,Staff_email FROM Staff WHERE Staff_Code = '.$row['Project_Responsible']));
              $cmRow = MySQL_Fetch_Array(ExecuteQuery('SELECT Staff_First_Name, Staff_Last_Name,Staff_email FROM Staff WHERE Staff_Code = '.$row[$costManagerOnCategory]));
              $approver = $cmRow['Staff_email'];
              $ApproverName = $cmRow['Staff_First_Name'].' '.$cmRow['Staff_Last_Name'];
              if(!$approver)
                {
                    $approver = $rowB['Staff_email'];
                    $ApproverName = $rowB['Staff_First_Name'].' '.$rowB['Staff_Last_Name'];
                }
              
              $email = 'The following order requires approval:'.Chr(10).
                       'PENDING ORDER NO:       '.$_SESSION['EditInternalOrder'][7].Chr(10).
                       'DATE:                   '.GetTextualDateFromDatabaseDate($date).Chr(10).
                       'REQUESTED BY:           '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].Chr(10).
                       'PROJECT:                '.$row['Project_Pastel_Prefix'].' - '.$row['Project_Description'].Chr(10).
                       'SUPPLIER:               '.$row['Supplier_Name'].' - '.$row['Supplier_Phone'].Chr(10).
                       'BEE Level               '.$levelBEE.
                      $qNumber.
                       'COMMENTS:               '.$row['OrderNo_Comments'].Chr(10).
                       'FOLLOW UP DATE:         '.GetTextualDateFromDatabaseDate($_SESSION['EditInternalOrder'][16]).Chr(10).
                       'DELIVERY DATE:          '.GetTextualDateFromDatabaseDate($_SESSION['EditInternalOrder'][17]).Chr(10).
                       'TO BE APPROVED BY:      '.$ApproverName.Chr(10).
                       'TOTAL (EXCL. VAT):      '.$loCurrencySymbol.$_POST['Cost'].Chr(10).Chr(10);
              
              $html = 'The following order requires approval:
                      <BR /><BR />
                      <TABLE border=0>
                        <TR><TD><B>Pending Order No:</B></TD><TD>'.$_SESSION['EditInternalOrder'][7].'</TD></TR>
                        <TR><TD><B>Date:</B></TD><TD>'.GetTextualDateFromDatabaseDate($date).'</TD></TR>
                        <TR><TD><B>Requested By:</B></TD><TD>'.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].'</TD></TR>
                        <TR><TD><B>Project:</B></TD><TD>'.$row['Project_Pastel_Prefix'].' - '.$row['Project_Description'].'</TD></TR>
                        <TR><TD><B>Supplier:</B></TD><TD>'.$row['Supplier_Name'].' - '.$row['Supplier_Phone'].'</TD></TR>
                        <TR><TD><B>BEE Level:</B></TD>'.$tdLevel.'</TR>
                            '.$qNumber2.'
                        <TR><TD><B>Comments:</B></TD><TD>'.$row['OrderNo_Comments'].'</TD></TR>
                        <TR><TD><B>Follow Up Date:</B></TD><TD>'.GetTextualDateFromDatabaseDate($_SESSION['EditInternalOrder'][16]).'</TD></TR>
                        <TR><TD><B>Delivery Date:</B></TD><TD>'.GetTextualDateFromDatabaseDate($_SESSION['EditInternalOrder'][17]).'</TD></TR>
                        <TR><TD><B>To Be Approved By:</B></TD><TD>'.$ApproverName.'</TD></TR>
                        <TR><TD><B>Total (Excl. VAT):</B></TD><TD>'.$loCurrencySymbol.$_POST['Cost'].'</TD></TR>
                      </TABLE>
                      <BR /><BR />';
              
              $email .= 'QTY          '.Chr(9).Chr(9).'SUPPLIER CODE'.Chr(9).Chr(9).'DESCRIPTION  '.Chr(9).Chr(9).'VALUE        '.Chr(9).'NETT VALUE        '.Chr(9).'DISCOUNT        '.Chr(9).Chr(9).'TOTAL        '.Chr(10);
              $html .= '<TABLE border=1><TR><TD>QTY</TD><TD>SUPPLIER CODE</TD><TD>DESCRIPTION</TD><TD>VALUE</TD><TD>NETT VALUE</TD><TD>DISCOUNT</TD><TD>TOTAL</TD><TD>UNCONTROLLED GOODS QTY</TD><TD>MATCHED ON</TD></TR>';
              
              $resultSet = ExecuteQuery('SELECT * FROM Items WHERE Items_Order_Number = "'.$_SESSION['EditInternalOrder'][7].'"');
             $uncontrolledGoods = getUncontrolledGoodsList();
              while ($rowC = MySQL_Fetch_Array($resultSet))
              {
                $nett_value_mail = $rowC['Items_Original_Value'];
                $discount = 0;
                                if($rowC['Items_Discount'] > 0)
                                {
                                    $nett_value_mail -= (($rowC['Items_Discount']/100) * $nett_value_mail);
                                    $discount = ($rowC['Items_Discount']/100) * $rowC['Items_Original_Total'];
                                }
                  
                $email .= $rowC['Items_Quantity'].Chr(9).Chr(9).Chr(9).$rowC['Items_Supplier_Code'].Chr(9).Chr(9).Chr(9).$rowC['Items_Description'].Chr(9).Chr(9).Chr(9).Chr(9).Chr(9).$loCurrencySymbol.SPrintF('%02.2f', $rowC['Items_Original_Value']).Chr(9).Chr(9).$loCurrencySymbol.SPrintF('%02.2f', $nett_value_mail).Chr(9).Chr(9).$rowC['Items_Discount'].'%'.Chr(9).Chr(9).$loCurrencySymbol.SPrintF('%02.2f', ($rowC['Items_Original_Total'] - $discount)).Chr(10).Chr(10);
               
                $theQTYValue = isInUncontrolled($rowC['Items_Supplier_Code'], $rowC['Items_Description'], $uncontrolledGoods);
                
                if($theQTYValue == 0){
                  $html .= '<TR><TD>'.$rowC['Items_Quantity'].'</TD><TD>'.$rowC['Items_Supplier_Code'].'</TD><TD>'.$rowC['Items_Description'].'</TD><TD>'.$loCurrencySymbol.SPrintF('%02.2f', $rowC['Items_Original_Value']).'</TD><TD>'.$loCurrencySymbol.SPrintF('%02.2f', $nett_value_mail).'</TD><TD>'.$rowC['Items_Discount'].'%'.'</TD><TD>'.$loCurrencySymbol.SPrintF('%02.2f', ($rowC['Items_Original_Total'] - $discount)).'</TD><TD></TD><TD></TD></TR>';
                }
                else{
                  $html .= '<TR style="background-color: rgba(255, 122, 0, 0.61);"><TD>'.$rowC['Items_Quantity'].'</TD><TD>'.$rowC['Items_Supplier_Code'].'</TD><TD>'.$rowC['Items_Description'].'</TD><TD>'.$loCurrencySymbol.SPrintF('%02.2f', $rowC['Items_Original_Value']).'</TD><TD>'.$loCurrencySymbol.SPrintF('%02.2f', $nett_value_mail).'</TD><TD>'.$rowC['Items_Discount'].'%'.'</TD><TD>'.$loCurrencySymbol.SPrintF('%02.2f', ($rowC['Items_Original_Total'] - $discount)).'</TD><TD>'.$theQTYValue.'</TD><TD>'.matchedon($rowC['Items_Supplier_Code'], $rowC['Items_Description'], $uncontrolledGoods).'</TD></TR>';
                }
              }
              $email .= Chr(10).Chr(10).Chr(10).'___________________________________'.Chr(10).$rowB['Staff_First_Name'].' '.$rowB['Staff_Last_Name'];
              $html .= '</TABLE><BR /><BR /><BR /><BR />___________________________________<BR /><BR />'.$rowB['Staff_First_Name'].' '.$rowB['Staff_Last_Name'];
              
              $rand = Rand();
              while (StrLen($rand) != 5)
              {
                if (StrLen($rand) > 5)
                  $rand = SubStr($rand, 0, 5);
                else
                  $rand = Rand();
              }
              $email .= Chr(10).Chr(10).Chr(10).'Approve this request here '.getS4ExternalLink().'/intranew/InternalOrders.php?type=approve&id='.$rand.$_SESSION['EditInternalOrder'][7].' here.';
              $html .= '<BR /><BR /><BR />Approve this request <A href="'.getS4ExternalLink().'/intranew/InternalOrders.php?type=approve&id='.$rand.$_SESSION['EditInternalOrder'][7].'">here</A>.';
              
              SendMailHTML($approver, 'Updated Order Waiting Approval - S4 - '.$_SESSION['EditInternalOrder'][7], $email, $html);
//              SendMailHTML('cherie@s4integration.co.za', 'Updated Order Waiting Approval - S4 - '.$_SESSION['EditInternalOrder'][7], $email, $html);
//              SendMailHTML('andrew@s4integration.co.za', 'Updated Order Waiting Approval - S4 - '.$_SESSION['EditInternalOrder'][7], $email, $html);
//              SendMailHTML('arthur@s4integration.co.za', 'Updated Order Waiting Approval - S4 - '.$_SESSION['EditInternalOrder'][7], $email, $html);
//              SendMailHTML('johannes@s4integration.co.za', 'Updated Order Waiting Approval - S4 - '.$_SESSION['EditInternalOrder'][7], $email, $html);
//			  SendMailHTML('gideon@s4integration.co.za', 'Updated Order Waiting Approval - S4 - '.$_SESSION['EditInternalOrder'][7], $email, $html);
//			  SendMailHTML('malcolm@s4integration.co.za', 'Updated Order Waiting Approval - S4 - '.$_SESSION['EditInternalOrder'][7], $email, $html);
//			  SendMailHTML('stefan@s4integration.co.za', 'Updated Order Waiting Approval - S4 - '.$_SESSION['EditInternalOrder'][7], $email, $html);
			  
            }
            
            $_SESSION['InternalOrderSuccess'] = 'geh!';
            Session_Unregister('EditInternalOrder');
          } else
            $_SESSION['InternalOrderFail'] = 'geh!';
//          }
//          
//          else{   
//		if (ExecuteQuery('UPDATE OrderNo SET OrderNo_Delivery = "'.$_POST['Delivery'].'", OrderNo_Comments = "'.$_POST['Comments'].'", OrderNo_Ent = "'.$_SESSION['cUID'].'" , OrderNo_Date_Delivery = "'.$_SESSION['EditInternalOrder'][17].'", OrderNo_Date_FollowUp = "'.$_SESSION['EditInternalOrder'][16].'" WHERE OrderNo_Number = "'.$_SESSION['EditInternalOrder'][7].'"'))
//                {
//                   // ExecuteQuery('DELETE FROM Reminder WHERE Reminder_System_Reference = "IO" AND Reminder_System_Reference_ID = "'.$_SESSION['EditInternalOrder'][7].'"');
//                    $_SESSION['InternalOrderSuccess'] = 'geh!';
//                    Session_Unregister('EditInternalOrder');
//                } 
//                else
//                $_SESSION['InternalOrderFail'] = 'geh!';  
//          }
          
        } else
          $_SESSION['InternalOrderIncomplete'] = 'geh!';
        break;
      default: //Remove
        break;
    }
  }
  
  
  function needsReaproval(){

     // print_r("does this show: ".$_SESSION['EditInternalOrder'][7]." \n");
      
     // echo '    SELECT * FROM OrderNo WHERE OrderNo_Number = "'.$_SESSION['EditInternalOrder'][7].'"        ';
      
        $loRSC = ExecuteQuery('SELECT * FROM OrderNo WHERE OrderNo_Number = "'.$_SESSION['EditInternalOrder'][7].'"');
        $row = MySQL_Fetch_Array($loRSC);
      // echo'</br>';
      // echo'</br>';
      //  print_r($row);
      //    echo'</br>';
       //  echo'</br>';
        $old_OrderNo_Requested  = (int)$row['OrderNo_Requested'];
        $old_OrderNo_Supplier = (int)$row['OrderNo_Supplier'];
        $old_OrderNo_Project = (int)$row['OrderNo_Project'];
        $old_OrderNo_Type = (int)$row['OrderNo_Type'];
        $old_OrderNo_Original_Currency = (int)$row['OrderNo_Original_Currency'];
        $old_OrderNo_Original_Total_Cost = $row['OrderNo_Original_Total_Cost'];
        $old_OrderNo_Comments = $row['OrderNo_Comments'];
      
        $resultSet = ExecuteQuery('SELECT * FROM SupplierInvoice WHERE SupplierInvoice_S4OrderNo = "'.$_SESSION['EditInternalOrder'][7].'"');
        $old_invoices = "";
        while ($rowTemp = MySQL_Fetch_Array($resultSet))
        {
          $old_invoices .= $rowTemp['SupplierInvoice_Number'].'; ';
        }

        $old_OrderNo_Delivery = $row['OrderNo_Delivery'];
        $old_OrderNo_Date_FollowUp = $row['OrderNo_Date_FollowUp'];
        $old_OrderNo_Date_Delivery = $row['OrderNo_Date_Delivery'];
        $old_OrderNo_ReportCategory = $row['OrderNo_ReportCategory'];
        $old_quotation_number = $row['quotation_number'];
        
        
  if( 
           (int)$_POST['Requested'] != $old_OrderNo_Requested   
        || (int)$_POST['Supplier'] != $old_OrderNo_Supplier   
        || strcmp($_POST['quotation_number'],$old_quotation_number) != 0  
        || (int)$_POST['Project'] != $old_OrderNo_Project   
        || (int)$_POST['OrderType'] != $old_OrderNo_Type  
        || (int)$_POST['CostCurrency'] != $old_OrderNo_Original_Currency
        || (int)$_POST['ReportCategory'] != $old_OrderNo_ReportCategory
    ){
     //  echo'</br> re approve';  
        return true;
      } 

      else {
          // check items
          
        $items = array();     
        $resultSet = ExecuteQuery('SELECT * FROM Items WHERE Items_Order_Number = "'.$_SESSION['EditInternalOrder'][7].'"');
        while ($row = MySQL_Fetch_Array($resultSet))
        {
             $items[] = array($row['Items_Supplier_Code'], $row['Items_Description'], $row['Items_Original_Value'], $row['Items_Quantity'], $row['Items_Backorder'], $row['Items_Asset'], $row['Items_ID'],$row['Items_StorageType'],$row['Items_Discount']);
        }
        

        
        //  echo'</br> </br> ITEMS </br>' ;
          
          $changed = false;
          
          foreach($items as  $item){
              
             // print_r($item);
            //   echo'</br>';
               if(!in_array ($item, $_SESSION['EditInternalOrder'])){
                    $changed = true;
               }   
          }
          
          
          // check for new items
          if(!$changed){
          //  echo'</br> </br> NEW ITEMS </br>' ;
          
            foreach($_SESSION['EditInternalOrder'] as  $item){
                $index = array_search($item, $_SESSION['EditInternalOrder']);
                if($index > 23){
                //  print_r($item);
               //   echo'</br>';
                  
                  if(!in_array ($item, $items)){
                    $changed = true;
               }   
                  
                }

            }
          }
          return $changed;
      }
      
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Handles the user's maintenance selection.                                //
  //////////////////////////////////////////////////////////////////////////////
  function HandleMaintain()
  {
    switch ($_POST['Submit'])
    {
      case 'Add':
        {
          $_SESSION['AddInternalOrder'] = array();
          if ($_SESSION['cUID'] != 32)
            $_SESSION['AddInternalOrder'][0] = $_SESSION['cUID'];
          $_SESSION['AddInternalOrder'][3] = '1';
          $_SESSION['AddInternalOrder'][4] = '1';
          //$_SESSION['AddInternalOrder'][17] =0;
        }
        break;
      case 'Approve':
        {
          if ($_POST['InternalOrderApprove'] == ""){
            $_SESSION['InternalOrderIncomplete'] = 'geh!';
          }     
          else
          {
            $_SESSION['ApproveInternalOrder'] = array($_POST['InternalOrderApprove']);
            $_SESSION['ApproveInternalOrder'][1] = $_SESSION['cUID'];
            $_SESSION['ApproveInternalOrder'][3] = 2;
          }
        }
        break;
      case 'Edit':
        {
          /*if ($_POST['InternalOrderEdit'] == "")*/
          if (!CheckNumeric($_POST['InternalOrderEdit']) || (StrLen($_POST['InternalOrderEdit']) != 7))
            $_SESSION['InternalOrderIncomplete'] = 'geh!';
          else
            {
              $loRS = ExecuteQuery('SELECT * FROM OrderNo WHERE OrderNo_Number = "'.$_POST['InternalOrderEdit'].'"');
              if (MySQL_Num_Rows($loRS) == 0)
                $_SESSION['InternalOrderIncomplete'] = 'geh!';
              else
              {
                $row = MySQL_Fetch_Array($loRS);
                $_SESSION['EditInternalOrder'] = array();
                $_SESSION['EditInternalOrder'][0] = $row['OrderNo_Requested'];
                $_SESSION['EditInternalOrder'][1] = $row['OrderNo_Supplier'];
                $_SESSION['EditInternalOrder'][2] = $row['OrderNo_Project'];
                $_SESSION['EditInternalOrder'][3] = $row['OrderNo_Type'];
                $_SESSION['EditInternalOrder'][4] = $row['OrderNo_Original_Currency'];
                $_SESSION['EditInternalOrder'][5] = $row['OrderNo_Original_Total_Cost'];
                $_SESSION['EditInternalOrder'][6] = $row['OrderNo_Comments'];
                $_SESSION['EditInternalOrder'][7] = $_POST['InternalOrderEdit'];
                
                
                $resultSet = ExecuteQuery('SELECT * FROM SupplierInvoice WHERE SupplierInvoice_S4OrderNo = "'.$_POST['InternalOrderEdit'].'"');
                $invoices = "";
                while ($rowTemp = MySQL_Fetch_Array($resultSet))
                {
                  $invoices .= $rowTemp['SupplierInvoice_Number'].'; ';
                }
                $_SESSION['EditInternalOrder'][8] = $invoices;
                
                $_SESSION['EditInternalOrder'][9] = 'geh!';
                $_SESSION['EditInternalOrder'][10] = $row['OrderNo_Delivery'];
                $_SESSION['EditInternalOrder'][11] = "";
                $_SESSION['EditInternalOrder'][12] = "";
                $_SESSION['EditInternalOrder'][13] = "";
                $_SESSION['EditInternalOrder'][14] = "";
                $_SESSION['EditInternalOrder'][15] = "";
                $_SESSION['EditInternalOrder'][16] = $row['OrderNo_Date_FollowUp'];
                $_SESSION['EditInternalOrder'][17] = $row['OrderNo_Date_Delivery'];
                $_SESSION['EditInternalOrder'][18] = "";
                $_SESSION['EditInternalOrder'][20] = "";
                $_SESSION['EditInternalOrder'][21] = "";
                $_SESSION['EditInternalOrder'][22] = $row['OrderNo_ReportCategory'];
                $_SESSION['EditInternalOrder'][23] = $row['quotation_number'];
                
                $resultSet = ExecuteQuery('SELECT * FROM Items WHERE Items_Order_Number = "'.$_POST['InternalOrderEdit'].'"');
                while ($row = MySQL_Fetch_Array($resultSet))
                {
                  $_SESSION['EditInternalOrder'][] = array($row['Items_Supplier_Code'], $row['Items_Description'], $row['Items_Original_Value'], $row['Items_Quantity'], $row['Items_Backorder'], $row['Items_Asset'], $row['Items_ID'],$row['Items_StorageType'],$row['Items_Discount']);
                }
                
              }
            }
        }
        break;
      case 'Mark':
        {
          /*if ($_POST['InternalOrderMark'] == "")*/
          if (!CheckNumeric($_POST['InternalOrderMark']) || (StrLen($_POST['InternalOrderMark']) != 7))
            $_SESSION['InternalOrderIncomplete'] = 'geh!';
          else
          {
            $loRS = ExecuteQuery('SELECT * FROM OrderNo WHERE OrderNo_Number = "'.$_POST['InternalOrderMark'].'"');
            if (MySQL_Num_Rows($loRS) == 0)
              $_SESSION['InternalOrderIncomplete'] = 'geh!';
            else
            {
              $row = MySQL_Fetch_Array($loRS);
              $_SESSION['MarkInternalOrder'] = array($_POST['InternalOrderMark']);
              $_SESSION['MarkInternalOrder'][1] = $row['OrderNo_Complete'];
            }
          }
        }
        break;
      default:
        break;
    }
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Handles the user's submission of a change to internal order status.      //
  //////////////////////////////////////////////////////////////////////////////
  function HandleMark() //TODO.
  {
    $_SESSION['MarkInternalOrder'][1] = $_POST['Status'];
  
    switch ($_POST['Submit'])
    {
      case 'Cancel':
        Session_Unregister('MarkInternalOrder');
        break;
      case 'Submit':
        if (CheckFields())
        {
          $row = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM OrderNo WHERE OrderNo_Number = "'.$_SESSION['MarkInternalOrder'][0].'"'));
          $loCurrencyRow = MySQL_Fetch_Array(ExecuteQuery('SELECT C.Currency_Rate, C.Currency_Symbol FROM Currency C WHERE Currency_ID = "'.$_POST['CostCurrency'].'"'));
          $loCurrencySymbol = $loCurrencyRow['Currency_Symbol'];
          
          $success = true;
          if ($row['OrderNo_Complete'] != $_POST['Status'])
          {
            switch ($_POST['Status'])
            {
              case '0':
                $success = ExecuteQuery('UPDATE OrderNo SET OrderNo_Complete = "0", OrderNo_Comments = CONCAT(OrderNo_Comments, " ", "This order was marked as incomplete on '.Date('d M Y').'") WHERE OrderNo_Number = "'.$_SESSION['MarkInternalOrder'][0].'"');
                break;
              case '1':
                $success = ExecuteQuery('UPDATE OrderNo SET OrderNo_Complete = "1", OrderNo_Comments = CONCAT(OrderNo_Comments, " ", "This order was marked as complete on '.Date('d M Y').'") WHERE OrderNo_Number = "'.$_SESSION['MarkInternalOrder'][0].'"');
                break;
              case '2':
                $success = ExecuteQuery('UPDATE OrderNo SET OrderNo_Complete = "2", OrderNo_Comments = CONCAT(OrderNo_Comments, " ", "This order was cancelled on '.Date('d M Y').'") WHERE OrderNo_Number = "'.$_SESSION['MarkInternalOrder'][0].'"');
                break;
              case '3':
                $success = ExecuteQuery('UPDATE OrderNo SET OrderNo_Complete = "3", OrderNo_Comments = CONCAT(OrderNo_Comments, " ", "This order was followed up.") WHERE OrderNo_Number = "'.$_SESSION['MarkInternalOrder'][0].'"');
                break;
              default:
                break;
            }
          }
          if ($success)
          {
               $row2 = MySQL_Fetch_Array(ExecuteQuery('SELECT `Level` FROM BEE_Certificate WHERE Supplier_Code = '.$row['Supplier_Code']));
                $levelBEE = 'non-compliant';
                 if($row2['Level']!=null){
                      if($row2['Level'] == 9){
                       $levelBEE = "N/A - International";
                    }
                    
                     else if($row2['Level'] == 10){
                           $levelBEE = "Awaiting feedback";
                    }
                    
                    else if($row2['Level'] == 0){
                       $levelBEE = "non-compliant";
                    }
                    
                    else
                        $levelBEE = $row2['Level'];
                }
                
                  $tdLevel = '<TD style="font-weight: bold; background-color: rgba(255, 0, 0, 0.32); padding-left: 0.5em; padding-right: 0.5em;" >non-compliant</TD>';
              if($row2['Level']!=null){
                   if($row2['Level'] == 9){
                        $tdLevel = '<TD style="font-weight: bold; background-color: rgba(10, 148, 23, 0.32); padding-left: 0.5em; padding-right: 0.5em; " >N/A - International</TD>';
                    }
                    else if($row2['Level'] == 10){
                         $tdLevel = '<TD style="font-weight: bold; background-color: rgba(10, 115, 254, 0.58); padding-left: 0.5em; padding-right: 0.5em; " >Awaiting feedback</TD>';
                    } 
                    else if($row2['Level'] == 0){
                        $tdLevel = '<TD style="font-weight: bold; background-color: rgba(255, 0, 0, 0.32); padding-left: 0.5em; padding-right: 0.5em; " >non-compliant</TD>';
                    }
                    
              else if($row2['Level'] < 6) { // 1 2 3 4 5 
                        $tdLevel = '<TD style="font-weight: bold; background-color: rgba(10, 148, 23, 0.32); padding-left: 0.5em; padding-right: 0.5em; " >'.$row2['Level'].'</TD>';
                    }
                    
                    else {   // 6 7 8
                          $tdLevel = '<TD style="font-weight: bold; background-color: rgba(245, 253, 21, 0.58); padding-left: 0.5em; padding-right: 0.5em;" >'.$row2['Level'].'</TD>';
                    }
            }
                $qNumber2 = "";
                $qNumber = $row['quotation_number'];
                if($qNumber!= null || $qNumber!= '' ){

                     $qNumber = 'QUOTATION NUMBER:               '.$row['quotation_number'];
                     $qNumber2 = '<TR><TD><B>Quotation number:</B></TD><TD>'.$row['quotation_number'].'</TR>';
                }
              
              
//            ExecuteQuery('DELETE FROM Reminder WHERE Reminder_System_Reference = "IO" AND Reminder_System_Reference_ID = "'.$_SESSION['MarkInternalOrder'][0].'"');
            if ($_POST['Status'] == '3')
            {
              $row = MySQL_Fetch_Array(ExecuteQuery('SELECT Staff_First_Name, Staff_Last_Name, Staff_email, Supplier_Name, Supplier_Phone, Project_Description, Project_Pastel_Prefix, Project_Responsible, OrderNo.* FROM Staff, Supplier, Project, OrderNo WHERE Staff_Code = OrderNo_Requested AND Supplier_Code = OrderNo_Supplier AND Project_Code = OrderNo_Project AND OrderNo_Number= "'.$_SESSION['MarkInternalOrder'][0].'"'));
                     
              if ($row['OrderNo_Approved'])
              {
                $rowB = MySQL_Fetch_Array(ExecuteQuery('SELECT Staff_First_Name, Staff_Last_Name FROM Staff WHERE Staff_Code = '.$_POST['Approved']));
                $emailReminder = 'ORDER NO:               '.$row['OrderNo_Number'].Chr(10).
                         'DATE:                   '.GetTextualDateFromDatabaseDate($row['OrderNo_Date_Time']).Chr(10).
                         'PROJECT:                '.$row['Project_Pastel_Prefix'].' - '.$row['Project_Description'].Chr(10).
                         'SUPPLIER:               '.$row['Supplier_Name'].' - '.$row['Supplier_Phone'].Chr(10).
                         'BEE Level:              '.$levelBEE.Chr(10).  
                          $qNumber.
                         'COMMENTS:               '.$row['OrderNo_Date_FollowUp'].Chr(10).
                         'FOLLOW UP DATE:         '.$row['OrderNo_Comments'].Chr(10).
                         'DELIVERY DATE:          '.$row['OrderNo_Date_Delivery'].Chr(10).
                         'APPROVED BY:            '.$rowB['Staff_First_Name'].' '.$rowB['Staff_Last_Name'].
                         'TOTAL (EXCL. VAT):      '.$loCurrencySymbol.$row['OrderNo_Original_Total_Cost'].Chr(10).Chr(10);
                
                $htmlReminder = '<BR /><BR />
                        <TABLE border=0>
                          <TR><TD><B>Order No:</B></TD><TD>'.$row['OrderNo_Number'].'</TD></TR>
                          <TR><TD><B>Date:</B></TD><TD>'.GetTextualDateFromDatabaseDate($row['OrderNo_Date_Time']).'</TD></TR>
                          <TR><TD><B>Project:</B></TD><TD>'.$row['Project_Pastel_Prefix'].' - '.$row['Project_Description'].'</TD></TR>
                          <TR><TD><B>Supplier:</B></TD><TD>'.$row['Supplier_Name'].' - '.$row['Supplier_Phone'].'</TD></TR>
                          <TR><TD><B>BEE Level:</B></TD>'.$tdLevel.'</TR>   
                              '.$qNumber2.'
                          <TR><TD><B>Comments:</B></TD><TD>'.$row['OrderNo_Comments'].'</TD></TR>
                          <TR><TD><B>Follow Up Date:</B></TD><TD>'.GetTextualDateFromDatabaseDate($row['OrderNo_Date_FollowUp']).'</TD></TR>
                          <TR><TD><B>Delivery Date:</B></TD><TD>'.GetTextualDateFromDatabaseDate($row['OrderNo_Date_Delivery']).'</TD></TR>
                          <TR><TD><B>Approved By:</B></TD><TD>'.$rowB['Staff_First_Name'].' '.$rowB['Staff_Last_Name'].'</TD></TR>
                          <TR><TD><B>Total (Excl. VAT):</B></TD><TD>'.$loCurrencySymbol.$row['OrderNo_Original_Total_Cost'].'</TD></TR>
                        </TABLE>
                        <BR /><BR />';
                
                $emailReminder .= 'QTY          '.Chr(9).'SUPPLIER CODE'.Chr(9).'DESCRIPTION  '.Chr(9).Chr(9).'VALUE        '.Chr(9).'NETT VALUE        '.Chr(9).'DISCOUNT        '.Chr(9).'TOTAL        '.Chr(10);
                $htmlReminder .= '<TABLE border=1><TR><TD>QTY</TD><TD>SUPPLIER CODE</TD><TD>DESCRIPTION</TD><TD>VALUE</TD><TD>NETT VALUE</TD><TD>DISCOUNT</TD><TD>TOTAL</TD></TR>';
                
                $resultSet = ExecuteQuery('SELECT * FROM Items WHERE Items_Order_Number = "'.$_SESSION['MarkInternalOrder'][0].'"');
                while ($rowC = MySQL_Fetch_Array($resultSet))
                {
                  $nett_value_mail = $rowC['Items_Original_Value'];
                  $discount = 0;
                                if($rowC['Items_Discount'] > 0)
                                {
                                    $nett_value_mail -= (($rowC['Items_Discount']/100) * $nett_value_mail);
                                    $discount = ($rowC['Items_Discount']/100) * $rowC['Items_Original_Total'];
                                }
                  $emailReminder .= $rowC['Items_Quantity'].Chr(9).Chr(9).Chr(9).$rowC['Items_Supplier_Code'].Chr(9).Chr(9).Chr(9).$rowC['Items_Description'].Chr(9).Chr(9).Chr(9).Chr(9).Chr(9).$loCurrencySymbol.SPrintF('%02.2f', $rowC['Items_Original_Value']).Chr(9).Chr(9).$loCurrencySymbol.SPrintF('%02.2f', $nett_value_mail).Chr(9).Chr(9).$rowC['Items_Discount'].'%'.Chr(9).Chr(9).$loCurrencySymbol.SPrintF('%02.2f', ($rowC['Items_Original_Total'] - $discount)).Chr(10).Chr(10);
                  $htmlReminder .= '<TR><TD>'.$rowC['Items_Quantity'].'</TD><TD>'.$rowC['Items_Supplier_Code'].'</TD><TD>'.$rowC['Items_Description'].'</TD><TD>'.$loCurrencySymbol.SPrintF('%02.2f', $rowC['Items_Original_Value']).'</TD><TD>'.$loCurrencySymbol.SPrintF('%02.2f', $nett_value_mail).'</TD><TD>'.$rowC['Items_Discount'].'%'.'</TD><TD>'.$loCurrencySymbol.SPrintF('%02.2f', ($rowC['Items_Original_Total'] - $discount)).'</TD></TR>';
                }
                
//                $today = Date('Y-m-d');
//                if (!DatabaseDateLater($row['OrderNo_Date_Delivery'], $today) && ($row['OrderNo_Date_Delivery'] != $today))
                //  ExecuteQuery('INSERT INTO Reminder VALUES("", "'.$_SESSION['cUID'].'", "'.Date('Y-m-d H:i:s').'", "'.$row['OrderNo_Requested'].'", "'.GetReminderDate($row['OrderNo_Date_Delivery']).'", "4", "5", "32", "Order <B>'.$row['OrderNo_Number'].'</B> due for delivery on '.GetTextualDateFromDatabaseDate($row['OrderNo_Date_Delivery']).'. '.$row['Supplier_Name'].' for '.$row['Project_Description'].'.", "IO", "'.$row['OrderNo_Number'].'", "", "")');
                //else
//                  ExecuteQuery('INSERT INTO Reminder VALUES("", "'.$_SESSION['cUID'].'", "'.Date('Y-m-d H:i:s').'", "'.$row['OrderNo_Requested'].'", "'.GetReminderDate($row['OrderNo_Date_Delivery']).'", "4", "5", "32", "Order <B>'.$row['OrderNo_Number'].'</B> due for delivery on '.GetTextualDateFromDatabaseDate($row['OrderNo_Date_Delivery']).'. '.$row['Supplier_Name'].' for '.$row['Project_Description'].'.", "IO", "'.$row['OrderNo_Number'].'", "Reminder for the delivery of the following order: '.$emailReminder.'", "Reminder for the delivery of the following order: '.$htmlReminder.'")');
                
              }
            } else
            if ($_POST['Status'] == '0')
            {
              if ($row['OrderNo_Approved'])
              {
                $rowB = MySQL_Fetch_Array(ExecuteQuery('SELECT Staff_First_Name, Staff_Last_Name FROM Staff WHERE Staff_Code = '.$_POST['Approved']));
                
                $emailReminder = Chr(10).'ORDER NO:               '.$row['OrderNo_Number'].Chr(10).
                         'DATE:                   '.GetTextualDateFromDatabaseDate($row['OrderNo_Date_Time']).Chr(10).
                         'PROJECT:                '.$row['Project_Pastel_Prefix'].' - '.$row['Project_Description'].Chr(10).
                         'SUPPLIER:               '.$row['Supplier_Name'].' - '.$row['Supplier_Phone'].Chr(10).
                         'BEE Level:              '.$levelBEE.Chr(10).  
                          $qNumber.
                         'COMMENTS:               '.$row['OrderNo_Date_FollowUp'].Chr(10).
                         'FOLLOW UP DATE:         '.$row['OrderNo_Comments'].Chr(10).
                         'DELIVERY DATE:          '.$row['OrderNo_Date_Delivery'].Chr(10).
                         'APPROVED BY:            '.$rowB['Staff_First_Name'].' '.$rowB['Staff_Last_Name'].
                         'TOTAL (EXCL. VAT):      '.$loCurrencySymbol.$row['OrderNo_Original_Total_Cost'].Chr(10).Chr(10);
                
                $htmlReminder = '<BR /><BR />
                        <TABLE border=0>
                          <TR><TD><B>Order No:</B></TD><TD>'.$row['OrderNo_Number'].'</TD></TR>
                          <TR><TD><B>Date:</B></TD><TD>'.GetTextualDateFromDatabaseDate($row['OrderNo_Date_Time']).'</TD></TR>
                          <TR><TD><B>Project:</B></TD><TD>'.$row['Project_Pastel_Prefix'].' - '.$row['Project_Description'].'</TD></TR>
                          <TR><TD><B>Supplier:</B></TD><TD>'.$row['Supplier_Name'].' - '.$row['Supplier_Phone'].'</TD></TR>
                         <TR><TD><B>BEE Level:</B></TD>'.$tdLevel.'</TR>   
                         '.$qNumber2.'
                          <TR><TD><B>Comments:</B></TD><TD>'.$row['OrderNo_Comments'].'</TD></TR>
                          <TR><TD><B>Follow Up Date:</B></TD><TD>'.GetTextualDateFromDatabaseDate($row['OrderNo_Date_FollowUp']).'</TD></TR>
                          <TR><TD><B>Delivery Date:</B></TD><TD>'.GetTextualDateFromDatabaseDate($row['OrderNo_Date_Delivery']).'</TD></TR>
                          <TR><TD><B>Approved By:</B></TD><TD>'.$rowB['Staff_First_Name'].' '.$rowB['Staff_Last_Name'].'</TD></TR>
                          <TR><TD><B>Total (Excl. VAT):</B></TD><TD>'.$loCurrencySymbol.$row['OrderNo_Original_Total_Cost'].'</TD></TR>
                        </TABLE>
                        <BR /><BR />';
                
                $emailReminder .= 'QTY          '.Chr(9).'SUPPLIER CODE'.Chr(9).'DESCRIPTION  '.Chr(9).Chr(9).'VALUE        '.Chr(9).'NETT VALUE        '.Chr(9).'DISCOUNT        '.Chr(9).'TOTAL        '.Chr(10);
                $htmlReminder .= '<TABLE border=1><TR><TD>QTY</TD><TD>SUPPLIER CODE</TD><TD>DESCRIPTION</TD><TD>VALUE</TD><TD>NETT VALUE</TD><TD>DISCOUNT</TD><TD>TOTAL</TD></TR>';
                
                $resultSet = ExecuteQuery('SELECT * FROM Items WHERE Items_Order_Number = "'.$_SESSION['MarkInternalOrder'][0].'"');
                while ($rowC = MySQL_Fetch_Array($resultSet))
                {
                  $nett_value_mail = $rowC['Items_Original_Value'];
                  $discount = 0;
                                if($rowC['Items_Discount'] > 0)
                                {
                                    $nett_value_mail -= (($rowC['Items_Discount']/100) * $nett_value_mail);
                                    $discount = ($rowC['Items_Discount']/100) * $rowC['Items_Original_Total'];
                                }
                                
                  $emailReminder .= $rowC['Items_Quantity'].Chr(9).Chr(9).Chr(9).$rowC['Items_Supplier_Code'].Chr(9).Chr(9).Chr(9).$rowC['Items_Description'].Chr(9).Chr(9).Chr(9).Chr(9).Chr(9).$loCurrencySymbol.SPrintF('%02.2f', $rowC['Items_Original_Value']).Chr(9).Chr(9).$loCurrencySymbol.SPrintF('%02.2f', $nett_value_mail).Chr(9).Chr(9).$rowC['Items_Discount'].'%'.Chr(9).Chr(9).$loCurrencySymbol.SPrintF('%02.2f', ($rowC['Items_Original_Total'] - $discount)).Chr(10).Chr(10);
                  $htmlReminder .= '<TR><TD>'.$rowC['Items_Quantity'].'</TD><TD>'.$rowC['Items_Supplier_Code'].'</TD><TD>'.$rowC['Items_Description'].'</TD><TD>'.$loCurrencySymbol.SPrintF('%02.2f', $rowC['Items_Original_Value']).'</TD><TD>'.$loCurrencySymbol.SPrintF('%02.2f', $nett_value_mail).'</TD><TD>'.$rowC['Items_Discount'].'%'.'</TD><TD>'.$loCurrencySymbol.SPrintF('%02.2f', ($rowC['Items_Original_Total'] - $discount)).'</TD></TR>';
                }
                
               // $today = Date('Y-m-d');
                //if (!DatabaseDateLater($row['OrderNo_Date_Delivery'], $today) && ($row['OrderNo_Date_Delivery'] != $today))
                //  ExecuteQuery('INSERT INTO Reminder VALUES("", "'.$_SESSION['cUID'].'", "'.Date('Y-m-d H:i:s').'", "'.$row['OrderNo_Requested'].'", "'.GetReminderDate($row['OrderNo_Date_Delivery']).'", "4", "5", "32", "Order <B>'.$row['OrderNo_Number'].'</B> due for delivery on '.GetTextualDateFromDatabaseDate($row['OrderNo_Date_Delivery']).'. '.$row['Supplier_Name'].' for '.$row['Project_Description'].'.", "IO", "'.$row['OrderNo_Number'].'", "", "")');
                //else
                 // ExecuteQuery('INSERT INTO Reminder VALUES("", "'.$_SESSION['cUID'].'", "'.Date('Y-m-d H:i:s').'", "'.$row['OrderNo_Requested'].'", "'.GetReminderDate($row['OrderNo_Date_Delivery']).'", "4", "5", "32", "Order <B>'.$row['OrderNo_Number'].'</B> due for delivery on '.GetTextualDateFromDatabaseDate($row['OrderNo_Date_Delivery']).'. '.$row['Supplier_Name'].' for '.$row['Project_Description'].'.", "IO", "'.$row['OrderNo_Number'].'", "Reminder for the delivery of the following order: '.$emailReminder.'", "Reminder for the delivery of the following order: '.$htmlReminder.'")');
                //if (!DatabaseDateLater($row['OrderNo_Date_FollowUp'], $today) && ($row['OrderNo_Date_FollowUp'] != $today))
                //  ExecuteQuery('INSERT INTO Reminder VALUES("", "'.$_SESSION['cUID'].'", "'.Date('Y-m-d H:i:s').'", "'.$row['OrderNo_Requested'].'", "'.GetReminderDate($row['OrderNo_Date_FollowUp']).'", "4", "5", "32", "Order <B>'.$row['OrderNo_Number'].'</B> due for follow up on '.GetTextualDateFromDatabaseDate($row['OrderNo_Date_FollowUp']).'. '.$row['Supplier_Name'].' for '.$row['Project_Description'].'.", "IO", "'.$row['OrderNo_Number'].'", "", "")');
                //else
                //  ExecuteQuery('INSERT INTO Reminder VALUES("", "'.$_SESSION['cUID'].'", "'.Date('Y-m-d H:i:s').'", "'.$row['OrderNo_Requested'].'", "'.GetReminderDate($row['OrderNo_Date_FollowUp']).'", "4", "5", "32", "Order <B>'.$row['OrderNo_Number'].'</B> due for follow up on '.GetTextualDateFromDatabaseDate($row['OrderNo_Date_FollowUp']).'. '.$row['Supplier_Name'].' for '.$row['Project_Description'].'.", "IO", "'.$row['OrderNo_Number'].'", "Reminder for the follow up of the following order: '.$emailReminder.'", "Reminder for the follow up of the following order: '.$htmlReminder.'")');
              }
            }
            
            if ($_SESSION['cUID'] != '32')
            {
              $row = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = '.$_SESSION['cUID'].''));
              
              $email = 'Internal Order status has been updated. The details are as follows:'.Chr(10).
                       'ORDER NUMBER:          '.$_SESSION['MarkInternalOrder'][0].Chr(10).
                       'UPDATED BY:            '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].Chr(10).Chr(10).
                       'Please review the updated details to ensure that everything is in order.';
              $html = 'Internal Order status has been updated. The details are as follows:
                      <BR /><BR />
                      <TABLE border=0>
                        <TR><TD><B>Order Number:</B></TD><TD>'.$_SESSION['MarkInternalOrder'][0].'</TD></TR>
                        <TR><TD><B>Updated By:</B></TD><TD>'.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].'</TD></TR>
                      </TABLE>
                      <BR />
                      Please review the updated details to ensure that everything is in order.
                      <BR /><BR />';
              
           //   SendMailHTML('cherie@s4integration.co.za', 'Internal Order Status Updated - S4 - '.$_SESSION['MarkInternalOrder'][0], $email, $html);
              SendMailHTML('office.admin@s4.co.za', 'Internal Order Status Updated - S4 - '.$_SESSION['MarkInternalOrder'][0], $email, $html);
              SendMailHTML('amelia@s4integration.co.za', 'Internal Order Status Updated - S4 - '.$_SESSION['MarkInternalOrder'][0], $email, $html);
            }
            $_SESSION['InternalOrderSuccess'] = 'geh!';
            Session_Unregister('MarkInternalOrder');
          } else
            $_SESSION['InternalOrderFail'] = 'geh!';
        } else
          $_SESSION['InternalOrderIncomplete'] = 'geh!';
        break;
      default:
        break;
    }
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Handles the user's submission of an internal order selection.            //
  //////////////////////////////////////////////////////////////////////////////
  function HandleView()
  {
    if (CheckFields())
    {
      switch ($_POST['Type'])
      { 
        case 'Back':
            exportInternalOrders();
          break;
        case 'ViewExceeded':
          $_SESSION['ViewInternalOrderExceeded'] = array();
          break;
        case 'ViewList':
          $_SESSION['ViewInternalOrderList'] = array($_POST['StartYear'].$_POST['StartMonth'].$_POST['StartDay']);
          $_SESSION['ViewInternalOrderList'][1] = $_POST['EndYear'].$_POST['EndMonth'].$_POST['EndDay'];
          $_SESSION['ViewInternalOrderList'][2] = $_POST['Status'];
          $_SESSION['ViewInternalOrderList'][3] = $_POST['Project'];
          $_SESSION['ViewInternalOrderList'][4] = $_POST['Supplier'];
          $_SESSION['ViewInternalOrderList'][5] = $_POST['Deficit'];
          break;
        case 'ViewOutstanding':
          $_SESSION['ViewInternalOrderOutstanding'] = array();
          break;
        case 'ViewSingle':
          $_SESSION['ViewInternalOrderSingle'] = array($_POST['InternalOrder']);
          break;
        default:
          break;
      }
    } else
      $_SESSION['InternalOrderIncomplete'] = 'geh!';
  }
  
  function filterData(&$str)
  {
            $str = preg_replace("/\t/", "\\t", $str);
            $str = preg_replace("/\r?\n/", "\\n", $str);
            if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
  }
 
  function ViewSingle(){
      unsset($_SESSION['ViewInternalOrderSingle']);
  }
  
   function HandleMarkMultiple(){
     $_SESSION['MarkInternalOrderMultipleList'][0] = $_POST['StatusMark'];
     $_SESSION['MarkInternalOrderMultipleList'][1] = $_POST['SupplierMark'];
     $_SESSION['MarkInternalOrderMultipleList'][2] = $_POST['start_dateMark'];
     $_SESSION['MarkInternalOrderMultipleList'][3] = $_POST['end_dateMark']; 
     $_SESSION['MarkInternalOrderMultipleList'][4] = $_POST['getPOSComplete']; 
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Ajax methods                                                             //
  //////////////////////////////////////////////////////////////////////////////
  function getSuplierCode(){
    $resultSet = ExecuteQuery('SELECT `Level` FROM BEE_Certificate WHERE Supplier_Code = '.$_GET['supplier_code']);
    $level ='non-compliant';  
    while($row = MySQL_Fetch_Array($resultSet)){
          $level = $row['Level'];
      }
     if($level == 9){
          $level = "N/A - International";
      }
      
     else if ($level ==0){
          $level = 'non-compliant';
      }
      
      else if ($level == 10){
          $level = 'Awaiting feedback';
      }

     $yearago = Date('Y-m-d H:i:s', MkTime(Date("H"), Date("i"), Date("s"), Date("m")-12, Date("d"), Date("Y"))); 
     $resultSet = ExecuteQuery('SELECT DISTINCT Items_Supplier_Code,Items_Description FROM Items LEFT JOIN OrderNo ON Items_Order_Number = OrderNo.OrderNo_Number WHERE Items_Due_Date > "'.$yearago.'" AND  Items_Supplier_Code != "" AND Items_Supplier_Code IS NOT NULL AND Items_Description IS NOT NULL AND OrderNo_Supplier ='.$_GET['supplier_code'].' ORDER BY Items_Supplier_Code'); 
     $suppliers = array();
     while ($row = MySQL_Fetch_Array($resultSet)) {
         array_push($suppliers,$row);
     }   
     
     
     $resultSet = ExecuteQuery('SELECT Supplier_IsDC FROM Supplier WHERE Supplier_Code = '.$_GET['supplier_code']);  
     $row = MySQL_Fetch_Array($resultSet);
     
     $response = array($level, $suppliers, $yearago, $row['Supplier_IsDC']);
     echo json_encode($response);
  }

  function crossCheckProjectItems()
  {
      $ErrorItems = "";
      $style = 'style ="border: 1px solid #dddada;"';
      if (SizeOf($_SESSION['AddInternalOrder']) > 20){
          for ($count = 20; $count < SizeOf($_SESSION['AddInternalOrder']); $count++)
          {
              $itemSupplierCode = $_SESSION['EditInternalOrder'][$count][0];
              $description = $_SESSION['AddInternalOrder'][$count][1];

              $ErrorItems .= '<tr><td '.$style.'>'.$itemSupplierCode.'</td><td '.$style.'>'.$description.'</td><td '.$style.'> '.$_SESSION['AddInternalOrder'][$count][3].' </td><td '.$style.'>'.$itemSupplierCode.'</td></tr>';

          }

      }
  }

  function ItemCheck($itemSupplierCode, $description,$OrderNo_Project){
    $parameter = $itemSupplierCode." ".$description." ".$OrderNo_Project;
    $arrayR = array();
    /*
     * Added this function which will be called for each item added, this function checks if the items that are being added match any of the ones in the database(for the selected project)
     Checking Order:

    1. Full Part Number

    if no match:

    2. Full Description

    if still no match:

    3. Partial Part Number (regular expression) for first 5 characters*/

    if($itemSupplierCode && $description ){
        $row =ExecuteQuery("SELECT * FROM S4Admin.Items,OrderNo,Supplier
                                    where Supplier.Supplier_Code = OrderNo.OrderNo_Supplier 
                                            AND Supplier_IsDC = 0 
                                            AND OrderNo.OrderNo_Number  = Items.Items_Order_Number 
                                            AND  (Items_Supplier_Code =  '$itemSupplierCode' or Items_Description = '$description' or  LEFT(Items_Supplier_Code,5) = LEFT('$itemSupplierCode',5)) 
                                            AND OrderNo_Project = $OrderNo_Project");
    }else if($description){
        $row =ExecuteQuery("SELECT * FROM S4Admin.Items,OrderNo,Supplier
        where Supplier.Supplier_Code = OrderNo.OrderNo_Supplier AND Supplier_IsDC = 0 AND OrderNo.OrderNo_Number  = Items.Items_Order_Number AND  Items_Description = '$description'
        AND OrderNo_Project = $OrderNo_Project");

    }else{
        $row =ExecuteQuery("SELECT * FROM S4Admin.Items,OrderNo,Supplier
        where Supplier.Supplier_Code = OrderNo.OrderNo_Supplier AND Supplier_IsDC = 0 AND OrderNo.OrderNo_Number  = Items.Items_Order_Number AND  (Items_Supplier_Code =  '$itemSupplierCode' or  LEFT(Items_Supplier_Code,5) = LEFT('$itemSupplierCode',5)) 
        AND OrderNo_Project = $OrderNo_Project");
    }
    if($row === FALSE){
          die (mysql_error());
      }

      while ($rowResult = mysql_fetch_array($row))
      {
          $Items_Supplier_Code = $rowResult['Items_Supplier_Code'];
          $OrderNo = $rowResult['OrderNo_Number'];
          $ItemDescr = $rowResult['Items_Description'];
          $Qty = $rowResult['Items_Quantity'];
          array_push($arrayR, array("SupplierCode"=>$Items_Supplier_Code,
                                        "OrderNo"=>$OrderNo,
                                        "ItemDescr"=>$ItemDescr,
                                        "Qty"=>$Qty));
      }
      return $arrayR;

  }
  
  function doCheck(){
        $ErrorItems = "";
        $style = 'style ="border: 1px solid #dddada;"';
        $response ="";//stores headers
        $warning = "";//will print table of all the matching items in the database
        $itemcheck = array();//stores the items matching with the items being ordered
        $returnNoItems = '<h4>There are no matches on the controlled goods sections.</h4>';
        if($_GET['docheck'] === 'edit'){

            if (SizeOf($_SESSION['EditInternalOrder']) > 23)
            {
                    for ($count = 24; $count <= SizeOf($_SESSION['EditInternalOrder']); $count++)
                    {
                      if ($_SESSION['EditInternalOrder'][$count] != "")
                      {
                          $supplierCode =  $_SESSION['EditInternalOrder'][$count][0];
                          $discribtion =   $_SESSION['EditInternalOrder'][$count][1];
                          $UncontrolledgoodsList = getUncontrolledGoodsList();
                          $quan = isInUncontrolled($supplierCode, $discribtion, $UncontrolledgoodsList);
                          if($quan!=0){
                             // $ErrorItems .= $supplierCode.'__'.$discribtion.'___'.$quan.'</br>';
                              $ErrorItems .= '<tr><td '.$style.'>'.$supplierCode.'</td><td '.$style.'>'.$discribtion.'</td><td '.$style.'> '.$_SESSION['EditInternalOrder'][$count][3].' </td><td '.$style.'>'.$quan.'</td><td '.$style.'>'.matchedon($supplierCode, $discribtion, $UncontrolledgoodsList).'</td></tr>';
                          }
                      }

              }
            }
        }
        else{

          if (SizeOf($_SESSION['AddInternalOrder']) > 20)
          {
              $count_check = 0;
              for ($count = 20; $count < SizeOf($_SESSION['AddInternalOrder']); $count++)
              {
                  if ($_SESSION['AddInternalOrder'][$count] != "")
                  {
                          $supplierCode =  $_SESSION['AddInternalOrder'][$count][0];
                          $discribtion =   $_SESSION['AddInternalOrder'][$count][1];
                          $UncontrolledgoodsList = getUncontrolledGoodsList();
                          $quan = isInUncontrolled($supplierCode, $discribtion, $UncontrolledgoodsList);
                          $OrderNo_Project =  $_SESSION['AddInternalOrder'][2];
                          if($quan!=0){
                             // $ErrorItems .= $supplierCode.'__'.$discribtion.'___'.$quan.'</br>';
                              $ErrorItems .= '<tr><td '.$style.'>'.$supplierCode.'</td><td '.$style.'>'.$discribtion.'</td><td '.$style.'> '.$_SESSION['AddInternalOrder'][$count][3].' </td><td '.$style.'>'.$quan.'</td><td '.$style.'>'.matchedon($supplierCode, $discribtion, $UncontrolledgoodsList).'</td></tr>';
                          }
                          //foreach item, check if it matches the ones on the database
                          $itemcheck = ItemCheck($supplierCode,$discribtion,$OrderNo_Project);

                  }
              }
          }
            if(sizeof($itemcheck) > 0){
                for($i = 0; $i < sizeof($itemcheck); $i++)
                {
                    $warning .= '<tr><td '.$style.'>'.$itemcheck[$i]["OrderNo"].'</td><td '.$style.'>'.$itemcheck[$i]["ItemDescr"].'</td><td '.$style.'> '
                        .$itemcheck[$i]["Qty"].' </td><td '.$style.'>'.$itemcheck[$i]["SupplierCode"].'</td></tr>';
                }
            }
        }


        if($ErrorItems === ""){
            echo $returnNoItems;
        }else if($warning === ""){
            echo '<h4>Item(s) do not match any items on the database that have been ordered for this project.</h4><br/>';//"<br\>Item(s) do not match any items on the database that have been ordered for this project.".'<br\> <br\>';
        }

        if($ErrorItems){
           echo $response.'<h3>Controlled Goods</h3><br/><table style="width:99.9%; text-align: left; border-colapse: colapse;"><tr><th '.$style.'> Supplier Code </th><th '.$style.'> Description </th><th '.$style.'> QTY to Order </th><th '.$style.'> QTY in Store  </th><th '.$style.'>Matched On</th></tr>'.$ErrorItems.'<table> <br/><br>                  
                </table>';
        }
    /*if it return matching items create table*/
        if($warning){
            echo $response.' <h3>Items of the same project</h3><br/><h5>These items have been ordered for this project, and they match with the items youre ordering, please check that youre not ordering an Item that has been ordered.</h5> <br/><table style="width:99.9%; text-align: left; border-colapse: colapse;">
                       <tr><th '.$style.'> Order Number</th><th '.$style.'> Description </th><th '.$style.'> QTY ordered </th><th '.$style.'>Item Manufacture Code</th></tr>
                       '.$warning.'';
        }





  }
  
  function getMultipleOrderView(){
      
     
      
       //get the dates    
         $startDate = $_GET['dateS'];
         $endDate = $_GET['dateE'];
        
     // get order status       
        switch ($_GET['orderStatus'])
        {
          case "":
            $status = "";
            break;
          default:
            $status = ' AND OrderNo_Complete = "'.$_GET['orderStatus'].'"';
            break;
        }
        
        //get where status
          $Category = '';
        if ($_GET['Category'] !== "") {  
          $Category = ' AND ReportsCategory_ID = "'.$_GET['Category'].'"';
        } 
        
        $project = '';
        if ($_GET['project'] !== "") { 
          
             // update to  be 1990,01,01 to get all orders 
           if ($_GET['allProjectsOrders'] === 'true' ){
              $startDate = '1990-01-01';
              $_GET['dateS'] = '1990-01-01';
               $endDate = '2990-01-01';
              $_GET['dateE'] = '2990-01-01';
            }

            
          $project = ' AND OrderNo_Project = "'.$_GET['project'].'"';
        } 
        $supplier = '';
        if ($_GET['supplier'] !== ""){
          $supplier = ' AND OrderNo_Supplier = "'.$_GET['supplier'].'"';
        }  
        
        $date = Date('Y-m-d 0:0:0');
        if ($_GET['NotDeliverd'] === 'true'){
              $deliver = ' AND OrderNo_Date_Delivery <= "'.$date.'"';
         }
        
//          $supplier2 = '';
//        if ($_GET['deficit'] === 'true')
//           $supplier2 =' AND s.Supplier_IsDC = 0 ';
        
        // select all the order numbers
            if ($_SESSION['cAuth'] & 32){
 
           $q = 'SELECT rc.*, Currency_Symbol,CONCAT(Staff_First_Name, " ", Staff_Last_Name) AS Requested_By,  CONCAT(Project_Pastel_Prefix, " - ", Project_Description) AS Project, '
                   . 'Supplier_Name, o.* FROM OrderNo o '
                   . 'LEFT JOIN Supplier s on o.OrderNo_Supplier = s.Supplier_Code '
                   . 'LEFT JOIN Project p on o.OrderNo_Project = p.Project_Code '
                   . 'LEFT JOIN Staff st on o.OrderNo_Requested = st.Staff_Code '
                   . 'LEFT JOIN Currency c on OrderNo_Original_Currency = c.Currency_ID '
                   . 'LEFT JOIN ReportsCategory rc on o.OrderNo_ReportCategory = rc.ReportsCategory_ID '
                   . 'WHERE OrderNo_Date_Time BETWEEN "'.$_GET['dateS'].' 00:00:00" AND "'.$_GET['dateE'].' 23:59:59" '.$status.$project.$supplier.$deliver.$Category
                   . ' ORDER BY OrderNo_Date_Time ASC';
            
             $resultSet = ExecuteQuery($q);
        } 
            else{   //rc.*,
              $q = 'SELECT rc.*, Currency_Symbol,CONCAT(Staff_First_Name, " ", Staff_Last_Name) AS Requested_By,  CONCAT(Project_Pastel_Prefix, " - ", Project_Description) AS Project, '
                      . 'Supplier_Name, o.* FROM OrderNo o '
                      . 'LEFT JOIN Supplier s on o.OrderNo_Supplier = s.Supplier_Code '
                      . 'LEFT JOIN Project p on o.OrderNo_Project = p.Project_Code '
                      . 'LEFT JOIN Staff st on o.OrderNo_Requested = st.Staff_Code '
                      . 'LEFT JOIN Currency c on OrderNo_Original_Currency = c.Currency_ID '
                      . 'LEFT JOIN ReportsCategory rc on o.OrderNo_ReportCategory = rc.ReportsCategory_ID '
                      . 'WHERE OrderNo_Date_Time BETWEEN "'.$_GET['dateS'].' 00:00:00" AND "'.$_GET['dateE'].' 23:59:59" '.$status.$project.$supplier.$deliver.$Category
                      . ' ORDER BY OrderNo_Date_Time ASC';

            $resultSet = ExecuteQuery($q);
        }
         
        //$invoice_array: array of the Key:Value InternalOrder number: all the invoices  
        //$total_array:  array of the Key:Value InternalOrder number:  total of all the invoices 
        $invoice_array = array();
        $total_array = array();
        $SupplierInvoice_Number_resultset = ExecuteQuery('SELECT SupplierInvoice_Total, SupplierInvoice_S4OrderNo, `SupplierInvoice_Number` FROM SupplierInvoice'); 
        while($Invoice_row = MySQL_Fetch_Array($SupplierInvoice_Number_resultset)){
            $num = $Invoice_row['SupplierInvoice_S4OrderNo'];
            if($invoice_array[$num]==null){                   
                 $invoice_array[$num] = $Invoice_row['SupplierInvoice_Number'].' ';
            }else{
                $invoice_array[$num] = $invoice_array[$num].', '.$Invoice_row['SupplierInvoice_Number']; 
            }      
            
            if($total_array[$num]==null){                   
                 $total_array[$num] = $Invoice_row['SupplierInvoice_Total'];
            }else{
                $total_array[$num] = $total_array[$num] + $Invoice_row['SupplierInvoice_Total']; 
            }   
        }
              
        
        $latest_array = array();
        $resultSet1 = ExecuteQuery('SELECT SupplierInvoiceItems_Items_ID, SupplierInvoiceItems_Value  FROM SupplierInvoiceItems ORDER BY SupplierInvoiceItems_ID DESC');
        while($Invoice = MySQL_Fetch_Array($resultSet1)){  
            $id = $Invoice['SupplierInvoiceItems_Items_ID'];
            if($latest_array [$id]==null){                   
                 $latest_array [$id] = $Invoice['SupplierInvoiceItems_Value'];
            } 
         }
         
        
         
         // get all the items where Items_Quantity > Items_Storeman_Received
        if ($_GET['deficit'] === 'true'  ){
           $resultSetTempB = ExecuteQuery('SELECT * FROM Items WHERE Items_Quantity > Items_Storeman_Received AND Items_StorageType!= 5 Order by Items_Order_Number');
        }/* returns items that have not been delivered to the storeroom*/
        else if( $_GET['NotDeliverd'] === 'true'){
            $resultSetTempB = ExecuteQuery('SELECT * FROM Items WHERE Items_Storeman_Received = 0  AND Items_StorageType!= 5 Order by Items_Order_Number');
        }
         else{  
             $resultSetTempB = ExecuteQuery('SELECT * FROM Items Order by Items_Order_Number ');
         }
       

        //$Items_array: array of the Key:Value InternalOrder number: array of all the items
        $Items_array = array();
        while($sub_row = MySQL_Fetch_Array($resultSetTempB)){
           
            for($i=0; $i< sizeof($sub_row); $i++){
                 unset($sub_row[$i]);
            }
 
            $nett_value = $sub_row['Items_Value'];
                if($sub_row['Items_Discount'] > 0)
                  $nett_value -= ($sub_row['Items_Discount']/100) * $nett_value;
            
            $sub_row['Nett_Value']=  round($nett_value,2);
            
        if($latest_array[$sub_row['Items_ID']]!= null){
                $sub_row['Latest_Value']=  $latest_array[$sub_row['Items_ID']];  
            }
            else{
                 $sub_row['Latest_Value']=  '';  
            }
            
            $type = RetutnStorageTypeDesc($sub_row['Items_StorageType']);
            $sub_row['Items_StorageType'] = $type;
            
            $key = $sub_row['Items_Order_Number'];
            if($Items_array[$key]==null){  
                $Items_array[$key] = array($sub_row);                     
            }
            else{
                array_push($Items_array[$key], $sub_row);  // add to array with that key 
            }
       }    
     
        if (MySQL_Num_Rows($resultSet) > 0){
            $list = array(); 
            while ($row = MySQL_Fetch_Array($resultSet)) {

                if ($_GET['deficit'] === 'true' || $_GET['NotDeliverd'] === 'true'){  // only want to see order that has Items that is dificit
                    
                     $num=$row['OrderNo_Number'];
                     if($Items_array[$num]!=null){
                      //remove the first elements that do not have keys in row
                     for($i=0; $i< sizeof($row); $i++){
                         unset($row[$i]);
                     }
                    //append the Invoice array for Order using Key
                    if($invoice_array[$num] == null){                   
                         $row['SupplierInvoice'] = '';
                    }else{
                         $row['SupplierInvoice'] = $invoice_array[$num];
                    }         

                    $row['Currency_Symbol'] = iconv('Windows-1252', 'UTF-8//TRANSLIT',$row['Currency_Symbol']);

                     //append the Invoice total array for Order using Key
                    if($total_array[$num]==null){  
                         $row['total'] = '';
                    }else{
                         $row['total'] = $total_array[$num]; 
                    }    

                       // add items
                    $row['Items'] = $Items_array[$num];

                    array_push($list, $row);      
                    }                
                }
                else{
                    //remove the first elements that do not have keys in row
                     for($i=0; $i< sizeof($row); $i++){
                         unset($row[$i]);
                     }


                    $num=$row['OrderNo_Number'];
                    //append the Invoice array for Order using Key
                    if($invoice_array[$num] == null){                   
                         $row['SupplierInvoice'] = '';
                    }else{
                         $row['SupplierInvoice'] = $invoice_array[$num];
                    }         

                    $row['Currency_Symbol'] = iconv('Windows-1252', 'UTF-8//TRANSLIT',$row['Currency_Symbol']);

                     //append the Invoice total array for Order using Key
                    if($total_array[$num]==null){  
                         $row['total'] = '';
                    }else{
                         $row['total'] = $total_array[$num]; 
                    }    


                    if($Items_array[$num]==null){  
                         $row['Items'] = array();
                    }else{
                         $row['Items'] = $Items_array[$num];
                    }   

                    // add items

                    array_push($list, $row);   
                
                }
         }
        
         //echo json_encode($Items_array);
            echo json_encode($list);
    }
        
        else{
            $r = array();
             echo json_encode($r);
        }
      
  }
  
   // check to see if it is a valid order number for marking
  function validOrderNumber($ordernumber){
        if (!CheckNumeric($ordernumber) || (StrLen($ordernumber) != 7))
           return false;
        else
        {
            $loRS = ExecuteQuery('SELECT * FROM OrderNo WHERE OrderNo_Number = "'.$ordernumber.'"');
            if (MySQL_Num_Rows($loRS) == 0)
              return false;
            else
            {
               return true;
            }
        }
  }

  function MarkOrdersList($ordernumberList,$status){
     $failed =  array();
     foreach ($ordernumberList as $order){
        if(!MarkOrder($order, $status)){
            array_push($failed, $order);
        }   
     }
     
     if(sizeof($failed) <= 0){
        $_SESSION['InternalOrderSuccess'] = 'geh!';
        $st = "PASS";
        
     }
     else{
        $_SESSION['InternalOrderFail'] = 'geh!';
        $st = "FAIL";
     }
     echo json_encode(array($st,$failed)); 
  }
  
  // return true or false 
  function MarkOrder($ordernumber, $status){      
       if ($ordernumber !== "" &&  $status !=="")
        {    
          if(validOrderNumber($ordernumber)){ 
          $row = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM OrderNo WHERE OrderNo_Number = "'.$ordernumber.'"'));
          $loCurrencyRow = MySQL_Fetch_Array(ExecuteQuery('SELECT C.Currency_Rate, C.Currency_Symbol FROM Currency C WHERE Currency_ID = "'.$row['OrderNo_Currency'].'"'));
          $loCurrencySymbol = $loCurrencyRow['Currency_Symbol'];
          
          $success = true;
          if ($row['OrderNo_Complete'] != $status)
          {
            switch ($status)
            {
              case '0':
                $success = ExecuteQuery('UPDATE OrderNo SET OrderNo_Complete = "0", OrderNo_Comments = CONCAT(OrderNo_Comments, " ", "This order was marked as incomplete on '.Date('d M Y').'") WHERE OrderNo_Number = "'.$ordernumber.'"');
                break;
              case '1':
                $success = ExecuteQuery('UPDATE OrderNo SET OrderNo_Complete = "1", OrderNo_Comments = CONCAT(OrderNo_Comments, " ", "This order was marked as complete on '.Date('d M Y').'") WHERE OrderNo_Number = "'.$ordernumber.'"');
                break;
              case '2':
                $success = ExecuteQuery('UPDATE OrderNo SET OrderNo_Complete = "2", OrderNo_Comments = CONCAT(OrderNo_Comments, " ", "This order was cancelled on '.Date('d M Y').'") WHERE OrderNo_Number = "'.$ordernumber.'"');
                break;
              case '3':
                $success = ExecuteQuery('UPDATE OrderNo SET OrderNo_Complete = "3", OrderNo_Comments = CONCAT(OrderNo_Comments, " ", "This order was followed up.") WHERE OrderNo_Number = "'.$ordernumber.'"');
                break;
              default:
                break;
            }
          }
          if ($success)
          {
               $row2 = MySQL_Fetch_Array(ExecuteQuery('SELECT `Level` FROM BEE_Certificate WHERE Supplier_Code = '.$row['Supplier_Code']));
                $levelBEE = 'non-compliant';
                 if($row2['Level']!=null){
                      if($row2['Level'] == 9){
                       $levelBEE = "N/A - International";
                    }
                    
                     else if($row2['Level'] == 10){
                           $levelBEE = "Awaiting feedback";
                    }
                    
                    else if($row2['Level'] == 0){
                       $levelBEE = "non-compliant";
                    }
                    
                    else
                        $levelBEE = $row2['Level'];
                }
                
                  $tdLevel = '<TD style="font-weight: bold; background-color: rgba(255, 0, 0, 0.32); padding-left: 0.5em; padding-right: 0.5em;" >non-compliant</TD>';
              if($row2['Level']!=null){
                   if($row2['Level'] == 9){
                        $tdLevel = '<TD style="font-weight: bold; background-color: rgba(10, 148, 23, 0.32); padding-left: 0.5em; padding-right: 0.5em; " >N/A - International</TD>';
                    }
                    else if($row2['Level'] == 10){
                         $tdLevel = '<TD style="font-weight: bold; background-color: rgba(10, 115, 254, 0.58); padding-left: 0.5em; padding-right: 0.5em; " >Awaiting feedback</TD>';
                    } 
                    else if($row2['Level'] == 0){
                        $tdLevel = '<TD style="font-weight: bold; background-color: rgba(255, 0, 0, 0.32); padding-left: 0.5em; padding-right: 0.5em; " >non-compliant</TD>';
                    }
                    
              else if($row2['Level'] < 6) { // 1 2 3 4 5 
                        $tdLevel = '<TD style="font-weight: bold; background-color: rgba(10, 148, 23, 0.32); padding-left: 0.5em; padding-right: 0.5em; " >'.$row2['Level'].'</TD>';
                    }
                    
                    else {   // 6 7 8
                          $tdLevel = '<TD style="font-weight: bold; background-color: rgba(245, 253, 21, 0.58); padding-left: 0.5em; padding-right: 0.5em;" >'.$row2['Level'].'</TD>';
                    }
            }
                $qNumber2 = "";
                $qNumber = $row['quotation_number'];
                if($qNumber!= null || $qNumber!= '' ){

                     $qNumber = 'QUOTATION NUMBER:               '.$row['quotation_number'];
                     $qNumber2 = '<TR><TD><B>Quotation number:</B></TD><TD>'.$row['quotation_number'].'</TR>';
                }
              
              
            //ExecuteQuery('DELETE FROM Reminder WHERE Reminder_System_Reference = "IO" AND Reminder_System_Reference_ID = "'.$ordernumber.'"');
            if ($_POST['Status'] == '3')
            {
              $row = MySQL_Fetch_Array(ExecuteQuery('SELECT Staff_First_Name, Staff_Last_Name, Staff_email, Supplier_Name, Supplier_Phone, Project_Description, Project_Pastel_Prefix, Project_Responsible, OrderNo.* FROM Staff, Supplier, Project, OrderNo WHERE Staff_Code = OrderNo_Requested AND Supplier_Code = OrderNo_Supplier AND Project_Code = OrderNo_Project AND OrderNo_Number= "'.$ordernumber.'"'));
                     
              if ($row['OrderNo_Approved'])
              {
                $rowB = MySQL_Fetch_Array(ExecuteQuery('SELECT Staff_First_Name, Staff_Last_Name FROM Staff WHERE Staff_Code = '.$_POST['Approved']));
                $emailReminder = 'ORDER NO:               '.$row['OrderNo_Number'].Chr(10).
                         'DATE:                   '.GetTextualDateFromDatabaseDate($row['OrderNo_Date_Time']).Chr(10).
                         'PROJECT:                '.$row['Project_Pastel_Prefix'].' - '.$row['Project_Description'].Chr(10).
                         'SUPPLIER:               '.$row['Supplier_Name'].' - '.$row['Supplier_Phone'].Chr(10).
                         'BEE Level:              '.$levelBEE.Chr(10).  
                          $qNumber.
                         'COMMENTS:               '.$row['OrderNo_Date_FollowUp'].Chr(10).
                         'FOLLOW UP DATE:         '.$row['OrderNo_Comments'].Chr(10).
                         'DELIVERY DATE:          '.$row['OrderNo_Date_Delivery'].Chr(10).
                         'APPROVED BY:            '.$rowB['Staff_First_Name'].' '.$rowB['Staff_Last_Name'].
                         'TOTAL (EXCL. VAT):      '.$loCurrencySymbol.$row['OrderNo_Original_Total_Cost'].Chr(10).Chr(10);
                
                $htmlReminder = '<BR /><BR />
                        <TABLE border=0>
                          <TR><TD><B>Order No:</B></TD><TD>'.$row['OrderNo_Number'].'</TD></TR>
                          <TR><TD><B>Date:</B></TD><TD>'.GetTextualDateFromDatabaseDate($row['OrderNo_Date_Time']).'</TD></TR>
                          <TR><TD><B>Project:</B></TD><TD>'.$row['Project_Pastel_Prefix'].' - '.$row['Project_Description'].'</TD></TR>
                          <TR><TD><B>Supplier:</B></TD><TD>'.$row['Supplier_Name'].' - '.$row['Supplier_Phone'].'</TD></TR>
                          <TR><TD><B>BEE Level:</B></TD>'.$tdLevel.'</TR>   
                              '.$qNumber2.'
                          <TR><TD><B>Comments:</B></TD><TD>'.$row['OrderNo_Comments'].'</TD></TR>
                          <TR><TD><B>Follow Up Date:</B></TD><TD>'.GetTextualDateFromDatabaseDate($row['OrderNo_Date_FollowUp']).'</TD></TR>
                          <TR><TD><B>Delivery Date:</B></TD><TD>'.GetTextualDateFromDatabaseDate($row['OrderNo_Date_Delivery']).'</TD></TR>
                          <TR><TD><B>Approved By:</B></TD><TD>'.$rowB['Staff_First_Name'].' '.$rowB['Staff_Last_Name'].'</TD></TR>
                          <TR><TD><B>Total (Excl. VAT):</B></TD><TD>'.$loCurrencySymbol.$row['OrderNo_Original_Total_Cost'].'</TD></TR>
                        </TABLE>
                        <BR /><BR />';
                
                $emailReminder .= 'QTY          '.Chr(9).'SUPPLIER CODE'.Chr(9).'DESCRIPTION  '.Chr(9).Chr(9).'VALUE        '.Chr(9).'NETT VALUE        '.Chr(9).'DISCOUNT        '.Chr(9).'TOTAL        '.Chr(10);
                $htmlReminder .= '<TABLE border=1><TR><TD>QTY</TD><TD>SUPPLIER CODE</TD><TD>DESCRIPTION</TD><TD>VALUE</TD><TD>NETT VALUE</TD><TD>DISCOUNT</TD><TD>TOTAL</TD></TR>';
                
                $resultSet = ExecuteQuery('SELECT * FROM Items WHERE Items_Order_Number = "'.$ordernumber.'"');
                while ($rowC = MySQL_Fetch_Array($resultSet))
                {
                  $nett_value_mail = $rowC['Items_Original_Value'];
                  $discount = 0;
                                if($rowC['Items_Discount'] > 0)
                                {
                                    $nett_value_mail -= (($rowC['Items_Discount']/100) * $nett_value_mail);
                                    $discount = ($rowC['Items_Discount']/100) * $rowC['Items_Original_Total'];
                                }
                  $emailReminder .= $rowC['Items_Quantity'].Chr(9).Chr(9).Chr(9).$rowC['Items_Supplier_Code'].Chr(9).Chr(9).Chr(9).$rowC['Items_Description'].Chr(9).Chr(9).Chr(9).Chr(9).Chr(9).$loCurrencySymbol.SPrintF('%02.2f', $rowC['Items_Original_Value']).Chr(9).Chr(9).$loCurrencySymbol.SPrintF('%02.2f', $nett_value_mail).Chr(9).Chr(9).$rowC['Items_Discount'].'%'.Chr(9).Chr(9).$loCurrencySymbol.SPrintF('%02.2f', ($rowC['Items_Original_Total'] - $discount)).Chr(10).Chr(10);
                  $htmlReminder .= '<TR><TD>'.$rowC['Items_Quantity'].'</TD><TD>'.$rowC['Items_Supplier_Code'].'</TD><TD>'.$rowC['Items_Description'].'</TD><TD>'.$loCurrencySymbol.SPrintF('%02.2f', $rowC['Items_Original_Value']).'</TD><TD>'.$loCurrencySymbol.SPrintF('%02.2f', $nett_value_mail).'</TD><TD>'.$rowC['Items_Discount'].'%'.'</TD><TD>'.$loCurrencySymbol.SPrintF('%02.2f', ($rowC['Items_Original_Total'] - $discount)).'</TD></TR>';
                }
                
//                $today = Date('Y-m-d');
//                if (!DatabaseDateLater($row['OrderNo_Date_Delivery'], $today) && ($row['OrderNo_Date_Delivery'] != $today))
                //  ExecuteQuery('INSERT INTO Reminder VALUES("", "'.$_SESSION['cUID'].'", "'.Date('Y-m-d H:i:s').'", "'.$row['OrderNo_Requested'].'", "'.GetReminderDate($row['OrderNo_Date_Delivery']).'", "4", "5", "32", "Order <B>'.$row['OrderNo_Number'].'</B> due for delivery on '.GetTextualDateFromDatabaseDate($row['OrderNo_Date_Delivery']).'. '.$row['Supplier_Name'].' for '.$row['Project_Description'].'.", "IO", "'.$row['OrderNo_Number'].'", "", "")');
                //else
                 // ExecuteQuery('INSERT INTO Reminder VALUES("", "'.$_SESSION['cUID'].'", "'.Date('Y-m-d H:i:s').'", "'.$row['OrderNo_Requested'].'", "'.GetReminderDate($row['OrderNo_Date_Delivery']).'", "4", "5", "32", "Order <B>'.$row['OrderNo_Number'].'</B> due for delivery on '.GetTextualDateFromDatabaseDate($row['OrderNo_Date_Delivery']).'. '.$row['Supplier_Name'].' for '.$row['Project_Description'].'.", "IO", "'.$row['OrderNo_Number'].'", "Reminder for the delivery of the following order: '.$emailReminder.'", "Reminder for the delivery of the following order: '.$htmlReminder.'")');
                
              }
            } else
            if ($_POST['Status'] == '0')
            {
              if ($row['OrderNo_Approved'])
              {
                $rowB = MySQL_Fetch_Array(ExecuteQuery('SELECT Staff_First_Name, Staff_Last_Name FROM Staff WHERE Staff_Code = '.$_POST['Approved']));
                
                $emailReminder = Chr(10).'ORDER NO:               '.$row['OrderNo_Number'].Chr(10).
                         'DATE:                   '.GetTextualDateFromDatabaseDate($row['OrderNo_Date_Time']).Chr(10).
                         'PROJECT:                '.$row['Project_Pastel_Prefix'].' - '.$row['Project_Description'].Chr(10).
                         'SUPPLIER:               '.$row['Supplier_Name'].' - '.$row['Supplier_Phone'].Chr(10).
                         'BEE Level:              '.$levelBEE.Chr(10).  
                          $qNumber.
                         'COMMENTS:               '.$row['OrderNo_Date_FollowUp'].Chr(10).
                         'FOLLOW UP DATE:         '.$row['OrderNo_Comments'].Chr(10).
                         'DELIVERY DATE:          '.$row['OrderNo_Date_Delivery'].Chr(10).
                         'APPROVED BY:            '.$rowB['Staff_First_Name'].' '.$rowB['Staff_Last_Name'].
                         'TOTAL (EXCL. VAT):      '.$loCurrencySymbol.$row['OrderNo_Original_Total_Cost'].Chr(10).Chr(10);
                
                $htmlReminder = '<BR /><BR />
                        <TABLE border=0>
                          <TR><TD><B>Order No:</B></TD><TD>'.$row['OrderNo_Number'].'</TD></TR>
                          <TR><TD><B>Date:</B></TD><TD>'.GetTextualDateFromDatabaseDate($row['OrderNo_Date_Time']).'</TD></TR>
                          <TR><TD><B>Project:</B></TD><TD>'.$row['Project_Pastel_Prefix'].' - '.$row['Project_Description'].'</TD></TR>
                          <TR><TD><B>Supplier:</B></TD><TD>'.$row['Supplier_Name'].' - '.$row['Supplier_Phone'].'</TD></TR>
                         <TR><TD><B>BEE Level:</B></TD>'.$tdLevel.'</TR>   
                         '.$qNumber2.'
                          <TR><TD><B>Comments:</B></TD><TD>'.$row['OrderNo_Comments'].'</TD></TR>
                          <TR><TD><B>Follow Up Date:</B></TD><TD>'.GetTextualDateFromDatabaseDate($row['OrderNo_Date_FollowUp']).'</TD></TR>
                          <TR><TD><B>Delivery Date:</B></TD><TD>'.GetTextualDateFromDatabaseDate($row['OrderNo_Date_Delivery']).'</TD></TR>
                          <TR><TD><B>Approved By:</B></TD><TD>'.$rowB['Staff_First_Name'].' '.$rowB['Staff_Last_Name'].'</TD></TR>
                          <TR><TD><B>Total (Excl. VAT):</B></TD><TD>'.$loCurrencySymbol.$row['OrderNo_Original_Total_Cost'].'</TD></TR>
                        </TABLE>
                        <BR /><BR />';
                
                $emailReminder .= 'QTY          '.Chr(9).'SUPPLIER CODE'.Chr(9).'DESCRIPTION  '.Chr(9).Chr(9).'VALUE        '.Chr(9).'NETT VALUE        '.Chr(9).'DISCOUNT        '.Chr(9).'TOTAL        '.Chr(10);
                $htmlReminder .= '<TABLE border=1><TR><TD>QTY</TD><TD>SUPPLIER CODE</TD><TD>DESCRIPTION</TD><TD>VALUE</TD><TD>NETT VALUE</TD><TD>DISCOUNT</TD><TD>TOTAL</TD></TR>';
                
                $resultSet = ExecuteQuery('SELECT * FROM Items WHERE Items_Order_Number = "'.$ordernumber.'"');
                while ($rowC = MySQL_Fetch_Array($resultSet))
                {
                  $nett_value_mail = $rowC['Items_Original_Value'];
                  $discount = 0;
                                if($rowC['Items_Discount'] > 0)
                                {
                                    $nett_value_mail -= (($rowC['Items_Discount']/100) * $nett_value_mail);
                                    $discount = ($rowC['Items_Discount']/100) * $rowC['Items_Original_Total'];
                                }
                                
                  $emailReminder .= $rowC['Items_Quantity'].Chr(9).Chr(9).Chr(9).$rowC['Items_Supplier_Code'].Chr(9).Chr(9).Chr(9).$rowC['Items_Description'].Chr(9).Chr(9).Chr(9).Chr(9).Chr(9).$loCurrencySymbol.SPrintF('%02.2f', $rowC['Items_Original_Value']).Chr(9).Chr(9).$loCurrencySymbol.SPrintF('%02.2f', $nett_value_mail).Chr(9).Chr(9).$rowC['Items_Discount'].'%'.Chr(9).Chr(9).$loCurrencySymbol.SPrintF('%02.2f', ($rowC['Items_Original_Total'] - $discount)).Chr(10).Chr(10);
                  $htmlReminder .= '<TR><TD>'.$rowC['Items_Quantity'].'</TD><TD>'.$rowC['Items_Supplier_Code'].'</TD><TD>'.$rowC['Items_Description'].'</TD><TD>'.$loCurrencySymbol.SPrintF('%02.2f', $rowC['Items_Original_Value']).'</TD><TD>'.$loCurrencySymbol.SPrintF('%02.2f', $nett_value_mail).'</TD><TD>'.$rowC['Items_Discount'].'%'.'</TD><TD>'.$loCurrencySymbol.SPrintF('%02.2f', ($rowC['Items_Original_Total'] - $discount)).'</TD></TR>';
                }
                
//                $today = Date('Y-m-d');
//                if (!DatabaseDateLater($row['OrderNo_Date_Delivery'], $today) && ($row['OrderNo_Date_Delivery'] != $today))
                //  ExecuteQuery('INSERT INTO Reminder VALUES("", "'.$_SESSION['cUID'].'", "'.Date('Y-m-d H:i:s').'", "'.$row['OrderNo_Requested'].'", "'.GetReminderDate($row['OrderNo_Date_Delivery']).'", "4", "5", "32", "Order <B>'.$row['OrderNo_Number'].'</B> due for delivery on '.GetTextualDateFromDatabaseDate($row['OrderNo_Date_Delivery']).'. '.$row['Supplier_Name'].' for '.$row['Project_Description'].'.", "IO", "'.$row['OrderNo_Number'].'", "", "")');
                //else
                  //ExecuteQuery('INSERT INTO Reminder VALUES("", "'.$_SESSION['cUID'].'", "'.Date('Y-m-d H:i:s').'", "'.$row['OrderNo_Requested'].'", "'.GetReminderDate($row['OrderNo_Date_Delivery']).'", "4", "5", "32", "Order <B>'.$row['OrderNo_Number'].'</B> due for delivery on '.GetTextualDateFromDatabaseDate($row['OrderNo_Date_Delivery']).'. '.$row['Supplier_Name'].' for '.$row['Project_Description'].'.", "IO", "'.$row['OrderNo_Number'].'", "Reminder for the delivery of the following order: '.$emailReminder.'", "Reminder for the delivery of the following order: '.$htmlReminder.'")');
//                if (!DatabaseDateLater($row['OrderNo_Date_FollowUp'], $today) && ($row['OrderNo_Date_FollowUp'] != $today))
                //  ExecuteQuery('INSERT INTO Reminder VALUES("", "'.$_SESSION['cUID'].'", "'.Date('Y-m-d H:i:s').'", "'.$row['OrderNo_Requested'].'", "'.GetReminderDate($row['OrderNo_Date_FollowUp']).'", "4", "5", "32", "Order <B>'.$row['OrderNo_Number'].'</B> due for follow up on '.GetTextualDateFromDatabaseDate($row['OrderNo_Date_FollowUp']).'. '.$row['Supplier_Name'].' for '.$row['Project_Description'].'.", "IO", "'.$row['OrderNo_Number'].'", "", "")');
                //else
//                  ExecuteQuery('INSERT INTO Reminder VALUES("", "'.$_SESSION['cUID'].'", "'.Date('Y-m-d H:i:s').'", "'.$row['OrderNo_Requested'].'", "'.GetReminderDate($row['OrderNo_Date_FollowUp']).'", "4", "5", "32", "Order <B>'.$row['OrderNo_Number'].'</B> due for follow up on '.GetTextualDateFromDatabaseDate($row['OrderNo_Date_FollowUp']).'. '.$row['Supplier_Name'].' for '.$row['Project_Description'].'.", "IO", "'.$row['OrderNo_Number'].'", "Reminder for the follow up of the following order: '.$emailReminder.'", "Reminder for the follow up of the following order: '.$htmlReminder.'")');
              }
            }
            
            if ($_SESSION['cUID'] != '32')
            {
              $row = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = '.$_SESSION['cUID'].''));
              
              $email = 'Internal Order status has been updated. The details are as follows:'.Chr(10).
                       'ORDER NUMBER:          '.$ordernumber.Chr(10).
                       'UPDATED BY:            '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].Chr(10).Chr(10).
                       'Please review the updated details to ensure that everything is in order.';
              $html = 'Internal Order status has been updated. The details are as follows:
                      <BR /><BR />
                      <TABLE border=0>
                        <TR><TD><B>Order Number:</B></TD><TD>'.$ordernumber.'</TD></TR>
                        <TR><TD><B>Updated By:</B></TD><TD>'.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].'</TD></TR>
                      </TABLE>
                      <BR />
                      Please review the updated details to ensure that everything is in order.
                      <BR /><BR />';
              
           //   SendMailHTML('cherie@s4integration.co.za', 'Internal Order Status Updated - S4 - '.$ordernumber, $email, $html);
              SendMailHTML('office.admin@s4.co.za', 'Internal Order Status Updated - S4 - '.$ordernumber, $email, $html);
              SendMailHTML('amelia@s4integration.co.za', 'Internal Order Status Updated - S4 - '.$ordernumber, $email, $html);
            }
            return true;
          } else
             return false;
          }
          else{
             return false;
          }
        }  
       else{
          return false;
       }
  }
  
  function SendErrorEmail($message,$staff){
   
      if(isset($_GET['Message']) && isset($_GET['StaffCode'])){
          
          $failed = false;
          
           foreach($_GET['StaffCode'] as $curStaff) {
               
               if(!BuildSendErrorEmail($message,$curStaff))
                   $failed = true; 
           }

           if(!$failed){
                $_SESSION['InternalOrderSuccess'] = 'geh!';
                echo "pass";
           }
           else{
               $_SESSION['InternalOrderFail'] = 'geh!';
               echo'failed';
           }
           
           
          
      }
      else{
            $_SESSION['InternalOrderFail'] = 'geh!';
            echo'failed';
      }
  }
  
  function BuildSendErrorEmail($message,$staff){
    
      
     // if(isset($_SESSION['$_GET['Message']) &&  $_GET['StaffCode']']))
      try{
                
        $Row_staff = MySQL_Fetch_Array(ExecuteQuery('SELECT Staff_First_Name FROM Staff WHERE Staff_Code = "'.$staff.'"'));

        $row = MySQL_Fetch_Array(ExecuteQuery('SELECT concat(s.Staff_First_Name," ",s.Staff_Last_Name) as Req, sp.Supplier_Name, o.quotation_number, concat(p.Project_Pastel_Prefix,"-",p.Project_Description) as Project ,
        t.OrderType_Description, c.Currency_Symbol , o.OrderNo_Total_Cost, o.OrderNo_Date_Time, o.OrderNo_Comments, o.OrderNo_Delivery FROM OrderNo o
        LEFT JOIN Staff s on s.Staff_Code =  OrderNo_Requested 
        LEFT JOIN Supplier sp on sp.Supplier_Code = OrderNo_Supplier
        LEFT JOIN Project p on p.Project_Code = OrderNo_Project
        LEFT JOIN OrderType t on t.OrderType_ID = OrderNo_Type
        LEFT JOIN Currency c on c.Currency_ID = OrderNo_Currency
        WHERE OrderNo_Number = '.$_SESSION['ViewInternalOrderSingle'][0]));
        
        $dev = "";
               switch ($row['OrderNo_Delivery'])
               {
                  case '0':
                    $dev = 'Collect';
                    break;
                  case '1':
                    $dev ='Delivery'; 
                    break;  
                  case '2':
                   $dev = 'Courier';
                    break;
                  default:
                    break;
                }   

        $contents2html = '
            <p>Hi '.$Row_staff['Staff_First_Name'].'</p>
            <p style="font-family: sans-serif;">'.$message.'</p>
            </br>
            <p><b><u>Order details for '.$_SESSION['ViewInternalOrderSingle'][0].'</u></b></p>
            </br>
            <TABLE border=0>
            <TR><TD><B>Requested By:</B></TD><TD>'.$row['Req'].'</TD></TR>
            <TR><TD><B>Supplier:</B></TD><TD>'.$row['Supplier_Name'].'</TD></TR>
            <TR><TD><B>Quotation number:</B></TD><TD>'.$row['quotation_number'].'</TD></TR>
            <TR><TD><B>Project:</B></TD><TD>'.$row['Project'].'</TD></TR>
            <TR><TD><B>Order Type:</B></TD><TD>'.$row['OrderType_Description'].'</TD></TR>
            <TR><TD><B>Total (VAT excl.):</B></TD><TD>'.$row['Currency_Symbol'].' '.$row['OrderNo_Total_Cost'].'</TD></TR>
            <TR><TD><B>Date Logged:</B></TD><TD>'.GetTextualDateFromDatabaseDate($row['OrderNo_Date_Time']).'</TD></TR>    
            <TR><TD><B>Delivery Method:</B></TD><TD>'.$dev.'</TD></TR>    
            <TR><TD><B>Comments:</B></TD><TD>'.$row['OrderNo_Comments'].'</TD></TR>  
            </TABLE>

            </br>
            <p><b><u>Items details for '.$_SESSION['ViewInternalOrderSingle'][0].'</u></b></p>
            </br>';

           $contents2 = 'Hi '.$Row_staff['Staff_First_Name'].Chr(10).
           $message.Chr(10).
           'Requested:                    '.$row['Req'].Chr(10).
           'Supplier:                     '.$row['Supplier_Name'].Chr(10).
           'Quotation number:             '.$row['quotation_number'].Chr(10).
           'Project:                      '.$row['Project'].Chr(10).
           'Order Type:                   '.$row['OrderType_Description'].Chr(10).
           'Total (VAT excl.):            '.$row['Currency_Symbol'].' '.$row['OrderNo_Total_Cost'].Chr(10).
           'Date Logged:                  '.GetTextualDateFromDatabaseDate($row['OrderNo_Date_Time']).Chr(10).
           'Delivery Method:              '.$dev.Chr(10).
           'Comments:                     '.$row['OrderNo_Comments'].Chr(10).
           '                                                                      '.Chr(10).
           'Items details for '.$_SESSION['ViewInternalOrderSingle'][0].Chr(10).
           '                                                                      '.Chr(10);
             
            $resultSet = ExecuteQuery('SELECT Items_ID ,Items_Supplier_Code,Items_Description, IF(Items_Backorder = 0,"No","Yes") as Backorder,Items_Original_Value, ROUND((Items_Original_Value*((100- Items_Discount)/100)),2) as Discount_Incl,Items_Quantity,Items_Discount, IF(Items_Asset = 0,"No","Yes") as Asset,s.StorageType_Name, Items_StorageType,Items_Received_Quantity ,Items_Storeman_Received FROM Items 
            LEFT JOIN StorageType s on StorageType_ID = Items_StorageType
            WHERE Items_Order_Number = '.$_SESSION['ViewInternalOrderSingle'][0]);
          
            $contents2html .= '<TABLE border=1>'
                    . '<TR><TH>Manufacturer Code</TH>'
                    . '<TH>Manufacturer Description</TH>'
                     . '<TH>Backorder</TH>'
                     . '<TH>Original Value (VAT excl.)</TH>'
                     . '<TH>Nett Value(Discount Incl.)</TH>'
                     . '<TH>Quantity</TH>'
                     . '<TH>Discount</TH>'
                     . '<TH>Asset</TH>'
                     . '<TH>Storage Type</TH>'
                     . '<TH>Invoice Captured</TH>'
                     . '<TH>Storeman Received</TH>'
                    . '</TR>';
            
           
            $contents2 .= 'Manufacturer Code'.Chr(9).'Manufacturer Description   '.Chr(9).'Backorder'.Chr(9).'Original Value (VAT excl.)'
                    .Chr(9).'Nett Value(Discount Incl.)'.Chr(9).'Quantity'.Chr(9).'Discount'.Chr(9).'Asset'.Chr(9).'Storage Type   '
                    .Chr(9).'Invoice Captured '.Chr(9).'Storeman Received'.Chr(9);
            
            while($item = MySQL_Fetch_Array($resultSet)){
                 $colour='';
                 if($item['Items_StorageType']!= 5 && ($item['Items_Received_Quantity'] != $item['Items_Storeman_Received']) ){
                       $colour = 'style="background-color: #fefc92;"';
                 }  
                
                  $contents2html .= '<TR '.$colour.'><TD>'.$item['Items_Supplier_Code'].'</TD>'
                          . '<TD>'.$item['Items_Description'].'</TD>'
                          . '<TD>'.$item['Backorder'].'</TD>'
                          . '<TD>'.$row['Currency_Symbol'].' '.$item['Items_Original_Value'].'</TD>'
                          . '<TD>'.$row['Currency_Symbol'].' '.$item['Discount_Incl'].'</TD>'
                          . '<TD>'.$item['Items_Quantity'].'</TD>'
                          . '<TD>'.$item['Items_Discount'].'</TD>'
                          . '<TD>'.$item['Asset'].'</TD>'
                          . '<TD>'.RetutnStorageTypeDesc($item['Items_StorageType']).'</TD>'
                          . '<TD>'.$item['Items_Received_Quantity'].'</TD>'
                          . '<TD>'.$item['Items_Storeman_Received'].'</TD>'; 
                  
                  $contents2 .= $item['Items_Supplier_Code'].Chr(9).
                          $item['Items_Description'].Chr(9).
                          $item['Backorder'].Chr(9).
                          $row['Currency_Symbol'].' '.$item['Items_Original_Value'].Chr(9).
                          $row['Currency_Symbol'].' '.$item['Discount_Incl'].Chr(9).
                          $item['Items_Quantity'].Chr(9).
                          $item['Items_Discount'].Chr(9).
                          $item['Asset'].Chr(9).
                          RetutnStorageTypeDesc($item['Items_StorageType']).Chr(9).
                          $item['Items_Received_Quantity'].Chr(9).
                          $item['Items_Storeman_Received'].Chr(9);
                  
             }
            $contents2html .= '</table>';

         SendMailHTML($staff, 'Invoice capture Error', $contents2 , $contents2html);
        
         return true;
           
               
      }
      catch(Exception $e){
           return false;
      }
      
  }
 
