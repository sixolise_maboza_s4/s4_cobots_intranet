<?php
  // DETAILS ///////////////////////////////////////////////////////////////////
  //                                                                          //
  //                    Last Edited By: Gareth Ambrose                        //
  //                        Date: 18 December 2007                            //
  //                                                                          //
  //////////////////////////////////////////////////////////////////////////////
  // This page manages user authentication and logging in.                    //
  //////////////////////////////////////////////////////////////////////////////
  
  include '../Scripts/Include.php';
  SetSettings();
  

	$resultSet = ExecuteQuery('SELECT * FROM Staff WHERE Staff_First_Name ="'.$_POST['Username'].'" AND Staff_Password = old_password("'.$_POST['Password'].'") AND Staff_IsEmployee > "0"');

  if (MySQL_Num_Rows($resultSet) == 1)
  {
    $row = MySQL_Fetch_Array($resultSet);

    $_SESSION['cAuth'] = $row['Staff_Auth_Level'];
  	$_SESSION['cUID'] = $row['Staff_Code'];

      /*store these*/
      setcookie("Username",$_POST['Username'],time()+(1800)); //30 mins
      setcookie("Password",$_POST['Password'],time()+(1800));

  	$date = Date('Y-m-d H:i:s');
    ExecuteQuery('UPDATE Staff SET Staff_LastAccess = "'.$date.'", Staff_AccessCount = "'.($row['Staff_AccessCount']+1).'" WHERE Staff_First_Name = "'.$_POST['Username'].'"');
  	ExecuteQuery('INSERT INTO Login VALUES("", "'.$row['Staff_Code'].'","'.$date.'")');
  } else
    $_SESSION['NotValid'] = true;

  if($_SESSION['NotValid'] == false && CheckPasswordStrength($_POST['Password']) == true) {
    $_SESSION['PasswordsWeakPassword'] = 'geh!';
    Header('Location: ../Passwords.php');
  }
  else if($_SESSION['NotValid'] == true) {
      Header('Location: ../Login.php');

  }
  else
    Header('Location: ../index.php');
     // echo  $_SESSION['cAuth'] ;


  
  function CheckPasswordStrength($pwd){
     $isWeak = false;
     
     //Password too short!
     if( strlen($pwd) < 8 ) {
         $isWeak = true;
      }
      
      //Password must include at least one number!
      if( !preg_match("#[0-9]+#", $pwd) ) {
          $isWeak = true;
      }

      if( !preg_match("#[A-Z]+#", $pwd) ) {
          $isWeak = true;
      }
      return $isWeak;
  }

?>