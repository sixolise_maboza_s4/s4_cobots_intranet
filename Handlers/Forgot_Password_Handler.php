<?php

  ob_start("ob_gzhandler");
  include '../Scripts/Include.php';
  SetSettings();
  $_POST = Replace('"', '\'\'', $_POST);

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if(isset($_POST['NewRequest']) && isset($_POST['URL']) ){
    
    $resultSet = ExecuteQuery('SELECT * FROM Staff WHERE `Staff_First_Name` = "'.$_POST['NewRequest'].'"');
    $row = MySQL_Fetch_Array($resultSet);
    
    if(mysql_num_rows($resultSet) != 0 ){
        $code = getRandomCode();
        $date = Date('Y-m-d H:i:s');
        $resultSet2 = ExecuteQuery('INSERT INTO Forgot_Password (`Code`, `Staff_Code`,`Date_Created`,`Is_Used`) VALUES ("'.$code.'", "'.$row['Staff_Code'].'","'.$date.'", 0)');
        $id = mysql_insert_id();
        $link = $_POST['URL'].'Forgot_Password.php?'.$code.$id; 
        
        $contents2 = 'Hi '.$_POST['NewRequest'].Chr(10).
                     'You have requested a password change'.Chr(10).
                     'Click the link below to reset your password. Please note that the link will expire in 2 days.'.Chr(10).
                     $link.
                     'If you did not request a password change please inform S4 immediately.'.Chr(10).
                     'Kind Regards'.Chr(10).
                     'S4 Integration'.Chr(10); 
                    
       
        $contents2html = '<TABLE border=0>
                            <TR>Hi '.$_POST['NewRequest'].'</TR>
                            <TR></TR>
                            <TR>You have requested a password change</TR>
                            <TR></TR>
                            <TR>Click the link below to reset your password. Please note that the link will expire in 2 days.</TR>
                            <TR>'.$link.'</TR>
                            <TR></TR>
                            <TR>If you did not request a password change please inform S4 immediately.</TR>
                            <TR></TR>
                            <TR>Kind Regards</TR>
                            <TR>S4 Integration</TR>
                          </TABLE>';  

        SendMailHTML($row['Staff_email'], 'Intranet Details Update', $contents2 , $contents2html);
        echo $link;
    }
    
    else{
        echo 'Username not found';
    }
    
    
}


else if(isset($_POST['Code']) && isset($_POST['ID'])){
    
    $resultSet = ExecuteQuery('SELECT * FROM Forgot_Password WHERE Id = '.$_POST['ID'].' AND Code = "'.$_POST['Code'].'"');
    $row = MySQL_Fetch_Array($resultSet);
    
    if(mysql_num_rows($resultSet) != 0 ){        
        
       $thirtyDays = Date('Y-m-d', MkTime(0, 0, 0, Date("m") , Date("d") - 2, Date("Y"))); 
       if($row['Is_Used'] == 1){
           echo 'used'; 
       }
       
       else if($row['Date_Created'] >= $thirtyDays ){
           echo $row['Staff_Code'];  
       }
       
       else {
           echo 'expired'; 
       }
    }
    else{
        echo 'noMatch';
       //echo 'SELECT * FROM Forgot_Password WHERE Id = '.$_POST['ID'].' AND Code = "'.$_POST['Code'].'"';
    }
    
}

else if(isset($_POST['Password']) && isset($_POST['Staff_Code']) && isset($_POST['ID'])){
    
   if(ExecuteQuery('UPDATE Staff SET Staff_Password = old_password("'.$_POST['Password'].'") WHERE Staff_Code = "'.$_POST['Staff_Code'].'"')){
       if(ExecuteQuery('UPDATE `Forgot_Password` SET Is_Used= 1 WHERE `Id` = '.$_POST['ID'])){
            echo 'changed';
       }
   }
   else{
         echo 'failed';
   }
    
}


function getRandomCode(){
     $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';

    for ($i = 0; $i < 8; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}
