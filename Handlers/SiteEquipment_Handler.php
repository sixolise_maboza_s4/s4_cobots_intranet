<?php
  // DETAILS ///////////////////////////////////////////////////////////////////
  //                                                                          //
  //                    Last Edited By: Gareth Ambrose                        //
  //                        Date: 23 January 2008                             //
  //                                                                          //
  //////////////////////////////////////////////////////////////////////////////
  // This page handles the back-end for the Site Equipment page.              //
  //////////////////////////////////////////////////////////////////////////////
  
  include '../Scripts/Include.php';
  SetSettings();
  CheckLoggedIn();
  $_POST = Replace('"', '\'\'', $_POST);
  
  switch ($_POST['Type'])
  {
    //User has submitted information for new site equipment.
    case 'Add':
      HandleAdd();
    	break;
    //User has submitted modified information for site equipment.
    case 'Edit':
      HandleEdit();
    	break;
    //User has selected to Add, Edit or Remove site equipment.
    case 'Maintain':
      HandleMaintain();
    	break;
    //User has selected to Remove site equipment.
    case 'Remove':
      HandleRemove();
    	break;
    //User has selected to view individual site equipment.
    case 'ViewSingle':
    //User has selected to view site equipment.
    case 'ViewList':
      HandleView();
    	break;
    //User has reached this page incorrectly. If they are not authorised they are redirected to the main page from the Site Equipment page.
    default:
    	break;
  }
  Header('Location: ../SiteEquipment.php?'.Rand());
  
  //////////////////////////////////////////////////////////////////////////////
  // Checks that all the required fields have values and that these values    //
  // are valid.                                                               //
  //////////////////////////////////////////////////////////////////////////////
  function CheckFields()
  {
    switch ($_POST['Type'])
    {
      case 'Add':
      case 'Edit':
        if (($_POST['Responsible'] == "") || ($_POST['SerialNumber'] == "") || ($_POST['Site'] == "") || ($_POST['Description'] == ""))
          return false;
        
        if (!(CheckDate($_POST['Month'], $_POST['Day'], $_POST['Year'])))
          return false;
        break;
      default:
        return false;
        break;
    }
    
    return true;
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Handles the user's submission of information for new site equipment.     //
  //////////////////////////////////////////////////////////////////////////////
  function HandleAdd()
  {
    $_SESSION['AddSiteEquipment'][0] = $_POST['Responsible'];
    $_SESSION['AddSiteEquipment'][1] = $_POST['Description'];
    $_SESSION['AddSiteEquipment'][2] = $_POST['Site'];
    $_SESSION['AddSiteEquipment'][3] = $_POST['Year'].$_POST['Month'].$_POST['Day'];
    $_SESSION['AddSiteEquipment'][4] = $_POST['SerialNumber'];
  
    switch ($_POST['Submit'])
    {
      case 'Cancel':
        Session_Unregister('AddSiteEquipment');
        break;
      case 'Submit':
        if (CheckFields())
        {
          if (ExecuteQuery('INSERT INTO SiteEquipment VALUES("","'.$_POST['Responsible'].'","'.GetDatabaseDate($_POST['Day'], $_POST['Month'], $_POST['Year']).'","'.$_POST['SerialNumber'].'","'.$_POST['Description'].'","'.$_POST['Site'].'")'))
    	    {
            $_SESSION['SiteEquipmentSuccess'] = 'geh!';
            Session_Unregister('AddSiteEquipment');
          } else
            $_SESSION['SiteEquipmentFail'] = 'geh!';
        } else
          $_SESSION['SiteEquipmentIncomplete'] = 'geh!';
        break;
      default:
        break;
    }
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Handles the user's submission of modified information for site equipment //
  //////////////////////////////////////////////////////////////////////////////
  function HandleEdit()
  {
    $_SESSION['EditSiteEquipment'][1] = $_POST['Responsible'];
    $_SESSION['EditSiteEquipment'][2] = $_POST['Description'];
    $_SESSION['EditSiteEquipment'][3] = $_POST['Site'];
    $_SESSION['EditSiteEquipment'][4] = $_POST['Year'].$_POST['Month'].$_POST['Day'];
    $_SESSION['EditSiteEquipment'][5] = $_POST['SerialNumber'];
  
    switch ($_POST['Submit'])
    {
      case 'Cancel':
        Session_Unregister('EditSiteEquipment');
        break;
      case 'Submit':
        if (CheckFields())
        {
          if (ExecuteQuery('UPDATE SiteEquipment SET SiteEquipment_Responsible = "'.$_POST['Responsible'].'", SiteEquipment_Description = "'.$_POST['Description'].'", SiteEquipment_SerialNumber = "'.$_POST['SerialNumber'].'", SiteEquipment_Site = "'.$_POST['Site'].'", SiteEquipment_Date_Taken = "'.GetDatabaseDate($_POST['Day'], $_POST['Month'], $_POST['Year']).'" WHERE SiteEquipment_ID = "'.$_SESSION['EditSiteEquipment'][0].'"'))
    	    {
            $_SESSION['SiteEquipmentSuccess'] = 'geh!';
            Session_Unregister('EditSiteEquipment');
          } else
            $_SESSION['SiteEquipmentFail'] = 'geh!';
        } else
          $_SESSION['SiteEquipmentIncomplete'] = 'geh!';
        break;
      default:
        break;
    }
  }
  
  ////////////////////////////////////////////////////////////////////////////// 
  // Handles the user's maintenance selection.                                //
  ////////////////////////////////////////////////////////////////////////////// 
  function HandleMaintain()
  {
    switch ($_POST['Submit'])
    {
      case 'Add':
        $_SESSION['AddSiteEquipment'] = array($_SESSION['cUID']);
        break;
      case 'Edit':
        if ($_POST['EditSiteEquipment'] == "")
          $_SESSION['SiteEquipmentIncomplete'] = 'geh!'; 
        else
        {
          $row = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM SiteEquipment WHERE SiteEquipment_ID = '.$_POST['EditSiteEquipment'].''));
          $_SESSION['EditSiteEquipment'] = array($_POST['EditSiteEquipment']);
          $_SESSION['EditSiteEquipment'][1] = $row['SiteEquipment_Responsible'];
          $_SESSION['EditSiteEquipment'][2] = $row['SiteEquipment_Description'];
          $_SESSION['EditSiteEquipment'][3] = $row['SiteEquipment_Site'];
          $_SESSION['EditSiteEquipment'][4] = GetSessionDateFromDatabaseDate($row['SiteEquipment_Date_Taken']);
          $_SESSION['EditSiteEquipment'][5] = $row['SiteEquipment_SerialNumber'];
        }
        break;
      case 'Remove':
        if ($_POST['RemoveSiteEquipment'] == "")
          $_SESSION['SiteEquipmentIncomplete'] = 'geh!';
        else
          $_SESSION['RemoveSiteEquipment'] = array($_POST['RemoveSiteEquipment']);
        break;
      default:
        break;
    }
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Handles the user's submission to remove site equipment.                  //
  //////////////////////////////////////////////////////////////////////////////
  function HandleRemove()
  {
    switch ($_POST['Submit'])
    {
      case 'No':
        Session_Unregister('RemoveSiteEquipment');
        break;
      case 'Yes':
        if (ExecuteQuery('DELETE FROM SiteEquipment WHERE SiteEquipment_ID ="'.$_SESSION['RemoveSiteEquipment'][0].'"'))
  	    {
          $_SESSION['SiteEquipmentSuccess'] = 'geh!';
          Session_Unregister('RemoveSiteEquipment');
        } else
          $_SESSION['SiteEquipmentFail'] = 'geh!';
        break;
      default:
        break;
    }
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Handles the user's submission of a site equipment selection.             //
  //////////////////////////////////////////////////////////////////////////////
  function HandleView()
  {
    switch ($_POST['Type'])
    {
      case 'ViewSingle':
        if ($_POST['SiteEquipment'] == "")
          $_SESSION['SiteEquipmentIncomplete'] = 'geh!';
        else
          $_SESSION['ViewSiteEquipmentSingle'] = array($_POST['SiteEquipment']);
        break;
      case 'ViewList':
          $_SESSION['ViewSiteEquipmentList'] = array($_POST['SiteEquipment']);
        break;
      default:
        break;
    }
  }
?>