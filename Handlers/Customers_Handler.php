<?php
  // DETAILS ///////////////////////////////////////////////////////////////////
  //                                                                          //
  //                    Last Edited By: Gareth Ambrose                        //
  //                        Date: 18 February 2008                            //
  //                                                                          //
  //////////////////////////////////////////////////////////////////////////////
  // This page handles the back-end for the Maintain Customers page.          //
  //////////////////////////////////////////////////////////////////////////////
  
  include '../Scripts/Include.php';
  SetSettings();
  CheckLoggedIn();
  $_POST = Replace('"', '\'\'', $_POST);
  
  switch ($_POST['Type'])
  {
    //User has submitted information for a new customer.
    case 'Add':
      HandleAdd();
    	break;
    //User has submitted modified information for a customer.
    case 'Edit':
      HandleEdit();
    	break;
    //User has selected to Add or Edit a customer.
    case 'Maintain':
      HandleMaintain();
    	break;
    //User has selected to view a customer.
    case 'View':
      HandleView();
    	break;
    //User has reached this page incorrectly. If they are not authorised they are redirected to the main page from the Customers page.
    default:
    	break;
  }
  Header('Location: ../Customers.php?'.Rand());
  
  //////////////////////////////////////////////////////////////////////////////
  // Checks that all the required fields have values and that these values    //
  // are valid.                                                               //
  //////////////////////////////////////////////////////////////////////////////
  function CheckFields()
  {
    switch ($_POST['Type'])
    {
      case 'Add':
        if ($_POST['Prefix'] == "")
          return false;   
      case 'Edit':
        if (($_POST['CompanyName'] == "") || ($_POST['Description'] == "") || ($_POST['Contact'] == "") || ($_POST['Valid'] == "") || ($_POST['Delivery'] == ""))
          return false; 
        break;
      default:
        return false;
        break;
    }
    return true;
  }
  
  function insertPaymentTerms($paymentArray,$customerID)
  {
      for($i=0; $i < sizeof($paymentArray); $i++){
         ExecuteQuery('INSERT INTO CustomerPaymentTerms(`CustomerPaymentTerms_Name`,`CustomerPaymentTerms_Value`,`CustomerPaymentTerms_Customer`) VALUES("'.$paymentArray[$i][0].'", "'.$paymentArray[$i][1].'","'.$customerID.'")'); 
      }
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Handles the user's submission of information for a new customer.         //
  //////////////////////////////////////////////////////////////////////////////
  function HandleAdd()
  {
    $_SESSION['AddCustomer'][22] = array();
    foreach ($_SESSION['AddCustomer'][21] as $i)
        $_SESSION['AddCustomer'][22][] =  array($_POST['text'.$i],$_POST['text'.$i.'Value']);
  
      
    $_SESSION['AddCustomer'][0] = $_POST['CompanyName'];
    $_SESSION['AddCustomer'][1] = $_POST['Description'];
    $_SESSION['AddCustomer'][2] = $_POST['Payment'];
    $_SESSION['AddCustomer'][3] = $_POST['Account'];
    $_SESSION['AddCustomer'][4] = $_POST['Prefix'];
    $_SESSION['AddCustomer'][5] = $_POST['Contact'];
    $_SESSION['AddCustomer'][6] = $_POST['ContactWork'];
    $_SESSION['AddCustomer'][7] = $_POST['Email'];
    $_SESSION['AddCustomer'][8] = $_POST['ContactFax'];
    $_SESSION['AddCustomer'][9] = $_POST['PhysicalStreet1'];
    $_SESSION['AddCustomer'][10] = $_POST['PhysicalTownCity'];
    $_SESSION['AddCustomer'][11] = $_POST['PhysicalStreet2'];
    $_SESSION['AddCustomer'][12] = $_POST['PhysicalCountry'];
    $_SESSION['AddCustomer'][13] = $_POST['PhysicalSuburb'];
    $_SESSION['AddCustomer'][14] = $_POST['PhysicalPostalCode'];
    $_SESSION['AddCustomer'][15] = $_POST['PostalStreet1'];
    $_SESSION['AddCustomer'][16] = $_POST['PostalTownCity'];
    $_SESSION['AddCustomer'][17] = $_POST['PostalStreet2'];
    $_SESSION['AddCustomer'][18] = $_POST['PostalCountry'];
    $_SESSION['AddCustomer'][19] = $_POST['PostalSuburb'];
    $_SESSION['AddCustomer'][20] = $_POST['PostalPostalCode']; 
    $_SESSION['AddCustomer'][23] = $_POST['conditions'];
    $_SESSION['AddCustomer'][24] = $_POST['DaysFromInvoice'];
    $_SESSION['AddCustomer'][25] = $_POST['Valid'];
    $_SESSION['AddCustomer'][26] = $_POST['Delivery'];
    $_SESSION['AddCustomer'][27] = $_POST['InvoiceStatement'];
    $_SESSION['AddCustomer'][28] = $_POST['TermsCon'];
    
    switch ($_POST['Submit'])
    {
      case 'Cancel':
        Session_Unregister('AddCustomer');
        break;
      case 'Submit':
        if (CheckFields())
        {
          if (ExecuteQuery('INSERT INTO Customer VALUES("", "'.$_POST['CompanyName'].'", "'.$_POST['ContactWork'].'", "'.$_POST['ContactFax'].'", "'.$_POST['Email'].'", "'.$_POST['Contact'].'", "'.$_POST['PhysicalStreet1'].'", "'.$_POST['PhysicalSuburb'].'", "'.$_POST['PhysicalTownCity'].'", "'.$_POST['PhysicalCountry'].'", "'.$_POST['PhysicalPostalCode'].'", "'.$_POST['Payment'].'", "", "'.$_POST['PhysicalStreet2'].'", "'.$_POST['Description'].'", "'.$_POST['Account'].'", "'.$_POST['PostalStreet1'].'", "'.$_POST['PostalStreet2'].'", "'.$_POST['PostalSuburb'].'", "'.$_POST['PostalTownCity'].'", "'.$_POST['PostalCountry'].'", "'.$_POST['PostalPostalCode'].'", "'.$_POST['Prefix'].'","'.$_POST['conditions'].'","'.$_POST['DaysFromInvoice'].'","'.$_POST['Valid'].'","'.$_POST['Delivery'].'","'.$_POST['InvoiceStatement'].'","'.$_POST['TermsCon'].'")'))
  	      {
                $rowCust = MySQL_Fetch_Array(ExecuteQuery('SELECT Customer_Code FROM Customer WHERE Customer_Name = "'.$_POST['CompanyName'].'"'));
                insertPaymentTerms($_SESSION['AddCustomer'][22],$rowCust['Customer_Code']);
                //ExecuteQuery('INSERT INTO CustomerTermsConditions(`CustomerTermsConditions_Desc`,`CustomerTermsConditions_Customer`) VALUES("'.$_POST['conditions'].'","'.$rowCust['Customer_Code'].'")'); 
              
            if ($_SESSION['cUID'] != '32')
            {
              $row = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = '.$_SESSION['cUID'].''));
              
              $email = 'Customer details have been added. The details are as follows:'.Chr(10).
                       'CUSTOMER:              '.$_POST['CompanyName'].Chr(10).
                       'ADDED BY:              '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].Chr(10).Chr(10).
                       'Please review the new details to ensure that everything is in order.';
              $html = 'Customer details have been added. The details are as follows:
                      <BR /><BR />
                      <TABLE border=0>
                        <TR><TD><B>Customer:</B></TD><TD>'.$_POST['CompanyName'].'</TD></TR>
                        <TR><TD><B>Added By:</B></TD><TD>'.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].'</TD></TR>
                      </TABLE>
                      <BR />
                      Please review the new details to ensure that everything is in order.
                      <BR /><BR />';
              
              SendMailHTML('cherie@s4integration.co.za', 'Customer Added', $email, $html);
            }
            $_SESSION['CustomerSuccess'] = 'geh!';
            Session_Unregister('AddCustomer');
          } else
    	      $_SESSION['CustomerFail'] = 'geh!';
        } else
          $_SESSION['CustomerIncomplete'] = 'geh!';
        break;
      default:
        break;
    }
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Handles the user's submission of modified information for a customer.    //
  //////////////////////////////////////////////////////////////////////////////
  function HandleEdit()
  {
    $_SESSION['EditCustomer'][22] = array();
    foreach ($_SESSION['EditCustomer'][21] as $i)
        $_SESSION['EditCustomer'][22][] =  array($_POST['text'.$i],$_POST['text'.$i.'Value']); 
      
    $_SESSION['EditCustomer'][1] = $_POST['CompanyName'];
    $_SESSION['EditCustomer'][2] = $_POST['Description'];
    $_SESSION['EditCustomer'][3] = $_POST['Payment'];
    $_SESSION['EditCustomer'][4] = $_POST['Account'];
    $_SESSION['EditCustomer'][5] = $_POST['Contact'];
    $_SESSION['EditCustomer'][6] = $_POST['ContactWork'];
    $_SESSION['EditCustomer'][7] = $_POST['Email'];
    $_SESSION['EditCustomer'][8] = $_POST['ContactFax'];
    $_SESSION['EditCustomer'][9] = $_POST['PhysicalStreet1'];
    $_SESSION['EditCustomer'][10] = $_POST['PhysicalTownCity'];
    $_SESSION['EditCustomer'][11] = $_POST['PhysicalStreet2'];
    $_SESSION['EditCustomer'][12] = $_POST['PhysicalCountry'];
    $_SESSION['EditCustomer'][13] = $_POST['PhysicalSuburb'];
    $_SESSION['EditCustomer'][14] = $_POST['PhysicalPostalCode'];
    $_SESSION['EditCustomer'][15] = $_POST['PostalStreet1'];
    $_SESSION['EditCustomer'][16] = $_POST['PostalTownCity'];
    $_SESSION['EditCustomer'][17] = $_POST['PostalStreet2'];
    $_SESSION['EditCustomer'][18] = $_POST['PostalCountry'];
    $_SESSION['EditCustomer'][19] = $_POST['PostalSuburb'];
    $_SESSION['EditCustomer'][20] = $_POST['PostalPostalCode'];
    $_SESSION['EditCustomer'][23] = $_POST['conditions'];
    $_SESSION['EditCustomer'][24] = $_POST['DaysFromInvoice'];
    $_SESSION['EditCustomer'][25] = $_POST['Valid'];
    $_SESSION['EditCustomer'][26] = $_POST['Delivery'];
    $_SESSION['EditCustomer'][27] = $_POST['InvoiceStatement'];
    $_SESSION['EditCustomer'][28] = $_POST['TermsCon'];
    
    switch ($_POST['Submit'])
    {
      case 'Cancel':
        Session_Unregister('EditCustomer');
        break;
      case 'Submit':
        if (CheckFields())
        {
          if (ExecuteQuery('UPDATE Customer SET Customer_Name = "'.$_POST['CompanyName'].'", Customer_Phone = "'.$_POST['ContactWork'].'", Customer_Fax = "'.$_POST['ContactFax'].'", Customer_Email = "'.$_POST['Email'].'", Customer_Contact = "'.$_POST['Contact'].'", Customer_Address_Street = "'.$_POST['PhysicalStreet1'].'", Customer_Address_Suburb = "'.$_POST['PhysicalSuburb'].'", Customer_Address_City = "'.$_POST['PhysicalTownCity'].'", Customer_Address_Country = "'.$_POST['PhysicalCountry'].'", Customer_Address_Code = "'.$_POST['PhysicalPostalCode'].'", Customer_Payment_Method = "'.$_POST['Payment'].'", Customer_Address_Street2 = "'.$_POST['PhysicalStreet2'].'", Customer_Information = "'.$_POST['Description'].'", Customer_Account = "'.$_POST['Account'].'", Customer_Postal_Street = "'.$_POST['PostalStreet1'].'", Customer_Postal_Street2 = "'.$_POST['PostalStreet2'].'",Customer_Postal_Suburb = "'.$_POST['PostalSuburb'].'", Customer_Postal_City = "'.$_POST['PostalTownCity'].'", Customer_Postal_Country = "'.$_POST['PostalCountry'].'", Customer_Postal_Code = "'.$_POST['PostalPostalCode'].'", Customer_TermsConditions ="'.$_POST['conditions'].'", Customer_DaysFromInvoice = "'.$_POST['DaysFromInvoice'].'", Customer_PriceValid ="'.$_POST['Valid'].'", Customer_DeliveryDays = "'.$_POST['Delivery'].'", Customer_InvoiceStatement = "'.$_POST['InvoiceStatement'].'", Customer_tcHeader = "'.$_POST['TermsCon'].'" WHERE Customer_Code = "'.$_SESSION['EditCustomer'][0].'"'))
  	      {
                ExecuteQuery('DELETE FROM CustomerPaymentTerms WHERE CustomerPaymentTerms_Customer = '.$_SESSION['EditCustomer'][0].'');
                insertPaymentTerms($_SESSION['EditCustomer'][22],$_SESSION['EditCustomer'][0]);
                //ExecuteQuery('UPDATE CustomerTermsConditions SET `CustomerTermsConditions_Desc` = "'.$_POST['conditions'].'", WHERE `CustomerTermsConditions_Customer` = "'.$_SESSION['EditCustomer'][0].'"'); 
            if ($_SESSION['cUID'] != '32')
            {
              $row = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = '.$_SESSION['cUID'].''));
              
              $email = 'Customer details have been updated. The details are as follows:'.Chr(10).
                       'CUSTOMER:              '.$_POST['CompanyName'].Chr(10).
                       'UPDATED BY:            '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].Chr(10).Chr(10).
                       'Please review the updated details to ensure that everything is in order.';
              $html = 'Customer details have been updated. The details are as follows:
                      <BR /><BR />
                      <TABLE border=0>
                        <TR><TD><B>Customer:</B></TD><TD>'.$_POST['CompanyName'].'</TD></TR>
                        <TR><TD><B>Updated By:</B></TD><TD>'.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].'</TD></TR>
                      </TABLE>
                      <BR />
                      Please review the updated details to ensure that everything is in order.
                      <BR /><BR />';
              
              SendMailHTML('cherie@s4integration.co.za', 'Customer Updated', $email, $html);
            }
            $_SESSION['CustomerSuccess'] = 'geh!';
            Session_Unregister('EditCustomer');
          } else
    	      $_SESSION['CustomerFail'] = 'geh!';
        } else
          $_SESSION['CustomerIncomplete'] = 'geh!';
        break;
      default:
        break;
    }
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Handles the user's maintenance selection.                                //
  //////////////////////////////////////////////////////////////////////////////
  function HandleMaintain()
  {
    switch ($_POST['Submit'])
    {
      case 'Add':
        $_SESSION['AddCustomer'] = array();
        $_SESSION['AddCustomer'][6] = '+__-__-_______';
        $_SESSION['AddCustomer'][8] = '+__-__-_______';
        $_SESSION['AddCustomer'][24] = 30;
        $_SESSION['AddCustomer'][23] = 
'- Prices exclude VAT.
- Project commences on placement of an official order. 
- Delivery date is dependent on availability and workload.
- Goods remain property of S4 until paid in full.';
        $_SESSION['AddCustomer'][25] = 10;
        $_SESSION['AddCustomer'][26] = 30;
        $_SESSION['AddCustomer'][27] = "invoice";
        $_SESSION['AddCustomer'][28] = "Our standard terms and conditions shall apply.";
        break;
      case 'Edit':
          if ($_POST['Customer'] == "")
            $_SESSION['CustomerIncomplete'] = 'geh!';
          else
          {
            $row = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Customer WHERE Customer_Code = '.$_POST['Customer'].''));
            $_SESSION['EditCustomer'] = array($_POST['Customer']);
            $_SESSION['EditCustomer'][1] = $row['Customer_Name'];
            $_SESSION['EditCustomer'][2] = $row['Customer_Information'];
            $_SESSION['EditCustomer'][3] = $row['Customer_Payment_Method'];
            $_SESSION['EditCustomer'][4] = $row['Customer_Account'];
            $_SESSION['EditCustomer'][5] = $row['Customer_Contact'];
            $_SESSION['EditCustomer'][6] = $row['Customer_Phone'];
            $_SESSION['EditCustomer'][7] = $row['Customer_Email'];
            $_SESSION['EditCustomer'][8] = $row['Customer_Fax'];
            $_SESSION['EditCustomer'][9] = $row['Customer_Address_Street'];
            $_SESSION['EditCustomer'][10] = $row['Customer_Address_City'];
            $_SESSION['EditCustomer'][11] = $row['Customer_Address_Street2'];
            $_SESSION['EditCustomer'][12] = $row['Customer_Address_Country'];
            $_SESSION['EditCustomer'][13] = $row['Customer_Address_Suburb'];
            $_SESSION['EditCustomer'][14] = $row['Customer_Address_Code'];
            $_SESSION['EditCustomer'][15] = $row['Customer_Postal_Street'];
            $_SESSION['EditCustomer'][16] = $row['Customer_Postal_City'];
            $_SESSION['EditCustomer'][17] = $row['Customer_Postal_Street2'];
            $_SESSION['EditCustomer'][18] = $row['Customer_Postal_Country'];
            $_SESSION['EditCustomer'][19] = $row['Customer_Postal_Suburb'];
            $_SESSION['EditCustomer'][20] = $row['Customer_Postal_Code'];
            $_SESSION['EditCustomer'][23] = $row['Customer_TermsConditions'];
            $_SESSION['EditCustomer'][24] = $row['Customer_DaysFromInvoice'];
            $_SESSION['EditCustomer'][25] = $row['Customer_PriceValid'];
            $_SESSION['EditCustomer'][26] = $row['Customer_DeliveryDays'];
            $_SESSION['EditCustomer'][27] = $row['Customer_InvoiceStatement'];
            $_SESSION['EditCustomer'][28] = $row['Customer_tcHeader'];
            
            $resultSet = ExecuteQuery('SELECT * FROM CustomerPaymentTerms WHERE CustomerPaymentTerms_Customer = "'.$_POST['Customer'].'" ORDER BY CustomerPaymentTerms_ID');
                while ($rowP = MySQL_Fetch_Array($resultSet))
                   $_SESSION['EditCustomer'][22][] = array($rowP['CustomerPaymentTerms_Name'], $rowP['CustomerPaymentTerms_Value']);    
            
            /**$rowP = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM CustomerTermsConditions WHERE CustomerTermsConditions_Customer = "'.$_POST['Customer'].'"'));
            $_SESSION['EditCustomer'][23]= $rowP['CustomerPaymentTerms_Name'];**/
            if(!$_SESSION['EditCustomer'][23])
            {
                 $_SESSION['EditCustomer'][23] = 
'- Prices exclude VAT.
- Project commences on placement of an official order. 
- Delivery date is dependent on availability and workload.
- Goods remain property of S4 until paid in full.
';
            }
          }
        break;
      default:
        break;
    }
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Handles the user's submission of a customer selection.                   //
  //////////////////////////////////////////////////////////////////////////////
  function HandleView()
  {
    if ($_POST['Customer'] == "")
      $_SESSION['CustomerIncomplete'] = 'geh!';
    else{
      $_SESSION['ViewCustomer'] = array($_POST['Customer']);
      
      $_SESSION['ViewCustomer'][1] = array();
      $resultSet = ExecuteQuery('SELECT * FROM CustomerPaymentTerms WHERE CustomerPaymentTerms_Customer = "'.$_POST['Customer'].'" ORDER BY CustomerPaymentTerms_ID');
      if (MySQL_Num_Rows($resultSet) == 0)
          $_SESSION['ViewCustomer'][1] = array(array("On Commissioning", 100), array("On Delivery", 0), array("On Order", 0));
      else
          while($rowP = MySQL_Fetch_Array($resultSet))
              $_SESSION['ViewCustomer'][1][] = array($rowP['CustomerPaymentTerms_Name'], $rowP['CustomerPaymentTerms_Value']);
      
      $_SESSION['ViewCustomer'][2] = 
'- Prices exclude VAT.
- Project commences on placement of an official order. 
- Delivery date is dependent on availability and workload.
- Goods remain property of S4 until paid in full.
';
     

    }
  }
?>