<?php 
//////////////////////////////////////////////////////////////////////////////
  // This page allows users to manage and view suplpiers.                     //
  //////////////////////////////////////////////////////////////////////////////
  
  include 'Scripts/Include.php';
  SetSettings();
  CheckAuthorisation('Suppliers.php');
  error_reporting(~0); ini_set('display_errors', 1);
  error_reporting(E_ERROR);
  
  //////////////////////////////////////////////////////////////////////////////
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3c.org/TR/1999/REC-html401-19991224/loose.dtd">
<HTML>
  <HEAD>
    <?php
      // PHP SCRIPT ////////////////////////////////////////////////////////////
      BuildHead('Suppliers');
    include ('Scripts/header.php');
      //////////////////////////////////////////////////////////////////////////
    ?>

         
        <script>
  	$(document).ready(function() {
	    $(".seltest").chosen({
		no_results_text: "No results matched"
            });
		$(".datepicker").datepicker({ 
			dateFormat: 'yy-mm-dd', 
			showOn: "button",
			buttonImage: "Images/calendar.jpg",
			buttonImageOnly: true
		});

        $("#supplierStatus").on('click',function () {
            $(this).val(this.checked ? 1 : 0);
            console.log($(this).val());
        });
 	});

       </script>
       <STYLE type="text/css">
           
           
           #ERRERS{
               display:none;
               margin-bottom: 1em;
           }
             #DCSuppliers_dxDataGrid{
                width: 80%;
                margin: auto;
            }
            
            #buttonCancle{
                width: 80%;
                margin: auto;
                text-align: right;
            }
            
           
            #gridContainerSupplier{
                  font-size: 12px;
                  font-weight: bold;
                  width: 80%;
                  margin: auto;
            }
            table{
                  border:none;
            } 
      
            img{
                border: none;
                margin-bottom: -5px;
            }
           
            #Containergrid, #lastLine{
                width: 80%;
                margin: auto;
            } 
            
            #lastLine{
                padding-top: 2em;
            }
            
            #heading{
               padding:2em; 
            }
            
             .redhighlight{
                text-shadow: none !important;
                font-weight: bold;
                background-color: rgba(255, 0, 0, 0.32);
                padding-left: 0.5em;
                padding-right: 0.5em;
                width: 115px;
            }
            
            .greenhighlight{
                text-shadow: none !important;
                font-weight: bold;
                background-color: rgba(10, 148, 23, 0.32);
                padding-left: 0.5em;
                padding-right: 0.5em;
                width: 115px;
            }
            
            .yellowhighlight{
                text-shadow: none !important;
                font-weight: bold;
                background-color: rgba(245, 253, 21, 0.58);
                padding-left: 0.5em;
                padding-right: 0.5em;
                width: 115px;
            }
            
            .bluehighlight{
                text-shadow: none !important;
                font-weight: bold;
                background-color: rgba(10, 115, 254, 0.58);
                padding-left: 0.5em;
                padding-right: 0.5em;
                width: 115px;
            }
            
            </STYLE>
  </HEAD>
  <BODY> 
     
    <?php
      // PHP SCRIPT ////////////////////////////////////////////////////////////

      BuildBanner();
      //////////////////////////////////////////////////////////////////////////
    ?>
    <DIV id="main">
      <?php 
        // PHP SCRIPT //////////////////////////////////////////////////////////
      BuildTopBar();
        BuildMenu('Main', 'Suppliers.php');                               
        ////////////////////////////////////////////////////////////////////////
      ?>
        <section id="content_wrapper">
            <?php BuildBreadCrumb($currentPath);?>
            <!-- -------------- Content -------------- -->
            <section id="content" class="table-layout">
                <!-- -------------- Column Center -------------- -->
                <div class="chute chute-center" style="height: 869px;">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel">
        <?php
          // PHP SCRIPT ////////////////////////////////////////////////////////
          BuildMessageSet('Supplier');
          //////////////////////////////////////////////////////////////////////    
        ?>      
        <?php
          // PHP SCRIPT ////////////////////////////////////////////////////////  
         if (isset($_SESSION['RemoveCertificate']))
          {
              $row = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM BEE_Certificate WHERE Supplier_Code = '.$_SESSION['RemoveCertificate'][0]));
               
            
            BuildContentHeader('Remove: '.$row['File'], "", "", false);
            echo '<DIV class="contentflow">
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center" class="short">
                      <FORM method="post" action="Handlers/Suppliers_Handler.php">
                        <INPUT name="Type" type="hidden" value="Remove" />
                        <TR>
                          <TD colspan="4" class="header">Remove
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="4" align="center">Are you sure you wish to remove this file?
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="2" class="center">
                            <INPUT tabindex="1" name="Submit" type="submit" class="button" value="Yes" />   
                            <INPUT tabindex="2" name="Submit" type="submit" class="button" value="No" />                  
                          </TD>
                        </TR>
                      </FORM>
                    </TABLE>  
                  </DIV>';   
        }
        else if(isset($_SESSION['ViewSingleCertifiacte'])){
               
                 BuildContentHeader('View BEE Certificate', "", "", false);
            echo '<DIV class="contentflow">';
               if($_SESSION['ViewSingleCertifiacte'][3]=="")
               { 
                   echo '<p>There is no certificate for this supplier</p>';
               }
               else{
                   echo '<BR /><BR />
                    <TABLE cellspacing="5" align="center" class="short">
                      <FORM method="post" action="Handlers/Suppliers_Handler.php">
                        <INPUT name="Type" type="hidden" value="ViewBEESingle">
                        <TR>
                          <TD colspan="4" class="header">BEE Certificate Details
                          </TD>
                        </TR>
                        <TR>
                       <TD class="short"">Supplier:
                     
                            </TD>
                            <TD>';
                               echo $_SESSION['ViewSingleCertifiacte'][3];
                      echo '</TD>
                        </TR>
                     <TR>
                            <TD>BEE level:
                            </TD>
                            <TD>';
                            echo  makeLevelGroup($_SESSION['ViewSingleCertifiacte'][1]);
                      echo '</TD>
                          <TR>                          

                          <TR>
                            <TD>Expiry date:
                            </TD>
                            <TD>';
                          echo make_date($_SESSION['ViewSingleCertifiacte'][2]);
                      echo '</TD>
                         </TR>
                          <TD>File:
                         <SPAN class="note">**
                            </SPAN>
                          </TD>
                          <TD>
                            <a tabindex="-1" href="Files/Intranet/BeeCertificate/'.$_SESSION['ViewSingleCertifiacte'][4].'">'.$_SESSION['ViewSingleCertifiacte'][4].'</a>
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="5" class="right">  
                            <INPUT tabindex="25" name="Submit" type="submit" class="button" value="Cancel" />                   
                          </TD>
                        </TR>
                      </FORM>
                    </TABLE>  
                  </DIV>  
                  <DIV>
                    <BR />
                  </DIV>';                       
            }       
             
            }
        
       
        
          else if(isset($_SESSION['ViewBEECertificate_list'])){
               echo '<div id="gridContainerSupplier"></div>'; 
          }
          else if (isset($_SESSION['AddBEECertificate']))
          {
              
            BuildContentHeader('Add BEE Certificates', "", "", false);
            echo '<DIV class="contentflow">
                    <P>Enter the details of the certificate below. Ensure that all the required information is entered before submission and that this information is valid.</P>
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center" class="short">
                      <FORM method="post" action="Handlers/Suppliers_Handler.php" enctype="multipart/form-data">
                        <INPUT name="Type" type="hidden" value="ManageBEECertificates_Add">
                        <TR>
                          <TD colspan="4" class="header">BEE Certificate Details
                          </TD>
                        </TR>
                        <TR>
                       <TD class="short"">Supplier:
                            <SPAN class="note">*
                            </SPAN>
                            </TD>
                            <TD>';
                               BuildNoCertificateSuppliers(2, 'Supplier', 'standard seltest', $_SESSION['AddBEECertificate'][0]);
                      echo '</TD>
                        </TR>
                          <TR>
                            <TD>BEE level:
                            </TD>
                            <TD>';
                             BuildInternalOrderBEElevelSelector(4, 'Level', 'standard',  $_SESSION['AddBEECertificate'][1]);
                      echo '</TD>
                         </TR>
                         <TR>';
                              BuildLabelCell('Expiry date:', 'short');
                              echo '<TD colspan="2">';
					echo "<input type='text' size='14' class='datepicker' value='".date($_SESSION['AddBEECertificate'][2])."' name='dateselected'/>";
                        echo '</TR>
                       
                        <TR>
                          <TD>File:
                          <SPAN class="note">**
                            </SPAN>
                          </TD>
                          <TD>
                              <INPUT tabindex="2" name="Upload" type="file" class="file" value="'. $_SESSION['AddBEECertificate'][3].'" />
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="5" class="center">
                            <INPUT tabindex="24" name="Submit" type="submit" class="button" value="Submit" />   
                            <INPUT tabindex="25" name="Submit" type="submit" class="button" value="Cancel" />                   
                          </TD>
                        </TR>
                      </FORM>
                    </TABLE>  
                  </DIV>  
                  <DIV>
                    <BR />
                    <SPAN class="note">*
                    </SPAN>
                    These fields are required.
                     <BR />
                     <SPAN class="note">**
                    </SPAN>
                     Files are limited to a maximum of 5MB.
                     Files must be pdf or docx format.
                  </DIV>';
                        
                  // unset($_SESSION['AddBEECerificate']);
          }
          else if (isset($_SESSION['EditBEECertificate']))
          {
              BuildContentHeader('Edit BEE Certificate - '.$_SESSION['EditBEECertificate'][3], "", "", false);
                echo '<DIV class="contentflow">
                    <P>Enter the details of the certificate below. Ensure that all the required information is entered before submission and that this information is valid.</P>
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center" class="short">
                      <FORM method="post" action="Handlers/Suppliers_Handler.php" enctype="multipart/form-data">
                        <INPUT name="Type" type="hidden" value="ManageBEECertificates_Edit">
                        <TR>
                          <TD colspan="4" class="header">BEE Certificate Details
                          </TD>
                        </TR>
                       <TR>
                            <TD>BEE level:
                            </TD>
                            <TD>';
                             BuildInternalOrderBEElevelSelector(4, 'Level', 'standard', (int)$_SESSION['EditBEECertificate'][1]);
                      echo '</TD>
                         </TR>
                         <TR>';
                              BuildLabelCell('Expiry date:', 'short');
                              echo '<TD colspan="2">';
					echo "<input type='text' size='14' class='datepicker' value='".date($_SESSION['EditBEECertificate'][2])."' name='dateselected'/>";
                        echo '</TR>
                       
                        <TR>
                          <TD>File:
                           <SPAN class="note">**
                            </SPAN>
                          </TD>
                          <TD>
                            <INPUT tabindex="2" name="Upload" type="file" class="file" value="'.$_SESSION['EditBEECertificate'][3].'" />
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="5" class="center">
                            <INPUT tabindex="24" name="Submit" type="submit" class="button" value="Submit" />   
                            <INPUT tabindex="25" name="Submit" type="submit" class="button" value="Cancel" />                   
                          </TD>
                        </TR>
                      </FORM>
                    </TABLE>  
                  </DIV>  
                  <DIV>
                    <BR />
                    <SPAN class="note">*
                    </SPAN>
                    These fields are required.
                     <BR />
                     <SPAN class="note">**
                    </SPAN>
                     Files are limited to a maximum of 5MB.
                     Files must be pdf or docx format.
                  </DIV>';
                        
                  // unset($_SESSION['AddBEECerificate']);
          }
          else if (isset($_SESSION['AddSupplier']))
          {
            BuildContentHeader('Add Supplier', "", "", false);
            echo '<DIV class="contentflow">
                    <P>Enter the details of the supplier below. Ensure that all the required information is entered before submission and that this information is valid.</P>
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center" class="long">
                      <FORM method="post" action="Handlers/Suppliers_Handler.php">
                        <INPUT name="Type" type="hidden" value="Add">
                        <TR>
                          <TD colspan="4" class="header">Company Details
                          </TD>
                        </TR>
                        <TR>
                          <TD class="short">Company Name:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>
                            <INPUT tabindex="1" name="CompanyName" type="text" class="text standard" maxlength="100" value="'.$_SESSION['AddSupplier'][0].'" />
                          </TD>
                          <TD class="short">Description of Supply:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD rowspan="4">
                            <TEXTAREA tabindex="5" name="Description" class="standard" maxlength="100">'.$_SESSION['AddSupplier'][1].'</TEXTAREA>
                          </TD>
                        </TR>
                        <TR>
                          <TD>Payment Method:
                          </TD>
                          <TD>';
                            BuildPaymentMethod(2, 'Payment', $_SESSION['AddSupplier'][2], 'standard');
                    echo '</TD>
                        </TR>
                        <TR>
                          <TD class="short">Account Number:
                          </TD>
                          <TD>
                            <INPUT tabindex="3" name="Account" type="text" class="text short" maxlength="15" value="'.$_SESSION['AddSupplier'][3].'" />
                          </TD>
                        </TR>
                        <TR>
                          <TD>Category:
                          </TD>
                          <TD>';
                            BuildSupplierCategorySelector(4, 'SupplierCategory', 'standard test', $_SESSION['AddSupplier'][20]);
                    echo '</TD>
                        </TR>    
                        <TR>   
                          <TD>Is DC Item Supplier:</TD>';
                          if($_SESSION['AddSupplier'][22]){
                            echo'<TD><input type="checkbox" name="DCSupplier" checked></TD>';
                          }
                          else{
                              echo'<TD><input type="checkbox" name="DCSupplier"></TD>';
                              
                          }
                        echo'</TR>    
                        <TR>
                          <TD colspan="4" class="header">Contact Details
                          </TD>
                        </TR>
                        <TR>
                          <TD>Contact Person:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>
                            <INPUT tabindex="8" name="Contact" type="text" class="text standard" maxlength="100" value="'.$_SESSION['AddSupplier'][4].'" />
                          </TD>
                          <TD>Contact Number (Work):
                            <SPAN class="note">**
                            </SPAN>
                          </TD>
                          <TD>
                            <INPUT tabindex="10" name="ContactWork" type="text" class="text short" value="'.$_SESSION['AddSupplier'][5].'" maxlength="15" />
                          </TD>
                        </TR>
                        <TR>
                          <TD>Email address:
                          </TD>
                          <TD>
                            <INPUT tabindex="9" name="Email" type="text" class="text standard" maxlength="50" value="'.$_SESSION['AddSupplier'][6].'" />
                          </TD>
                          <TD>Contact Number (Fax):
                            <SPAN class="note">**
                            </SPAN>
                          </TD>
                          <TD>
                            <INPUT tabindex="11" name="ContactFax" type="text" class="text short" value="'.$_SESSION['AddSupplier'][7].'" maxlength="15" />
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="4" class="header">Physical Address Details
                          </TD>
                        </TR>
                        <TR>
                          <TD>Street 1:
                          </TD>
                          <TD>
                            <INPUT tabindex="12" name="PhysicalStreet1" type="text" class="text standard" maxlength="50" value="'.$_SESSION['AddSupplier'][8].'" />
                          </TD>
                          <TD>Town / City:
                          </TD>
                          <TD>
                            <INPUT tabindex="15" name="PhysicalTownCity" type="text" class="text standard" maxlength="20" value="'.$_SESSION['AddSupplier'][9].'" />
                          </TD>
                        </TR>
                        <TR>
                          <TD>Street 2:
                          </TD>
                          <TD>
                            <INPUT tabindex="13" name="PhysicalStreet2" type="text" class="text standard" maxlength="50" value="'.$_SESSION['AddSupplier'][10].'" />
                          </TD>
                          <TD>Country:
                          </TD>
                          <TD>
                            <INPUT tabindex="16" name="PhysicalCountry" type="text" class="text standard" maxlength="20" value="'.$_SESSION['AddSupplier'][11].'" />
                          </TD>
                        </TR>
                        <TR>
                          <TD>Suburb:
                          </TD>
                          <TD>
                            <INPUT tabindex="14" name="PhysicalSuburb" type="text" class="text standard" maxlength="50" value="'.$_SESSION['AddSupplier'][12].'" />
                          </TD>
                          <TD>Postal Code:
                          </TD>
                          <TD>
                            <INPUT tabindex="17" name="PhysicalPostalCode" type="text" class="text veryshort" maxlength="6" value="'.$_SESSION['AddSupplier'][13].'" />
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="4" class="header">Postal Address Details
                          </TD>
                        </TR>
                        <TR>
                          <TD>Street 1:
                          </TD>
                          <TD>
                            <INPUT tabindex="18" name="PostalStreet1" type="text" class="text standard" maxlength="50" value="'.$_SESSION['AddSupplier'][14].'" />
                          </TD>
                          <TD>Town / City:
                          </TD>
                          <TD>
                            <INPUT tabindex="21" name="PostalTownCity" type="text" class="text standard" maxlength="20" value="'.$_SESSION['AddSupplier'][15].'" />
                          </TD>
                        </TR>
                        <TR>
                          <TD>Street 2:
                          </TD>
                          <TD>
                            <INPUT tabindex="19" name="PostalStreet2" type="text" class="text standard" maxlength="50" value="'.$_SESSION['AddSupplier'][16].'" />
                          </TD>
                          <TD>Country:
                          </TD>
                          <TD>
                            <INPUT tabindex="22" name="PostalCountry" type="text" class="text standard" maxlength="20" value="'.$_SESSION['AddSupplier'][17].'" />
                          </TD>
                        </TR>
                        <TR>
                          <TD>Suburb:
                          </TD>
                          <TD>
                            <INPUT tabindex="20" name="PostalSuburb" type="text" class="text standard" maxlength="50" value="'.$_SESSION['AddSupplier'][18].'" />
                          </TD>
                          <TD>Postal Code:
                          </TD>
                          <TD>
                            <INPUT tabindex="23" name="PostalPostalCode" type="text" class="text veryshort" maxlength="6" value="'.$_SESSION['AddSupplier'][19].'" />
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="5" class="center">
                            <INPUT tabindex="24" name="Submit" type="submit" class="button" value="Submit" />   
                            <INPUT tabindex="25" name="Submit" type="submit" class="button" value="Cancel" />                   
                          </TD>
                        </TR>
                      </FORM>
                    </TABLE>  
                  </DIV>  
                  <DIV>
                    <BR />
                    <SPAN class="note">*
                    </SPAN>
                    These fields are required.
                    <BR />
                    <SPAN class="note">**
                    </SPAN>
                     These fields must be completed as far as possible and the international number format must be used. Eg: +27-41-1234567.
                  </DIV>';
          } 
          else if (isset($_SESSION['EditSupplier']))
          {
            $row = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Supplier WHERE Supplier_Code = "'.$_SESSION['EditSupplier'][0].'"'));        		    
            BuildContentHeader('Edit Supplier - '.$row['Supplier_Name'], "", "", false);
            echo '<DIV class="contentflow">
                    <P>Once you have made changes, ensure that you submit them otherwise they will not take effect.<P>
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center" class="long">
                      <FORM method="post" action="Handlers/Suppliers_Handler.php">
                        <INPUT name="Type" type="hidden" value="Edit">
                        <TR>
                          <TD colspan="4" class="header">Company Details
                          </TD>
                        </TR>
                        <TR>
                          <TD class="short">Company Name:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>
                            <INPUT tabindex="1" name="CompanyName" type="text" class="text standard" value="'.$_SESSION['EditSupplier'][1].'" maxlength="100" />
                          </TD>
                          <TD class="short">Description of Supply:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD rowspan="4">
                            <TEXTAREA tabindex="5" name="Description" class="standard" maxlength="100">'.$_SESSION['EditSupplier'][2].'</TEXTAREA>
                          </TD>
                        </TR>
                        <TR>
                          <TD>Payment Method:
                          </TD>
                          <TD>';
                            BuildPaymentMethod(2, 'Payment', $_SESSION['EditSupplier'][3], 'standard');
                    echo '</TD>
                        </TR>
                        <TR>
                          <TD class="short">Account Number:
                          </TD>
                          <TD>
                            <INPUT tabindex="3" name="Account" type="text" class="text short" value="'.$_SESSION['EditSupplier'][4].'" maxlength="15" />
                          </TD>
                        </TR>
                        <TR>
                          <TD>Category:
                           </TD>
                          <TD>';
                            BuildSupplierCategorySelector(4, 'SupplierCategory', 'standard test', $_SESSION['EditSupplier'][21]);
                    echo '</TD>
                        </TR>
                          <TD>Is DC type Supplier:</TD>'; 
                          if($_SESSION['EditSupplier'][22]){
                            echo'<TD><input type="checkbox" name="DCSupplier" checked></TD>';
                          }
                          else{
                              echo'<TD><input type="checkbox" name="DCSupplier"></TD>';
                              
                          }
                          echo'<td>Inactive:</td>';
                          if($_SESSION['EditSupplier'][23]){
                              echo'<td><input type="checkbox" id="supplierStatus" name="SupplierIsAInactive" checked><td>';
                          }
                          else{
                              echo'<TD><input type="checkbox" id="supplierStatus" name="SupplierIsAInactive"></TD>';
                          }
                         echo'<TR>
                          <TD colspan="4" class="header">Contact Details
                          </TD>
                        </TR>
                        <TR>
                          <TD>Contact Person:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>
                            <INPUT tabindex="9" name="Contact" type="text" class="text standard" value="'.$_SESSION['EditSupplier'][5].'" maxlength="100" />
                          </TD>
                          <TD>Contact Number (Work):
                            <SPAN class="note">**
                            </SPAN>
                          </TD>
                          <TD>
                            <INPUT tabindex="11" name="ContactWork" type="text" class="text short"';                     
                            if (($_SESSION['EditSupplier'][6] == "-") || ($_SESSION['EditSupplier'][6] == "") || ($_SESSION['EditSupplier'][6] == "--"))
                              echo ' value="+__-__-_______"';
                            else
                              echo ' value="'.$_SESSION['EditSupplier'][6].'"';
                    echo ' maxlength="15" />
                          </TD>
                        </TR>
                        <TR>
                          <TD>Email address:
                          </TD>
                          <TD>
                            <INPUT tabindex="10" name="Email" type="text" class="text standard" value="'.$_SESSION['EditSupplier'][7].'" maxlength="50" />
                          </TD>
                          <TD>Contact Number (Fax):
                            <SPAN class="note">**
                            </SPAN>
                          </TD>
                          <TD>
                            <INPUT tabindex="12" name="ContactFax" type="text" class="text short"';                    
                            if (($_SESSION['EditSupplier'][8] == "-") || ($_SESSION['EditSupplier'][8] == "") || ($_SESSION['EditSupplier'][8] == "--"))
                              echo ' value="+__-__-_______"';
                            else
                              echo ' value="'.$_SESSION['EditSupplier'][8].'"';
                    echo ' maxlength="15" />
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="4" class="header">Physical Address Details
                          </TD>
                        </TR>
                        <TR>
                          <TD>Street 1:
                          </TD>
                          <TD>
                            <INPUT tabindex="13" name="PhysicalStreet1" type="text" class="text standard" value="'.$_SESSION['EditSupplier'][9].'" maxlength="50" />
                          </TD>
                          <TD>Town / City:
                          </TD>
                          <TD>
                            <INPUT tabindex="16" name="PhysicalTownCity" type="text" class="text standard" value="'.$_SESSION['EditSupplier'][10].'" maxlength="20" />
                          </TD>
                        </TR>
                        <TR>
                          <TD>Street 2:
                          </TD>
                          <TD>
                            <INPUT tabindex="14" name="PhysicalStreet2" type="text" class="text standard" value="'.$_SESSION['EditSupplier'][11].'" maxlength="50" />
                          </TD>
                          <TD>Country:
                          </TD>
                          <TD>
                            <INPUT tabindex="17" name="PhysicalCountry" type="text" class="text standard" value="'.$_SESSION['EditSupplier'][12].'" maxlength="20" />
                          </TD>
                        </TR>
                        <TR>
                          <TD>Suburb:
                          </TD>
                          <TD>
                            <INPUT tabindex="15" name="PhysicalSuburb" type="text" class="text standard" value="'.$_SESSION['EditSupplier'][13].'" maxlength="50" />
                          </TD>
                          <TD>Postal Code:
                          </TD>
                          <TD>
                            <INPUT tabindex="18" name="PhysicalPostalCode" type="text" class="text veryshort" value="'.$_SESSION['EditSupplier'][14].'" maxlength="6" />
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="4" class="header">Postal Address Details
                          </TD>
                        </TR>
                        <TR>
                          <TD>Street 1:
                          </TD>
                          <TD>
                            <INPUT tabindex="19" name="PostalStreet1" type="text" class="text standard" value="'.$_SESSION['EditSupplier'][15].'" maxlength="50" />
                          </TD>
                          <TD>Town / City:
                          </TD>
                          <TD>
                            <INPUT tabindex="22" name="PostalTownCity" type="text" class="text standard" value="'.$_SESSION['EditSupplier'][16].'" maxlength="20" />
                          </TD>
                        </TR>
                        <TR>
                          <TD>Street 2:
                          </TD>
                          <TD>
                            <INPUT tabindex="20" name="PostalStreet2" type="text" class="text standard" value="'.$_SESSION['EditSupplier'][17].'" maxlength="50" />
                          </TD>
                          <TD>Country:
                          </TD>
                          <TD>
                            <INPUT tabindex="23" name="PostalCountry" type="text" class="text standard" value="'.$_SESSION['EditSupplier'][18].'" maxlength="20" />
                          </TD>
                        </TR>
                        <TR>
                          <TD>Suburb:
                          </TD>
                          <TD>
                            <INPUT tabindex="21" name="PostalSuburb" type="text" class="text standard" value="'.$_SESSION['EditSupplier'][19].'" maxlength="50" />
                          </TD>
                          <TD>Postal Code:
                          </TD>
                          <TD>
                            <INPUT tabindex="24" name="PostalPostalCode" type="text" class="text veryshort" value="'.$_SESSION['EditSupplier'][20].'" maxlength="6" />
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="5" class="center">
                            <INPUT tabindex="26" name="Submit" type="submit" class="button" value="Submit" />   
                            <INPUT tabindex="27" name="Submit" type="submit" class="button" value="Cancel" />                    
                          </TD>
                        </TR>
                      </FORM>
                    </TABLE>  
                  </DIV>  
                  <DIV>
                    <BR />
                    <SPAN class="note">*
                    </SPAN>
                    These fields are required.
                    <BR />
                    <SPAN class="note">**
                    </SPAN>
                     These fields must be completed as far as possible and the international number format must be used. Eg: +27-41-1234567.
                  </DIV>';
          } 
          else if (isset($_SESSION['ViewSupplierList']))
          {
            if ($_SESSION['ViewSupplierList'][0])
            {
              $loRowSupplierCategory = MySQL_Fetch_Array(ExecuteQuery('SELECT SC.Name FROM SupplierCategory SC WHERE (SC.Supplier_Category_ID = "'.$_SESSION['ViewSupplierList'][0].'")'));
              
              $loSupplierCategoryJoin = ' WHERE (S.Supplier_Category_ID = "'.$_SESSION['ViewSupplierList'][0].'")';
              $loSupplierCategoryTitle = ' - '.$loRowSupplierCategory['Name'];
            } else
              $loSupplierCategoryTitle = ' - All';
            
            $resultSet = ExecuteQuery('SELECT S.* FROM Supplier S'.$loSupplierCategoryJoin.' ORDER BY S.Supplier_Name ASC');
            BuildContentHeader('View Suppliers'.$loSupplierCategoryTitle, "", "", true);
            if (MySQL_Num_Rows($resultSet) > 0)
            {
              echo '<DIV class="contentflow">
                      <P>These are the suppliers listed.</P>
                      <BR /><BR />
                      <TABLE cellspacing="5" align="center" class="long">
                        <TR>
                          <TD colspan="6" class="header">Supplier Details
                          </TD>
                        </TR>
                        <TR>
                          <TD class="subheader">Name
                          </TD>
                          <TD class="subheader">Description
                          </TD>
                          <TD class="subheader">Contact Person
                          </TD>
                          <TD class="subheader">Telephone
                          </TD>
                          <TD class="subheader">Fax
                          </TD>
                          <TD class="subheader">Email
                          </TD>
                        </TR>';             
                        $count = 0;                          
                        while ($row = MySQL_Fetch_Array($resultSet))
                        { 
                          if ($count % 2 == 0)
                            $colour = 'rowA';
                          else
                            $colour = 'rowB';  
                          
                          echo '<TD class="'.$colour.' vershort">'.$row['Supplier_Name'].'
                                </TD>
                                <TD class="'.$colour.' vershort">'.$row['Supplier_Information'].'
                                </TD>
                                <TD class="'.$colour.' small">'.$row['Supplier_Contact'].'
                                </TD>
                                <TD class="'.$colour.'">'.$row['Supplier_Phone'].'
                                </TD>
                                <TD class="'.$colour.'">'.$row['Supplier_Fax'].'
                                </TD>
                                <TD class="'.$colour.' small">'.$row['Supplier_Email'].'
                                </TD>
                              </TR>';                         
                          $count++;
                        }
                echo '</TABLE> 
                    </DIV>';                  
            } else
              echo '<DIV class="contentflow">
                      <P>There are no suppliers listed.</P>
                      <BR /><BR />
                    </DIV>';
            Session_Unregister('ViewSupplierList');
          } 
          else if (isset($_SESSION['ViewSupplierSingle']))
          {
            $row = MySQL_Fetch_Array(ExecuteQuery('SELECT S.* FROM Supplier S WHERE (S.Supplier_Code = "'.$_SESSION['ViewSupplierSingle'][0].'")'));
            BuildContentHeader('View Supplier - '.$row['Supplier_Name'], "", "", false);
            echo '<DIV class="contentflow">
                    <P>These are the supplier details.<P>
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center" class="long">';
                    if ($_SESSION['cAuth'] & 16)
                    {
                      echo '<TR>
                              <TD colspan="4" class="header">Company Details
                              </TD>
                            </TR>
                            <TR>
                              <TD>Payment Method:
                              </TD>
                              <TD class="bold">';
                                $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT Payment_Description FROM Payment WHERE Payment_ID = "'.$row['Supplier_Payment_Method'].'"'));
                                echo $rowTemp['Payment_Description'];                                  
                        echo '</TD>
                              <TD>Account Number:
                              </TD>
                              <TD class="bold">'.$row['Supplier_Account'].'
                              </TD>
                            </TR>
                            <TR>
                              <TD>Supplier Category:
                              </TD>
                              <TD class="bold">';
                                $loRSSupplierCategory = ExecuteQuery('SELECT SC.Name FROM SupplierCategory SC WHERE (SC.Supplier_Category_ID = "'.$row['Supplier_Category_ID'].'")');
                                if (MySQL_Num_Rows($loRSSupplierCategory))
                                {
                                  $loRowSupplierCategory = MySQL_Fetch_Array($loRSSupplierCategory);
                                  echo $loRowSupplierCategory['Name'];
                                }
                        echo '</TD>
                            <TD>Is DC Supplier: </TD><TD>';
                                if($row['Supplier_IsDC']){
                                      echo'<b>true';
                                }
                                else{
                                      echo'<b>false';
                                }
                                
                            echo'</b></TD>
                            </TR>
                            <tr>
                            <td>
                            Inactive Supplier:</td><td>';
                                if($row['Supplier_IsActive']){
                                    echo '<b>true</b>';
                                }else{
                                    echo '<b>false</b>';
                                }

echo'</td>
</tr>
                            ';
                    }
                echo '<TR>
                        <TD colspan="4" class="header">Contact Details
                        </TD>
                      </TR>
                      <TR>
                        <TD class="short">Contact Person:
                        </TD>
                        <TD class="bold">'.$row['Supplier_Contact'].'
                        </TD>
                        <TD class="short">Contact Number (Work):
                        </TD>
                        <TD class="bold">'.$row['Supplier_Phone'].'
                        </TD>
                      </TR>
                      <TR>
                        <TD>Email address:
                        </TD>
                        <TD class="bold">'.$row['Supplier_Email'].'
                        </TD>
                        <TD>Contact Number (Fax):
                        </TD>
                        <TD class="bold">'.$row['Supplier_Fax'].'
                        </TD>
                      </TR>
                      <TR>
                        <TD colspan="4" class="header">Physical Address Details
                        </TD>
                      </TR>
                      <TR>
                        <TD>Street 1:
                        </TD>
                        <TD class="bold">'.$row['Supplier_Address_Street'].'
                        </TD>
                        <TD>Town / City:
                        </TD>
                        <TD class="bold">'.$row['Supplier_Address_City'].'
                        </TD>
                      </TR>
                      <TR>
                        <TD>Street 2:
                        </TD>
                        <TD class="bold">'.$row['Supplier_Address_Street2'].'
                        </TD>
                        <TD>Country:
                        </TD>
                        <TD class="bold">'.$row['Supplier_Address_Country'].'
                        </TD>
                      </TR>
                      <TR>
                        <TD>Suburb:
                        </TD>
                        <TD class="bold">'.$row['Supplier_Address_Suburb'].'
                        </TD>
                        <TD>Postal Code:
                        </TD>
                        <TD class="bold">'.$row['Supplier_Address_Code'].'
                        </TD>
                      </TR>
                      <TR>
                        <TD colspan="4" class="header">Postal Address Details
                        </TD>
                      </TR>
                      <TR>
                        <TD>Street 1:
                        </TD>
                        <TD class="bold">'.$row['Postal_Address_Street'].'
                        </TD>
                        <TD>Town / City:
                        </TD>
                        <TD class="bold">'.$row['Postal_Address_City'].'
                        </TD>
                      </TR>
                      <TR>
                        <TD>Street 2:
                        </TD>
                        <TD class="bold">'.$row['Postal_Address_Street2'].'
                        </TD>
                        <TD>Country:
                        </TD>
                        <TD class="bold">'.$row['Postal_Address_Country'].'
                        </TD>
                      </TR>
                      <TR>
                        <TD>Suburb:
                        </TD>
                        <TD class="bold">'.$row['Postal_Address_Suburb'].'
                        </TD>
                        <TD>Postal Code:
                        </TD>
                        <TD class="bold">'.$row['Postal_Address_Code'].'
                        </TD>
                      </TR>
                    </TABLE>  
                  </DIV>';
            Session_Unregister('ViewSupplierSingle');
          } else
           if (isset($_SESSION['DCSuppliersInternalOrders']))
          {
            echo'<div id = "ERRERS">Requested operation failed. An error occurred during the submission process.</div>';   
            BuildContentHeader('DC Suppliers', "", true,true);
            echo '<DIV class="contentflow"> 
                    <P>Select the Suppliers that must be added or removed from the DC Supplier list.</P>
                    <BR /><BR />
                    <div id = "Loader" class="loader">Loading...</div>
                    <DIV id=DCSuppliers_dxDataGrid></div><div id = "buttonCancle">';
                      echo'<FORM method="post" action="Handlers/Suppliers_Handler.php">
                        <INPUT name="Type" type="hidden" value="DCSuppliers">';
                         echo' <INPUT tabindex="3" name="Submit" type="submit" class="button" value="Cancel" />';               
                  echo'  </FORM></div>';  

          }
          else
          {
            if ($_SESSION['cAuth'] & 16)
            {
              BuildContentHeader('Maintenance', "", "", true);
              echo '<DIV class="contentflow">
                      <P style="text-align: left">New suppliers can be added using the form below. Details on suppliers already listed can also be edited.</P>
                      <BR /><BR />                
                      <TABLE cellspacing="5" align="center" class="standard">
                        <FORM method="post" action="Handlers/Suppliers_Handler.php">
                          <INPUT name="Type" type="hidden" value="Maintain">
                          <TR>
                            <TD colspan="4" class="header">Add
                            </TD>
                          </TR>
                          <TR>
                            <TD colspan="3">To add a new supplier, click Add and complete the form that is displayed.
                            </TD>
                            <TD class="right">
                              <INPUT tabindex="1" name="Submit" type="submit" class="button" value="Add" />
                            </TD>
                          </TR>
                          <TR>
                            <TD colspan="4" class="header">Edit
                            </TD>
                          </TR>
                          <TR>
                            <TD colspan="4">To edit a supplier, specify the particulars and click Edit.
                            </TD>
                          </TR>
                          <TR>
                            <TD class="short"">Supplier:
                            <SPAN class="note">*
                            </SPAN>
                            </TD>
                            <TD>';
                              BuildSupplierSelector(2, 'Supplier', 'standard seltest', "");
                      echo '</TD>
                            <TD colspan="2" class="right">
                              <INPUT tabindex="3" name="Submit" type="submit" class="button" value="Edit" />                   
                            </TD>
                          </TR>';
                              if($_SESSION['cAuth'] & 32){
                               echo '<TR>
                                  <TD colspan="4" class="header">DC Suppliers
                                  </TD>
                                </TR>
                                <TR>
                                  <TD colspan="3">To add or remove a Supplier to the DC Supplier list. Click manange.
                                  </TD>
                                  <TD class="right"><INPUT tabindex="1" name="Submit" type="submit" class="button" value="Manage" /></TD>
                                </TR>';
                              
                       }

                        echo '</FORM>
                      </TABLE>
                    </DIV>';
              
              BuildContentHeader('View Suppliers', "", "", true);
            } else
              BuildContentHeader('View Suppliers', "", "", true);
            
        
 echo '<DIV class="contentflow">
            <P >Suppliers can be viewed by using the form below.</P>
            <BR /><BR />
            <TABLE cellspacing="5" align="center" class="standard">
                <FORM method="post" action="Handlers/Suppliers_Handler.php">
                    <INPUT name="Type" type="hidden" value="ViewSingle">
                        <TR>
                          <TD colspan="4" class="header">View Single
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="4">To view suppliers, specify the particulars and click View.
                          </TD>
                        </TR>
                        <TR>
                          <TD class="short">Supplier:
                            <SPAN class="note">*</SPAN>
                          </TD>
                          <TD>';
                            BuildSupplierSelector(4, 'Supplier', 'standard seltest', "");
                    echo '</TD>
                          <TD colspan="2" class="right">
                            <INPUT tabindex="5" name="Submit" type="submit" class="button" value="View" />                   
                          </TD>
                        </TR>
                </FORM>
                <FORM method="post" action="Handlers/Suppliers_Handler.php">
                    <INPUT name="Type" type="hidden" value="ViewList">
                        <TR>
                          <TD colspan="4" class="header">View List
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="4">To view suppliers, specify the particulars and click View.</TD>
                        </TR>
                        <TR>
                          <TD class="short">Category:
                          </TD>
                          <TD>';
                            BuildSupplierCategorySelector(6, 'SupplierCategory', 'standard seltest', "");
                    echo '</TD>
                          <TD colspan="2" class="right">
                            <INPUT tabindex="7" name="Submit" type="submit" class="button" value="View" />                   
                          </TD>
                        </TR>
                </FORM>
            </TABLE>
        </DIV>'; 
              
      if ($_SESSION['cAuth'] & 16)
            {               
                    
                       BuildContentHeader('Manage BEE Certificates', "", "", false);          
              echo '<DIV class="contentflow"><p>New certificates can be added or edited by using the form below.</p>'.
                      '<br></br>
                        <TABLE cellspacing="5" align="center" class="standard">
                      <FORM method="post" action="Handlers/Suppliers_Handler.php">
                        <INPUT name="Type" type="hidden" value="ManageBEECertificates">
                        <TR>
                          <TD colspan="5" class="header">Add
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="4" >To add a new certificate click Add and complete the form that is displayed.</TD>
                          <TD colspan="1" class="right">
                              <INPUT tabindex="7" name="Submit" type="submit" class="button" value="Add" />         
                          </TD>
                        </TR>
                     
                        
                      </FORM>
                      <FORM method="post" action="Handlers/Suppliers_Handler.php">
                        <INPUT name="Type" type="hidden" value="ManageBEECertificates">
                        <TR>
                          <TD colspan="5" class="header">Edit 
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="5">To edit an certificate, specify the supplier, click Edit and complete the form that is displayed.
                          </TD>
                        </TR>
                        <TR>
                          <TD class="short">Supplier:
                           <SPAN class="note" colspan="1">*</SPAN>
                          </TD>
                          <TD colspan="3">';
                     BuildCertificateSuppliers(8, 'Supplier', 'standard seltest', "");
                    echo '</TD>
                          <TD colspan="2" class="right">
                            <INPUT tabindex="7" name="Submit" type="submit" class="button" value="Edit" />                   
                          </TD>
                        </TR>
                      </FORM>
                      
                    <FORM method="post" action="Handlers/Suppliers_Handler.php">
                        <INPUT name="Type" type="hidden" value="ManageBEECertificates">
                        <TR>
                          <TD colspan="5" class="header">Remove
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="5">To remove a file, specify the supplier, click Remove and confirm when prompted.
                          </TD>
                        </TR>
                        <TR>
                          <TD class="short">Supplier:
                           <SPAN class="note" colspan="1">*</SPAN>
                          </TD>
                          <TD colspan="3">';
                        BuildCertificateSuppliers(8, 'Supplier', 'standard seltest', $_SESSION['RemoveCertificate'][0]);
                    echo '</TD>
                          <TD colspan="2" class="right">
                            <INPUT tabindex="7" name="Submit" type="submit" class="button" value="Remove" />                   
                          </TD>
                        </TR>
                      </FORM>


                         </FORM>
                        
                   
                     </TABLE>'
                            .'</DIV>';
                    BuildContentHeader('View BEE Certificates', "", "", false);          
              echo '<DIV class="contentflow"><p>Certificates can be viewed by using the form below.</p>'.
                     '<br></br>'.
                        '<TABLE cellspacing="5" align="center" class="standard">'
                      . '<FORM method="post" action="Handlers/Suppliers_Handler.php">
                        <INPUT name="Type" type="hidden" value="ViewBEESingle">
                        <TR>
                          <TD colspan="5" class="header">View Single 
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="5">To view an certificate, specify the supplier and click View.
                          </TD>
                        </TR>
                        <TR>
                          <TD class="short">Supplier:
                           <SPAN class="note" colspan="1">*</SPAN>
                          </TD>
                          <TD colspan="3">';
                        BuildCertificateSuppliers(8, 'Supplier', 'standard seltest', "");
                    echo '</TD>
                          <TD class="right">
                            <INPUT tabindex="7" name="Submit" type="submit" class="button" value="View" />                   
                          </TD>
                        </TR>
                      </FORM>
                       
                    <FORM method="post" action="Handlers/Suppliers_Handler.php">
                        <INPUT name="Type" type="hidden" value="ViewBEEList">
                        <TR>
                          <TD colspan="5" class="header">View List 
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="4">View all certificates
                          </TD>
                          <TD class = "right">
                            <INPUT id ="makeList" tabindex="7" name="Submit" type="button" class="button" value="View" />     
                          </TD>
                        </TR>
                    </FORM>            
                        <FORM method="post" action="Handlers/Suppliers_Handler.php">
                        <INPUT name="Type" type="hidden" value="Export">
                        <TR>
                          <TD colspan="5" class="header">Download
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="4">Download all the BEE certificates.
                          </TD>
                           <TD class="right"> 
                             <INPUT tabindex="7" name="Submit" type="submit" class="button" value="download" />  
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="5" class="header">Non-Certificate Suppliers 
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="4">View all Suppliers that do not have BEE certificates.
                          </TD>
                           <TD class="right"> 
                             <INPUT id ="NOCertificates" tabindex="7" name="Submit" type="button" class="button" value="View" />  
                          </TD>
                        </TR>                        
                        
                         </FORM>'
                      . '</TABLE>'
                    . '</DIV>';
            }
          }
          //////////////////////////////////////////////////////////////////////
        ?>
                            </div>
                        </div>
                    </div>
            </section
    </DIV>
    <?php
      // PHP SCRIPT ////////////////////////////////////////////////////////////
      BuildFooter();
      //////////////////////////////////////////////////////////////////////////
    ?>    

      <script src="Scripts/FileScripts/Suppliers.js" type="text/javascript"></script>
      
      
      
  </BODY>
</HTML>