<?php
//////////////////////////////////////////////////////////////////////////////
// This page allows users to manage and view internal orders.               //
//////////////////////////////////////////////////////////////////////////////

include 'Scripts/Include.php';
include 'Scripts/ReportPDF.php';
SetSettings();

//////////////////////////////////////////////////////////////////////////////
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3c.org/TR/1999/REC-html401-19991224/loose.dtd">
<HTML>
<HEAD>
    <TITLE> S4 Integration - Report</TITLE>
</HEAD>
<BODY>
<?php
// Get All Order Captured today ////////////////////////////////////////////////////////////

$today = date("Y-m-d");
$row = ExecuteQuery('SELECT CONCAT(S.Staff_First_Name, " ", S.Staff_Last_Name) as ApprovedBy, O.*  FROM `OrderNo`  O left join Staff S on S.Staff_Code =  O.OrderNo_Approved_By  WHERE DATE(OrderNo_Date_Time) = "'.$today.'"');


/*Store Squery results to array*/



if (MySQL_Num_Rows($row) > 0)
{

//    PDF Attachment
    $pdf_filename= 'Files/Temp/S4I_Internal_Order_Report_'.date('Ymd').'.pdf';
    $columnLabels = array( "Description", "Supplier Code", "Qty", "Value","Disc","Total" );
    $pdf = GeneratePdf($pdf_filename,$columnLabels,$data,$row,$rowB,$loCurrencySymbol);
    $pdf->Output($pdf_filename, "F");
    SendMailHTML("nico@s4integration.co.za", 'Internal Order Report '.date('Ymd'), $email, $html,$pdf_filename);
             SendMailHTML("andrew@s4integration.co.za", 'S4 Integration Internal Order Report '.date('Ymd'), $email, $html,$pdf_filename);
			 SendMailHTML("shawn.browne@s4integration.co.za", 'S4 Integration Internal Order Report '.date('Ymd'), $email, $html,$pdf_filename);
			 SendMailHTML("renaldo.oconnor@s4integration.co.za", 'S4 Integration Internal Order Report '.date('Ymd'), $email, $html,$pdf_filename);
			 SendMailHTML("stefan@s4integration.co.za", 'S4 Integration Internal Order Report '.date('Ymd'), $email, $html,$pdf_filename);


    unlink($pdf_filename);
}

//////////////////////////////////////////////////////////////////////////
?>
</BODY>
</HTML>