<?php
  // DETAILS ///////////////////////////////////////////////////////////////////
  //                                                                          //
  //                    Last Edited By: Gareth Ambrose                        //
  //                        Date: 06 March 2009                               //
  //                                                                          //
  //////////////////////////////////////////////////////////////////////////////
  // This page allows users to manage and view expenses.                      //
  //////////////////////////////////////////////////////////////////////////////
  
  include 'Scripts/Include.php';
  SetSettings();
  CheckAuthorisation('ReportExpenses.php');
  
  //////////////////////////////////////////////////////////////////////////////
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3c.org/TR/1999/REC-html401-19991224/loose.dtd">
<HTML>
  <HEAD>
    <?php
      // PHP SCRIPT ////////////////////////////////////////////////////////////
      BuildHead('Staff Expenses');
      include ('Scripts/header.php');
      //////////////////////////////////////////////////////////////////////////
    ?>
  </HEAD>
  <BODY>
    <?php
      // PHP SCRIPT ////////////////////////////////////////////////////////////
      BuildBanner();
      //////////////////////////////////////////////////////////////////////////
    ?>
    <DIV class="main">
      <?php
        // PHP SCRIPT //////////////////////////////////////////////////////////
      BuildTopBar();
        BuildMenu('Main', 'ReportExpenses.php');
        ////////////////////////////////////////////////////////////////////////
      ?>
        <section id="content_wrapper">
            <?php BuildBreadCrumb($currentPath);?>
            <!-- -------------- Content -------------- -->
            <section id="content" class="table-layout">
                <!-- -------------- Column Center -------------- -->
                <div class="chute chute-center" style="height: 869px;">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel">
        <?php
          // PHP SCRIPT ////////////////////////////////////////////////////////
          BuildMessageSet('ReportExpenses');
          //////////////////////////////////////////////////////////////////////
        ?>
        <?php
          // PHP SCRIPT ////////////////////////////////////////////////////////
          if (isset($_SESSION['FinaliseReportExpenses']))
          {
            BuildContentHeader('Finalise Expenses', "", "", false);  
            echo '<DIV class="contentflow">
                    <BR/><BR/>
                    <TABLE cellspacing="5" align="center" class="short">
                      <FORM method="post" action="Handlers/ReportExpenses_Handler.php">
                        <INPUT name="Type" type="hidden" value="Finalise" />
                        <TR>
                          <TD colspan="4" class="header">Finalise
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="4" class="center">Are you sure you wish to finalise expenses?
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="4" class="center">
                            <INPUT tabindex="1" name="Submit" type="submit" class="button" value="Yes" />   
                            <INPUT tabindex="2" name="Submit" type="submit" class="button" value="No" />                  
                          </TD>
                        </TR>
                      </FORM>
                    </TABLE>  
                  </DIV>';                             
          } else
          if (isset($_SESSION['PayReportExpenses']))
          {
            BuildContentHeader('Pay Expenses', "", "", false);  
            echo '<DIV class="contentflow">
                    <BR/><BR/>
                    <TABLE cellspacing="5" align="center" class="short">
                      <FORM method="post" action="Handlers/ReportExpenses_Handler.php">
                        <INPUT name="Type" type="hidden" value="Pay" />
                        <TR>
                          <TD colspan="4" class="header">Pay
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="4" class="center">Are you sure you wish to mark reviewed expenses as paid?
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="4" class="center">
                            <INPUT tabindex="1" name="Submit" type="submit" class="button" value="Yes" />   
                            <INPUT tabindex="2" name="Submit" type="submit" class="button" value="No" />                  
                          </TD>
                        </TR>
                      </FORM>
                    </TABLE>  
                  </DIV>';                             
          } else
          if (isset($_SESSION['ReviewReportExpenses'])) 
          {
            BuildContentHeader('Review Expenses', "", "", false);  
            echo '<DIV class="contentflow">
                    <BR/><BR/>
                    <TABLE cellspacing="5" align="center" class="short">
                      <FORM method="post" action="Handlers/ReportExpenses_Handler.php">
                        <INPUT name="Type" type="hidden" value="Review" />
                        <TR>
                          <TD colspan="4" class="header">Review
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="4" class="center">Are you sure you wish to mark unpaid expenses as reviewed?
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="4" class="center">
                            <INPUT tabindex="1" name="Submit" type="submit" class="button" value="Yes" />   
                            <INPUT tabindex="2" name="Submit" type="submit" class="button" value="No" />                  
                          </TD>
                        </TR>
                      </FORM>
                    </TABLE>  
                  </DIV>';                             
          } else 
          if (isset($_SESSION['ViewReportExpenses'][2]))
          {
            $startDate = GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][0]);
            $endDate = GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][1]);
            if ($_SESSION['ViewReportExpenses'][0] == $_SESSION['ViewReportExpenses'][1])
              $dateRange = GetTextualDateFromSessionDate($_SESSION['ViewReportExpenses'][0]);
            else        
              $dateRange = GetTextualDateFromSessionDate($_SESSION['ViewReportExpenses'][0]).' to '.GetTextualDateFromSessionDate($_SESSION['ViewReportExpenses'][1]);
            
            $all = false;
            switch ($_SESSION['ViewReportExpenses'][4])
            {
              case "":
                $reportType = 'All';
                $payment = ' > "-1"';
                $all = true;
                break;
              case '1':
                $reportType = 'Paid';
                $payment = ' = "1"';
                break;
              case '2':
                $reportType = 'Reviewed';
                $payment = ' = "2"';
                break;
              case '0':
                $reportType = 'Unpaid';
                $payment = ' = "0"';
                break;
            }
            
            function GetPayment($payment)
            {
              switch ($payment)
              {
                case '0':
                  return 'Unpaid';
                  break;
                case '1':
                  return 'Paid';
                  break;
                case '2':
                  return 'Reviewed';
                  break;
                default:
                  break;
              }
            }
            
            $row = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = "'.$_SESSION['ViewReportExpenses'][3].'"'));                    
            switch ($_SESSION['ViewReportExpenses'][2])
            {              
              case '0':
                switch ($_SESSION['ViewReportExpenses'][3])
                {
                  case "":
                    if ($_SESSION['ViewReportExpenses'][4] == 2)
                      BuildContentHeader('Expense Report ('.$reportType.')', "", "", false);
                    else
                      BuildContentHeader('Expense Report ('.$reportType.') - '.$dateRange, "", "", false);
                    echo '<DIV class="contentflow">
                            <P class="hide">These are the expenses listed.</P>
                            <BR/><BR/>';
                            
                    $resultSet = ExecuteQuery('SELECT * FROM Staff WHERE Staff_IsEmployee = "1" ORDER BY Staff_First_Name, Staff_Last_Name');
                    while ($row = MySQL_Fetch_Array($resultSet))
                    { 
                      echo '<TABLE cellspacing="5" align="center" class="long" style="page-break-after: always">
                              <TR>';
                                if ($all)
                                  echo '<TD colspan="6" class="header">';
                                else
                                  echo '<TD colspan="5" class="header">';
                                echo $row['Staff_First_Name'].' '.$row['Staff_Last_Name'].'
                                      </TD>
                                    </TR>';
                              $total = 0;
                              $subTotal = 0;
                              if ($_SESSION['ViewReportExpenses'][4] == 2)
                                $resultSetTemp = ExecuteQuery('SELECT * FROM Deduction WHERE Deduction_Payment '.$payment.' AND Deduction_Name = '.$row['Staff_Code'].' ORDER BY Deduction_Date');
                              else
                                $resultSetTemp = ExecuteQuery('SELECT * FROM Deduction WHERE Deduction_Payment '.$payment.' AND Deduction_Name = '.$row['Staff_Code'].' AND Deduction_Date BETWEEN "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][0]).'" AND "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][1]).' 23:59:59" ORDER BY Deduction_Date');
                              if (MySQL_Num_Rows($resultSetTemp) > 0)
                              {
                                if ($all)
                                  echo '<TD colspan="6" class="subheaderclear">Deductions
                                        </TD>
                                      </TR>
                                      <TR>
                                        <TD class="subheader veryshort">Payment
                                        </TD>';
                                else
                                  echo '<TD colspan="5" class="subheaderclear">Deductions
                                        </TD>
                                      </TR>
                                      <TR>';
                                  echo '<TD class="subheader veryshort">Date
                                        </TD>
                                        <TD colspan="3" class="subheader">Description
                                        </TD>
                                        <TD class="subheader tiny">Value
                                        </TD>
                                      </TR>';      
                                      while ($rowTemp = MySQL_Fetch_Array($resultSetTemp))
                                      {             
                                        echo '<TR>';
                                          if ($all)
                                          echo '<TD class="rowA center">'.GetPayment($rowTemp['Deduction_Payment']).'
                                                </TD>';
                                          echo '<TD class="rowA">'.GetTextualDateFromDatabaseDate($rowTemp['Deduction_Date']).'
                                                </TD>
                                                <TD colspan="3" class="rowA">'.$rowTemp['Deduction_Description'].'
                                                </TD>
                                                <TD class="rowA center">R'.SPrintF('%02.2f', -$rowTemp['Deduction_Value']).'
                                                </TD>
                                              </TR>';     
                                        //$total -= $rowTemp['Deduction_Value'];
                                        $subTotal -= $rowTemp['Deduction_Value'];
                                      }
                              } else
                                if ($all)
                                  echo '<TD colspan="6" class="subheader">Deductions
                                        </TD>
                                      </TR>';
                                else
                                  echo '<TD colspan="5" class="subheader">Deductions
                                        </TD>
                                      </TR>';
                              if ($all)
                                echo '<TR>
                                        <TD colspan="5" class="rowB bold">Total
                                        </TD>';
                              else
                                echo '<TR>
                                        <TD colspan="4" class="rowB bold">Total
                                        </TD>';
                          echo '<TD class="rowB center bold">R'.SPrintF('%02.2f', $subTotal).'
                                </TD>
                              </TR>';
                              $subTotal = 0;
                              if ($_SESSION['ViewReportExpenses'][4] == 2)
                                $resultSetTemp = ExecuteQuery('SELECT * FROM Overtime WHERE Overtime_Payment '.$payment.' AND Overtime_Name = "'.$row['Staff_Code'].'" ORDER BY Overtime_Start'); 
                              else
                                $resultSetTemp = ExecuteQuery('SELECT * FROM Overtime WHERE Overtime_Payment '.$payment.' AND Overtime_Name = "'.$row['Staff_Code'].'" AND Overtime_Start BETWEEN "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][0]).'" AND "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][1]).' 23:59:59" ORDER BY Overtime_Start'); 
                              $rowTempB = MySQL_Fetch_Array(ExecuteQuery('SELECT SUM(OvertimeBank_Hours) AS Overtime FROM OvertimeBank WHERE OvertimeBank_Name = '.$row['Staff_Code'].' AND OvertimeBank_Start <= "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][1]).' 23:59:59" AND OvertimeBank_IsApproved = "1"'));
                              if (MySQL_Num_Rows($resultSet) > 0)
                              {
                                if ($all)
                                  echo '<TD colspan="6" class="subheaderclear">Overtime
                                        </TD>
                                      </TR>
                                      <TR>
                                        <TD class="subheader veryshort">Payment
                                        </TD>';
                                else
                                  echo '<TD colspan="5" class="subheaderclear">Overtime
                                        </TD>
                                      </TR>
                                      <TR>';
                                  echo '<TD class="subheader veryshort">Start
                                        </TD>
                                        <TD class="subheader veryshort">End
                                        </TD>
                                        <TD colspan="2" class="subheader">Description
                                        </TD>
                                        <TD class="subheader tiny">Hours
                                        </TD>
                                      </TR>';
                                      while ($rowTemp = MySQL_Fetch_Array($resultSetTemp))
                                      {
                                        echo '<TR>';
                                          if ($all)
                                          echo '<TD class="rowA center">'.GetPayment($rowTemp['Overtime_Payment']).'
                                                </TD>';
                                          echo '<TD class="rowA center">'.GetTextualDateTimeFromDatabaseDateTime($rowTemp['Overtime_Start']).'
                                                </TD>
                                                <TD class="rowA center">'.GetTextualDateTimeFromDatabaseDateTime($rowTemp['Overtime_End']).'
                                                </TD>
                                                <TD colspan="2" class="rowA">'.$rowTemp['Overtime_Description'].'
                                                </TD>
                                                <TD class="rowA center">'.SPrintF('%02.2f', $rowTemp['Overtime_Hours']).'
                                                </TD>
                                              </TR>'; 
                                        $subTotal += $rowTemp['Overtime_Hours'];
                                      }
                              } else
                                if ($all)
                                  echo '<TD colspan="6" class="subheader">Overtime
                                        </TD>
                                      </TR>';
                                else
                                  echo '<TD colspan="5" class="subheader">Overtime
                                        </TD>
                                      </TR>';
                              if ($all)
                                echo '<TR>
                                        <TD colspan="5" class="rowB bold">Total
                                        </TD>
                                        <TD class="rowB center bold">'.SPrintF('%02.2f', $subTotal).'
                                        </TD>                             
                                      </TR>
                                      <TR>
                                        <TD colspan="5" class="rowB bold">Banked Overtime Balance:
                                        </TD> ';
                              else
                                echo '<TR>
                                        <TD colspan="4" class="rowB bold">Total
                                        </TD>
                                        <TD class="rowB center bold">'.SPrintF('%02.2f', $subTotal).'
                                        </TD>                             
                                      </TR>
                                      <TR>
                                        <TD colspan="4" class="rowB bold">Banked Overtime Balance:
                                        </TD> ';
                          echo '<TD class="rowB center bold">'.SPrintF('%02.2f', $rowTempB['Overtime']).'
                                </TD>                             
                              </TR>';
                              $subTotal = 0;
                              if ($_SESSION['ViewReportExpenses'][4] == 2)
                                $resultSetTemp = ExecuteQuery('SELECT * FROM ReimbursiveAllowance WHERE ReimbursiveAllowance_Payment '.$payment.' AND ReimbursiveAllowance_Name = "'.$row['Staff_Code'].'" ORDER BY ReimbursiveAllowance_Date');
                              else
                                $resultSetTemp = ExecuteQuery('SELECT * FROM ReimbursiveAllowance WHERE ReimbursiveAllowance_Payment '.$payment.' AND ReimbursiveAllowance_Name = "'.$row['Staff_Code'].'" AND ReimbursiveAllowance_Date BETWEEN "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][0]).'" AND "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][1]).' 23:59:59" ORDER BY ReimbursiveAllowance_Date'); 
                              if (MySQL_Num_Rows($resultSetTemp) > 0)
                              {
                                echo '<TR>';
                                      if ($all)
                                        echo '<TD colspan="6" class="subheaderclear">Reimbursements
                                              </TD>
                                            </TR>
                                            <TR>
                                              <TD class="subheader veryshort">Payment
                                              </TD>';
                                      else
                                        echo '<TD colspan="5" class="subheaderclear">Reimbursements
                                              </TD>
                                            </TR>
                                            <TR>';
                                  echo '<TD class="subheader veryshort">Date
                                        </TD>
                                        <TD colspan="3" class="subheader">Description
                                        </TD>
                                        <TD class="subheader tiny">Value
                                        </TD>
                                      </TR>';      
                                      while ($rowTemp = MySQL_Fetch_Array($resultSetTemp))
                                      {
                                        echo '<TR>';
                                          if ($all)
                                          echo '<TD class="rowA center">'.GetPayment($rowTemp['ReimbursiveAllowance_Payment']).'
                                                </TD>';
                                          echo '<TD class="rowA">'.GetTextualDateFromDatabaseDate($rowTemp['ReimbursiveAllowance_Date']).'
                                                </TD>
                                                <TD colspan="3" class="rowA">'.$rowTemp['ReimbursiveAllowance_Description'].'
                                                </TD>
                                                <TD class="rowA center">R'.SPrintF('%02.2f', $rowTemp['ReimbursiveAllowance_Value']).'
                                                </TD>
                                              </TR>';   
                                        $total += $rowTemp['ReimbursiveAllowance_Value'];
                                        $subTotal += $rowTemp['ReimbursiveAllowance_Value'];
                                      }
                              } else
                                if ($all)
                                  echo '<TD colspan="6" class="subheader">Reimbursements
                                        </TD>
                                      </TR>';
                                else
                                  echo '<TD colspan="5" class="subheader">Reimbursements
                                        </TD>
                                      </TR>';
                              if ($all)
                                echo '<TR>
                                        <TD colspan="5" class="rowB bold">Total
                                        </TD>';
                              else
                                echo '<TR>
                                        <TD colspan="4" class="rowB bold">Total
                                        </TD>';
                          echo '<TD class="rowB center bold">R'.SPrintF('%02.2f', $subTotal).'
                                </TD>
                              </TR>';
                              //$subTotal = 0;
                              $shiftAfternoonTotal = 0;
                              $shiftNightTotal = 0;
                              if ($_SESSION['ViewReportExpenses'][4] == 2)
                                $resultSetTemp = ExecuteQuery('SELECT * FROM ShiftAllowance WHERE ShiftAllowance_Payment '.$payment.' AND ShiftAllowance_Name = '.$row['Staff_Code'].' ORDER BY ShiftAllowance_Date');
                              else
                                $resultSetTemp = ExecuteQuery('SELECT * FROM ShiftAllowance WHERE ShiftAllowance_Payment '.$payment.' AND ShiftAllowance_Name = '.$row['Staff_Code'].' AND ShiftAllowance_Date BETWEEN "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][0]).'" AND "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][1]).' 23:59:59" ORDER BY ShiftAllowance_Date'); 
                              if (MySQL_Num_Rows($resultSetTemp) > 0)
                              {
                                echo '<TR>';
                                      if ($all)
                                  echo '<TD colspan="6" class="subheaderclear">Shift Allowances
                                        </TD>
                                      </TR>
                                      <TR>
                                        <TD class="subheader veryshort">Payment
                                        </TD>';
                                else
                                  echo '<TD colspan="5" class="subheaderclear">Shift Allowances
                                        </TD>
                                      </TR>
                                      <TR>';
                                  echo '<TD class="subheader short">Date
                                        </TD>
                                        <TD colspan="4" class="subheader short">Shift
                                        </TD>
                                      </TR>';
                                      while ($rowTemp = MySQL_Fetch_Array($resultSetTemp))
                                      {             
                                        $rowTempB = MySQL_Fetch_Array(ExecuteQuery('SELECT ShiftAllowanceType_Description FROM ShiftAllowanceType WHERE ShiftAllowanceType_ID = "'.$rowTemp['ShiftAllowance_Shift'].'"'));
                                        if ($rowTemp['ShiftAllowance_Shift'] == 1)
                                          $shiftAfternoonTotal += 1;
                                        elseif ($rowTemp['ShiftAllowance_Shift'] == 2)
                                          $shiftNightTotal += 1;
                                        echo '<TR>';
                                          if ($all)
                                          echo '<TD class="rowA center">'.GetPayment($rowTemp['ShiftAllowance_Payment']).'
                                                </TD>';
                                          echo '<TD class="rowA">'.GetTextualDateFromDatabaseDate($rowTemp['ShiftAllowance_Date']).'
                                                </TD>
                                                <TD colspan="4" class="rowA">'.$rowTempB['ShiftAllowanceType_Description'].'
                                                </TD>
                                              </TR>';
                                        //$total += $rowTemp['ShiftAllowance_Value'];
                                        //$subTotal += $rowTemp['ShiftAllowance_Value'];
                                      }
                              } else
                                if ($all)
                                  echo '<TD colspan="6" class="subheader">Shift Allowances
                                        </TD>
                                      </TR>';
                                else
                                  echo '<TD colspan="5" class="subheader">Shift Allowances
                                        </TD>
                                      </TR>';
                              if ($all)
                                echo '<TR>
                                        <TD colspan="5" class="rowB bold">Shift AF
                                        </TD>
                                        <TD class="rowB center bold">'.$shiftAfternoonTotal.'
                                        </TD>
                                      </TR>
                                      <TR>
                                        <TD colspan="5" class="rowB bold">Shift NI
                                        </TD>
                                        <TD class="rowB center bold">'.$shiftNightTotal.'
                                        </TD>';
                              else
                                echo '<TR>
                                        <TD colspan="4" class="rowB bold">Shift AF
                                        </TD>
                                        <TD class="rowB center bold">'.$shiftAfternoonTotal.'
                                        </TD>
                                      </TR>
                                      <TR>
                                        <TD colspan="4" class="rowB bold">Shift NI
                                        </TD>
                                        <TD class="rowB center bold">'.$shiftNightTotal.'
                                        </TD>';
                          echo '</TR>';
                              $subTotal = 0;
                              if ($_SESSION['ViewReportExpenses'][4] == 2)
                                $resultSetTemp = ExecuteQuery('SELECT * FROM Standby WHERE Standby_Payment '.$payment.' AND Standby_Name = "'.$row['Staff_Code'].'" ORDER BY Standby_DateStart');
                              else
                                $resultSetTemp = ExecuteQuery('SELECT * FROM Standby WHERE Standby_Payment '.$payment.' AND Standby_Name = "'.$row['Staff_Code'].'" AND Standby_DateStart BETWEEN "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][0]).'" AND "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][1]).' 23:59:59" ORDER BY Standby_DateStart');
                              if (MySQL_Num_Rows($resultSetTemp) > 0)
                              { 
                                echo '<TR>';
                                      if ($all)
                                  echo '<TD colspan="6" class="subheaderclear">Standby Allowances
                                        </TD>
                                      </TR>
                                      <TR>
                                        <TD class="subheader veryshort">Payment
                                        </TD>';
                                else
                                  echo '<TD colspan="5" class="subheaderclear">Standby Allowances
                                        </TD>
                                      </TR>
                                      <TR>';
                                  echo '<TD class="subheader veryshort">Date
                                        </TD>
                                        <TD class="subheader standard">Type
                                        </TD>
                                        <TD colspan="2" class="subheader standard">Description
                                        </TD>
                                        <TD class="subheader">Value
                                        </TD>
                                      </TR>';         
                                      while ($rowTemp = MySQL_Fetch_Array($resultSetTemp))
                                      {             
                                        $rowTempB = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM StandbyType WHERE StandbyType_ID = "'.$rowTemp['Standby_Type'].'"'));
                                        echo '<TR>';
                                          if ($all)
                                          echo '<TD class="rowA center">'.GetPayment($rowTemp['Standby_Payment']).'
                                                </TD>';
                                          echo '<TD class="rowA">'.GetTextualDateFromDatabaseDate($rowTemp['Standby_DateStart']).'
                                                </TD>
                                                <TD class="rowA">'.$rowTempB['StandbyType_Description'].'
                                                </TD>
                                                <TD colspan="2" class="rowA">'.$rowTemp['Standby_Comments'].'
                                                </TD>
                                                <TD class="rowA center">R'.SPrintF('%02.2f', $rowTemp['Standby_Value']).'
                                                </TD>
                                              </TR>'; 
                                        //$total += $rowTemp['Standby_Value'];
                                        $subTotal += $rowTemp['Standby_Value'];
                                      }
                              } else
                                if ($all)
                                  echo '<TD colspan="6" class="subheader">Standby Allowances
                                        </TD>
                                      </TR>';
                                else
                                  echo '<TD colspan="5" class="subheader">Standby Allowances
                                        </TD>
                                      </TR>';
                              if ($all)
                                echo '<TR>
                                        <TD colspan="5" class="rowB bold">Total
                                        </TD>';
                              else
                                echo '<TR>
                                        <TD colspan="4" class="rowB bold">Total
                                        </TD>';
                          echo '<TD class="rowB center bold">R'.SPrintF('%02.2f', $subTotal).'
                                </TD>
                              </TR>';
                              $subTotal = 0;
                              if ($_SESSION['ViewReportExpenses'][4] == 2)
                                $resultSetTemp = ExecuteQuery('SELECT * FROM TravelAllowance WHERE TravelAllowance_Payment '.$payment.' AND TravelAllowance_Name = "'.$row['Staff_Code'].'" ORDER BY TravelAllowance_Date');
                              else
                                $resultSetTemp = ExecuteQuery('SELECT * FROM TravelAllowance WHERE TravelAllowance_Payment '.$payment.' AND TravelAllowance_Name = "'.$row['Staff_Code'].'" AND TravelAllowance_Date BETWEEN "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][0]).'" AND "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][1]).' 23:59:59" ORDER BY TravelAllowance_Date'); 
                              if (MySQL_Num_Rows($resultSetTemp) > 0)
                              { 
                                echo '<TR>';
                                if ($all)
                                  echo '<TD colspan="6" class="subheaderclear">Travel Allowances
                                        </TD>
                                      </TR>
                                      <TR>
                                        <TD class="subheader veryshort">Payment
                                        </TD>';
                                else
                                  echo '<TD colspan="5" class="subheaderclear">Travel Allowances
                                        </TD>
                                      </TR>
                                      <TR>';
                                  echo '<TD class="subheader veryshort">Date
                                        </TD>
                                        <TD class="subheader veryshort">Destination
                                        </TD>
                                        <TD class="subheader tiny">Distance (km)
                                        </TD>
                                        <TD class="subheader">Description
                                        </TD>
                                        <TD class="subheader tiny">Value
                                        </TD>
                                      </TR>';      
                                      while ($rowTemp = MySQL_Fetch_Array($resultSetTemp))
                                      {             
                                        echo '<TR>';
                                          if ($all)
                                          echo '<TD class="rowA center">'.GetPayment($rowTemp['TravelAllowance_Payment']).'
                                                </TD>';
                                          echo '<TD class="rowA">'.GetTextualDateFromDatabaseDate($rowTemp['TravelAllowance_Date']).'
                                                </TD>
                                                <TD class="rowA">'.$rowTemp['TravelAllowance_Destination'].'
                                                </TD>
                                                <TD class="rowA center">'.$rowTemp['TravelAllowance_Km'].'
                                                </TD>
                                                <TD class="rowA">'.$rowTemp['TravelAllowance_Description'].'
                                                </TD>
                                                <TD class="rowA center">R'.SPrintF('%02.2f', $rowTemp['TravelAllowance_Value']).'
                                                </TD>
                                              </TR>';   
                                        $total += $rowTemp['TravelAllowance_Value'];
                                        $subTotal += $rowTemp['TravelAllowance_Value'];
                                      }
                              } else
                                if ($all)
                                  echo '<TD colspan="6" class="subheader">Travel Allowances
                                        </TD>
                                      </TR>';
                                else
                                  echo '<TD colspan="5" class="subheader">Travel Allowances
                                        </TD>
                                      </TR>';
                              if ($all)
                                echo '<TR>
                                        <TD colspan="5" class="rowB bold">Total
                                        </TD>';
                              else
                                echo '<TR>
                                        <TD colspan="4" class="rowB bold">Total
                                        </TD>';
                          echo '<TD class="rowB center bold">R'.SPrintF('%02.2f', $subTotal).'
                                </TD>
                              </TR>';
                              $subTotal = 0;
                              if ($_SESSION['ViewReportExpenses'][4] == 2)
                                $resultSetTemp = ExecuteQuery('SELECT * FROM OutOfTownAllowance WHERE OutOfTownAllowance_Payment '.$payment.' AND OutOfTownAllowance_Name = "'.$row['Staff_Code'].'" ORDER BY OutOfTownAllowance_Date_Start');
                              else
                                $resultSetTemp = ExecuteQuery('SELECT * FROM OutOfTownAllowance WHERE OutOfTownAllowance_Payment '.$payment.' AND OutOfTownAllowance_Name = "'.$row['Staff_Code'].'" AND OutOfTownAllowance_Date_Start BETWEEN "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][0]).'" AND "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][1]).' 23:59:59" ORDER BY OutOfTownAllowance_Date_Start'); 
                              if (MySQL_Num_Rows($resultSetTemp) > 0)
                              { 
                                echo '<TR>';
                                if ($all)
                                  echo '<TD colspan="6" class="subheaderclear">Out-of-Town Allowances
                                        </TD>
                                      </TR>
                                      <TR>
                                        <TD class="subheader veryshort">Payment
                                        </TD>';
                                else
                                  echo '<TD colspan="5" class="subheaderclear">Out-of-Town Allowances
                                        </TD>
                                      </TR>
                                      <TR>';
                                  echo '<TD class="subheader veryshort">Date
                                        </TD>
                                        <TD class="subheader veryshort">Type
                                        </TD>
                                        <TD class="subheader tiny">Days
                                        </TD>
                                        <TD class="subheader">Description
                                        </TD>
                                        <TD class="subheader">Value
                                        </TD>
                                      </TR>';
                                      while ($rowTemp = MySQL_Fetch_Array($resultSetTemp))
                                      {
                                        $rowType = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM OutOfTownAllowanceType WHERE OutOfTownAllowanceType_ID = "'.$rowTemp['OutOfTownAllowance_Type'].'"'));
                                        echo '<TR>';
                                        if ($all)
                                          echo '<TD class="rowA center">'.GetPayment($rowTemp['OutOfTownAllowance_Payment']).'
                                                </TD>';
                                          echo '<TD class="rowA center">'.GetTextualDateFromDatabaseDate($rowTemp['OutOfTownAllowance_Date_Start']).' to '.GetTextualDateFromDatabaseDate($rowTemp['OutOfTownAllowance_Date_End']).'
                                                </TD>
                                                <TD class="rowA center">'.$rowType['OutOfTownAllowanceType_Description'].'
                                                </TD>
                                                <TD class="rowA center">'.$rowTemp['OutOfTownAllowance_Days'].'
                                                </TD>
                                                <TD class="rowA">'.$rowTemp['OutOfTownAllowance_Description'].'
                                                </TD>
                                                <TD class="rowA center">R'.SPrintF('%02.2f', $rowTemp['OutOfTownAllowance_Value']).'
                                                </TD>
                                              </TR>';
                                        $total += $rowTemp['OutOfTownAllowance_Value'];
                                        $subTotal += $rowTemp['OutOfTownAllowance_Value'];
                                      }
                              } else
                                if ($all)
                                  echo '<TD colspan="6" class="subheader">Out-of-Town Allowances
                                        </TD>
                                      </TR>';
                                else
                                  echo '<TD colspan="5" class="subheader">Out-of-Town Allowances
                                        </TD>
                                      </TR>';
                              if ($all)
                                echo '<TR>
                                        <TD colspan="5" class="rowB bold">Total
                                        </TD>';
                              else
                                echo '<TR>
                                        <TD colspan="4" class="rowB bold">Total
                                        </TD>';
                          echo '<TD class="rowB center bold">R'.SPrintF('%02.2f', $subTotal).'
                                </TD>
                              </TR>';
                              if ($all)
                                echo '<TR>
                                        <TD colspan="5" class="rowA bold">Reimbursive Total
                                        </TD>';
                              else
                                echo '<TR>
                                        <TD colspan="4" class="rowA bold">Reimbursive Total
                                        </TD>';
                          echo '<TD class="rowA center bold tiny">R'.SPrintF('%02.2f', $total).'
                                </TD>                             
                              </TR>    
                          </TABLE>
                          <BR/><BR/>';     
                    } 
                    echo '</DIV>';
                    break;
                  default:
                    if ($_SESSION['ViewReportExpenses'][4] == 2)
                      BuildContentHeader('Expense Report ('.$reportType.') - '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'], "", "", false);
                    else
                      BuildContentHeader('Expense Report ('.$reportType.') - '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].' for '.$dateRange, "", "", false);
                    echo '<DIV class="contentflow">
                            <P class="hide">These are the expenses listed.</P>
                            <BR/><BR/>';                            
                      echo '<TABLE cellspacing="5" align="center" class="long">
                              <TR>';
                                if ($all)
                                  echo '<TD colspan="6" class="header">';
                                else
                                  echo '<TD colspan="5" class="header">';
                                echo $row['Staff_First_Name'].' '.$row['Staff_Last_Name'].'
                                      </TD>
                                    </TR>';
                              $total = 0;
                              $subTotal = 0;
                              if ($_SESSION['ViewReportExpenses'][4] == 2)
                                $resultSetTemp = ExecuteQuery('SELECT * FROM Deduction WHERE Deduction_Payment '.$payment.' AND Deduction_Name = "'.$_SESSION['ViewReportExpenses'][3].'" ORDER BY Deduction_Date');
                              else
                                $resultSetTemp = ExecuteQuery('SELECT * FROM Deduction WHERE Deduction_Payment '.$payment.' AND Deduction_Name = "'.$_SESSION['ViewReportExpenses'][3].'" AND Deduction_Date BETWEEN "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][0]).'" AND "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][1]).' 23:59:59" ORDER BY Deduction_Date'); 
                              if (MySQL_Num_Rows($resultSetTemp) > 0)
                              { 
                                echo '<TR>';
                                if ($all)
                                  echo '<TD colspan="6" class="subheaderclear">Deductions
                                        </TD>
                                      </TR>
                                      <TR>
                                        <TD class="subheader veryshort">Payment
                                        </TD>';
                                else
                                  echo '<TD colspan="5" class="subheaderclear">Deductions
                                        </TD>
                                      </TR>
                                      <TR>';
                                  echo '<TD class="subheader veryshort">Date
                                        </TD>
                                        <TD colspan="3" class="subheader">Description
                                        </TD>
                                        <TD class="subheader tiny">Value
                                        </TD>
                                      </TR>';      
                                      while ($rowTemp = MySQL_Fetch_Array($resultSetTemp))
                                      {             
                                        echo '<TR>';
                                          if ($all)
                                          echo '<TD class="rowA center">'.GetPayment($rowTemp['Deduction_Payment']).'
                                                </TD>';
                                          echo '<TD class="rowA">'.GetTextualDateFromDatabaseDate($rowTemp['Deduction_Date']).'
                                                </TD>
                                                <TD colspan="3" class="rowA">'.$rowTemp['Deduction_Description'].'
                                                </TD>
                                                <TD class="rowA center">R'.SPrintF('%02.2f', -$rowTemp['Deduction_Value']).'
                                                </TD>
                                              </TR>';     
                                        //$total -= $rowTemp['Deduction_Value'];
                                        $subTotal -= $rowTemp['Deduction_Value'];
                                      }
                              } else
                                if ($all)
                                  echo '<TD colspan="6" class="subheader">Deductions
                                        </TD>
                                      </TR>';
                                else
                                  echo '<TD colspan="5" class="subheader">Deductions
                                        </TD>
                                      </TR>';
                              if ($all)
                                echo '<TR>
                                        <TD colspan="5" class="rowB bold">Total
                                        </TD>';
                              else
                                echo '<TR>
                                        <TD colspan="4" class="rowB bold">Total
                                        </TD>';
                          echo '<TD class="rowB center bold">R'.SPrintF('%02.2f', $subTotal).'
                                </TD>
                              </TR>';
                              $subTotal = 0;
                              if ($_SESSION['ViewReportExpenses'][4] == 2)
                                $resultSetTemp = ExecuteQuery('SELECT * FROM Overtime WHERE Overtime_Payment '.$payment.' AND Overtime_Name = "'.$_SESSION['ViewReportExpenses'][3].'" ORDER BY Overtime_Start');
                              else
                                $resultSetTemp = ExecuteQuery('SELECT * FROM Overtime WHERE Overtime_Payment '.$payment.' AND Overtime_Name = "'.$_SESSION['ViewReportExpenses'][3].'" AND Overtime_Start BETWEEN "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][0]).'" AND "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][1]).' 23:59:59" ORDER BY Overtime_Start'); 
                              $rowTempB = MySQL_Fetch_Array(ExecuteQuery('SELECT SUM(OvertimeBank_Hours) AS Overtime FROM OvertimeBank WHERE OvertimeBank_Name = '.$_SESSION['ViewReportExpenses'][3].' AND OvertimeBank_Start <= "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][1]).' 23:59:59" AND OvertimeBank_IsApproved = "1"'));
                              if (MySQL_Num_Rows($resultSetTemp) > 0)
                              { 
                                echo '<TR>';
                                if ($all)
                                  echo '<TD colspan="6" class="subheaderclear">Overtime
                                        </TD>
                                      </TR>
                                      <TR>
                                        <TD class="subheader veryshort">Payment
                                        </TD>';
                                else
                                  echo '<TD colspan="5" class="subheaderclear">Overtime
                                        </TD>
                                      </TR>
                                      <TR>';
                                  echo '<TD class="subheader veryshort">Start
                                        </TD>
                                        <TD class="subheader veryshort">End
                                        </TD>
                                        <TD colspan="2" class="subheader">Description
                                        </TD>
                                        <TD class="subheader tiny">Hours
                                        </TD>
                                      </TR>';
                                      while ($rowTemp = MySQL_Fetch_Array($resultSetTemp))
                                      {
                                        echo '<TR>';
                                          if ($all)
                                          echo '<TD class="rowA center">'.GetPayment($rowTemp['Overtime_Payment']).'
                                                </TD>';
                                          echo '<TD class="rowA center">'.GetTextualDateTimeFromDatabaseDateTime($rowTemp['Overtime_Start']).'
                                                </TD>
                                                <TD class="rowA center">'.GetTextualDateTimeFromDatabaseDateTime($rowTemp['Overtime_End']).'
                                                </TD>
                                                <TD colspan="2" class="rowA">'.$rowTemp['Overtime_Description'].'
                                                </TD>
                                                <TD class="rowA center">'.SPrintF('%02.2f', $rowTemp['Overtime_Hours']).'
                                                </TD>
                                              </TR>'; 
                                        $subTotal += $rowTemp['Overtime_Hours'];
                                      }
                              } else
                                if ($all)
                                  echo '<TD colspan="6" class="subheader">Overtime
                                        </TD>
                                      </TR>';
                                else
                                  echo '<TD colspan="5" class="subheader">Overtime
                                        </TD>
                                      </TR>';
                              if ($all)
                                echo '<TR>
                                        <TD colspan="5" class="rowB bold">Total
                                        </TD>
                                        <TD class="rowB center bold">'.SPrintF('%02.2f', $subTotal).'
                                        </TD>                             
                                      </TR>
                                      <TR>
                                        <TD colspan="5" class="rowB bold">Banked Overtime Balance:
                                        </TD> ';
                              else
                                echo '<TR>
                                        <TD colspan="4" class="rowB bold">Total
                                        </TD>
                                        <TD class="rowB center bold">'.SPrintF('%02.2f', $subTotal).'
                                        </TD>                             
                                      </TR>
                                      <TR>
                                        <TD colspan="4" class="rowB bold">Banked Overtime Balance:
                                        </TD> ';
                          echo '<TD class="rowB center bold">'.SPrintF('%02.2f', $rowTempB['Overtime']).'
                                </TD>                             
                              </TR>';
                              $subTotal = 0;
                              if ($_SESSION['ViewReportExpenses'][4] == 2)
                                $resultSetTemp = ExecuteQuery('SELECT * FROM ReimbursiveAllowance WHERE ReimbursiveAllowance_Payment '.$payment.' AND ReimbursiveAllowance_Name = "'.$_SESSION['ViewReportExpenses'][3].'" ORDER BY ReimbursiveAllowance_Date');
                              else
                                $resultSetTemp = ExecuteQuery('SELECT * FROM ReimbursiveAllowance WHERE ReimbursiveAllowance_Payment '.$payment.' AND ReimbursiveAllowance_Name = "'.$_SESSION['ViewReportExpenses'][3].'" AND ReimbursiveAllowance_Date BETWEEN "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][0]).'" AND "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][1]).' 23:59:59" ORDER BY ReimbursiveAllowance_Date'); 
                              if (MySQL_Num_Rows($resultSetTemp) > 0)
                              { 
                                echo '<TR>';
                                if ($all)
                                  echo '<TD colspan="6" class="subheaderclear">Reimbursements
                                        </TD>
                                      </TR>
                                      <TR>
                                        <TD class="subheader veryshort">Payment
                                        </TD>';
                                else
                                  echo '<TD colspan="5" class="subheaderclear">Reimbursements
                                        </TD>
                                      </TR>
                                      <TR>';
                                  echo '<TD class="subheader veryshort">Date
                                        </TD>
                                        <TD colspan="3" class="subheader">Description
                                        </TD>
                                        <TD class="subheader tiny">Value
                                        </TD>
                                      </TR>';      
                                      while ($rowTemp = MySQL_Fetch_Array($resultSetTemp))
                                      {
                                        echo '<TR>';
                                          if ($all)
                                          echo '<TD class="rowA center">'.GetPayment($rowTemp['ReimbursiveAllowance_Payment']).'
                                                </TD>';
                                          echo '<TD class="rowA">'.GetTextualDateFromDatabaseDate($rowTemp['ReimbursiveAllowance_Date']).'
                                                </TD>
                                                <TD colspan="3" class="rowA">'.$rowTemp['ReimbursiveAllowance_Description'].'
                                                </TD>
                                                <TD class="rowA center">R'.SPrintF('%02.2f', $rowTemp['ReimbursiveAllowance_Value']).'
                                                </TD>
                                              </TR>';   
                                        $total += $rowTemp['ReimbursiveAllowance_Value'];
                                        $subTotal += $rowTemp['ReimbursiveAllowance_Value'];
                                      }
                              } else
                                if ($all)
                                  echo '<TD colspan="6" class="subheader">Reimbursements
                                        </TD>
                                      </TR>';
                                else
                                  echo '<TD colspan="5" class="subheader">Reimbursements
                                        </TD>
                                      </TR>';
                              if ($all)
                                echo '<TR>
                                        <TD colspan="5" class="rowB bold">Total
                                        </TD>';
                              else
                                echo '<TR>
                                        <TD colspan="4" class="rowB bold">Total
                                        </TD>';
                          echo '<TD class="rowB center bold">R'.SPrintF('%02.2f', $subTotal).'
                                </TD>
                              </TR> ';
                              //$subTotal = 0;
                              $shiftAfternoonTotal = 0;
                              $shiftNightTotal = 0;
                              if ($_SESSION['ViewReportExpenses'][4] == 2)
                                $resultSetTemp = ExecuteQuery('SELECT * FROM ShiftAllowance WHERE ShiftAllowance_Payment '.$payment.' AND ShiftAllowance_Name = "'.$_SESSION['ViewReportExpenses'][3].'" ORDER BY ShiftAllowance_Date');
                              else
                                $resultSetTemp = ExecuteQuery('SELECT * FROM ShiftAllowance WHERE ShiftAllowance_Payment '.$payment.' AND ShiftAllowance_Name = "'.$_SESSION['ViewReportExpenses'][3].'" AND ShiftAllowance_Date BETWEEN "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][0]).'" AND "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][1]).' 23:59:59" ORDER BY ShiftAllowance_Date'); 
                              if (MySQL_Num_Rows($resultSetTemp) >0 )
                              { 
                                echo '<TR>';
                                if ($all)
                                  echo '<TD colspan="6" class="subheaderclear">Shift Allowances
                                        </TD>
                                      </TR>
                                      <TR>
                                        <TD class="subheader veryshort">Payment
                                        </TD>';
                                else
                                  echo '<TD colspan="5" class="subheaderclear">Shift Allowances
                                        </TD>
                                      </TR>
                                      <TR>';
                                  echo '<TD class="subheader short">Date
                                        </TD>
                                        <TD colspan="4" class="subheader short">Shift
                                        </TD>
                                      </TR>';        
                                      while ($rowTemp = MySQL_Fetch_Array($resultSetTemp))
                                      {             
                                        $rowTempB = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM ShiftAllowanceType WHERE ShiftAllowanceType_ID = "'.$rowTemp['ShiftAllowance_Shift'].'"'));
                                        if ($rowTemp['ShiftAllowance_Shift'] == 1)
                                          $shiftAfternoonTotal += 1;
                                        elseif ($rowTemp['ShiftAllowance_Shift'] == 2)
                                          $shiftNightTotal += 1;
                                        echo '<TR>';
                                          if ($all)
                                          echo '<TD class="rowA center">'.GetPayment($rowTemp['ShiftAllowance_Payment']).'
                                                </TD>';
                                          echo '<TD class="rowA">'.GetTextualDateFromDatabaseDate($rowTemp['ShiftAllowance_Date']).'
                                                </TD>
                                                <TD colspan="4" class="rowA">'.$rowTempB['ShiftAllowanceType_Description'].'
                                                </TD>
                                              </TR>'; 
                                        //$total += $rowTemp['ShiftAllowance_Value'];
                                        //$subTotal += $rowTemp['ShiftAllowance_Value'];
                                      }
                              } else
                                if ($all)
                                  echo '<TD colspan="6" class="subheader">Shift Allowances
                                        </TD>
                                      </TR>';
                                else
                                  echo '<TD colspan="5" class="subheader">Shift Allowances
                                        </TD>
                                      </TR>';
                              if ($all)
                                echo '<TR>
                                        <TD colspan="5" class="rowB bold">Shift AF
                                        </TD>
                                        <TD class="rowB center bold">'.$shiftAfternoonTotal.'
                                        </TD>
                                      </TR>
                                      <TR>
                                        <TD colspan="5" class="rowB bold">Shift NI
                                        </TD>
                                        <TD class="rowB center bold">'.$shiftNightTotal.'
                                        </TD>';
                              else
                                echo '<TR>
                                        <TD colspan="4" class="rowB bold">Shift AF
                                        </TD>
                                        <TD class="rowB center bold">'.$shiftAfternoonTotal.'
                                        </TD>
                                      </TR>
                                      <TR>
                                        <TD colspan="4" class="rowB bold">Shift NI
                                        </TD>
                                        <TD class="rowB center bold">'.$shiftNightTotal.'
                                        </TD>';
                          echo '</TR>';
                              $subTotal = 0;
                              if ($_SESSION['ViewReportExpenses'][4] == 2)
                                $resultSetTemp = ExecuteQuery('SELECT * FROM Standby WHERE Standby_Payment '.$payment.' AND Standby_Name = "'.$_SESSION['ViewReportExpenses'][3].'" ORDER BY Standby_DateStart');
                              else
                                $resultSetTemp = ExecuteQuery('SELECT * FROM Standby WHERE Standby_Payment '.$payment.' AND Standby_Name = "'.$_SESSION['ViewReportExpenses'][3].'" AND Standby_DateStart BETWEEN "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][0]).'" AND "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][1]).' 23:59:59" ORDER BY Standby_DateStart'); 
                              if (MySQL_Num_Rows($resultSetTemp) > 0)
                              { 
                                echo '<TR>';
                                if ($all)
                                  echo '<TD colspan="6" class="subheaderclear">Standby Allowances
                                        </TD>
                                      </TR>
                                      <TR>
                                        <TD class="subheader veryshort">Payment
                                        </TD>';
                                else
                                  echo '<TD colspan="5" class="subheaderclear">Standby Allowances
                                        </TD>
                                      </TR>
                                      <TR>';
                                  echo '<TD class="subheader veryshort">Date
                                        </TD>
                                        <TD class="subheader standard">Type
                                        </TD>
                                        <TD colspan="2" class="subheader standard">Description
                                        </TD>
                                        <TD class="subheader">Value
                                        </TD>
                                      </TR>';         
                                      while ($rowTemp = MySQL_Fetch_Array($resultSetTemp))
                                      {             
                                        $rowTempB = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM StandbyType WHERE StandbyType_ID = "'.$rowTemp['Standby_Type'].'"'));
                                        echo '<TR>';
                                          if ($all)
                                          echo '<TD class="rowA center">'.GetPayment($rowTemp['Standby_Payment']).'
                                                </TD>';
                                          echo '<TD class="rowA">'.GetTextualDateFromDatabaseDate($rowTemp['Standby_DateStart']).'
                                                </TD>
                                                <TD class="rowA">'.$rowTempB['StandbyType_Description'].'
                                                </TD>
                                                <TD colspan="2" class="rowA">'.$rowTemp['Standby_Comments'].'
                                                </TD>
                                                <TD class="rowA center">R'.SPrintF('%02.2f', $rowTemp['Standby_Value']).'
                                                </TD>
                                              </TR>'; 
                                        //$total += $rowTemp['Standby_Value'];
                                        $subTotal += $rowTemp['Standby_Value'];
                                      }
                              } else
                                if ($all)
                                  echo '<TD colspan="6" class="subheader">Standby Allowances
                                        </TD>
                                      </TR>';
                                else
                                  echo '<TD colspan="5" class="subheader">Standby Allowances
                                        </TD>
                                      </TR>';
                              if ($all)
                                echo '<TR>
                                        <TD colspan="5" class="rowB bold">Total
                                        </TD>';
                              else
                                echo '<TR>
                                        <TD colspan="4" class="rowB bold">Total
                                        </TD>';
                          echo '<TD class="rowB center bold">R'.SPrintF('%02.2f', $subTotal).'
                                </TD>
                              </TR>';
                              $subTotal = 0;
                              if ($_SESSION['ViewReportExpenses'][4] == 2)
                                $resultSetTemp = ExecuteQuery('SELECT * FROM TravelAllowance WHERE TravelAllowance_Payment '.$payment.' AND TravelAllowance_Name = "'.$_SESSION['ViewReportExpenses'][3].'" ORDER BY TravelAllowance_Date');
                              else
                                $resultSetTemp = ExecuteQuery('SELECT * FROM TravelAllowance WHERE TravelAllowance_Payment '.$payment.' AND TravelAllowance_Name = "'.$_SESSION['ViewReportExpenses'][3].'" AND TravelAllowance_Date BETWEEN "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][0]).'" AND "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][1]).' 23:59:59" ORDER BY TravelAllowance_Date'); 
                              if (MySQL_Num_Rows($resultSetTemp) > 0)
                              { 
                                echo '<TR>';
                                if ($all)
                                  echo '<TD colspan="6" class="subheaderclear">Travel Allowances
                                        </TD>
                                      </TR>
                                      <TR>
                                        <TD class="subheader veryshort">Payment
                                        </TD>';
                                else
                                  echo '<TD colspan="5" class="subheaderclear">Travel Allowances
                                        </TD>
                                      </TR>
                                      <TR>';
                                  echo '<TD class="subheader veryshort">Date
                                        </TD>
                                        <TD class="subheader veryshort">Destination
                                        </TD>
                                        <TD class="subheader tiny">Distance (km)
                                        </TD>
                                        <TD class="subheader">Description
                                        </TD>
                                        <TD class="subheader tiny">Value
                                        </TD>
                                      </TR>';      
                                      while ($rowTemp = MySQL_Fetch_Array($resultSetTemp))
                                      {            
                                        echo '<TR>';
                                          if ($all)
                                          echo '<TD class="rowA center">'.GetPayment($rowTemp['TravelAllowance_Payment']).'
                                                </TD>';
                                          echo '<TD class="rowA">'.GetTextualDateFromDatabaseDate($rowTemp['TravelAllowance_Date']).'
                                                </TD>
                                                <TD class="rowA">'.$rowTemp['TravelAllowance_Destination'].'
                                                </TD>
                                                <TD class="rowA center">'.$rowTemp['TravelAllowance_Km'].'
                                                </TD>
                                                <TD class="rowA">'.$rowTemp['TravelAllowance_Description'].'
                                                </TD>
                                                <TD class="rowA center">R'.SPrintF('%02.2f', $rowTemp['TravelAllowance_Value']).'
                                                </TD>
                                              </TR>';   
                                        $total += $rowTemp['TravelAllowance_Value'];
                                        $subTotal += $rowTemp['TravelAllowance_Value'];
                                      }
                              } else
                                if ($all)
                                  echo '<TD colspan="6" class="subheader">Travel Allowances
                                        </TD>
                                      </TR>';
                                else
                                  echo '<TD colspan="5" class="subheader">Travel Allowances
                                        </TD>
                                      </TR>';
                              if ($all)
                                echo '<TR>
                                        <TD colspan="5" class="rowB bold">Total
                                        </TD>';
                              else
                                echo '<TR>
                                        <TD colspan="4" class="rowB bold">Total
                                        </TD>';
                          echo '<TD class="rowB center bold">R'.SPrintF('%02.2f', $subTotal).'
                                </TD>
                              </TR>';
                              $subTotal = 0;
                              if ($_SESSION['ViewReportExpenses'][4] == 2)
                                $resultSetTemp = ExecuteQuery('SELECT * FROM OutOfTownAllowance WHERE OutOfTownAllowance_Payment '.$payment.' AND OutOfTownAllowance_Name = "'.$_SESSION['ViewReportExpenses'][3].'" ORDER BY OutOfTownAllowance_Date_Start');
                              else
                                $resultSetTemp = ExecuteQuery('SELECT * FROM OutOfTownAllowance WHERE OutOfTownAllowance_Payment '.$payment.' AND OutOfTownAllowance_Name = "'.$_SESSION['ViewReportExpenses'][3].'" AND OutOfTownAllowance_Date_Start BETWEEN "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][0]).'" AND "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][1]).' 23:59:59" ORDER BY OutOfTownAllowance_Date_Start'); 
                              if (MySQL_Num_Rows($resultSetTemp) > 0)
                              { 
                                echo '<TR>';
                                if ($all)
                                  echo '<TD colspan="6" class="subheaderclear">Out-of-Town Allowances
                                        </TD>
                                      </TR>
                                      <TR>
                                        <TD class="subheader veryshort">Payment
                                        </TD>';
                                else
                                  echo '<TD colspan="5" class="subheaderclear">Out-of-Town Allowances
                                        </TD>
                                      </TR>
                                      <TR>';
                                  echo '<TD class="subheader veryshort">Date
                                        </TD>
                                        <TD class="subheader veryshort">Type
                                        </TD>
                                        <TD class="subheader tiny">Days
                                        </TD>
                                        <TD class="subheader">Description
                                        </TD>
                                        <TD class="subheader">Value
                                        </TD>
                                      </TR>';      
                                      while ($rowTemp = MySQL_Fetch_Array($resultSetTemp))
                                      {
                                        $rowType = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM OutOfTownAllowanceType WHERE OutOfTownAllowanceType_ID = "'.$rowTemp['OutOfTownAllowance_Type'].'"'));
                                        echo '<TR>';
                                        if ($all)
                                          echo '<TD class="rowA center">'.GetPayment($rowTemp['OutOfTownAllowance_Payment']).'
                                                </TD>';
                                          echo '<TD class="rowA center">'.GetTextualDateFromDatabaseDate($rowTemp['OutOfTownAllowance_Date_Start']).' to '.GetTextualDateFromDatabaseDate($rowTemp['OutOfTownAllowance_Date_End']).'
                                                </TD>
                                                <TD class="rowA center">'.$rowType['OutOfTownAllowanceType_Description'].'
                                                </TD>
                                                <TD class="rowA center">'.$rowTemp['OutOfTownAllowance_Days'].'
                                                </TD>
                                                <TD class="rowA">'.$rowTemp['OutOfTownAllowance_Description'].'
                                                </TD>
                                                <TD class="rowA center">R'.SPrintF('%02.2f', $rowTemp['OutOfTownAllowance_Value']).'
                                                </TD>
                                              </TR>';
                                        $total += $rowTemp['OutOfTownAllowance_Value'];
                                        $subTotal += $rowTemp['OutOfTownAllowance_Value'];
                                      }
                              } else
                                if ($all)
                                  echo '<TD colspan="6" class="subheader">Out-of-Town Allowances
                                        </TD>
                                      </TR>';
                                else
                                  echo '<TD colspan="5" class="subheader">Out-of-Town Allowances
                                        </TD>
                                      </TR>';
                              if ($all)
                                echo '<TR>
                                        <TD colspan="5" class="rowB bold">Total
                                        </TD>';
                              else
                                echo '<TR>
                                        <TD colspan="4" class="rowB bold">Total
                                        </TD>';
                          echo '<TD class="rowB center bold">R'.SPrintF('%02.2f', $subTotal).'
                                </TD>
                              </TR>';
                              if ($all)
                                echo '<TR>
                                        <TD colspan="5" class="rowA bold">Reimbursive Total
                                        </TD>';
                              else
                                echo '<TR>
                                        <TD colspan="4" class="rowA bold">Reimbursive Total
                                        </TD>';
                          echo '<TD class="rowA center bold tiny">R'.SPrintF('%02.2f', $total).'
                                </TD>                             
                              </TR>    
                          </TABLE>
                          <BR/><BR/>'; 
                    break;
                }
                break;
              case '1': 
                switch ($_SESSION['ViewReportExpenses'][3])
                {       
                  case "":    
                    if ($_SESSION['ViewReportExpenses'][4] == 2)
                    {
                      BuildContentHeader('Deduction Report ('.$reportType.')', "", "", false);
                      $query = 'SELECT * FROM Deduction WHERE Deduction_Payment '.$payment.' LIMIT 1';
                    } else
                    {
                      BuildContentHeader('Deduction Report ('.$reportType.') - '.$dateRange, "", "", false);
                      $query = 'SELECT * FROM Deduction WHERE Deduction_Payment '.$payment.' AND Deduction_Date BETWEEN "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][0]).'" AND "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][1]).' 23:59:59" LIMIT 1';      
                    }
                    if (MySQL_Num_Rows(ExecuteQuery($query)) > 0)
                    {     
                      echo '<DIV class="contentflow">
                              <P>These are the deductions listed.</P>
                              <BR/><BR/>';
                              
                      $resultSet = ExecuteQuery('SELECT * FROM Staff WHERE Staff_IsEmployee = "1" ORDER BY Staff_First_Name, Staff_Last_Name');
                      while ($row = MySQL_Fetch_Array($resultSet))
                      { 
                        echo '<TABLE cellspacing="5" align="center" class="long" style="page-break-after: always">
                                <TR>';
                                if ($all)
                                  echo '<TD colspan="4" class="header">';
                                else
                                  echo '<TD colspan="3" class="header">';
                                echo $row['Staff_First_Name'].' '.$row['Staff_Last_Name'].'
                                      </TD>
                                    </TR>';
                                $total = 0;
                                if ($_SESSION['ViewReportExpenses'][4] == 2)
                                  $resultSetTemp = ExecuteQuery('SELECT * FROM Deduction WHERE Deduction_Payment '.$payment.' AND Deduction_Name = '.$row['Staff_Code'].' ORDER BY Deduction_Date');
                                else
                                  $resultSetTemp = ExecuteQuery('SELECT * FROM Deduction WHERE Deduction_Payment '.$payment.' AND Deduction_Name = '.$row['Staff_Code'].' AND Deduction_Date BETWEEN "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][0]).'" AND "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][1]).' 23:59:59" ORDER BY Deduction_Date'); 
                                if (MySQL_Num_Rows($resultSetTemp) > 0)
                                { 
                                  echo '<TR>';
                                  if ($all)
                                    echo '<TD class="subheader veryshort">Payment
                                          </TD>';
                                    echo '<TD class="subheader veryshort">Date
                                          </TD>
                                          <TD class="subheader">Description
                                          </TD>
                                          <TD class="subheader tiny">Value
                                          </TD>
                                        </TR>';      
                                        while ($rowTemp = MySQL_Fetch_Array($resultSetTemp))
                                        {             
                                          echo '<TR>';
                                          if ($all)
                                            echo '<TD class="rowA center">'.GetPayment($rowTemp['Overtime_Payment']).'
                                                  </TD>';
                                            echo '<TD class="rowA">'.GetTextualDateFromDatabaseDate($rowTemp['Deduction_Date']).'
                                                  </TD>
                                                  <TD class="rowA">'.$rowTemp['Deduction_Description'].'
                                                  </TD>
                                                  <TD class="rowA center">R'.SPrintF('%02.2f', -$rowTemp['Deduction_Value']).'
                                                  </TD>
                                                </TR>'; 
                                          $total -= $rowTemp['Deduction_Value'];
                                        }
                                }
                                if ($all)
                                  echo '<TR>
                                          <TD colspan="3" class="rowB bold">Total:
                                          </TD>';
                                else
                                  echo '<TR>
                                          <TD colspan="2" class="rowB bold">Total:
                                          </TD>';
                            echo '<TD class="rowB center bold tiny">R'.SPrintF('%02.2f', $total).'
                                  </TD>
                                </TR>     
                              </TABLE>
                              <BR/><BR/>';     
                      } 
                      echo '</DIV>';
                    } else                      
                      echo '<DIV class="contentflow">
                              <P>There are no deductions listed for the given date range.</P>
                            </DIV>';
                    break;
                  default:  
                    if ($_SESSION['ViewReportExpenses'][4] == 2)
                    {
                      BuildContentHeader('Deduction Report ('.$reportType.') - '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'], "", "", false);
                      $resultSet = ExecuteQuery('SELECT * FROM Deduction WHERE Deduction_Payment '.$payment.' AND Deduction_Name = "'.$_SESSION['ViewReportExpenses'][3].'" ORDER BY Deduction_Date');
                    } else
                    {
                      BuildContentHeader('Deduction Report ('.$reportType.') - '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].' for '.$dateRange, "", "", false);
                      $resultSet = ExecuteQuery('SELECT * FROM Deduction WHERE Deduction_Payment '.$payment.' AND Deduction_Name = "'.$_SESSION['ViewReportExpenses'][3].'" AND Deduction_Date BETWEEN "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][0]).'" AND "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][1]).' 23:59:59" ORDER BY Deduction_Date'); 
                    }
                    if (MySQL_Num_Rows($resultSet) > 0)
                    { 
                      echo '<DIV class="contentflow">
                              <P>These are the deductions listed.</P>
                              <BR/><BR/>
                              <TABLE cellspacing="5" align="center" class="long">
                                <TR>';
                                if ($all)
                                  echo '<TD colspan="4" class="header">Deduction Details
                                        </TD>
                                      </TR>
                                      <TR>
                                        <TD class="subheader veryshort">Payment
                                        </TD>';
                                else
                                  echo '<TD colspan="3" class="header">Deduction Details
                                        </TD>
                                      </TR>
                                      <TR>';
                            echo '<TD class="subheader veryshort">Date
                                  </TD>
                                  <TD class="subheader">Description
                                  </TD>
                                  <TD class="subheader tiny">Value
                                  </TD>
                                </TR>';      
                                $total = 0;
                                while ($row = MySQL_Fetch_Array($resultSet))
                                {             
                                  echo '<TR>';
                                    if ($all)
                                    echo '<TD class="rowA center">'.GetPayment($row['Overtime_Payment']).'
                                          </TD>';
                                    echo '<TD class="rowA">'.GetTextualDateFromDatabaseDate($row['Deduction_Date']).'
                                          </TD>
                                          <TD class="rowA">'.$row['Deduction_Description'].'
                                          </TD>
                                          <TD class="rowA center">R'.SPrintF('%02.2f', -$row['Deduction_Value']).'
                                          </TD>
                                        </TR>';   
                                  $total -= $row['Deduction_Value'];
                                }
                                if ($all)
                                  echo '<TR>
                                          <TD colspan="3" class="rowB bold">Total:
                                          </TD>';
                                else
                                  echo '<TR>
                                          <TD colspan="2" class="rowB bold">Total:
                                          </TD>';
                            echo '<TD class="rowB center bold">R'.SPrintF('%02.2f', $total).'
                                  </TD>                             
                                </TR>     
                              </TABLE>
                            </DIV>';  
                    } else                      
                      echo '<DIV class="contentflow">
                              <P>There are no deductions listed for the given date range.</P>
                            </DIV>';   
                    break;
                }
                break;
              case '2': 
                switch ($_SESSION['ViewReportExpenses'][3])
                {       
                  case "":    
                    if ($_SESSION['ViewReportExpenses'][4] == 2)
                    {
                      BuildContentHeader('Reimbursement Report ('.$reportType.')', "", "", false);
                      $query = 'SELECT * FROM ReimbursiveAllowance WHERE ReimbursiveAllowance_Payment '.$payment.' LIMIT 1';
                    } else
                    {
                      BuildContentHeader('Reimbursement Report ('.$reportType.') - '.$dateRange, "", "", false);
                      $query = 'SELECT * FROM ReimbursiveAllowance WHERE ReimbursiveAllowance_Payment '.$payment.' AND ReimbursiveAllowance_Date BETWEEN "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][0]).'" AND "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][1]).' 23:59:59" LIMIT 1';
                    }
                    if (MySQL_Num_Rows(ExecuteQuery($query)) > 0)
                    {     
                      echo '<DIV class="contentflow">
                              <P>These are the reimbursements listed.</P>
                              <BR/><BR/>';
                              
                      $resultSet = ExecuteQuery('SELECT * FROM Staff WHERE Staff_IsEmployee = "1" ORDER BY Staff_First_Name, Staff_Last_Name');
                      while ($row = MySQL_Fetch_Array($resultSet))
                      { 
                        echo '<TABLE cellspacing="5" align="center" class="long" style="page-break-after: always">
                                <TR>';
                                if ($all)
                                  echo '<TD colspan="4" class="header">';
                                else
                                  echo '<TD colspan="3" class="header">';
                                echo $row['Staff_First_Name'].' '.$row['Staff_Last_Name'].'
                                      </TD>
                                    </TR>';
                                $total = 0;
                                if ($_SESSION['ViewReportExpenses'][4] == 2)
                                  $resultSetTemp = ExecuteQuery('SELECT * FROM ReimbursiveAllowance WHERE ReimbursiveAllowance_Payment '.$payment.' AND ReimbursiveAllowance_Name = "'.$row['Staff_Code'].'" ORDER BY ReimbursiveAllowance_Date');
                                else
                                  $resultSetTemp = ExecuteQuery('SELECT * FROM ReimbursiveAllowance WHERE ReimbursiveAllowance_Payment '.$payment.' AND ReimbursiveAllowance_Name = "'.$row['Staff_Code'].'" AND ReimbursiveAllowance_Date BETWEEN "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][0]).'" AND "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][1]).' 23:59:59" ORDER BY ReimbursiveAllowance_Date'); 
                                if (MySQL_Num_Rows($resultSetTemp) > 0)
                                { 
                                  echo '<TR>';
                                  if ($all)
                                    echo '<TD class="subheader veryshort">Payment
                                          </TD>';
                                    echo '<TD class="subheader veryshort">Date
                                          </TD>
                                          <TD class="subheader">Description
                                          </TD>
                                          <TD class="subheader tiny">Value
                                          </TD>
                                        </TR>';      
                                        while ($rowTemp = MySQL_Fetch_Array($resultSetTemp))
                                        {             
                                          echo '<TR>';
                                          if ($all)
                                            echo '<TD class="rowA center">'.GetPayment($rowTemp['ReimbursiveAllowance_Payment']).'
                                                  </TD>';
                                            echo '<TD class="rowA">'.GetTextualDateFromDatabaseDate($rowTemp['ReimbursiveAllowance_Date']).'
                                                  </TD>
                                                  <TD class="rowA">'.$rowTemp['ReimbursiveAllowance_Description'].'
                                                  </TD>
                                                  <TD class="rowA center">R'.SPrintF('%02.2f', $rowTemp['ReimbursiveAllowance_Value']).'
                                                  </TD>
                                                </TR>';   
                                          $total += $rowTemp['ReimbursiveAllowance_Value'];
                                        }
                                }
                                if ($all)
                                  echo '<TR>
                                          <TD colspan="3" class="rowB bold">Total:
                                          </TD>';
                                else
                                  echo '<TR>
                                          <TD colspan="2" class="rowB bold">Total:
                                          </TD>';
                            echo '<TD class="rowB center bold tiny">R'.SPrintF('%02.2f', $total).'
                                  </TD>
                                </TR>     
                              </TABLE>
                              <BR/><BR/>';     
                      } 
                      echo '</DIV>';
                    } else                      
                      echo '<DIV class="contentflow">
                              <P>There are no reimbursements listed for the given date range.</P>
                            </DIV>';       
                    break;
                  default:   
                    if ($_SESSION['ViewReportExpenses'][4] == 2)
                    {
                      BuildContentHeader('Reimbursement Report ('.$reportType.') - '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'], "", "", false);
                      $resultSet = ExecuteQuery('SELECT * FROM ReimbursiveAllowance WHERE ReimbursiveAllowance_Payment '.$payment.' AND ReimbursiveAllowance_Name = "'.$_SESSION['ViewReportExpenses'][3].'" ORDER BY ReimbursiveAllowance_Date');
                    } else
                    {  BuildContentHeader('Reimbursement Report ('.$reportType.') - '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].' for '.$dateRange, "", "", false);
                      $resultSet = ExecuteQuery('SELECT * FROM ReimbursiveAllowance WHERE ReimbursiveAllowance_Payment '.$payment.' AND ReimbursiveAllowance_Name = "'.$_SESSION['ViewReportExpenses'][3].'" AND ReimbursiveAllowance_Date BETWEEN "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][0]).'" AND "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][1]).' 23:59:59" ORDER BY ReimbursiveAllowance_Date'); 
                    }
                    if (MySQL_Num_Rows($resultSet) > 0)
                    { 
                      echo '<DIV class="contentflow">
                              <P>These are the shift allowances listed.</P>
                              <BR/><BR/>
                              <TABLE cellspacing="5" align="center" class="long">
                                <TR>';
                                if ($all)
                                  echo '<TD colspan="4" class="header">Reimbursement Details
                                        </TD>
                                      </TR>
                                      <TR>
                                        <TD class="subheader veryshort">Payment
                                        </TD>';
                                else
                                  echo '<TD colspan="3" class="header">Reimbursement Details
                                        </TD>
                                      </TR>
                                      <TR>';
                            echo '<TD class="subheader veryshort">Date
                                  </TD>
                                  <TD class="subheader">Description
                                  </TD>
                                  <TD class="subheader tiny">Value
                                  </TD>
                                </TR>';      
                                $total = 0;
                                while ($row = MySQL_Fetch_Array($resultSet))
                                {             
                                  echo '<TR>';
                                    if ($all)
                                    echo '<TD class="rowA center">'.GetPayment($row['ReimbursiveAllowance_Payment']).'
                                          </TD>';
                                    echo '<TD class="rowA">'.GetTextualDateFromDatabaseDate($row['ReimbursiveAllowance_Date']).'
                                          </TD>
                                          <TD class="rowA">'.$row['ReimbursiveAllowance_Description'].'
                                          </TD>
                                          <TD class="rowA">R'.SPrintF('%02.2f', $row['ReimbursiveAllowance_Value']).'
                                          </TD>
                                        </TR>';    
                                  $total += $row['ReimbursiveAllowance_Value'];
                                } 
                                if ($all)
                                  echo '<TR>
                                          <TD colspan="3" class="rowB bold">Total:
                                          </TD>';
                                else
                                  echo '<TR>
                                          <TD colspan="2" class="rowB bold">Total:
                                          </TD>';
                            echo '<TD class="rowB center bold">R'.SPrintF('%02.2f', $total).'
                                  </TD>                             
                                </TR>     
                              </TABLE>
                            </DIV>';  
                    } else                      
                      echo '<DIV class="contentflow">
                              <P>There are no reimbursements listed for the given date range.</P>
                            </DIV>';  
                    break;
                }
                break;
              case '3': 
                switch ($_SESSION['ViewReportExpenses'][3])
                {       
                  case "":   
                    if ($_SESSION['ViewReportExpenses'][4] == 2)
                    {
                      BuildContentHeader('Shift Allowance Report ('.$reportType.')', "", "", false);
                      $query = 'SELECT * FROM ShiftAllowance WHERE ShiftAllowance_Payment '.$payment.' LIMIT 1';
                    } else
                    {
                      BuildContentHeader('Shift Allowance Report ('.$reportType.') - '.$dateRange, "", "", false);
                      $query = 'SELECT * FROM ShiftAllowance WHERE ShiftAllowance_Payment '.$payment.' AND ShiftAllowance_Date BETWEEN "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][0]).'" AND "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][1]).' 23:59:59" LIMIT 1';
                    }
                    if (MySQL_Num_Rows(ExecuteQuery($query)) > 0)
                    {     
                      echo '<DIV class="contentflow">
                              <P>These are the shift allowances listed.</P>
                              <BR/><BR/>';
                              
                      $resultSet = ExecuteQuery('SELECT * FROM Staff WHERE Staff_IsEmployee = "1" ORDER BY Staff_First_Name, Staff_Last_Name');
                      while ($row = MySQL_Fetch_Array($resultSet))
                      { 
                        echo '<TABLE cellspacing="5" align="center" class="short" style="page-break-after: always">
                                <TR>';
                                if ($all)
                                  echo '<TD colspan="3" class="header">';
                                else
                                  echo '<TD colspan="2" class="header">';
                                echo $row['Staff_First_Name'].' '.$row['Staff_Last_Name'].'
                                      </TD>
                                    </TR>';
                                //$total = 0;
                                $shiftAfternoonTotal = 0;
                                $shiftNightTotal = 0;
                                if ($_SESSION['ViewReportExpenses'][4] == 2)
                                  $resultSetTemp = ExecuteQuery('SELECT * FROM ShiftAllowance WHERE ShiftAllowance_Payment '.$payment.' AND ShiftAllowance_Name = '.$row['Staff_Code'].' ORDER BY ShiftAllowance_Date');
                                else
                                  $resultSetTemp = ExecuteQuery('SELECT * FROM ShiftAllowance WHERE ShiftAllowance_Payment '.$payment.' AND ShiftAllowance_Name = '.$row['Staff_Code'].' AND ShiftAllowance_Date BETWEEN "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][0]).'" AND "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][1]).' 23:59:59" ORDER BY ShiftAllowance_Date'); 
                                if (MySQL_Num_Rows($resultSetTemp) > 0)
                                {
                                  echo '<TR>';
                                  if ($all)
                                    echo '<TD class="subheader veryshort">Payment
                                          </TD>';
                                    echo '<TD class="subheader short">Date
                                          </TD>
                                          <TD class="subheader short">Shift
                                          </TD>
                                        </TR>';        
                                        while ($rowTemp = MySQL_Fetch_Array($resultSetTemp))
                                        {             
                                          $rowTempB = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM ShiftAllowanceType WHERE ShiftAllowanceType_ID = "'.$rowTemp['ShiftAllowance_Shift'].'"'));
                                          if ($rowTemp['ShiftAllowance_Shift'] == 1)
                                            $shiftAfternoonTotal += 1;
                                          elseif ($rowTemp['ShiftAllowance_Shift'] == 2)
                                            $shiftNightTotal += 1;
                                          echo '<TR>';
                                          if ($all)
                                            echo '<TD class="rowA center">'.GetPayment($rowTemp['ShiftAllowance_Payment']).'
                                                  </TD>';
                                            echo '<TD class="rowA">'.GetTextualDateFromDatabaseDate($rowTemp['ShiftAllowance_Date']).'
                                                  </TD>
                                                  <TD class="rowA">'.$rowTempB['ShiftAllowanceType_Description'].'
                                                  </TD>
                                                </TR>'; 
                                          //$total += $rowTemp['ShiftAllowance_Value'];
                                        }
                                }
                                if ($all)
                                  echo '<TR>
                                          <TD colspan="2" class="rowB bold">Shift AF:
                                          </TD>
                                          <TD class="rowB bold">'.$shiftAfternoonTotal.'
                                          </TD>
                                        </TR>
                                        <TR>
                                          <TD colspan="2" class="rowB bold">Shift NI:
                                          </TD>
                                          <TD class="rowB bold">'.$shiftNightTotal.'
                                          </TD>';
                                else
                                  echo '<TR>
                                          <TD class="rowB bold">Shift AF:
                                          </TD>
                                          <TD class="rowB bold">'.$shiftAfternoonTotal.'
                                          </TD>
                                        </TR>
                                        <TR>
                                          <TD class="rowB bold">Shift NI:
                                          </TD>
                                          <TD class="rowB bold">'.$shiftNightTotal.'
                                          </TD>';
                          echo '</TR>     
                              </TABLE>
                              <BR/><BR/>';     
                      } 
                      echo '</DIV>';
                    } else                      
                      echo '<DIV class="contentflow">
                              <P>There are no shift allowances listed for the given date range.</P>
                            </DIV>';    
                    break;
                  default:
                    $shiftAfternoonTotal = 0;
                    $shiftNightTotal = 0;
                    if ($_SESSION['ViewReportExpenses'][4] == 2)
                    {
                      BuildContentHeader('Shift Allowance Report ('.$reportType.') - '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'], "", "", false);
                      $resultSet = ExecuteQuery('SELECT * FROM ShiftAllowance WHERE ShiftAllowance_Payment '.$payment.' AND ShiftAllowance_Name = "'.$_SESSION['ViewReportExpenses'][3].'" ORDER BY ShiftAllowance_Date');
                    } else
                    {  BuildContentHeader('Shift Allowance Report ('.$reportType.') - '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].' for '.$dateRange, "", "", false);
                      $resultSet = ExecuteQuery('SELECT * FROM ShiftAllowance WHERE ShiftAllowance_Payment '.$payment.' AND ShiftAllowance_Name = "'.$_SESSION['ViewReportExpenses'][3].'" AND ShiftAllowance_Date BETWEEN "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][0]).'" AND "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][1]).' 23:59:59" ORDER BY ShiftAllowance_Date'); 
                    }
                    if (MySQL_Num_Rows($resultSet) > 0)
                    { 
                      echo '<DIV class="contentflow">
                              <P>These are the shift allowances listed.</P>
                              <BR/><BR/>
                              <TABLE cellspacing="5" align="center" class="short">
                                <TR>';
                                if ($all)
                                  echo '<TD colspan="3" class="header">Shift Allowance Details
                                        </TD>
                                      </TR>
                                      <TR>
                                        <TD class="subheader veryshort">Payment
                                        </TD>';
                                else
                                  echo '<TD colspan="2" class="header">Shift Allowance Details
                                        </TD>
                                      </TR>
                                      <TR>';
                            echo '<TD class="subheader short">Date
                                  </TD>
                                  <TD class="subheader short">Shift
                                  </TD>
                                </TR>';
                                $total = 0;
                                while ($row = MySQL_Fetch_Array($resultSet))
                                {             
                                  $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM ShiftAllowanceType WHERE ShiftAllowanceType_ID = "'.$row['ShiftAllowance_Shift'].'"'));
                                  if ($row['ShiftAllowance_Shift'] == 1)
                                    $shiftAfternoonTotal += 1;
                                  elseif ($row['ShiftAllowance_Shift'] == 2)
                                    $shiftNightTotal += 1;
                                  echo '<TR>';
                                    if ($all)
                                    echo '<TD class="rowA center">'.GetPayment($row['ShiftAllowance_Payment']).'
                                          </TD>';
                                    echo '<TD class="rowA">'.GetTextualDateFromDatabaseDate($row['ShiftAllowance_Date']).'
                                          </TD>
                                          <TD class="rowA">'.$rowTemp['ShiftAllowanceType_Description'].'
                                          </TD>
                                        </TR>';   
                                  $total += $row['ShiftAllowance_Value'];
                                }
                                if ($all)
                                  echo '<TR>
                                          <TD colspan="2" class="rowB bold">Shift AF:
                                          </TD>
                                          <TD class="rowB bold">'.$shiftAfternoonTotal.'
                                          </TD>
                                        </TR>
                                        <TR>
                                          <TD colspan="2" class="rowB bold">Shift NI:
                                          </TD>
                                          <TD class="rowB bold">'.$shiftNightTotal.'
                                          </TD>';
                                else
                                  echo '<TR>
                                          <TD class="rowB bold">Shift AF:
                                          </TD>
                                          <TD class="rowB bold">'.$shiftAfternoonTotal.'
                                          </TD>
                                        </TR>
                                        <TR>
                                          <TD class="rowB bold">Shift NI:
                                          </TD>
                                          <TD class="rowB bold">'.$shiftNightTotal.'
                                          </TD>';
                          echo '</TR>     
                              </TABLE>
                            </DIV>';  
                    } else                      
                      echo '<DIV class="contentflow">
                              <P>There are no shift allowances listed for the given date range.</P>
                            </DIV>';  
                    break;
                }
                break;
              case '4': 
                switch ($_SESSION['ViewReportExpenses'][3])
                {       
                  case "":    
                    if ($_SESSION['ViewReportExpenses'][4] == 2)
                    {
                      BuildContentHeader('Standby Allowance Report ('.$reportType.')', "", "", false);
                      $query = 'SELECT * FROM Standby WHERE Standby_Payment '.$payment.' LIMIT 1';
                    } else
                    {
                      BuildContentHeader('Standby Allowance Report ('.$reportType.') - '.$dateRange, "", "", false);
                      $query = 'SELECT * FROM Standby WHERE Standby_Payment '.$payment.' AND Standby_DateStart BETWEEN "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][0]).'" AND "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][1]).' 23:59:59" LIMIT 1';
                    }
                    if (MySQL_Num_Rows(ExecuteQuery($query)) > 0)
                    {     
                      echo '<DIV class="contentflow">
                              <P>These are the standby allowances listed.</P>
                              <BR/><BR/>';
                              
                      $resultSet = ExecuteQuery('SELECT * FROM Staff WHERE Staff_IsEmployee = "1" ORDER BY Staff_First_Name, Staff_Last_Name');
                      while ($row = MySQL_Fetch_Array($resultSet))
                      { 
                        echo '<TABLE cellspacing="5" align="center" class="long" style="page-break-after: always">
                                <TR>';
                                if ($all)
                                  echo '<TD colspan="5" class="header">';
                                else
                                  echo '<TD colspan="4" class="header">';
                                echo $row['Staff_First_Name'].' '.$row['Staff_Last_Name'].'
                                      </TD>
                                    </TR>';
                                $total = 0;
                                if ($_SESSION['ViewReportExpenses'][4] == 2)
                                  $resultSetTemp = ExecuteQuery('SELECT * FROM Standby WHERE Standby_Payment '.$payment.' AND Standby_Name = "'.$row['Staff_Code'].'" ORDER BY Standby_DateStart');
                                else
                                  $resultSetTemp = ExecuteQuery('SELECT * FROM Standby WHERE Standby_Payment '.$payment.' AND Standby_Name = "'.$row['Staff_Code'].'" AND Standby_DateStart BETWEEN "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][0]).'" AND "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][1]).' 23:59:59" ORDER BY Standby_DateStart'); 
                                if (MySQL_Num_Rows($resultSetTemp) > 0)
                                { 
                                  echo '<TR>';
                                  if ($all)
                                    echo '<TD class="subheader veryshort">Payment
                                          </TD>';
                                    echo '<TD class="subheader veryshort">Date
                                          </TD>
                                          <TD class="subheader standard">Type
                                          </TD>
                                          <TD class="subheader standard">Description
                                          </TD>
                                          <TD class="subheader">Value
                                          </TD>
                                        </TR>';         
                                        while ($rowTemp = MySQL_Fetch_Array($resultSetTemp))
                                        {             
                                          $rowTempB = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM StandbyType WHERE StandbyType_ID = "'.$rowTemp['Standby_Type'].'"'));
                                          echo '<TR>';
                                          if ($all)
                                            echo '<TD class="rowA center">'.GetPayment($rowTemp['Standby_Payment']).'
                                                  </TD>';
                                            echo '<TD class="rowA">'.GetTextualDateFromDatabaseDate($rowTemp['Standby_DateStart']).'
                                                  </TD>
                                                  <TD class="rowA">'.$rowTempB['StandbyType_Description'].'
                                                  </TD>
                                                  <TD class="rowA">'.$rowTemp['Standby_Comments'].'
                                                  </TD>
                                                  <TD class="rowA center">R'.SPrintF('%02.2f', $rowTemp['Standby_Value']).'
                                                  </TD>
                                                </TR>'; 
                                          $total += $rowTemp['Standby_Value'];
                                        }
                                }
                                if ($all)
                                  echo '<TR>
                                          <TD colspan="4" class="rowB bold">Total:
                                          </TD>';
                                else
                                  echo '<TR>
                                          <TD colspan="3" class="rowB bold">Total:
                                          </TD>';
                            echo '<TD class="rowB center bold tiny">R'.SPrintF('%02.2f', $total).'
                                  </TD>
                                </TR>     
                              </TABLE>
                              <BR/><BR/>';     
                      } 
                      echo '</DIV>';
                    } else                      
                      echo '<DIV class="contentflow">
                              <P>There are no standby allowances listed for the given date range.</P>
                            </DIV>';    
                    break;
                  default:
                    if ($_SESSION['ViewReportExpenses'][4] == 2)
                    {
                      BuildContentHeader('Standby Allowance Report ('.$reportType.') - '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'], "", "", false);
                      $resultSet = ExecuteQuery('SELECT * FROM Standby WHERE Standby_Payment '.$payment.' AND Standby_Name = "'.$_SESSION['ViewReportExpenses'][3].'" ORDER BY Standby_DateStart');
                    } else
                    {  BuildContentHeader('Standby Allowance Report ('.$reportType.') - '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].' for '.$dateRange, "", "", false);
                      $resultSet = ExecuteQuery('SELECT * FROM Standby WHERE Standby_Payment '.$payment.' AND Standby_Name = "'.$_SESSION['ViewReportExpenses'][3].'" AND Standby_DateStart BETWEEN "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][0]).'" AND "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][1]).' 23:59:59" ORDER BY Standby_DateStart'); 
                    }
                    if (MySQL_Num_Rows($resultSet) > 0)
                    { 
                      echo '<DIV class="contentflow">
                              <P>These are the standby allowances listed.</P>
                              <BR/><BR/>
                              <TABLE cellspacing="5" align="center" class="long">
                                <TR>';
                                if ($all)
                                  echo '<TD colspan="5" class="header">Standby Allowance Details
                                        </TD>
                                      </TR>
                                      <TR>
                                        <TD class="subheader veryshort">Payment
                                        </TD>';
                                else
                                  echo '<TD colspan="4" class="header">Standby Allowance Details
                                        </TD>
                                      </TR>
                                      <TR>';
                            echo '<TD class="subheader veryshort">Date
                                  </TD>
                                  <TD class="subheader standard">Type
                                  </TD>
                                  <TD class="subheader">Description
                                  </TD>
                                  <TD class="subheader">Value
                                  </TD>
                                </TR>';        
                                $total = 0;
                                while ($row = MySQL_Fetch_Array($resultSet))
                                {             
                                  $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM StandbyType WHERE StandbyType_ID = "'.$row['Standby_Type'].'"'));
                                  echo '<TR>';
                                    if ($all)
                                    echo '<TD class="rowA center">'.GetPayment($row['Standby_Payment']).'
                                          </TD>';
                                    echo '<TD class="rowA">'.GetTextualDateFromDatabaseDate($row['Standby_DateStart']).'
                                          </TD>
                                          <TD class="rowA">'.$rowTemp['StandbyType_Description'].'
                                          </TD>
                                          <TD class="rowA">'.$row['Standby_Comments'].'
                                          </TD>
                                          <TD class="rowA center">R'.SPrintF('%02.2f', $row['Standby_Value']).'
                                          </TD>
                                        </TR>';    
                                  $total += $row['Standby_Value'];
                                }
                                if ($all)
                                  echo '<TR>
                                          <TD colspan="4" class="rowB bold">Total:
                                          </TD>';
                                else
                                  echo '<TR>
                                          <TD colspan="3" class="rowB bold">Total:
                                          </TD>';
                            echo '<TD class="rowB center bold">R'.SPrintF('%02.2f', $total).'
                                  </TD>                             
                                </TR>     
                              </TABLE>
                            </DIV>';  
                    } else                      
                      echo '<DIV class="contentflow">
                              <P>There are no standby allowances listed for the given date range.</P>
                            </DIV>';
                    break;
                }
                break;
              case '5':                
                switch ($_SESSION['ViewReportExpenses'][3])
                {       
                  case "":    
                    if ($_SESSION['ViewReportExpenses'][4] == 2)
                      BuildContentHeader('Expense Summary ('.$reportType.')', "", "", false);
                    else
                      BuildContentHeader('Expense Summary ('.$reportType.') - '.$dateRange, "", "", false);
                    echo '<DIV class="contentflow">
                            <P class="hide">This is the expenses summary.</P>
                            <BR/><BR/>
                            <TABLE cellspacing="5" align="center" class="long">
                              <TR>
                                <TD colspan="10" class="header">Expense Details
                                </TD>
                              </TR>
                              <TR>
                              <TD class="subheader short">Staff Name
                              </TD>
                              <TD class="subheader veryshort">Deductions
                              </TD>
                              <TD class="subheader veryshort">Overtime
                              </TD>
                              <TD class="subheader veryshort">Reimbursements
                              </TD>
                              <TD class="subheader veryshort">Shift AF
                              </TD>
                              <TD class="subheader veryshort">Shift NI
                              </TD>
                              <TD class="subheader veryshort">Standby Allowances
                              </TD>
                              <TD class="subheader veryshort">Travel Allowances
                              </TD>
                              <TD class="subheader veryshort">Out-of-Town Allowances
                              </TD>
                              <TD class="subheader veryshort bold">Total
                              </TD>
                            </TR>';
                            $deductionTotal = 0;
                            $overtimeTotal = 0;
                            $reimbursementTotal = 0;
                            $shiftAfternoonTotal = 0;
                            $shiftNightTotal = 0;
                            $standbyTotal = 0;
                            $travelTotal = 0; 
                            $totalTotal = 0;
                            $outTotal = 0;
                            $resultSet = ExecuteQuery('SELECT * FROM Staff WHERE Staff_IsEmployee = "1" ORDER BY Staff_First_Name, Staff_Last_Name');
                            while ($row = MySQL_Fetch_Array($resultSet))
                            { 
                              echo '<TR>
                                      <TD class="rowA">'.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].'
                                      </TD>';
                                    $total = 0;
                                    $subTotal = 0;
                                    if ($_SESSION['ViewReportExpenses'][4] == 2)
                                      $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT SUM(Deduction_Value) AS Deductions FROM Deduction WHERE Deduction_Payment '.$payment.' AND Deduction_Name = '.$row['Staff_Code'].''));
                                    else
                                      $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT SUM(Deduction_Value) AS Deductions FROM Deduction WHERE Deduction_Payment '.$payment.' AND Deduction_Name = '.$row['Staff_Code'].' AND Deduction_Date BETWEEN "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][0]).'" AND "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][1]).' 23:59:59"'));
                                    if ($rowTemp['Deductions'] == "")
                                      $subTotal = 0; 
                                    else
                                      $subTotal = $rowTemp['Deductions']; 
                                    $deductionTotal -= $subTotal;
                                    $total -= $subTotal;
                                echo '<TD class="rowA center">R'.SPrintF('%02.2f', -$subTotal).'
                                      </TD>';
                                    
                                    if ($_SESSION['ViewReportExpenses'][4] == 2)
                                      $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT SUM(Overtime_Hours) AS Hours FROM Overtime WHERE Overtime_Payment '.$payment.' AND Overtime_Name = '.$row['Staff_Code'].''));
                                    else
                                      $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT SUM(Overtime_Hours) AS Hours FROM Overtime WHERE Overtime_Payment '.$payment.' AND Overtime_Name = '.$row['Staff_Code'].' AND Overtime_Start BETWEEN "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][0]).'" AND "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][1]).' 23:59:59"')); 
                                    if ($rowTemp['Hours'] == "")
                                      $subTotal = 0; 
                                    else
                                      $subTotal = $rowTemp['Hours'];
                                    $overtimeTotal += $subTotal;
                                echo '<TD class="rowA center">'.SPrintF('%02.2f', $subTotal).'
                                      </TD>';
                                    
                                    if ($_SESSION['ViewReportExpenses'][4] == 2)
                                      $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT SUM(ReimbursiveAllowance_Value) AS ReimbursiveAllowances FROM ReimbursiveAllowance WHERE ReimbursiveAllowance_Payment '.$payment.' AND ReimbursiveAllowance_Name = '.$row['Staff_Code'].''));
                                    else
                                      $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT SUM(ReimbursiveAllowance_Value) AS ReimbursiveAllowances FROM ReimbursiveAllowance WHERE ReimbursiveAllowance_Payment '.$payment.' AND ReimbursiveAllowance_Name = '.$row['Staff_Code'].' AND ReimbursiveAllowance_Date BETWEEN "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][0]).'" AND "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][1]).' 23:59:59"')); 
                                    if ($rowTemp['ReimbursiveAllowances'] == "")
                                      $subTotal = 0; 
                                    else
                                      $subTotal = $rowTemp['ReimbursiveAllowances'];
                                    $reimbursementTotal += $subTotal;
                                    $total += $subTotal;
                                echo '<TD class="rowA center">R'.SPrintF('%02.2f', $subTotal).'
                                      </TD>';
                                    
                                    /*if ($_SESSION['ViewReportExpenses'][4] == 2)
                                      $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT SUM(ShiftAllowance_Value) AS ShiftAllowances FROM ShiftAllowance WHERE ShiftAllowance_Payment '.$payment.' AND ShiftAllowance_Name = '.$row['Staff_Code'].''));
                                    else
                                      $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT SUM(ShiftAllowance_Value) AS ShiftAllowances FROM ShiftAllowance WHERE ShiftAllowance_Payment '.$payment.' AND ShiftAllowance_Name = '.$row['Staff_Code'].' AND ShiftAllowance_Date BETWEEN "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][0]).'" AND "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][1]).' 23:59:59"')); 
                                    if ($rowTemp['ShiftAllowances'] == "")
                                      $subTotal = 0; 
                                    else
                                      $subTotal = $rowTemp['ShiftAllowances']; 
                                    $shiftTotal += $subTotal;
                                    $total += $subTotal;
                                echo '<TD class="rowA center">R'.SPrintF('%02.2f', $subTotal).'
                                      </TD>';*/
                                    if ($_SESSION['ViewReportExpenses'][4] == 2)
                                      $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT COUNT(*) AS ShiftAllowances FROM ShiftAllowance WHERE ShiftAllowance_Payment '.$payment.' AND ShiftAllowance_Name = '.$row['Staff_Code'].' AND ShiftAllowance_Shift = "1"'));
                                    else
                                      $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT COUNT(*) AS ShiftAllowances FROM ShiftAllowance WHERE ShiftAllowance_Payment '.$payment.' AND ShiftAllowance_Name = '.$row['Staff_Code'].' AND ShiftAllowance_Date BETWEEN "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][0]).'" AND "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][1]).' 23:59:59" AND ShiftAllowance_Shift = "1"')); 
                                    $subTotal = $rowTemp['ShiftAllowances']; 
                                    $shiftAfternoonTotal += $subTotal;
                                echo '<TD class="rowA center">'.$subTotal.'
                                      </TD>';
                                    if ($_SESSION['ViewReportExpenses'][4] == 2)
                                      $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT COUNT(*) AS ShiftAllowances FROM ShiftAllowance WHERE ShiftAllowance_Payment '.$payment.' AND ShiftAllowance_Name = '.$row['Staff_Code'].' AND ShiftAllowance_Shift = "2"'));
                                    else
                                      $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT COUNT(*) AS ShiftAllowances FROM ShiftAllowance WHERE ShiftAllowance_Payment '.$payment.' AND ShiftAllowance_Name = '.$row['Staff_Code'].' AND ShiftAllowance_Date BETWEEN "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][0]).'" AND "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][1]).' 23:59:59" AND ShiftAllowance_Shift = "2"')); 
                                    $subTotal = $rowTemp['ShiftAllowances']; 
                                    $shiftNightTotal += $subTotal;
                                echo '<TD class="rowA center">'.$subTotal.'
                                      </TD>';
                                    
                                    if ($_SESSION['ViewReportExpenses'][4] == 2)
                                      $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT SUM(Standby_Value) AS StandbyAllowances FROM Standby WHERE Standby_Payment '.$payment.' AND Standby_Name = '.$row['Staff_Code'].''));
                                    else
                                      $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT SUM(Standby_Value) AS StandbyAllowances FROM Standby WHERE Standby_Payment '.$payment.' AND Standby_Name = '.$row['Staff_Code'].' AND Standby_DateStart BETWEEN "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][0]).'" AND "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][1]).' 23:59:59"')); 
                                    if ($rowTemp['StandbyAllowances'] == "")
                                      $subTotal = 0; 
                                    else
                                      $subTotal = $rowTemp['StandbyAllowances']; 
                                    $standbyTotal += $subTotal;
                                    $total += $subTotal;
                                echo '<TD class="rowA center">R'.SPrintF('%02.2f', $subTotal).'
                                      </TD>';
                                    
                                    if ($_SESSION['ViewReportExpenses'][4] == 2)
                                      $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT SUM(TravelAllowance_Value) AS TravelAllowances FROM TravelAllowance WHERE TravelAllowance_Payment '.$payment.' AND TravelAllowance_Name = '.$row['Staff_Code'].''));
                                    else
                                      $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT SUM(TravelAllowance_Value) AS TravelAllowances FROM TravelAllowance WHERE TravelAllowance_Payment '.$payment.' AND TravelAllowance_Name = '.$row['Staff_Code'].' AND TravelAllowance_Date BETWEEN "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][0]).'" AND "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][1]).' 23:59:59"')); 
                                    if ($rowTemp['TravelAllowances'] == "")
                                      $subTotal = 0; 
                                    else
                                      $subTotal = $rowTemp['TravelAllowances']; 
                                    $travelTotal += $subTotal;
                                    $total += $subTotal;
                                echo '<TD class="rowA center">R'.SPrintF('%02.2f', $subTotal).'
                                      </TD>';
                                    
                                    if ($_SESSION['ViewReportExpenses'][4] == 2)
                                      $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT SUM(OutOfTownAllowance_Value) AS OutOfTownAllowances FROM OutOfTownAllowance WHERE OutOfTownAllowance_Payment '.$payment.' AND OutOfTownAllowance_Name = '.$row['Staff_Code'].''));
                                    else
                                      $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT SUM(OutOfTownAllowance_Value) AS OutOfTownAllowances FROM OutOfTownAllowance WHERE OutOfTownAllowance_Payment '.$payment.' AND OutOfTownAllowance_Name = '.$row['Staff_Code'].' AND OutOfTownAllowance_Date_Start BETWEEN "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][0]).'" AND "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][1]).' 23:59:59"')); 
                                    if ($rowTemp['OutOfTownAllowances'] == "")
                                      $subTotal = 0; 
                                    else
                                      $subTotal = $rowTemp['OutOfTownAllowances']; 
                                    $outTotal += $subTotal;
                                    $total += $subTotal;
                                    
                                    $totalTotal += $total;
                                echo '<TD class="rowA center">R'.SPrintF('%02.2f', $subTotal).'
                                      </TD>
                                      <TD class="rowB center bold">R'.SPrintF('%02.2f', $total).'
                                      </TD>
                                    </TR>';    
                            } 
                      echo '<TR>
                              <TD class="rowB bold">Total:
                              </TD>
                              <TD class="rowB center bold">R'.SPrintF('%02.2f', $deductionTotal).'
                              </TD>
                              <TD class="rowB center bold">'.SPrintF('%02.2f', $overtimeTotal).'
                              </TD>
                              <TD class="rowB center bold">R'.SPrintF('%02.2f', $reimbursementTotal).'
                              </TD>
                              <TD class="rowB center bold">'.$shiftAfternoonTotal.'
                              </TD>
                              <TD class="rowB center bold">'.$shiftNightTotal.'
                              </TD>
                              <TD class="rowB center bold">R'.SPrintF('%02.2f', $standbyTotal).'
                              </TD>
                              <TD class="rowB center bold">R'.SPrintF('%02.2f', $travelTotal).'
                              </TD>
                              <TD class="rowB center bold">R'.SPrintF('%02.2f', $outTotal).'
                              </TD>
                              <TD class="rowB center bold">R'.SPrintF('%02.2f', $totalTotal).'
                              </TD>
                            </TR>
                          </TABLE>
                        </DIV>';
                    break;
                  default:
                    if ($_SESSION['ViewReportExpenses'][4] == 2)
                      BuildContentHeader('Expense Summary ('.$reportType.') - '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'], "", "", false);
                    else
                      BuildContentHeader('Expense Summary ('.$reportType.') - '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].' for '.$dateRange, "", "", false);
                    echo '<DIV class="contentflow">
                            <P class="hide">This is the expenses summary.</P>
                            <BR/><BR/>
                            <TABLE cellspacing="5" align="center" class="long">
                              <TR>
                                <TD colspan="9" class="header">Expense Details
                                </TD>
                              </TR>
                              <TR>
                                <TD class="subheader veryshort">Deductions
                                </TD>
                                <TD class="subheader veryshort">Overtime
                                </TD>
                                <TD class="subheader veryshort">Reimbursements
                                </TD>
                                <TD class="subheader veryshort">Shift AF
                                </TD>
                                <TD class="subheader veryshort">Shift NI
                                </TD>
                                <TD class="subheader veryshort">Standby Allowances
                                </TD>
                                <TD class="subheader veryshort">Travel Allowances
                                </TD>
                                <TD class="subheader veryshort">Out-of-Town Allowances
                                </TD>
                                <TD class="subheader veryshort">Total
                                </TD>
                              </TR>
                              <TR>';
                              $total = 0;
                              $subTotal = 0;
                              if ($_SESSION['ViewReportExpenses'][4] == 2)
                                $row = MySQL_Fetch_Array(ExecuteQuery('SELECT SUM(Deduction_Value) AS Deductions FROM Deduction WHERE Deduction_Payment '.$payment.' AND Deduction_Name = '.$_SESSION['ViewReportExpenses'][3].''));
                              else
                                $row = MySQL_Fetch_Array(ExecuteQuery('SELECT SUM(Deduction_Value) AS Deductions FROM Deduction WHERE Deduction_Payment '.$payment.' AND Deduction_Name = '.$_SESSION['ViewReportExpenses'][3].' AND Deduction_Date BETWEEN "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][0]).'" AND "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][1]).' 23:59:59"'));
                              if ($row['Deductions'] == "")
                                $subTotal = 0; 
                              else
                                $subTotal = $row['Deductions']; 
                              $total -= $subTotal;
                          echo '<TD class="rowA center">R'.SPrintF('%02.2f', -$subTotal).'
                                </TD>';
                              
                              if ($_SESSION['ViewReportExpenses'][4] == 2)
                                $row = MySQL_Fetch_Array(ExecuteQuery('SELECT SUM(Overtime_Hours) AS Hours FROM Overtime WHERE Overtime_Payment '.$payment.' AND Overtime_Name = '.$_SESSION['ViewReportExpenses'][3].''));
                              else
                                $row = MySQL_Fetch_Array(ExecuteQuery('SELECT SUM(Overtime_Hours) AS Hours FROM Overtime WHERE Overtime_Payment '.$payment.' AND Overtime_Name = '.$_SESSION['ViewReportExpenses'][3].' AND Overtime_Start BETWEEN "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][0]).'" AND "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][1]).' 23:59:59"'));
                              if ($row['Hours'] == "")
                                $subTotal = 0; 
                              else
                                $subTotal = $row['Hours'];
                          echo '<TD class="rowA center">'.SPrintF('%02.2f', $subTotal).'
                                </TD>';
                              
                              if ($_SESSION['ViewReportExpenses'][4] == 2)
                                $row = MySQL_Fetch_Array(ExecuteQuery('SELECT SUM(ReimbursiveAllowance_Value) AS ReimbursiveAllowances FROM ReimbursiveAllowance WHERE ReimbursiveAllowance_Payment '.$payment.' AND ReimbursiveAllowance_Name = '.$_SESSION['ViewReportExpenses'][3].''));
                              else
                                $row = MySQL_Fetch_Array(ExecuteQuery('SELECT SUM(ReimbursiveAllowance_Value) AS ReimbursiveAllowances FROM ReimbursiveAllowance WHERE ReimbursiveAllowance_Payment '.$payment.' AND ReimbursiveAllowance_Name = '.$_SESSION['ViewReportExpenses'][3].' AND ReimbursiveAllowance_Date BETWEEN "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][0]).'" AND "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][1]).' 23:59:59"')); 
                              if ($row['ReimbursiveAllowances'] == "")
                                $subTotal = 0; 
                              else
                                $subTotal = $row['ReimbursiveAllowances'];  
                              $total += $subTotal;
                          echo '<TD class="rowA center">R'.SPrintF('%02.2f', $subTotal).'
                                </TD>';
                                    
                              /*if ($_SESSION['ViewReportExpenses'][4] == 2)
                                $row = MySQL_Fetch_Array(ExecuteQuery('SELECT SUM(ShiftAllowance_Value) AS ShiftAllowances FROM ShiftAllowance WHERE ShiftAllowance_Payment '.$payment.' AND ShiftAllowance_Name = '.$_SESSION['ViewReportExpenses'][3].''));
                              else
                                $row = MySQL_Fetch_Array(ExecuteQuery('SELECT SUM(ShiftAllowance_Value) AS ShiftAllowances FROM ShiftAllowance WHERE ShiftAllowance_Payment '.$payment.' AND ShiftAllowance_Name = '.$_SESSION['ViewReportExpenses'][3].' AND ShiftAllowance_Date BETWEEN "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][0]).'" AND "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][1]).' 23:59:59"')); 
                              if ($row['ShiftAllowances'] == "")
                                $subTotal = 0; 
                              else
                                $subTotal = $row['ShiftAllowances'];  
                              $total += $subTotal;
                          echo '<TD class="rowA center">R'.SPrintF('%02.2f', $subTotal).'
                                </TD>';*/
                                if ($_SESSION['ViewReportExpenses'][4] == 2)
                                  $row = MySQL_Fetch_Array(ExecuteQuery('SELECT COUNT(*) AS ShiftAllowances FROM ShiftAllowance WHERE ShiftAllowance_Payment '.$payment.' AND ShiftAllowance_Name = '.$_SESSION['ViewReportExpenses'][3].' AND ShiftAllowance_Shift = "1"'));
                                else
                                  $row = MySQL_Fetch_Array(ExecuteQuery('SELECT COUNT(*) AS ShiftAllowances FROM ShiftAllowance WHERE ShiftAllowance_Payment '.$payment.' AND ShiftAllowance_Name = '.$_SESSION['ViewReportExpenses'][3].' AND ShiftAllowance_Date BETWEEN "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][0]).'" AND "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][1]).' 23:59:59" AND ShiftAllowance_Shift = "1"')); 
                          echo '<TD class="rowA center">'.$row['ShiftAllowances'].'
                                </TD>';
                                if ($_SESSION['ViewReportExpenses'][4] == 2)
                                  $row = MySQL_Fetch_Array(ExecuteQuery('SELECT COUNT(*) AS ShiftAllowances FROM ShiftAllowance WHERE ShiftAllowance_Payment '.$payment.' AND ShiftAllowance_Name = '.$_SESSION['ViewReportExpenses'][3].' AND ShiftAllowance_Shift = "2"'));
                                else
                                  $row = MySQL_Fetch_Array(ExecuteQuery('SELECT COUNT(*) AS ShiftAllowances FROM ShiftAllowance WHERE ShiftAllowance_Payment '.$payment.' AND ShiftAllowance_Name = '.$_SESSION['ViewReportExpenses'][3].' AND ShiftAllowance_Date BETWEEN "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][0]).'" AND "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][1]).' 23:59:59" AND ShiftAllowance_Shift = "2"')); 
                          echo '<TD class="rowA center">'.$row['ShiftAllowances'].'
                                </TD>';
                              
                              if ($_SESSION['ViewReportExpenses'][4] == 2)
                                $row = MySQL_Fetch_Array(ExecuteQuery('SELECT SUM(Standby_Value) AS StandbyAllowances FROM Standby WHERE Standby_Payment '.$payment.' AND Standby_Name = '.$_SESSION['ViewReportExpenses'][3].''));
                              else
                                $row = MySQL_Fetch_Array(ExecuteQuery('SELECT SUM(Standby_Value) AS StandbyAllowances FROM Standby WHERE Standby_Payment '.$payment.' AND Standby_Name = '.$_SESSION['ViewReportExpenses'][3].' AND Standby_DateStart BETWEEN "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][0]).'" AND "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][1]).' 23:59:59"')); 
                              if ($row['StandbyAllowances'] == "")
                                $subTotal = 0; 
                              else
                                $subTotal = $row['StandbyAllowances']; 
                              $total += $subTotal;
                          echo '<TD class="rowA center">R'.SPrintF('%02.2f', $subTotal).'
                                </TD>';
                              
                              if ($_SESSION['ViewReportExpenses'][4] == 2)
                                $row = MySQL_Fetch_Array(ExecuteQuery('SELECT SUM(TravelAllowance_Value) AS TravelAllowances FROM TravelAllowance WHERE TravelAllowance_Payment '.$payment.' AND TravelAllowance_Name = '.$_SESSION['ViewReportExpenses'][3].''));
                              else
                                $row = MySQL_Fetch_Array(ExecuteQuery('SELECT SUM(TravelAllowance_Value) AS TravelAllowances FROM TravelAllowance WHERE TravelAllowance_Payment '.$payment.' AND TravelAllowance_Name = '.$_SESSION['ViewReportExpenses'][3].' AND TravelAllowance_Date BETWEEN "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][0]).'" AND "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][1]).' 23:59:59"')); 
                              if ($row['TravelAllowances'] == "")
                                $subTotal = 0; 
                              else
                                $subTotal = $row['TravelAllowances']; 
                              $total += $subTotal;
                          echo '<TD class="rowA center">R'.SPrintF('%02.2f', $subTotal).'
                                </TD>';
                              
                              if ($_SESSION['ViewReportExpenses'][4] == 2)
                                $row = MySQL_Fetch_Array(ExecuteQuery('SELECT SUM(OutOfTownAllowance_Value) AS OutOfTownAllowances FROM OutOfTownAllowance WHERE OutOfTownAllowance_Payment '.$payment.' AND OutOfTownAllowance_Name = '.$_SESSION['ViewReportExpenses'][3].''));
                              else
                                $row = MySQL_Fetch_Array(ExecuteQuery('SELECT SUM(OutOfTownAllowance_Value) AS OutOfTownAllowances FROM OutOfTownAllowance WHERE OutOfTownAllowance_Payment '.$payment.' AND OutOfTownAllowance_Name = '.$_SESSION['ViewReportExpenses'][3].' AND OutOfTownAllowance_Date_Start BETWEEN "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][0]).'" AND "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][1]).' 23:59:59"')); 
                              if ($row['OutOfTownAllowances'] == "")
                                $subTotal = 0; 
                              else
                                $subTotal = $row['OutOfTownAllowances']; 
                              $total += $subTotal;
                              
                          echo '<TD class="rowA center">R'.SPrintF('%02.2f', $subTotal).'
                                </TD>
                                <TD class="rowB center bold">R'.SPrintF('%02.2f', $total).'
                                </TD>
                              </TR>
                            </TABLE>'; 
                    break;
                }
                break;
              case '6': 
                switch ($_SESSION['ViewReportExpenses'][3])
                {       
                  case "":      
                    if ($_SESSION['ViewReportExpenses'][4] == 2)
                    {
                      BuildContentHeader('Travel Allowance Report ('.$reportType.')', "", "", false);
                      $query = 'SELECT * FROM TravelAllowance WHERE TravelAllowance_Payment '.$payment.' LIMIT 1';
                    } else
                    {
                      BuildContentHeader('Travel Allowance Report ('.$reportType.') - '.$dateRange, "", "", false);
                      $query = 'SELECT * FROM TravelAllowance WHERE TravelAllowance_Payment '.$payment.' AND TravelAllowance_Date BETWEEN "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][0]).'" AND "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][1]).' 23:59:59" LIMIT 1';
                    }
                    if (MySQL_Num_Rows(ExecuteQuery($query)) > 0)
                    {     
                      echo '<DIV class="contentflow">
                              <P>These are the travel allowances listed.</P>
                              <BR/><BR/>';
                              
                      $resultSet = ExecuteQuery('SELECT * FROM Staff WHERE Staff_IsEmployee = "1" ORDER BY Staff_First_Name, Staff_Last_Name');
                      while ($row = MySQL_Fetch_Array($resultSet))
                      { 
                        echo '<TABLE cellspacing="5" align="center" class="long" style="page-break-after: always">
                                <TR>';
                                if ($all)
                                  echo '<TD colspan="6" class="header">';
                                else
                                  echo '<TD colspan="5" class="header">';
                                echo $row['Staff_First_Name'].' '.$row['Staff_Last_Name'].'
                                        </TD>
                                      </TR>';
                                $total = 0;
                                if ($_SESSION['ViewReportExpenses'][4] == 2)
                                  $resultSetTemp = ExecuteQuery('SELECT * FROM TravelAllowance WHERE TravelAllowance_Payment '.$payment.' AND TravelAllowance_Name = "'.$row['Staff_Code'].'" ORDER BY TravelAllowance_Date');
                                else
                                  $resultSetTemp = ExecuteQuery('SELECT * FROM TravelAllowance WHERE TravelAllowance_Payment '.$payment.' AND TravelAllowance_Name = "'.$row['Staff_Code'].'" AND TravelAllowance_Date BETWEEN "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][0]).'" AND "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][1]).' 23:59:59" ORDER BY TravelAllowance_Date'); 
                                if (MySQL_Num_Rows($resultSetTemp) > 0)
                                { 
                                  echo '<TR>';
                                  if ($all)
                                    echo '<TD class="subheader veryshort">Payment
                                          </TD>';
                                    echo '<TD class="subheader veryshort">Date
                                          </TD>
                                          <TD class="subheader veryshort">Destination
                                          </TD>
                                          <TD class="subheader tiny">Distance (km)
                                          </TD>
                                          <TD class="subheader">Description
                                          </TD>
                                          <TD class="subheader">Value
                                          </TD>
                                        </TR>';
                                        while ($rowTemp = MySQL_Fetch_Array($resultSetTemp))
                                        {             
                                          echo '<TR>';
                                          if ($all)
                                            echo '<TD class="rowA center">'.GetPayment($rowTemp['TravelAllowance_Payment']).'
                                                  </TD>';
                                            echo '<TD class="rowA">'.GetTextualDateFromDatabaseDate($rowTemp['TravelAllowance_Date']).'
                                                  </TD>
                                                  <TD class="rowA">'.$rowTemp['TravelAllowance_Destination'].'
                                                  </TD>
                                                  <TD class="rowA center">'.$rowTemp['TravelAllowance_Km'].'
                                                  </TD>
                                                  <TD class="rowA">'.$rowTemp['TravelAllowance_Description'].'
                                                  </TD>
                                                  <TD class="rowA center">R'.SPrintF('%02.2f', $rowTemp['TravelAllowance_Value']).'
                                                  </TD>
                                                </TR>';  
                                          $total += $rowTemp['TravelAllowance_Value'];
                                        }
                                }
                                if ($all)
                                  echo '<TR>
                                          <TD colspan="5" class="rowB bold">Total:
                                          </TD>';
                                else
                                  echo '<TR>
                                          <TD colspan="4" class="rowB bold">Total:
                                          </TD>';
                            echo '<TD class="rowB center bold tiny">R'.SPrintF('%02.2f', $total).'
                                  </TD>
                                </TR>
                              </TABLE>
                              <BR/><BR/>';     
                      } 
                      echo '</DIV>';
                    } else                      
                      echo '<DIV class="contentflow">
                              <P>There are no travel allowances listed for the given date range.</P>
                            </DIV>';  
                    break;
                  default:    
                    if ($_SESSION['ViewReportExpenses'][4] == 2)
                    {
                      BuildContentHeader('Travel Allowance Report ('.$reportType.') - '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'], "", "", false);
                      $resultSet = ExecuteQuery('SELECT * FROM TravelAllowance WHERE TravelAllowance_Payment '.$payment.' AND TravelAllowance_Name = "'.$_SESSION['ViewReportExpenses'][3].'" ORDER BY TravelAllowance_Date');
                    } else
                    {  BuildContentHeader('Travel Allowance Report ('.$reportType.') - '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].' for '.$dateRange, "", "", false);
                      $resultSet = ExecuteQuery('SELECT * FROM TravelAllowance WHERE TravelAllowance_Payment '.$payment.' AND TravelAllowance_Name = "'.$_SESSION['ViewReportExpenses'][3].'" AND TravelAllowance_Date BETWEEN "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][0]).'" AND "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][1]).' 23:59:59" ORDER BY TravelAllowance_Date'); 
                    }
                    if (MySQL_Num_Rows($resultSet) > 0)
                    { 
                      echo '<DIV class="contentflow">
                              <P>These are the travel allowances listed.</P>
                              <BR/><BR/>
                              <TABLE cellspacing="5" align="center" class="long">
                                <TR>';
                                if ($all)
                                  echo '<TD colspan="6" class="header">Travel Allowance Details
                                        </TD>
                                      </TR>
                                      <TR>
                                        <TD class="subheader veryshort">Payment
                                        </TD>';
                                else
                                  echo '<TD colspan="5" class="header">Travel Allowance Details
                                        </TD>
                                      </TR>
                                      <TR>';
                            echo '<TD class="subheader veryshort">Date
                                  </TD>
                                  <TD class="subheader veryshort">Destination
                                  </TD>
                                  <TD class="subheader tiny">Distance (km)
                                  </TD>
                                  <TD class="subheader">Description
                                  </TD>
                                  <TD class="subheader">Value
                                  </TD>
                                </TR>';        
                                $total = 0;
                                while ($row = MySQL_Fetch_Array($resultSet))
                                {             
                                  echo '<TR>';
                                  if ($all)
                                    echo '<TD class="rowA center">'.GetPayment($row['TravelAllowance_Payment']).'
                                          </TD>';
                                    echo '<TD class="rowA">'.GetTextualDateFromDatabaseDate($row['TravelAllowance_Date']).'
                                          </TD>
                                          <TD class="rowA">'.$row['TravelAllowance_Destination'].'
                                          </TD>
                                          <TD class="rowA center">'.$row['TravelAllowance_Km'].'
                                          </TD>
                                          <TD class="rowA">'.$row['TravelAllowance_Description'].'
                                          </TD>
                                          <TD class="rowA center">R'.SPrintF('%02.2f', $row['TravelAllowance_Value']).'
                                          </TD>
                                        </TR>'; 
                                  $total += $row['TravelAllowance_Value'];
                                }  
                                if ($all)
                                  echo '<TR>
                                          <TD colspan="5" class="rowB bold">Total:
                                          </TD>';
                                else
                                  echo '<TR>
                                          <TD colspan="4" class="rowB bold">Total:
                                          </TD>';
                            echo '<TD class="rowB center bold">R'.SPrintF('%02.2f', $total).'
                                  </TD>                             
                                </TR>     
                              </TABLE>
                            </DIV>';  
                    } else                      
                      echo '<DIV class="contentflow">
                              <P>There are no travel allowances listed.</P>
                            </DIV>';
                    break;
                }
                break;
              case '7': 
                switch ($_SESSION['ViewReportExpenses'][3])
                {       
                  case "":
                    if ($_SESSION['ViewReportExpenses'][4] == 2)
                    {
                      BuildContentHeader('Overtime Report ('.$reportType.')', "", "", false);
                      $query = 'SELECT * FROM Overtime WHERE Overtime_Payment '.$payment.' LIMIT 1';
                    } else
                    {
                      BuildContentHeader('Overtime Report ('.$reportType.') - '.$dateRange, "", "", false);
                      $query = 'SELECT * FROM Overtime WHERE Overtime_Payment '.$payment.' AND Overtime_Start BETWEEN "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][0]).'" AND "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][1]).' 23:59:59" LIMIT 1';
                    }
                    if (MySQL_Num_Rows(ExecuteQuery($query)) > 0)
                    {
                      echo '<DIV class="contentflow">
                              <P>This is the overtime listed.</P>
                              <BR/><BR/>';
                              
                      $resultSet = ExecuteQuery('SELECT * FROM Staff WHERE Staff_IsEmployee = "1" ORDER BY Staff_First_Name, Staff_Last_Name');
                      while ($row = MySQL_Fetch_Array($resultSet))
                      { 
                        echo '<TABLE cellspacing="5" align="center" class="long" style="page-break-after: always">
                                <TR>';
                                if ($all)
                                  echo '<TD colspan="5" class="header">';
                                else
                                  echo '<TD colspan="4" class="header">';
                                echo $row['Staff_First_Name'].' '.$row['Staff_Last_Name'].'
                                        </TD>
                                      </TR>';
                                $total = 0;
                                if ($_SESSION['ViewReportExpenses'][4] == 2)
                                  $resultSetTemp = ExecuteQuery('SELECT * FROM Overtime WHERE Overtime_Payment '.$payment.' AND Overtime_Name = "'.$row['Staff_Code'].'" ORDER BY Overtime_Start');
                                else
                                  $resultSetTemp = ExecuteQuery('SELECT * FROM Overtime WHERE Overtime_Payment '.$payment.' AND Overtime_Name = "'.$row['Staff_Code'].'" AND Overtime_Start BETWEEN "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][0]).'" AND "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][1]).' 23:59:59" ORDER BY Overtime_Start'); 
                                $rowTempB = MySQL_Fetch_Array(ExecuteQuery('SELECT SUM(OvertimeBank_Hours) AS Overtime FROM OvertimeBank WHERE OvertimeBank_Name = '.$row['Staff_Code'].' AND OvertimeBank_Start <= "'.$_SESSION['ViewReportExpenses'][1].' 23:59:59" AND OvertimeBank_IsApproved = "1"'));
                                if (MySQL_Num_Rows($resultSetTemp) > 0)
                                { 
                                  echo '<TR>';
                                  if ($all)
                                    echo '<TD class="subheader veryshort">Payment
                                          </TD>';
                                    echo '<TD class="subheader veryshort">Start
                                          </TD>
                                          <TD class="subheader veryshort">End
                                          </TD>
                                          <TD class="subheader">Description
                                          </TD>
                                          <TD class="subheader tiny">Hours
                                          </TD>
                                        </TR>';       
                                        while ($rowTemp = MySQL_Fetch_Array($resultSetTemp))
                                        {             
                                          echo '<TR>';
                                          if ($all)
                                            echo '<TD class="rowA center">'.GetPayment($rowTemp['Overtime_Payment']).'
                                                  </TD>';
                                            echo '<TD class="rowA center">'.GetTextualDateTimeFromDatabaseDateTime($rowTemp['Overtime_Start']).'
                                                  </TD>
                                                  <TD class="rowA center">'.GetTextualDateTimeFromDatabaseDateTime($rowTemp['Overtime_End']).'
                                                  </TD>
                                                  <TD class="rowA">'.$rowTemp['Overtime_Description'].'
                                                  </TD>
                                                  <TD class="rowA center">'.$rowTemp['Overtime_Hours'].'
                                                  </TD>
                                                </TR>';
                                          $total += $rowTemp['Overtime_Hours'];
                                        }
                                }
                                if ($all)
                                  echo '<TR>
                                          <TD colspan="4" class="rowB bold">Total:
                                          </TD>
                                          <TD class="rowB center bold">'.SPrintF('%02.2f', $total).'
                                          </TD>                             
                                        </TR>
                                        <TR>
                                          <TD colspan="4" class="rowB bold">Banked Overtime Balance:';
                                else
                                  echo '<TR>
                                          <TD colspan="3" class="rowB bold">Total:
                                          </TD>  
                                          <TD class="rowB center bold">'.SPrintF('%02.2f', $total).'
                                          </TD>                             
                                        </TR>
                                        <TR>
                                          <TD colspan="3" class="rowB bold">Banked Overtime Balance:';
                            echo '<TD class="rowB center bold">'.SPrintF('%02.2f', $rowTempB['Overtime']).'
                                  </TD>
                                </TR>     
                              </TABLE>
                              <BR/><BR/>';     
                      } 
                      echo '</DIV>';
                    } else                      
                      echo '<DIV class="contentflow">
                              <P>There is no overtime listed for the given date range.</P>
                            </DIV>';  
                    break;
                  default:    
                    if ($_SESSION['ViewReportExpenses'][4] == 2)
                    {
                      BuildContentHeader('Overtime Report ('.$reportType.') - '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'], "", "", false);
                      $resultSet = ExecuteQuery('SELECT * FROM Overtime WHERE Overtime_Payment '.$payment.' AND Overtime_Name = "'.$_SESSION['ViewReportExpenses'][3].'" ORDER BY Overtime_Start');
                    } else
                    {
                      BuildContentHeader('Overtime Report ('.$reportType.') - '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].' for '.$dateRange, "", "", false);
                      $resultSet = ExecuteQuery('SELECT * FROM Overtime WHERE Overtime_Payment '.$payment.' AND Overtime_Name = "'.$_SESSION['ViewReportExpenses'][3].'" AND Overtime_Start BETWEEN "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][0]).'" AND "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][1]).' 23:59:59" ORDER BY Overtime_Start'); 
                    }
                    $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT SUM(OvertimeBank_Hours) AS Overtime FROM OvertimeBank WHERE OvertimeBank_Name = '.$_SESSION['ViewReportExpenses'][3].' AND OvertimeBank_Start <= "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][1]).' 23:59:59" AND OvertimeBank_IsApproved = "1"'));
                    if (MySQL_Num_Rows($resultSet) > 0)
                    {
                      echo '<DIV class="contentflow">
                              <P>This is the overtime listed.</P>
                              <BR/><BR/>
                              <TABLE cellspacing="5" align="center" class="long">
                                <TR>';
                                if ($all)
                                  echo '<TD colspan="5" class="header">Overtime Details
                                        </TD>
                                      </TR>
                                      <TR>
                                        <TD class="subheader veryshort">Payment
                                        </TD>';
                                else
                                  echo '<TD colspan="4" class="header">Overtime Details
                                        </TD>
                                      </TR>
                                      <TR>';
                            echo '<TD class="subheader veryshort">Start
                                  </TD>
                                  <TD class="subheader veryshort">End
                                  </TD>
                                  <TD class="subheader">Description
                                  </TD>
                                  <TD class="subheader tiny">Hours
                                  </TD>
                                </TR>';        
                                $total = 0;
                                while ($row = MySQL_Fetch_Array($resultSet))
                                {             
                                  echo '<TR>';
                                    if ($all)
                                    echo '<TD class="rowA center">'.GetPayment($row['Overtime_Payment']).'
                                          </TD>';
                                    echo '<TD class="rowA center">'.GetTextualDateTimeFromDatabaseDateTime($row['Overtime_Start']).'
                                          </TD>
                                          <TD class="rowA center">'.GetTextualDateTimeFromDatabaseDateTime($row['Overtime_End']).'
                                          </TD>
                                          <TD class="rowA">'.$row['Overtime_Description'].'
                                          </TD>
                                          <TD class="rowA center">'.SPrintF('%02.2f', $row['Overtime_Hours']).'
                                          </TD>
                                        </TR>'; 
                                  $total += $row['Overtime_Hours'];
                                }
                                if ($all)
                                  echo '<TR>
                                          <TD colspan="4" class="rowB bold">Total:
                                          </TD>  
                                          <TD class="rowB center bold">'.SPrintF('%02.2f', $total).'
                                          </TD>                             
                                        </TR>
                                        <TR>
                                          <TD colspan="4" class="rowB bold">Banked Overtime Balance:
                                          </TD>';
                                else
                                  echo '<TR>
                                          <TD colspan="3" class="rowB bold">Total:
                                          </TD>  
                                          <TD class="rowB center bold">'.SPrintF('%02.2f', $total).'
                                          </TD>                             
                                        </TR>
                                        <TR>
                                          <TD colspan="3" class="rowB bold">Banked Overtime Balance:
                                          </TD>';
                            echo '<TD class="rowB center bold">'.SPrintF('%02.2f', $rowTemp['Overtime']).'
                                  </TD>
                                </TR>
                              </TABLE>
                            </DIV>';  
                    } else                      
                      echo '<DIV class="contentflow">
                              <P>This is the overtime listed.</P>
                              <BR/><BR/>
                              <TABLE cellspacing="5" align="center" class="long">
                                <TR>
                                  <TD colspan="2" class="header">Overtime Details
                                  </TD>
                                </TR>
                                <TR>
                                  <TD class="rowB bold">Banked Overtime Balance:
                                  </TD>  
                                  <TD class="rowB center bold">'.SPrintF('%02.2f', $rowTemp['Overtime']).'
                                  </TD>                             
                                </TR>
                              </TABLE>
                            </DIV>';
                    break;
                }
                break;
              case '8':
                switch ($_SESSION['ViewReportExpenses'][3])
                {
                  case "":
                    if ($_SESSION['ViewReportExpenses'][4] == 2)
                    {
                      BuildContentHeader('Out-of-Town Allowance Report ('.$reportType.')', "", "", false);
                      $query = 'SELECT * FROM OutOfTownAllowance WHERE OutOfTownAllowance_Payment '.$payment.' LIMIT 1';
                    } else
                    {
                      BuildContentHeader('Out-of-Town Allowance Report ('.$reportType.') - '.$dateRange, "", "", false);
                      $query = 'SELECT * FROM OutOfTownAllowance WHERE OutOfTownAllowance_Payment '.$payment.' AND OutOfTownAllowance_Date_Start BETWEEN "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][0]).'" AND "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][1]).' 23:59:59" LIMIT 1';
                    }
                    if (MySQL_Num_Rows(ExecuteQuery($query)) > 0)
                    {
                      echo '<DIV class="contentflow">
                              <P>These are the out-of-town allowances listed.</P>
                              <BR/><BR/>';
                              
                      $resultSet = ExecuteQuery('SELECT * FROM Staff WHERE Staff_IsEmployee = "1" ORDER BY Staff_First_Name, Staff_Last_Name');
                      while ($row = MySQL_Fetch_Array($resultSet))
                      { 
                        echo '<TABLE cellspacing="5" align="center" class="long" style="page-break-after: always">
                                <TR>';
                                if ($all)
                                  echo '<TD colspan="6" class="header">';
                                else
                                  echo '<TD colspan="5" class="header">';
                                echo $row['Staff_First_Name'].' '.$row['Staff_Last_Name'].'
                                        </TD>
                                      </TR>';
                                $total = 0;
                                if ($_SESSION['ViewReportExpenses'][4] == 2)
                                  $resultSetTemp = ExecuteQuery('SELECT * FROM OutOfTownAllowance WHERE OutOfTownAllowance_Payment '.$payment.' AND OutOfTownAllowance_Name = "'.$row['Staff_Code'].'" ORDER BY OutOfTownAllowance_Date_Start');
                                else
                                  $resultSetTemp = ExecuteQuery('SELECT * FROM OutOfTownAllowance WHERE OutOfTownAllowance_Payment '.$payment.' AND OutOfTownAllowance_Name = "'.$row['Staff_Code'].'" AND OutOfTownAllowance_Date_Start BETWEEN "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][0]).'" AND "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][1]).' 23:59:59" ORDER BY OutOfTownAllowance_Date_Start'); 
                                if (MySQL_Num_Rows($resultSetTemp) > 0)
                                { 
                                  echo '<TR>';
                                  if ($all)
                                    echo '<TD class="subheader veryshort">Payment
                                          </TD>';
                                    echo '<TD class="subheader veryshort">Date
                                          </TD>
                                          <TD class="subheader veryshort">Type
                                          </TD>
                                          <TD class="subheader tiny">Days
                                          </TD>
                                          <TD class="subheader">Description
                                          </TD>
                                          <TD class="subheader">Value
                                          </TD>
                                        </TR>';
                                        while ($rowTemp = MySQL_Fetch_Array($resultSetTemp))
                                        {
                                          $rowType = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM OutOfTownAllowanceType WHERE OutOfTownAllowanceType_ID = "'.$rowTemp['OutOfTownAllowance_Type'].'"'));
                                          echo '<TR>';
                                          if ($all)
                                            echo '<TD class="rowA center">'.GetPayment($rowTemp['OutOfTownAllowance_Payment']).'
                                                  </TD>';
                                            echo '<TD class="rowA center">'.GetTextualDateFromDatabaseDate($rowTemp['OutOfTownAllowance_Date_Start']).' to '.GetTextualDateFromDatabaseDate($rowTemp['OutOfTownAllowance_Date_End']).'
                                                  </TD>
                                                  <TD class="rowA center">'.$rowType['OutOfTownAllowanceType_Description'].'
                                                  </TD>
                                                  <TD class="rowA center">'.$rowTemp['OutOfTownAllowance_Days'].'
                                                  </TD>
                                                  <TD class="rowA">'.$rowTemp['OutOfTownAllowance_Description'].'
                                                  </TD>
                                                  <TD class="rowA center">R'.SPrintF('%02.2f', $rowTemp['OutOfTownAllowance_Value']).'
                                                  </TD>
                                                </TR>';
                                          $total += $row['OutOfTownAllowance_Value'];
                                        }
                                }
                                if ($all)
                                  echo '<TR>
                                          <TD colspan="5" class="rowB bold">Total:
                                          </TD>';
                                else
                                  echo '<TR>
                                          <TD colspan="4" class="rowB bold">Total:
                                          </TD>';
                            echo '<TD class="rowB center bold tiny">R'.SPrintF('%02.2f', $total).'
                                  </TD>
                                </TR>
                              </TABLE>
                              <BR/><BR/>';     
                      } 
                      echo '</DIV>';
                    } else                      
                      echo '<DIV class="contentflow">
                              <P>There are no out-of-town allowances listed for the given date range.</P>
                            </DIV>';  
                    break;
                  default:    
                    if ($_SESSION['ViewReportExpenses'][4] == 2)
                    {
                      BuildContentHeader('Out-of-Town Allowance Report ('.$reportType.') - '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'], "", "", false);
                      $resultSet = ExecuteQuery('SELECT * FROM OutOfTownAllowance WHERE OutOfTownAllowance_Payment '.$payment.' AND OutOfTownAllowance_Name = "'.$_SESSION['ViewReportExpenses'][3].'" ORDER BY OutOfTownAllowance_Date_Start');
                    } else
                    {  BuildContentHeader('Out-of-Town Allowance Report ('.$reportType.') - '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].' for '.$dateRange, "", "", false);
                      $resultSet = ExecuteQuery('SELECT * FROM OutOfTownAllowance WHERE OutOfTownAllowance_Payment '.$payment.' AND OutOfTownAllowance_Name = "'.$_SESSION['ViewReportExpenses'][3].'" AND OutOfTownAllowance_Date_Start BETWEEN "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][0]).'" AND "'.GetDatabaseDateFromSessionDate($_SESSION['ViewReportExpenses'][1]).' 23:59:59" ORDER BY OutOfTownAllowance_Date_Start'); 
                    }
                    if (MySQL_Num_Rows($resultSet) > 0)
                    { 
                      echo '<DIV class="contentflow">
                              <P>These are the out-of-town allowances listed.</P>
                              <BR/><BR/>
                              <TABLE cellspacing="5" align="center" class="long">
                                <TR>';
                                if ($all)
                                  echo '<TD colspan="6" class="header">Out-of-Town Allowance Details
                                        </TD>
                                      </TR>
                                      <TR>
                                        <TD class="subheader veryshort">Payment
                                        </TD>';
                                else
                                  echo '<TD colspan="5" class="header">Out-of-Town Allowance Details
                                        </TD>
                                      </TR>
                                      <TR>';
                            echo '<TD class="subheader veryshort">Date
                                  </TD>
                                  <TD class="subheader veryshort">Type
                                  </TD>
                                  <TD class="subheader tiny">Days
                                  </TD>
                                  <TD class="subheader">Description
                                  </TD>
                                  <TD class="subheader">Value
                                  </TD>
                                </TR>';
                                $total = 0;
                                while ($row = MySQL_Fetch_Array($resultSet))
                                {
                                  $rowType = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM OutOfTownAllowanceType WHERE OutOfTownAllowanceType_ID = "'.$row['OutOfTownAllowance_Type'].'"'));
                                  echo '<TR>';
                                  if ($all)
                                    echo '<TD class="rowA center">'.GetPayment($row['OutOfTownAllowance_Payment']).'
                                          </TD>';
                                    echo '<TD class="rowA center">'.GetTextualDateFromDatabaseDate($row['OutOfTownAllowance_Date_Start']).' to '.GetTextualDateFromDatabaseDate($row['OutOfTownAllowance_Date_End']).'
                                          </TD>
                                          <TD class="rowA center">'.$rowType['OutOfTownAllowanceType_Description'].'
                                          </TD>
                                          <TD class="rowA center">'.$row['OutOfTownAllowance_Days'].'
                                          </TD>
                                          <TD class="rowA">'.$row['OutOfTownAllowance_Description'].'
                                          </TD>
                                          <TD class="rowA center">R'.SPrintF('%02.2f', $row['OutOfTownAllowance_Value']).'
                                          </TD>
                                        </TR>';
                                  $total += $row['OutOfTownAllowance_Value'];
                                }
                                if ($all)
                                  echo '<TR>
                                          <TD colspan="5" class="rowB bold">Total:
                                          </TD>';
                                else
                                  echo '<TR>
                                          <TD colspan="4" class="rowB bold">Total:
                                          </TD>';
                            echo '<TD class="rowB center bold">R'.SPrintF('%02.2f', $total).'
                                  </TD>
                                </TR>
                              </TABLE>
                            </DIV>';
                    } else
                      echo '<DIV class="contentflow">
                              <P>There are no out-of-town allowances listed.</P>
                            </DIV>';
                    break;
                }
                break;
              default:
                echo 'Kyaaaaaaaaaah! *chop* Invalid report type! Error! Error!';
                break;
            }
            Session_Unregister('ViewReportExpenses');
          } else
          { 
            if ($_SESSION['cAuth'] & 64)
            {
              BuildContentHeader('Maintenance', "", "", true);
              echo '<DIV class="contentflow">
                      <P>Expenses can be marked as paid or reviewed by using the form below. Expenses can also be finalised.</P>
                      <BR />
                      <P class="bold">The finalising of expenses involves transferring excess overtime and all banked overtime for selected staff (as per the Excess function on the <A href="Overtime.php">Overtime page</a>). It also includes allocating the monthly annual leave for the current month if it is not yet allocated (as per the Allocate function on the <A href="Leave.php">Leave page</a>). The social club fees are similarly deducted.</P>
                      <BR />
                      <P class="bold underline">This is the procedure for handling expenses at month-end: All expenses must be finalised as mentioned above. Then all unpaid expenses must first be previewed and then marked as reviewed. Expenses now marked as reviewed must be viewed and checked. Once expenses have all been paid they can be marked as such on the Intranet.</P>
                      <BR/><BR/>
                      <TABLE cellspacing="5" align="center" class="standard">
                        <FORM method="post" action="Handlers/ReportExpenses_Handler.php">
                          <INPUT name="Type" type="hidden" value="Maintain" />
                          <TR>
                            <TD colspan="2" class="header">Finalise
                            </TD>
                          </TR>
                          <TR>
                            <TD>To finalise expenses for review and payment, click Finalise and confirm when prompted.
                            </TD>
                            <TD class="right">
                              <INPUT tabindex="1" name="Submit" type="submit" class="button" value="Finalise" />                   
                            </TD>
                          </TR>
                          <TR>
                            <TD colspan="2" class="header">Pay
                            </TD>
                          </TR>
                          <TR>
                            <TD>To mark expenses as paid, click Pay and confirm when prompted.
                            </TD>
                            <TD class="right">
                              <INPUT tabindex="1" name="Submit" type="submit" class="button" value="Pay" />                   
                            </TD>
                          </TR>
                          <TR>
                            <TD colspan="2" class="header">Review
                            </TD>
                          </TR>
                          <TR>
                            <TD>To mark expenses as reviewed, click Review and confirm when prompted.
                            </TD>
                            <TD class="right">
                              <INPUT tabindex="2" name="Submit" type="submit" class="button" value="Review" />                   
                            </TD>
                          </TR>
                        </FORM>
                      </TABLE>
                    </DIV>';    
                       
              BuildContentHeader('View Expense Reports', "", "", true); 
            } else
              BuildContentHeader('View Expense Reports', "", "", true);
                               
            echo '<DIV class="contentflow">
                    <P>Expenses can be viewed by using the form below.';
                        if ($_SESSION['cAuth'] & 64)
                          echo ' When checking unpaid expenses, always try to make the date range within the last 2-3 months to cater for any late submissions. Once you are sure you of all unpaid expenses, mark them as reviewed. Once that has been done, generate a report for reviewed expenses and use this to determine the expenses to be paid. After everything is checked and confirmed, mark reviewed expenses as paid to complete the process.';
              echo '</P>
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center" class="standard">
                      <FORM method="post" action="Handlers/ReportExpenses_Handler.php">
                        <INPUT name="Type" type="hidden" value="View">
                        <TR>
                          <TD colspan="4" class="header">View
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="4">To view expenses, specify the particulars and click View. A date does not need to be specified when viewing reviewed expenses.
                          </TD>
                        </TR>
                        <TR>
                          <TD class="short">Report Type:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD colspan="3">';
                            BuildExpenseReportSelector(3, 'ReportExpenses', 'standard');
                    echo '</TD>
                        </TR>
                        <TR>
                          <TD>Payment:
                          </TD>
                          <TD colspan="3">';
                            BuildPaymentTypeSelector(4, 'Payment', 'standard');
                    echo '</TD>
                        </TR>';
                        if ($_SESSION['cAuth'] & 64)
                        {
                          echo '<TR>
                                  <TD>Staff Name:
                                  </TD>
                                  <TD colspan="3">';
                                    BuildStaffSelector(5, 'Staff', 'standard', $_SESSION['cUID'], true);
                            echo '</TD>
                                </TR>';
                        }
                  echo '<TR>
                          <TD>Date Range:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                            BuildDaySelector(6, 'StartDay', "");
                            echo '&nbsp;';
                            BuildMonthSelector(7, 'StartMonth', "");
                            echo '&nbsp;';
                            BuildYearSelector(8, 'StartYear', "");
                            echo ' to ';
                            BuildDaySelector(9, 'EndDay', "");
                            echo '&nbsp;';
                            BuildMonthSelector(10, 'EndMonth', "");
                            echo '&nbsp;';
                            BuildYearSelector(11, 'EndYear', "");
                    echo '</TD>
                          <TD colspan="2" class="right">
                            <INPUT tabindex="12" name="Submit" type="submit" class="button" value="View" />                   
                          </TD>
                        </TR>
                      </FORM>
                    </TABLE> 
                  </DIV>';
          }
          //////////////////////////////////////////////////////////////////////
        ?>
                            </div>
                        </div>
                    </div>
            </section
    </DIV>
    <?php
      // PHP SCRIPT ////////////////////////////////////////////////////////////
      BuildFooter();
      //////////////////////////////////////////////////////////////////////////
    ?>
  </BODY>
</HTML>