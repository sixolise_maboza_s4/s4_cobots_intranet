<?php
  // DETAILS ///////////////////////////////////////////////////////////////////
  //                                                                          //
  //                    Last Edited By: Gareth Ambrose                        //
  //                        Date: 03 February 2010                            //
  //                                                                          //
  //////////////////////////////////////////////////////////////////////////////
  // This page allows users to manage and view overtime.                      //
  //////////////////////////////////////////////////////////////////////////////
  
  include 'Scripts/Include.php';
  SetSettings();
  CheckAuthorisation('Overtime.php');
  
  //////////////////////////////////////////////////////////////////////////////
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3c.org/TR/1999/REC-html401-19991224/loose.dtd">
<HTML>
  <HEAD>
       <meta http-equiv="content-type" content="text/html; charset=utf-8">
       
     
       
    <?php
      // PHP SCRIPT ////////////////////////////////////////////////////////////
      BuildHead('Overtime');
      include ('Scripts/header.php');
      
      //////////////////////////////////////////////////////////////////////////
      



    ?> 
      
        <style>
            
            
/*            ::-webkit-scrollbar { 
                display: none; 
            }*/

            
            #ERRERS{
                display: none;
                margin-bottom: 1em;
            }
            
            .dx-datagrid-rowsview{
              overflow-y: overlay  !important;
            }
            
            .Hours {
                display: inline-block; 
            }

            /* Tooltip text */
            .Hours .tooltiptext {
                visibility: hidden;
                background-color: #295885;
                color: #fff;
                text-align: center;
                padding: 5px ;
                
              
                /* Position the tooltip text - see examples below! */
                position: absolute;
                z-index: 1;
            }

            /* Show the tooltip text when you mouse over the tooltip container */
            .Hours:hover .tooltiptext {
                visibility: visible;
            }
            
            
            
            .hide{
                display: none;
            }
            
            .hidethedata{
                display: none;
            }
            
            .showDetail{
                color: blue;
                text-decoration: underline;

            }
            .highlight{
                background-color: yellow !important;
            }
            .wait {
                opacity: 0.2;
                
                cursor: wait;}
            
        </style>
    
      
  </HEAD>
  <BODY>
    <?php
      // PHP SCRIPT ////////////////////////////////////////////////////////////
      BuildBanner();
      $resultSt= ExecuteQuery('SELECT Staff_First_Name, Staff_Last_Name FROM Staff WHERE Staff_Code = "'.$_SESSION['cUID'].'"');
      $LoggedinFullName="";
      if ((isset($_SESSION['RequestOvertimeNormal']) || isset($_SESSION['RequestOvertimeBanked'])) && (MySQL_Num_Rows($resultSt) == 1))
      {
            $loggedInStaff = MySQL_Fetch_Array($resultSt);
            if (StrLen($loggedInStaff["Staff_First_Name"].' '.$loggedInStaff["Staff_Last_Name"]) > 16)
                  $LoggedinFullName = SubStr($loggedInStaff["Staff_First_Name"].' '.$loggedInStaff["Staff_Last_Name"], 0, 16).'...</B>';
             else
                  $LoggedinFullName = $loggedInStaff["Staff_First_Name"].' '.$loggedInStaff["Staff_Last_Name"];
      }
      //////////////////////////////////////////////////////////////////////////
    ?>
    <DIV id="main">
      <?php
        // PHP SCRIPT //////////////////////////////////////////////////////////
      BuildTopBar();
        BuildMenu('Main', 'Overtime.php');
        ////////////////////////////////////////////////////////////////////////
      ?>
        <section id="content_wrapper">
            <?php BuildBreadCrumb($currentPath);?>
            <!-- -------------- Content -------------- -->
            <section id="content" class="table-layout">
                <!-- -------------- Column Center -------------- -->
                <div class="chute chute-center" style="height: 869px;">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel">
        <?php
          // PHP SCRIPT ////////////////////////////////////////////////////////
          if ($_GET['id'])
          {
            $id = SubStr($_GET['id'], 6, StrLen($_GET['id']) - 6);
            switch (SubStr($_GET['id'], 5, 1))
            {
              case '0': //Banked overtime request.
                $resultSet = ExecuteQuery('SELECT * FROM OvertimeBank WHERE OvertimeBank_ID = "'.$id.'" AND OvertimeBank_IsApproved = "0"');
                if (MySQL_Num_Rows($resultSet) > 0)
                {
                  $row = MySQL_Fetch_Array($resultSet);
                  switch ($_GET['type'])
                  {
                    case 'approve':
                      if ($row['OvertimeBank_Authorised'] == $_SESSION['cUID'])
                      {
                        if (ExecuteQuery('UPDATE OvertimeBank SET OvertimeBank_IsApproved = "1" WHERE OvertimeBank_ID = "'.$id.'"'))
                  	    {
                          $row = MySQL_Fetch_Array(ExecuteQuery('SELECT OvertimeBank.* FROM OvertimeBank WHERE OvertimeBank_ID = "'.$id.'"'));
                          $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT Staff_First_Name, Staff_Last_Name, Staff_email FROM Staff WHERE Staff_Code = '.$row['OvertimeBank_Name'].''));
                          
                          $email = 'The banked overtime use you requested has been approved. The details of the approved banked overtime use are as follows:'.Chr(10).
                                   'NAME:                  '.$rowTemp['Staff_First_Name'].' '.$rowTemp['Staff_Last_Name'].Chr(10).
                                   'PERIOD:                '.GetTextualDateTimeFromDatabaseDateTime($row['OvertimeBank_Start']).' until '.GetTextualDateTimeFromDatabaseDateTime($row['OvertimeBank_End']).Chr(10).
                                   'HOURS:                 '.SPrintF('%02.2f', -$row['OvertimeBank_Hours']).Chr(10).
                                   'DESCRIPTION:           '.$row['OvertimeBank_Description'];
                          
                          $html = 'The banked overtime use you requested has been approved. The details of the approved banked overtime use are as follows:
                                  <BR /><BR />
                                  <TABLE border=0>
                                    <TR><TD><B>Name:</B></TD><TD>'.$rowTemp['Staff_First_Name'].' '.$rowTemp['Staff_Last_Name'].'</TD></TR>
                                    <TR><TD><B>Period:</B></TD><TD>'.GetTextualDateTimeFromDatabaseDateTime($row['OvertimeBank_Start']).' until '.GetTextualDateTimeFromDatabaseDateTime($row['OvertimeBank_End']).'</TD></TR>
                                    <TR><TD><B>Hours:</B></TD><TD>'.SPrintF('%02.2f', -$row['OvertimeBank_Hours']).'</TD></TR>
                                    <TR><TD><B>Description:</B></TD><TD>'.$row['OvertimeBank_Description'].'</TD></TR>
                                  </TABLE>';
                          
                          SendMailHTML($rowTemp['Staff_email'], 'Banked Overtime Use Approved', $email, $html);
                          $_SESSION['OvertimeSuccess'] = 'geh!';
                        } else
                          $_SESSION['OvertimeFail'] = 'geh!';
                      }
                      break;
                    case 'deny':
                      if ($row['OvertimeBank_Authorised'] == $_SESSION['cUID'])
                      {
                        $_SESSION['ApproveOvertimeBanked'] = array($id);

                      }
                      break;
                    default:
                      break;
                  }
                }
                break;
              case '1': //Normal overtime request.
                $resultSet = ExecuteQuery('SELECT * FROM OvertimePreApp WHERE OvertimePreApp_ID = "'.$id.'" AND OvertimePreApp_IsApproved = "0"');
                if (MySQL_Num_Rows($resultSet) > 0)
                {
                  $row = MySQL_Fetch_Array($resultSet);
                  switch ($_GET['type'])
                  {
                    case 'approve':
                      if ($row['OvertimePreApp_Authorised'] == $_SESSION['cUID'])
                      {
                        $date = Date('Y-m-d H:i:s');  
                        if (ExecuteQuery('UPDATE OvertimePreApp SET OvertimePreApp_IsApproved = "1",`OvertimePreApp_ApprovedDate` = "'.$date.'"  WHERE OvertimePreApp_ID = "'.$id.'"'))
                  	    {
                          $row = MySQL_Fetch_Array(ExecuteQuery('SELECT OvertimePreApp.* FROM OvertimePreApp WHERE OvertimePreApp_ID = "'.$id.'"'));
                          $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT Staff_First_Name, Staff_Last_Name, Staff_email FROM Staff WHERE Staff_Code = '.$row['OvertimePreApp_Name'].''));
                          $rowTempB = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Project WHERE Project_Code = "'.$row['OvertimePreApp_Project'].'"'));
                          
                          $email = 'The overtime you requested has been approved. The details of the approved overtime are as follows:'.Chr(10).
                                   'NAME:                   '.$rowTemp['Staff_First_Name'].' '.$rowTemp['Staff_Last_Name'].Chr(10).
                                   'PERIOD:                 '.GetTextualDateTimeFromDatabaseDateTime($row['OvertimePreApp_Start']).' until '.GetTextualDateTimeFromDatabaseDateTime($row['OvertimePreApp_End']).Chr(10).
                                   'PROJECT:                '.$rowTempB['Project_Description'].Chr(10).
                                   'HOURS:                  '.SPrintF('%02.2f', $row['OvertimePreApp_Hours']).Chr(10).
                                   'DESCRIPTION:            '.$row['OvertimePreApp_Description'].Chr(10).
                                   'MULTIPLE ENTRIES:       '.($_POST['Multi'] ? 'Yes' : 'No');
                          
                          $html = 'The overtime you requested has been approved. The details of the approved overtime are as follows:
                                  <BR /><BR />
                                  <TABLE border=0>
                                    <TR><TD><B>Name:</B></TD><TD>'.$rowTemp['Staff_First_Name'].' '.$rowTemp['Staff_Last_Name'].'</TD></TR>
                                    <TR><TD><B>Period:</B></TD><TD>'.GetTextualDateTimeFromDatabaseDateTime($row['OvertimePreApp_Start']).' until '.GetTextualDateTimeFromDatabaseDateTime($row['OvertimePreApp_End']).'</TD></TR>
                                    <TR><TD><B>Project:</B></TD><TD>'.$rowTempB['Project_Description'].'</TD></TR>
                                    <TR><TD><B>Hours:</B></TD><TD>'.SPrintF('%02.2f', $row['OvertimePreApp_Hours']).'</TD></TR>
                                    <TR><TD><B>Description:</B></TD><TD>'.$row['OvertimePreApp_Description'].'</TD></TR>
                                    <TR><TD><B>Multiple Entries:</B></TD><TD>'.($_POST['Multi'] ? 'Yes' : 'No').'</TD></TR>
                                  </TABLE>';
                          
                          SendMailHTML($rowTemp['Staff_email'], 'Overtime Approved', $email, $html);
                           if(AddOvertime($id)){
                            $_SESSION['OvertimeSuccess'] = 'geh!';
                          }
                           else{
                                $_SESSION['OvertimeFail'] = 'geh!';
                           }
                        } else
                          $_SESSION['OvertimeFail'] = 'geh!';
                      }
                      break;
                    case 'deny':
                      if ($row['OvertimePreApp_Authorised'] == $_SESSION['cUID'])
                      {
                        $_SESSION['ApproveOvertimeNormal'] = array($id);

                      }
                      break;
                    default:
                      break;
                  }
                }
                break;
              default:
                break;
            }
          }
          
          
          function AddOvertime($PerApprovedID){

            $row = MySQL_Fetch_Array(ExecuteQuery('SELECT OvertimePreApp.*, (OvertimePreApp_Hours - OvertimePreApp_OvertimeHoursUsed) AS OvertimeAvail, Project_Description FROM OvertimePreApp, Project WHERE OvertimePreApp_ID = "'.$PerApprovedID.'" AND OvertimePreApp_Project = Project_Code')); 
            if($row['OvertimePreApp_OvertimeHoursUsed'] == 0){
               if ($row['OvertimePreApp_Bank'] == '1')
                 $query = 'INSERT INTO OvertimeBank VALUES("'.$row['OvertimePreApp_Name'].'", "'.Date('Y-m-d H:i:s').'", "'.$row['OvertimePreApp_Hours'].'", "'.$row['OvertimePreApp_Project'].'", "auto add", "'.$row['OvertimePreApp_Authorised'].'", "", "'.$row['OvertimePreApp_Start'].'", "'.$row['OvertimePreApp_End'].'", "0", "", "'.$row['OvertimePreApp_Billable'].'", "'.$PerApprovedID.'", "1")';
                if (ExecuteQuery($query))
                 {
                  $hours = $row['OvertimePreApp_OvertimeHoursUsed'] + $row['OvertimePreApp_Hours'];   //$row['OvertimePreApp_OvertimeHoursUsed'] == 0
                  if ($hours >= $row['OvertimePreApp_Hours'])
                   $used = '1';                         // always here
                  else
                   $used = '0';

                 if ($row['OvertimePreApp_Multi'] == '1')
                   $query = 'UPDATE OvertimePreApp SET OvertimePreApp_OvertimeUsed = "'.$used.'", OvertimePreApp_OvertimeHoursUsed = "'.$hours.'" WHERE OvertimePreApp_ID = "'.$PerApprovedID.'"';
                 else
                   $query = 'UPDATE OvertimePreApp SET OvertimePreApp_OvertimeUsed = "1", OvertimePreApp_OvertimeHoursUsed = "'.$hours.'" WHERE OvertimePreApp_ID = "'.$PerApprovedID.'"';

                 if (ExecuteQuery($query))
                 {
                  return true;
                 } else
                   return false;
             } else
                 return false;    
             }
            return false;    
  }
          
          BuildMessageSet('Overtime');
          //////////////////////////////////////////////////////////////////////    
        ?>  
        <?php 
          // PHP SCRIPT ////////////////////////////////////////////////////////
          if (isset($_SESSION['AddOvertime'])) 
          {
            $row = MySQL_Fetch_Array(ExecuteQuery('SELECT (OvertimePreApp_Hours - OvertimePreApp_OvertimeHoursUsed) AS OvertimeAvail, Project_Description FROM OvertimePreApp, Project WHERE OvertimePreApp_ID = "'.$_SESSION['AddOvertime'][0].'" AND OvertimePreApp_Project = Project_Code'));
            BuildContentHeader('Add Overtime - '.$row['Project_Description'], "", "", false);  
            echo '<DIV class="contentflow">
                    <P>Enter the details of the overtime below. Ensure that all the required information is entered before submission and that this information is valid. Information on calculating overtime can be found <A tabindex="-1" href="Rates.php">here</A>.</P>
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center">
                      <FORM method="post" action="Handlers/Overtime_Handler.php">
                        <INPUT name="Type" type="hidden" value="Add">
                        <TR>
                          <TD colspan="2" class="header">Overtime Details
                          </TD>
                        </TR>
                        <TR>
                          <TD class="short">Start Date and Time:
                            <SPAN class="note">**
                            </SPAN>
                          </TD>
                          <TD>'; 
                            BuildDaySelector(1, 'StartDay', GetDayFromSessionDate($_SESSION['AddOvertime'][1]));
                            echo '&nbsp;';
                            BuildMonthSelector(2, 'StartMonth', GetMonthFromSessionDate($_SESSION['AddOvertime'][1]));
                            echo '&nbsp;';
                            BuildYearSelector(3, 'StartYear', GetYearFromSessionDate($_SESSION['AddOvertime'][1]));
                            echo '&nbsp;at&nbsp;';
                            BuildHourSelector(4, 'StartHour', GetHourFromSessionDateTime($_SESSION['AddOvertime'][1]));
                            echo '&nbsp;:';
                            BuildMinuteSelector(5, 'StartMinute', GetMinuteFromSessionDateTime($_SESSION['AddOvertime'][1]));
                    echo '</TD>
                        </TR>
                        <TR>
                          <TD>End Date and Time:
                            <SPAN class="note">**
                            </SPAN>
                          </TD>
                          <TD>';
                            BuildDaySelector(6, 'EndDay', GetDayFromSessionDate($_SESSION['AddOvertime'][3]));
                            echo '&nbsp;';
                            BuildMonthSelector(7, 'EndMonth', GetMonthFromSessionDate($_SESSION['AddOvertime'][3]));
                            echo '&nbsp;';
                            BuildYearSelector(8, 'EndYear', GetYearFromSessionDate($_SESSION['AddOvertime'][3]));
                            echo '&nbsp;at&nbsp;';
                            BuildHourSelector(9, 'EndHour', GetHourFromSessionDateTime($_SESSION['AddOvertime'][3]));
                            echo '&nbsp;:';
                            BuildMinuteSelector(10, 'EndMinute', GetMinuteFromSessionDateTime($_SESSION['AddOvertime'][3]));
                    echo '</TD>
                        </TR>
                        <TR>
                          <TD>Overtime Hours:
                            <SPAN class="note">***
                            </SPAN>
                          </TD>
                          <TD>
                            <INPUT tabindex="11" name="Hours" type="text" class="veryshort" maxlength="11" value="'.$_SESSION['AddOvertime'][4].'" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(Available: <B>'.$row['OvertimeAvail'].'</B>).
                          </TD>
                        </TR>
                        <TR>
                          <TD class="vtop">Description:
                            <SPAN class="note">
                            </SPAN>
                          </TD>
                          <TD>
                            <TEXTAREA tabindex="12" name="Description" class="long" maxlength="500">'.$_SESSION['AddOvertime'][2].'</TEXTAREA>
                          </TD>
                        </TR>
                        <TR>
                          <TD>Overtime Banked: 
                          </TD>
                          <TD class="bold">'.$_SESSION['AddOvertime'][5].'
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="2" class="center">
                            <INPUT tabindex="13" name="Submit" type="submit" class="button" value="Submit" />
                            <INPUT tabindex="14" name="Submit" type="submit" class="button" value="Cancel" />                   
                          </TD>
                        </TR>
                      </FORM>
                    </TABLE> 
                    
                  </DIV> 
                  <DIV>
                    <BR />
                    <SPAN class="note">*
                    </SPAN>
                    These fields are required.
                    <BR />
                    <SPAN class="note">**
                    </SPAN>
                    The actual start and end times must be used.
                    <BR />
                    <SPAN class="note">***
                    </SPAN>
                    These are the calculated overtime hours. This includes the additonal hour for travel time - if applicable.
                  </DIV>';
          } else
          if (isset($_SESSION['ApproveOvertimeBanked']))
          {            
            $row = MySQL_Fetch_Array(ExecuteQuery('SELECT OvertimeBank.* FROM OvertimeBank WHERE OvertimeBank_ID = "'.$_SESSION['ApproveOvertimeBanked'][0].'"')); 
            $startDate = GetDatabaseDate(GetDayFromDatabaseDate($row['OvertimeBank_Start']) - 7, GetMonthFromDatabaseDate($row['OvertimeBank_Start']), GetYearFromDatabaseDate($row['OvertimeBank_Start']));
            $endDate = GetDatabaseDate(GetDayFromDatabaseDate($row['OvertimeBank_End']) + 7, GetMonthFromDatabaseDate($row['OvertimeBank_End']), GetYearFromDatabaseDate($row['OvertimeBank_End']));
            if ($row['OvertimeBank_Start'] == $row['OvertimeBank_End'])
              $dateRange = GetTextualDateFromDatabaseDate($row['OvertimeBank_Start']);
            else        
              $dateRange = GetTextualDateFromDatabaseDate($row['OvertimeBank_Start']).' to '.GetTextualDateFromDatabaseDate($row['OvertimeBank_End']);
            
            $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = "'.$row['OvertimeBank_Name'].'"'));
            $rowTempB = MySQL_Fetch_Array(ExecuteQuery('SELECT SUM(OvertimeBank_Hours) AS Avail FROM OvertimeBank WHERE OvertimeBank_Name = '.$row['OvertimeBank_Name'].' AND OvertimeBank_IsApproved = "1"'));
            $rowApprove = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = '.$row['OvertimeBank_Authorised'].''));
            BuildContentHeader('Approve Banked Overtime Use - '.$rowTemp['Staff_First_Name'].' '.$rowTemp['Staff_Last_Name'].' for '.$dateRange, "", "", true);
            echo '<DIV class="contentflow">
                    <P>This banked overtime use is set to be authorised by <B>'.$rowApprove['Staff_First_Name'].' '.$rowApprove['Staff_Last_Name'].'</B></P>
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center" class="short">
                      <TR>
                        <TD colspan="2" class="header">Overtime Details
                        </TD>
                      </TR>
                      <TR>
                        <TD class="short">Start Date and Time:
                        </TD>
                        <TD class="bold">'.GetTextualDateTimeFromDatabaseDateTime($row['OvertimeBank_Start']).'
                        </TD>
                      </TR>
                      <TR>
                        <TD>End Date and Time:
                        </TD>
                        <TD class="bold">'.GetTextualDateTimeFromDatabaseDateTime($row['OvertimeBank_End']).'
                        </TD>    
                      </TR>
                      <TR>                 
                        <TD>Hours:
                        </TD>
                        <TD class="bold">'.-$row['OvertimeBank_Hours'].'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(Available: '.SPrintF('%02.2f', $rowTempB['Avail']).')
                        </TD>  
                      </TR>
                      <TR>
                        <TD class="vtop">Description:
                        </TD>
                        <TD rowspan="4" class="bold vtop">'.$row['OvertimeBank_Description'].'
                        </TD>
                      </TR>
                    </TABLE>  
                  </DIV>
                  <DIV>
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center" class="short">
                      <FORM method="post" action="Handlers/Overtime_Handler.php">
                        <INPUT name="Type" type="hidden" value="ApproveBankedOvertime" />
                        <TR>
                          <TD colspan="2" class="header">Approve
                          </TD>
                        </TR>
                        <TR>
                          <TD class="short">Reason (If Denied):
                          </TD>
                          <TD>
                            <INPUT tabindex="1" name="Reason" type="text" class="text" value="'.$_SESSION['ApproveOvertimeBanked'][1].'" />
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="2" class="center">How do you wish to handle this request?
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="2" class="center">
                            <INPUT tabindex="1" name="Submit" type="submit" class="button" value="Approve" />
                            <INPUT tabindex="2" name="Submit" type="submit" class="button" value="Cancel" />
                            <INPUT tabindex="3" name="Submit" type="submit" class="button" value="Deny" />
                          </TD>
                        </TR>
                      </FORM>
                    </TABLE>  
                  </DIV>
                  <BR /><BR />';
            
            Session_Unregister('Graphs'); //This must be here or somewhere before otherwise there is a HUGE problem with caching and the session variables.  
            
            $resultSetAvail = ExecuteQuery('SELECT * FROM AvailabilityType ORDER BY AvailabilityType_ID ASC');
            if (MySQL_Num_Rows($resultSetAvail) > 0)
            {
              $availabilities = array();
              while ($row = MySQL_Fetch_Array($resultSetAvail))
              {
                $availabilities[] = $row['AvailabilityType_Description'];
              }
            }
            
            $resultSetLeave = ExecuteQuery('SELECT * FROM LeaveType ORDER BY LeaveType_ID ASC');
            if (MySQL_Num_Rows($resultSetLeave) > 0)
            {
              $leaves = array();
              while ($row = MySQL_Fetch_Array($resultSetLeave))
              {
                $leaves[] = $row['LeaveType_Description'];
              }
            }
            
            BuildGanttChart(array('SELECT CONCAT(Staff_First_Name, " ", SUBSTRING(Staff_Last_Name, 1, 1)) AS Staff_First_Name FROM Staff WHERE Staff_IsEmployee > 0 ORDER BY Staff_First_Name, Staff_Last_Name ASC',
                                  'SELECT CONCAT(Staff_First_Name, " ", SUBSTRING(Staff_Last_Name, 1, 1)) AS Staff_First_Name, OvertimeBank.* FROM OvertimeBank, Staff WHERE OvertimeBank_Name = Staff_Code AND (OvertimeBank_Start BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" OR OvertimeBank_End BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" OR (OvertimeBank_Start <= "'.$endDate.' 23:59:59" AND OvertimeBank_End >= "'.$startDate.'")) AND OvertimeBank_Payment = "3" AND OvertimeBank_IsApproved = "1" ORDER BY Staff_First_Name, Staff_Last_Name ASC',
                                  'SELECT CONCAT(Staff_First_Name, " ", SUBSTRING(Staff_Last_Name, 1, 1)) AS Staff_First_Name, `Leave`.* FROM `Leave`, Staff WHERE Leave_Employee = Staff_Code AND ((Leave_Start >= "'.$startDate.'" AND Leave_Start <= "'.$endDate.'") OR (Leave_End >= "'.$startDate.'" AND Leave_End <= "'.$endDate.'") OR (Leave_Start <= "'.$endDate.' 23:59:59" AND Leave_End >= "'.$startDate.'")) AND Leave_Days > 0 AND Leave_IsApproved = "1" ORDER BY Staff_First_Name, Staff_Last_Name ASC',
                                  'SELECT CONCAT(Staff_First_Name, " ", SUBSTRING(Staff_Last_Name, 1, 1)) AS Staff_First_Name, Availability.* FROM Availability, Staff WHERE Availability_Name = Staff_Code AND (Availability_Start BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" OR Availability_End BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" OR (Availability_Start <= "'.$startDate.' 23:59:59" AND Availability_End >= "'.$endDate.'")) ORDER BY Staff_First_Name, Staff_Last_Name ASC',
                                  'SELECT CONCAT(Staff_First_Name, " ", SUBSTRING(Staff_Last_Name, 1, 1)) AS Staff_First_Name, `Leave`.* FROM `Leave`, Staff WHERE Leave_Employee = Staff_Code AND ((Leave_Start >= "'.$startDate.'" AND Leave_Start <= "'.$endDate.'") OR (Leave_End >= "'.$startDate.'" AND Leave_End <= "'.$endDate.'") OR (Leave_Start <= "'.$endDate.' 23:59:59" AND Leave_End >= "'.$startDate.'")) AND Leave_Days > 0 AND Leave_IsApproved = "0" ORDER BY Staff_First_Name, Staff_Last_Name ASC',
                                  'SELECT CONCAT(Staff_First_Name, " ", SUBSTRING(Staff_Last_Name, 1, 1)) AS Staff_First_Name, OvertimeBank.* FROM OvertimeBank, Staff WHERE OvertimeBank_Name = Staff_Code AND (OvertimeBank_Start BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" OR OvertimeBank_End BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" OR (OvertimeBank_Start <= "'.$endDate.' 23:59:59" AND OvertimeBank_End >= "'.$startDate.'")) AND OvertimeBank_Payment = "3" AND OvertimeBank_IsApproved = "0" ORDER BY Staff_First_Name, Staff_Last_Name ASC'), "", "", 'Availability for '.$dateRange, $startDate, $endDate, 'Monthly', 
                            array(array('Staff_First_Name'),
                                  array('Staff_First_Name', 'OvertimeBank_Start', 'OvertimeBank_End', 'OvertimeBank_Payment'),
                                  array('Staff_First_Name', 'Leave_Start', 'Leave_End', 'Leave_Type'),
                                  array('Staff_First_Name', 'Availability_Start', 'Availability_End', 'Availability_Type'),
                                  array('Staff_First_Name', 'Leave_Start', 'Leave_End', 'Leave_Type'),
                                  array('Staff_First_Name', 'OvertimeBank_Start', 'OvertimeBank_End', 'OvertimeBank_Payment')), 
                            array(array(),
                                  array("", "", 'Banked Overtime'),
                                  $leaves,
                                  $availabilities,
                                  array('Pending'),
                                  array()),
                            array(array(),
                                  array("", "", '#FF511C'),
                                  array('#329E1F', '#03B272', '#0BB7AC', '#0B9BC6', '#0C61C9', '#6919FF', '#A215D6'),
                                  array('#FF11FB', '#FF0F83', '#DB0A2D', '#FFAA42', '#FFE049', '#B4FF32'),
                                  array('#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF'),
                                  array("", "", '#FFFFFF')));
            echo '<DIV class="contentflow">
                    <P>This is the availability over the requested overtime period.</P>
                    <BR /><BR />
                    <COMMENT>
                      <IMG src="Scripts/Graphing.php?Graph=0&Random='.Rand().'" />
                    </COMMENT>
                    <!--[if lte IE 6]>
                      <IFRAME src="Scripts/Graphing.php?Graph=0&Random='.Rand().'" width="900" height="600" frameborder="0"></IFRAME>
                    <![endif]-->
                    <!--[if gt IE 6]>
                      <IMG src="Scripts/Graphing.php?Graph=0&Random='.Rand().'" />
                    <![endif]-->
                  </DIV>
                  <BR /><BR />';
          } else
          if (isset($_SESSION['ApproveOvertimeNormal']))
          {            
            $row = MySQL_Fetch_Array(ExecuteQuery('SELECT OvertimePreApp.*, IF(OvertimePreApp_Multi > 0, "Yes", "No") AS Multi FROM OvertimePreApp WHERE OvertimePreApp_ID = "'.$_SESSION['ApproveOvertimeNormal'][0].'"')); 
            if ($row['OvertimePreApp_Start'] == $row['OvertimePreApp_End'])
              $dateRange = GetTextualDateFromDatabaseDate($row['OvertimePreApp_Start']);
            else        
              $dateRange = GetTextualDateFromDatabaseDate($row['OvertimePreApp_Start']).' to '.GetTextualDateFromDatabaseDate($row['OvertimePreApp_End']);
            
            $rowApprove = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = '.$row['OvertimePreApp_Authorised'].''));
            
            $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = "'.$row['OvertimePreApp_Name'].'"'));
            BuildContentHeader('Approve Overtime - '.$rowTemp['Staff_First_Name'].' '.$rowTemp['Staff_Last_Name'].' for '.$dateRange, "", "", true);
            $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Project WHERE Project_Code = "'.$row['OvertimePreApp_Project'].'"'));
            echo '<DIV class="contentflow">
                    <P>This overtime is set to be authorised by <B>'.$rowApprove['Staff_First_Name'].' '.$rowApprove['Staff_Last_Name'].'</B></P>
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center" class="short">
                      <TR>
                        <TD colspan="2" class="header">Overtime Details
                        </TD>
                      </TR>
                      <TR>
                        <TD class="short">Project:
                        </TD>
                        <TD class="bold">'.$rowTemp['Project_Description'].'
                        </TD>
                      </TR>
                      <TR>
                        <TD>Start Date and Time:
                        </TD>
                        <TD class="bold">'.GetTextualDateTimeFromDatabaseDateTime($row['OvertimePreApp_Start']).'
                        </TD>
                      </TR>
                      <TR>
                        <TD>End Date and Time:
                        </TD>
                        <TD class="bold">'.GetTextualDateTimeFromDatabaseDateTime($row['OvertimePreApp_End']).'
                        </TD>    
                      </TR>
                      <TR>                 
                        <TD>Hours:
                        </TD>
                        <TD class="bold">'.$row['OvertimePreApp_Hours'].'
                        </TD>  
                      </TR>
                      <TR>                 
                        <TD>Multiple Entries:
                        </TD>
                        <TD class="bold">'.$row['Multi'].'
                        </TD>  
                      </TR>
                      <TR>
                        <TD class="vtop">Description:
                        </TD>
                        <TD class="bold vtop">'.$row['OvertimePreApp_Description'].'
                        </TD>
                      </TR>
                    </TABLE>  
                  </DIV>
                  <DIV>
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center" class="short">
                      <FORM method="post" action="Handlers/Overtime_Handler.php">
                        <INPUT name="Type" type="hidden" value="ApproveNormalOvertime" />
                        <TR>
                          <TD colspan="2" class="header">Approve
                          </TD>
                        </TR>
                        <TR>
                          <TD class="short">Reason (If Denied):
                          </TD>
                          <TD>
                            <INPUT tabindex="1" name="Reason" type="text" class="text" value="'.$_SESSION['ApproveOvertimeNormal'][1].'" />
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="2" class="center">How do you wish to handle this request?
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="2" class="center">
                            <INPUT tabindex="1" name="Submit" type="submit" class="button" value="Approve" />
                            <INPUT tabindex="2" name="Submit" type="submit" class="button" value="Cancel" />
                            <INPUT tabindex="3" name="Submit" type="submit" class="button" value="Deny" />
                          </TD>
                        </TR>
                      </FORM>
                    </TABLE>  
                  <BR /> <BR /><BR /> <BR />';
            
            
                 $countFail =0;
                 $startDate = date('Y-m-d', strtotime(GetSessionDateFromDatabaseDate($row['OvertimePreApp_Start']).' - 4 week'));
                 $endDate = date('Y-m-d', strtotime(GetSessionDateFromDatabaseDate($row['OvertimePreApp_End']).' + 4 week'));
                 $dateRange = GetTextualDateFromDatabaseDate($startDate).' to '.GetTextualDateFromDatabaseDate($endDate);
                 
                 //$resultSet = ExecuteQuery('SELECT OvertimePreApp.*, Project_Description FROM OvertimePreApp, Project WHERE OvertimePreApp_Name = '.$row['OvertimePreApp_Name'].' AND OvertimePreApp_Project = Project_Code AND OvertimePreApp_Start BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" AND OvertimePreApp_IsApproved = "1" ORDER BY OvertimePreApp_Start ASC');
                 $resultSet = ExecuteQuery('SELECT CONCAT(DATE_FORMAT(`OvertimePreApp_Start`,"%d %b %Y"), " at ", DATE_FORMAT(`OvertimePreApp_Start`,"%H:%i")," to ",DATE_FORMAT(`OvertimePreApp_End`,"%d %b %Y"), " at ", DATE_FORMAT(`OvertimePreApp_End`,"%H:%i")) AS Period,DATE_FORMAT(`OvertimePreApp_Date_Log`,"%d %b %Y") AS LoggedDate, CONCAT(`Staff_First_Name`," ",`Staff_Last_Name`) As AuthBy, OvertimePreApp.*, Project_Description FROM OvertimePreApp, Project,Staff WHERE OvertimePreApp_Name = '.$row['OvertimePreApp_Name'].' AND OvertimePreApp_Project = Project_Code AND `OvertimePreApp_Authorised` = Staff.`Staff_Code` AND OvertimePreApp_Start BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" AND OvertimePreApp_IsApproved = "1" ORDER BY OvertimePreApp_Start ASC');
                 //$resultSet2 = ExecuteQuery('SELECT OvertimePreApp.*, Project_Description FROM OvertimePreApp, Project WHERE OvertimePreApp_Name = '.$row['OvertimePreApp_Name'].' AND OvertimePreApp_Project = Project_Code AND OvertimePreApp_Start BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" AND OvertimePreApp_IsApproved = "0" ORDER BY OvertimePreApp_Start ASC');
                 $resultSet2 = ExecuteQuery('SELECT CONCAT(DATE_FORMAT(`OvertimePreApp_Start`,"%d %b %Y"), " at ", DATE_FORMAT(`OvertimePreApp_Start`,"%H:%i")," to ",DATE_FORMAT(`OvertimePreApp_End`,"%d %b %Y"), " at ", DATE_FORMAT(`OvertimePreApp_End`,"%H:%i")) AS Period,DATE_FORMAT(`OvertimePreApp_Date_Log`,"%d %b %Y") AS LoggedDate, CONCAT(`Staff_First_Name`," ",`Staff_Last_Name`) As AuthBy, OvertimePreApp.*, Project_Description FROM OvertimePreApp, Project,Staff WHERE OvertimePreApp_Name = '.$row['OvertimePreApp_Name'].' AND OvertimePreApp_Project = Project_Code AND `OvertimePreApp_Authorised` = Staff.`Staff_Code` AND OvertimePreApp_Start BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" AND OvertimePreApp_IsApproved = "0" ORDER BY OvertimePreApp_Start ASC');
                 if ((MySQL_Num_Rows($resultSet) > 0) || (MySQL_Num_Rows($resultSet2) > 0))
                 {
                    $currentID = $row['OvertimePreApp_ID'];
                    $ApprovedCount =0;
                    BuildContentHeader('Overtime Summary Details for '.$dateRange, "", "", false);
                    echo '<DIV class="contentflow">';
                    
                    if (MySQL_Num_Rows($resultSet) > 0)
                    {
                        $ApprovedCount++;
                      echo '<P>This is the <strong>Approved overtime</strong> listed.</P>
                              <BR /><BR />';
                          /**echo '<TABLE cellspacing="5" align="center" class="long">
                                <TR>
                                  <TD colspan="8" class="header">Approved Overtime Details
                                  </TD>
                                </TR>
                                <TR>
                                  <TD class="subheader">Period
                                  </TD>
                                  <TD class="subheader">Hours
                                  </TD>
                                  <TD class="subheader">Hours Used
                                  </TD>
                                  <TD class="subheader">Hours Remaining
                                  </TD>
                                  <TD class="subheader">Project
                                  </TD>
                                  <TD class="subheader">Authorised By
                                  </TD>
                                  <TD class="subheader">Date Logged
                                  </TD>
					<TD class="subheader">Description
                                  </TD>
                                </TR>';
                                $totalAllocated = 0;
                                $totalUsed = 0;
                                $totalRemaining = 0;**/
                                $rows = array();
                                while ($row = MySQL_Fetch_Array($resultSet))
                                {
                                    $rows[] = $row;
                                  /**$totalAllocated += $row['OvertimePreApp_Hours'];
                                  $totalUsed += $row['OvertimePreApp_OvertimeHoursUsed'];
                                  
                                  $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = '.$row['OvertimePreApp_Authorised'].'')); 
                                  echo '<TR>
                                          <TD class="rowA">'.GetTextualDateFromDatabaseDate($row['OvertimePreApp_Start']).' to '.GetTextualDateFromDatabaseDate($row['OvertimePreApp_End']).'
                                          </TD>
                                          <TD class="rowA center">'.SPrintF('%02.2f', $row['OvertimePreApp_Hours']).'
                                          </TD>
                                          <TD class="rowA center">'.SPrintF('%02.2f', $row['OvertimePreApp_OvertimeHoursUsed']).'
                                          </TD>';
                                  
                                  if (($row['OvertimePreApp_OvertimeUsed'] == "1") || ($row['OvertimePreApp_End'] < Date('Y-m-d').' 23:59:59'))
                                    echo  '<TD class="rowA center">0.00
                                          </TD>';
                                  else
                                    {
                                      $totalRemaining += $row['OvertimePreApp_Hours'] - $row['OvertimePreApp_OvertimeHoursUsed'];                                    
                                      echo  '<TD class="rowA center">'.SPrintF('%02.2f', $row['OvertimePreApp_Hours'] - $row['OvertimePreApp_OvertimeHoursUsed']).'
                                            </TD>';
                                    }
                                  
                                  echo   '<TD class="rowA">'.$row['Project_Description'].'
                                          </TD>
                                          <TD class="rowA">'.$rowTemp['Staff_First_Name'].' '.$rowTemp['Staff_Last_Name'].'
                                          </TD>
                                          <TD class="rowA">'.GetTextualDateFromDatabaseDate($row['OvertimePreApp_Date_Log']).'
                                          </TD>
						<TD class="rowA">'.$row['OvertimePreApp_Description'].'
                                          </TD>
                                        </TR>';**/
                                }
                         /** echo '<TR>
                                  <TD class="rowB bold">Total
                                  </TD>
                                  <TD class="rowB center bold">'.SPrintF('%02.2f', $totalAllocated).'
                                  </TD>
                                  <TD class="rowB center bold">'.SPrintF('%02.2f', $totalUsed).'
                                  </TD>
                                  <TD class="rowB center bold">'.SPrintF('%02.2f', $totalRemaining).'
                                  </TD>
                                  <TD class="rowB bold">
                                  </TD>
                                  <TD class="rowB bold">
                                  </TD>
                                  <TD class="rowB bold">
                                  </TD>
                                  <TD class="rowB bold">
                                  </TD>
                                </TR>
                              </TABLE>
                            <BR /><BR />';**/
                          $approvedjson = json_encode($rows);
                          echo '<div style="width:950px; margin-left: auto; margin-right: auto;"><div id="approved" class="s4_grid"></div></div><BR /><BR /><BR /><BR />';
                    } else
                        $approvedjson = array();
                    
                  
                    if (MySQL_Num_Rows($resultSet2) > 0)
                    {
                      echo '<P>This is the <strong>Pending overtime</strong> listed. <strong>N.B:</strong><i>The current overtime is highlighted in yellow</i></P>
                              <BR /><BR />';
                      /**     echo '<TABLE cellspacing="5" align="center" class="long">
                                <TR>
                                  <TD colspan="8" class="header">Pending Overtime Details
                                  </TD>
                                </TR>
                                <TR>
                                  <TD class="subheader">Period
                                  </TD>
                                  <TD class="subheader">Hours
                                  </TD>
                                  <TD class="subheader">Hours Used
                                  </TD>
                                  <TD class="subheader">Hours Remaining
                                  </TD>
                                  <TD class="subheader">Project
                                  </TD>
                                  <TD class="subheader">Authorised By
                                  </TD>
                                  <TD class="subheader">Date Logged
                                  </TD>
					<TD class="subheader">Description
                                  </TD>
                                </TR>';
                                
                                
                                $totalAllocated = 0;
                                $totalUsed = 0;
                                $totalRemaining = 0;*/
                                $rows = array();
                                while ($row = MySQL_Fetch_Array($resultSet2))
                                {
                                    $rows[] = $row;
                                   /**if($row['OvertimePreApp_ID'] == $_SESSION['ApproveOvertimeNormal'][0])
                                       $bgColor = "yellow";
                                   else
                                        $bgColor = "rowA";
                                  $totalAllocated += $row['OvertimePreApp_Hours'];
                                  $totalUsed += $row['OvertimePreApp_OvertimeHoursUsed'];
                                  
                                  $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = '.$row['OvertimePreApp_Authorised'].'')); 
                                  echo '<TR>
                                          <TD class="'.$bgColor.'">'.GetTextualDateFromDatabaseDate($row['OvertimePreApp_Start']).' to '.GetTextualDateFromDatabaseDate($row['OvertimePreApp_End']).'
                                          </TD>
                                          <TD class="'.$bgColor.' center">'.SPrintF('%02.2f', $row['OvertimePreApp_Hours']).'
                                          </TD>
                                          <TD class="'.$bgColor.' center">'.SPrintF('%02.2f', $row['OvertimePreApp_OvertimeHoursUsed']).'
                                          </TD>';
                                  
                                  if (($row['OvertimePreApp_OvertimeUsed'] == "1") || ($row['OvertimePreApp_End'] < Date('Y-m-d').' 23:59:59'))
                                    echo  '<TD class="'.$bgColor.' center">0.00
                                          </TD>';
                                  else
                                    {
                                      $totalRemaining += $row['OvertimePreApp_Hours'] - $row['OvertimePreApp_OvertimeHoursUsed'];                                    
                                      echo  '<TD class="'.$bgColor.' center">'.SPrintF('%02.2f', $row['OvertimePreApp_Hours'] - $row['OvertimePreApp_OvertimeHoursUsed']).'
                                            </TD>';
                                    }
                                  
                                  echo   '<TD class="'.$bgColor.'">'.$row['Project_Description'].'
                                          </TD>
                                          <TD class="'.$bgColor.'">'.$rowTemp['Staff_First_Name'].' '.$rowTemp['Staff_Last_Name'].'
                                          </TD>
                                          <TD class="'.$bgColor.'">'.GetTextualDateFromDatabaseDate($row['OvertimePreApp_Date_Log']).'
                                          </TD>
						<TD class="'.$bgColor.'">'.$row['OvertimePreApp_Description'].'
                                          </TD>
                                        </TR>';**/
                                }
                        /**  echo '<TR>
                                  <TD class="rowB bold">Total
                                  </TD>
                                  <TD class="rowB center bold">'.SPrintF('%02.2f', $totalAllocated).'
                                  </TD>
                                  <TD class="rowB center bold">'.SPrintF('%02.2f', $totalUsed).'
                                  </TD>
                                  <TD class="rowB center bold">'.SPrintF('%02.2f', $totalRemaining).'
                                  </TD>
                                  <TD class="rowB bold">
                                  </TD>
                                  <TD class="rowB bold">
                                  </TD>
                                  <TD class="rowB bold">
                                  </TD>
                                  <TD class="rowB bold">
                                  </TD>
                                </TR>
                              </TABLE>
                              <BR /><BR />';**/
                          
                          $ordersjson = json_encode($rows);
                          echo '<div style="width:950px; margin-left: auto; margin-right: auto;"><div id="pending" class="s4_grid"></div></div><BR /><BR /><BR /><BR />';
                    } else
                       $countFail++;
                    
                    echo '</DIV>';
                 }
            
          } 
          else
          if(isset($_SESSION['ApproveOvertimeMulty'])){
              $row = MySQL_Fetch_Array(ExecuteQuery('SELECT concat(Staff_First_Name," ",Staff_Last_Name) as person FROM Staff WHERE Staff_Code ='.$_SESSION['ApproveOvertimeMulty']));
              echo '<div id = "ERRERS">Requested operation failed. An error occurred during the submission process.</div>';
              BuildContentHeader('Approve Multiple Overtime - '.$row['person'], "", "", false);  
              echo'</br><div id="overtimeApproveGrid"  data-staffcode ="'.$_SESSION['ApproveOvertimeMulty'].'">Loading...</div>';
              echo ' <FORM method="post" action="Handlers/Overtime_Handler.php"><INPUT name="Type" type="hidden" value="CancleSesssoin">'
              . '<INPUT  style="float: right;" name="Submit" type="submit" class="button" value="Back" /><FORM>';
               // unset($_SESSION['ApproveOvertimeMulty']);
          }else    
          if (isset($_SESSION['EditOvertime']))
          {
            $row = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM OvertimeBank WHERE OvertimeBank_ID = "'.$_SESSION['EditOvertime'][0].'"'));
            $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT (OvertimePreApp_Hours - OvertimePreApp_OvertimeHoursUsed + '.$row['OvertimeBank_Hours'].') AS OvertimeAvail FROM OvertimePreApp WHERE OvertimePreApp_ID = "'.$row['OvertimeBank_PreApproved'].'"'));
            BuildContentHeader('Edit Overtime - '.GetTextualDateTimeFromDatabaseDateTime($row['OvertimeBank_Start']).' to '.GetTextualDateTimeFromDatabaseDateTime($row['OvertimeBank_End']), "", "", false);  
            echo '<DIV class="contentflow">
                    <P>Once you have made changes, ensure that you submit them otherwise they will not take effect.</P>
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center">
                      <FORM method="post" action="Handlers/Overtime_Handler.php">
                        <INPUT name="Type" type="hidden" value="Edit">
                        <TR>
                          <TD colspan="2" class="header">Overtime Details
                          </TD>
                        </TR>
                        <TR>
                          <TD class="short">Start Date and Time:
                            <SPAN class="note">**
                            </SPAN>
                          </TD>
                          <TD>';
                            BuildDaySelector(1, 'StartDay', GetDayFromSessionDate($_SESSION['EditOvertime'][1]));
                            echo '&nbsp;';
                            BuildMonthSelector(2, 'StartMonth', GetMonthFromSessionDate($_SESSION['EditOvertime'][1]));
                            echo '&nbsp;';
                            BuildYearSelector(3, 'StartYear', GetYearFromSessionDate($_SESSION['EditOvertime'][1]));
                            echo '&nbsp;at&nbsp;';
                            BuildHourSelector(4, 'StartHour', GetHourFromSessionDateTime($_SESSION['EditOvertime'][1]));
                            echo '&nbsp;:';
                            BuildMinuteSelector(5, 'StartMinute', GetMinuteFromSessionDateTime($_SESSION['EditOvertime'][1]));
                    echo '</TD>
                        </TR>
                        <TR>
                          <TD>End Date:
                            <SPAN class="note">**
                            </SPAN>
                          </TD>
                          <TD>';
                            BuildDaySelector(6, 'EndDay', GetDayFromSessionDate($_SESSION['EditOvertime'][3]));
                            echo '&nbsp;';
                            BuildMonthSelector(7, 'EndMonth', GetMonthFromSessionDate($_SESSION['EditOvertime'][3]));
                            echo '&nbsp;';
                            BuildYearSelector(8, 'EndYear', GetYearFromSessionDate($_SESSION['EditOvertime'][3]));
                            echo '&nbsp;at&nbsp;';
                            BuildHourSelector(9, 'EndHour', GetHourFromSessionDateTime($_SESSION['EditOvertime'][3]));
                            echo '&nbsp;:';
                            BuildMinuteSelector(10, 'EndMinute', GetMinuteFromSessionDateTime($_SESSION['EditOvertime'][3]));
                    echo '</TD>
                        </TR>
                        <TR>
                          <TD>Overtime Hours:
                            <SPAN class="note">***
                            </SPAN>
                          </TD>
                          <TD>
                            <INPUT tabindex="11" name="Hours" type="text" class="veryshort" maxlength="11" value="'.$_SESSION['EditOvertime'][4].'">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(Available: <B>'.$rowTemp['OvertimeAvail'].'</B>).
                          </TD>
                        </TR>
                        <TR>
                          <TD class="vtop">Description:
                            <SPAN class="note">
                            </SPAN>
                          </TD>
                          <TD>
                            <TEXTAREA tabindex="12" name="Description" class="long" maxlength="500">'.$_SESSION['EditOvertime'][2].'</TEXTAREA>
                          </TD>
                        </TR>
                        <TR>
                          <TD>Overtime Banked: 
                          </TD>
                          <TD class="bold">Yes
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="2" class="center">
                            <INPUT tabindex="12" name="Submit" type="submit" class="button" value="Submit" />
                            <INPUT tabindex="13" name="Submit" type="submit" class="button" value="Cancel" />                   
                          </TD>
                        </TR>
                      </FORM>
                    </TABLE> 
                  </DIV> 
                  <DIV>
                    <BR />
                    <SPAN class="note">*
                    </SPAN>
                    These fields are required.
                    <BR />
                    <SPAN class="note">**
                    </SPAN>
                    The actual start and end times must be used.
                    <BR />
                    <SPAN class="note">***
                    </SPAN>
                    These are the calculated overtime hours. This includes the additonal hour for travel time - if applicable.
                  </DIV>';
          } else
          if (isset($_SESSION['ExcessOvertime']))
          {
            BuildContentHeader('Transfer Excess Overtime', "", "", false);  
            echo '<DIV class="contentflow">
                    <BR/><BR/>
                    <TABLE cellspacing="5" align="center" class="short">
                      <FORM method="post" action="Handlers/Overtime_Handler.php">
                        <INPUT name="Type" type="hidden" value="Excess" />
                        <TR>
                          <TD colspan="4" class="header">Transfer Excess
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="4" class="center">Are you sure you wish to transfer excess banked overtime?
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="2" class="center">
                            <INPUT tabindex="1" name="Submit" type="submit" class="button" value="Yes" />   
                            <INPUT tabindex="2" name="Submit" type="submit" class="button" value="No" />                  
                          </TD>
                        </TR>
                      </FORM>
                    </TABLE>  
                  </DIV>';                             
          } else
          if (isset($_SESSION['RequestOvertimeBanked']))
          {
              $rowRequestFor = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = '.$_SESSION['RequestOvertimeBanked'][100].''));
              $row = MySQL_Fetch_Array(ExecuteQuery('SELECT SUM(OvertimeBank_Hours) AS Avail FROM OvertimeBank WHERE OvertimeBank_Name = '.$_SESSION['cUID'].' AND OvertimeBank_IsApproved = "1"'));
              if((($_SESSION['cAuth'] & 64) || ($_SESSION['cUID'] == 30)) && ($_SESSION['RequestOvertimeNormal'][100] != $_SESSION['cUID']))
                  BuildContentHeader('Request Banked Overtime Use - '.$rowRequestFor['Staff_First_Name'].' '.$rowRequestFor['Staff_Last_Name'], "", "", false);
              else
            BuildContentHeader('Request Banked Overtime Use', "", "", false);
            echo '<DIV class="contentflow">
                    <P>Enter the details of the banked overtime use below. Ensure that all the required information is entered before submission and that this information is valid. When you submit this request the person authorising the banked overtime will be notified. Once the banked overtime use has been approved you will be notified by mail.</P>
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center">
                      <FORM method="post" action="Handlers/Overtime_Handler.php">
                        <INPUT name="Type" type="hidden" value="RequestBankedOvertime" />
                        <TR>
                          <TD colspan="2" class="header">Overtime Details
                          </TD>
                        </TR>';
                     if ($LoggedinFullName)
                         {
                            echo'<TR>
                            <TD colspan="4" style="color:red; text-align:center;">You are logged in as : '.$LoggedinFullName.'</TD></TR>';
                         }
                   echo'<TR>
                          <TD>Authorise By:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                            BuildStaffManSelector(1, 'Authorise', 'seltest long', $_SESSION['RequestOvertimeBanked'][4]);
                    echo '</TD>
                        </TR>
                        <TR>
                          <TD class="short">Start Date and Time:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                            BuildDaySelector(2, 'StartDay', GetDayFromSessionDate($_SESSION['RequestOvertimeBanked'][0]));
                            echo '&nbsp;';
                            BuildMonthSelector(3, 'StartMonth', GetMonthFromSessionDate($_SESSION['RequestOvertimeBanked'][0]));
                            echo '&nbsp;';
                            BuildYearSelector(4, 'StartYear', GetYearFromSessionDate($_SESSION['RequestOvertimeBanked'][0]));
                            echo '&nbsp;at&nbsp;';
                            BuildHourSelector(5, 'StartHour', GetHourFromSessionDateTime($_SESSION['RequestOvertimeBanked'][0]));
                            echo '&nbsp;:';
                            BuildMinuteSelector(6, 'StartMinute', GetMinuteFromSessionDateTime($_SESSION['RequestOvertimeBanked'][0]));
                    echo '</TD>
                        </TR>
                        <TR>
                          <TD>End Date and Time:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                            BuildDaySelector(7, 'EndDay', GetDayFromSessionDate($_SESSION['RequestOvertimeBanked'][2]));
                            echo '&nbsp;';
                            BuildMonthSelector(8, 'EndMonth', GetMonthFromSessionDate($_SESSION['RequestOvertimeBanked'][2]));
                            echo '&nbsp;';
                            BuildYearSelector(9, 'EndYear', GetYearFromSessionDate($_SESSION['RequestOvertimeBanked'][2]));
                            echo '&nbsp;at&nbsp;';
                            BuildHourSelector(10, 'EndHour', GetHourFromSessionDateTime($_SESSION['RequestOvertimeBanked'][2]));
                            echo '&nbsp;:';
                            BuildMinuteSelector(11, 'EndMinute', GetMinuteFromSessionDateTime($_SESSION['RequestOvertimeBanked'][2]));
                    echo '</TD>
                        </TR>
                        <TR>
                          <TD>Hours:
                            <SPAN class="note">**
                            </SPAN>
                          </TD>
                          <TD>
                            <INPUT tabindex="12" name="Hours" type="text" class="text veryshort" value="'.$_SESSION['RequestOvertimeBanked'][3].'" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(Available: <B>'.SPrintF('%02.2f', $row['Avail']).'</B>)
                          </TD>
                        </TR>
                        <TR>
                          <TD class="vtop">Description:
                          </TD>
                          <TD>
                            <TEXTAREA tabindex="13" name="Description" class="long">'.$_SESSION['RequestOvertimeBanked'][1].'</TEXTAREA>
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="2" class="center">
                            <INPUT tabindex="14" id="requestBankedOvertime" name="Submit" type="submit" class="button" value="Submit" />
                            <INPUT tabindex="15" name="Submit" type="submit" class="button" value="Cancel" />                   
                          </TD>
                        </TR>
                      </FORM>
                    </TABLE>
                  </DIV>
                  <DIV>
                    <BR />
                    <SPAN class="note">*
                    </SPAN>
                    These fields are required.
                    <BR />
                    <SPAN class="note">**
                    </SPAN>
                    This is the number of hours that will be used over the time period.
                  </DIV>';                             
          } else
          if (isset($_SESSION['RequestOvertimeNormal'])) 
          {
            $row = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = '.$_SESSION['RequestOvertimeNormal'][100].''));
            if ((($_SESSION['cAuth'] & 64) || ($_SESSION['cUID'] == 30)) && ($_SESSION['RequestOvertimeNormal'][100] != $_SESSION['cUID']))
              BuildContentHeader('Request Overtime - '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'], "", "", false);
            else
              BuildContentHeader('Request Overtime', "", "", false);
            echo '<DIV class="contentflow">
                    <P>Enter the details of the overtime below. Ensure that all the required information is entered before submission and that this information is valid. When you submit this request the person authorising the overtime will be notified. Once the overtime has been approved you will be notified by mail.</P>
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center" class="long">
                      <FORM method="post" action="Handlers/Overtime_Handler.php">
                        <INPUT name="Type" type="hidden" value="RequestNormalOvertime" />
                        <TR>
                          <TD colspan="4" class="header">Overtime Details
                          </TD>
                        </TR>';
                     if ($LoggedinFullName)
                         {
                            echo'<TR>
                            <TD colspan="4" style="color:red; text-align:center;">You are logged in as : '.$LoggedinFullName.'</TD></TR>';
                         }
                   echo'<TR>
                          <TD>Authorise By:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                            BuildStaffManSelector(1, 'Authorise', 'seltest', $_SESSION['RequestOvertimeNormal'][0], '8) OR (Staff_Code = "244"');
                    echo '</TD>
                          <TD class="short">Description:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD rowspan="4">
                            <TEXTAREA tabindex="14" name="Description" class="standard">'.$_SESSION['RequestOvertimeNormal'][1].'</TEXTAREA>
                          </TD>
                        </TR>
                        <TR>
                          <TD>Project:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                            BuildProjectSelector(2, 'Project', 'seltest long', $_SESSION['RequestOvertimeNormal'][2], 'Open', true, true, true, true);
                    echo '</TD>
                        </TR>
                        <TR>
                          <TD class="short">Start Date and Time:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                            BuildDaySelector(3, 'StartDay', GetDayFromSessionDate($_SESSION['RequestOvertimeNormal'][3]));
                            echo '&nbsp;';
                            BuildMonthSelector(4, 'StartMonth', GetMonthFromSessionDate($_SESSION['RequestOvertimeNormal'][3]));
                            echo '&nbsp;';
                            BuildYearSelector(5, 'StartYear', GetYearFromSessionDate($_SESSION['RequestOvertimeNormal'][3]));
                            echo '&nbsp;at&nbsp;';
                            BuildHourSelector(6, 'StartHour', GetHourFromSessionDateTime($_SESSION['RequestOvertimeNormal'][3]));
                            echo '&nbsp;:';
                            BuildMinuteSelector(7, 'StartMinute', GetMinuteFromSessionDateTime($_SESSION['RequestOvertimeNormal'][3]));
                    echo '</TD>
                        </TR>
                        <TR>
                          <TD>End Date and Time:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                            BuildDaySelector(8, 'EndDay', GetDayFromSessionDate($_SESSION['RequestOvertimeNormal'][4]));
                            echo '&nbsp;';
                            BuildMonthSelector(9, 'EndMonth', GetMonthFromSessionDate($_SESSION['RequestOvertimeNormal'][4]));
                            echo '&nbsp;';
                            BuildYearSelector(10, 'EndYear', GetYearFromSessionDate($_SESSION['RequestOvertimeNormal'][4]));
                            echo '&nbsp;at&nbsp;';
                            BuildHourSelector(11, 'EndHour', GetHourFromSessionDateTime($_SESSION['RequestOvertimeNormal'][4]));
                            echo '&nbsp;:';
                            BuildMinuteSelector(12, 'EndMinute', GetMinuteFromSessionDateTime($_SESSION['RequestOvertimeNormal'][4]));
                    echo '</TD>
                        </TR>
                        <TR>
                          <TD>Hours:
                            <SPAN class="note">**
                            </SPAN>
                          </TD>
                          <TD class="bold">
                            <INPUT tabindex="13" name="Hours" type="text" class="text veryshort" value="'.$_SESSION['RequestOvertimeNormal'][5].'" />
                          </TD>
                          <TD>Multiple Entries:
                            <SPAN class="note">**
                            </SPAN>
                          </TD>
                          <TD>';
                            BuildYesNoSelector(15, 'Multi', 'veryshort', $_SESSION['RequestOvertimeNormal'][6]);
                    echo '</TD>
                        </TR>
                        <TR>
                          <TD colspan="4" class="center">
                            <INPUT tabindex="16" name="Submit" type="submit" class="button" value="Submit" />
                            <INPUT tabindex="17" name="Submit" type="submit" class="button" value="Cancel" />                   
                          </TD>
                        </TR>
                      </FORM>
                    </TABLE>
                  </DIV>
                  <DIV>
                    <BR />
                    <SPAN class="note">*
                    </SPAN>
                    These fields are required.
                    <BR />
                    <SPAN class="note">**
                    </SPAN>
                    This is the number of hours that will be used over the time period.
                  </DIV>';                             
          } else
          if (isset($_SESSION['TransferOvertime'])) 
          {
            $row = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = '.$_SESSION['TransferOvertime'][0].''));            
            BuildContentHeader('Transfer Overtime - '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'], "", "", false); 
            $row = MySQL_Fetch_Array(ExecuteQuery('SELECT SUM(OvertimeBank_Hours) AS Avail FROM OvertimeBank WHERE OvertimeBank_Name = "'.$_SESSION['TransferOvertime'][0].'" AND OvertimeBank_IsApproved = "1"')); 
            if ($row['Avail'] > 0)
            {
              echo '<DIV class="contentflow">
                      <P>Enter the details of the transfer below. Ensure that all the required information is entered before submission and that this information is valid.</P>
                      <BR /><BR />
                      <TABLE cellspacing="5" align="center" class="short">
                        <FORM method="post" action="Handlers/Overtime_Handler.php">
                          <INPUT name="Type" type="hidden" value="Transfer">
                          <TR>
                            <TD colspan="2" class="header">Overtime Details
                            </TD>
                          </TR>
                          <TR>
                            <TD class="short">Overtime Hours:
                              <SPAN class="note">*
                              </SPAN>
                            </TD>
                            <TD class="standard">
                              <INPUT tabindex="1" name="Hours" type="text" class="veryshort" maxlength="11" value="'.$_SESSION['TransferOvertime'][1].'" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(Available: <B>'.$row['Avail'].'</B>).
                            </TD>
                          </TR>
                          <TR>
                            <TD class="short vtop">Description:
                              <SPAN class="note">*
                              </SPAN>
                            </TD>
                            <TD class="standard">
                              <TEXTAREA tabindex="2" name="Description" class="long" maxlength="500">'.$_SESSION['TransferOvertime'][2].'</TEXTAREA>
                            </TD>
                          </TR>
                          <TR>
                            <TD colspan="2" class="center">
                              <INPUT tabindex="3" name="Submit" type="submit" class="button" value="Submit" />
                              <INPUT tabindex="4" name="Submit" type="submit" class="button" value="Cancel" />                   
                            </TD>
                          </TR>
                        </FORM>
                      </TABLE> 
                    </DIV> 
                    <DIV>
                      <BR />
                      <SPAN class="note">*
                      </SPAN>
                      These fields are required.
                    </DIV>';       
            } else
            {
			  Session_Unregister('TransferOvertime');
              echo '<DIV class="contentflow">
                      <P>There is no overtime banked for this staff member.</P>
                    </DIV>';
            }
          } else
          if (isset($_SESSION['ViewOvertime']))
          {
            $startDate = GetDatabaseDateFromSessionDate($_SESSION['ViewOvertime'][0]);
            $endDate = GetDatabaseDateFromSessionDate($_SESSION['ViewOvertime'][1]);
            if ($_SESSION['ViewOvertime'][0] == $_SESSION['ViewOvertime'][1])
              $dateRange = GetTextualDateFromSessionDate($_SESSION['ViewOvertime'][0]);
            else
              $dateRange = GetTextualDateFromSessionDate($_SESSION['ViewOvertime'][0]).' to '.GetTextualDateFromSessionDate($_SESSION['ViewOvertime'][1]);
            
            if ($_SESSION['ViewOvertime'][3] != "")
              $row = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = '.$_SESSION['ViewOvertime'][3].''));
            switch ($_SESSION['ViewOvertime'][2])
            {
              case "":
                switch($_SESSION['ViewOvertime'][3])
                {
                  case "":
                    BuildContentHeader('Overtime - '.$dateRange, "", "", false);
                    $resultSet = ExecuteQuery('SELECT * FROM Staff WHERE Staff_IsEmployee = "1" ORDER BY Staff_First_Name, Staff_Last_Name');
                    echo '<DIV class="contentflow">
                            <P>This is the overtime listed.</P>
                            <BR /><BR />
                            <TABLE cellspacing="5" align="center" class="standard">
                              <TR>
                                <TD colspan="6" class="header">Overtime Details
                                </TD>
                              </TR>
                              <TR>
                                <TD class="subheader">Staff Name
                                </TD>
                                <TD colspan="5" class="subheader">Overtime Hours
                                </TD>
                              </TR>
                              <TR>
                                <TD class="subheader noborder">
                                </TD>
                                <TD class="subheader noborder">
                                </TD>
                                <TD class="subheader noborder">
                                </TD>
                                <TD colspan="3" class="subheader">Pre-Approved
                                </TD>
                              </TR>
                              <TR>
                                <TD class="subheader noborder">
                                </TD>
                                <TD class="subheader">Banked
                                </TD>
                                <TD class="subheader">Non-Banked
                                </TD>
                                <TD class="subheader">Approved
                                </TD>
                                <TD class="subheader">Used
                                </TD>
                                <TD class="subheader">Remaining
                                </TD>
                              </TR>';
                              $totalBanked = 0;
                              $totalNonBanked = 0;
                              $totalAllocated = 0;
                              $totalUsed = 0;
                              $totalRemaining = 0;
                              while ($row = MySQL_Fetch_Array($resultSet))
                              {
                                $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT SUM(OvertimeBank_Hours) AS Overtime FROM OvertimeBank WHERE OvertimeBank_Name = '.$row['Staff_Code'].' AND OvertimeBank_Start BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" AND OvertimeBank_IsApproved = "1"'));
                                $totalBanked += $rowTemp['Overtime'];
                                                                  
                                echo '<TR>
                                        <TD class="rowA">'.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].'
                                        </TD>
                                        <TD class="rowA center">'.SPrintF('%02.2f', $rowTemp['Overtime']).'
                                        </TD>';
                                
                                $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT SUM(Overtime_Hours) AS Overtime FROM Overtime WHERE Overtime_Name = '.$row['Staff_Code'].' AND Overtime_Start BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59"'));
                                $totalNonBanked += $rowTemp['Overtime'];
                                
                                  echo '<TD class="rowA center">'.SPrintF('%02.2f', $rowTemp['Overtime']).'
                                        </TD>';
                                        
                                $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT SUM(OvertimePreApp_Hours) AS OvertimeTotal, SUM(OvertimePreApp_OvertimeHoursUsed) AS OvertimeUsed, SUM(OvertimePreApp_Hours) AS OvertimeRemaining FROM OvertimePreApp WHERE OvertimePreApp_Name = '.$row['Staff_Code'].' AND OvertimePreApp_Start BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" AND OvertimePreApp_IsApproved = "1"'));
                                $totalAllocated += $rowTemp['OvertimeTotal'];
                                $totalUsed += $rowTemp['OvertimeUsed'];
                                
                                  echo '<TD class="rowA center">'.SPrintF('%02.2f', $rowTemp['OvertimeTotal']).'
                                        </TD>
                                        <TD class="rowA center">'.SPrintF('%02.2f', $rowTemp['OvertimeUsed']).'
                                        </TD>';
                                
                                $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT SUM(OvertimePreApp_Hours - OvertimePreApp_OvertimeHoursUsed) AS OvertimeRemaining FROM OvertimePreApp WHERE OvertimePreApp_Name = '.$row['Staff_Code'].' AND OvertimePreApp_OvertimeUsed = "0" AND OvertimePreApp_Start BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" AND OvertimePreApp_IsApproved = "1" AND OvertimePreApp_End > "'.Date('Y-m-d').' 23:59:59"'));
                                $totalRemaining += $rowTemp['OvertimeRemaining'];
                                  
                                  echo '<TD class="rowA center">'.SPrintF('%02.2f', $rowTemp['OvertimeRemaining']).'
                                        </TD>
                                      </TR>';
                              }
                        echo '<TR>
                                <TD class="rowB bold">Total
                                </TD>
                                <TD class="rowB center bold">'.SPrintF('%02.2f', $totalBanked).'
                                </TD>
                                <TD class="rowB center bold">'.SPrintF('%02.2f', $totalNonBanked).'
                                </TD>
                                <TD class="rowB center bold">'.SPrintF('%02.2f', $totalAllocated).'
                                </TD>
                                <TD class="rowB center bold">'.SPrintF('%02.2f', $totalUsed).'
                                </TD>
                                <TD class="rowB center bold">'.SPrintF('%02.2f', $totalRemaining).'
                                </TD>
                              </TR>
                            </TABLE>
                          </DIV>
                          <BR /><BR />';
                    break;
                  default:
                    $countFail = 0;
                    BuildContentHeader('Overtime - '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].' for '.$dateRange, "", "", false);
                    $resultSet = ExecuteQuery('SELECT OvertimeBank.*, Project_Description FROM OvertimeBank, Project WHERE OvertimeBank_Name = "'.$_SESSION['ViewOvertime'][3].'" AND OvertimeBank_Project = Project_Code AND OvertimeBank_Start BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" AND OvertimeBank_IsApproved = "1" ORDER BY OvertimeBank_Start ASC');
                    if (MySQL_Num_Rows($resultSet) > 0)
                    {   
                      echo '<DIV class="contentflow">
                              <P>This is the banked overtime listed.</P>
                              <BR /><BR />
                              <TABLE cellspacing="5" align="center" class="long">
                                <TR>
                                  <TD colspan="5" class="header">Banked Overtime Details
                                  </TD>
                                </TR>
                                <TR>
                                  <TD class="subheader">Period
                                  </TD>
                                  <TD class="subheader">Hours
                                  </TD>
                                  <TD class="subheader">Project
                                  </TD>
                                  <TD class="subheader">Authorised By
                                  </TD>
                                  <TD class="subheader">Date Logged
                                  </TD>
                                </TR>';
                                $total = 0;
                                while ($row = MySQL_Fetch_Array($resultSet))
                                {
                                  $total += $row['OvertimeBank_Hours'];
                                  
                                  $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = "'.$row['OvertimeBank_Authorised'].'"')); 
                                  echo '<TR>
                                          <TD class="rowA">'.GetTextualDateTimeFromDatabaseDateTime($row['OvertimeBank_Start']).' to '.GetTextualDateTimeFromDatabaseDateTime($row['OvertimeBank_End']).'
                                          </TD>
                                          <TD class="rowA center">'.SPrintF('%02.2f', $row['OvertimeBank_Hours']).'
                                          </TD>
                                          <TD class="rowA">'.$row['Project_Description'].'
                                          </TD>
                                          <TD class="rowA">'.$rowTemp['Staff_First_Name'].' '.$rowTemp['Staff_Last_Name'].'
                                          </TD>
                                          <TD class="rowA">'.GetTextualDateFromDatabaseDate($row['OvertimeBank_Date_Log']).'
                                          </TD>
                                        </TR>';
                                }
                          echo '<TR>
                                  <TD class="rowB bold">Total
                                  </TD>
                                  <TD class="rowB center bold">'.SPrintF('%02.2f', $total).'
                                  </TD>
                                  <TD class="rowB bold">
                                  </TD>
                                  <TD class="rowB bold">
                                  </TD>
                                  <TD class="rowB bold">
                                  </TD>
                                </TR>
                              </TABLE>
                            </DIV>
                            <BR /><BR />';  
                    } else
                      $countFail++;                        
                    
                    $resultSet = ExecuteQuery('SELECT Overtime.*, Project_Description FROM Overtime, Project WHERE Overtime_Name = '.$_SESSION['ViewOvertime'][3].' AND Overtime_Project = Project_Code AND Overtime_Start BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" ORDER BY Overtime_Start ASC');
                    if (MySQL_Num_Rows($resultSet) > 0)
                    {   
                      echo '<DIV class="contentflow">
                              <P>This is the non-banked overtime listed.</P>
                              <BR /><BR />
                              <TABLE cellspacing="5" align="center" class="long">
                                <TR>
                                  <TD colspan="5" class="header">Non-Banked Overtime Details
                                  </TD>
                                </TR>
                                <TR>
                                  <TD class="subheader">Period
                                  </TD>
                                  <TD class="subheader">Hours
                                  </TD>
                                  <TD class="subheader">Project
                                  </TD>
                                  <TD class="subheader">Authorised By
                                  </TD>
                                  <TD class="subheader">Date Logged
                                  </TD>
                                </TR>';
                                $total = 0;
                                while ($row = MySQL_Fetch_Array($resultSet))
                                {
                                  $total += $row['Overtime_Hours'];
                                  
                                  if ($row['Overtime_Authorised'])
                                    $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = '.$row['Overtime_Authorised'].'')); 
                                  else
                                    $rowTemp = array();
                                  
                                  echo '<TR>
                                          <TD class="rowA">'.GetTextualDateTimeFromDatabaseDateTime($row['Overtime_Start']).' to '.GetTextualDateTimeFromDatabaseDateTime($row['Overtime_End']).'
                                          </TD>
                                          <TD class="rowA center">'.SPrintF('%02.2f', $row['Overtime_Hours']).'
                                          </TD>
                                          <TD class="rowA">'.$row['Project_Description'].'
                                          </TD>
                                          <TD class="rowA">'.$rowTemp['Staff_First_Name'].' '.$rowTemp['Staff_Last_Name'].'
                                          </TD>
                                          <TD class="rowA">'.GetTextualDateFromDatabaseDate($row['Overtime_Date_Log']).'
                                          </TD>
                                        </TR>';
                                }
                          echo '<TR>
                                  <TD class="rowB bold">Total
                                  </TD>
                                  <TD class="rowB center bold">'.SPrintF('%02.2f', $total).'
                                  </TD>
                                  <TD class="rowB bold">
                                  </TD>
                                  <TD class="rowB bold">
                                  </TD>
                                  <TD class="rowB bold">
                                  </TD>
                                </TR>
                              </TABLE>
                            </DIV>
                            <BR /><BR />';
                    } else
                      $countFail++;  
                    
                    $resultSet = ExecuteQuery('SELECT OvertimePreApp.*, Project_Description FROM OvertimePreApp, Project WHERE OvertimePreApp_Name = '.$_SESSION['ViewOvertime'][3].' AND OvertimePreApp_Project = Project_Code AND OvertimePreApp_Start BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" AND OvertimePreApp_IsApproved = "1" ORDER BY OvertimePreApp_Start ASC');
                    if (MySQL_Num_Rows($resultSet) > 0)
                    {
                      echo '<DIV class="contentflow">
                              <P>This is the pre-approved overtime listed.</P>
                              <BR /><BR />
                              <TABLE cellspacing="5" align="center" class="long">
                                <TR>
                                  <TD colspan="8" class="header">Pre-Approved Overtime Details
                                  </TD>
                                </TR>
                                <TR>
                                  <TD class="subheader">Period
                                  </TD>
                                  <TD class="subheader">Hours
                                  </TD>
                                  <TD class="subheader">Hours Used
                                  </TD>
                                  <TD class="subheader">Hours Remaining
                                  </TD>
                                  <TD class="subheader">Project
                                  </TD>
                                  <TD class="subheader">Authorised By
                                  </TD>
                                  <TD class="subheader">Date Logged
                                  </TD>
					<TD class="subheader">Description
                                  </TD>
                                </TR>';
                                $totalAllocated = 0;
                                $totalUsed = 0;
                                $totalRemaining = 0;
                                while ($row = MySQL_Fetch_Array($resultSet))
                                {
                                  $totalAllocated += $row['OvertimePreApp_Hours'];
                                  $totalUsed += $row['OvertimePreApp_OvertimeHoursUsed'];
                                  
                                  $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = '.$row['OvertimePreApp_Authorised'].'')); 
                                  echo '<TR>
                                          <TD class="rowA">'.GetTextualDateFromDatabaseDate($row['OvertimePreApp_Start']).' to '.GetTextualDateFromDatabaseDate($row['OvertimePreApp_End']).'
                                          </TD>
                                          <TD class="rowA center">'.SPrintF('%02.2f', $row['OvertimePreApp_Hours']).'
                                          </TD>
                                          <TD class="rowA center">'.SPrintF('%02.2f', $row['OvertimePreApp_OvertimeHoursUsed']).'
                                          </TD>';
                                  
                                  if (($row['OvertimePreApp_OvertimeUsed'] == "1") || ($row['OvertimePreApp_End'] < Date('Y-m-d').' 23:59:59'))
                                    echo  '<TD class="rowA center">0.00
                                          </TD>';
                                  else
                                    {
                                      $totalRemaining += $row['OvertimePreApp_Hours'] - $row['OvertimePreApp_OvertimeHoursUsed'];                                    
                                      echo  '<TD class="rowA center">'.SPrintF('%02.2f', $row['OvertimePreApp_Hours'] - $row['OvertimePreApp_OvertimeHoursUsed']).'
                                            </TD>';
                                    }
                                  
                                  echo   '<TD class="rowA">'.$row['Project_Description'].'
                                          </TD>
                                          <TD class="rowA">'.$rowTemp['Staff_First_Name'].' '.$rowTemp['Staff_Last_Name'].'
                                          </TD>
                                          <TD class="rowA">'.GetTextualDateFromDatabaseDate($row['OvertimePreApp_Date_Log']).'
                                          </TD>
						<TD class="rowA">'.$row['OvertimePreApp_Description'].'
                                          </TD>
                                        </TR>';
                                }
                          echo '<TR>
                                  <TD class="rowB bold">Total
                                  </TD>
                                  <TD class="rowB center bold">'.SPrintF('%02.2f', $totalAllocated).'
                                  </TD>
                                  <TD class="rowB center bold">'.SPrintF('%02.2f', $totalUsed).'
                                  </TD>
                                  <TD class="rowB center bold">'.SPrintF('%02.2f', $totalRemaining).'
                                  </TD>
                                  <TD class="rowB bold">
                                  </TD>
                                  <TD class="rowB bold">
                                  </TD>
                                  <TD class="rowB bold">
                                  </TD>
                                  <TD class="rowB bold">
                                  </TD>
                                </TR>
                              </TABLE>
                            </DIV>';
                    } else
                      $countFail++; 
                    
                    if ($countFail == 3)
                      echo '<DIV class="contentflow">
                              <P>There is no overtime listed for the given date range.</P>
                           </DIV>';                          
                    break;
                }
                break;
              case 'Banked':                
                switch($_SESSION['ViewOvertime'][3])
                {
                  case "":
                    BuildContentHeader('Banked Overtime - '.$dateRange, "", "", false);
                    $resultSet = ExecuteQuery('SELECT * FROM Staff WHERE Staff_IsEmployee = "1" ORDER BY Staff_First_Name, Staff_Last_Name');
                    echo '<DIV class="contentflow">
                            <P>This is the banked overtime listed.</P>
                            <BR /><BR />
                            <TABLE cellspacing="5" align="center" class="short">
                              <TR>
                                <TD colspan="5" class="header">Banked Overtime Details
                                </TD>
                              </TR>
                              <TR>
                                <TD class="subheader">Staff Name
                                </TD>
                                <TD class="subheader">Hours
                                </TD>
                              </TR>';
                              $total = 0;
                              while ($row = MySQL_Fetch_Array($resultSet))
                              {
                                $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT SUM(OvertimeBank_Hours) AS Overtime FROM OvertimeBank WHERE OvertimeBank_Name = '.$row['Staff_Code'].' AND OvertimeBank_Start BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" AND OvertimeBank_IsApproved = "1"'));
                                $total += $rowTemp['Overtime'];
                                                                  
                                echo '<TR>
                                        <TD class="rowA">'.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].'
                                        </TD>
                                        <TD class="rowA center">'.SPrintF('%02.2f', $rowTemp['Overtime']).'
                                        </TD>
                                      </TR>';
                              }
                        echo '<TR>
                                <TD class="rowB bold">Total
                                </TD>
                                <TD class="rowB center bold">'.SPrintF('%02.2f', $total).'
                                </TD>
                              </TR>
                            </TABLE>
                          </DIV>';
                    break;
                  default:
                    BuildContentHeader('Banked Overtime - '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].' for '.$dateRange, "", "", false);
                    $resultSet = ExecuteQuery('SELECT OvertimeBank.*, Project_Description FROM OvertimeBank, Project WHERE OvertimeBank_Name = '.$_SESSION['ViewOvertime'][3].' AND OvertimeBank_Project = Project_Code AND OvertimeBank_Start BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" AND OvertimeBank_IsApproved = "1" ORDER BY OvertimeBank_Start ASC');
                    if (MySQL_Num_Rows($resultSet) > 0)
                    {   
                      echo '<DIV class="contentflow">
                              <P>This is the banked overtime listed.</P>
                              <BR /><BR />
                              <TABLE cellspacing="5" align="center" class="long">
                                <TR>
                                  <TD colspan="5" class="header">Banked Overtime Details
                                  </TD>
                                </TR>
                                <TR>
                                  <TD class="subheader">Period
                                  </TD>
                                  <TD class="subheader">Hours
                                  </TD>
                                  <TD class="subheader">Project
                                  </TD>
                                  <TD class="subheader">Authorised By
                                  </TD>
                                  <TD class="subheader">Date Logged
                                  </TD>
                                </TR>';
                                $total = 0;
                                while ($row = MySQL_Fetch_Array($resultSet))
                                {
                                  $total += $row['OvertimeBank_Hours'];
                                  
                                  $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = "'.$row['OvertimeBank_Authorised'].'"')); 
                                  echo '<TR>
                                          <TD class="rowA">'.GetTextualDateTimeFromDatabaseDateTime($row['OvertimeBank_Start']).' to '.GetTextualDateTimeFromDatabaseDateTime($row['OvertimeBank_End']).'
                                          </TD>
                                          <TD class="rowA center">'.SPrintF('%02.2f', $row['OvertimeBank_Hours']).'
                                          </TD>
                                          <TD class="rowA">'.$row['Project_Description'].'
                                          </TD>
                                          <TD class="rowA">'.$rowTemp['Staff_First_Name'].' '.$rowTemp['Staff_Last_Name'].'
                                          </TD>
                                          <TD class="rowA">'.GetTextualDateFromDatabaseDate($row['OvertimeBank_Date_Log']).'
                                          </TD>
                                        </TR>';
                                }
                          echo '<TR>
                                  <TD class="rowB bold">Total
                                  </TD>
                                  <TD class="rowB center bold">'.SPrintF('%02.2f', $total).'
                                  </TD>
                                  <TD class="rowB bold">
                                  </TD>
                                  <TD class="rowB bold">
                                  </TD>
                                  <TD class="rowB bold">
                                  </TD>
                                </TR>
                              </TABLE>
                            </DIV>';
                    } else
                      echo '<DIV class="contentflow">
                              <P>There is no banked overtime listed for the given date range.</P>
                           </DIV>';
                    break;
                }
                break;
              case 'Requested-Banked':                
                switch($_SESSION['ViewOvertime'][3])
                {
                  case "":
                    BuildContentHeader('Requested-Banked Overtime - '.$dateRange, "", "", false);
                    $resultSet = ExecuteQuery('SELECT * FROM Staff WHERE Staff_IsEmployee = "1" ORDER BY Staff_First_Name, Staff_Last_Name');
                    echo '<DIV class="contentflow">
                            <P>This is the requested-banked overtime listed.</P>
                            <BR /><BR />
                            <TABLE cellspacing="5" align="center" class="short">
                              <TR>
                                <TD colspan="5" class="header">Requested-Banked Overtime Details
                                </TD>
                              </TR>
                              <TR>
                                <TD class="subheader">Staff Name
                                </TD>
                                <TD class="subheader">Hours
                                </TD>
                              </TR>';
                              $total = 0;
                              while ($row = MySQL_Fetch_Array($resultSet))
                              {
                                $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT SUM(OvertimeBank_Hours) AS Overtime FROM OvertimeBank WHERE OvertimeBank_Name = '.$row['Staff_Code'].' AND OvertimeBank_Start BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" AND OvertimeBank_IsApproved = "0"'));
                                $total += $rowTemp['Overtime'];
                                                                  
                                echo '<TR>
                                        <TD class="rowA">'.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].'
                                        </TD>
                                        <TD class="rowA center">'.SPrintF('%02.2f', $rowTemp['Overtime']).'
                                        </TD>
                                      </TR>';
                              }
                        echo '<TR>
                                <TD class="rowB bold">Total
                                </TD>
                                <TD class="rowB center bold">'.SPrintF('%02.2f', $total).'
                                </TD>
                              </TR>
                            </TABLE>
                          </DIV>';
                    break;
                  default:
                    BuildContentHeader('Requested-Banked Overtime - '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].' for '.$dateRange, "", "", false);
                    $resultSet = ExecuteQuery('SELECT OvertimeBank.*, Project_Description FROM OvertimeBank, Project WHERE OvertimeBank_Name = '.$_SESSION['ViewOvertime'][3].' AND OvertimeBank_Project = Project_Code AND OvertimeBank_Start BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" AND OvertimeBank_IsApproved = "0" ORDER BY OvertimeBank_Start ASC');
                    if (MySQL_Num_Rows($resultSet) > 0)
                    {   
                      echo '<DIV class="contentflow">
                              <P>This is the requested-banked overtime listed.</P>
                              <BR /><BR />
                              <TABLE cellspacing="5" align="center" class="long">
                                <TR>
                                  <TD colspan="5" class="header">Requested-Banked Overtime Details
                                  </TD>
                                </TR>
                                <TR>
                                  <TD class="subheader">Period
                                  </TD>
                                  <TD class="subheader">Hours
                                  </TD>
                                  <TD class="subheader">Project
                                  </TD>
                                  <TD class="subheader">Authorised By
                                  </TD>
                                  <TD class="subheader">Date Logged
                                  </TD>
                                </TR>';
                                $total = 0;
                                while ($row = MySQL_Fetch_Array($resultSet))
                                {
                                  $total += $row['OvertimeBank_Hours'];
                                  
                                  $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = "'.$row['OvertimeBank_Authorised'].'"')); 
                                  echo '<TR>
                                          <TD class="rowA">'.GetTextualDateTimeFromDatabaseDateTime($row['OvertimeBank_Start']).' to '.GetTextualDateTimeFromDatabaseDateTime($row['OvertimeBank_End']).'
                                          </TD>
                                          <TD class="rowA center">'.SPrintF('%02.2f', $row['OvertimeBank_Hours']).'
                                          </TD>
                                          <TD class="rowA">'.$row['Project_Description'].'
                                          </TD>
                                          <TD class="rowA">'.$rowTemp['Staff_First_Name'].' '.$rowTemp['Staff_Last_Name'].'
                                          </TD>
                                          <TD class="rowA">'.GetTextualDateFromDatabaseDate($row['OvertimeBank_Date_Log']).'
                                          </TD>
                                        </TR>';
                                }
                          echo '<TR>
                                  <TD class="rowB bold">Total
                                  </TD>
                                  <TD class="rowB center bold">'.SPrintF('%02.2f', $total).'
                                  </TD>
                                  <TD class="rowB bold">
                                  </TD>
                                  <TD class="rowB bold">
                                  </TD>
                                  <TD class="rowB bold">
                                  </TD>
                                </TR>
                              </TABLE>
                            </DIV>';
                    } else
                      echo '<DIV class="contentflow">
                              <P>There is no requested-banked overtime listed for the given date range.</P>
                           </DIV>';
                    break;
                }
                break;
              case 'Non-Banked':                
                switch($_SESSION['ViewOvertime'][3])
                {
                  case "":
                    BuildContentHeader('Non-Banked Overtime - '.$dateRange, "", "", false);
                    $resultSet = ExecuteQuery('SELECT * FROM Staff WHERE Staff_IsEmployee = "1" ORDER BY Staff_First_Name, Staff_Last_Name');
                    echo '<DIV class="contentflow">
                            <P>This is the non-banked overtime listed.</P>
                            <BR /><BR />
                            <TABLE cellspacing="5" align="center" class="short">
                              <TR>
                                <TD colspan="5" class="header">Non-Banked Overtime Details
                                </TD>
                              </TR>
                              <TR>
                                <TD class="subheader">Staff Name
                                </TD>
                                <TD class="subheader">Hours
                                </TD>
                              </TR>';
                              $total = 0;
                              while ($row = MySQL_Fetch_Array($resultSet))
                              {
                                $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT SUM(Overtime_Hours) AS Overtime FROM Overtime WHERE Overtime_Name = '.$row['Staff_Code'].' AND Overtime_Start BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59"'));
                                $total += $rowTemp['Overtime'];
                                                                  
                                echo '<TR>
                                        <TD class="rowA">'.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].'
                                        </TD>
                                        <TD class="rowA center">'.SPrintF('%02.2f', $rowTemp['Overtime']).'
                                        </TD>
                                      </TR>';
                              }
                        echo '<TR>
                                <TD class="rowB bold">Total
                                </TD>
                                <TD class="rowB center bold">'.SPrintF('%02.2f', $total).'
                                </TD>
                              </TR>
                            </TABLE>
                          </DIV>';
                    break;
                  default:
                    BuildContentHeader('Non-Banked Overtime - '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].' for '.$dateRange, "", "", false);
                    $resultSet = ExecuteQuery('SELECT Overtime.*, Project_Description FROM Overtime, Project WHERE Overtime_Name = '.$_SESSION['ViewOvertime'][3].' AND Overtime_Project = Project_Code AND Overtime_Start BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" ORDER BY Overtime_Start ASC');
                    if (MySQL_Num_Rows($resultSet) > 0)
                    {   
                      echo '<DIV class="contentflow">
                              <P>This is the non-banked overtime listed.</P>
                              <BR /><BR />
                              <TABLE cellspacing="5" align="center" class="long">
                                <TR>
                                  <TD colspan="5" class="header">Non-Banked Overtime Details
                                  </TD>
                                </TR>
                                <TR>
                                  <TD class="subheader">Period
                                  </TD>
                                  <TD class="subheader">Hours
                                  </TD>
                                  <TD class="subheader">Project
                                  </TD>
                                  <TD class="subheader">Authorised By
                                  </TD>
                                  <TD class="subheader">Date Logged
                                  </TD>
                                </TR>';
                                $total = 0;
                                while ($row = MySQL_Fetch_Array($resultSet))
                                {
                                  $total += $row['Overtime_Hours'];
                                  
                                  if ($row['Overtime_Authorised'] != "")
                                    $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = '.$row['Overtime_Authorised'].''));
                                  else
                                    $rowTemp = array();
                                  echo '<TR>
                                          <TD class="rowA">'.GetTextualDateTimeFromDatabaseDateTime($row['Overtime_Start']).' to '.GetTextualDateTimeFromDatabaseDateTime($row['Overtime_End']).'
                                          </TD>
                                          <TD class="rowA center">'.SPrintF('%02.2f', $row['Overtime_Hours']).'
                                          </TD>
                                          <TD class="rowA">'.$row['Project_Description'].'
                                          </TD>
                                          <TD class="rowA">'.$rowTemp['Staff_First_Name'].' '.$rowTemp['Staff_Last_Name'].'
                                          </TD>
                                          <TD class="rowA">'.GetTextualDateFromDatabaseDate($row['Overtime_Date_Log']).'
                                          </TD>
                                        </TR>';
                                }
                          echo '<TR>
                                  <TD class="rowB bold">Total
                                  </TD>
                                  <TD class="rowB center bold">'.SPrintF('%02.2f', $total).'
                                  </TD>
                                  <TD class="rowB bold">
                                  </TD>
                                  <TD class="rowB bold">
                                  </TD>
                                  <TD class="rowB bold">
                                  </TD>
                                </TR>
                              </TABLE>
                            </DIV>';
                    } else
                      echo '<DIV class="contentflow">
                              <P>There is no non-banked overtime listed for the given date range.</P>
                           </DIV>';
                    break;
                }
                break;
              case 'Pre-Approved':                
                switch($_SESSION['ViewOvertime'][3])
                {
                  case "":
                    BuildContentHeader('Pre-Approved Overtime - '.$dateRange, "", "", false);
                    $resultSet = ExecuteQuery('SELECT * FROM Staff WHERE Staff_IsEmployee = "1" ORDER BY Staff_First_Name, Staff_Last_Name');
                    echo '<DIV class="contentflow">
                            <P>This is the pre-approved overtime listed.</P>
                            <BR /><BR />
                            <TABLE cellspacing="5" align="center" class="standard">
                              <TR>
                                <TD colspan="5" class="header">Pre-Approved Overtime Details
                                </TD>
                              </TR>
                              <TR>
                                <TD class="subheader">Staff Name
                                </TD>
                                <TD class="subheader">Hours Approved
                                </TD>
                                <TD class="subheader">Hours Used
                                </TD>
                                <TD class="subheader">Hours Remaining
                                </TD>
                              </TR>';
                              $totalAllocated = 0;
                              $totalUsed = 0;
                              $totalRemaining = 0;
                              while ($row = MySQL_Fetch_Array($resultSet))
                              {
                                $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT SUM(OvertimePreApp_Hours) AS OvertimeTotal, SUM(OvertimePreApp_OvertimeHoursUsed) AS OvertimeUsed, SUM(OvertimePreApp_Hours) AS OvertimeRemaining FROM OvertimePreApp WHERE OvertimePreApp_Name = '.$row['Staff_Code'].' AND OvertimePreApp_Start BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" AND OvertimePreApp_IsApproved = "1"'));
                                $totalAllocated += $rowTemp['OvertimeTotal'];
                                $totalUsed += $rowTemp['OvertimeUsed'];
                                                                  
                                echo '<TR>
                                        <TD class="rowA">'.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].'
                                        </TD>
                                        <TD class="rowA center">'.SPrintF('%02.2f', $rowTemp['OvertimeTotal']).'
                                        </TD>
                                        <TD class="rowA center">'.SPrintF('%02.2f', $rowTemp['OvertimeUsed']).'
                                        </TD>';
                                                                                
                                $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT SUM(OvertimePreApp_Hours - OvertimePreApp_OvertimeHoursUsed) AS OvertimeRemaining FROM OvertimePreApp WHERE OvertimePreApp_Name = '.$row['Staff_Code'].' AND OvertimePreApp_OvertimeUsed = "0" AND OvertimePreApp_Start BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" AND OvertimePreApp_IsApproved = "1"'));
                                $totalRemaining += $rowTemp['OvertimeRemaining'];
                                  
                                  echo '<TD class="rowA center">'.SPrintF('%02.2f', $rowTemp['OvertimeRemaining']).'
                                        </TD>
                                      </TR>';
                              }
                        echo '<TR>
                                <TD class="rowB bold">Total
                                </TD>
                                <TD class="rowB center bold">'.SPrintF('%02.2f', $totalAllocated).'
                                </TD>
                                <TD class="rowB center bold">'.SPrintF('%02.2f', $totalUsed).'
                                </TD>
                                <TD class="rowB center bold">'.SPrintF('%02.2f', $totalRemaining).'
                                </TD>
                              </TR>
                            </TABLE>
                          </DIV>';
                    break;
                  default:
                    BuildContentHeader('Pre-Approved Overtime - '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].' for '.$dateRange, "", "", false);
                    $resultSet = ExecuteQuery('SELECT OvertimePreApp.*, Project_Description FROM OvertimePreApp, Project WHERE OvertimePreApp_Name = '.$_SESSION['ViewOvertime'][3].' AND OvertimePreApp_Project = Project_Code AND OvertimePreApp_Start BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" AND OvertimePreApp_IsApproved = "1" ORDER BY OvertimePreApp_Start ASC');
                    if (MySQL_Num_Rows($resultSet) > 0)
                    {   
                      echo '<DIV class="contentflow">
                              <P>This is the pre-approved overtime listed.</P>
                              <BR /><BR />
                              <TABLE cellspacing="5" align="center" class="long">
                                <TR>
                                  <TD colspan="8" class="header">Pre-Approved Overtime Details
                                  </TD>
                                </TR>
                                <TR>
                                  <TD class="subheader">Period
                                  </TD>
                                  <TD class="subheader">Hours
                                  </TD>
                                  <TD class="subheader">Hours Used
                                  </TD>
                                  <TD class="subheader">Hours Remaining
                                  </TD>
                                  <TD class="subheader">Project
                                  </TD>
                                  <TD class="subheader">Authorised By
                                  </TD>
                                  <TD class="subheader">Date Logged
                                  </TD>
					<TD class="subheader">Description
                                  </TD>
                                </TR>';
                                $totalAllocated = 0;
                                $totalUsed = 0;
                                $totalRemaining = 0;
                                while ($row = MySQL_Fetch_Array($resultSet))
                                {
                                  $totalAllocated += $row['OvertimePreApp_Hours'];
                                  $totalUsed += $row['OvertimePreApp_OvertimeHoursUsed'];
                                  
                                  $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT Staff_First_Name, Staff_Last_Name FROM Staff WHERE Staff_Code = "'.$row['OvertimePreApp_Authorised'].'"')); 
                                  echo '<TR>
                                          <TD class="rowA">'.GetTextualDateFromDatabaseDate($row['OvertimePreApp_Start']).' to '.GetTextualDateFromDatabaseDate($row['OvertimePreApp_End']).'
                                          </TD>
                                          <TD class="rowA center">'.SPrintF('%02.2f', $row['OvertimePreApp_Hours']).'
                                          </TD>
                                          <TD class="rowA center">'.SPrintF('%02.2f', $row['OvertimePreApp_OvertimeHoursUsed']).'
                                          </TD>';
                                          
                                  if (($row['OvertimePreApp_OvertimeUsed'] == "1") || ($row['OvertimePreApp_End'] < Date('Y-m-d').' 23:59:59'))
                                    echo  '<TD class="rowA center">0.00
                                          </TD>';
                                  else
                                    {
                                      $totalRemaining += $row['OvertimePreApp_Hours'] - $row['OvertimePreApp_OvertimeHoursUsed'];                                    
                                      echo  '<TD class="rowA center">'.SPrintF('%02.2f', $row['OvertimePreApp_Hours'] - $row['OvertimePreApp_OvertimeHoursUsed']).'
                                            </TD>';
                                    }
                                          
                                  echo   '<TD class="rowA">'.$row['Project_Description'].'
                                          </TD>
                                          <TD class="rowA">'.$rowTemp['Staff_First_Name'].' '.$rowTemp['Staff_Last_Name'].'
                                          </TD>
                                          <TD class="rowA">'.GetTextualDateFromDatabaseDate($row['OvertimePreApp_Date_Log']).'
                                          </TD>
						<TD class="rowA">'.$row['OvertimePreApp_Description'].'
                                          </TD>
                                        </TR>';
                                }
                          echo '<TR>
                                  <TD class="rowB bold">Total
                                  </TD>
                                  <TD class="rowB center bold">'.SPrintF('%02.2f', $totalAllocated).'
                                  </TD>
                                  <TD class="rowB center bold">'.SPrintF('%02.2f', $totalUsed).'
                                  </TD>
                                  <TD class="rowB center bold">'.SPrintF('%02.2f', $totalRemaining).'
                                  </TD>
                                  <TD class="rowB bold">
                                  </TD>
                                  <TD class="rowB bold">
                                  </TD>
                                  <TD class="rowB bold">
                                  </TD>
                                  <TD class="rowB bold">
                                  </TD>
                                </TR>
                              </TABLE>
                            </DIV>';
                    } else
                      echo '<DIV class="contentflow">
                              <P>There is no pre-approved overtime listed for the given date range.</P>
                           </DIV>';
                    break;
                }
                break;
              case 'Requested-Normal':                
                switch($_SESSION['ViewOvertime'][3])
                {
                  case "":
                    BuildContentHeader('Requested-Normal Overtime - '.$dateRange, "", "", false);
                    $resultSet = ExecuteQuery('SELECT * FROM Staff WHERE Staff_IsEmployee = "1" ORDER BY Staff_First_Name, Staff_Last_Name');
                    echo '<DIV class="contentflow">
                            <P>This is the requested-normal overtime listed.</P>
                            <BR /><BR />
                            <TABLE cellspacing="5" align="center" class="standard">
                              <TR>
                                <TD colspan="5" class="header">Requested-Normal Overtime Details
                                </TD>
                              </TR>
                              <TR>
                                <TD class="subheader">Staff Name
                                </TD>
                                <TD class="subheader">Hours Approved
                                </TD>
                                <TD class="subheader">Hours Used
                                </TD>
                                <TD class="subheader">Hours Remaining
                                </TD>
                              </TR>';
                              $totalAllocated = 0;
                              $totalUsed = 0;
                              $totalRemaining = 0;
                              while ($row = MySQL_Fetch_Array($resultSet))
                              {
                                $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT SUM(OvertimePreApp_Hours) AS OvertimeTotal, SUM(OvertimePreApp_OvertimeHoursUsed) AS OvertimeUsed, SUM(OvertimePreApp_Hours) AS OvertimeRemaining FROM OvertimePreApp WHERE OvertimePreApp_Name = '.$row['Staff_Code'].' AND OvertimePreApp_Start BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" AND OvertimePreApp_IsApproved = "0"'));
                                $totalAllocated += $rowTemp['OvertimeTotal'];
                                $totalUsed += $rowTemp['OvertimeUsed'];
                                                                  
                                echo '<TR>
                                        <TD class="rowA">'.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].'
                                        </TD>
                                        <TD class="rowA center">'.SPrintF('%02.2f', $rowTemp['OvertimeTotal']).'
                                        </TD>
                                        <TD class="rowA center">'.SPrintF('%02.2f', $rowTemp['OvertimeUsed']).'
                                        </TD>';
                                                                                
                                $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT SUM(OvertimePreApp_Hours - OvertimePreApp_OvertimeHoursUsed) AS OvertimeRemaining FROM OvertimePreApp WHERE OvertimePreApp_Name = '.$row['Staff_Code'].' AND OvertimePreApp_OvertimeUsed = "0" AND OvertimePreApp_Start BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" AND OvertimePreApp_IsApproved = "1"'));
                                $totalRemaining += $rowTemp['OvertimeRemaining'];
                                  
                                  echo '<TD class="rowA center">'.SPrintF('%02.2f', $rowTemp['OvertimeRemaining']).'
                                        </TD>
                                      </TR>';
                              }
                        echo '<TR>
                                <TD class="rowB bold">Total
                                </TD>
                                <TD class="rowB center bold">'.SPrintF('%02.2f', $totalAllocated).'
                                </TD>
                                <TD class="rowB center bold">'.SPrintF('%02.2f', $totalUsed).'
                                </TD>
                                <TD class="rowB center bold">'.SPrintF('%02.2f', $totalRemaining).'
                                </TD>
                              </TR>
                            </TABLE>
                          </DIV>';
                    break;
                  default:
                    BuildContentHeader('Requested-Normal Overtime - '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].' for '.$dateRange, "", "", false);
                    $resultSet = ExecuteQuery('SELECT OvertimePreApp.*, Project_Description FROM OvertimePreApp, Project WHERE OvertimePreApp_Name = '.$_SESSION['ViewOvertime'][3].' AND OvertimePreApp_Project = Project_Code AND OvertimePreApp_Start BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" AND OvertimePreApp_IsApproved = "0" ORDER BY OvertimePreApp_Start ASC');
                    if (MySQL_Num_Rows($resultSet) > 0)
                    {   
                      echo '<DIV class="contentflow">
                              <P>This is the requested-normal overtime listed.</P>
                              <BR /><BR />
                              <TABLE cellspacing="5" align="center" class="long">
                                <TR>
                                  <TD colspan="8" class="header">Requested-Normal Overtime Details
                                  </TD>
                                </TR>
                                <TR>
                                  <TD class="subheader">Period
                                  </TD>
                                  <TD class="subheader">Hours
                                  </TD>
                                  <TD class="subheader">Hours Used
                                  </TD>
                                  <TD class="subheader">Hours Remaining
                                  </TD>
                                  <TD class="subheader">Project
                                  </TD>
                                  <TD class="subheader">Authorised By
                                  </TD>
                                  <TD class="subheader">Date Logged
                                  </TD>
					<TD class="subheader">Description
                                  </TD>
                                </TR>';
                                $totalAllocated = 0;
                                $totalUsed = 0;
                                $totalRemaining = 0;
                                while ($row = MySQL_Fetch_Array($resultSet))
                                {
                                  $totalAllocated += $row['OvertimePreApp_Hours'];
                                  $totalUsed += $row['OvertimePreApp_OvertimeHoursUsed'];
                                  
                                  $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT Staff_First_Name, Staff_Last_Name FROM Staff WHERE Staff_Code = "'.$row['OvertimePreApp_Authorised'].'"')); 
                                  echo '<TR>
                                          <TD class="rowA">'.GetTextualDateFromDatabaseDate($row['OvertimePreApp_Start']).' to '.GetTextualDateFromDatabaseDate($row['OvertimePreApp_End']).'
                                          </TD>
                                          <TD class="rowA center">'.SPrintF('%02.2f', $row['OvertimePreApp_Hours']).'
                                          </TD>
                                          <TD class="rowA center">'.SPrintF('%02.2f', $row['OvertimePreApp_OvertimeHoursUsed']).'
                                          </TD>';
                                          
                                  if (($row['OvertimePreApp_OvertimeUsed'] == "1") || ($row['OvertimePreApp_End'] < Date('Y-m-d').' 23:59:59'))
                                    echo  '<TD class="rowA center">0.00
                                          </TD>';
                                  else
                                    {
                                      $totalRemaining += $row['OvertimePreApp_Hours'] - $row['OvertimePreApp_OvertimeHoursUsed'];                                    
                                      echo  '<TD class="rowA center">'.SPrintF('%02.2f', $row['OvertimePreApp_Hours'] - $row['OvertimePreApp_OvertimeHoursUsed']).'
                                            </TD>';
                                    }
                                          
                                  echo   '<TD class="rowA">'.$row['Project_Description'].'
                                          </TD>
                                          <TD class="rowA">'.$rowTemp['Staff_First_Name'].' '.$rowTemp['Staff_Last_Name'].'
                                          </TD>
                                          <TD class="rowA">'.GetTextualDateFromDatabaseDate($row['OvertimePreApp_Date_Log']).'
                                          </TD>
						<TD class="rowA">'.$row['OvertimePreApp_Description'].'
                                          </TD>
                                        </TR>';
                                }
                          echo '<TR>
                                  <TD class="rowB bold">Total
                                  </TD>
                                  <TD class="rowB center bold">'.SPrintF('%02.2f', $totalAllocated).'
                                  </TD>
                                  <TD class="rowB center bold">'.SPrintF('%02.2f', $totalUsed).'
                                  </TD>
                                  <TD class="rowB center bold">'.SPrintF('%02.2f', $totalRemaining).'
                                  </TD>
                                  <TD class="rowB bold">
                                  </TD>
                                  <TD class="rowB bold">
                                  </TD>
                                  <TD class="rowB bold">
                                  </TD>
                                  <TD class="rowB bold">
                                  </TD>
                                </TR>
                              </TABLE>
                            </DIV>';
                    } else
                      echo '<DIV class="contentflow">
                              <P>There is no requested-normal overtime listed for the given date range.</P>
                           </DIV>';
                    break;
                }
                break;
              default:
                echo 'Kyaaaaaaaaaah! *chop* Invalid report type! Error! Error!';   
                break;
            }
            Session_Unregister('ViewOvertime');
          } else
          if (isset($_SESSION['ViewOvertimeStatistics']))
          {
            Session_Unregister('Graphs'); //This must be here or before otherwise there is a HUGE problem with caching and the session variables.  
            
            $startDate = GetDatabaseDateFromSessionDate($_SESSION['ViewOvertimeStatistics'][0]);
            $endDate = GetDatabaseDateFromSessionDate($_SESSION['ViewOvertimeStatistics'][1]);
            if ($_SESSION['ViewOvertimeStatistics'][0] == $_SESSION['ViewOvertimeStatistics'][1])
              $dateRange = GetTextualDateFromSessionDate($_SESSION['ViewOvertimeStatistics'][0]);
            else
              $dateRange = GetTextualDateFromSessionDate($_SESSION['ViewOvertimeStatistics'][0]).' to '.GetTextualDateFromSessionDate($_SESSION['ViewOvertimeStatistics'][1]);
            
            switch ($_SESSION['ViewOvertimeStatistics'][2])
            {
              case "":
                BuildContentHeader('Overtime Statistics for All Staff - '.$dateRange, "", "", false);
                $queries = array('SELECT DATE_FORMAT(OvertimeBank_Start, "%y/%m") as Axis, SUM(OvertimeBank_Hours) FROM OvertimeBank WHERE OvertimeBank_Hours > 0 AND OvertimeBank_IsApproved = "1" AND OvertimeBank_Start BETWEEN "'.$startDate.'" AND "'.$endDate.'" GROUP BY Axis ASC',
                                 'SELECT DATE_FORMAT(Overtime_Start, "%y/%m") as Axis, SUM(Overtime_Hours) FROM Overtime WHERE Overtime_Start BETWEEN "'.$startDate.'" AND "'.$endDate.'" GROUP BY Axis ASC');
                break;
              default:
                $row = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = '.$_SESSION['ViewOvertimeStatistics'][2].''));
                BuildContentHeader('Overtime Statistics for '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].' - '.$dateRange, "", "", false);
                $queries = array('SELECT DATE_FORMAT(OvertimeBank_Start, "%y/%m") as Axis, SUM(OvertimeBank_Hours) FROM OvertimeBank WHERE OvertimeBank_Name = '.$_SESSION['ViewOvertimeStatistics'][2].' AND OvertimeBank_Hours > 0 AND OvertimeBank_IsApproved = "1" AND OvertimeBank_Start BETWEEN "'.$startDate.'" AND "'.$endDate.'" GROUP BY Axis ASC',
                                 'SELECT DATE_FORMAT(Overtime_Start, "%y/%m") as Axis, SUM(Overtime_Hours) FROM Overtime WHERE Overtime_Name = '.$_SESSION['ViewOvertimeStatistics'][2].' AND Overtime_Start BETWEEN "'.$startDate.'" AND "'.$endDate.'" GROUP BY Axis ASC');
                break;
            }
            $display = false;
            foreach ($queries as $query)
            {
              if (MySQL_Num_Rows(ExecuteQuery($query)))
              {
                $display = true;
                break;
              }
            }
            if ($display)
            {
              BuildLineGraph($queries, 600, 400, 'Banked Overtime VS Paid Overtime', 'Number of Hours', 'Date', $startDate, $endDate,
                            array('Banked Overtime', 'Paid Overtime'),
                            array('#FF511C', '#0C61C9'));
              echo '<DIV class="contentflow">
                      <P>These are the overtime statistics.</P>
                      <BR /><BR />
                      <COMMENT>
                        <IMG src="Scripts/Graphing.php?Graph=0&Random='.Rand().'" />
                      </COMMENT>
                      <!--[if lte IE 6]>
                        <IFRAME src="Scripts/Graphing.php?Graph=0&Random='.Rand().'" width="900" height="600" frameborder="0"></IFRAME>
                      <![endif]-->
                      <!--[if gt IE 6]>
                        <IMG src="Scripts/Graphing.php?Graph=0&Random='.Rand().'" />
                      <![endif]-->
                    </DIV>
                    <BR /><BR />';
            }
            else
              echo '<DIV class="contentflow">
                      <P>There is no overtime listed for the given date range.</P>
                   </DIV>';
            Session_Unregister('ViewOvertimeStatistics');
          } else
          {
            BuildContentHeader('Maintenance - Banked Overtime', "", "", true);
            echo '<DIV class="contentflow">
                    <P>Banked overtime <font color = green>(you want to take time off) </font>use can be requested by using the form below.';
                    if ($_SESSION['cAuth'] & 64)
                      echo ' It can also be approved and transferred.';
                    else
                      if ($_SESSION['cAuth'] & 8)
                      echo ' It can also be approved.';
              echo '</P>
                    <BR /><BR />
                    <DIV align="center">
                     <img src="Images/banked_overtime2.png" alt="banked overtime" style="width:140px;height:130px;margin-right:15px;float:right;border:none">
                    <TABLE cellspacing="5"  class="standard">
                      <FORM method="post" action="Handlers/Overtime_Handler.php">
                        <INPUT name="Type" type="hidden" value="MaintainBankedOvertime">';
                        if ($_SESSION['cAuth'] & 8)
                        {
                          echo '<TR>
                                  <TD colspan="4" class="header">Approve
                                  </TD>
                                </TR>
                                <TR>
                                  <TD colspan="4">To approve or deny the use of banked overtime, specify the particulars, click Approve and confirm when prompted.
                                  </TD>
                                </TR>
                                <TR>
                                  <TD class="short">Request:
                                    <SPAN class="note">*
                                    </SPAN>
                                  </TD>
                                  <TD>';
                                    BuildOvertimeApproveSelector('5', 'ApproveOvertime', 'standard', $_SESSION['cUID']);
                            echo '</TD>
                                  <TD colspan="2" class="right">
                                    <INPUT tabindex="6" name="Submit" type="submit" class="button" value="Approve" />                   
                                  </TD>
                                </TR>';
                        }
              if($_SESSION['cAuth'] & 64){
                  echo '<TR>
                          <TD colspan="4" class="header">Request
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="4">To request the use of banked overtime, click Request and complete the form that is displayed.
                          </TD>
                          </TR>
                          <TR> <TD class="short">Request:
                                    <SPAN class="note">*
                                    </SPAN>
                                  </TD><TD>';
                  BuildStaffSelector(10, 'RequestOvertimeBankedOvertime', 'standard', $_SESSION['cUID']);
                  echo'
                          </TD><TD colspan="2" class="right">
                            <INPUT tabindex="10" name="Submit" type="submit" class="button" value="Request" />                   
                          </TD>
                        </TR>';
              }
              else{
                  echo '<TR>
                          <TD colspan="4" class="header">Request
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="3">To request the use of banked overtime, click Request and complete the form that is displayed.
                          </TD>
                          <TD class="right">
                            <INPUT tabindex="11" name="Submit" type="submit" class="button" value="Request" />                   
                          </TD>
                        </TR>';
              }
                        if ($_SESSION['cAuth'] & 64)
                        {
                          echo '<TR>
                                  <TD colspan="4" class="header">Transfer
                                  </TD>
                                </TR>
                                <TR>
                                  <TD colspan="4">To transfer banked overtime, click Transfer and complete the form that is displayed.
                                  </TD>
                                </TR>
                                <TR>
                                  <TD>Staff Name:
                                    <SPAN class="note">*
                                    </SPAN>
                                  </TD>
                                  <TD>';
                                    BuildStaffSelector(12, 'TransferStaff', 'standard', $_SESSION['cUID']);
                            echo '</TD>
                                  <TD colspan="2" class="right">
                                    <INPUT tabindex="13" name="Submit" type="submit" class="button" value="Transfer" />                   
                                  </TD>
                                </TR>
                                <TR>
                                  <TD colspan="4" class="header">Transfer Excess
                                  </TD>
                                </TR>
                                <TR>
                                  <TD colspan="3">To transfer excess banked overtime, click Excess and confirm when prompted.
                                  </TD>
                                  <TD class="right">
                                    <INPUT tabindex="14" name="Submit" type="submit" class="button" value="Excess" />                   
                                  </TD>
                                </TR>';
                        }
                echo '</FORM>
                    </TABLE>                  
                   
                    </DIV>
                  </DIV>';
          
            BuildContentHeader('Maintenance - Normal Overtime', "", "", true);                     
            echo '<DIV class="contentflow">
                    <P>Overtime<font color = green> (you are working extra hours) </font> can be recorded and requested by using the form below.';
                    if (($_SESSION['cAuth'] & 8) || ($_SESSION['cUID'] == 244))
                      echo ' It can also be approved.';
              echo ' Existing overtime can be edited.</P>
                    <BR /><BR />
                    <Div align= "center">
                   <img src="Images/overtime2.png" alt="Normal overtime" style="width:140px;height:140px;margin-right:15px;float:right;border:none;">
                    <TABLE cellspacing="5"  class="standard">
                      <FORM method="post" action="Handlers/Overtime_Handler.php">
                        <INPUT name="Type" type="hidden" value="MaintainNormalOvertime">';
//                        <TR>
//                          <TD colspan="4" class="header">Add
//                          </TD>
//                        </TR>
//                        <TR>
//                          <TD colspan="4">To record overtime, specify the particulars, click Add and complete the form that is displayed.
//                          </TD>
//                        </TR>
//                        <TR>
//                          <TD class="short">Project:
//                            <SPAN class="note">*
//                            </SPAN>
//                          </TD>
//                          <TD>';
//                            BuildOvertimePreApprovedSelector(1, 'AddOvertime', 'long');
//                    echo '</TD>
//                          <TD colspan="2" class="right">
//                            <INPUT tabindex="2" name="Submit" type="submit" class="button" value="Add" />                   
//                          </TD>
//                        </TR>';
                        if (($_SESSION['cAuth'] & 8) || ($_SESSION['cUID'] == 244))
                        {
                          echo '<TR>
                                  <TD colspan="4" class="header">Approve
                                  </TD>
                                </TR>
                                <TR>
                                  <TD colspan="4">To approve or deny overtime, specify the particulars, click Approve and confirm when prompted.
                                  </TD>
                                </TR>
                                <TR>
                                  <TD class="short">Staff Name:
                                    <SPAN class="note">*
                                    </SPAN>
                                  </TD>
                                  <TD>';
                                    BuildOvertimeApproveNormalSelector('3', 'ApproveOvertime', 'standard', $_SESSION['cUID']);
                            echo '</TD>
                                  <TD colspan="2" class="right">
                                    <INPUT tabindex="4" name="Submit" type="submit" class="button" value="Approve" />                   
                                  </TD>
                                </TR>';
                        }
                  echo '<TR>
                       </TR>';
                        if (($_SESSION['cAuth'] & 8) || ($_SESSION['cUID'] == 244))
                        {
                          echo '<TR>
                                  <TD colspan="4" class="header">Approve Multiple
                                  </TD>
                                </TR>
                                <TR>
                                  <TD colspan="4">Approve or deny multiple overtime requests for a particular staff member.
                                  </TD>
                                </TR>
                                <TR>
                                  <TD class="short">Staff Name:
                                    <SPAN class="note">*
                                    </SPAN>
                                  </TD>
                                  <TD>';
                                    BuildOvertimeApproveNormalSelectorMultiple('3', 'ApproveOvertimeMulty', 'standard', $_SESSION['cUID']);
                            echo '</TD>
                                  <TD colspan="2" class="right">
                                    <INPUT tabindex="4" name="Submit" type="submit" class="button" value="Approve Multiple" />                   
                                  </TD>
                                </TR>';
                        }
                  echo '<TR>
                          <TD colspan="4" class="header">Edit
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="4">To edit recorded overtime, specify the particulars, click Edit and complete the form that is displayed.
                          </TD>
                        </TR>
                        <TR>
                          <TD class="short">Overtime:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                            BuildOvertimeBankedEditSelector(7, 'EditOvertime', 'long');
                    echo '</TD>
                          <TD colspan="2" class="right">
                            <INPUT tabindex="8" name="Submit" type="submit" class="button" value="Edit" />                   
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="4" class="header">Request
                          </TD>
                        </TR>';
                        if ($_SESSION['cAuth'] & 64)
                        {
                          echo '
                                <TR>
                                  <TD colspan="4">To request overtime, specify the particulars, click Request and complete the form that is displayed.
                                  </TD>
                                </TR>
                                <TR>
                                  <TD class="short">Request:
                                    <SPAN class="note">*
                                    </SPAN>
                                  </TD>
                                  <TD>';
                                    BuildStaffSelector(10, 'RequestOvertimeNormal', 'standard', $_SESSION['cUID']);
                            echo '</TD>
                                  <TD colspan="2" class="right">
                                    <INPUT tabindex="10" name="Submit" type="submit" class="button" value="Request" />                   
                                  </TD>
                                </TR>';

                        } else
                          echo '<TR>
                                  <TD colspan="3">To request overtime, click Request and complete the form that is displayed.
                                  </TD>
                                  <TD class="right">
                                    <INPUT tabindex="10" name="Submit" type="submit" class="button" value="Request" />                   
                                  </TD>
                                </TR>';
                echo '</FORM>
                    </TABLE>
                    </DIV>
                  </DIV>';                
                  
            BuildContentHeader('Overtime Policy', "", "", true);
            echo '<DIV class="contentflow">
                    <P>This is the S4 overtime policy. It follows the Basic Conditions of Employment Act of 1997 which can be found <A tabindex="-1" href="./Files/Intranet/Basic_Conditions_of_Employment.pdf">here</A>.
                      <BR /><BR />
                      When overtime is expected a request to work that overtime must be submitted and approved. Once approved that overtime can be worked and logged. Overtime can be requested at most 3 days after it has been worked.
                      <BR /><BR />
                      By default, any overtime worked is banked. This means that you can use this overtime to take time off work - provided it is feasible. Banked overtime can also be transferred and paid out like normal overtime. By default banked overtime is capped at 100 hours, so once the number of hours reaches 100 any further banked overtime will be paid out. If you wish to change the way your overtime is paid out speak to your supervisor. Additional information can be found <A tabindex="-1" href="./Files/Intranet/Expenses_Guideline.pdf">here</A>.
                      <BR /><BR />
                      Overtime is calculated by multiplying the number of hours of overtime by the applicable multiplier. If overtime does not fall into any of the categories below then no multiplier is applicable. An additional hour of overtime can also be added if travel was necessary. Travel to remote sites by car can be included as OT if out of working hours, Travel by air cannot.
                    </P>
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center" class="standard">
                      <TR>
                        <TD colspan="2" class="header">Rate Multiplier Details
                        </TD>
                      </TR>
                      <TR>
                        <TD class="subheader">Multiplier
                        </TD>
                        <TD class="subheader">Description
                        </TD>
                      </TR>
                      <TR>
                        <TD class="rowA">1.5x
                        </TD>
                        <TD class="rowA">Any work done on a Saturday, non-religious holiday or a Monday following a public holiday on Sunday.              
                        </TD>
                      </TR>
                      <TR>
                        <TD class="standard rowB">2x
                        </TD>
                        <TD class="rowB">Any work done on a Sunday or religious holiday.
                        </TD>
                      </TR>
                    </TABLE>
                  </DIV>';
            
            BuildContentHeader('View Overtime', "", "", true);              
            echo '<DIV class="contentflow">
                    <P>Overtime can be viewed by using the form below.</P>
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center" class="standard">
                      <FORM method="post" action="Handlers/Overtime_Handler.php">
                        <INPUT name="Type" type="hidden" value="View">
                        <TR>
                          <TD colspan="4" class="header">View
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="4">To view overtime, specify the particulars and click View.
                          </TD>
                        </TR>
                        <TR>
                          <TD class="short">Overtime Type:
                          </TD>
                          <TD>';
                            BuildOvertimeTypeSelector(15, 'Overtime', 'standard', "");
                    echo '</TD>
                        </TR>';
                        if (($_SESSION['cAuth'] & 64) || ($_SESSION['cUID'] == 30)) //Add Stefan Buys
                        {
                          echo '<TR>
                                  <TD>Staff Name:
                                  </TD>
                                  <TD>';
                                    BuildStaffSelector(16, 'Staff', 'standard', $_SESSION['cUID']);
                            echo '</TD>
                                </TR>';
                        }
                  echo '<TR>
                          <TD>Date Range:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                            BuildDaySelector(17, 'StartDay', "");
                            echo '&nbsp;';
                            BuildMonthSelector(18, 'StartMonth', "");
                            echo '&nbsp;';
                            BuildYearSelector(19, 'StartYear', "");
                            echo ' to ';
                            BuildDaySelector(20, 'EndDay', "");
                            echo '&nbsp;';
                            BuildMonthSelector(21, 'EndMonth', "");
                            echo '&nbsp;';
                            BuildYearSelector(22, 'EndYear', "");
                    echo '</TD>
                          <TD colspan="2" class="right">
                            <INPUT tabindex="23" name="Submit" type="submit" class="button" value="View" />                   
                          </TD>
                        </TR>
                      </FORM>';
                      if (($_SESSION['cAuth'] & 64) || ($_SESSION['cUID'] == 30)) //or Stefan Buys
                      {
                        echo '<FORM method="post" action="Handlers/Overtime_Handler.php">
                                <INPUT name="Type" type="hidden" value="ViewStatistics">
                                <TR>
                                  <TD colspan="4" class="header">View Statistics
                                  </TD>
                                </TR>
                                <TR>
                                  <TD colspan="4">To view overtime, specify the particulars and click View.
                                  </TD>
                                </TR>
                                <TR>
                                  <TD>Staff Name:
                                  </TD>
                                  <TD>';
                                    BuildStaffSelector(24, 'Staff', 'standard', $_SESSION['cUID']);
                            echo '</TD>
                                </TR>
                                <TR>
                                  <TD>Date Range:
                                    <SPAN class="note">*
                                    </SPAN>
                                  </TD>
                                  <TD>';
                                    BuildMonthSelector(25, 'StartMonth', "");
                                    echo '&nbsp;';
                                    BuildYearSelector(26, 'StartYear', "");
                                    echo ' to ';
                                    BuildMonthSelector(27, 'EndMonth', "");
                                    echo '&nbsp;';
                                    BuildYearSelector(28, 'EndYear', "");
                            echo '</TD>
                                  <TD colspan="2" class="right">
                                    <INPUT tabindex="29" name="Submit" type="submit" class="button" value="View" />                   
                                  </TD>
                                </TR>
                              </FORM>';
                      }
              echo '</TABLE> 
                  </DIV>'; 
          }       
          //////////////////////////////////////////////////////////////////////
        ?>
                            </div>
                        </div>
                    </div>
            </section>
    </DIV>
    <?php 
      // PHP SCRIPT ////////////////////////////////////////////////////////////
      BuildFooter();
      //////////////////////////////////////////////////////////////////////////
      if (isset($_SESSION['ApproveOvertimeNormal']))
      {    
        echo '<script src="Scripts/DevExpress/s4_DevExpressGrids.js"></script>
              <script>
                 var approvedCount = "'.$ApprovedCount.'";
                 var s4_columns = [{dataField: "OvertimePreApp_ID",visible:false},{dataField: "Period",width: 280,caption: "Period"}, 
                                   {dataField: "OvertimePreApp_Hours",width: 55,caption: "Hours"}, {caption: "Project",width: 180,dataField: "Project_Description"}, 
                                   {caption: "Authorised By",width: 150,dataField: "AuthBy"},
                                   {caption: "Date Logged",width: 100,dataField: "LoggedDate",dataType: "date",format: "dd MMM yyyy"}, 
                                   {caption: "Description",dataField: "OvertimePreApp_Description"}];

                 if(approvedCount != 0) 
                        FilterGrid("approved",'.$approvedjson.',false,"'.$currentID.'",s4_columns);

                 FilterGrid("pending",'.$ordersjson.',true,"'.$currentID.'",s4_columns);
             </script>';
      }
    
      if (isset($_SESSION['RequestOvertimeNormal']) || isset($_SESSION['ApproveOvertimeMulty']) || isset($_SESSION['RequestOvertimeBanked'])|| isset($_SESSION['TransferOvertime'])){
           echo'<script src="Scripts/FileScripts/Overtime.js" type="text/javascript"></script>';
      }
    ?>    
  </BODY> 
</HTML>