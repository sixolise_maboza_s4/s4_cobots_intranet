<?php
 
  include 'Scripts/Include.php';  
  SetSettings();
  CheckAuthorisation('Projects.php');
  //////////////////////////////////////////////////////////////////////////////
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3c.org/TR/1999/REC-html401-19991224/loose.dtd">
<HTML>
  <HEAD>
    <?php
      // PHP SCRIPT ////////////////////////////////////////////////////////////
      BuildHead('Projects');
      //////////////////////////////////////////////////////////////////////////
    ?>
        <link rel="stylesheet" type="text/css" media="screen" href="Stylesheets/themes/redmond/jquery-ui-custom.css" />
        <link rel="stylesheet" type="text/css" media="screen" href="Stylesheets/themes/ui.jqgrid.css" />
        <link rel="stylesheet" type="text/css" media="screen" href="Stylesheets/themes/ui.multiselect.css" />
        <script src="Scripts/js/jquery.js" type="text/javascript"></script>
        <script src="Scripts/js/i18n/grid.locale-en.js" type="text/javascript"></script>
	<script type="text/javascript">
            $.jgrid.no_legacy_api = true;
            $.jgrid.useJSON = true;
	</script>
        <script src="Scripts/js/jquery.jqGrid.min.js" type="text/javascript"></script>
        <script src="Scripts/js/jquery.jqChart.min.js" type="text/javascript"></script> 
        <script src="Scripts/js/jquery-ui-custom.min.js" type="text/javascript"></script>
	<script type="text/javascript">
		function setStatusChart(project, name) {
			var url;
			url = "ProjectProgressCharts.php?prc=" + project + "&prn=" + name; 
			$('#statusContent').load(url);
		}
	
		function selectElementContents(el) {
			var body = document.body, range, sel;
			if (document.createRange && window.getSelection) {
				range = document.createRange();
				sel = window.getSelection();
				sel.removeAllRanges();
				try {
					range.selectNodeContents(el);
					sel.addRange(range);
				} catch (e) {
					range.selectNode(el);
					sel.addRange(range);
				}
			} else if (body.createTextRange) {
				range = body.createTextRange();
				range.moveToElementText(el);
				range.select();
			}
		}
	</script>
  </HEAD>
  <BODY>
    <?php
      // PHP SCRIPT ////////////////////////////////////////////////////////////
      BuildBanner();
      //////////////////////////////////////////////////////////////////////////
    ?>
    <DIV class="contentcontainer">
      <?php
        // PHP SCRIPT //////////////////////////////////////////////////////////
        BuildMenu('Main', 'Projects.php');
        ////////////////////////////////////////////////////////////////////////
      ?>
      <DIV class="content">
	 <?php
          // PHP SCRIPT ////////////////////////////////////////////////////////
          BuildMessageSet('Project');
          //////////////////////////////////////////////////////////////////////    
        ?>
        <BR /><BR />
	 <DIV class="left">
            <SPAN class="heading">Project Progress
            </SPAN>
            <BR class="veryshort" />
          </DIV><HR>
        <div align="center">
          <?php include ("ProjectProgressGrid.php");?>
        </div>
          
        <BR /><BR />
 	<HR>
        <BR /><BR />

        <!-- <div align="center">
          <?php include ("ProjectProgressCharts.php");?>
        </div> -->
		
		<div id="statusContent" style="height:500px"></div>
		

      </DIV>
    </DIV>
    <?php
      // PHP SCRIPT ////////////////////////////////////////////////////////////
      BuildFooter();
      //////////////////////////////////////////////////////////////////////////
    ?>
  </BODY>
</HTML>