(function($) {
    $(document).ready(function() {
        $('#file').change(function() {
            $('#fileUpload').submit();
        });
        $("#timezone").select2();
    });
})(jQuery);