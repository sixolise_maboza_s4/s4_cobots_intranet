<?php
  // DETAILS ///////////////////////////////////////////////////////////////////
  //                                                                          //
  //                    Last Edited By: Gareth Ambrose                        //
  //                        Date: 12 May 2008                                 //
  //                                                                          //
  //////////////////////////////////////////////////////////////////////////////
  // This page allows manage allowances.                                      //
  //////////////////////////////////////////////////////////////////////////////
  
  include 'Scripts/Include.php';
  SetSettings();
  CheckAuthorisation('Allowances.php');
  
  //////////////////////////////////////////////////////////////////////////////
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3c.org/TR/1999/REC-html401-19991224/loose.dtd">
<HTML>
  <HEAD>
    <?php
      // PHP SCRIPT ////////////////////////////////////////////////////////////
      BuildHead('Staff Allowances');
    include('Scripts/header.php');
      //////////////////////////////////////////////////////////////////////////
    ?>
  </HEAD>
  <BODY>
    <?php
      // PHP SCRIPT ////////////////////////////////////////////////////////////
      BuildBanner();
      //////////////////////////////////////////////////////////////////////////
    ?>
    <DIV class="main">
      <?php
        // PHP SCRIPT //////////////////////////////////////////////////////////
      BuildTopBar();
        BuildMenu('Main', 'Allowances.php');
        ////////////////////////////////////////////////////////////////////////
      ?>
        <section id="content_wrapper">
            <?php BuildBreadCrumb($currentPath);?>
            <!-- -------------- Content -------------- -->
            <section id="content" class="table-layout">
                <!-- -------------- Column Center -------------- -->
                <div class="chute chute-center" style="height: 869px;">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel">
        <?php
          // PHP SCRIPT ////////////////////////////////////////////////////////
          BuildMessageSet('Allowance');
          //////////////////////////////////////////////////////////////////////    
        ?>
        <?php
          // PHP SCRIPT ////////////////////////////////////////////////////////
          if (isset($_SESSION['AddAllowance']))
          { 
            if ($_SESSION['cAuth'] & 64)
            {           
              $row = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = "'.$_SESSION['AddAllowance'][1].'"'));
              BuildContentHeader('Add '.$_SESSION['AddAllowance'][0].' Allowance - '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'], "", "", true);
            } else
              BuildContentHeader('Add '.$_SESSION['AddAllowance'][0].' Allowance', "", "", true);
            echo '<DIV class="contentflow">
                    <P>Enter the details of the allowance below. Ensure that all the required information is entered before submission and that this information is valid.</P>
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center" class="short">
                      <FORM method="post" action="Handlers/Allowances_Handler.php">
                        <INPUT name="Type" type="hidden" value="Add">
                        <TR>
                          <TD colspan="4" class="header">Allowance Details
                          </TD>
                        </TR>';
                        switch ($_SESSION['AddAllowance'][0])
                        {
                          case 'Out-of-Town':
                            echo '<TR>
                                    <TD class="short">Start Date:
                                      <SPAN class="note">*
                                      </SPAN>
                                    </TD>
                                    <TD>';
                                      BuildDaySelector(1, 'StartDay', GetDayFromSessionDate($_SESSION['AddAllowance'][2]));
                                      echo '&nbsp;';
                                      BuildMonthSelector(2, 'StartMonth', GetMonthFromSessionDate($_SESSION['AddAllowance'][2]));
                                      echo '&nbsp;';
                                      BuildYearSelector(3, 'StartYear', GetYearFromSessionDate($_SESSION['AddAllowance'][2]));
                              echo '</TD>
                                  </TR>
                                  <TR>
                                    <TD class="short">End Date:
                                      <SPAN class="note">*
                                      </SPAN>
                                    </TD>
                                    <TD>';
                                      BuildDaySelector(3, 'EndDay', GetDayFromSessionDate($_SESSION['AddAllowance'][3]));
                                      echo '&nbsp;';
                                      BuildMonthSelector(4, 'EndMonth', GetMonthFromSessionDate($_SESSION['AddAllowance'][3]));
                                      echo '&nbsp;';
                                      BuildYearSelector(5, 'EndYear', GetYearFromSessionDate($_SESSION['AddAllowance'][3]));
                              echo '</TD>
                                  </TR>
                                  <TR>
                                    <TD>Number of Days:
                                      <SPAN class="note">*
                                      </SPAN>
                                    </TD>
                                    <TD>
                                      <INPUT tabindex="6" name="Days" type="text" class="text day" maxlength="3" value="'.$_SESSION['AddAllowance'][4].'" />
                                    </TD>
                                  </TR>
                                  <TR>
                                    <TD>Type:
                                      <SPAN class="note">*
                                      </SPAN>
                                    </TD>
                                    <TD>';
                                      BuildOutOfTownAllowanceTypeSelector(7, 'AllowanceType', 'long', $_SESSION['AddAllowance'][5]);
                              echo '</TD>
                                  </TR>
                                  <TR>
                                    <TD class="vtop">Description:
                                      <SPAN class="note">*
                                      </SPAN>
                                    </TD>
                                    <TD>
                                      <TEXTAREA tabindex="8" name="Description" class="long" maxlength="500">'.$_SESSION['AddAllowance'][6].'</TEXTAREA>
                                    </TD>
                                  </TR>';
                            $notes = "";
                            break;
                          case 'Shift':
                            echo '<TR>
                                    <TD class="short">Date:
                                      <SPAN class="note">**
                                      </SPAN>
                                    </TD>
                                    <TD>';
                                      BuildDaySelector(1, 'Day', GetDayFromSessionDate($_SESSION['AddAllowance'][2]));
                                      echo '&nbsp;';
                                      BuildMonthSelector(2, 'Month', GetMonthFromSessionDate($_SESSION['AddAllowance'][2]));
                                      echo '&nbsp;';
                                      BuildYearSelector(3, 'Year', GetYearFromSessionDate($_SESSION['AddAllowance'][2]));
                              echo '</TD>
                                  </TR>
                                  <TR>
                                    <TD>Shift:
                                      <SPAN class="note">*
                                      </SPAN>
                                    </TD>
                                    <TD>';
                                      BuildShiftSelector(4, 'Shift', 'long', $_SESSION['AddAllowance'][4]);
                              echo '</TD>
                                  </TR>
                                  <TR>
                                    <TD>Project:
                                      <SPAN class="note">***
                                      </SPAN>
                                    </TD>
                                    <TD>';
                                      BuildStaffProjectSelector(5, 'Project', 'long', $_SESSION['AddAllowance'][3], $_SESSION['cUID']);
                              echo '</TD>
                                  </TR>';
                            $notes = '<BR />
                                      <SPAN class="note">**
                                      </SPAN>
                                      If the shift started after midnight then use the previous day\'s date.
                                      <BR />
                                      <SPAN class="note">***
                                      </SPAN>
                                      VW guys use \'VWSA DSA Contract\' and GM guys use \'GMSA DSA Contract\' with whichever plant.';
                            break;
                          case 'Standby':
                            echo '<TR>
                                    <TD class="short">Date:
                                      <SPAN class="note">**
                                      </SPAN>
                                    </TD>
                                    <TD>';
                                      BuildDaySelector(1, 'Day', GetDayFromSessionDate($_SESSION['AddAllowance'][2]));
                                      echo '&nbsp;';
                                      BuildMonthSelector(2, 'Month', GetMonthFromSessionDate($_SESSION['AddAllowance'][2]));
                                      echo '&nbsp;';
                                      BuildYearSelector(3, 'Year', GetYearFromSessionDate($_SESSION['AddAllowance'][2]));
                              echo '</TD>
                                  </TR>
                                  <TR>
                                    <TD>Standby For:
                                      <SPAN class="note">*
                                      </SPAN>
                                    </TD>
                                    <TD>';
                                      BuildStandbySelector(4, 'Standby', 'long', $_SESSION['AddAllowance'][4]);
                              echo '</TD>
                                  </TR>
                                  <TR>
                                    <TD class="vtop">Description:
                                      <SPAN class="note">***
                                      </SPAN>
                                    </TD>
                                    <TD>
                                      <TEXTAREA tabindex="5" name="Description" class="long" maxlength="500">'.$_SESSION['AddAllowance'][3].'</TEXTAREA>
                                    </TD>
                                  </TR>';
                            $notes = '<BR />
                                      <SPAN class="note">**
                                      </SPAN>
                                      This is either the day of the standby or the day it started.
                                      <BR />
                                      <SPAN class="note">***
                                      </SPAN>
                                      Detail all standby information that is not shown by the other fields. This includes things like the reason for standby (if was not scheduled), etc.';
                            break;
                          case 'Travel':                                                  
                            echo '<TR>
                                    <TD class="short">Date:
                                      <SPAN class="note">*
                                      </SPAN>
                                    </TD>
                                    <TD>';
                                      BuildDaySelector(1, 'Day', GetDayFromSessionDate($_SESSION['AddAllowance'][2]));
                                      echo '&nbsp;';
                                      BuildMonthSelector(2, 'Month', GetMonthFromSessionDate($_SESSION['AddAllowance'][2]));
                                      echo '&nbsp;';
                                      BuildYearSelector(3, 'Year', GetYearFromSessionDate($_SESSION['AddAllowance'][2]));
                              echo '</TD>
                                  </TR>
                                  <TR>
                                    <TD>Destination:
                                      <SPAN class="note">*
                                      </SPAN>
                                    </TD>
                                    <TD>
                                      <INPUT tabindex="4" name="Destination" type="text" class="text long" maxlength="50" value="'.$_SESSION['AddAllowance'][4].'" />
                                    </TD>
                                  </TR>
                                  <TR>
                                    <TD>Distance:
                                      <SPAN class="note">*
                                      </SPAN>
                                    </TD>
                                    <TD class="bold">
                                      <INPUT tabindex="5" name="Distance" type="text" class="text veryshort" maxlength="4" value="'.$_SESSION['AddAllowance'][5].'" /> km
                                    </TD>
                                  </TR>
                                  <TR>
                                    <TD class="vtop">Description:
                                      <SPAN class="note">*
                                      </SPAN>
                                    </TD>
                                    <TD>
                                      <TEXTAREA tabindex="6" name="Description" class="long" maxlength="100">'.$_SESSION['AddAllowance'][3].'</TEXTAREA>
                                    </TD>
                                  </TR>';
                            $notes = "";
                            break;
                          default:   
                            echo 'Kyaaaaaaaaaah! *chop* Invalid allowance type! Error! Error! '.$_SESSION['AddAllowance'][0];                       
                            break;
                        }
                  echo '<TR>
                          <TD colspan="2" class="center">
                            <INPUT tabindex="9" name="Submit" type="submit" class="button" value="Submit" />   
                            <INPUT tabindex="10" name="Submit" type="submit" class="button" value="Cancel" />                  
                          </TD>
                        </TR>
                      </FORM>
                    </TABLE>  
                  </DIV>  
                  <DIV>
                    <BR />
                    <SPAN class="note">*
                    </SPAN>
                    These fields are required. '.$notes.'
                  </DIV>';
          } else
          if (isset($_SESSION['EditAllowance']))
          {
            switch ($_SESSION['EditAllowance'][1])
            {
              case 'Out-of-Town':
                $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM OutOfTownAllowance WHERE OutOfTownAllowance_ID = "'.$_SESSION['EditAllowance'][0].'"'));
                $staffCode = $rowTemp['OutOfTownAllowance_Name'];
                $date = GetTextualDateFromDatabaseDate($rowTemp['OutOfTownAllowance_Date_Start']).' to '.GetTextualDateFromDatabaseDate($rowTemp['OutOfTownAllowance_Date_End']);
                break;
              case 'Shift':
                $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM ShiftAllowance WHERE ShiftAllowance_ID = "'.$_SESSION['EditAllowance'][0].'"'));
                $staffCode = $rowTemp['ShiftAllowance_Name'];
                $date = GetTextualDateFromDatabaseDate($rowTemp['ShiftAllowance_Date']);
                break;
              case 'Standby':
                $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Standby WHERE Standby_ID = "'.$_SESSION['EditAllowance'][0].'"'));
                $staffCode = $rowTemp['Standby_Name'];
                $date = GetTextualDateFromDatabaseDate($rowTemp['Standby_DateStart']);
                break;
              case 'Travel':
                $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM TravelAllowance WHERE TravelAllowance_ID = "'.$_SESSION['EditAllowance'][0].'"'));
                $staffCode = $rowTemp['TravelAllowance_Name'];
                $date = GetTextualDateFromDatabaseDate($rowTemp['TravelAllowance_Date']);
                break;
              default:
                break;
            }
            
            if ($_SESSION['cAuth'] & 64)
            {
              $row = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = '.$staffCode.''));
              BuildContentHeader('Edit '.$_SESSION['EditAllowance'][1].' Allowance - '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].' for '.$date, "", "", true);
            } else
              BuildContentHeader('Edit '.$_SESSION['EditAllowance'][1].' Allowance for '.$date, "", "", true);
              
            echo '<DIV class="contentflow">
                    <P>Once you have made changes, ensure that you submit them otherwise they will not take effect.</P>
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center" class="short">
                      <FORM method="post" action="Handlers/Allowances_Handler.php">
                        <INPUT name="Type" type="hidden" value="Edit">
                        <TR>
                          <TD colspan="2" class="header">Allowance Details
                          </TD>
                        </TR>';
                        switch ($_SESSION['EditAllowance'][1])
                        {
                          case 'Out-of-Town':
                            echo '<TR>
                                    <TD class="short">Start Date:
                                      <SPAN class="note">*
                                      </SPAN>
                                    </TD>
                                    <TD>';
                                      BuildDaySelector(1, 'StartDay', GetDayFromSessionDate($_SESSION['EditAllowance'][2]));
                                      echo '&nbsp;';
                                      BuildMonthSelector(2, 'StartMonth', GetMonthFromSessionDate($_SESSION['EditAllowance'][2]));
                                      echo '&nbsp;';
                                      BuildYearSelector(3, 'StartYear', GetYearFromSessionDate($_SESSION['EditAllowance'][2]));
                              echo '</TD>
                                  </TR>
                                  <TR>
                                    <TD class="short">End Date:
                                      <SPAN class="note">*
                                      </SPAN>
                                    </TD>
                                    <TD>';
                                      BuildDaySelector(3, 'EndDay', GetDayFromSessionDate($_SESSION['EditAllowance'][3]));
                                      echo '&nbsp;';
                                      BuildMonthSelector(4, 'EndMonth', GetMonthFromSessionDate($_SESSION['EditAllowance'][3]));
                                      echo '&nbsp;';
                                      BuildYearSelector(5, 'EndYear', GetYearFromSessionDate($_SESSION['EditAllowance'][3]));
                              echo '</TD>
                                  </TR>
                                  <TR>
                                    <TD>Number of Days:
                                      <SPAN class="note">*
                                      </SPAN>
                                    </TD>
                                    <TD>
                                      <INPUT tabindex="6" name="Days" type="text" class="text day" maxlength="3" value="'.$_SESSION['EditAllowance'][4].'" />
                                    </TD>
                                  </TR>
                                  <TR>
                                    <TD>Type:
                                      <SPAN class="note">*
                                      </SPAN>
                                    </TD>
                                    <TD>';
                                      BuildOutOfTownAllowanceTypeSelector(7, 'AllowanceType', 'long', $_SESSION['EditAllowance'][5]);
                              echo '</TD>
                                  </TR>
                                  <TR>
                                    <TD class="vtop">Description:
                                      <SPAN class="note">*
                                      </SPAN>
                                    </TD>
                                    <TD>
                                      <TEXTAREA tabindex="8" name="Description" class="long" maxlength="500">'.$_SESSION['EditAllowance'][6].'</TEXTAREA>
                                    </TD>
                                  </TR>';
                            $notes = "";
                            break;
                          case 'Shift':
                            echo '<TR>
                                    <TD class="short">Date:
                                      <SPAN class="note">**
                                      </SPAN>
                                    </TD>
                                    <TD>';
                                      BuildDaySelector(1, 'Day', GetDayFromSessionDate($_SESSION['EditAllowance'][2]));
                                      echo '&nbsp;';
                                      BuildMonthSelector(2, 'Month', GetMonthFromSessionDate($_SESSION['EditAllowance'][2]));
                                      echo '&nbsp;';
                                      BuildYearSelector(3, 'Year', GetYearFromSessionDate($_SESSION['EditAllowance'][2]));
                              echo '</TD>
                                  </TR>
                                  <TR>
                                    <TD>Shift:
                                      <SPAN class="note">*
                                      </SPAN>
                                    </TD>
                                    <TD>';
                                      BuildShiftSelector(4, 'Shift', 'long', $_SESSION['EditAllowance'][4]);
                              echo '</TD>
                                  </TR>
                                  <TR>
                                    <TD>Project:
                                      <SPAN class="note">***
                                      </SPAN>
                                    </TD>
                                    <TD>';
                                      BuildStaffProjectSelector(5, 'Project', 'long', $_SESSION['EditAllowance'][3], $_SESSION['cUID']);
                              echo '</TD>
                                  </TR>';
                            $notes = '<BR />
                                      <SPAN class="note">**
                                      </SPAN>
                                      If the shift started after midnight then use the previous day\'s date.
                                      <BR />
                                      <SPAN class="note">***
                                      </SPAN>
                                      VW guys use \'VW DSA Contract\' and GM guys use \'GM DSA Contract\' with whichever plant.';
                            break;
                          case 'Standby':                          
                            echo '<TR>
                                    <TD class="short">Date:
                                      <SPAN class="note">**
                                      </SPAN>
                                    </TD>
                                    <TD>';
                                      BuildDaySelector(1, 'Day', GetDayFromSessionDate($_SESSION['EditAllowance'][2]));
                                      echo '&nbsp;';
                                      BuildMonthSelector(2, 'Month', GetMonthFromSessionDate($_SESSION['EditAllowance'][2]));
                                      echo '&nbsp;';
                                      BuildYearSelector(3, 'Year', GetYearFromSessionDate($_SESSION['EditAllowance'][2]));
                              echo '</TD>
                                  </TR>
                                  <TR>
                                    <TD>Standby For:
                                      <SPAN class="note">*
                                      </SPAN>
                                    </TD>
                                    <TD>';
                                      BuildStandbySelector(4, 'Standby', 'long', $_SESSION['EditAllowance'][4]);
                              echo '</TD>
                                  </TR>
                                  <TR>
                                    <TD class="vtop">Description:
                                      <SPAN class="note">***
                                      </SPAN>
                                    </TD>
                                    <TD>
                                      <TEXTAREA tabindex="5" name="Description" class="long" maxlength="500">'.$_SESSION['EditAllowance'][3].'</TEXTAREA>
                                    </TD>
                                  </TR>';
                            $notes = '<BR />
                                      <SPAN class="note">**
                                      </SPAN>
                                      This is either the day of the standby or the day it started.
                                      <BR />
                                      <SPAN class="note">***
                                      </SPAN>
                                      Detail all standby information that is not shown by the other fields. This includes things like the reason for standby (if was not scheduled), etc.';
                            break;
                          case 'Travel':                                                  
                            echo '<TR>
                                    <TD class="short">Date:
                                      <SPAN class="note">*
                                      </SPAN>
                                    </TD>
                                    <TD>';
                                      BuildDaySelector(1, 'Day', GetDayFromSessionDate($_SESSION['EditAllowance'][2]));
                                      echo '&nbsp;';
                                      BuildMonthSelector(2, 'Month', GetMonthFromSessionDate($_SESSION['EditAllowance'][2]));
                                      echo '&nbsp;';
                                      BuildYearSelector(3, 'Year', GetYearFromSessionDate($_SESSION['EditAllowance'][2]));
                              echo '</TD>
                                  </TR>
                                  <TR>
                                    <TD>Destination:
                                      <SPAN class="note">*
                                      </SPAN>
                                    </TD>
                                    <TD>
                                      <INPUT tabindex="4" name="Destination" type="text" class="text long" value="'.$_SESSION['EditAllowance'][4].'" maxlength="50" />
                                    </TD>
                                  </TR>
                                  <TR>
                                    <TD>Distance:
                                      <SPAN class="note">*
                                      </SPAN>
                                    </TD>
                                    <TD class="bold">
                                      <INPUT tabindex="5" name="Distance" type="text" class="text veryshort" value="'.$_SESSION['EditAllowance'][5].'" maxlength="4" /> km
                                    </TD>
                                  </TR>
                                  <TR>
                                    <TD class="vtop">Description:
                                      <SPAN class="note">*
                                      </SPAN>
                                    </TD>
                                    <TD>
                                      <TEXTAREA tabindex="6" name="Description" class="long" maxlength="100">'.$_SESSION['EditAllowance'][3].'</TEXTAREA>
                                    </TD>
                                  </TR>';
                            $notes = "";
                            break;
                          default:   
                            echo 'Kyaaaaaaaaaah! *chop* Invalid allowance type! Error! Error! '.$_SESSION['EditAllowance'][1];                       
                            break;
                        }    
                  echo '<TR>
                          <TD colspan="2" class="center">
                            <INPUT tabindex="7" name="Submit" type="submit" class="button" value="Submit" />   
                            <INPUT tabindex="8" name="Submit" type="submit" class="button" value="Cancel" />                  
                          </TD>
                        </TR>
                      </FORM>
                    </TABLE>  
                  </DIV>  
                  <DIV>
                    <BR />
                    <SPAN class="note">*
                    </SPAN>
                    These fields are required. '.$notes.'
                  </DIV>';
          } else
          if (isset($_SESSION['RemoveAllowance']))
          {            
            switch ($_SESSION['RemoveAllowance'][1])
            {
              case 'Out-of-Town':
                $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM OutOfTownAllowance WHERE OutOfTownAllowance_ID = "'.$_SESSION['RemoveAllowance'][0].'"'));
                $staffCode = $rowTemp['OutOfTownAllowance_Name'];
                $date = GetTextualDateFromDatabaseDate($rowTemp['OutOfTownAllowance_Date_Start']).' to '.GetTextualDateFromDatabaseDate($rowTemp['OutOfTownAllowance_Date_End']);
                break;
              case 'Shift':
                $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM ShiftAllowance WHERE ShiftAllowance_ID = "'.$_SESSION['RemoveAllowance'][0].'"'));
                $staffCode = $rowTemp['ShiftAllowance_Name'];
                $date = GetTextualDateFromDatabaseDate($rowTemp['ShiftAllowance_Date']);
                break;
              case 'Standby':
                $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Standby WHERE Standby_ID = "'.$_SESSION['RemoveAllowance'][0].'"'));
                $staffCode = $rowTemp['Standby_Name'];
                $date = GetTextualDateFromDatabaseDate($rowTemp['Standby_DateStart']);
                break;
              case 'Travel':
                $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM TravelAllowance WHERE TravelAllowance_ID = "'.$_SESSION['RemoveAllowance'][0].'"'));
                $staffCode = $rowTemp['TravelAllowance_Name'];
                $date = GetTextualDateFromDatabaseDate($rowTemp['TravelAllowance_Date']);
                break;
              default:
                break;
            }
            
            if ($_SESSION['cAuth'] & 64)
            {
              $row = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = '.$staffCode.''));
              BuildContentHeader('Remove '.$_SESSION['RemoveAllowance'][1].' Allowance - '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].' for '.$date, "", "", true);
            } else
              BuildContentHeader('Remove '.$_SESSION['RemoveAllowance'][1].' Allowance for '.$date, "", "", true);
            
            echo '<DIV class="contentflow">
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center" class="short">
                      <TR>
                        <TD colspan="2" class="header">Allowance Details
                        </TD>
                      </TR>';
                      switch ($_SESSION['RemoveAllowance'][1])
                      {
                          case 'Out-of-Town':
                            $rowTempB = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM OutOfTownAllowanceType WHERE OutOfTownAllowanceType_ID = "'.$rowTemp['OutOfTownAllowance_Type'].'"'));                                                 
                            echo '<TR>
                                    <TD class="short">Start Date:
                                    </TD>
                                    <TD class="bold">'.GetTextualDateFromDatabaseDate($rowTemp['OutOfTownAllowance_Date_Start']).'
                                    </TD>
                                  </TR>
                                  <TR>
                                    <TD class="short">End Date:
                                    </TD>
                                    <TD class="bold">'.GetTextualDateFromDatabaseDate($rowTemp['OutOfTownAllowance_Date_End']).'
                                    </TD>
                                  </TR>
                                  <TR>
                                    <TD>Type:
                                    </TD>
                                    <TD class="bold">'.$rowTempB['OutOfTownAllowanceType_Description'].'
                                    </TD>
                                  </TR>
                                  <TR>
                                    <TD>Number of Days:
                                    </TD>
                                    <TD class="bold">'.$rowTemp['OutOfTownAllowance_Days'].'
                                    </TD>
                                  </TR>
                                  <TR>
                                    <TD class="vtop">Description:
                                    </TD>
                                    <TD class="bold vtop">'.$rowTemp['OutOfTownAllowance_Description'].'
                                    </TD>
                                  </TR>';
                            break;
                        case 'Shift':
                          $rowTempB = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Project WHERE Project_Code = "'.$rowTemp['ShiftAllowance_Project'].'"'));
                          $rowTempC = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM ShiftAllowanceType WHERE ShiftAllowanceType_ID = "'.$rowTemp['ShiftAllowance_Shift'].'"'));                                                 
                          echo '<TR>
                                  <TD class="short">Date:
                                  </TD>
                                  <TD class="bold short">'.GetTextualDateFromDatabaseDate($rowTemp['ShiftAllowance_Date']).'
                                  </TD>
                                </TR>
                                <TR>
                                  <TD>Project:
                                  </TD>
                                  <TD class="bold">'.$rowTempB['Project_Description'].'
                                  </TD>
                                </TR>
                                <TR>
                                  <TD>Shift:
                                  </TD>
                                  <TD class="bold">'.$rowTempC['ShiftAllowanceType_Description'].'
                                  </TD>
                                </TR>';
                          break;
                        case 'Standby':
                          $rowTempB = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM StandbyType WHERE StandbyType_ID = "'.$rowTemp['Standby_Type'].'"'));                                                 
                          echo '<TR>
                                  <TD class="short">Date:
                                  </TD>
                                  <TD class="bold short">'.GetTextualDateFromDatabaseDate($rowTemp['Standby_DateStart']).'
                                  </TD>
                                </TR>
                                <TR>
                                  <TD>Standy For:
                                  </TD>
                                  <TD class="bold">'.$rowTempB['StandbyType_Description'].'
                                  </TD>
                                </TR>
                                <TR>
                                  <TD>Description:
                                  </TD>
                                  <TD rowspan="4" class="bold">'.$rowTemp['Standby_Comments'].'
                                  </TD>
                                </TR>';
                          break;
                        case 'Travel':                                                  
                          echo '<TR>
                                  <TD class="short">Date:
                                  </TD>
                                  <TD class="bold short">'.GetTextualDateFromDatabaseDate($rowTemp['TravelAllowance_Date']).'
                                  </TD>
                                </TR>
                                <TR>
                                  <TD>Destination:
                                  </TD>
                                  <TD class="bold">'.$rowTemp['TravelAllowance_Destination'].'
                                  </TD>
                                </TR>
                                <TR>
                                  <TD>Distance:
                                  </TD>
                                  <TD class="bold">'.$rowTemp['TravelAllowance_Km'].' km
                                  </TD>
                                </TR>
                                <TR>
                                  <TD>Description:
                                  </TD>
                                  <TD rowspan="4" class="bold">'.$rowTemp['TravelAllowance_Description'].'
                                  </TD>
                                </TR>';
                          break;
                        default:   
                          echo 'Kyaaaaaaaaaah! *chop* Invalid allowance type! Error! Error! '.$_SESSION['RemoveAllowance'][1];                       
                          break;
                      }
              echo '</TABLE>
                    <BR /><BR />
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center" class="short">
                      <FORM method="post" action="Handlers/Allowances_Handler.php">
                        <INPUT name="Type" type="hidden" value="Remove" />
                        <TR>
                          <TD colspan="4" class="header">Remove
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="4" class="center">Are you sure you wish to remove this allowance entry?
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="2" class="center">
                            <INPUT tabindex="1" name="Submit" type="submit" class="button" value="Yes" />   
                            <INPUT tabindex="2" name="Submit" type="submit" class="button" value="No" />                  
                          </TD>
                        </TR>
                      </FORM>
                    </TABLE>  
                  </DIV>';
          } else
          {
            BuildContentHeader('Maintenance', "", "", true);
            echo '<DIV class="contentflow">
                    <P>Allowances can be recorded by using the form below. Details on allowances already listed can also be edited or removed.</P>
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center" class="standard">
                      <FORM method="post" action="Handlers/Allowances_Handler.php">
                        <INPUT name="Type" type="hidden" value="Maintain">
                        <TR>
                          <TD colspan="4" class="header">Add
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="4">To record an allowance, specify the particulars, click Add and complete the form that is displayed.
                          </TD>
                        </TR>';
                        if ($_SESSION['cAuth'] & 64)
                        {
                          echo '<TR>
                                  <TD class="short">Staff Name:
                                    <SPAN class="note">*
                                    </SPAN>
                                  </TD>
                                  <TD>';
                                    BuildStaffSelector(1, 'Staff', 'standard', $_SESSION['cUID'], true);
                            echo '</TD>
                                </TR>';
                        }
                  echo '<TR>
                          <TD class="short">Allowance Type:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>'; 
                            BuildAllowanceTypeSelector(2, 'AddAllowance', 'standard', "");
                    echo '</TD>
                          <TD colspan="2" class="right">
                            <INPUT tabindex="3" name="Submit" type="submit" class="button" value="Add" />                   
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="4" class="header">Edit
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="4">To edit an allowance, specify the particulars, click Edit and complete the form that is displayed. Select only one entry at a time.
                          </TD>
                        </TR>
                        <TR>
                          <TD class="short">Out-of-Town Allowance:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                          if ($_SESSION['cAuth'] & 64)
                            BuildAllowanceSelector(4, 'EditAllowanceOutOfTown', 'long', "", 'Out-of-Town');
                          else
                            BuildAllowanceSelector(4, 'EditAllowanceOutOfTown', 'long', $_SESSION['cUID'], 'Out-of-Town');
                    echo '</TD>
                        </TR>
                        <TR>
                          <TD class="short">Shift Allowance:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                          if ($_SESSION['cAuth'] & 64)
                            BuildAllowanceSelector(4, 'EditAllowanceShift', 'long', "", 'Shift');
                          else
                            BuildAllowanceSelector(4, 'EditAllowanceShift', 'long', $_SESSION['cUID'], 'Shift');
                    echo '</TD>
                        </TR>
                        <TR>
                          <TD class="short">Standby Allowance:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                          if ($_SESSION['cAuth'] & 64)
                            BuildAllowanceSelector(5, 'EditAllowanceStandby', 'long', "", 'Standby');
                          else
                            BuildAllowanceSelector(5, 'EditAllowanceStandby', 'long', $_SESSION['cUID'], 'Standby');
                    echo '</TD>
                        </TR>
                        <TR>
                          <TD class="short">Travel Allowance:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                          if ($_SESSION['cAuth'] & 64)
                            BuildAllowanceSelector(6, 'EditAllowanceTravel', 'long', "", 'Travel');
                          else
                            BuildAllowanceSelector(6, 'EditAllowanceTravel', 'long', $_SESSION['cUID'], 'Travel');
                    echo '</TD>
                          <TD colspan="2" class="right">
                            <INPUT tabindex="7" name="Submit" type="submit" class="button" value="Edit" />                   
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="4" class="header">Remove
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="4">To remove an allowance, specify the particulars, click Remove and confirm when prompted. Select only one entry at a time.
                          </TD>
                        </TR>
                        <TR>
                          <TD class="short">Out-of-Town Allowance:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                          if ($_SESSION['cAuth'] & 64)
                            BuildAllowanceSelector(8, 'RemoveAllowanceOutOfTown', 'long', "", 'Out-of-Town');
                          else
                            BuildAllowanceSelector(8, 'RemoveAllowanceOutOfTown', 'long', $_SESSION['cUID'], 'Out-of-Town');
                    echo '</TD>
                        </TR>
                        <TR>
                          <TD class="short">Shift Allowance:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                          if ($_SESSION['cAuth'] & 64)
                            BuildAllowanceSelector(8, 'RemoveAllowanceShift', 'long', "", 'Shift');
                          else
                            BuildAllowanceSelector(8, 'RemoveAllowanceShift', 'long', $_SESSION['cUID'], 'Shift');
                    echo '</TD>
                        </TR>
                        <TR>
                          <TD class="short">Standby Allowance:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                          if ($_SESSION['cAuth'] & 64)
                            BuildAllowanceSelector(9, 'RemoveAllowanceStandby', 'long', "", 'Standby');
                          else
                            BuildAllowanceSelector(9, 'RemoveAllowanceStandby', 'long', $_SESSION['cUID'], 'Standby');
                    echo '</TD>
                        </TR>
                        <TR>
                          <TD class="short">Travel Allowance:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                          if ($_SESSION['cAuth'] & 64)
                            BuildAllowanceSelector(10, 'RemoveAllowanceTravel', 'long', "", 'Travel');
                          else
                            BuildAllowanceSelector(10, 'RemoveAllowanceTravel', 'long', $_SESSION['cUID'], 'Travel');
                    echo '</TD>
                          <TD colspan="2" class="right">
                            <INPUT tabindex="11" name="Submit" type="submit" class="button" value="Remove" />                   
                          </TD>
                        </TR>
                      </FORM>
                    </TABLE>
                  </DIV>';
          }
          //////////////////////////////////////////////////////////////////////
        ?>
      </div>
    </div>
    </div>
    </section
    </DIV>
    <?php
      // PHP SCRIPT ////////////////////////////////////////////////////////////
      BuildFooter();
      //////////////////////////////////////////////////////////////////////////
    ?>
  </BODY>
</HTML>