<?php 
  // DETAILS ///////////////////////////////////////////////////////////////////
  //                                                                          //
  //                    Last Edited By: Gareth Ambrose                        //
  //                        Date: 29 January 2008                             //
  //                                                                          //
  //////////////////////////////////////////////////////////////////////////////
  // This page allows users to view permissions.                              //
  //////////////////////////////////////////////////////////////////////////////
   
  include 'Scripts/Include.php';
  SetSettings();
  CheckAuthorisation('Permissions.php'); 
  
  //////////////////////////////////////////////////////////////////////////////
?>  
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3c.org/TR/1999/REC-html401-19991224/loose.dtd">
<HTML>
  <HEAD>
    <?php 
      // PHP SCRIPT ////////////////////////////////////////////////////////////
      BuildHead('Permissions');
    include ('Scripts/header.php');
      //////////////////////////////////////////////////////////////////////////
    ?>   
  </HEAD>
  <BODY> 
    <?php 
      // PHP SCRIPT ////////////////////////////////////////////////////////////
      BuildBanner();
      //////////////////////////////////////////////////////////////////////////
    ?>    
    <DIV class="contentcontainer">
      <?php 
        // PHP SCRIPT //////////////////////////////////////////////////////////          
        BuildMenu('Main', 'Permissions.php');                            
        ////////////////////////////////////////////////////////////////////////
      ?>
      <DIV class="content">
        <BR /><BR />  
        <?php
          // PHP SCRIPT ////////////////////////////////////////////////////////          
          BuildContentHeader('Page Permissions', "", "", false);
          //////////////////////////////////////////////////////////////////////
        ?>    
        <DIV class="contentflow">
          <P>These are the intranet page permissions that control the acces level needed to view a page. As the permission level increases, so does the related functionality and importance of a page. Currently some permission levels are not used and are reserved for future use.</P>
          <BR /><BR />
          <TABLE cellspacing="5" align="center" class="long">
            <TR>
              <TD colspan="2" class="header">Page Permission Details
              </TD>
            </TR>
            <TR>
              <TD class="subheader short">Level / Mask
              </TD>
              <TD class="subheader">Description
              </TD>
            </TR>
            <TR>
              <TD class="rowA">0
              </TD>
              <TD class="rowA">Unrestricted. Viewable to any visitor to the intranet. This is used to provide casuals with access to certain areas of the intranet.               
              </TD>
            </TR>
            <TR>
              <TD class="rowB">1
              </TD>
              <TD class="rowB">Restricted. This is the lowest level of restricted access. It is used on pages which are accessible to non-employees.
              </TD>
            </TR>
            <TR>
              <TD class="rowA">2
              </TD>
              <TD class="rowA">Restricted. Reserved for future use.              
              </TD>
            </TR>
            <TR>
              <TD class="rowB">4
              </TD>
              <TD class="rowB">Restricted. This is used for general employee pages.              
              </TD>
            </TR>
            <TR>
              <TD class="rowA">8
              </TD>
              <TD class="rowA">Restricted. This is used to control pages which allow the authorisation of leave and overtime. It's also used for RFQs.
              </TD>
            </TR>
            <TR>
              <TD class="rowB">16
              </TD>
              <TD class="rowB">Restricted. Administration Level 1. Allows for creating quotes and managing casuals.
              </TD>
            </TR>
            <TR>
              <TD class="rowA">32
              </TD>
              <TD class="rowA">Restricted. Administration Level 2.
              </TD>
            </TR>
            <TR>
              <TD class="rowB">64
              </TD>
              <TD class="rowB">Restricted. Management Level 1.
              </TD>
            </TR>
            <TR>
              <TD class="rowA">128
              </TD>
              <TD class="rowA">Restricted. Management Level 2. Not used.
              </TD>
            </TR>
            <TR>
              <TD class="rowB">255
              </TD>
              <TD class="rowB">Restricted. Development and Administrator Level. These pages are not masked - the user auth level has to equal 255 in order to have access.
              </TD>
            </TR>
          </TABLE>
        </DIV>
        <?php
          // PHP SCRIPT ////////////////////////////////////////////////////////
          BuildContentHeader('User Permissions', "", "", true);
          //////////////////////////////////////////////////////////////////////
        ?>
        <DIV class="contentflow">
          <P>These are the intranet user permissions for controlling intranet use. As the permission level increases, so does its level of accessibility.</P>
          <BR /><BR />
          <TABLE cellspacing="5" align="center" class="long">
            <TR>
              <TD colspan="2" class="header">User Permission Details
              </TD>
            </TR>
            <TR>
              <TD class="subheader short">Level
              </TD>
              <TD class="subheader">Description
              </TD>
            </TR>
            <TR>
              <TD class="rowA">0
              </TD>
              <TD class="rowA">Blocks all access to restricted areas of the intranet. Assigned to previous S4 employee accounts.
              </TD>
            </TR>
            <TR>
              <TD class="rowB">1
              </TD>
              <TD class="rowB">Assigned to non-employees who require access to the intranet. This is the lowest level of access.
              </TD>
            </TR>
            <TR>
              <TD class="rowA">5
              </TD>
              <TD class="rowA">Assigned to general employees. Allows access to the majority of the intranet but does not allow access to any administration or management functions.
              </TD>
            </TR>
            <TR>
              <TD class="rowB">13
              </TD>
              <TD class="rowB">Assigned to site managers. Allows authorisation of leave and overtime as well as general employee functions.
              </TD>
            </TR>
            <TR>
              <TD class="rowA">21
              </TD>
              <TD class="rowA">Assigned to general employees which can also perform basic administration functions such as submitting quotes.
              </TD>
            </TR>
            <TR>
              <TD class="rowB">53
              </TD>
              <TD class="rowB">Assigned to special administration employees. Allows access to all administration and general employee functions.
              </TD>
            </TR>
            <TR>
              <TD class="rowA">223
              </TD>
              <TD class="rowA">Assigned to management. Allows access to the entire intranet aside from special adminstration functions.
              </TD>
            </TR>
            <TR>
              <TD class="rowB">255
              </TD>
              <TD class="rowB">Assigned to development. Allows access to the full intranet to assist in its development and for intranet adminsitration.
              </TD>
            </TR>
          </TABLE>
        </DIV>
        <BR /><BR />
      </DIV>
    </DIV>
    <?php
      // PHP SCRIPT ////////////////////////////////////////////////////////////
      BuildFooter();
      //////////////////////////////////////////////////////////////////////////
    ?>
  </BODY>
</HTML>