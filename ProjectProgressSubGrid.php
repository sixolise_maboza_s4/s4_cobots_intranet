<?php
require_once 'Scripts/jq-config.php';
require_once ABSPATH."php/jqGrid.php";
require_once ABSPATH."php/jqGridPdo.php";
require_once 'Scripts/Include.php'; 

//$conn = new PDO(DB_DSN,DB_USER,DB_PASSWORD);

$conn = Connect();

$conn->query("SET NAMES utf8");

// Get the needed parameters passed from the main grid
// By default we add to postData subgrid and rowid parameters in the main grid
$subtable = jqGridUtils::Strip($_REQUEST["ProjectProgressSubGrid"]);
$rowid = jqGridUtils::Strip($_REQUEST["rowid"]);
$projectCode = jqGridUtils::Strip($_REQUEST["Project_Code"]);
$materialBudget = jqGridUtils::Strip($_REQUEST["Project_MaterialBudget"]);
$labourBudget = jqGridUtils::Strip($_REQUEST["Project_LabourBudget"]);


$resultSet = ExecuteQuery('SELECT SUM(OrderNo_Total_Cost) FROM OrderNo WHERE OrderNo_Project = '.$projectCode);

//$resultSet = ExecuteQuery('SELECT SUM(OrderNo_Total_Cost) FROM OrderNo WHERE OrderNo_Project = 100');
if($resultSet) {
    while ($row = MySQL_Fetch_Array($resultSet)) {
        $materialPurchased = $row[0];
    } 
} else {
    $materialPurchased = "0";
}

$resultSet = ExecuteQuery('SELECT SUM(ProjectTimeAllocation_Entry_Hours) FROM ProjectTimeAllocation_Entry WHERE ProjectTimeAllocation_Entry_Project = '.$projectCode);

//$resultSet = ExecuteQuery('SELECT SUM(OrderNo_Total_Cost) FROM OrderNo WHERE OrderNo_Project = 100');
if($resultSet) {
    while ($row = MySQL_Fetch_Array($resultSet)) {
        $labourHours = $row[0];
    } 
} else {
    $labourHours = "0";
}



// create the jqGrid instance
$grid = new jqGridRender($conn);
// set the SQL select query
$grid->SelectCommand = "SELECT * FROM ProjectStatus WHERE ProjectCode = ?";
// set the output format to json
$grid->dataType = 'json';
$grid->table = 'ProjectStatus';
$grid->setPrimaryKeyId('PrimaryKey');
$grid->serialKey = true;
$grid->del = false;
$grid->debug = true;

$grid->showError = true; 
$grid->customError = "MyError"; 

//Custom function to show the error 
function MyError($oper, $msg)  
{ 
    if($oper == "edit") { 
        return "EDIT: ".$msg; 
    } 
	
	if($oper == "del") { 
        return "DELETE: ".$msg; 
    } 
}

// Definition of the labels
$mylabels = array("ProjectCode"=>"Project", 
                  "DateEntry"=>"Date", 
                  "ProjectCompletion"=>"Completion %", 
                  "MaterialCurrent"=>"Material Purchased", 
                  "MaterialPercent"=>"Material Used %", 
                  "LabourCurrent"=>"Labour (Hours)", 
                  "LabourPercent"=>"Labour %");

// Let the grid create the column model automatically
$grid->setColModel(null,array(&$rowid),$mylabels);

// javascriptCode for the datepicker
$mydatepicker = <<<DATEPICK
function(elm){
	setTimeout(function(){
       	jQuery(elm).datepicker({dateFormat:'yy-mm-dd'});
              jQuery('.ui-datepicker').css({'font-size':'90%'});
       },200);
}
DATEPICK;


$grid->setDbTime('Y-m-d');
$grid->setDbDate('Y-m-d');
$grid->setUserDate('Y-m-d');
$grid->setUserTime('Y-m-d');

$grid->setColProperty('PrimaryKey', array("hidden"=>true, "width"=>50, "editable"=>false));
$grid->setColProperty('ProjectCode', array("hidden"=>true, "frozen"=>true, "width"=>500, "editable"=>true, "editoptions"=>array("defaultValue"=>$projectCode)));
//$grid->setColProperty('DateEntry', array("formatter"=>"date","formatoptions"=>array("srcformat"=>"Y-m-d", "newformat"=>"Y-m-d"),"editoptions"=>array("value"=>date("Y-m-d"), "readonly"=>false, "dataInit"=>"js:".$mydatepicker), "searchoptions"=>array("dataInit"=>"js:".$mydatepicker)));
$grid->setColProperty('DateEntry', array("editoptions"=>array("value"=>date("Y-m-d"), "readonly"=>false, "dataInit"=>"js:".$mydatepicker), "searchoptions"=>array("dataInit"=>"js:".$mydatepicker)));
$grid->setColProperty('ProjectCompletion', array("width"=>300, "editable"=>true));
$grid->setColProperty('MaterialBudget', array("hidden"=>true, "width"=>300, "editable"=>true, "editoptions"=>array("defaultValue"=>$materialBudget)));
$grid->setColProperty('MaterialCurrent', array("hidden"=>true, "width"=>300, "editable"=>true, "editoptions"=>array("defaultValue"=>$materialPurchased)));
$grid->setColProperty('MaterialPercent', array("width"=>300, "editable"=>true));
$grid->setColProperty('LabourBudget', array("hidden"=>true, "width"=>300, "editable"=>true, "editoptions"=>array("defaultValue"=>$labourBudget)));
$grid->setColProperty('LabourCurrent', array("hidden"=>true, "width"=>300, "editable"=>true, "editoptions"=>array("defaultValue"=>$labourHours)));
$grid->setColProperty('LabourPercent', array("width"=>300, "editable"=>true));

//$grid->debug = true;

//$grid->setSelect("ProjectCode", "SELECT DISTINCT Project_Description FROM Project", true, true, true, array(""=>"..."));

// Set the url from where we obtain the data
$grid->setUrl('ProjectProgressSubGrid.php');

// Set some grid options
$grid->setGridOptions(array(
    "width"=>1400,
    "rowNum"=>10,
    "sortname"=>"DateEntry",
    "height"=>110,
    "postData"=>array("ProjectProgressSubGrid"=>$subtable,"rowid"=>$projectCode))
);

//enable toolbarsearch
$grid->toolbarfilter = true;

// add navigator with the default properties
$grid->navigator = true;
$grid->setNavOptions('navigator',array('search'=>false));

$grid->setNavOptions('edit',array("closeAfterEdit"=>true,"editCaption"=>"Edit","bSubmit"=>"Update","width"=>500,"height"=>300));


$subtable = $subtable."_t";
$pager = $subtable."_p";

$grid->renderGrid($subtable,$pager, true, null, array(&$rowid), true,true);


//echo '<p>$rowid = '.$rowid.'</p>';
//echo '<p>$projectCode = '.$projectCode.'</p>';
//echo '<p>$materialBudget = '.$materialBudget.'</p>';
//echo '<p>$labourBudget = '.$labourBudget.'</p>';
//echo '<p>SELECT SUM(OrderNo_Total_Cost) FROM OrderNo WHERE OrderNo_Project = '.$projectCode.'</p>';

$conn = null;
?>