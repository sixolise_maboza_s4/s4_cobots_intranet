<?php
  //////////////////////////////////////////////////////////////////////////////
  // This page allows users to manage and view internal orders.               //
  //////////////////////////////////////////////////////////////////////////////
  
  include 'Scripts/Include.php';
  SetSettings();
  CheckAuthorisation('InternalOrders.php');
  
  //////////////////////////////////////////////////////////////////////////////
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3c.org/TR/1999/REC-html401-19991224/loose.dtd">
<HTML>
  <HEAD>
      <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <?php
      // PHP SCRIPT ////////////////////////////////////////////////////////////
      BuildHead('Internal Orders');
      //////////////////////////////////////////////////////////////////////////
    ///
    include('Scripts/header.php');
    ?>
      <script>
  	$(document).ready(function() {
   
             
		$(".datepicker").datepicker({ 
			dateFormat: 'yy-mm-dd', 
			showOn: "button",
			buttonImage: "Images/calendar.jpg",
			buttonImageOnly: true
		});    
 	});
	</script>
       <script type='text/javascript'>
	  		
        var pasteddata = "";
        function handlepaste(elem, e) {
                var savedcontent = elem.innerHTML;

                if (e && e.clipboardData && e.clipboardData.getData) {// Webkit - get data from clipboard, put into editdiv, cleanup, then cancel event
                        elem.innerHTML = e.clipboardData.getData('text/plain');

                        waitforpastedata(elem, savedcontent);
                        if (e.preventDefault) {
                                        e.stopPropagation();
                                        e.preventDefault();
                        }
                        return false;
                }
                else {// Everything else - empty editdiv and allow browser to paste content into it, then cleanup
                        elem.innerHTML = "";
                        waitforpastedata(elem, savedcontent);
                        return true;
                }
        }

        function waitforpastedata (elem, savedcontent) {
                if (elem.childNodes && elem.childNodes.length > 0) {
                        processpaste(elem, savedcontent);
                }
                else {
                        that = {
                                e: elem,
                                s: savedcontent
                        }
                        that.callself = function () {
                                waitforpastedata(that.e, that.s)
                        }
                        setTimeout(that.callself,20);
                }
        }

        function processpaste (elem, savedcontent) {
                //Call ajax, store in session variable
                pasteddata = elem.innerHTML;
                elem.innerHTML = savedcontent;

                var request = $.ajax({
                        url: "clipboardInjectorInternalOrders.php",
                        type: "post",
                        data: "clipboardDataInternalOrders=" + pasteddata

                });

                request.done(function(response)
                        {
                                var c = confirm(response);
                                if (c === true)
                                {
                                        //Trigger the add button
                                        $('#AddRowButton').click();
                                }
                                else
                                {
                                        //Prepare another AJAX connection to clear the session
                                        var request2 = $.ajax({
                                                url: "clipboardClearInternalOrders.php",
                                                type: "get",
                                                data: "command=clear"
                                        });

                                        request2.done( function(){
                                                alert("Clipboard information successfully removed.");
                                        });
                                }
                        }

                );

                request.fail(function (jqXHR, textStatus, errorThrown)
                        {
                                // log the error to the console
                                console.error("The following error occured: " + textStatus, errorThrown);
                        }
    );

        }
        </script>
      
        <script src="Scripts/jquery.cookie.js" type="text/javascript"></script>
        <script src="Scripts/picnet.table.filter.min.js" type="text/javascript"></script>
        <script>
        $(document).ready(function() {
	    $.getScript('Scripts/CustomTableFilter.js');
 	});
        </script>
        
        <STYLE type="text/css">
          
            
            #lableMark{
                margin-top: 3px;
                padding-right: 6px;
            }
            
            #ERRORBOX{
                display:none;
            }
            
            #box{
                width: 99%;
            }
            
            #MarkData{
                display: none;
            }
            
            .bottomButtons{
               text-align: right;
               margin: auto;
            }
            
            .highlight{
                  background-color: rgba(255, 122, 0, 0.61);
              }  
            
            .blurout{
                opacity: 0.2;
                pointer-events: none;
            }
            
            #buildAuto{
                color: red;
                float: right;
                
            }
            .dx-widget, .dx-widget input, .dx-widget textarea {
                font-family: Arial, Sans-Serif !important;
                font-size: 10pt !important;
                font-weight: normal !important;
                color: #000000 !important;
            }
            
             <?php
             if (isset($_SESSION['AddInternalOrder']) || isset($_SESSION['EditInternalOrder'])){
             
                echo'.dx-overlay-content.dx-popup-normal.dx-popup-draggable.dx-resizable {
                     width: 700px !important;
                }

                .dx-overlay-content.dx-popup-normal.dx-popup-draggable.dx-resizable.dx-dropdowneditor-overlay-flipped {
                    width: 700px !important;
                  }';
            }
           ?>
            
            
/*            .dx-overlay-content.dx-popup-normal.dx-popup-draggable.dx-resizable {
                 width: 700px !important;
            }
            
            .dx-overlay-content.dx-popup-normal.dx-popup-draggable.dx-resizable.dx-dropdowneditor-overlay-flipped {
                width: 700px !important;
             }
            */
            
            .autocomplete {
                border-color: darkgrey !important;
            }
            .dx-item.dx-list-item { 
                border-top:1px solid #aaa !important; 
            }
            
            .autocomplete{
                margin: 10px;
            }
            
            .pager{
                width: 100%;
                padding-bottom: 3em;
            }
            .dx-datagrid-rowsview.dx-datagrid-nowrap.dx-scrollable.dx-scrollable-customizable-scrollbars.dx-scrollable-both.dx-scrollable-simulated.dx-visibility-change-handler {
            height: auto !important;
            }
            
            
            #gridContainerInternalOrderList {
                width: 100%;
            }
            .internal-grid-container {
                padding: 10px 0 10px 0;
            }

            .internal-grid-container > div:first-child {
                padding: 0 0 5px 10px;
                font-size: 14px;
                font-weight: bold;
            }

            .internal-grid-container > .internal-grid {
                border: 1px solid #d2d2d2;
            }   
            
            .white{
                color: white;
                font-size: 12px;
            }
            
            .size{
               font-size: 12px;
            }
            .Hide{
                display: none;
            }
            
            .loader,
            .loader:after {
              border-radius: 50%;
              width: 10em;
              height: 10em;
            }
           .loader {
                font-size: 10px;
                text-indent: -9999em;
                border-top: 1.1em solid rgba(41,88,133, 0.2);
                border-right: 1.1em solid rgba(41,88,133, 0.2);
                border-bottom: 1.1em solid rgba(41,88,133, 0.2);
                border-left: 1.1em solid #295885;
                -webkit-animation: load8 1.1s infinite linear;
                animation: load8 1.1s infinite linear;
                position: fixed;
                left: 50%;
                top: 50%;
                z-index: 9;
                transform: translate(-50%, -50%);
                -o-transform: translate(-50%, -50%);
                -moz-transform: translate(-50%, -50%);
                -webkit-transform: translate(-50%, -50%);
            }
            @-webkit-keyframes load8 {
              0% {
                -webkit-transform: rotate(0deg);
                transform: rotate(0deg);
              }
              100% {
                -webkit-transform: rotate(360deg);
                transform: rotate(360deg);
              }
            }
            @keyframes load8 {
              0% {
                -webkit-transform: rotate(0deg);
                transform: rotate(0deg);
              }
              100% {
                -webkit-transform: rotate(360deg);
                transform: rotate(360deg);
              }
            }
 
            .black {
                color: black;
                font-weight: 400 !important;
            }
            
            /*colors:*/
            .red{
                text-shadow: none !important;
                font-weight: bold;
                background-color: rgba(255, 0, 0, 0.32);
                padding-left: 0.5em;
                padding-right: 0.5em;
                width: 115px;
            }
            
            .green{
                text-shadow: none !important;
                font-weight: bold;
                background-color: rgba(10, 148, 23, 0.32);
                padding-left: 0.5em;
                padding-right: 0.5em;
                width: 115px;
            }
            
            .yellow{
                text-shadow: none !important;
                font-weight: bold;
                background-color: rgba(245, 253, 21, 0.58);
                padding-left: 0.5em;
                padding-right: 0.5em;
                width: 115px;
            }
            
            .blue{
                text-shadow: none !important;
                font-weight: bold;
                background-color:  rgba(10, 115, 254, 0.58);
                padding-left: 0.5em;
                padding-right: 0.5em;
                width: 115px;
            }
            
            #coverAll{
                    position: fixed;
                    top: 0;
                    left: 0;
                    background-color: rgba(0, 0, 0, 0.38);
                    z-index: 600;
                    width: 100%;
                    height: 100%;
            }
            
            #UncontroledGoodsItems{
                background: white;
                margin: auto;
                width: 50%;
                position: fixed;
                max-height: 50%;
                top: 25%;
                left: 25%;
                z-index: 700;
                overflow: overlay;
            }
            
            #phead{
                background-color: #295885;
                font-size: large;
                font-weight: 700;
                padding: 0.5em;
                color: white;
                
            }
            
            #pfoot{
           
                bottom: 0;
                text-align: right;
                padding-top: 0.5em;
                padding-bottom: 0.5em;
            }
           
            </STYLE>


  </HEAD>
  <BODY>
     
    <DIV id ="coverAll" class='Hide'></DIV>  
    <?php
      // PHP SCRIPT ////////////////////////////////////////////////////////////
      BuildBanner();
      //////////////////////////////////////////////////////////////////////////
    ?>
    <DIV id="main">


      <?php
      BuildTopBar();
        // PHP SCRIPT //////////////////////////////////////////////////////////
        BuildMenu('Main', 'InternalOrders.php');
        ////////////////////////////////////////////////////////////////////////
      ?>
        <section id="content_wrapper">

            <?php
            $current = "Internal orders";
            BuildBreadCrumb($current);?>
            <!-- -------------- Content -------------- -->
            <section id="content" class="table-layout">
                <!-- -------------- Column Center -------------- -->
                <div class="chute chute-center" style="height: 869px;">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel">
        <?php
          // PHP SCRIPT ////////////////////////////////////////////////////////
          if ($_GET['id'])
          {
            $id = SubStr($_GET['id'], 5, StrLen($_GET['id']) - 5);
            $resultSet = ExecuteQuery('SELECT * FROM OrderNo WHERE OrderNo_Number = "'.$id.'" AND OrderNo_Approved = "0"');
            if (MySQL_Num_Rows($resultSet) > 0)
            {
                $row = MySQL_Fetch_Array($resultSet);
                $resultSetCM = MySQL_Fetch_Array(ExecuteQuery('SELECT Project_Responsible,Project_CostManager FROM Project WHERE Project_Code = "'.$row['OrderNo_Project'].'"'));
              if ((($_SESSION['cAuth'] & 32) || ($resultSetCM['Project_CostManager'] == $_SESSION['cUID']) || ($resultSetCM['Project_CostManager'] == $_SESSION['cUID']) || ($_SESSION['cUID'] == 30) || ($_SESSION['cUID'] == 310))) //auth level or Stefan Buys or Shawn Browne
              {
                switch ($_GET['type'])
                {
                  case 'approve':
                    $_SESSION['ApproveInternalOrder'] = array($id);
                    $_SESSION['ApproveInternalOrder'][1] = $_SESSION['cUID'];
                    $_SESSION['ApproveInternalOrder'][3] = 2;
                    break;
                  default:
                    break;
                }
              }
            }
          }
          
          BuildMessageSet('InternalOrder');
          //////////////////////////////////////////////////////////////////////
        ?>
        <?php
          // PHP SCRIPT ////////////////////////////////////////////////////////
          $infoBox = '<BR />
                        <DIV class="infobox">
                            <DIV class="head">info</DIV>
                            <DIV class="body"><STRONG>Storage Type : </STRONG><BR></BR>
                                This is the summary on how to select the tracking method.<BR></BR>
                                <UL>
                                <LI><STRONG>TRA - </STRONG>Goods barcoded.<BR></BR> 
                                     <ul><li>Project goods worth > R 500.00 ea. (always stored in secure store).</li></ul></LI><BR></BR>
                                <LI><STRONG>RES - </STRONG>Goods barcoded and storeman notified to barcode box, not individual contents. 
                                      <BR></BR><ul><li>Resale goods worth > R 500.00 ea. to be resold as-is (always stored in secure store)</li></ul></LI><BR></BR>
                                <LI><STRONG>UNT - </STRONG>Goods not barcoded.<BR></BR>
                                      <ul><li>Project goods worth < R 500.00 ea. (always stored in secure store)</li>
                                          <li>Resale goods worth < R 500.00 ea. to be resold as-is (always stored in secure store)</li>
                                          <li>All manufactured goods (always stored in unsecured store)</li>
                                          </ul></LI><BR></BR>
                                <LI><STRONG>CONS - </STRONG>Consumable Goods not barcoded.<BR></BR>
                                    <ul><li>Consumable goods always stored in Non-project store.</li></ul></LI><BR></BR>
                                </UL>
                            </DIV>
                        </DIV>';
            
          if (isset($_SESSION['AddInternalOrder'])){
            BuildContentHeader('Add Internal Order', "", "", true);
            echo '<DIV class="contentflow">
                    <P>Enter the details of the internal order below. Ensure that all the required information is entered before submission and that this information is valid. Note that there must be at least one item listed before an order can be submitted. When you submit this order admin will be notified and the order will be sent for approval.</P>
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center" class="longer">
                      <FORM method="post" action="Handlers/InternalOrders_Handler.php">
                        <INPUT name="Type" type="hidden" value="Add">
                        <TR>
                          <TD colspan="4" class="header">Internal Order Details
                          </TD>
                        </TR>
                        <DIV class ="Hide" id = "UncontroledGoodsItems" ><div id = "phead">Database & Controlled Goods Item Check</div><div id ="items" style="overflow:scroll;"></div><div id="pfoot"><INPUT tabindex="14" id="sendEditRequest" name="Submit" type="submit" class="button" value="Submit" /><input type="button" value="Cancel" class = "button"  style = "margin-left: 0.3em; margin-right: 2.5em;" id = "cancleUncontroledItems"></div></DIV>
                        <TR>
                          <TD class="short">Requested By:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                            BuildStaffSelector(1, 'Requested', 'standard', $_SESSION['AddInternalOrder'][0], true);
                    echo '</TD>
                          <TD class="short">Comments:
                          </TD>
                          <TD rowspan="4">
                            <TEXTAREA tabindex="9" name="Comments" class="standard" maxlength="200">'.$_SESSION['AddInternalOrder'][6].'</TEXTAREA>
                          </TD>
                        </TR>
                        <TR>
                          <TD>Supplier:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD><span id ="buildAuto"></span>';         
                    
                        $Restricted = false;
                        $restrict = "";

                        $resultSet = ExecuteQuery('SELECT Supplier_Code, Supplier_Name FROM Supplier WHERE  Supplier_IsInactive = 0 AND 1=1'.$restrict.' ORDER BY Supplier_Name');
                        echo '<SELECT tabindex="2" name="Supplier" class="seltest" id="SupplierSelect">                                                                   
                                <OPTION value="">< ... ></OPTION>';                  
                        while ($row = MySQL_Fetch_Array($resultSet))
                        { 
                          if ($row['Supplier_Code'] == $_SESSION['AddInternalOrder'][1])
                            echo '<OPTION value="'.iconv('Windows-1252', 'UTF-8//TRANSLIT', $row['Supplier_Code']).'" SELECTED>'.iconv('Windows-1252', 'UTF-8//TRANSLIT', $row['Supplier_Name']).'</OPTION>';
                          else
                            echo '<OPTION value="'.iconv('Windows-1252', 'UTF-8//TRANSLIT', $row['Supplier_Code']).'">'.iconv('Windows-1252', 'UTF-8//TRANSLIT', $row['Supplier_Name']).'</OPTION>';
                        }         
                        echo '</SELECT>';
                          
                    echo '</TD>
                        </TR>
                        

                    <TR>
                          <TD>BEE level:</TD>
                          <TD>
                          <div id = "bee" class = "red">non-compliant</div>
                           </TD>
                    </TR>

                        <TR>
                          <TD>Quotation number:
                            <SPAN class="note">
                            </SPAN>
                          </TD>
                          <TD>';
                             BuildTextBox(2, 'quotation_number', 'text', 'standard', 100, $_SESSION['AddInternalOrder'][19]);
     
                    echo '</TD>
                        </TR>
                        <TR>
                          <TD>Project:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                            BuildProjectSelector(3, 'Project', 'seltest', $_SESSION['AddInternalOrder'][2], 'Open', true, true, true, false);
                    echo '</TD>
                        </TR>
                        <TR>
                          <TD>Order Type:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                            BuildInternalOrderTypeSelector(4, 'OrderType', 'standard', $_SESSION['AddInternalOrder'][3]);
                    echo '</TD>
                        </TR>
                        <TR>
                          <TD>Total (VAT excl.):
                            <SPAN class="note">**
                            </SPAN>
                          </TD>
                          <TD class="bold">';
                            BuildCurrencySelector(5, 'CostCurrency', 'veryshort',iconv('Windows-1252', 'UTF-8//TRANSLIT', $_SESSION['AddInternalOrder'][4]) );
                      echo ' '.SPrintF('%02.2f', $_SESSION['AddInternalOrder'][5]).'
                            <INPUT name="Cost" type="hidden" value="'.$_SESSION['AddInternalOrder'][5].'">
                          </TD>
                          <TD>Delivery Method:
                          </TD>
                          <TD>';
                            BuildDeliveryMethodSelector(10, 'Delivery', 'standard', $_SESSION['AddInternalOrder'][7]);
                    echo '</TD>
                        </TR>
                        <TR>
                          <TD>Follow Up Date:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD><input type="text" size="14" class="datepicker" value ="'.$_SESSION['AddInternalOrder'][13].'" name="FollowDate"/> ';
                    echo '</TD>
                          <TD>Delivery Date:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD><input type="text" size="14" class="datepicker"  value ="'.$_SESSION['AddInternalOrder'][14].'"   name="DeliveryDate"/>';

                    echo '</TD>
                        </TR>
                       <TR>
                            <TD>Category:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                               BuildReportsCategorySelector(16, 'ReportCategory', 'standard', $_SESSION['AddInternalOrder'][18]);                    echo '</TD>
                        </TR>
                        <TR>
                          
                          <TD>';
                               $_SESSION['AddInternalOrder'][20];
                        echo '</TD>
                        </TR>                        
                        <TR>
                          <TD colspan="4" class="center">';
        //on live server                
        if ( (($_SESSION['cAuth'] & 32)  || ($_SESSION['cUID'] == 30)) &&  ($_SESSION['cUID'] !=282)) //auth level or Stefan Buys
                        echo '<INPUT  name="Submit" type="submit" class="button" value="AutoApprove" />';
                        $add = "'add'";
                        echo'<input tabindex="14" type="button" id = "openUncontrolledgoods" onclick="OpenUncontolledPopup('.$add.')" value="Submit" class="button"> 
                            <INPUT tabindex="15" name="Submit" type="submit" class="button" value="Cancel" />              
                          </TD>
                        </TR>
                      </TABLE> 
                       <BR /><BR />';
		
        //Now add some div which can contain information from excel or clipboard
					  echo '<div id="clipboardDiv" contenteditable="true" onpaste="handlepaste(this, event)">Paste Excel here (manufacturer code, manufacturer description, backorder, value, quantity, asset, storage type,discount)</div>';
                      echo '<TABLE id = "Items_AddIternalOrder" cellspacing="5" align="center" class="longer blurout">
                        <TR>
                          <TD colspan="11" class="header">Item Details
                          </TD>
                        </TR>
                        <TR>
                          <TD class="subheader">Manufacturer Code
                          </TD>
                          <TD class="subheader">Manufacturer Description
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD class="subheader">Backorder
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD class="subheader">Value (VAT excl.)
                            <SPAN class="note">**
                            </SPAN>
                          </TD>
                          <TD class="subheader">Quantity
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD class="subheader">Asset
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD class="subheader">Storage Type
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD class="subheader veryshort">Discount(%)
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD class="subheader">Total
                          </TD>
                        </TR>
                        <TR>
                          <TD class="center test">
                            <INPUT tabindex="16" name="SupplierCode" type="text" class="text veryshort Hide" maxlength="100" value="'.$_SESSION['AddInternalOrder'][8].'" />
                            <div id = "SupplierCode" class="autocomplete"></div> 
                          </TD>
                          <TD class="center testing">
                          <INPUT tabindex="17" name="Description" type="text" class="text standard" maxlength="100" value="'.$_SESSION['AddInternalOrder'][9].'" />         
                          </TD>
                          <TD class="center">';
                            BuildYesNoSelector(18, 'Backorder', 'veryshort', $_SESSION['AddInternalOrder'][10]);
                    echo '</TD>
                          <TD class="center">
                            <INPUT tabindex="19" name="Value" type="text" class="text veryshort" maxlength="13" value="'.$_SESSION['AddInternalOrder'][11].'" />
                          </TD>
                          <TD class="center">
                            <INPUT tabindex="20" name="Quantity" type="text" class="text veryshort" maxlength="11" value="'.$_SESSION['AddInternalOrder'][12].'" />
                          </TD>
                          <TD class="center">';
                            BuildYesNoSelector(21, 'Asset', 'veryshort', $_SESSION['AddInternalOrder'][15]);
                    echo '</TD>
                         <TD class="center">';
                            BuildStorageTypeSelector(21, 'StorageType', 'veryshort StorageADDF', $_SESSION['AddInternalOrder'][16]);
                    echo '</TD>
                          <TD class="center">';
                                    if($_SESSION['AddInternalOrder'][17] !="")$DiscountValue = $_SESSION['AddInternalOrder'][17];
                                    else $DiscountValue=0;
                            echo'<INPUT tabindex="21" name="Discount" type="text" class="text veryshort" maxlength="5" value="'.$DiscountValue.'" />
                          </TD>
                          <TD class="center">-
                          </TD>
                          <TD class="center">
                            <INPUT id="AddRowButton" tabindex="22" name="Submit" type="submit" style="width: 54px; height: 21px; font-size: 8pt; font-weight: bold;" value="Add" />
                          </TD>
                        </TR>';
                        $count = 20;
                        $countA = 0;
                        while ($count <= SizeOf($_SESSION['AddInternalOrder']))
                        {
                          if ($_SESSION['AddInternalOrder'][$count] != "")
                          {
                            if ($countA % 2 != 0)
                              $colour = 'rowA';
                            else
                              $colour = 'rowB';
                            
                            echo '<TR>                                  
                                    <TD class="'.$colour.' center test">
                                      <INPUT tabindex="'.($count + 8).'" name="SupplierCode'.$count.'" type="text" class="text veryshort Hide" maxlength="100" value="'.$_SESSION['AddInternalOrder'][$count][0].'" />
                                      <div id = "SupplierCode'.$count.'" class="autocomplete"></div>  
                                    </TD>
                                    <TD class="'.$colour.' testing">
                                      <INPUT tabindex="'.($count + 8).'" name="Description'.$count.'" type="text" class="text standard" maxlength="100" value="'.$_SESSION['AddInternalOrder'][$count][1].'" />     
                                    </TD>
                                    <TD class="'.$colour.' center">';
                                      BuildYesNoSelector($count + 8, 'Backorder'.$count, 'veryshort', $_SESSION['AddInternalOrder'][$count][4]);
                              echo '</TD>
                                    <TD class="'.$colour.' center">
                                      <INPUT tabindex="'.($count + 8).'" name="Value'.$count.'" type="text" class="text veryshort" maxlength="13" value="'.SPrintF('%02.2f', $_SESSION['AddInternalOrder'][$count][2]).'" />
                                    </TD>
                                    <TD class="'.$colour.' center">
                                      <INPUT tabindex="'.($count + 8).'" name="Quantity'.$count.'" type="text" class="text veryshort" maxlength="11" value="'.$_SESSION['AddInternalOrder'][$count][3].'" />
                                    </TD>
                                    <TD class="'.$colour.' center">';
                                      BuildYesNoSelector($count + 8, 'Asset'.$count, 'veryshort', $_SESSION['AddInternalOrder'][$count][5]);
                              echo '</TD>
                                    <TD class="'.$colour.' center">';
                
                                      BuildStorageTypeSelector($count + 8, 'StorageType'.$count, 'veryshort StorageADD', $_SESSION['AddInternalOrder'][$count][6]);
                              echo '</TD>
                                    <TD class="'.$colour.' center">
                                      <INPUT tabindex="'.($count + 5).'" name="Discount'.$count.'" type="text" class="text veryshort" maxlength="5" value="'.$_SESSION['AddInternalOrder'][$count][7].'" />
                                    </TD>';
                                       $originalValue = SPrintF('%02.2f', $_SESSION['AddInternalOrder'][$count][2]*$_SESSION['AddInternalOrder'][$count][3]);
                                           if($_SESSION['AddInternalOrder'][$count][7] > 0)
                                               $originalValue -= SPrintF('%02.2f',(($_SESSION['AddInternalOrder'][$count][7]/100)*$originalValue));
                               echo'<TD class="'.$colour.' center">'.SPrintF('%02.2f',$originalValue ).'
                                    </TD>
                                    <TD class="center">
                                      <INPUT tabindex="'.($count + 8).'" name="Remove'.$count.'" type="submit" style="width: 54px; height: 21px; font-size: 8pt; font-weight: bold;" value="Remove" />
                                    </TD>
                                    <TD class="center">
                                      <INPUT tabindex="'.($count + 8).'" name="Update'.$count.'" type="submit" style="width: 54px; height: 21px; font-size: 8pt; font-weight: bold;" value="Update" />
                                    </TD>
                                  </TR>';
                            $countA++;
                          }
                          $count++;
                        }
                echo '</TABLE> 
                    </FORM>  
                  </DIV>  
                  <DIV>
                    <BR /><BR />
                    <SPAN class="note">*
                    </SPAN>
                    These fields are required.
                    <BR />
                    <SPAN class="note">**
                    </SPAN>
                    Make sure that all values entered are in the same currency.
                  </DIV>';
                echo $infoBox;
          } else
          if (isset($_SESSION['ApproveInternalOrder']))
          {
            BuildContentHeader('Approve Internal Order - '.$_SESSION['ApproveInternalOrder'][0], "", "", true);
            $row = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM OrderNo WHERE OrderNo_Number = "'.$_SESSION['ApproveInternalOrder'][0].'"'));
            $loRowCurrency = MySQL_Fetch_Array(ExecuteQuery('SELECT C.Currency_Symbol FROM Currency C WHERE (C.Currency_ID = "'.$row['OrderNo_Original_Currency'].'")'));
            $loCurrencySymbol = $loRowCurrency['Currency_Symbol'];
          
            echo '<DIV class="contentflow">
                    <P>These are the internal order details.</P>
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center" class="long">
                      <TR>
                        <TD colspan="4" class="header">Internal Order Details
                        </TD>
                      </TR>
                      <TR>
                        <TD class="short">Requested By:
                        </TD>
                        <TD class="bold">';
                          $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = '.$row['OrderNo_Requested'].''));        		    
                          echo $rowTemp['Staff_First_Name'].' '.$rowTemp['Staff_Last_Name'].'
                        </TD>
                        <TD class="short vtop">Comments:
                        </TD>
                        <TD rowspan="4" class="short" class="vtop bold">'.$row['OrderNo_Comments'].'
                        </TD>
                      </TR>
                      <TR>
                        <TD>Supplier:
                        </TD>
                        <TD class="bold">';
                          $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Supplier WHERE  Supplier_IsInactive = 0 AND Supplier_Code = '.$row['OrderNo_Supplier'].''));
                          echo $rowTemp['Supplier_Name'].'
                        </TD>
                      </TR>
                     
                     <TR>
                        <TD>BEE Level:
                        </TD>
                        <TD class="bold">';
                           $rowTemp2 = MySQL_Fetch_Array(ExecuteQuery('SELECT `Level` FROM BEE_Certificate WHERE Supplier_Code ='.$rowTemp['Supplier_Code']));     		                 
                           $value = $rowTemp2['Level'];
                           if($value==null){
                                $value = '<span class ="red"">non-compliant</span>'; 
                           } 
                           else if($value=="0"){
                                $value = '<span class ="red">non-compliant</span>';
                           }
                           
                          else if($value=="9"){
                                  $value = '<span class ="green">N/A - International</span>'; 
                           }
                          else if($value=="10"){
                                  $value = '<span class ="blue">Awaiting feedback</span>'; 
                           }
                           else if($value==="1" || $value=="2" || $value=="3" || $value ==="4"  || $value === "5" ){
                                 $value = '<span class ="green">'.$value.'</span>'; 
                           }
                           
                           else {
                                $value = '<span class="yellow" >'.$value.'</span>';  
                           }
                           
                           
                           echo $value;
                       echo' </TD>
                      </TR>';
                      
                    $rowTemp =  MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM OrderNo WHERE  OrderNo_Number = '.$row['OrderNo_Number'].''));     
                    if($rowTemp['quotation_number']!=""){          
                    echo  '<TR>
                        <TD>Quotation number:
                        </TD>
                        <TD class="bold">';       		    
                          echo $rowTemp['quotation_number'].'
                        </TD>
                      </TR>';
                      }         
                                  
                    echo'<TR>
                        <TD>Project:
                        </TD>
                        <TD class="bold">';
                          $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Project WHERE Project_Code = '.$row['OrderNo_Project'].''));        		    
                          echo $rowTemp['Project_Pastel_Prefix'].' - '.$rowTemp['Project_Description'].'
                        </TD>
                      </TR>
                      <TR>
                        <TD>Cost Manager:
                        </TD>
                        <TD  colspan="3" class="bold">';
                          
                             $costmanagerCategory = $row['OrderNo_ReportCategory'];
                             if( strcmp($row['OrderNo_ReportCategory'], "0") == 0){
                                 $costmanagerCategory = "";
                             }
                          
                          $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT Staff_First_Name,Staff_Last_Name FROM Staff st,Project pr,OrderNo ord WHERE st.Staff_Code = pr.Project_CostManager'.$costmanagerCategory.' AND pr.Project_Code = ord.OrderNo_Project AND ord.OrderNo_Number = "'.$row['OrderNo_Number'].'"'));        		    
                          echo $rowTemp['Staff_First_Name'].' '.$rowTemp['Staff_Last_Name'].'
                        </TD>
                      </TR>
                      <TR>
                        <TD>Order Type:
                        </TD>
                        <TD colspan="3" class="bold">';
                          $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM OrderType WHERE OrderType_ID = '.$row['OrderNo_Type'].''));        		    
                          echo $rowTemp['OrderType_Description'].'
                        </TD>
                      </TR>
                      <TR>
                        <TD>Total (VAT excl.):
                        </TD>
                        <TD class="bold">'.iconv('Windows-1252', 'UTF-8//TRANSLIT', $loCurrencySymbol.SPrintF('%02.2f', $row['OrderNo_Original_Total_Cost'])).'
                        </TD>
                        <TD>Delivery Method:
                        </TD>
                        <TD class="bold">';
                          switch ($row['OrderNo_Delivery'])
                          {
                            case '0':
                              echo 'Collect';
                              break;
                            case '1':
                              echo 'Delivery'; 
                              break;  
                            case '2':
                              echo 'Courier';
                              break;
                            default:
                              break;
                          }               
                  echo '</TD>
                      </TR>
                      <TR>
                        <TD>Follow Up Date:
                        </TD>
                        <TD class="bold">'.GetTextualDateFromDatabaseDate($row['OrderNo_Date_FollowUp']).'
                        </TD>
                        <TD>Delivery Date:
                        </TD>
                        <TD class="bold">'.GetTextualDateFromDatabaseDate($row['OrderNo_Date_Delivery']).'
                        </TD>
                      </TR>
                      <TR>
                        <TD>Date Logged:
                        </TD>
                        <TD colspan="3" class="bold">'.GetTextualDateTimeFromDatabaseDateTime($row['OrderNo_Date_Time']).'
                        </TD>
                      </TR>
<TR></TR>
                      <TR>
                        <TD colspan="4" class="header">Additional Details
                        </TD>
                      </TR>
                      <TR>
                        <TD>Supplier Invoice:
                        </TD>
                        <TD class="bold">';
                          $resultSet = ExecuteQuery('SELECT * FROM SupplierInvoice WHERE SupplierInvoice_S4OrderNo = "'.$row['OrderNo_Number'].'" ORDER BY SupplierInvoice_Number ASC');        		    
                          while ($rowTemp = MySQL_Fetch_Array($resultSet))
                          {
                            echo $rowTemp['SupplierInvoice_Number'].'; ';
                          }
                  echo '</TD>
                        <TD>Order Status:
                        </TD>
                        <TD class="bold">';
                          if ($row['OrderNo_Complete'] == '1')
                            echo 'Complete';
                          else
                            if ($row['OrderNo_Complete'] == '2')
                              echo 'Cancelled';
                            else
                              echo 'Incomplete';
                  echo '</TD>
                      </TR>
                    </TABLE>'; 
                    $resultSet = ExecuteQuery('SELECT * FROM Items WHERE Items_Order_Number = "'.$row['OrderNo_Number'].'"');        		    
                    if (MySQL_Num_Rows($resultSet) > 0)
                    {  
                      echo '<BR /><BR />
                            <TABLE cellspacing="5" align="center" class="long">
                              <TR>
                                <TD colspan="12" class="header">Item Details </br> Items highlighted in Orange may be in the uncontrolled goods area of the store 
                                </TD>
                              </TR>
                              <TR>
                                <TD class="subheader">Manufacturer Code
                                </TD>
                                <TD class="subheader">Manufacturer Description
                                </TD>
                                <TD class="subheader">Backorder
                                </TD>
                                <TD class="subheader">Original Value (VAT excl.)
                                </TD>
                                <TD class="subheader">Nett Value(Discount Incl.)
                                </TD>
                                <TD class="subheader">Quantity
                                </TD>
                                <TD class="subheader">Discount
                                </TD>
                                <TD class="subheader">Asset
                                </TD>
                                <TD class="subheader">Invoice Capture
                                </TD>
                                <TD class="subheader">Storeman Received
                                </TD>
                                <TD class="subheader">Storage Type
                                </TD>
                                 <TD class="subheader">Quantity in Storage
                                </TD>
                              </TR>';
                              $count = 0;
                              $resultSet = ExecuteQuery('SELECT * FROM Items WHERE Items_Order_Number = "'.$row['OrderNo_Number'].'"');        		    
                            
                            $Uncontrolledgoods = array();
                           
                            $Resultlist = ExecuteQuery('SELECT result.Items_Supplier_Code , count(result.Items_Supplier_Code) as Quantity FROM ((SELECT i.Items_Supplier_Code FROM `S4Admin`.Items i LEFT JOIN stock_control.items_barcode ib ON (i.Items_ID = ib.items_id) LEFT JOIN S4Admin.OrderNo o on o.OrderNo_Number = i.Items_Order_Number LEFT JOIN S4Admin.Supplier s  on o.OrderNo_Supplier = s.Supplier_Code WHERE ib.items_barcode_id in (SELECT items_barcode_id FROM stock_control.items_barcode WHERE shelf_id = 153 AND db = "s4")) UNION ALL (SELECT i.Items_Supplier_Code FROM `CobotsAdmin`.Items i LEFT JOIN stock_control.items_barcode ib ON (i.Items_ID = ib.items_id) LEFT JOIN CobotsAdmin.OrderNo o on o.OrderNo_Number = i.Items_Order_Number LEFT JOIN CobotsAdmin.Supplier s  on o.OrderNo_Supplier = s.Supplier_Code WHERE ib.items_barcode_id in (SELECT items_barcode_id FROM stock_control.items_barcode WHERE shelf_id = 153 AND db = "mm")) UNION ALL (SELECT i.part_number as `Items_Supplier_Code` FROM `stock_control`.no_order_items i LEFT JOIN stock_control.items_barcode ib ON (i.Items_ID = ib.items_id) WHERE ib.items_barcode_id in (SELECT items_barcode_id FROM stock_control.items_barcode WHERE shelf_id = 153 AND db = "no"))) result GROUP BY result.Items_Supplier_Code ORDER BY Quantity Desc');
                            while ($row = MySQL_Fetch_Array($Resultlist)) {
                                  $Uncontrolledgoods[strtolower($row['Items_Supplier_Code'])] = $row['Quantity'];   
                            }
                              
                              while ($rowTempB = MySQL_Fetch_Array($resultSet))
                              {                        
                                if ($count % 2 == 0)
                                  $colour = 'rowA';
                                else
                                  $colour = 'rowB';
                               
                                
                                $UncontrolledQuantity1 = 0;

                                if($rowTempB['Items_Supplier_Code'] !== "" && $rowTempB['Items_Supplier_Code'] != null){
                                   // print_r('got on sp');
                                    if (array_key_exists(strtolower($rowTempB['Items_Supplier_Code']),$Uncontrolledgoods))
                                     {

                                       $colour = 'highlight';
                                       $UncontrolledQuantity1 = $Uncontrolledgoods[strtolower($rowTempB['Items_Supplier_Code'])];

                                    }
                                }
                                else{

                                    $length = 0;

                                    foreach ($Uncontrolledgoods as $code => $CurUncontrolledQuantity){  
                                        if($code!=""){
                                            $c = '/'.$code.'/';
                                            if(preg_match($c,strtolower($rowTempB['Items_Description']))){
                                                $colour = 'highlight';
                                                  if(strlen($code) > $length){
                                                    $length = strlen($code);
                                                    $UncontrolledQuantity1 = $CurUncontrolledQuantity;
                                                  }
                                                

                                        } 
                                        }
                                    }        
                                }
   
                                $nett_value = $rowTempB['Items_Original_Value'];
                                if($rowTempB['Items_Discount'] > 0)
                                    $nett_value -= ($rowTempB['Items_Discount']/100) * $nett_value;

                                echo '<TR>                                  
                                        <TD class="'.$colour.' center bold">'.$rowTempB['Items_Supplier_Code'].'
                                        </TD>
                                        <TD class="'.$colour.' bold">'.$rowTempB['Items_Description'].'
                                        </TD>
                                        <TD class="'.$colour.' center bold">'.
                                          (($rowTempB['Items_Backorder'] == '1') ? 'Yes' : 'No').'
                                        </TD>
                                        <TD class="'.$colour.' center bold">'.iconv('Windows-1252', 'UTF-8//TRANSLIT', $loCurrencySymbol.SPrintF('%02.2f', $rowTempB['Items_Original_Value'])).'
                                        </TD>
                                        <TD class="'.$colour.' center bold">'.iconv('Windows-1252', 'UTF-8//TRANSLIT', $loCurrencySymbol.SPrintF('%02.2f', $nett_value)).'
                                        </TD>
                                        <TD class="'.$colour.' center bold">'.$rowTempB['Items_Quantity'].'
                                        </TD>
                                        <TD class="'.$colour.' center bold">'.$rowTempB['Items_Discount']."%".'
                                        </TD>
                                        <TD class="'.$colour.' center bold">'.
                                          (($rowTempB['Items_Asset'] == '1') ? 'Yes' : 'No').'
                                        </TD>
                                        <TD class="'.$colour.' center bold">'.$rowTempB['Items_Received_Quantity'].'
                                        </TD>
                                        <TD class="'.$colour.' center bold">'.$rowTempB['Items_Storeman_Received'].'
                                        </TD>
                                        <TD class="'.$colour.' center bold">'.RetutnStorageTypeDesc($rowTempB['Items_StorageType']).'
                                        </TD>
                                        <TD class="'.$colour.' center bold">'.$UncontrolledQuantity1.'
                                        </TD> 
                                      </TR>';
                                $count++;
                              }
                          echo '</TABLE>';  
                        }
              echo '<BR /><BR />
                    <P>Enter the details of the approval below. Ensure that all the required information is entered before submission and that this information is valid. <B>No details are necessary if denying an order.</B></P>
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center" class="short">
                      <FORM method="post" action="Handlers/InternalOrders_Handler.php">
                        <INPUT name="Type" type="hidden" value="Approve">
                        <TR>
                          <TD colspan="2" class="header">Approval Details
                          </TD>
                        </TR>
                        <TR>
                          <TD class="short">Approved By:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                            BuildStaffSelector(1, 'Approved', 'long', $_SESSION['ApproveInternalOrder'][1], true);
                    echo '</TD>
                        </TR>
                        <TR class= "hide">
                          <TD>Approval Type:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                            BuildApprovalSelector(2, 'ApprovedType', 'long', $_SESSION['ApproveInternalOrder'][3]);
                    echo '</TD>
                        </TR>
                        <TR>
                          <TD class="vtop">Comments:
                          </TD>
                          <TD>
                            <TEXTAREA tabindex="3" name="Comments" class="long" maxlength="100">'.$_SESSION['ApproveInternalOrder'][2].'</TEXTAREA>
                          </TD>
                        </TR>
                        <TR>
                          <TD>Discrepency:
                          </TD>
                          <TD>';
                            BuildOrderDiscrepencyTypeSelector(4, 'Discrepency', 'long', $_SESSION['ApproveInternalOrder'][4]);
                    echo '</TD>
                        </TR>
                        <TR>
                          <TD colspan="2" class="center">
                            <INPUT tabindex="5" name="Submit" type="submit" class="button" value="Approve" />
                            <INPUT tabindex="6" name="Submit" type="submit" class="button" value="Cancel" />
                            <INPUT tabindex="7" name="Submit" type="submit" class="button" value="Deny" />
                          </TD>
                        </TR>
                      </TABLE> 
                    </FORM>  
                  </DIV>  
                  <DIV>
                    <BR /><BR />
                    <SPAN class="note">*
                    </SPAN>
                    These fields are required.
                  </DIV>';
          } else
          if (isset($_SESSION['EditInternalOrder']))
          {
            if ($_SESSION['cAuth'] & 32)
              BuildContentHeader('Edit Internal Order - '.$_SESSION['EditInternalOrder'][7], "", "", true);
            else
              BuildContentHeader('Edit Internal Order', "", "", true);
    
            echo '<DIV class="contentflow">';
                   echo'<P>Once you have made changes, ensure that you submit them otherwise they will not take effect. Note that there must be at least one item listed before an order can be submitted.</P>
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center" class="longer">
                      <FORM method="post" action="Handlers/InternalOrders_Handler.php">
                        <INPUT name="Type" type="hidden" value="Edit">
                        <TR>
                          <TD colspan="4" class="header">Internal Order Details';
                   echo'
                          </TD>
                        </TR>
                        <DIV class ="Hide" id = "UncontroledGoodsItems"><div id = "phead">The following items might be on the uncontrolled goods shelf</div><div id ="items"></div><div id="pfoot"><INPUT tabindex="14" id="sendEditRequest" name="Submit" type="submit" class="button" value="Submit" /><input type="button" value="Cancel" class = "button"  style = "margin-left: 0.3em; margin-right: 2.5em;" id = "cancleUncontroledItems"></div></DIV>
                        <TR>
                          <TD class="short">Requested By:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';
                            BuildStaffSelector(1, 'Requested', 'standard', $_SESSION['EditInternalOrder'][0], true);
                    echo '</TD>
                          <TD class="short">Comments:
                          </TD>
                          <TD rowspan="4">
                            <TEXTAREA tabindex="9" name="Comments" class="standard" maxlength="200">'.$_SESSION['EditInternalOrder'][6].'</TEXTAREA>
                          </TD>
                        </TR>

                        <TR>
                          <TD>Supplier:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';          
                        
                        
                        $Restricted = false;
                        $restrict = "";
                        if ($Restricted)
                          $restrict = ' AND Supplier_Code <> 413';

                        $resultSet = ExecuteQuery('SELECT Supplier_Code, Supplier_Name FROM Supplier WHERE  Supplier_IsInactive = 0 AND 1=1'.$restrict.' ORDER BY Supplier_Name');
                        echo '<SELECT tabindex="2" name="Supplier" class="seltest" id="SupplierSelect2">                                                                   
                                <OPTION value="">< ... ></OPTION>';                  
                        while ($row = MySQL_Fetch_Array($resultSet))
                        { 
                          if ($row['Supplier_Code'] == $_SESSION['EditInternalOrder'][1])
                            echo '<OPTION value="'.$row['Supplier_Code'].'" SELECTED>'.$row['Supplier_Name'].'</OPTION>';
                          else
                            echo '<OPTION value="'.$row['Supplier_Code'].'">'.$row['Supplier_Name'].'</OPTION>';
                        }         
                        echo '</SELECT>';
                           
                           //change to textBox
                    echo '</TD>
                        </TR>
                        

                    <TR>
                          <TD>BEE level:</TD>
                          <TD>
                          <div id = "bee2" class = "red" >non-compliant</div>
                           </TD>
                    </TR>

                           <TR>
                          <TD >Quotation number:
                           
                          </TD>
                          <TD colspan="3">';
                           BuildTextBox(2, 'quotation_number', 'text', 'standard', 100, $_SESSION['EditInternalOrder'][23]);
                    echo '</TD>
                        </TR>
                        <TR>
                          <TD>Project:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD colspan="3">';
                            BuildProjectSelector(3, 'Project', 'seltest', $_SESSION['EditInternalOrder'][2], 'All', true); 
                       echo'</TR>
                        <TR>
                          <TD>Order Type:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD colspan="3">';
                            BuildInternalOrderTypeSelector(4, 'OrderType', 'standard', $_SESSION['EditInternalOrder'][3]);
                    echo '</TD>
                        </TR>
                        <TR>
                          <TD>Total (VAT excl.):
                            <SPAN class="note">**
                            </SPAN>
                          </TD>
                          <TD class="bold">';
                            BuildCurrencySelector(5, 'CostCurrency', 'veryshort', $_SESSION['EditInternalOrder'][4]);
                      echo ' '.SPrintF('%02.2f', $_SESSION['EditInternalOrder'][5]).'
                            <INPUT name="Cost" type="hidden" value="'.$_SESSION['EditInternalOrder'][5].'">
                          </TD>
                          <TD>Delivery Method:
                          </TD>
                          <TD>';
                            BuildDeliveryMethodSelector(10, 'Delivery', 'standard', $_SESSION['EditInternalOrder'][10]);
                    echo '</TD>
                        </TR>
                        <TR>
                          <TD>Follow Up Date:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD><input type="text" size="14" class="datepicker" value ="'.$_SESSION['EditInternalOrder'][16].'" name="FollowDate"/>';
//                            BuildDaySelector(6, 'FollowDay', GetDayFromSessionDate($_SESSION['EditInternalOrder'][16]));
//                            echo '&nbsp;';
//                            BuildMonthSelector(7, 'FollowMonth', GetMonthFromSessionDate($_SESSION['EditInternalOrder'][16]));
//                            echo '&nbsp;';
//                            BuildYearSelector(8, 'FollowYear', GetYearFromSessionDate($_SESSION['EditInternalOrder'][16]));
                    echo '</TD>
                          <TD>Delivery Date:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD><input type="text" size="14" class="datepicker" value ="'.$_SESSION['EditInternalOrder'][17].'" name="DeliveryDate"/>';
//                            BuildDaySelector(11, 'DeliveryDay', GetDayFromSessionDate($_SESSION['EditInternalOrder'][17]));
//                            echo '&nbsp;';
//                            BuildMonthSelector(12, 'DeliveryMonth', GetMonthFromSessionDate($_SESSION['EditInternalOrder'][17]));
//                            echo '&nbsp;';
//                            BuildYearSelector(13, 'DeliveryYear', GetYearFromSessionDate($_SESSION['EditInternalOrder'][17]));
                    echo '</TD>
                        </TR>
                        <TR>
                            <TD>Category:
                               <SPAN class="note">*
                               </SPAN>
                            </TD>
                            <TD colspan="3">';
                                  BuildReportsCategorySelector(14, 'ReportCategory', 'standard', $_SESSION['EditInternalOrder'][22]);                    
                      echo '</TD>
                        </TR>'; 
                      
                        if ($_SESSION['cAuth'] & 32)
                        {
                          echo '<TR>
                                  <TD colspan="4" class="header">Additional Details
                                  </TD>
                                </TR>
                                <TR>
                                  <TD>Supplier Invoice:
                                  </TD>
                                  <TD class="bold" colspan="3">
                                    <INPUT name="SupplierInvoice" type="hidden" value="'.$_SESSION['EditInternalOrder'][8].'" />'.$_SESSION['EditInternalOrder'][8].'
                                  </TD>
                                </TR>';
                        }
                  echo '<TR>
                          <TD colspan="4" class="center">';
                  //if ($_SESSION['cAuth'] & 32)
                  //              echo '<INPUT name="Submit" type="submit" class="button" value="AutoApprove" />';
                   $edit = "'edit'";
                    echo'<input tabindex="14" type="button" id = "openUncontrolledgoods" onclick="OpenUncontolledPopup('.$edit.')" value="Submit" class="button">
                         <INPUT tabindex="15" name="Submit" type="submit" class="button" value="Cancel" />                  
                        </TD>
                        </TR>
                      </TABLE> 
                      <BR /><BR />
                      <TABLE cellspacing="5" align="center" class="longer">
                        <TR>
                          <TD colspan="11" class="header">Item Details
                          </TD>
                        </TR>
                        <TR>
                          <TD class="subheader">Manufacturer Code
                          </TD>
                          <TD class="subheader">Manufacturer Description
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD class="subheader">Backorder
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD class="subheader">Value (VAT excl.)
                            <SPAN class="note">**
                            </SPAN>
                          </TD>
                          <TD class="subheader">Quantity
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD class="subheader">Asset
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD  colspan="3" class="subheader">Storage Type
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD class="subheader veryshort">Discount(%)
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD class="subheader">Total
                          </TD>
                        </TR>
                        <TR>
                          <TD class="center test">
                            <INPUT tabindex="16" name="SupplierCode" type="text" class="text veryshort Hide" maxlength="100" value="'.$_SESSION['EditInternalOrder'][11].'" />
                             <div id = "SupplierCode" class="autocomplete"></div>   
                          </TD>
                          <TD class="center testing">
                            <INPUT tabindex="17" name="Description" type="text" class="text standard" maxlength="100" value="'.$_SESSION['EditInternalOrder'][12].'" />      
                          </TD>
                          <TD class="center">';
                            BuildYesNoSelector(18, 'Backorder', 'veryshort', $_SESSION['EditInternalOrder'][13]);
                    echo '</TD>
                          <TD class="center">
                            <INPUT tabindex="19" name="Value" type="text" class="text veryshort" maxlength="13" value="'.$_SESSION['EditInternalOrder'][14].'" />
                          </TD>
                          <TD class="center">
                            <INPUT tabindex="20" name="Quantity" type="text" class="text veryshort" maxlength="11" value="'.$_SESSION['EditInternalOrder'][15].'" />
                          </TD>
                          <TD class="center">';
                            BuildYesNoSelector(21, 'Asset', 'veryshort', $_SESSION['EditInternalOrder'][18]);
                    echo '</TD>
                          <TD class="center" colspan="3">';
                            BuildStorageTypeSelector(21, 'StorageType', 'veryshort StorageADDF', $_SESSION['EditInternalOrder'][20]);
                    echo '</TD>
                          <TD class="center">';
                                    if($_SESSION['EditInternalOrder'][21] !="")$DiscountValue = $_SESSION['EditInternalOrder'][21];
                                    else $DiscountValue=0;
                         echo'<INPUT tabindex="21" name="Discount" type="text" class="text veryshort" maxlength="5" value="'.$_SESSION['EditInternalOrder'][21].'" />
                          </TD>
                          <TD class="center">-
                          </TD>
                          <TD class="center">
                            <INPUT tabindex="22" name="Submit" type="submit" style="width: 54px; height: 21px; font-size: 8pt; font-weight: bold;" value="Add" />
                          </TD>
                        </TR>';
                        $count = 24;
                        $countA = 0;
                        while ($count <= SizeOf($_SESSION['EditInternalOrder']))
                        {
                          if (($_SESSION['EditInternalOrder'][$count] != "") && ($_SESSION['EditInternalOrder'][$count][1] != ""))
                          {
                            if ($countA % 2 == 0)
                              $colour = 'rowB';
                            else
                              $colour = 'rowA';
                            
                            echo '<TR>
                                    <TD class="'.$colour.' center test">
                                      <INPUT tabindex="'.($count + 5).'" name="SupplierCode'.$count.'" type="text" class="text veryshort Hide" maxlength="100" value="'.$_SESSION['EditInternalOrder'][$count][0].'" />
                                     <div id = "SupplierCode'.$count.'" class="autocomplete"></div></TD>
                                    </TD>
                                    <TD class="'.$colour.' center testing">
                                      <INPUT tabindex="'.($count + 5).'" name="Description'.$count.'" type="text" class="text standard" maxlength="100" value="'.$_SESSION['EditInternalOrder'][$count][1].'" />                                 
                                    
                                    <TD class="'.$colour.' center">';
                                      BuildYesNoSelector($count + 5, 'Backorder'.$count, 'veryshort', $_SESSION['EditInternalOrder'][$count][4]);
                              echo '</TD>
                                    <TD class="'.$colour.' center">
                                      <INPUT tabindex="'.($count + 5).'" name="Value'.$count.'" type="text" class="text veryshort" maxlength="13" value="'.SPrintF('%02.2f', $_SESSION['EditInternalOrder'][$count][2]).'" />
                                    </TD>
                                    <TD class="'.$colour.' center">
                                      <INPUT tabindex="'.($count + 5).'" name="Quantity'.$count.'" type="text" class="text veryshort" maxlength="11" value="'.$_SESSION['EditInternalOrder'][$count][3].'" />
                                    </TD>
                                    <TD class="'.$colour.' center">';
                                      BuildYesNoSelector($count + 5, 'Asset'.$count, 'veryshort', $_SESSION['EditInternalOrder'][$count][5]);
                              echo '</TD>
                                    <TD class="'.$colour.' center">';
                                      BuildStorageTypeSelector($count + 5, 'StorageType'.$count, 'veryshort StorageADD', $_SESSION['EditInternalOrder'][$count][7]);
                              echo '</TD>
                                    <TD class="'.$colour.' center">
                                      <INPUT tabindex="'.($count + 5).'" name="Discount'.$count.'" type="text" class="text veryshort" maxlength="5" value="'.$_SESSION['EditInternalOrder'][$count][8].'" />
                                    </TD>';
                                       $originalValue = SPrintF('%02.2f', $_SESSION['EditInternalOrder'][$count][2]*$_SESSION['EditInternalOrder'][$count][3]);
                                           if($_SESSION['EditInternalOrder'][$count][8] > 0)
                                               $originalValue -= SPrintF('%02.2f',(($_SESSION['EditInternalOrder'][$count][8]/100)*$originalValue));
                               echo'<TD class="'.$colour.' center">'.SPrintF('%02.2f',$originalValue ).'
                                    </TD>
                                    <TD class="center">
                                      <INPUT tabindex="'.($count + 5).'" name="Remove'.$count.'" type="submit" style="width: 54px; height: 21px; font-size: 8pt; font-weight: bold;" value="Remove" />
                                    </TD>
                                    <TD class="center">
                                      <INPUT tabindex="'.($count + 5).'" name="Update'.$count.'" type="submit" style="width: 54px; height: 21px; font-size: 8pt; font-weight: bold;" value="Update" />
                                    </TD>
                                  </TR>';
                            $countA++;
                          }
                          $count++;
                        }
                echo '</TABLE> 
                    </FORM>  
                  </DIV>  
                  <DIV>
                    <BR /><BR />
                    <SPAN class="note">*
                    </SPAN>
                    These fields are required.
                    <BR />
                    <SPAN class="note">**
                    </SPAN>
                    Make sure that all values entered are in the same currency.
                  </DIV>';
                echo $infoBox;
          } else
          if (isset($_SESSION['MarkInternalOrder']))
          {
            BuildContentHeader('Mark Internal Order - '.$_SESSION['MarkInternalOrder'][0], "", true);
            echo '<DIV class="contentflow">
                    <P>Enter the details of the change below. Ensure that all the required information is entered before submission and that this information is valid.</P>
                    <BR /><BR />                 
                   <TABLE cellspacing="5" align="center" class="short">
                        <FORM method="post" action="Handlers/InternalOrders_Handler.php">
                        <INPUT name="Type" type="hidden" value="Mark">
                        <TR>
                          <TD colspan="2" class="header">Mark Details
                          </TD>
                        </TR>
                        <TR>
                          <TD class="short">Status:
                            <SPAN class="note">*
                            </SPAN>
                         </TD>
                          <TD>';
                            BuildInternalOrderStatusSelector(1, 'Status', 'long', $_SESSION['MarkInternalOrder'][1]);
                    echo '</TD>
                        </TR>
                        <TR>
                         <TD colspan="2" class="center">
                         <INPUT tabindex="2" name="Submit" type="submit" class="button" value="Submit" />   
                         <INPUT tabindex="3" name="Submit" type="submit" class="button" value="Cancel" />    
                        </TD>
                        </TR>
                      </TABLE> 
                  </FORM>
                  </DIV>  
                  <DIV>
                    <BR /><BR />
                    <SPAN class="note">*
                    </SPAN>
                    These fields are required.
                  </DIV>';
          }  else
          if (isset($_SESSION['MarkInternalOrderMultipleList'])){
               BuildContentHeader('Mark Internal Order', "", true);
            echo '<DIV class="contentflow">
                    <P>Select the checkboxes and then use the dropdown to mark all the selected orders.</P>
                    <BR /><BR />
                    <div id = "Loader" class="loader">Loading...</div>
                    <DIV id=MarkData 
                        data-StatusMark="'.$_SESSION['MarkInternalOrderMultipleList'][0].'"
                        data-SupplierMark="'. $_SESSION['MarkInternalOrderMultipleList'][1].'"
                        data-DateSMark="'. $_SESSION['MarkInternalOrderMultipleList'][2].'"
                        data-DateEMark="'. $_SESSION['MarkInternalOrderMultipleList'][3].'" 
                        data-PosComp="'. $_SESSION['MarkInternalOrderMultipleList'][4].'" 
                    style="display: none;" >
                    </DIV>
                    <div style="height: 38px; text-align: center; float: right;">
                      Number of Records: <div id="numberBoxContainer" style="display:inline-block;vertical-align:middle;"></div></div>
                    </br></br></br>
                    <DIV id=Mutiple_Mark_dxDataGrid></div>';
            
                      echo'<DIV class="bottomButtons" > </br><FORM method="post" action="Handlers/InternalOrders_Handler.php">
                        <INPUT name="Type" type="hidden" value="MarkMultipleList">';
                         echo' <INPUT tabindex="3" name="Submit" type="submit" class="button" value="Cancel" />';               

                  echo'  </FORM></DIV>';  

          }else
          if (isset($_SESSION['ViewInternalOrderExceeded']))
          {
            $resultSet = ExecuteQuery('SELECT OrderDiscrepency.*, OrderDiscrepencyType_Description, SupplierInvoice_Number FROM OrderDiscrepency, OrderDiscrepencyType, SupplierInvoice WHERE OrderDiscrepency_Type = OrderDiscrepencyType_ID AND OrderDiscrepency_Invoice = SupplierInvoice_ID ORDER BY OrderDiscrepency_Order, SupplierInvoice_Number, OrderDiscrepencyType_Description ASC');
            BuildContentHeader('View Internal Orders Discrepencies', "", "", true);
            if (MySQL_Num_Rows($resultSet) > 0)
            {
              echo '<DIV class="contentflow">
                      <P>These are the internal orders listed with discrepencies. Orders with multiple discrepencies are listed multiple times.</P>
                      <BR /><BR />
                      <TABLE cellspacing="5" align="center" class="long">
                        <TR>
                          <TD colspan="5" class="header">Internal Order Details
                          </TD>
                        </TR>
                        <TR>
                          <TD class="subheader">Order Number
                          </TD>
                          <TD class="subheader">Invoice Number
                          </TD>
                          <TD class="subheader">Requested By
                          </TD>
                          <TD class="subheader">Discrepency
                          </TD>
                          <TD class="subheader">Description
                          </TD>
                        </TR>';
                        while ($row = MySQL_Fetch_Array($resultSet))
                        {                        
                          if ($count % 2 == 0)
                            $colour = 'rowA';
                          else
                            $colour = 'rowB';
                          
                          if ($row['OrderDiscrepency_Order'] != "")
                            $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT Staff_First_Name, Staff_Last_Name FROM OrderNo, Staff WHERE OrderNo_Requested = Staff_Code AND OrderNo_Number = "'.$row['OrderDiscrepency_Order'].'"'));
                          else
                            $rowTemp = array();
                          echo '<TR>                                  
                                  <TD class="'.$colour.' center">'.$row['OrderDiscrepency_Order'].'
                                  </TD>
                                  <TD class="'.$colour.' center">'.$row['SupplierInvoice_Number'].'
                                  </TD>
                                  <TD class="'.$colour.' center">'.$rowTemp['Staff_First_Name'].' '.$rowTemp['Staff_Last_Name'].'
                                  </TD>
                                  <TD class="'.$colour.' center">'.$row['OrderDiscrepencyType_Description'].'
                                  </TD>
                                  <TD class="'.$colour.'">'.$row['OrderDiscrepency_Description'].'
                                  </TD>
                                </TR>';
                          $count++;
                        }
                echo '</TABLE>
                    </DIV>';
            } else
              echo '<DIV class="contentflow">
                      <P>There are no internal orders listed with discrepencies.</P>
                      <BR /><BR />
                    </DIV>';
            Session_Unregister('ViewInternalOrderExceeded');
          } else
          if (isset($_SESSION['ViewInternalOrderList']))
          {
            $startDate = GetDatabaseDateFromSessionDate($_SESSION['ViewInternalOrderList'][0]);
            $endDate = GetDatabaseDateFromSessionDate($_SESSION['ViewInternalOrderList'][1]);
            if ($_SESSION['ViewInternalOrderList'][0] == $_SESSION['ViewInternalOrderList'][1])
              $dateRange = GetTextualDatstart_dateMarkromSessionDate($_SESSION['ViewInternalOrderList'][0]);
            else        
              $dateRange = GetTextualDateFromSessionDate($_SESSION['ViewInternalOrderList'][0]).' to '.GetTextualDateFromSessionDate($_SESSION['ViewInternalOrderList'][1]);
            
            switch ($_SESSION['ViewInternalOrderList'][2])
            {
              case "":
                $status = "";
                break;
              default:
                $status = ' AND OrderNo_Complete = "'.$_SESSION['ViewInternalOrderList'][2].'"';
                break;
            }
            
            $count = 0;
            if ($_SESSION['ViewInternalOrderList'][3] == "")
            {  
              $projectTitle = ' - All Projects';  
              $project = "";  
              $count++;        
            } else
            {  
              $row = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Project WHERE Project_Code = "'.$_SESSION['ViewInternalOrderList'][3].'"'));
              $projectTitle = ' - '.$row['Project_Description'];
              $project = ' AND OrderNo_Project = "'.$_SESSION['ViewInternalOrderList'][3].'"';
            } 
           
            if ($_SESSION['ViewInternalOrderList'][4] == "")
            {
              $supplierTitle = ' - All Suppliers';  
              $supplier = "";
              $count++;
            } else
            {  
              $row = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Supplier WHERE Supplier_Code = "'.$_SESSION['ViewInternalOrderList'][4].'"'));
              $supplierTitle = ' - '.$row['Supplier_Name'];
              $supplier = ' AND OrderNo_Supplier = "'.$_SESSION['ViewInternalOrderList'][4].'"';
            }  
            
            BuildContentHeader('View Internal Orders'.$projectTitle.$supplierTitle.' - '.$dateRange, "", "", true);
            
            if ($_SESSION['cAuth'] & 32)
            {
              $resultSet = ExecuteQuery('SELECT * FROM OrderNo WHERE 1=1'.$status.$project.$supplier.' AND OrderNo_Date_Time BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" ORDER BY OrderNo_Date_Time ASC');
              $count++;
            } else
            {
              $resultSet = ExecuteQuery('SELECT * FROM OrderNo WHERE 1=1'.$status.$project.$supplier.' AND OrderNo_Approved = "1" AND OrderNo_Date_Time BETWEEN "'.$startDate.' 00:00:00" AND "'.$endDate.' 23:59:59" ORDER BY OrderNo_Date_Time ASC');
              $count++;
            }
            if (MySQL_Num_Rows($resultSet) > 0)
            {
              echo '<DIV class="">
                        <P>These are the internal orders listed. Please note it may take some time to generate this report.</P>
                        <BR /><BR />
                        <p style="margin: 2em 0em 0.5em 2em;">
                        <label for="Expand" id="collapseAll"><strong><a href="#">Expand All</a></strong></label>
                        <label style="padding-left:10em;" for="search"><strong>Global Search </strong></label><input type="text" id="search" placeholder="Search the entire page..."/>
                        <INPUT name="go" id="Go" type="button"  value="Go" />                   
                        </p>
                        <TABLE cellspacing="5" align="center" class="Custom" id="parent_table">
                            <THEAD>
                                <TR class="titleTr header">
                                    <th colspan="'.($count + 7).'" class="titleTd">Internal Order Details
                                    </th>
                                </TR>
                                <TR class="headingTr">
                                    <th></th>
                                    <th class="center small">Order Number</th>';
                                    if ($_SESSION['ViewInternalOrderList'][3] == "")
                                        echo'<th class="center">Project</th>';
                                    if ($_SESSION['ViewInternalOrderList'][4] == "")
                                        echo'<th class="center"> Supplier</th>';
                               echo' <th class="center">Quotation number</th>
                                    <th class="center small">Date Placed</th>
                                    <th class="center">Placed By</th>
                                    <th class="center veryshort">Supplier Invoice</th>
                                    <th class="center veryshort">Original Total</th>
                                    <th class="center veryshort">Invoice Total</th>
                                    
                                </TR>
                                
                             </THEAD>
                             <TBODY>';
                                 $rowColorCount =0;
                                 while ($row = MySQL_Fetch_Array($resultSet))
                                     {
                                        if ($rowColorCount % 2 == 0)
                                            $ParentRowcolour = 'rowA';
                                        else
                                            $ParentRowcolour = 'rowB';
                          
                                 $loRowCurrency = MySQL_Fetch_Array(ExecuteQuery('SELECT C.Currency_Symbol FROM Currency C WHERE (C.Currency_ID = "'.$row['OrderNo_Original_Currency'].'")'));
                                 $loCurrencySymbol = $loRowCurrency['Currency_Symbol'];
                                 //Invoices are recorded in Rands so don't worry too much about currencies here... yet...
                                 $orderTable = "";       
                                 $orderTable.= '<TR class="parentRow">
                                                    <TD class="left '.$ParentRowcolour.'"><Img src="Images/Bullet_toggle_plus.png" style="max-width:70px; max-height:20px;height: auto; border:none;"/></TD>
                                                    <TD class="center '.$ParentRowcolour.'">'.$row['OrderNo_Number'].'</TD>';
                                 if ($_SESSION['ViewInternalOrderList'][3] == "")
                                 {
                                    $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Project WHERE Project_Code = "'.$row['OrderNo_Project'].'"'));
                                    $orderTable.= '<TD class="'.$ParentRowcolour.'">'.$rowTemp['Project_Pastel_Prefix'].' - '.$rowTemp['Project_Description'].'</TD>';
                                 }
                                 if ($_SESSION['ViewInternalOrderList'][4] == "")
                                 {
                                    $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Supplier WHERE Supplier_Code = "'.$row['OrderNo_Supplier'].'"'));
                                    $orderTable.= '<TD class="'.$ParentRowcolour.'">'.$rowTemp['Supplier_Name'].'</TD>';
                                 }
                                 
                                $orderTable.= '<TD class="'.$ParentRowcolour.'">'.$row['quotation_number'].'</TD>';
                                
                                 $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = '.$row['OrderNo_Requested']));
                                 $orderTable.=    '<TD class="center '.$ParentRowcolour.'">'.GetTextualDateFromDatabaseDate($row['OrderNo_Date_Time']).'
                                                   </TD>                                
                                                   <TD class="center '.$ParentRowcolour.'">'.$rowTemp['Staff_First_Name'].' '.$rowTemp['Staff_Last_Name'].'
                                                   </TD>
                                                   <TD class="center '.$ParentRowcolour.'">';
                                 $resultSetTemp = ExecuteQuery('SELECT * FROM SupplierInvoice WHERE SupplierInvoice_S4OrderNo = "'.$row['OrderNo_Number'].'" ORDER BY SupplierInvoice_Number ASC');        		    
                                 while ($rowTemp = MySQL_Fetch_Array($resultSetTemp))
                                     {
                                         $orderTable.= $rowTemp['SupplierInvoice_Number'].', ';
                                     }           
                                 $resultSetInvoice = ExecuteQuery('SELECT * FROM SupplierInvoice WHERE SupplierInvoice_S4OrderNo = "'.$row['OrderNo_Number'].'"');
                                 if (MySQL_Num_Rows($resultSetInvoice) > 0)
                                 {
                                     $total = 0;
                                     while ($rowTempB = MySQL_Fetch_Array($resultSetInvoice))
                                     {
                                         $total += $rowTempB['SupplierInvoice_Total'];
                                     }
                                     $total = 'R'.SPrintF('%02.2f', $total);
                                 }
                                 else
                                    $total = "";
                                $orderTable.= '</TD>
                                               <TD class="center '.$ParentRowcolour.'">R'.SPrintF('%02.2f', $row['OrderNo_Total_Cost']).'
                                               </TD>
                                               <TD class="center '.$ParentRowcolour.'">'.$total.'
                                               </TD>
                                           </TR>
                                           <TR class="nested" style="display:none;">
                                               <TD colspan="'.($count + 7).'" ><div>';
                                   if ($_SESSION["ViewInternalOrderList"][5])
										$resultSetTempB = ExecuteQuery('SELECT * FROM Items WHERE Items_Order_Number = "'.$row['OrderNo_Number'].'" AND Items_Quantity <> Items_Storeman_Received');
									else
										$resultSetTempB = ExecuteQuery('SELECT * FROM Items WHERE Items_Order_Number = "'.$row['OrderNo_Number'].'"');
                                    
                                   if (MySQL_Num_Rows($resultSetTempB) > 0)
                                    {
                                        echo $orderTable.'<TABLE cellspacing="5" align="center" class="sub_table"><THEAD><TR class="headingTr">
                                              <TH class="subheader">Manufacturer Code
                                              </TH>
                                              <TH class="subheader">Manufacturer Description
                                              </TH>
                                              <TH class="subheader">Backorder
                                              </TH>
                                              <TH class="subheader">Original Value (VAT excl.)
                                              </TH>
                                              <TH class="subheader">Nett Value(Discount Incl.)
                                              </TH>
                                              <TH class="subheader">Invoice Value (VAT excl.)
                                              </TH>
                                              <TH class="subheader">Quantity
                                              </TH>
                                              <TH class="subheader">Asset
                                              </TH>
                                              <TH class="subheader"> Invoice Captured
                                              </TH>
                                              <TH class="subheader">Storeman Received
                                              </TH>
                                              <TH class="subheader">Storage Type
                                              </TH>
                                              <TH class="subheader">Discount
                                              </TH>
                                            </TR></THEAD><TBODY>';
                                            $countA = 0;        		    
                                            while ($rowTempB = MySQL_Fetch_Array($resultSetTempB))
                                            {                        
                                              if ($countA % 2 == 0)
                                                $colour = 'rowA';
                                              else
                                                $colour = 'rowB';
                                              
                                              $nett_value = $rowTempB['Items_Value'];
                                              if($rowTempB['Items_Discount'] > 0)
                                                    $nett_value -= ($rowTempB['Items_Discount']/100) * $nett_value;
                                              
                                              $resultSetInvoice = ExecuteQuery('SELECT * FROM SupplierInvoiceItems WHERE SupplierInvoiceItems_Items_ID = '.$rowTempB['Items_ID'].' ORDER BY SupplierInvoiceItems_ID DESC LIMIT 1');
                                              if (MySQL_Num_Rows($resultSetInvoice) > 0)
                                              {
                                                $rowInvoice = MySQL_Fetch_Array($resultSetInvoice);
                                                $latestValue = 'R'.SPrintF('%02.2f', $rowInvoice['SupplierInvoiceItems_Value']);
                                              }
                                              else
                                                $latestValue = "";
                                              
                                              echo '<TR>                                  
                                                      <TD class="'.$colour.' center">'.$rowTempB['Items_Supplier_Code'].'
                                                      </TD>
                                                      <TD class="'.$colour.'">'.$rowTempB['Items_Description'].'
                                                      </TD>
                                                      <TD class="'.$colour.' center">'.
                                                        (($rowTempB['Items_Backorder'] == '1') ? 'Yes' : 'No').'
                                                      </TD>
                                                      <TD class="'.$colour.' center">R'.SPrintF('%02.2f', $rowTempB['Items_Value']).'
                                                      </TD>
                                                      <TD class="'.$colour.' center">R'.SPrintF('%02.2f', $nett_value).'
                                                      </TD>
                                                      <TD class="'.$colour.' center">'.$latestValue.'
                                                      </TD>
                                                      <TD class="'.$colour.' center">'.$rowTempB['Items_Quantity'].'
                                                      </TD>
                                                      <TD class="'.$colour.' center">'.
                                                        (($rowTempB['Items_Asset'] == '1') ? 'Yes' : 'No').'
                                                      </TD>
                                                      <TD class="'.$colour.' center">'.$rowTempB['Items_Received_Quantity'].'
                                                      </TD>
                                                      <TD class="'.$colour.' center">'.$rowTempB['Items_Storeman_Received'].'
                                                      </TD>
                                                      <TD class="'.$colour.' center">'.RetutnStorageTypeDesc($rowTempB['Items_StorageType']).
                                                     '</TD>RetutnStorageTypeDesc
                                                      <TD class="'.$colour.' center">'.$rowTempB['Items_Discount']."%".'
                                                      </TD>
                                                    </TR>';
                                              $countA++;
                                            }
			    echo '</TBODY></TABLE>';
                        }                       
                        echo'<div></TD></TR></TD></TR>';
                        $rowColorCount++;
                      }
                 
              echo '</TBODY></TABLE></DIV>';
            } else
              echo '<DIV class="contentflow">
                      <P>There are no internal orders listed for the given date range.</P>
                      <BR /><BR />
                    </DIV>';
            Session_Unregister('ViewInternalOrderList');
          } else
          if (isset($_SESSION['ViewInternalOrderOutstanding']))
          {
            $loRS = ExecuteQuery('SELECT OrderNo_Number FROM OrderNo WHERE OrderNo_Complete = "0"');
            BuildContentHeader('View Outstanding Internal Orders', "", "", true);
            if (MySQL_Num_Rows($loRS) > 0)
            {
              echo '<DIV class="contentflow">
                      <P>These are the outstanding internal orders listed. Please note it may take some time to generate this report and that any order without items listed has a discrepency.</P>
                      <BR /><BR />
                      <TABLE cellspacing="5" align="center" class="standard">
                        <TR>
                          <TD colspan="4" class="header">Internal Order Details
                          </TD>
                        </TR>
                        <TR>
                          <TD class="subheader">Order Number
                          </TD>
                          <TD class="subheader">Manufacturer Code
                          </TD>
                          <TD class="subheader">Manufacturer Description
                          </TD>
                          <TD class="subheader">Quantity Outstanding
                          </TD>
                        </TR>';
                        $alt = true;
                        while ($loRow = MySQL_Fetch_Array($loRS))
                        {
                          $colour = ($alt ? 'rowA' : 'rowB');
                          
                          $loRSItems = ExecuteQuery('SELECT Items_Supplier_Code, Items_Description, Items_Quantity, Items_Received_Quantity FROM Items WHERE Items_Order_Number = "'.$loRow['OrderNo_Number'].'" AND Items_Received = "0"');
                          echo '<TR>
                                  <TD class="'.$colour.' center" rowspan="'.(MySQL_Num_Rows($loRSItems) > 1 ? MySQL_Num_Rows($loRSItems) : '1' ).'">'.$loRow['OrderNo_Number'].'
                                  </TD>';
                          if (MySQL_Num_Rows($loRSItems) > 0)
                          {
                            $loCount = 0;
                            while ($loRowItem = MySQL_Fetch_Array($loRSItems))
                            {
                              if ($loCount > 0)
                                echo '<TR>';
                                
                                echo '<TD class="'.$colour.' center">'.$loRowItem['Items_Supplier_Code'].'
                                      </TD>
                                      <TD class="'.$colour.' center">'.$loRowItem['Items_Description'].'
                                      </TD>
                                      <TD class="'.$colour.' center">'.($loRowItem['Items_Quantity'] - $loRowItem['Items_Received_Quantity']).'
                                      </TD>
                                    </TR>';
                              $loCount++;
                            }
                            $alt = !$alt;
                          } else
                              echo '<TD class="'.$colour.' center bold" colspan="3">There are no outstanding items.
                                    </TD>
                                  </TR>';
                        }
                echo '</TABLE>
                    </DIV>';
            } else
              echo '<DIV class="contentflow">
                      <P>There are no outstanding internal orders listed.</P>
                      <BR /><BR />
                    </DIV>';
            Session_Unregister('ViewInternalOrderOutstanding');
          } else
          if (isset($_SESSION['ViewInternalOrderSingle']))
          {
             
            
            $listForEmail = '032,212,321';  //cherie, almelia, dwayn 
            $row = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM OrderNo WHERE OrderNo_Number = "'.$_SESSION['ViewInternalOrderSingle'][0].'"'));
            $loRowCurrency = MySQL_Fetch_Array(ExecuteQuery('SELECT C.Currency_Symbol FROM Currency C WHERE (C.Currency_ID = "'.$row['OrderNo_Original_Currency'].'")'));
            $loCurrencySymbol = $loRowCurrency['Currency_Symbol'];
           
            BuildContentHeader('View Internal Order - '.$row['OrderNo_Number'], "", "", true);
            echo '<DIV class="contentflow">
                    <P>These are the internal order details.</P>
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center" class="standard">
                      <TR>
                        <TD colspan="4" class="header">Internal Order Details
                        </TD>
                      </TR>
                      <TR>
                        <TD class="short">Requested By:
                        </TD>
                        <TD class="bold">';
                          $listForEmail .=  ','.$row['OrderNo_Requested'];
                          $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = '.$row['OrderNo_Requested'].''));        		    
                          echo $rowTemp['Staff_First_Name'].' '.$rowTemp['Staff_Last_Name'].'
                        </TD>
                        <TD class="short vtop">Comments:
                        </TD>
                        <TD rowspan="4" class="short vtop bold">'.$row['OrderNo_Comments'].'
                        </TD>
                      </TR>
                      
                      <TR>
                        <TD>Supplier:
                        </TD>
                        <TD class="bold">';
                          $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Supplier WHERE  Supplier_IsInactive = 0 AND Supplier_Code = '.$row['OrderNo_Supplier'].''));
                          echo $rowTemp['Supplier_Name'].'
                        </TD>
                      </TR>';
                                  
                    $rowTemp =  MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM OrderNo WHERE  OrderNo_Number = '.$row['OrderNo_Number'].''));        		    
                           
                    if($rowTemp['quotation_number']!=""){          
                    echo  '<TR>
                        <TD>Quotation number:
                        </TD>
                        <TD class="bold">';       		    
                          echo $rowTemp['quotation_number'].'
                        </TD>
                      </TR>';
                      }
                 
                        echo '<TD>Project:
                        </TD>
                        <TD class="bold">';
                          $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT * , IFNULL(`Project_CostManager` ,  `Project_Responsible`) AS costmanager FROM Project WHERE Project_Code = '.$row['OrderNo_Project'].''));        		    
                          $listForEmail .=  ','.$rowTemp['costmanager'];
                          echo $rowTemp['Project_Pastel_Prefix'].' - '.$rowTemp['Project_Description'].'
                          
                        </TD>
                      </TR>
                      <TR>
                        <TD>Order Type:
                        </TD>
                        <TD colspan="3" class="bold">';
                          $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM OrderType WHERE OrderType_ID = '.$row['OrderNo_Type'].''));        		    
                          echo $rowTemp['OrderType_Description'].'
                        </TD>
                      </TR>
                      <TR>
                        <TD>Total (VAT excl.):
                        </TD>
                        <TD class="bold">'.iconv('Windows-1252', 'UTF-8//TRANSLIT', $loCurrencySymbol.SPrintF('%02.2f', $row['OrderNo_Original_Total_Cost'])).'
                        </TD>
                        <TD>Delivery Method:
                        </TD>
                        <TD class="bold">';
                          switch ($row['OrderNo_Delivery'])
                          {
                            case '0':
                              echo 'Collect';
                              break;
                            case '1':
                              echo 'Delivery'; 
                              break;  
                            case '2':
                              echo 'Courier';
                              break;
                            default:
                              break;
                          }               
                  echo '</TD>
                      </TR>
                      <TR>
                        <TD>Date Logged:
                        </TD>
                        <TD class="bold">'.GetTextualDateTimeFromDatabaseDateTime($row['OrderNo_Date_Time']).'
                        </TD>
                      </TR>
                       <TR>
                      
                      </TR>
                      <TR>
                        <TD colspan="4" class="header">Additional Details
                        </TD>
                      </TR>
                      <TR>
                        <TD>Supplier Invoice:
                        </TD>
                        <TD class="bold">';
                          $resultSet = ExecuteQuery('SELECT * FROM SupplierInvoice WHERE SupplierInvoice_S4OrderNo = "'.$row['OrderNo_Number'].'" ORDER BY SupplierInvoice_Number ASC');        		    
                          while ($rowTemp = MySQL_Fetch_Array($resultSet))
                          {
                            echo $rowTemp['SupplierInvoice_Number'].'; ';
                          }
                  echo '</TD>
                        <TD>Order Status:
                        </TD>
                        <TD class="bold">';
                          if ($row['OrderNo_Complete'] == '1')
                            echo 'Complete';
                          else
                            if ($row['OrderNo_Complete'] == '2')
                              echo 'Cancelled';
                            else
                              echo 'Incomplete';
                  echo '</TD>
                      </TR>';
                      if (($_SESSION['cAuth'] & 32) || ($_SESSION['cAuth'] & 64))
                      {
                        $resultSetDiscrepency = ExecuteQuery('SELECT OrderDiscrepency.*, OrderDiscrepencyType_Description, SupplierInvoice_Number FROM OrderDiscrepency, OrderDiscrepencyType, SupplierInvoice WHERE OrderDiscrepency_Order = "'.$row['OrderNo_Number'].'" AND OrderDiscrepency_Type = OrderDiscrepencyType_ID AND OrderDiscrepency_Invoice = SupplierInvoice_ID');  
                        if (MySQL_Num_Rows($resultSetDiscrepency))
                        {
                          echo '<TR>
                                  <TD colspan="4" class="header">Discrepency Details
                                  </TD>
                                </TR>
                                <TR>
                                  <TD class="subheader">Invoice Number
                                  </TD>
                                  <TD class="subheader">Discrepency
                                  </TD>
                                  <TD colspan="2" class="subheader">Description
                                  </TD>
                                </TR>';
                                $count = 0;
                                while ($rowDiscrepency = MySQL_Fetch_Array($resultSetDiscrepency))
                                {
                                  if ($count % 2 == 0)
                                    $colour = 'rowA';
                                  else
                                    $colour = 'rowB';
                                  
                                  echo '<TR>
                                          <TD class="'.$colour.' center bold">'.$rowDiscrepency['SupplierInvoice_Number'].'
                                          </TD>
                                          <TD class="'.$colour.' center bold">'.$rowDiscrepency['OrderDiscrepencyType_Description'].'
                                          </TD>
                                          <TD colspan="2" class="'.$colour.' center bold">'.$rowDiscrepency['OrderDiscrepency_Description'].'
                                          </TD>
                                        </TR>';
                                  $count++;
                                }
                        }
                      }
                      echo'<TR><TD colspan="4" class="center">
                                 <FORM method="post" action="Handlers/InternalOrders_Handler.php">
                                      <INPUT name="Type" type="hidden" value="ViewSingleOrders">
                                     <INPUT tabindex="3" name="Submit" type="submit" class="button" value="Cancel" />         
                                  </FORM>
                              </TD>
                       </TR></TABLE>';
                    $resultSet = ExecuteQuery('SELECT * FROM Items WHERE Items_Order_Number = "'.$row['OrderNo_Number'].'"');        		    
                    if (MySQL_Num_Rows($resultSet) > 0)
                    {  
                      echo '<BR /><BR />
                            <BR /><BR />
                            <TABLE cellspacing="5" align="center" class="standard">
                              <TR>
                                <TD colspan="11" class="header">Item Details
                                </TD>
                              </TR>
                              <TR>
                                <TD class="subheader">Manufacturer Code
                                </TD>
                                <TD class="subheader">Manufacturer Description
                                </TD>
                                <TD class="subheader">Backorder
                                </TD>
                                <TD class="subheader">Original Value (VAT excl.)
                                </TD>
                                <TD class="subheader">Nett Value(Discount Incl.)
                                </TD>
                                <TD class="subheader">Quantity
                                </TD>
                                <TD class="subheader">Discount
                                </TD>
                                <TD class="subheader">Asset
                                </TD>
                                <!--<TD class="subheader">Received
                                </TD>-->
                                <TD class="subheader">Storage Type
                                </TD>
                                <TD class="subheader">Invoice Captured
                                </TD>
                                <TD class="subheader">Storeman Received
                                </TD>
                              </TR>';
                              $count = 0;
                              $resultSet = ExecuteQuery('SELECT * FROM Items WHERE Items_Order_Number = "'.$row['OrderNo_Number'].'"');        		    
                              while ($rowTempB = MySQL_Fetch_Array($resultSet))
                              {
                                if($rowTempB['Items_StorageType']!= 5 && ($rowTempB['Items_Received_Quantity'] != $rowTempB['Items_Storeman_Received']) ){
                                    $colour = 'yellow';
                                }  
                                else if ($count % 2 == 0)
                                  $colour = 'rowA';
                                else
                                  $colour = 'rowB';
                                $nett_value = $rowTempB['Items_Original_Value'];
                                if($rowTempB['Items_Discount'] > 0)
                                    $nett_value -= ($rowTempB['Items_Discount']/100) * $nett_value;
                                echo '<TR>
                                        <TD class="'.$colour.' center bold">'.$rowTempB['Items_Supplier_Code'].'
                                        </TD>
                                        <TD class="'.$colour.' bold">'.$rowTempB['Items_Description'].'
                                        </TD>
                                        <TD class="'.$colour.' center bold">'.
                                          (($rowTempB['Items_Backorder'] == '1') ? 'Yes' : 'No').'
                                        </TD>
                                        <TD class="'.$colour.' center bold">'.iconv('Windows-1252', 'UTF-8//TRANSLIT', $loCurrencySymbol.SPrintF('%02.2f', $rowTempB['Items_Original_Value'])).'
                                        </TD>
                                        <TD class="'.$colour.' center bold">'.iconv('Windows-1252', 'UTF-8//TRANSLIT', $loCurrencySymbol.SPrintF('%02.2f', $nett_value)).'
                                        </TD>
                                        <TD class="'.$colour.' center bold">'.$rowTempB['Items_Quantity'].'
                                        </TD>
                                        <TD class="'.$colour.' center bold">'.$rowTempB['Items_Discount']."%".'
                                        </TD>
                                        <TD class="'.$colour.' center bold">'.
                                          (($rowTempB['Items_Asset'] == '1') ? 'Yes' : 'No').'
                                        </TD>
                                        <!--<TD class="'.$colour.' center bold">'.$rowTempB['Items_Received_Quantity'].'
                                        </TD>-->
                                        <TD class="'.$colour.' center bold">'.RetutnStorageTypeDesc($rowTempB['Items_StorageType']).'
                                        </TD>
                                        <TD class="'.$colour.' center">'.$rowTempB['Items_Received_Quantity'].'
                                        </TD>
                                        <TD class="'.$colour.' center">'.$rowTempB['Items_Storeman_Received'].'
                                        </TD>
                                      </TR>';
                                $count++;
                              }
                          echo '</TABLE>';
                          
                          // Report error section
                           echo '<div class="bottomButtons" style=" width:790px;"></br><INPUT id ="ShowReportError" tabindex="7" name="Submit" type="button" value="Report Error"/></div>';
                           

               echo '<DIV id="ERRORBOX">
                <P>A email can be sent using the form below</P>
                <BR /><BR />
                <TABLE cellspacing="2" align="center" class="standard">
                            <TR>
                              <TD colspan="2" class="header">Add Recipient
                              </TD>
                            </TR>
                            <TR>
                              <TD colspan="2">Select Staff members as a recipient.
                              </TD>
                            </TR>
                            <TR>
                              <TD class="short">Recipient:
                                <SPAN class="note">*
                                </SPAN>
                              </TD>
                              <TD>
                                 <span id="Individual">';  buildInvoiceReportContactList(2, "contacts", "seltest3 Long", $listForEmail); echo'</span>
                              </TD>
                             </TR>
                            <TR>
                              <TD colspan="2" class="header">Add Addtional Information
                              </TD>
                            </TR>
                            <TR>
                              <TD colspan="2"> 
                                    <textarea id="box" placeholder="Type message here"  tabindex="9" name="Comments" class="standard" maxlength="459"></textarea>
                              </TD>
                            </TR>
                            <TR>
                              <TD colspan="2" class="right">
                                    <INPUT id ="SendErrorEmail" tabindex="7" name="Submit" type="button" value="Send"/>
                              </TD>
                            </TR>    
                </TABLE>
              </DIV>';
                           
                           
                    }
                    $resultSet = ExecuteQuery('SELECT * FROM SupplierInvoice WHERE SupplierInvoice_S4OrderNo = "'.$row['OrderNo_Number'].'" ORDER BY SupplierInvoice_Number ASC');        		    
                    if (MySQL_Num_Rows($resultSet) > 0)
                    {
                      while ($rowTemp = MySQL_Fetch_Array($resultSet))
                      {
                        echo '<BR /><BR />
                              <BR /><BR />
                              <TABLE cellspacing="5" align="center" class="standard">
                                <TR>
                                  <TD colspan="5" class="header">Invoice Details - '.$rowTemp['SupplierInvoice_Number'].'
                                  </TD>
                                </TR>
                                <TR>
                                  <TD class="short">Date and Time Invoiced:
                                  </TD>
                                  <TD class="bold">'.GetTextualDateTimeFromDatabaseDateTime($rowTemp['SupplierInvoice_DateTime']).'
                                  </TD>
                                </TR>
                                <TR>
                                  <TD class="short">Total (VAT excl.):
                                  </TD>
                                  <TD class="bold">R'.SPrintF('%02.2f', $rowTemp['SupplierInvoice_Total']).'
                                  </TD>
                                </TR>';
                                $resultSetTemp = ExecuteQuery('SELECT SupplierInvoiceItems.*, Items_Description, Items_Supplier_Code FROM SupplierInvoiceItems, Items WHERE SupplierInvoiceItems_Invoice_Number = '.$rowTemp['SupplierInvoice_ID'].' AND SupplierInvoiceItems_Items_ID = Items_ID');        		    
                                if (MySQL_Num_Rows($resultSetTemp) > 0)
                                {
                                  echo '<TR>
                                          <TD class="subheader">Manufacturer Code
                                          </TD>
                                          <TD class="subheader">Manufacturer Description
                                          </TD>
                                          <TD class="subheader">Value (VAT excl.)
                                          </TD>
                                          <TD class="subheader">Quantity
                                          </TD>
                                          <TD class="subheader">Total
                                          </TD>
                                        </TR>';
                                        $count = 0;
                                        while ($rowTempB = MySQL_Fetch_Array($resultSetTemp))
                                        {
                                          if ($count % 2 == 0)
                                            $colour = 'rowA';
                                          else
                                            $colour = 'rowB';
                                          
                                          if ($rowTempB['SupplierInvoiceItems_Value'] == "0")
                                          {
                                            $rowTempC = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Items WHERE Items_ID = "'.$rowTempB['SupplierInvoiceItems_Items_ID'].'"'));
                                            $value = $rowTempC['Items_Value'];
                                            $total = $value * $rowTempB['SupplierInvoiceItems_Quantity'];
                                          }
                                          else
                                          {
                                            $value = $rowTempB['SupplierInvoiceItems_Value'];
                                            $total = $rowTempB['SupplierInvoiceItems_Total'];
                                          }
                                          
                                          echo '<TR>
                                                  <TD class="'.$colour.' center bold">'.$rowTempB['Items_Supplier_Code'].'
                                                  </TD>
                                                  <TD class="'.$colour.' bold">'.$rowTempB['Items_Description'].'
                                                  </TD>
                                                  <TD class="'.$colour.' center bold">'.SPrintF('%02.2f', $value).'
                                                  </TD>
                                                  <TD class="'.$colour.' center bold">'.$rowTempB['SupplierInvoiceItems_Quantity'].'
                                                  </TD>
                                                  <TD class="'.$colour.' center bold">R'.SPrintF('%02.2f', $total).'
                                                  </TD>
                                                </TR>';
                                          $count++;
                                        }
                                }
                        echo '</TABLE>';
                      }
                    }
            echo '</DIV>';
           
          } else
          {
            BuildContentHeader('Maintenance', "", "", true);
echo'<P>New internal orders can be added by using the form below. Details on internal orders already listed can also be edited.</P>';


            echo '<DIV class="contentflow">
                    
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center" class="standard">
                      <FORM method="post" action="Handlers/InternalOrders_Handler.php" class="form-horizontal">
                        <INPUT name="Type" type="hidden" value="Maintain">
                        <TR>
                          <TD colspan="4" class="header">Add
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="3">To add a new internal order, click Add and complete the form that is displayed.
                          </TD>
                          <TD class="right">
                            <INPUT tabindex="1" name="Submit" type="submit" class="button" value="Add" />
                          </TD>
                        </TR>';                                                   
                        if ((($_SESSION['cAuth'] & 32) || ($_SESSION['cAuth'] & 64) || ($_SESSION['cAuth'] & 8) || ($_SESSION['cUID'] == 8)|| ($_SESSION['cUID'] == 34)|| ($_SESSION['cUID'] == 7) || ($_SESSION['cUID'] == 39) || ($_SESSION['cUID'] == 30 || ($_SESSION['cUID'] == 235) || ($_SESSION['cUID'] == 310) || ($_SESSION['cUID'] == 11) || ($_SESSION['cUID'] == 390)))) //Arthur, Johannes, Gideon, Malcolm,Lance, Stefan Admin, Shawn Browne, Ettiene and Bosses can approve.
                        {
                          echo '<TR>
                                  <TD colspan="4" class="header">Approve
                                  </TD>
                                </TR>
                                <TR>
                                  <TD colspan="4">To approve an internal order, select the order number, click Approve and confirm when prompted.
                                  </TD>
                                </TR>
                                <TR>
                                  <TD class="short">Internal Order:
                                    <SPAN class="note">*
                                    </SPAN>
                                  </TD>
                                  <TD>';
                        
                                  if (($_SESSION['cAuth'] & 32) || ($_SESSION['cAuth'] & 64) || ($_SESSION['cUID'] == 8)|| ($_SESSION['cUID'] == 34)|| ($_SESSION['cUID'] == 7) || ($_SESSION['cUID'] == 39) || ($_SESSION['cUID'] == 30)|| ($_SESSION['cUID'] == 310) || ($_SESSION['cUID'] == 11)  ){
                                     buildTheApprovalDropdown(1, 'InternalOrderApprove', 'standard', true);
                                     // BuildInternalOrderSelector(1, 'InternalOrderApprove', 'standard', "", '0'); 
                                  }
                                     
                                  else{
                             
                                      buildTheApprovalDropdown(1, 'InternalOrderApprove', 'standard', false);
                                      //BuildInternalOrderSelector(1, 'InternalOrderApprove', 'standard', "", '0',"",false,false,"", false);
                                  }
                            echo '</TD>
                                  <TD colspan="2" class="right">
                                    <INPUT tabindex="1" name="Submit" type="submit" class="button" value="Approve" />                   
                                  </TD>
                                </TR>';
                        }
                  echo '<TR>
                          <TD colspan="4" class="header">Edit
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="4">To edit an internal order, specify the order number, click Edit and complete the form that is displayed.
                          </TD>
                        </TR>
                        <TR>
                          <TD class="short">Internal Order:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>
                          <TD>';                                                
                            if ($_SESSION['cAuth'] & 32 || $_SESSION['cUID'] == 310 || $_SESSION['cUID'] == 410) // give shawn access to edit all orders
                              /*BuildInternalOrderSelector(1, 'InternalOrderEdit', 'standard', "", "", "");*/
                              BuildTextBox(1, 'InternalOrderEdit', 'text', 'text short', 7, "");
                            else
                              BuildInternalOrderSelector(1, 'InternalOrderEdit', 'standard', "", "", '0', true, false, 100);
                    echo '</TD>
                          <TD colspan="2" class="right">
                            <INPUT tabindex="5" name="Submit" type="submit" class="button" value="Edit" />                   
                          </TD>
                        </TR>';
                         if ($_SESSION['cAuth'] & 32 || $_SESSION['cUID'] == 303 || $_SESSION['cUID'] == 321 || $_SESSION['cUID'] == 499) // give Dwain & Tyran access to mark all orders

                        {
                          echo '<TR>
                                  <TD colspan="4" class="header">Mark
                                  </TD>
                                </TR>
                                <TR>
                                  <TD colspan="4">To change the status of an internal order, specify the order number, click Mark and complete the form that is displayed.
                                  </TD>
                                </TR>
                                <TR>
                                  <TD class="short">Internal Order:
                                    <SPAN class="note">*
                                    </SPAN>
                                  </TD>
                                  <TD>';
                                    /*BuildInternalOrderSelector(1, 'InternalOrderMark', 'standard', "", "", "");*/
                                    BuildTextBox(1, 'InternalOrderMark', 'text', 'text short', 7, "");
                            echo '</TD>
                                  <TD colspan="2" class="right">
                                    <INPUT tabindex="1" name="Submit" type="submit" class="button" value="Mark" />                  
                                  </TD>
                                </TR>
                             </FORM>
                             <FORM method="post" action="Handlers/InternalOrders_Handler.php">
                                  <INPUT name="Type" type="hidden" value="MaintainMarkMultiple">
                                <TR>
                                <TD colspan="4" class="header">Mark Multiple
                                </TD>
                              </TR>
                              <TR>
                                <TD colspan="4">To mark internal orders, specify the particulars and click Mark.
                                </TD>
                              </TR>
                              <TR>
                                <TD class="short">Order Status:
                                </TD>
                                <TD colspan="3">';
                                  BuildCompletionStatusSelector(1, 'StatusMark', 'standard');
                          echo '</TD>
                              </TR>
                              <TR>
                                <TD>Supplier:
                                </TD>
                                <TD colspan="3">';
                                  BuildSupplierSelector(1, 'SupplierMark', 'seltest', "");
                          echo '</TD>
                              </TR>
                              <TR>
                                <TD> 
                                   Date Range: <SPAN class="note">*
                                  </SPAN>
                                </TD>
                                 <TD>'; 
                                  echo "<input name = 'start_dateMark' type='text' size='14' class='datepicker' value='".date('Y/m/d', time())."' /> to  "
                                      . " <input name = 'end_dateMark' type='text' size='14' class='datepicker' value='".date('Y/m/d', time())."' />"; 
                                echo '</TD>'
                                  . '<TD colspan="2" class="right"> 
                                        <INPUT tabindex="1" name="Submit" type="Submit" class="button" value="Mark" /> 
                                    </TD>';                                                     
                                echo '                    
                                  </TR>';
                              
                       }                    
                echo '</FORM>
                    </TABLE>
                  </DIV>';                     
              
            BuildContentHeader('View Internal Order', "", "", true);


            echo '<DIV class="contentflow">
                    <P>Internal orders can be viewed by using the form below.</P>
                    <BR /><BR />
                    <TABLE cellspacing="5" align="center" class="standard">';
                      if ($_SESSION['cAuth'] & 64)
                        echo '<FORM method="post" action="Handlers/InternalOrders_Handler.php">
                                <INPUT name="Type" type="hidden" value="ViewExceeded">
                                <TR>
                                  <TD colspan="4" class="header">View Discrepencies
                                  </TD>
                                </TR>
                                <TR>
                                  <TD colspan="2">To view internal orders with discrepencies, click View.
                                  </TD>
                                  <TD colspan="2" class="right">
                                    <INPUT tabindex="1" name="Submit" type="submit" class="button" value="View" />                   
                                  </TD>
                                </TR>
                              </FORM>';
                echo '<FORM method="post" action="Handlers/InternalOrders_Handler.php">
                        <INPUT name="Type" type="hidden" value="ViewOutstanding">
                        <TR>
                          <TD colspan="4" class="header">View Outstanding
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="2">To view outstanding internal orders, click View.
                          </TD>
                          <TD colspan="2" class="right">
                            <INPUT tabindex="1" name="Submit" type="submit" class="button" value="View" />                   
                          </TD>
                        </TR>
                      </FORM>
                      <FORM method="post" action="Handlers/InternalOrders_Handler.php">
                        <INPUT name="Type" type="hidden" value="ViewSingle">
                        <TR>
                          <TD colspan="4" class="header">View Single
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="4">To view an internal order, specify the order number and click View.
                          </TD>
                        </TR>
                        <TR>
                          <TD class="short">Internal Order:
                            <SPAN class="note">*
                            </SPAN>
                          </TD>';
                            /*if ($_SESSION['cAuth'] & 32)
                              BuildInternalOrderSelector(1, 'InternalOrder', 'standard', "");  
                            else
                              BuildInternalOrderSelector(1, 'InternalOrder', 'standard', "", '1');*/
                            BuildTextBoxCell(8, 'InternalOrder', 'text', 'text short', 7, "");
                    echo '<TD colspan="2" class="right">
                            <INPUT tabindex="1" name="Submit" type="submit" class="button" value="View" />                   
                          </TD>
                        </TR>
                      </FORM>
                      <FORM method="post" action="Handlers/InternalOrders_Handler.php">
                        <INPUT name="Type" type="hidden" value="ViewList">
                        <TR>
                          <TD colspan="4" class="header">View List
                          </TD>
                        </TR>
                        <TR>
                          <TD colspan="4">To view internal orders, specify the particulars and click View.
                          </TD>
                        </TR>
                        <TR>
                          <TD class="short">Order Status:
                          </TD>
                          <TD>';
                            BuildCompletionStatusSelector(1, 'Status', 'standard');
                    echo '</TD>
                        </TR>
                        <TR>
                          <TD>Project:
                          </TD>
                          <TD colspan="4">';
                            BuildProjectSelector(1, 'Project', 'seltest', "", 'All', true);  
                    echo '<input type="checkbox" name="allProjects" id = "allProjects"> All Orders 
                        </TD>
                        </TR>
                        <TR>
                          <TD>Supplier:
                          </TD>
                          <TD colspan="4">';
                            BuildSupplierSelector(1, 'Supplier', 'seltest', "");
                    echo '</TD>
                        </TR>
                          <TR>
                          <TD>Category:
                          </TD>
                          <TD colspan="4">';
                            BuildReportsCategorySelector(1, 'Category', 'seltest', "");
                    echo '</TD>
                        </TR>
                        <TR id="ViewMultiple_DatePickerRow">
                          <TD> 
                          Date Range: <SPAN class="note">*
                            </SPAN>
                          </TD>
                           <TD colspan="4">';
                            echo "<input id = 'start_date' type='text' size='14' class='datepicker' value='".date('Y/m/d', time())."' name='dateselected'/> to  "
                                . " <input id = 'end_date' type='text' size='14' class='datepicker' value='".date('Y/m/d', time())."' name='dateselected'/>"; 
                          echo '</TD>';                                                     
                          echo '<TR>
                                <TD colspan="2"><INPUT id ="deficit"  type="checkbox" name="Deficit" value="Deficit" />View deficit only
                                <INPUT id ="NotDeliverd"  type="checkbox" name="NotDeliverd" value="NotDeliverd" />Not Recieved</TD>
                                <TD class="right" colspan="3">
                                        <INPUT id="MakeList" tabindex="1" name="Submit" type="button" class="button" value="View" />                   
                                </TD>
                            </TR>
                      </FORM>';
              echo '<FORM method="post" action="Handlers/InternalCriticalSpares_Handler.php">
                          <INPUT name="Type" type="hidden" value="ViewExceeded">
                          <TR>
                            <TD colspan="4" class="header">View Cretical Spares
                            </TD>
                          </TR>
                          <TR>
                          <TD colspan="5">To view view critical Spares per project, select project and click view
                          </TD>
                        </TR>
                          <TR> 
                          <TD>Project:</TD>
                            <TD colspan="2">';
              BuildProjectSelector(1, 'PartsProject', 'seltest', "", 'All', true);
              echo '</TD>
                            <TD colspan="2" class="right">
                              <INPUT tabindex="1" name="Submit" type="submit" class="button" value="View" />                   
                            </TD>
                          </TR>
                          <TR>
                          <TD colspan="2">
                          </TD>
                        </TR>
                          <TR>
                            <TD colspan="5">To view view critical Spares for multiple projects per customer, select customer and click view
                            </TD>
                          </TR>
                          <TR> 
                          <TD>Customer:</TD>
                            <TD colspan="2">';
              BuildCustomerSelector(1, 'Customer', 'seltest', true);
              // BuildProjectSelector(1, 'PartsProject', 'seltest', "", 'All', true);
              echo '</TD>
                            <TD colspan="2" class="right">
                              <INPUT tabindex="1" name="Submit" type="submit" class="button" value="View" />                   
                            </TD>
                          </TR>
                        </FORM>';

              echo '</TABLE>
                  </DIV>';
          }
        ////////////////////////////////////////////////////////////////////
        echo' </div>
                      </div>
                  </div>
              </div>
      </section>

    </DIV>';
        ?>
          <?php
          // PHP SCRIPT ////////////////////////////////////////////////////////////
          BuildFooter();
          //////////////////////////////////////////////////////////////////////////
          ?>

          <script src="Scripts/FileScripts/InternalOrders.js" type="text/javascript"></script>

  </BODY>
</HTML>
